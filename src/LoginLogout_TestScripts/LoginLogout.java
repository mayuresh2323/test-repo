package LoginLogout_TestScripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import PageObjectRepository.LoginLogoutPOM;


public class LoginLogout {
	
	WebDriver driver;
	LoginLogoutPOM loginPage;
	//ExcelLib eLib;
	
	
	@BeforeClass
	public void configBeforClass()
	
	{
		driver = Driver.getBrowser();
		loginPage = PageFactory.initElements(driver, LoginLogoutPOM.class);
		
	}
	@Test(priority='1',description="1.To Verify Login to SureMDM")
	public void TestLogin() throws InterruptedException{
		//loginPage.loginToAPP(Config.userID, Config.password, Config.url);
		loginPage.loginToAPP(Config.url);
		
		
		
	}
	

}
