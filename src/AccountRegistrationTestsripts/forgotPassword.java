package AccountRegistrationTestsripts;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class forgotPassword extends Initialization{

	
	@Test(priority='1', description="Verify Already a user? Login in Forgot Password page")
	public void VerifyLoginPressKeyBoardEnter() throws IOException, InterruptedException {
		accountRegistrationPage.logoutfromApp();
		commonmethdpage.loginAgain();
		accountRegistrationPage.clickOnForgotPwd();
		accountRegistrationPage.clickOnAlreadyUserLink();
		accountRegistrationPage.verifynavigatedToSureMDM();
        Reporter.log("Pass >> Verify Login to SureMDM console with valid credentials and press Enter button through keyboard.",true);
		
	}
}
