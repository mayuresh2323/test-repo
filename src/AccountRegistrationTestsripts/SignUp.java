package AccountRegistrationTestsripts;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SignUp extends Initialization {


	@Test(priority='1', description="Verify Sign Up New User with Invalid Email ID.")
	public void VerifyUserWithInvalidEmail() throws IOException, InterruptedException {
		Reporter.log("Verify Sign Up New User with Invalid Email ID.",true);
		accountRegistrationPage.logoutfromApp();
		commonmethdpage.loginAgain();
		accountRegistrationPage.ClickOnNewUserSignUpLink();
		accountRegistrationPage.verifyFreeTrialText();
		accountRegistrationPage.EnterInvalidEmailID();
		accountRegistrationPage.verifyInvalidEmailidErrorMessage();
		Reporter.log("Pass >>Verify Sign Up New User with Invalid Email ID.",true); }


	@Test(priority='2',description="Verify Sign Up New User with valid Email ID and invalid password." )
	public void VerifyUserWithValidUserInvalidPWD() throws IOException,InterruptedException { 
	Reporter.log("Verify Sign Up New User with valid Email ID and invalid password.",true);
	accountRegistrationPage.verifyFreeTrialText();
	accountRegistrationPage.EnterEmailID();
	accountRegistrationPage.EnterweakPassword();
	accountRegistrationPage.verifyEmailIDTickMark();
	accountRegistrationPage.verifyStrengthofPassword(); 
	Reporter.log("Pass >>Verify Sign Up New User with valid Email ID and invalid password.",true); }

	@Test(priority='3',description="Verify Already a user? Login in Sign up page.") 
	public void VerifyAlreadyAUser() throws IOException, InterruptedException {
		Reporter.log("Verify Already a user? Login in Sign up page.",true);
		accountRegistrationPage.verifyFreeTrialText();
		accountRegistrationPage.navigateBacktoSureMDMLogin();
		Reporter.log("Pass >> Verify Already a user? Login in Sign up page.",true); }


	@Test(priority='4',description="Verify Sign Up New User by making few fields empty.") 
	public void VerifyNewUserMakingFewFieldsEmpty() throws IOException,InterruptedException {
		Reporter.log("VVerify Sign Up New User by making few fields empty.",true);
		commonmethdpage.loginAgain();
		accountRegistrationPage.ClickOnNewUserSignUpLink();
		accountRegistrationPage.EnterEmailID();
		accountRegistrationPage.EnterPassword();
		accountRegistrationPage.ConfirmPassword();
		accountRegistrationPage.enterDataInFewSignUPFields();
		accountRegistrationPage.enableEULAcheckbox();
		accountRegistrationPage.clickOnsignBtn();
		accountRegistrationPage.verifyprovideAllfieldsErrorMsg();
		Reporter.log("Pass >> Verify Sign Up New User by making few fields empty.",true); }

	@Test(priority='5',description="Verify disabling EULA checkbox during New User Sign Up.") 
	public void VerifyEULACheckBoxDisable() throws IOException, InterruptedException {
		Reporter.log("VVerify Sign Up New User by making few fields empty.",true);
		accountRegistrationPage.navigateBacktoSureMDMLogin();
		commonmethdpage.loginAgain();
		accountRegistrationPage.ClickOnNewUserSignUpLink();
		accountRegistrationPage.EnterEmailID();
		accountRegistrationPage.EnterPassword();
		accountRegistrationPage.ConfirmPassword();
		accountRegistrationPage.enterDataInSignUPPage(); 
		accountRegistrationPage.clickOnsignBtn();
		accountRegistrationPage.verifylicenseAgreementMsg();
		Reporter.log("Pass >> Verify disabling EULA checkbox during New User Sign Up.", true);
	}


	@Test(priority='6',description="Verify links for Terms of Use, Data Processor Addendum and Privacy Policy." )
	public void ValidateLinksArePresent() throws IOException,InterruptedException { 
	Reporter.log("Verify links for Terms of Use, Data Processor Addendum and Privacy Policy.",true); 
	accountRegistrationPage.ClickOnTermUseLink();
	accountRegistrationPage.windowhandlesTest();
	accountRegistrationPage.verifyTermsofUse();
	accountRegistrationPage.SwitchToMainWindow();

	accountRegistrationPage.ClickOnDataProcessorAddendum();
	accountRegistrationPage.windowhandlesTest();
	accountRegistrationPage.verifyDataProcessorAddendum();
	accountRegistrationPage.SwitchToMainWindow();

	accountRegistrationPage.ClickonPrivacyPolicyLink();
	accountRegistrationPage.windowhandlesTest();
	accountRegistrationPage.verifyPrivacyPolicy();
	accountRegistrationPage.SwitchToMainWindow();

	Reporter.log("Pass >>Verify links for Terms of Use, Data Processor Addendum and Privacy Policy." ,true);
	}


	@Test(priority='7',description="Verify the ZIP/Postal Code Field is present or Not") 
	public void ValidateZIPCodeFieldPresent() throws IOException, InterruptedException {
		Reporter.log("Verify the ZIP/Postal Code Field is present or Not",true);
		accountRegistrationPage.verifyZipCodeFiledForIndia();
		accountRegistrationPage.verifyZipCodeFiledForUs();
		Reporter.log("Pass >> Verify the ZIP/Postal Code Field is present or Not" ,true); }

	@Test(priority='8',description="Verify the ZIP/Postal Code Field by Entering the Valid Value")
	public void ValidateZIPCodeEnteringValidValues() throws IOException,InterruptedException {
		Reporter.log("Verify the ZIP/Postal Code Field by Entering the Valid Value",true);
		accountRegistrationPage.verifyZipCodeFiledForIndia();
		accountRegistrationPage.EnterValuesinZipCode();
		accountRegistrationPage.verifyZipCodeFiledForUs();
		accountRegistrationPage.EnterValuesinZipCode(); Reporter.
		log("Pass >> Verify the ZIP/Postal Code Field by Entering the Valid Value",true); }

	@Test(priority='9',description="Validate the ZIP/Postal Code Field")
	public void ValidateZIPCodeMandatoryforUS() throws IOException,InterruptedException {
		Reporter.log("Verify the ZIP/Postal Code Field by Entering the Valid Value",true);
		accountRegistrationPage.verifyZipCodeFiledForIndia();
		accountRegistrationPage.EnterValuesinZipCode();
		accountRegistrationPage.verifyZipCodeFiledForUs();
		accountRegistrationPage.EnterValuesinZipCode(); Reporter.
		log("Pass >> Verify the ZIP/Postal Code Field by Entering the Valid Value",true); }


	@Test(priority = 'A', description = "Verify intercom feature in Signup Page")
	public void ValidateIntercomSection() throws IOException, InterruptedException {
		Reporter.log("Verify intercom feature in Signup Page", true);
		accountRegistrationPage.verifyInterComSection();
		Reporter.log("Pass >>Verify intercom feature in Signup Page", true);
	}
	@Test(priority = 'B', description = "Verify New User Sign Up? should not be displayed when we launch DNS account.")
	public void ValidateSignUpForNewDNS() throws IOException, InterruptedException {
		Reporter.log("Verify New User Sign Up? should not be displayed when we launch DNS account.", true);
		driver.navigate().to(Config.EnterDNSURL);
		accountRegistrationPage.VerifySignupforNewDNS();

		Reporter.log("Pass >> Verify New User Sign Up? should not be displayed when we launch DNS account.", true);
	}
//Test22

}
