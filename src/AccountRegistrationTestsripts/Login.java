package AccountRegistrationTestsripts;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Login extends Initialization  {
	
	@Test(priority='1', description="Verify Login to SureMDM console with valid credentials and press \"Enter\" button through keyboard.")
	public void VerifyLoginPressKeyBoardEnter() throws IOException, InterruptedException {
		Reporter.log("",true);
		accountRegistrationPage.logoutfromApp();
		commonmethdpage.loginAgain();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword(Config.password);
		accountRegistrationPage.pressEnterToLogin();
		accountRegistrationPage.verifyloginIsSuccessful();
		Reporter.log("Pass >>Verify Login to SureMDM console with valid credentials and press \"Enter\" button through keyboard.",true);
}
	@Test(priority='2', description="Verify trying to login with Invalid Email ID or Password.")
	public void VerifyInvalidUserOrPwd() throws IOException, InterruptedException {
		Reporter.log("",true);
		accountRegistrationPage.logoutfromApp();
		commonmethdpage.loginAgain();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword("Testbn");
		accountRegistrationPage.pressEnterToLogin();
		accountRegistrationPage.verifyInvalidCredentialsMsg();
		Reporter.log("Pass >> Verify trying to login with Invalid Email ID or Password.",true);
}
	
	
}
