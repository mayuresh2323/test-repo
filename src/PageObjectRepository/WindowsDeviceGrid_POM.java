package PageObjectRepository;

import java.awt.Robot;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class WindowsDeviceGrid_POM extends WebDriverCommonLib
{
	Actions act=new Actions(Initialization.driver);
	WebDriverWait wait=new WebDriverWait(Initialization.driver, 20);
	


    JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
	 AssertLib ALib=new AssertLib();
     @FindBy(xpath="//td[@class='DeviceName']")
     private WebElement devicename;
     
     @FindBy(xpath="//td[@class='DeviceModelName']")
     private WebElement devicemodel;
     
     @FindBy(xpath="//td[@class='DeviceIPAddress']")
     private WebElement deviceIP;
     
     @FindBy(xpath="//td[@class='DeviceTimeZone']")
     private WebElement devicetimezone;
     
     @FindBy(id="all_ava_devices")
     private WebElement AllDevices;
     
     @FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
     private WebElement SearchDevice;
     
     @FindBy(xpath="//td[@class='DeviceName']")
     private WebElement DeviceName;
     
     @FindBy(xpath="(//th[@class='LastTimeStamp'])[1]")
     private WebElement DeviceLastConnected;
     
     @FindBy(id="newGroup")
     private WebElement NewGroup;
     
     @FindBy(xpath="(//div[@class='input-group ct-w100']/input[@class='form-control ct-model-input  ct-select-ele'])[1]")
     private WebElement Groupname;
     
     @FindBy(id="groupokbtn")
     private WebElement CreateGroupOKButton;
     
     @FindBy(xpath="(//li[@data-action='droptofolder'])[2]")
     private WebElement MoveToGroupButton;
     
     @FindBy(xpath="(//input[@placeholder='Search By Group Name'])[2]")
     private WebElement SearchGroupsButton;
     
     @FindBy(xpath="//li[@class='list-group-item node-groupList search-result']")
     private WebElement SearchedGroupName;
     
     @FindBy(xpath="//*[@id=\"groupListModal\"]/div/div/div[3]/button")
     private WebElement MoveDeviceButton;
     
     @FindBy(xpath="//div[@id='deleteGroup']")
     private WebElement DeletegroupButton;
     
     @FindBy(id="MoveDeviceWhileDeletingGroup")
     private WebElement ConfirmDeleteButton;
     
     @FindBy(xpath="//*[@id='groupstree']/ul/li[1]")
     private WebElement HomeButton;
     
     @FindBy(xpath="//*[@id='dis-card']/div[9]/p")
     private WebElement DeviceInfoAgentVersion;
     
     @FindBy(xpath="//*[@id='dis-card']/div[3]/p")
     private WebElement DeviceInfoDeviceModel;
     
     @FindBy(xpath="//*[@id='dis-card']/div[5]/p")
     private WebElement DeviceInfoDeviceOS;
     
     @FindBy(xpath="//span[@id='deviceOwnerName']")
     private WebElement DeviveInfoDeviceName;
     
     @FindBy(xpath="//*[@id='networkinfo-card']/div[3]")
     private WebElement DeviceInfoMAC;
     
     @FindBy(xpath="(//p[text()='L1HF8A307D7'])[2]")
     private WebElement DeviceInfoDeviceSerialNumber;
     
     @FindBy(xpath="//div[text()='42Gears-Gen']")
     private WebElement DeviceInfoDeviceWiFiSSID;
     
     @FindBy(xpath="//div[@class='pull-right search showClearBtn']")
     private WebElement CancelButton;
     
     
     @FindBy(xpath="//span[text()='Settings updated successfully.']")
     private WebElement NotificationOnUpdatedSettings;
     
     public void VerifyDeviceName()
     {
    	 String DeviceGridName = devicename.getText();
    	 String actual =DeviceGridName;
	   	 String expected = Config.Windows_DeviceName1;
	   	 String PassStatement ="PASS >> Actual and Expected values are matching"+" : "+DeviceGridName;
	   	 String FailStatement ="FAIL >> Actual and Expected values are not matching"+" : "+DeviceGridName;
	   	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
     }
     public void VerifyDeviceModel()
     {
    	 String DeviceGridmodel = devicemodel.getText();
    	 String actual=DeviceGridmodel;
    	 String expected=Config.Windows_DeviceModel;
    	 String PassStatement ="PASS >> Actual and Expected values are matching"+" : "+DeviceGridmodel;
	   	 String FailStatement ="FAIL >> Actual and Expected values are not matching"+" : "+DeviceGridmodel;
	   	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
     }
     public void VerifyIPAddress()
     {
    	 String DeviceGridIP = deviceIP.getText();
    	 boolean value = DeviceGridIP.contains(".");
    	 String PassStatement ="PASS >> IP Address is not empty"+" : "+DeviceGridIP;
	   	 String FailStatement ="FAIL >> IP Address is empty"+" : "+DeviceGridIP;
	   	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	   		
      }
     public void VerifyDeviceTimeZone()
     {
    	 String DeviceGridTImeZone = devicetimezone.getText();
    	 boolean value = DeviceGridTImeZone.contains("N/A");
    	 String PassStatement="PASS >> Time Zone is N/A For Windows Device"+" : "+DeviceGridTImeZone;
    	 String FailStatement ="FAIL >> Time Zone is" + DeviceGridTImeZone ;
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
     }
     public void VerifyDeviceNetworkOperator()
     {
    	WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='Operator']"));
    	js.executeScript("arguments[0].scrollIntoView()",ele);
    	String DeviceGridOperator = ele.getText();
    	boolean value = DeviceGridOperator.contains("N/A");
    	String PassStatement="PASS >> Network Operator is N/A For Windows Device"+" : "+DeviceGridOperator;
   	    String FailStatement ="FAIL >> Network operator" + DeviceGridOperator ;
        ALib.AssertTrueMethod(value, PassStatement, FailStatement);
     }
     public void VerifyDeviceNixAgentVersion()
     {
       WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='AgentVersion']"));
      js.executeScript("arguments[0].scrollIntoView()",ele);
       String DeviceAgentVersion = ele.getText();
       String actual=DeviceAgentVersion;
       String expected=Config.Windows_NixAgentVersion;
       String PassStatement ="PASS >> Actual and Expected values are matching"+" : "+DeviceAgentVersion;
  	   String FailStatement ="FAIL >> Actual and Expected values are not matching"+" : "+DeviceAgentVersion;
  	   ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

    	  
     }
     public void VerifyDeviceOSVersion()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='ReleaseVersion']"));
    	 js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DeviceOSVersion = ele.getText();
    	 String actual=DeviceOSVersion;
    	 String expected=Config.Windows_OSVersion;
    	 String PassStatement ="PASS >> Actual and Expected OS Versions are matching"+" : "+DeviceOSVersion;
    	 String FailStatement ="FAIL >> Actual and Expected OS Versions  are not matching"+" : "+DeviceOSVersion;
    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
    	 
     }
     public void VerifyDevicePhoneNumber()
     {
    	WebElement ele=Initialization.driver.findElement(By.xpath("//td[@class='PhoneNumber']"));
        js.executeScript("arguments[0].scrollIntoView()",ele);
        String DevicePhoneNumber = ele.getText();
        boolean value = DevicePhoneNumber.contains("N/A");
        String PassStatement ="PASS >> SIM Card is not supported for this device"+" : "+DevicePhoneNumber;
   	    String FailStatement ="FAIL >> Device Phone number is "+DevicePhoneNumber;
   	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
           
     }
     public void VerifyDeivceSerialNumber()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='SerialNumber']"));
    	 js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DeviceSerialNumber = ele.getText();
    	 String actual=DeviceSerialNumber;
    	 String expected=Config.Windows_SerialNumber;
    	 String PassStatement ="PASS >> Actual and Expected SerialNumbers are matching"+" : "+DeviceSerialNumber;
    	 String FailStatement ="FAIL >> Actual and Expected SerialNumbers  are not matching"+" : "+DeviceSerialNumber;
    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
     
     }
     public void verifyDeviceRootStatus()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='RootStatus']"));
    	 js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DeviceRootStatus = ele.getText();
    	 boolean value = DeviceRootStatus.contains("N/A");
    	 String PassStatement ="PASS >> Root Status is Unknown"+" : "+DeviceRootStatus;
    	 String FailStatement ="FAIL >> Root Status is "+DeviceRootStatus;
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
    	 
     }
     public void verifyDeviceKnoxStatus()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='KnoxStatus']"));
    	 js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DeviceKnoxStatus = ele.getText();
    	 boolean value = DeviceKnoxStatus.contains("N/A");
    	 String PassStatement ="PASS >> KNOX Status is N/A for Windows Device"+" : "+DeviceKnoxStatus;
    	 String FailStatement ="FAIL >> KNOX Status is "+DeviceKnoxStatus;
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
     }
     public void verifyDevicePollingMechanism()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='NixPollingType']"));
         js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DevicePollingStatus = ele.getText();
    	 boolean value = DevicePollingStatus.contains("Normal Polling");
    	 String PassStatement="PASS >> Polling Mechanism Status is Normal Polling for Windows Device"+" : "+DevicePollingStatus;
    	 String FailStatement="FAIL >> Polling Mechanism Status for Windows Device"+DevicePollingStatus;
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
     }
     public void verifyDeviceWifiStatus()
     {
    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='IsMobileHotSpotEnabled']"));
    	 js.executeScript("arguments[0].scrollIntoView()",ele);
    	 String DeviceWiFistatus = ele.getText();
         boolean value = DeviceWiFistatus.contains("N/A");
         String PassStatement="PASS >> WiFiHotspot Status is N/A for Windows Device"+" : "+DeviceWiFistatus;
    	 String FailStatement="FAIL >> WiFiHotspot Status for Windows Device"+DeviceWiFistatus;
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);  
     }
     public void verifyTotalNumberOfDevices() throws InterruptedException
     {
    	WebElement ele = Initialization.driver.findElement(By.xpath("//*[@id=\"tableContainer\"]/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button"));
    	ele.click();
    	sleep(2);
    	WebElement ele1 = Initialization.driver.findElement(By.xpath("//a[text()='50']"));
        act.click(ele1).perform();
        Thread.sleep(1000);
        List<WebElement> Home_Devices = Initialization.driver.findElements(By.xpath("//td[@class='DeviceName']"));
        int NoOFDevicesInHome = Home_Devices.size();
        System.out.println(NoOFDevicesInHome);
        sleep(4);
        AllDevices.click();
        Thread.sleep(3000);
        List<WebElement> All_Devices = Initialization.driver.findElements(By.xpath("//td[@class='DeviceName']"));
        int NoOfDevicesInAllDevices = All_Devices.size();
        System.out.println(NoOfDevicesInAllDevices);
        boolean flag;
        if(NoOfDevicesInAllDevices >= NoOFDevicesInHome )
        {
        	flag=true;
        }
        else
        {
        	flag=false;
        }
        String PassStatement="PASS >> Number of devices in all devices are greater than Number of devices in Home" +" ";
   	    String FailStatement="FAIL >> Number of devices in all devices are lesser than Number of devices in Home" +" ";
   	    ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
   	    sleep(7);
      }
     public void VerifySearchingOnDeviceName() throws AssertionError, InterruptedException
     {
    	 Thread.sleep(1000);
    	 SearchDevice.clear();
    	 SearchDevice.sendKeys(Config.Windows_DeviceName1);
    	 waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
    	 String Devicename = Initialization.driver.findElement(By.xpath("//p[text()='"+Config.Windows_DeviceName1+"']")).getText();
    	 String actual=Devicename;
    	 String expected=Config.Windows_DeviceName1;
    	 String PassStatement ="PASS >> Searched device is displayed in the gird"+" : "+Devicename;
    	 String FailStatement ="FAIL >> Searched device is not displayed in the grid";
    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
    	 
     }
     public void VerifySearchingonDeviceModel() throws InterruptedException
     {
    	 Thread.sleep(2000);
    	 SearchDevice.clear();
    	 SearchDevice.sendKeys(Config.Windows_DeviceModel);
    	 sleep(3);
    	 waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
    	 String Devicemodel = Initialization.driver.findElement(By.xpath("//td[@class='DeviceModelName']")).getText();
    	 String actual=Devicemodel;
    	 String expected=Config.Windows_DeviceModel;
    	 String PassStatement="PASS >>Searched Device is Displayed in the grid"+" : "+Devicemodel;
    	 String FailStatement="FAIL >>Searched Device is not displayed in the grid";
    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
    }
    public void verifySearchDevicenameSecondpart() throws InterruptedException
    {
    	SearchDevice.clear();
   	    Thread.sleep(1000);
    	SearchDevice.sendKeys("239");
    	waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
    	 String Devicename = Initialization.driver.findElement(By.xpath("//p[text()='"+Config.Windows_DeviceName1+"']")).getText();
    	 boolean value = Devicename.contains("239");
    	 String PassStatement="PASS >>Searched Device is Displayed in the grid"+" : "+Devicename;
    	 String FailStatement="FAIL >>Searched Device is not displayed in the grid";
    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
    	 
    }
   /* public void verifySearchDeviceByNotes() throws InterruptedException
    {
    	Thread.sleep(1000);
    	SearchDevice.clear();
        SearchDevice.sendKeys(Config.Windows_DeviceName1);
        waitForXpathPresent("//p[text()='KARTHIK-239']");
    	Initialization.driver.findElement(By.xpath("(//a[text()='Edit'])[2]")).click();
    	waitForVisibilityOf("//textarea[@placeholder='Add Notes']");
    	Initialization.driver.findElement(By.xpath("//textarea[@placeholder='Add Notes']")).clear();
    	Thread.sleep(100);
    	Initialization.driver.findElement(By.xpath("//textarea[@placeholder='Add Notes']")).sendKeys(Config.Windows_DeviceNotes);
    	Initialization.driver.findElement(By.xpath("//*[@id=\"popover453448\"]/div[2]/div/form/div/div[1]/div[2]/button[1]")).click();
    	SearchDevice.clear();
    	SearchDevice.sendKeys(Config.Windows_DeviceNotes);
    	waitForXpathPresent("//p[text()='KARTHIK-239']");
    	String DeviceNotes = Initialization.driver.findElement(By.xpath("//td[@class='Notes']")).getText();
    	String actual=DeviceNotes;
    	String expected=Config.Windows_DeviceNotes;
    	String PassStatement="PASS >>Searched Device is Displayed in the grid"+" : "+DeviceNotes;
   	    String FailStatement="FAIL >>Searched Device is not displayed in the grid";
    	ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
    	
    }*/
    public void verifySearchDeviceByConnectionSecurity() throws InterruptedException
    {
    	SearchDevice.clear();
   	    Thread.sleep(1000);
    	SearchDevice.sendKeys(Config.Windows_DeviceName1);
    	waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
        boolean status = Initialization.driver.findElement(By.xpath("//i[@title='Secured Connection']")).isDisplayed();
    	String PassStatement="PASS >>Searched Device is Displayed in the grid Under Secured Connection";
   	    String FailStatement="FAIL >>Searched Device is not displayed in the grid Under Secured Connection";
   	    ALib.AssertTrueMethod(status, PassStatement, FailStatement);	
   	    sleep(5);
    	
    }
    
    public void verifyDeviceBatteryStatus()
    {
    	String BatteryLevel = Initialization.driver.findElement(By.xpath("//td[@class='Battery']")).getText();
    	boolean value=BatteryLevel.contains("N/A");
    	
    	String PassStatement="PASS >>Battery Level is Displayed "+" : "+BatteryLevel ;
   	    String FailStatement="FAIL >>Battery Level is not Displayed"+" : "+BatteryLevel;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    }
    public void verifyDeviceLastConnected()
    {
    	String Lastconnectedtime = Initialization.driver.findElement(By.xpath("//td[@class='LastTimeStamp']")).getText();
    	boolean value=Lastconnectedtime.contains("N/A");
    	String PassStatement="PASS >>Last Connected Time is Displayed "+" : "+Lastconnectedtime ;
   	    String FailStatement="FAIL >>Last Connected Time is not Displayed"+" : "+Lastconnectedtime;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    	
    }
    public void verifyLastDeviceTime()
    {
    	String LastDeviceTime = Initialization.driver.findElement(By.xpath("//td[@class='DeviceTimeStamp']")).getText();
    	boolean value = LastDeviceTime.contains("N/A");
    	String PassStatement="PASS >>Device Last Time is Displayed "+" : "+LastDeviceTime ;
   	    String FailStatement="FAIL >>Device Last Time is not Displayed"+" : "+LastDeviceTime;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    	
    }
    public void verifyDeviceRegistered()
    {
    	String DeviceRegisteredTime = Initialization.driver.findElement(By.xpath("//td[@class='DeviceRegistered']")).getText();
    	boolean value = DeviceRegisteredTime.contains("N/A");
    	String PassStatement="PASS >>Device Registered Time is Displayed "+" : "+DeviceRegisteredTime ;
   	    String FailStatement="FAIL >>Device Registered Time is not Displayed"+" : "+DeviceRegisteredTime;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    	
    }
    
    public void verifyDeviceFreestorageMemeory(String Value)
    {
    	String DeviceFreeStorage = Initialization.driver.findElement(By.xpath("//td[@class='MemoryStorageAvailable']")).getText();
    	System.out.println(DeviceFreeStorage);
    	boolean value = DeviceFreeStorage.contains(Value);
    	String PassStatement="PASS >>Device Free Storage is Displayed "+" : "+DeviceFreeStorage ;
   	    String FailStatement="FAIL >>Device Free Storage is not Displayed"+" : "+DeviceFreeStorage;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    }
    public void verifyDeviceProgramMemory()
    {
    	String DeviceProgramStorage = Initialization.driver.findElement(By.xpath("//td[@class='PhysicalMemoryAvailable']")).getText();
    	boolean value=DeviceProgramStorage.contains("N/A");
    	String PassStatement="PASS >>Device Program Memory is Displayed "+" : "+DeviceProgramStorage ;
   	    String FailStatement="FAIL >>Device Program Memory is not Displayed"+" : "+DeviceProgramStorage;
   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
    	
    }
   public void verifDeviceRoamingStatus()
   {
	   String DeviceRoamingStatus = Initialization.driver.findElement(By.xpath("//td[@class='PhoneRoaming']")).getText();
	   boolean flag=DeviceRoamingStatus.contains("N/A");
	   String PassStatement="PASS >> Roaming Status is N/A for Windows Device"+" : "+DeviceRoamingStatus;
  	   String FailStatement="FAIL >> Roaming Status for Windows Device"+DeviceRoamingStatus;
  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyDeviceSignalStrength()
   {
	   String DeviceSignalStrength = Initialization.driver.findElement(By.xpath("//td[@class='PhoneSignal']")).getText();
	   boolean flag=DeviceSignalStrength.contains("N/A");
	   String PassStatement="PASS >> Signal Strength is N/A for Windows Device"+" : "+DeviceSignalStrength;
  	   String FailStatement="FAIL >> Signal Strength for Windows Device"+DeviceSignalStrength;
  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyDeviceSureLockVersion()
   {
	   String DeviceSureLockVersion=Initialization.driver.findElement(By.xpath("//td[@class='SureFoxVersion']")).getText();
	   boolean flag=DeviceSureLockVersion.contains("Not Installed");
	   String PassStatement="PASS >> SureLock is Not Installed for This Device"+" : "+DeviceSureLockVersion;
  	   String FailStatement="FAIL >> SureLock Version For This Device Is"+DeviceSureLockVersion;
  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyDeviceSureFoxVersion()
   {
	  String DeviceSureFoxVersion = Initialization.driver.findElement(By.xpath("//td[@class='SureFoxVersion']")).getText();
	  boolean flag=DeviceSureFoxVersion.contains("Not Installed");
	  String PassStatement="PASS >> SureFox is Not Installed for This Device"+" : "+DeviceSureFoxVersion;
 	   String FailStatement="FAIL >> SureF Version For This Device Is"+DeviceSureFoxVersion;
 	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	  
   }
   public void verifyDeviceSureVideoVersin()
   {
	    String DeviceSureVideoVersion = Initialization.driver.findElement(By.xpath("//td[@class='SureVideoVersion']")).getText();
	    boolean flag=DeviceSureVideoVersion.contains("Not Installed");
	    String PassStatement="PASS >> SureVideo is Not Installed for This Device"+" : "+DeviceSureVideoVersion;
	 	String FailStatement="FAIL >> SureVideo Version For This Device Is"+DeviceSureVideoVersion;
	 	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	    
   }
   public void verifyDeviceIMEI()
   {
	   String DeviceIMEI = Initialization.driver.findElement(By.xpath("//td[@class='IMEI']")).getText();
	   boolean flag=DeviceIMEI.contains("N/A");
	   String PassStatement="PASS >> IMEI Is N/A For Windows Device"+" : "+DeviceIMEI;
	   String FailStatement="FAIL >> IMEI For This Device Is"+DeviceIMEI;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyDeviceIMEI2()
   {
	   String DeviceIMEI2 = Initialization.driver.findElement(By.xpath("//td[@class='IMEI2']")).getText();
	   boolean flag=DeviceIMEI2.contains("N/A");
	   String PassStatement="PASS >> IMEI2 Is Unknown For Windows Device"+" : "+DeviceIMEI2;
	   String FailStatement="FAIL >> IMEI2 For This Device Is"+DeviceIMEI2;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyDeviceDataUsage()
   {
	    String DeviceDataUsage = Initialization.driver.findElement(By.xpath("//td[@class='DataUsage']")).getText();
	    boolean flag = DeviceDataUsage.contains("0 Bytes");
	    String PassStatement="PASS >> DataUsage is 0 Bytes for This Device"+" : "+DeviceDataUsage;
		String FailStatement="FAIL >> DataUsage For This Device Is"+DeviceDataUsage;
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyDeviceCPUUsage()
   {
	   boolean flag=true;
	   String DeviceCPUUsage = Initialization.driver.findElement(By.xpath("//td[@class='CpuUsage']")).getText();
	   if(DeviceCPUUsage.contains("N/A"))
	   {
		   flag=false;
	   }
	   String PassStatement="PASS >> CPU Usage for This Device"+" : "+DeviceCPUUsage;
	   String FailStatement="FAIL >> CPU Usage For This Device Is N/A";
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
	}
    public void verifyDeviceGPUUsage()
   {
	   String DeviceGPUUsage = Initialization.driver.findElement(By.xpath("//td[@class='GpuUsage']")).getText();
	   boolean flag=DeviceGPUUsage.contains("N/A");
	   String PassStatement="PASS >> GPU Usage for This Device"+" : "+DeviceGPUUsage;
	   String FailStatement="FAIL >> GPU Usage For This Device Is N/A";
	   ALib.AssertFalseMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyDeviceBatteryTemperature()
   {
	   String DeviceBatteryTemperature = Initialization.driver.findElement(By.xpath("//td[@class='Temperature']")).getText();
	   boolean flag=DeviceBatteryTemperature.contains("N/A");
	   String PassStatement="PASS >> Battery Temperature of This Device"+" : "+DeviceBatteryTemperature;
	   String FailStatement="FAIL >> Battery Temperature of This Device Is N/A";
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  	   
   }
   public void verifyDevicesupervisedStatus()
   {
	   String DevicesupervisedStatus = Initialization.driver.findElement(By.xpath("//td[@class='IsSupervised']")).getText();
	   boolean flag=DevicesupervisedStatus.contains("N/A");
	   String PassStatement="PASS >> Supervised Status of This Device is"+" : "+DevicesupervisedStatus;
	   String FailStatement="FAIL >> Supervised Status of This Device is"+" "+DevicesupervisedStatus;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);   
   }
   public void verifyDeviceEnrollmentStatus()
   {
	   String DevicEnrollmentStatus = Initialization.driver.findElement(By.xpath("//td[@class='Isenrolled']")).getText();
	   boolean flag=DevicEnrollmentStatus.contains("Yes");
	   String PassStatement="PASS >> Enrollment Status of This Device is"+" : "+DevicEnrollmentStatus;
	   String FailStatement="FAIL >> Enrollment Status of This Device is"+" "+DevicEnrollmentStatus;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyNetworkType()
   {
	   String DeviceNetworkType = Initialization.driver.findElement(By.xpath("//td[@class='NetworkType']")).getText();
	   boolean flag=DeviceNetworkType.contains("N/A");
	   String PassStatement="PASS >> NetworkType of This Device is"+" : "+DeviceNetworkType;
	   String FailStatement="FAIL >> NetworkType of This Device is"+" "+DeviceNetworkType;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  
   }
   public void verifyUsername()
   {
	   String DeviceUserName = Initialization.driver.findElement(By.xpath("//td[@class='DeviceUserName']")).getText();
	   boolean flag=DeviceUserName.contains("N/A");
	   String PassStatement="PASS >> UserName of This Device is"+" : "+DeviceUserName+" "+"Normal Enrollment";
	   String FailStatement="FAIL >> UserName of This Device is"+" : "+DeviceUserName+" "+"Other Enrollment";
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);    
   }
   public void verifySimSerialNumber()
   {
	   String DeviceSimSerialNumber = Initialization.driver.findElement(By.xpath("//td[@class='SimSerialNumber']")).getText();
	   boolean flag=DeviceSimSerialNumber.contains("N/A");
	   String PassStatement="PASS >> SimSerialNumber of This Device is"+" : "+DeviceSimSerialNumber;
	   String FailStatement="FAIL >> SimSerialNumber of This Device is"+" : "+DeviceSimSerialNumber;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyUSBStatus()
   {
	   String DeviceUSBStatus = Initialization.driver.findElement(By.xpath("//td[@class='USBPluggedIn']")).getText();
	   boolean flag = DeviceUSBStatus.contains("Plugged Out");
	   String PassStatement="PASS >> USB Status of This Device is"+" : "+DeviceUSBStatus;
	   String FailStatement="FAIL >> USB Status of This Device is"+" : "+DeviceUSBStatus;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
   }
   public void verifyBluetoothSSID()
   {
	   String DeviceBluetoothSSID = Initialization.driver.findElement(By.xpath("//td[@class='BSSID']")).getText();
	   boolean flag = DeviceBluetoothSSID.contains("N/A");
	   String PassStatement="PASS >> Bluetooth SSID of This Device is"+" : "+DeviceBluetoothSSID;
	   String FailStatement="FAIL >> Bluetooth SSID of This Device is"+" : "+DeviceBluetoothSSID;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifySurelockSettingsIdentifier()
   {
	   String SurelockSettingsIdentifier = Initialization.driver.findElement(By.xpath("//td[@class='SureLockSettingsVersionCode']")).getText();
	   boolean flag = SurelockSettingsIdentifier.contains("N/A");
	   String PassStatement="PASS >> SurelockSettingsIdentifier of This Device is"+" : "+SurelockSettingsIdentifier;
	   String FailStatement="FAIL >> SurelockSettingsIdentifier of This Device is"+" : "+SurelockSettingsIdentifier;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyDeviceOSBuildNumber()
   {
	   String verifyDeviceOSBuildNumber = Initialization.driver.findElement(By.xpath("//td[@class='OsBuildNumber']")).getText();
	   boolean flag = verifyDeviceOSBuildNumber.contains("N/A");
	   String PassStatement="PASS >> OS Build Number of This Device is"+" : "+verifyDeviceOSBuildNumber;
	   String FailStatement="FAIL >> OS Build Number of This Device is"+" : "+verifyDeviceOSBuildNumber;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifySecurityPatchDate()
   {
	   String SecurityPatchDate = Initialization.driver.findElement(By.xpath("//td[@class='SecurityPatchDate']")).getText();
	   boolean flag = SecurityPatchDate.contains("N/A");
	   String PassStatement="PASS >> SecurityPatchDate of This Device is"+" : "+SecurityPatchDate;
	   String FailStatement="FAIL >> SecurityPatchDate of This Device is"+" : "+SecurityPatchDate;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifyAndriodEnterpriseStatus()
   {
	   String AndroidEnterpriseStatus = Initialization.driver.findElement(By.xpath("//td[@class='AfwProfile']")).getText();
	   boolean flag = AndroidEnterpriseStatus.contains("N/A");
	   String PassStatement="PASS >> AndroidEnterpriseStatus of This Device is"+" : "+AndroidEnterpriseStatus;
	   String FailStatement="FAIL >> AndroidEnterpriseStatus of This Device is"+" : "+AndroidEnterpriseStatus;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	   
   }
   public void verifyLastTimeSync()
   {
	   String LastTimeSync = Initialization.driver.findElement(By.xpath("//td[@class='MTPSystemScanTimeStamp']")).getText();
	   boolean flag = LastTimeSync.contains("N/A");
	   String PassStatement="PASS >> LastTimeSync of This Device is"+" : "+LastTimeSync;
	   String FailStatement="FAIL >> LastTimeSync of This Device is"+" : "+LastTimeSync;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
   public void verifythreatCount()
   {
	   String threatCount = Initialization.driver.findElement(By.xpath("//td[@class='MTPSystemScanThreatCount']")).getText();
	   boolean flag = threatCount.contains("N/A");
	   String PassStatement="PASS >> ThreatCount of This Device is"+" : "+threatCount;
	   String FailStatement="FAIL >> ThreatCount of This Device is"+" : "+threatCount;
	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
   }
  public void verifyEncryptionStatus()
  {
	  String EncryptionStatus = Initialization.driver.findElement(By.xpath("//td[@class='IsEncryptionEnabled']")).getText();
	  boolean flag = EncryptionStatus.contains("N/A");
	  String PassStatement="PASS >> EncryptionStatus of This Device is"+" : "+EncryptionStatus;
	  String FailStatement="FAIL >> EncryptionStatus of This Device is"+" : "+EncryptionStatus;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
  }
  public void verifyCTSVerifiedStatus()
  {
	  String CTSVerifiedStatus = Initialization.driver.findElement(By.xpath("//td[@class='CtsProfileMatch']")).getText();
	  boolean flag = CTSVerifiedStatus.contains("N/A");
	  String PassStatement="PASS >> CTSVerifiedStatus of This Device is"+" : "+CTSVerifiedStatus;
	  String FailStatement="FAIL >> CTSVerifiedStatus of This Device is"+" : "+CTSVerifiedStatus;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
  }
  public void verifyPlatformIntegrity()
  {
	  String PlatformIntegrity = Initialization.driver.findElement(By.xpath("//td[@class='BasicIntegrity']")).getText();
	  boolean flag = PlatformIntegrity.contains("N/A");
	  String PassStatement="PASS >> PlatformIntegrity of This Device is"+" : "+PlatformIntegrity;
	  String FailStatement="FAIL >> PlatformIntegrity of This Device is"+" : "+PlatformIntegrity;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	  
  }
  
  public void verifyThreatProtection()
  {
	  String ThreatProtection = Initialization.driver.findElement(By.xpath("//td[@class='VerifyAppEnable']")).getText();
	  boolean flag = ThreatProtection.contains("N/A");
	  String PassStatement="PASS >> ThreatProtection of This Device is"+" : "+ThreatProtection;
	  String FailStatement="FAIL >> ThreatProtection of This Device is"+" : "+ThreatProtection;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  
  }
  public void verifyUSBEnableStatus()
  {
	  String USBEnableStatus = Initialization.driver.findElement(By.xpath("//td[@class='ADBEnable']")).getText();
	  boolean flag = USBEnableStatus.contains("N/A");
	  String PassStatement="PASS >> USBEnableStatus of This Device is"+" : "+USBEnableStatus;
	  String FailStatement="FAIL >> USBEnableStatus of This Device is"+" : "+USBEnableStatus;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
  }
  public void veriyUnknownSourceApps()
  {
	  String UnknownSourceAppsStatus = Initialization.driver.findElement(By.xpath("//td[@class='AllowUnknownSource']")).getText();
	  boolean flag = UnknownSourceAppsStatus.contains("N/A");
	  String PassStatement="PASS >> UnknownSourceAppsStatus of This Device is"+" : "+UnknownSourceAppsStatus;
	  String FailStatement="FAIL >> UnknownSourceAppsStatus of This Device is"+" : "+UnknownSourceAppsStatus;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
  }
  public void verifyDeviceLocalIP()
  {
	  String DeviceLocalIP = Initialization.driver.findElement(By.xpath("//td[@class='DeviceLocalIPAddress']")).getText();
	  boolean flag = DeviceLocalIP.contains("N/A");
	  String PassStatement="PASS >> DeviceLocalIP of This Device is"+" : "+DeviceLocalIP;
	  String FailStatement="FAIL >> DeviceLocalIP of This Device is"+" : "+DeviceLocalIP;
	  ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	  
  }
  public void verifyWifiSSID()
  {
	  String WifiSSID = Initialization.driver.findElement(By.xpath("//td[@class='WifiSSID']")).getText();
	  boolean flag=WifiSSID.equals(Config.Windows_DeviceWifiSSID);
	  String PassStatement="PASS >> WifiSSID of This Device is"+" : "+WifiSSID;
	  String FailStatement="FAIL >> WifiSSID of This Device is"+" : "+WifiSSID;
	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	  
  }
  public void verifyDeviceGroupPath() throws InterruptedException
  {
	  HomeButton.click();
	  waitForidPresent("deleteDeviceBtn");
	  sleep(3);
	  NewGroup.click();
	  waitForXpathPresent("(//div[@class='input-group ct-w100']/input[@class='form-control ct-model-input  ct-select-ele'])[1]");
	  sleep(3);
	  Groupname.sendKeys("AATestGroupPath");
	  CreateGroupOKButton.click();
	  sleep(15);
	  AllDevices.click();
	  waitForXpathPresent("//td[@class='DeviceName']");
	  sleep(2);
	  SearchDevice.sendKeys(Config.Windows_DeviceName1);
	  sleep(3);
	  waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
	  sleep(7);
	  act.contextClick(devicename).perform();
	  sleep(3);
	  waitForXpathPresent("(//li[@data-action='droptofolder'])[2]");
	  MoveToGroupButton.click();
	  sleep(2);
	  waitForXpathPresent("//li[@class='list-group-item node-groupList']");
	  sleep(3);
	  SearchGroupsButton.sendKeys("AATestGroupPath");
	  waitForXpathPresent("//ul/li[@class='list-group-item node-groupList']");
	  sleep(5);
	  SearchedGroupName.click();
	  sleep(3);
	  MoveDeviceButton.click();	
	  sleep(4);
	  waitForXpathPresent("//p[text()='"+Config.Windows_DeviceName1+"']");
	  sleep(7);
	  WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='DeviceGroupPath']"));
	  js.executeScript("arguments[0].scrollIntoView()",ele);
	  sleep(5);
	  String ActualGroupPath = Initialization.driver.findElement(By.xpath("//td[@class='DeviceGroupPath']")).getText();
	  sleep(2);
	  String ExpectedGroupPath=Config.windows_DeviceGroupPath;
	  String PassStatement="PASS >> GroupPath is Same As Expeced Group Path";
	  String FailStatement="FAIL >> GroupPath is Different From Expected GroupPath";	  
	  ALib.AssertEqualsMethod(ExpectedGroupPath, ActualGroupPath, PassStatement, FailStatement);
	  DeletegroupButton.click();
	  waitForidPresent("MoveDeviceWhileDeletingGroup");
	  sleep(2);
	  ConfirmDeleteButton.click();
}
 // ________________________________________________________________________________________________________________________________
 
  public void VerifyAgentVersionInDeviceInfoSection()
  {
	  String ActualVersion=DeviceInfoAgentVersion.getText();
	  String ExpectedVersion=Config.Windows_NixAgentVersion;
	  String PassStatement ="PASS >> Actual Agent Version and Expected Agent Versions are matching"+" : "+ActualVersion;
	  String FailStatement ="FAIL >> Actual Agent Version and Expected Agent Versions are not matching"+" : "+ActualVersion;
	  ALib.AssertEqualsMethod(ActualVersion, ExpectedVersion, PassStatement, FailStatement);
	  
  }
  public void VrifyDeviceModelInDeviceInfoSection()
  {
	  String ActualModel=DeviceInfoDeviceModel.getText();
	  String ExpectedModel=Config.Windows_DeviceModel;
	  String PassStatement ="PASS >> Actual Device Model and Expected Device Model are matching";
	  String FailStatement ="FAIL >> Actual Device Model and Expected Device Model are not matching";
	  ALib.AssertEqualsMethod(ActualModel, ExpectedModel, PassStatement, FailStatement);
  }
  public void VerifyDeviceOSInDeviceInfoSection()
  {
	  String ActualOS=DeviceInfoDeviceOS.getText();
	  String ExpectedOS=Config.windows_DeviceOperatingSystem;
	  String PassStatement ="PASS >> Actual Device OS and Expected Device OS are matching";
	  String FailStatement ="FAIL >> Actual Device OS and Expected Device OS are not matching";
	  ALib.AssertEqualsMethod(ActualOS, ExpectedOS, PassStatement, FailStatement);
	  
  }
  public void VerifyDeviceNameInDeviceInfoSection()
  {
	  String ActualDeviceName=DeviveInfoDeviceName.getText();
	  System.out.println(DeviveInfoDeviceName);
	  String ExpectedDeviceName=Config.Windows_DeviceName1;
	  String PassStatement ="PASS >> Actual Device Name and Expected Device Name are matching";
	  String FailStatement ="FAIL >> Actual Device Name and Expected Device Name are not matching";
	  ALib.AssertEqualsMethod(ExpectedDeviceName,ActualDeviceName,PassStatement, FailStatement);
	  
  }
  public void VerifyDeviceMACAddressInDeviceInfoSection()
  {
	  String ActualDeivceMAC=DeviceInfoMAC.getText();
	  String ExpectedDeviceMAC=Config.windows_DeviceMACAddress;
	  String PassStatement ="PASS >> Actual Device MAC and Expected Device MAC are matching";
	  String FailStatement ="FAIL >> Actual Device MAc and Expected Device MAc are not matching";
	  ALib.AssertEqualsMethod(ActualDeivceMAC, ExpectedDeviceMAC, PassStatement, FailStatement);
  }
  public void VerifyDeviceSerialNumberInDeviceInfoSection()
  {
	  String ActualDeviceSerialNumber=DeviceInfoDeviceSerialNumber.getText();
	  String ExpectedSerialNumber=Config.Windows_SerialNumber;
	  String PassStatement ="PASS >> Actual Device Serial Number and Expected Device Serial Number are matching";
	  String FailStatement ="FAIL >> Actual Device Serial Number and Expected Device Serial Number are not matching";
	  ALib.AssertEqualsMethod(ActualDeviceSerialNumber, ExpectedSerialNumber, PassStatement, FailStatement);
	  
  }
  public void VerifyDeviceWiFiSSIDInDeviceInfoSection()
  {
	  String ActualDeviceWiFiSSID=DeviceInfoDeviceWiFiSSID.getText();
	  String ExpectedWiFiSSID=Config.Windows_DeviceWifiSSID;
	  String PassStatement ="PASS >> Actual Device WiFi SSID and Expected Device WiFi SSID are matching";
	  String FailStatement ="FAIL >> Actual Device WiFi SSID and Expected Device WiFi SSID are not matching";
	  ALib.AssertEqualsMethod(ActualDeviceWiFiSSID, ExpectedWiFiSSID, PassStatement, FailStatement);
	  
  }
	
	public void LaunchNix() throws Exception{
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm");
		sleep(2);
	}
	
}
   
   
    


