package PageObjectRepository;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class Help_POM extends WebDriverCommonLib {
	AssertLib ALib=new AssertLib();
	
	@FindBy(xpath="//*[@id=\"helpsection_cont\"]")
	private WebElement HelpLink;
	
	@FindBy(xpath = "//a[text()='Categories']")
	private WebElement HelpNewTab;
	
	public void ClickOnHelp() throws InterruptedException{
		
		
		HelpLink.click();
		sleep(2);
		
	}
	
	public void VerifyCommunity() throws InterruptedException
	{
		String originalHandle = Initialization.driver.getWindowHandle();
		Initialization.driver.findElement(By.xpath("//*[@id=\"helplist\"]/div[2]/ul/li[1]")).click();
	    Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
	    ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
	    Initialization.driver.switchTo().window(tabs.get(1));
		boolean isdisplayed=true;
		 
		  try{
			  HelpNewTab.isDisplayed();
				   	 
				    }catch(Exception e)
				    {
				    	isdisplayed=false;
				   	 
				    }
		   Assert.assertTrue(isdisplayed,"FAIL >> 'Community Page did not launch successfully'");
		   Reporter.log("PASS >> 'Community' page launched succesfully",true );		
	     
		   for(String handle : Initialization.driver.getWindowHandles()) {
		        if (!handle.equals(originalHandle)) {
		        	Initialization.driver.switchTo().window(handle);
		        	Initialization.driver.close();
		        }
		    }

		  Initialization.driver.switchTo().window(originalHandle);
		  sleep(3);
	     
	}
}
