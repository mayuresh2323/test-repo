package PageObjectRepository;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.management.loading.PrivateClassLoader;
import javax.xml.xpath.XPath;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.opencv.photo.AlignMTB;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;

public class ReportsPOM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();
	WebDriverWait wait;

	public String originalHandle = Initialization.driver.getWindowHandle();
	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	@FindBy(xpath = "//a[text()='Reports']")
	private WebElement reportButton;

	@FindBy(xpath = "(//span[text()='System Log'])[1]")
	private WebElement systemlogoption;

	@FindBy(xpath = "(//span[text()='Asset Tracking'])[1]")
	private WebElement AssetTrackingOption;

	@FindBy(id = "applicationname")
	private WebElement InstalledJobReportApplicationNameTextBox;

	@FindBy(id = "applicationname")
	private WebElement AppVersionReportApplicationNameTextBox;

	@FindBy(xpath = "(//span[text()='Call Log Tracking'])[1]")
	private WebElement CallLogTrackingOption;

	@FindBy(xpath = "(//span[text()='Jobs Deployed'])[1]")
	private WebElement JobsDeployedOption;

	@FindBy(xpath = "(//span[text()='Installed Job Report'])[1]")
	private WebElement InstalledJobReportOption;

	@FindBy(xpath = "(//span[text()='Device Health Report'])[1]")
	private WebElement DeviceHealthReportOption;

	@FindBy(xpath = "(//span[text()='Device History'])[1]")
	private WebElement DeviceHistoryReportOption;

	@FindBy(xpath = "(//span[text()='Data Usage'])[last()]")
	private WebElement DatausageReportOption;

	@FindBy(xpath = "(//span[text()='Data Usage Legacy'])[1]")
	private WebElement DataUsageLegacyReportOption;

	@FindBy(xpath = "(//span[text()='Device Connected'])[1]")
	private WebElement DeviceConnected;

	@FindBy(xpath = "(//span[text()='App Version'])[1]")
	private WebElement AppVersionReportOption;

	@FindBy(xpath = "(//span[text()='Compliance Report'])[1]")
	private WebElement ComplianceReportReportOption;

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[9]/td/div")
	private WebElement DataUsageReportOption;

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[10]/td/div")
	private WebElement DataUsageLegacyReport;

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[12]/td/div")
	private WebElement AppVersionOption;

	@FindBy(xpath = "//div[@class='cover']/h3")
	private WebElement toastWhenNoOptionSelected;

	@FindBy(id = "ShowAllLogs")
	private WebElement ShowAllLogsTextBox;

	@FindBy(xpath = "//span[@class='ReportBadge']")
	private WebElement badgeCount;

	@FindBy(id = "generateReportBtn")
	private WebElement generateReportButton;

	@FindBy(xpath = "//span[contains(text(),'View Reports')]")
	private WebElement viewReportsOption;

	@FindBy(id = "date")
	private WebElement datE;

	@FindBy(xpath = ".//*[@id='date']")
	private WebElement date1;

	@FindBy(xpath = ".//*[@id='battery']")
	private WebElement BatteryPerformance;

	@FindBy(xpath = ".//*[@id='storagemb']")
	private WebElement storage;

	@FindBy(xpath = ".//*[@id='memorymb']")
	private WebElement physicalMemory;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[5]/input[2]]")
	private WebElement DeviceHealthRepportSelectGroupText;

	@FindBy(xpath = ".//*[@id='dGridContainer']/div[1]/div[1]/div[2]")
	private WebElement DeviceHistorySelectDeviceWrongSearchClear;

	@FindBy(xpath = "//tr[td[p[text()='System Log']]]/td[3]/p[text()='On Demand']")
	private WebElement logsRow ;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[1]")
	private WebElement reportDownloadButton;

	@FindBy(xpath = ".//*[@id='deleteReport']/i")
	private WebElement deleteReportButton;

	@FindBy(xpath = "	.//*[@id='OfflineReportGrid']/tbody/tr[1]/td[1]/p")
	private WebElement reportFirstRow;

	@FindBy(xpath = "//span[text()='On Demand Reports']")
	private WebElement OnDemandReportOption;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]")
	private WebElement viewReport;

	@FindBy(xpath = "//span[text()='Select row to delete.']")
	private WebElement warningmessageOnDeleteWhenNoReportisSelected;

	@FindBy(xpath = "//span[text()='Please select a device or group.']")
	private WebElement warningmessageOnGenerateReportWhenNoDeviceOrGroupisSelected;

	@FindBy(xpath = "//span[text()='Please select a device.']")
	private WebElement warningmessageOnGenerateReportWhenNoDeviceisSelected;

	@FindBy(xpath = "//span[text()='Please select a device from the list.']")
	private WebElement DeviceHistoryWarningMessageWhenNoDeviceisSelected;

	@FindBy(xpath = "//span[text()='No matching result found.']")
	private WebElement InvalidNameReportSearchWarning;

	@FindBy(xpath = "//span[text()='No matching result found.']")
	private WebElement DeviceHistoryWarningMessageWhenWrongDeviceSearchNameisGiven;

	@FindBy(xpath = "//span[text()='Please provide application name.']")
	private WebElement InstallJobReportSelectJobwarningmessageOnGenerateReportWhenNoApplicationNameIsSelected;

	@FindBy(xpath = "//span[text()='Please provide application name.']")
	private WebElement AppVersionWarningmessageOnGenerateReportWhenNoApplicationNameIsSelected;

	@FindBy(xpath = "//span[text()='Application name cannot be less than 3 characters.']")
	private WebElement AppVersionWarningmessageOnGenerateReportWhenApplicationNameisLessThan3Chars;

	// Device Activity

	@FindBy(xpath = ".//*[@id='dGridContainer']/div[1]/div[1]/div[2]/input")
	private WebElement deviceActivitySearchBox;

	@FindBy(xpath = "//span[text()='No matching result found.']")
	private WebElement WarningMessageWhenWrongDeviceSearchNameisGiven;

	@FindBy(xpath = "//p[text()='pers']")
	private WebElement DeviceActivityReportChooseADeviceToAdd;

	@FindBy(xpath = "//p[text()='vinod']")
	private WebElement DeviceActivityReportChooseADeviceToAdd1;

	@FindBy(xpath = "(//span[text()='Device Activity'])[1]")
	private WebElement DeviceActivity;

	@FindBy(xpath = "//h5[text()='Device Activity']")
	private WebElement DeviceAcitivityText;

	@FindBy(xpath = "//p[text()='Device Info Update and Online Offline Report']")
	private WebElement DeviceAcitivityText1;

	@FindBy(xpath = "(.//*[@id='date'])[1]")
	private WebElement DeviceActivityDate;

	@FindBy(xpath = ".//*[@id='masterBody']/div[53]/div[1]/div[1]/input")
	private WebElement DevicestartDate;

	@FindBy(xpath = ".//*[@id='masterBody']/div[53]/div[2]/div[1]/input")
	private WebElement DeviceEndDate;

	@FindBy(xpath = ".//*[@id='masterBody']/div[52]/div[3]/div/button[1]")
	private WebElement ApplyButton;

	@FindBy(xpath = "(.//*[@id='addbtn'])[1]")
	private WebElement AddDecive;

	@FindBy(xpath = "//h4[text()='Add Device']")
	private WebElement AddDeviceTitle;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]")
	private WebElement View1;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[2]/td[5]/a[2]")
	private WebElement View2;
	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[3]/td[5]/a[2]")
	private WebElement View3;
	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[4]/td[5]/a[2]")
	private WebElement View4;
	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[5]/td[5]/a[2]")
	private WebElement View5;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement DeletNo;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement Deletyes;

	@FindBy(xpath = ".//*[@id='dGrid']/tbody/tr/td[1]")
	private WebElement SearchedDevice;

	@FindBy(xpath = "//*[@id='dGridContainer']/div[1]/div[1]/div[2]/input")
	private WebElement SearchDeviceInReports;

	@FindBy(xpath = "//*[@id='reportList_tableCont']/section[1]/div[2]/div[1]/div[2]/input")
	private WebElement SearchDeviceInAppVersionReport;

	@FindBy(xpath = "//span[text()='Please select a job.']")
	private WebElement InstallJobReportSelectJobWarningMessageWhenNoJobisSelected;

	@FindBy(xpath = ".//*[@id='ShowOnlineOfflineLogs']")
	private WebElement ShowOnlineOfflineLogs_TextBox;

	@FindBy(xpath = ".//*[@id='ShowJobLogs']")
	private WebElement ShowJobLogs_TextBox;

	@FindBy(xpath = ".//*[@id='ShowRemoteSupportLogs']")
	private WebElement ShowRemoteSupportLogs_TextBox;

	@FindBy(xpath = ".//*[@id='ShowAppInstallLogs']")
	private WebElement ShowAppInstallLogs_TextBox;

	@FindBy(xpath = ".//*[@id='ShowDeviceInfoLogs']")
	private WebElement ShowDeviceInfoLogs_TextBox;

	@FindBy(id = "OfflineReportGrid")
	private WebElement reportsCount;

	@FindBy(xpath = ".//*[@id='report_viewSect']/div/div[2]/div[2]/div[4]/div[1]/span[2]/span/button")
	private WebElement navigationButton;

	@FindBy(xpath = ".//*[@id='report_viewSect']/div/div[2]/div[2]/div[4]/div[1]/span[2]/span/ul/li[2]/a")
	private WebElement navigationTo100;

	@FindBy(xpath = ".//*[@id='report_viewSect']/div/div[2]/div[2]/div[4]/div[1]/span[2]/span/ul/li[3]/a")
	private WebElement navigationTo200;

	@FindBy(xpath = ".//*[@id='masterBody']/div[45]/div[3]/ul/li[1]")
	private WebElement yesterdayData;

	@FindBy(xpath = "//span[text()='Please select a device from the list.']")
	private WebElement DeviceWarningMessageWhenNoDeviceisSelected;

	@FindBy(xpath = ".//*[@id='masterBody']/div[51]/div[1]/div[2]/table/thead/tr[1]/th[2]")
	private WebElement Calendar1Text;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[2]/input[2]")
	private WebElement selectGroupTextBox;

	@FindBy(xpath = ".//*[@id='deviceid']/input[2]")
	private WebElement DeviceHistorySelectDeviceTextBox;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[5]/input[2]")
	private WebElement DeviceHealthReportSelectGroupTextBox;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[3]/input[2]")
	private WebElement InstallJobReportSelectJobTextBox;

	@FindBy(xpath = ".//*[@id='SelectGroup']")
	private WebElement selectgroupbutton;

	@FindBy(xpath = ".//*[@id='masterBody']/div[51]/div[2]/div[2]/table/thead/tr[1]/th[2]")
	private WebElement Calendar2Text;

	@FindBy(xpath = ".//*[@id='masterBody']/div[43]/div[1]/div[2]/table/tbody/tr[4]/td[2]")
	private WebElement DatePickup;

	@FindBy(xpath = ".//*[@id='masterBody']/div[43]/div[1]/div[2]/table/tbody/tr[5]/td[2]")
	private WebElement DatePickup1;

	@FindBy(xpath = ".//*[@id='masterBody']/div[43]/div[3]/div/button[text()='Apply']")
	private WebElement CalendarCustomApply;

	@FindBy(xpath = ".//*[@id='masterBody']/div[43]/div[3]/div/button[text()='Cancel']")
	private WebElement CalendarCustomCancel;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr/td[1]/p[text()='System Log']")
	private WebElement SysLogCountRow;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[1]/span[1][text()='Select Group']")
	private WebElement SelectGroupText;

	@FindBy(xpath = ".//*[@id='DeviceName']/span[1][text()='Select Device or Group']")
	private WebElement SelectDeviceOrGroupText;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[2]/span[1][text()='Select Group']")
	private WebElement SelectDeviceOrGroupTextInJobDeployed;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Jobs Deployed']")
	private WebElement JobsDepoyedWindowText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Device History']")
	private WebElement DeviceHistoryWindowText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Device Health Report']")
	private WebElement DeviceHealthReportWindowText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Installed Job Report']")
	private WebElement InstalledJobReportWindowHeaderText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Device History']")
	private WebElement DeviceHistoryWindowHeaderText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='App Version']")
	private WebElement AppVersionWindowHeaderText;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Data Usage']")
	private WebElement DataUsageHeaderText;

	@FindBy(xpath = "//span[text()='Date & Time']")
	private WebElement WindowDateText;

	@FindBy(xpath = "//span[text()='Select Device']")
	private WebElement DeviceHealthSelectDeviceText;

	@FindBy(xpath = "//span[text()='Date']")
	private WebElement DeviceHealthReportWindowDateText;

	@FindBy(xpath = "//span[text()='Battery Percentage <=']")
	private WebElement DeviceHealthReportWindowBatteryPercentageText;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[3]/span[1]")
	private WebElement InstalledJobReportWindowSelectGroupText;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[2]/span[1]")
	private WebElement DataUsageReportWindowSelectGroupText;

	@FindBy(xpath = "//span[text()='Application Name']")
	private WebElement InstalledJobReportWindowApplicationNameText;

	@FindBy(xpath = ".//*[@id='addbtn']")
	private WebElement SelectDeviceOrGroupbuton;

	@FindBy(xpath = ".//*[@id='SelectJob']")
	private WebElement InstalledJobReportSelectJobButton;

	@FindBy(xpath = ".//*[@id='reportList_tableCont']/section[1]/div[4]/div/div")
	private WebElement textForNoDataAvailable;

	@FindBy(xpath = ".//*[@id='reportList_tableCont']/section[1]/div[4]/div")
	private WebElement textForAllReportColumnNames;

	@FindBy(xpath = ".//*[@id='deviceGroupTable']/div/div/div[1]/h4")
	private WebElement isAddDeviceWindowdisplayed;

	@FindBy(xpath = ".//*[@id='groupListModal']/div/div/div[1]/h4")
	private WebElement GroupListWindow;

	@FindBy(xpath = ".//*[@id='adddevicebtn']")
	private WebElement AddButtoninWindow;

	@FindBy(xpath = ".//*[@id='ApplicationType']")
	private WebElement AppversionApplicationType;

	@FindBy(xpath = ".//*[@id='groupList']/ul/li[2]")
	private WebElement SelectaGroupOnGroupListWindow;

	@FindBy(xpath = ".//*[@id='groupList']/ul")
	private WebElement DataUsageChooseAGroup;

	@FindBy(xpath = ".//*[@id='groupListModal']/div/div/div[3]/button")
	private WebElement OkbuttononOnWindow;

	@FindBy(id = "okbtn")
	private WebElement InstalledJobReportWindowOkButton;

	@FindBy(xpath = ".//*[@id='selJob_addList_modal']/div[1]/h4")
	private WebElement InstalledJobReportSelectJobWindowHeaderText;

	@FindBy(xpath = ".//*[@id='conmptableContainer']/div[3]/div/div")
	private WebElement InstalledJobReportMessageWhenNoJobsAvailable;

	@FindBy(xpath = "//p[text()='gps test']")
	private WebElement InstalledJobReportChooseLockJobToAdd;

	@FindBy(xpath = "//p[text()='testdevice']")
	private WebElement DeviceHistoryJobReportChooseADeviceToAdd;

	@FindBy(xpath = "//p[text()='File Transfer']")
	private WebElement InstalledJobReportChooseFileTransferJobToAdd;

	@FindBy(xpath = ".//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement InstalledJobReportSearchJobName;

	@FindBy(xpath = ".//*[@id='dGridContainer']/div[1]/div[1]/div[2]/input")
	private WebElement DeviceHisrotySearchADevice;

	@FindBy(xpath = ".//*[@id='SelectGroup']/i")
	private WebElement SelectGrouptButton;

	@FindBy(xpath = ".//*[@id='report_viewSect']/div/div[2]/div[1]/div[2]/input")
	private WebElement ReportSearchButtonInViewReport;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr")
	private WebElement SearchResultForAllLogsInViewReports;

	@FindBy(xpath = ".//*[@id='report_genrateSect']/div[1]/div[1]/div[1]/div/input")
	private WebElement reportMainSearchButton;

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr/td/div")
	private WebElement reportMainSearchSelectReport;

	@FindBy(id = "reportname")
	private WebElement ReportNameinViewReportHeader;

	@FindBy(xpath = "//p[text()='System Log']")
	private WebElement SystemLogSearch;

	@FindBy(xpath = "//p[text()='Asset Tracking']")
	private WebElement AssettrackingLogSearch;

	@FindBy(xpath = "//p[text()='Call Log Tracking']")
	private WebElement CallLogSearch;

	@FindBy(xpath = "//p[text()='Jobs Deployed']")
	private WebElement JobsDeployedLogSearch;

	@FindBy(xpath = "//p[text()='Installed Job Report']")
	private WebElement InstalledJobLogSearch;

	@FindBy(xpath = "//p[text()='Device Health Report']")
	private WebElement DeviceHealthReportLogSearch;

	@FindBy(xpath = "//p[text()='Device History']")
	private WebElement DeviceHistoryLogSearch;

	@FindBy(xpath = "//p[text()='Data Usage']")
	private WebElement DataUsageLogSearch;

	@FindBy(xpath = "//p[text()='Device Connected']")
	private WebElement DeviceConnectedLogSearch;

	@FindBy(xpath = "//*[@id='SelectGroup']")
	private WebElement SelectGroup;

	@FindBy(xpath = "//p[text()='App Version']")
	private WebElement AppVersionLogSearch;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[1]/p")
	private WebElement SearchReportResultFirstRowReport;

	@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td/p[text()='Data Usage']")
	private WebElement SearchReportResultFirstRowDataUsage;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesButtonInDeleteReportPopUp;

	@FindBy(xpath = ".//*[@id='dtree']/ul/li[3]")
	private WebElement SelectedGroup;

	@FindBy(xpath = "//*[@id='reportsGrid']/tbody/tr[2]/td/div/span[text()='Asset Tracking']")
	private WebElement AssetTracking;

	String DeviceHealthReportFrontElements = ".//*[@id='reportDetails']/div";

	String xpathlen = ".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th";

	SoftAssert s_assert = new SoftAssert();

	public void ClickOnReports_Button() throws InterruptedException {
		try {
			waitForXpathPresent("//a[text()='Reports']");
			sleep(4);
			reportButton.click();
			waitForXpathPresent("//span[text()='On Demand Reports']");
			sleep(2);
			Reporter.log("PASS>> Clicked On Reports", true);
		} catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//li[@class='nav-subgrp actAsSubGrp']/p/span[text()='More']"))
					.click();
			sleep(2);
			reportButton.click();
			waitForXpathPresent("//span[text()='On Demand Reports']");
			sleep(2);
			Reporter.log("PASS >> Clicked On Reports", true);
		}
	}

	@FindBy(xpath = "//*[@id='SelectJob']")
	private WebElement SelectJobInReport;

	public void ClickOnSelectJobOption() throws InterruptedException {
		SelectJobInReport.click();
		sleep(4);
	}

	// to validate system Log reports
	public void systemreports() throws InterruptedException, NumberFormatException, EncryptedDocumentException,
			InvalidFormatException, IOException {

		systemlogoption.click();
		waitForidPresent("ShowAllLogs");

		if (ShowAllLogsTextBox.isEnabled()) {

			Reporter.log("PASS>> Clicked on System logs and System Log window opened with show log option enabled",
					true);
		} else

		{
			Reporter.log("PASS>> Either System Log window did not open or 'Show All Logs' is disabled", true);

		}

	}

	// this is a common method for UnreadReportCount
	public void CountOfUnreadReports() throws InterruptedException {

		String s1 = badgeCount.getText();
		System.out.println(s1);
		Helper.highLightElement(Initialization.driver, generateReportButton);
		generateReportButton.click();
		int s11;
		if (s1 != null && !s1.isEmpty()) {
			s11 = Integer.parseInt(s1);

		} else {
			s11 = 0;
		}
		Reporter.log("PASS>> Initial unread report count is: " + s11, true);
		sleep(3);
		viewReportsOption.click();
		// badgeCount.click(); sleep(2);
		sleep(15);
		Initialization.driver.findElement(By.xpath("//section[@id='report_viewSect']/div/div[2]/div/div/button"))
				.click();
		sleep(30);
		String s2 = badgeCount.getText();
		System.out.println(s2);
		int s22;
		if (s1 != null && !s1.isEmpty()) {
			s22 = Integer.parseInt(s2);

		} else {
			s22 = 0;
		}

		Reporter.log("PASS>> Unread Report Count After Clicked on View Report Option: " + s22, true);

		boolean b1 = s22 > s11;

		Assert.assertTrue(b1, "FAIL>> Badge Count displayed is wrong");
		Reporter.log("PASS>> Badge Count is increased", true);
		sleep(5);
		reportFirstRow.click();
		sleep(5);
		String s3 = badgeCount.getText();
		int s33 = Integer.parseInt(s3);
		Reporter.log("PASS>> Unread Report Count after clicking on a report: " + s33, true);
		boolean b2 = s33 < s22;
		Assert.assertTrue(b2, "FAIL>> Badge Count displayed is wrong");
		Reporter.log("PASS>> Badge Count is decreased", true);

		// waitForidPresent("ShowAllLogs");
		sleep(4);

	}

	// Custom Reports
	public void CustomReports()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		datE.click();
		int count1 = 1;
		for (int i = 0; i <= count1; i++)
			if (i == 0) {
				String Actualvalue = Calendar1Text.getText();
				String Expectedvalue = ELib.getDatafromExcel("Sheet1", 75, 1);
				String PassStatement = "PASS>> Calendar of " + Expectedvalue + " displayed successfully";
				String FailStatement = "FAIL >>Calendar not displayed or wrong value displayed";
				ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
				sleep(5);
			} else if (i == 1) {
				datE.click();
				String Actualvalue = Calendar2Text.getText();
				String Expectedvalue = ELib.getDatafromExcel("Sheet1", 76, 1);
				String PassStatement = "PASS>> Calendar of " + Expectedvalue + " displayed successfully";
				String FailStatement = "FAIL >>Calendar not displayed or wrong value displayed";
				ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
			}

		String txt = DatePickup.getText();
		Reporter.log("Start date: " + txt);
		DatePickup.click();
		String txt1 = DatePickup1.getText();
		Reporter.log("End date: " + txt1);
		DatePickup1.click();
		sleep(4);
		CalendarCustomApply.click();
		sleep(4);

		// to check cancel of the menu
		datE.click();
		CalendarCustomCancel.click();
		boolean b = true;
		try {
			CalendarCustomCancel.isDisplayed();

		} catch (Exception e) {
			b = false;

		}

		Assert.assertTrue(b, "FAIL>> Cancel did not happen");
		Reporter.log("PASS>> Custom menu dismissed successfully");
		// CountOfUnreadReports();

	}

	// DeletingReport
	public void Validation_DeleteReport() throws InterruptedException {
		viewReportsOption.click();
		sleep(4);
		deleteReportButton.click();
		sleep(3);
		Reporter.log("PASS>> Deleted Report Successfully", true);
	}

	// ********Deleting Report when no report is selected**********************
	public void Validation_DeleteReport_WhenNoReportIsSelected() throws InterruptedException {
		deleteReportButton.click();
		sleep(2);

		boolean b = true;

		try {
			warningmessageOnDeleteWhenNoReportisSelected.isDisplayed();

		} catch (Exception e) {
			b = false;

		}

		Assert.assertTrue(b, "Fail: Receiving of notification failed");
		Reporter.log("PASS>> Alert Message : 'Select a row to delete.' is received", true);
		sleep(2);

	}

	// ******Downloading Report***********
	public void Validation_Download_Logs_Report() throws InterruptedException {
		reportFirstRow.click();
		sleep(2);
		reportDownloadButton.click();
		sleep(5);
		Initialization.driver.findElement(By.xpath("//*[@id='downloadReports_modal']/div/div[1]/img")).click();
		Reporter.log("PASS >> Downloaded report successfully - please refer screenshot");

	}

	// *******Method for Clicking on the navigating button *********
	public void navtigationButtonMethod() {
		if (navigationButton.isDisplayed()) {
			navigationButton.click();
			waitForidPresent("report_viewSect");
		} else {
			Reporter.log("PASS >> Navigation button not displayed- less than 50 rows of reports");
		}
	}

	public void Validation_Pagination() throws InterruptedException {
		viewReportsOption.click(); // it's dummy just used
		// System.out.println(SysLogCountRow.getSize());

		// getting default dimension when 50 devices are being displayed
		// Dimension reportCountsFor50 = reportsCount.getSize();
		// System.out.println(reportCountsFor50);
		// Reporter.log("PASS>> Dimension for 50 devices display is (1106,
		// 1277)",true);
		sleep(3);
		navtigationButtonMethod(); // calling the method

		// navigating to 100 in the DropDown
		if (navigationTo100.isDisplayed()) {
			navigationTo100.click();

			Dimension reportCountsFor100 = reportsCount.getSize();
			System.out.println(reportCountsFor100);
			Reporter.log("PASS>> Dimension for 100 devices display is (1106, 2527)-- Navigated successfully", true);

			navtigationButtonMethod(); // calling the method
		} else {
			Reporter.log("PASS >> Navigation to 100 is not avaiable since less than 50 report rows");
		}

		// navigating to 200 in the DropDown
		/*
		 * if(navigationTo200.isDisplayed()) { navigationTo200.click(); Dimension
		 * reportCountsFor200 = reportsCount.getSize();
		 * System.out.println(reportCountsFor200); Reporter.
		 * log("PASS>> Dimension for 200 devices display is (1106, 3777)-- Navigated successfully"
		 * ,true); }else{ Reporter.
		 * log("PASS >> Navigation to 200 is not avaiable since less than 50 report rows"
		 * ); }
		 */
		sleep(3);

		OnDemandReportOption.click();// keeping for continuation.
		sleep(3);
	}

	public void MobileDataUsageColumnVerificationDataUsageLegacyReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();

			value = ActualValue.equalsIgnoreCase(Config.AvailableStorageColumnValue1)
					|| ActualValue.equalsIgnoreCase(Config.AvailableStorageColumnValue2)
					|| ActualValue.equalsIgnoreCase(Config.WifiDataUsageLegacyColumnNegativeValue);

			if (value == true) {

				String FailMessage = "Mobile Data Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Mobile Data Usage in row is correct";
				System.out.println(PassMessage + ActualValue);
			}
		}

	}

	public void Validation_AlltheCheckboxesofSystemlogs() throws InterruptedException {

		// disable ShowAllLogs text box if it's enabled
		systemlogoption.click();
		waitForidPresent("ShowAllLogs");
		boolean x = ShowAllLogsTextBox.isEnabled();

		if (x == true)
			ShowAllLogsTextBox.click();
		Reporter.log("PASS>> ShowAllLogs Checkbox is unchecked", true);

		sleep(4);

		// Check if ShowOnlineOfflineLogs is enabled
		boolean check;
		try {
			ShowOnlineOfflineLogs_TextBox.isEnabled();
			check = true;
		} catch (Throwable T) {
			check = false;
		}
		String PassStatement1 = "PASS >> Show Online Offline Logs TextBox is Enabled";
		String FailStatement1 = "FAIL >> Show Online Offline Logs TextBox is Disabled";
		ALib.AssertTrueMethod(check, PassStatement1, FailStatement1);
		sleep(3);

		// Check if Show Job Logs is enabled
		boolean check1 = ShowJobLogs_TextBox.isEnabled();

		if (check1 == true) {

			Reporter.log("PASS>> show Job Logs TextBox is Enabled", true);
		} else {
			Reporter.log("FAIL>>show Job Logs TextBox is disabled", true);
		}

		// Check if Show Remote Support Logs TextBox is enabled
		boolean check2 = ShowRemoteSupportLogs_TextBox.isEnabled();
		if (check2 == true) {

			Reporter.log("PASS>> Show Remote Support Logs TextBox is enabled", true);
		} else {
			Reporter.log("FAIL >>Show Remote Support Logs TextBox is disabled", true);
		}

		// Check if ShowApp Install Logs text box is enabled
		boolean check3 = ShowAppInstallLogs_TextBox.isEnabled();
		if (check3 == true) {
			Reporter.log("PASS>> Show App Install Logs TextBox is enabled", true);
		} else {
			Reporter.log("FAIL >>Show App Install Logs TextBox is disabled", true);
		}

		// Check if Show Device Info Logs TextBox is enabled
		boolean check4 = ShowDeviceInfoLogs_TextBox.isEnabled();
		if (check4 == true) {
			Reporter.log("PASS>> Show Device Info Logs TextBox is enabled", true);
		} else {
			Reporter.log("PASS>> Show Device Info Logs TextBox is disabled", true);
		}
		ShowAllLogsTextBox.click();
		Reporter.log("PASS>> ShowAllLogs Checkbox is checked back", true);

	}

	public void AssetTrackingReport() throws InterruptedException {
		AssetTrackingOption.click();
		sleep(2);
		if (SelectGroupText.isDisplayed()) {
			Reporter.log("PASS>> Clicked on Assert Tracking logs and window opened successfully", true);
		} else {
			Reporter.log("FAIL>> Either Assert Tracking logs window did not open", true);
		}
		sleep(2);
		CountOfUnreadReports();
		viewReportsOption.click();
		sleep(4);
		boolean isDeletebuttonVisible;
		try {
			deleteReportButton.isDisplayed();
			isDeletebuttonVisible = true;
		} catch (Throwable T) {
			isDeletebuttonVisible = false;
		}
		String PassStatement1 = "PASS >> Delete Button is visible;view Reports page displayed";
		String FailStatement1 = "FAIL >> Delete button is missing or View Reports page did not load successfully";
		ALib.AssertTrueMethod(isDeletebuttonVisible, PassStatement1, FailStatement1);
		sleep(3);
	}

	public void DeleteAssetTrackingReportWhenNoReportSelected() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='deleteReport']/i")));
		Validation_DeleteReport_WhenNoReportIsSelected();
		sleep(6);
	}

	public void DeleteDeviceHistoryReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Device History']"));
		int deviceHistoryReportCount = ls.size();
		System.out.println(deviceHistoryReportCount);
		deleteReportButton.click();
		sleep(5);
		YesButtonInDeleteReportPopUp.click();
		sleep(2);
		// List<WebElement> ls1=
		// Initialization.driver.findElements(By.xpath("//p[text()='Device
		// History']"));
		// int count2 = ls1.size();
		// if(count2==(deviceHistoryReportCount-1))
		{
			Reporter.log("PASS >> deleted device history report successfully");
			// else/*
			/*
			 * { Assert.fail("FAIL >> failed to delete report");
			 */
		}
	}

	public void DeleteDataUsageReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Data Usage']"));
		int dataUsageReportCount = ls.size();
		System.out.println(dataUsageReportCount);
		deleteReportButton.click();
		sleep(6);
		YesButtonInDeleteReportPopUp.click();
		sleep(2);
		/*
		 * List<WebElement> ls1= Initialization.driver.findElements(By.
		 * xpath("//p[text()='Data Usage']")); int count2 = ls1.size();
		 * if(count2==(dataUsageReportCount-1)) {
		 */
		Reporter.log("PASS >> deleted data usage report successfully");
		/*
		 * } else { Assert.fail("FAIL >> failed to delete report"); }
		 */
	}

	public void DeleteDeviceConnectedReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Device Connected']"));
		int deviceConnectedReportCount = ls.size();
		System.out.println(deviceConnectedReportCount);
		deleteReportButton.click();
		sleep(6);
		YesButtonInDeleteReportPopUp.click();
		Reporter.log("PASS >> deleted device connected report successfully");

		// List<WebElement> ls1=
		// Initialization.driver.findElements(By.xpath("//p[text()='Device
		// Connected']"));
		// int count2 = ls1.size();
		// if(count2==(deviceConnectedReportCount-1))
		// {
		// Reporter.log("PASS >> deleted device connected report successfully");
		// } else
		// {
		// Assert.fail("FAIL >> failed to delete report");
		// }
		sleep(3);
	}

	public void DeleteAppVersionReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='App Version']"));
		int appVersionReportCount = ls.size();
		System.out.println(appVersionReportCount);
		deleteReportButton.click();
		sleep(6);
		YesButtonInDeleteReportPopUp.click();
		sleep(2);
		/*
		 * List<WebElement> ls1= Initialization.driver.findElements(By.
		 * xpath("//p[text()='App Version']")); int count2 = ls1.size();
		 * if(count2==(appVersionReportCount-1)) {
		 */
		Reporter.log("PASS >> deleted app version report successfully");
		/*
		 * } else { Assert.fail("FAIL >> failed to delete report"); }
		 */
	}

	public void DeleteAssetTrackingReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Asset Tracking']"));
		int assetTrackingReportCount = ls.size();
		System.out.println(assetTrackingReportCount);
		deleteReportButton.click();
		sleep(8);
		List<WebElement> ls1 = Initialization.driver.findElements(By.xpath("//p[text()='Asset Tracking']"));
		int count2 = ls1.size();
		if (count2 == (assetTrackingReportCount - 1)) {
			Reporter.log("PASS >> deleted asset tracking report successfully");
		} else {
			Assert.fail("FAIL >> failed to delete report");
		}
	}

	public void DeleteJobsDeployedReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Jobs Deployed']"));
		int jobsDeployedReportCount = ls.size();
		System.out.println(jobsDeployedReportCount);
		deleteReportButton.click();
		sleep(6);
		YesButtonInDeleteReportPopUp.click();
		sleep(2);
		// List<WebElement> ls1=
		// Initialization.driver.findElements(By.xpath("//p[text()='Jobs
		// Deployed']"));
		// int count2 = ls1.size();
		// if(count2==(jobsDeployedReportCount-1))
		{
			Reporter.log("PASS >> deleted jobs deployed report successfully");
			// } else
			// {
			// Assert.fail("FAIL >> failed to delete report");
		}
	}

	public void DeleteInstalledJobReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Installed Job Report']"));
		int installedJobReportCount = ls.size();
		System.out.println(installedJobReportCount);
		deleteReportButton.click();
		sleep(5);
		YesButtonInDeleteReportPopUp.click();
		// List<WebElement> ls1=
		// Initialization.driver.findElements(By.xpath("//p[text()='Installed
		// Job Report']"));
		// int count2 = ls1.size();
		// if(count2==(installedJobReportCount-1))
		{
			Reporter.log("PASS >> deleted installed job report successfully");
		} /*
			 * else { Assert.fail("FAIL >> failed to delete report"); }
			 */
		sleep(3);
	}

	public void DeleteDeviceHealthReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Device Health Report']"));
		int deviceHealthReportCount = ls.size();
		System.out.println(deviceHealthReportCount);
		deleteReportButton.click();
		sleep(6);
		YesButtonInDeleteReportPopUp.click();
		sleep(2);
		// List<WebElement> ls1=
		// Initialization.driver.findElements(By.xpath("//p[text()='Device
		// Health Report']"));
		// int count2 = ls1.size();
		// if(count2==(deviceHealthReportCount-1))
		// {
		Reporter.log("PASS >> deleted device health report successfully");
		// } else
		// {
		// Assert.fail("FAIL >> failed to delete report");
		// }
	}

	public void ViewReports() throws InterruptedException {

		// ***********Verifying Header of the viewing report and its
		// columns************
		viewReportsOption.click();
		sleep(4);

		viewReport.click();

		sleep(3);
		// String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1)); // it's related
		// items are on
		// top

		String Actualvalue = Initialization.driver.getTitle();
		String Expectedvalue = "SureMDM Report Viewer";
		String PassStatement = "PASS>> " + Expectedvalue
				+ " is displayed as title of the view report window-New Tab opened successfully";
		String FailStatement = "FAIL >>Wrong title or unable to view reports";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(5);
		Reporter.log("");
		Reporter.log("===Verifying Header of the viewing report and its columns=====");
		Reporter.log("");
		Reporter.log("The Report Name is--" + ReportNameinViewReportHeader.getText());
		Reporter.log("--------------------------------");
		/*
		 * String ColumnsContents =Initialization.driver.findElement(By.xpath(
		 * ".//*[@id='reportList_tableCont']/section[1]/div[2]")).getText();
		 * System.out.println("Columns Shown are=="+ColumnsContents);
		 */
		sleep(3);

		List<WebElement> ColumnHeaderCount = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int countOfColumns = ColumnHeaderCount.size();
		Reporter.log("Total Column Count is : " + countOfColumns);
		if (countOfColumns == 4 || countOfColumns == 7 || countOfColumns == 33 || countOfColumns == 5
				|| countOfColumns == 6 || countOfColumns == 3 || countOfColumns == 8) {
			Reporter.log("PASS >> Total Column Counts is correctly Displayed");
			Reporter.log("------------------------------");
		} else {
			Reporter.log("FAIL >> Total Column Counts is wrong");
			Reporter.log("------------------------------");
		}
		for (int i = 0; i < countOfColumns; i++) {
			String actual = ColumnHeaderCount.get(i).getText();
			Reporter.log("Column Name Found --" + actual);

			if (i == 0) {
				if (actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Name")
						|| actual.equalsIgnoreCase("Job Schedule Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
				}
			}
			if (i == 1) {
				if (actual.equalsIgnoreCase("Device Model") || actual.equalsIgnoreCase("Log Type")
						|| actual.equalsIgnoreCase("Last Connected") || actual.equalsIgnoreCase("Job Deployed Time")
						|| actual.equalsIgnoreCase("Mobile Data Usage (MB)") || actual.equalsIgnoreCase("Phone Number")
						|| actual.equalsIgnoreCase("SureFox") || actual.equalsIgnoreCase("Job Name")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}

			}
			if (i == 2) {
				if (actual.equalsIgnoreCase("OS Version") || actual.equalsIgnoreCase("Log Time")
						|| actual.equalsIgnoreCase("Battery Percent") || actual.equalsIgnoreCase("Job Status")
						|| actual.equalsIgnoreCase("Registered Date")
						|| actual.equalsIgnoreCase("Wi-Fi Data Usage (MB)") || actual.equalsIgnoreCase("Contact Name")
						|| actual.equalsIgnoreCase("SureFox Lite") || actual.equalsIgnoreCase("Job Schedule Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}

			if (i == 3) {
				if (actual.equalsIgnoreCase("OS Type") || actual.equalsIgnoreCase("Log Message")
						|| actual.equalsIgnoreCase("Available Storage") || actual.equalsIgnoreCase("Job Name")
						|| actual.equalsIgnoreCase("Device Status") || actual.equalsIgnoreCase("Call Type")
						|| actual.equalsIgnoreCase("Job Deployed Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}

			if (i == 4) {
				if (actual.equalsIgnoreCase("Registered Date") || actual.equalsIgnoreCase("Available Physical Memory")
						|| actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Time Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}
			if (i == 5) {
				if (actual.equalsIgnoreCase("Online Status") || actual.equalsIgnoreCase("User ID")
						|| actual.equalsIgnoreCase("Duration Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					;
					Reporter.log("");
				}
			}
			if (i == 6) {
				if (actual.equalsIgnoreCase("Last Connected") || actual.equalsIgnoreCase("SureFox")
						|| actual.equalsIgnoreCase("SureMDM Nix")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 7) {
				if (actual.equalsIgnoreCase("Device Time") || actual.equalsIgnoreCase("SureFox Lite")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 8) {
				if (actual.equalsIgnoreCase("MAC Address")) {
					Reporter.log("PASS >> Column Name is correct");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}

			}

		}

		// **********verifying third column data data***********************

		Reporter.log("**********verifying third column data data***********************");
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		String colName = ls.get(2).getText();
		if (colName.equalsIgnoreCase("Wi-Fi Data Usage (MB)")) {
			Reporter.log(
					"=================Verifying Call Log Tracking 3rd Column(Wi-Fi Data Usage (MB)) data============");
			Reporter.log("");
		}

		if (colName.equalsIgnoreCase("OS Version")) {
			Reporter.log("        =====Verifying Asset Tracking 3rd Column(OS Version) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Log Time")) {
			Reporter.log("        =====Verifying System Log 3rd Column(Log Time) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Contact Name")) {
			Reporter.log("        =====Verifying Call Log Tracking 3rd Column(Contact Name) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Registered Date")) {
			Reporter.log("        =====Verifying Device Connected 3rd Column(Registered Date) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Job Status")) {
			Reporter.log("        =====Verifying Device Connected 3rd Column(Job Status) Data ======");
		}
		Reporter.log("");

		List<WebElement> ThirdColumn = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr/td[3]"));
		int RowSize = ThirdColumn.size();
		System.out.println(RowSize);
		if (RowSize == 0) {
			Reporter.log("No Data Available");
		} else {
			for (int i = 0; i < RowSize; i++) {
				String Actualvalue1 = ThirdColumn.get(i).getText();
				Reporter.log(Actualvalue1);

				boolean isPresent = false;

				if (Actualvalue1.equalsIgnoreCase("Android") || Actualvalue1.equalsIgnoreCase("Android Wear")
						|| Actualvalue1.equalsIgnoreCase("PocketPC") || Actualvalue1.equalsIgnoreCase("Windows CE")
						|| Actualvalue1.equalsIgnoreCase("Windows") || Actualvalue1.equalsIgnoreCase("Offline")
						|| Actualvalue1.equalsIgnoreCase("NA") || Actualvalue1.equalsIgnoreCase("Charging")
						|| Actualvalue1.equalsIgnoreCase("Normal")) {
					s_assert.assertTrue(isPresent, "FAIL >> Wrong data found in the OS version Column");
					s_assert.assertAll();
				} else {
					Reporter.log("PASS >> Expected data found", true);
					Reporter.log("");
					sleep(3);

				}

			}

		}

		// PassScreenshot.captureScreenshot(Initialization.driver,
		// "SystemLogReportScreenshot");

		// ****going back to first tab****
		/*
		 * for(String handle : Initialization.driver.getWindowHandles()) { if
		 * (!handle.equals(originalHandle)) {
		 * Initialization.driver.switchTo().window(handle);
		 * Initialization.driver.close(); } }
		 * 
		 * Initialization.driver.switchTo().window(originalHandle);
		 */

		// **********verifying fourth column data data***********************

		// public void VerifyTheFourthColumnData(){

		Reporter.log("**********verifying fourth column data data***********************");
		List<WebElement> ls1 = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		String colName1 = ls1.get(3).getText();
		if (colName1.equalsIgnoreCase("OS Type")) {
			Reporter.log("=================Verifying Asset Tracking 4th Column(OS Type) data============");
			Reporter.log("");
		}

		if (colName1.equalsIgnoreCase("Log Message")) {
			Reporter.log("        =====Verifying System Log 4th Column(Log Message) Data ======");
		}
		Reporter.log("");

		if (colName1.equalsIgnoreCase("Device Status")) {
			Reporter.log("        =====Verifying Device Connected 4th Column(Device Status) Data ======");
		}
		Reporter.log("");

		if (colName1.equalsIgnoreCase("Call Type")) {
			Reporter.log("        =====Verifying Call Log Tracking 4th Column(Call Type) Data ======");
		}
		Reporter.log("");

		List<WebElement> FourthCol = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr/td[4]"));
		int RowSize1 = FourthCol.size();
		System.out.println(RowSize1);
		sleep(2);
		if (RowSize1 == 0) {
			Reporter.log("No Data Available");
		} else {
			for (int i = 0; i < RowSize; i++) {
				String Actualvalue1 = FourthCol.get(i).getText();
				Reporter.log(Actualvalue1);

				boolean isPresent = false;

				if (Actualvalue1.equalsIgnoreCase("ICE CREAM SANDWICH") || Actualvalue1.equalsIgnoreCase("Offline")
						|| Actualvalue1.equalsIgnoreCase("NOUGAT") || Actualvalue1.equalsIgnoreCase("LOLLIPOP")
						|| Actualvalue1.equalsIgnoreCase("MARSHMALLOW") || Actualvalue1.equalsIgnoreCase("Online")
						|| Actualvalue1.equalsIgnoreCase("NA") || Actualvalue1.equalsIgnoreCase("Charging")
						|| Actualvalue1.equalsIgnoreCase("42GearsUnifi")) {

					s_assert.assertTrue(isPresent, "FAIL >> Wrong data found in the Column");
					s_assert.assertAll();
				} else {
					Reporter.log("PASS >> Expected data found", true);
					Reporter.log("");

				}
			}
		}

		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);
		sleep(3);
	}

	public void CallLogReports() throws InterruptedException, NumberFormatException, EncryptedDocumentException,
			InvalidFormatException, IOException {
		CallLogTrackingOption.click();
		sleep(4);
		if (SelectDeviceOrGroupText.isDisplayed()) {
			Reporter.log("PASS>> Clicked on Call Log Tracking logs and window opened successfully", true);
		} else {
			Reporter.log("FAIL>> Call Log Tracking logs window did not open", true);
		}
		sleep(3);
		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			warningmessageOnGenerateReportWhenNoDeviceOrGroupisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail: Receiving of notification failed");
		Reporter.log("PASS>> Alert Message : 'Please select a device or group.' is displayed", true);
		sleep(3);
		SelectDeviceOrGroupbuton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='adddevicebtn']")));
		String Actualvalue = isAddDeviceWindowdisplayed.getText();
		String Expectedvalue = "Add Device";
		String PassStatement = "PASS >>" + Expectedvalue + " windows  displayed successfully";
		String FailStatement = "FAIL >>" + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		waitForPageToLoad();
		AddButtoninWindow.click();

		int count = 4;

		Select a = new Select(Initialization.driver.findElement(By.id("CallType")));
		sleep(4);
		a.selectByVisibleText("Incoming");
		sleep(4);

	}

	public void ClickOnJobsDeployedReport() throws InterruptedException {
		JobsDeployedOption.click();
		sleep(4);
	}

	public void JobsDeployedReport() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, InterruptedException {
		OnDemandReportOption.click();
		sleep(2);
		JobsDeployedOption.click();
		sleep(4);
		if (SelectDeviceOrGroupTextInJobDeployed.isDisplayed() && JobsDepoyedWindowText.isDisplayed()) {
			Reporter.log("PASS>> Clicked on Job Deployed and window opened successfully", true);
		} else {
			Reporter.log("FAIL>> JobDeployed window did not open", true);
		}
		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 104, 1));
		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 105, 1).equals("Today")) {
				datE.click();
				sleep(5);
				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 105, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 105, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);

			}
			CountOfUnreadReports();

			WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath(".//*[@id='reportDetails']/div[2]/span[1][text()='Select Group']")));
			sleep(5);

			// String Expectedvalue = "Home";
			// String PassStatement = "PASS >> "+Expectedvalue+" is displayed in
			// the Select Group Text box";
			// String FailStatement = "FAIL >> "+Expectedvalue+ " is not
			// displayed in the Select Group Text box" ;
			// ALib.AssertEqualsMethod(Expectedvalue, Actualvalue,
			// PassStatement, FailStatement);
			sleep(4);
			selectgroupbutton.click();
			WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 10);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(".//*[@id='groupListModal']/div/div/div[3]/button")));
			String Actualvalue1 = GroupListWindow.getText();
			String Expectedvalue1 = "Group List";
			String PassStatement1 = "PASS >> " + Expectedvalue1 + " windows  displayed successfully";
			String FailStatement1 = "FAIL >> " + Expectedvalue1 + " window is not displayed";
			ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
			waitForPageToLoad();
			SelectaGroupOnGroupListWindow.click();// selecting the 1st group
			// under home
			waitForPageToLoad();
			sleep(4);

			OkbuttononOnWindow.click();
			sleep(4);

			String Actualvalue = selectGroupTextBox.getAttribute("value");
			String Expectedvalue = Config.groupName;
			String PassStatement = "PASS >> Group is selected and " + Expectedvalue
					+ " group is  displayed in the Select Group Text box";
			String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select Group Text box";
			ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

			sleep(5);
			viewReportsOption.click();
			sleep(5);
		}
	}

	public void ClickOnInstallJobReport() throws InterruptedException {
		InstalledJobReportOption.click();
		sleep(3);
	}

	public void ClickOnDeviceHealthReport() throws InterruptedException {
		DeviceHealthReportOption.click();
		sleep(3);
	}

	public void VerifyInstallJobHeaderText() throws InterruptedException {

		String Actualvalue = InstalledJobReportWindowHeaderText.getText();
		String Expectedvalue = "Installed Job Report";
		String PassStatement = "PASS >> " + Expectedvalue + " is  displayed on the install job window";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed on the install job window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyInstallJobDateText() throws InterruptedException {

		String Actualvalue1 = WindowDateText.getText();
		String Expectedvalue1 = "Date";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed on the install job window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed on the install job window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(3);
	}

	public void VerifyInstallJobSelectGroupText() throws InterruptedException {

		String Actualvalue2 = InstalledJobReportWindowSelectGroupText.getText();
		String Expectedvalue2 = "Select Job";
		String PassStatement2 = "PASS >> " + Expectedvalue2 + " is  displayed on the install job window";
		String FailStatement2 = "FAIL >> " + Expectedvalue2 + " is not displayed on the install job window";
		ALib.AssertEqualsMethod(Expectedvalue2, Actualvalue2, PassStatement2, FailStatement2);
		sleep(3);
	}

	public void VerifyInstallJobApplicationNameText() throws InterruptedException {

		String Actualvalue3 = InstalledJobReportWindowApplicationNameText.getText();
		String Expectedvalue3 = "Application Name";
		String PassStatement3 = "PASS >> " + Expectedvalue3 + " is  displayed on the install job window";
		String FailStatement3 = "FAIL >> " + Expectedvalue3 + " is not displayed on the install job window";
		ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void VerifyInstalledJobReportGenerateWithoutProvidingApplicationName() throws InterruptedException {

		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			InstallJobReportSelectJobwarningmessageOnGenerateReportWhenNoApplicationNameIsSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Please provide application name.' is displayed", true);
		sleep(3);
	}

	public void VerifyInstalledJobReportGenerateReportWithApplicationNameWithlessCharacters() {

	}

	public void VerifyInstalledJobReportGenerateReporrtWithoutSelectingAGroup() {

	}

	public void VerifyInstalledJobReportSelectJobWindow() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("okbtn")));
		String Actualvalue = InstalledJobReportSelectJobWindowHeaderText.getText();
		String Expectedvalue = "Select Jobs To Add";
		String PassStatement = "PASS >> " + Expectedvalue + " window  displayed successfully";
		String FailStatement = "FAIL >> " + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(3);
		String OkButtonOnSelectGroupWindow = "OK";
		if (InstalledJobReportWindowOkButton.getText().equalsIgnoreCase("OkButtonOnSelectGroupWindow"))
			;
		Reporter.log("PASS>> " + OkButtonOnSelectGroupWindow + " is displayed on the install job select to add window",
				true);

	}

	public void VerifyInstalledJobReportClickOKWithoutAddingJobs() throws InterruptedException {
		InstalledJobReportSelectJobButton.click();
		sleep(3);
		InstalledJobReportWindowOkButton.click();
		boolean isdisplayed = true;

		try {
			InstallJobReportSelectJobWarningMessageWhenNoJobisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Please select a job.' is displayed", true);
		sleep(4);
	}

	public void VerifyInstalledJobReportsSelectJobsToAdd() throws InterruptedException {
		// String WarningWhenNoJobstoAdd= "No jobs available.";
		// String JobName1 = "Lock Job";
		// String JobName2 = "File Transfer";

		InstalledJobReportSearchJobName.sendKeys(Config.job);
		sleep(7);
		InstalledJobReportChooseLockJobToAdd.click();
		InstalledJobReportWindowOkButton.click();
		sleep(3);
		String Actualvalue = InstallJobReportSelectJobTextBox.getAttribute("value");
		String Expectedvalue = Config.job;
		String PassStatement = "PASS >> Job is selected and " + Expectedvalue
				+ " is  displayed in the Select Job Text box";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select Job Text box";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

	}

	public void VerifyInstallJobReportsProvidingApplicationName() throws InterruptedException {
		InstalledJobReportApplicationNameTextBox.sendKeys("gps");
		sleep(2);

	}

	public void VerifyInstalledJobReportGenerateDateWise()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 113, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 114, 1).equals("Today")) {
				datE.click();
				sleep(5);

				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 114, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 114, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);

			}
			VerifyInstalledJobReportClickOKWithoutAddingJobs();
			VerifyInstalledJobReportsSelectJobsToAdd();
			VerifyInstallJobReportsProvidingApplicationName();
			Reporter.log(ELib.getDatafromExcel("Sheet1", i + 114, 1));
			CountOfUnreadReports();
			sleep(4);

		}
		viewReportsOption.click();

		sleep(4);
		boolean isDeletebuttonVisible;
		try {
			deleteReportButton.isDisplayed();
			isDeletebuttonVisible = true;
		} catch (Throwable T) {
			isDeletebuttonVisible = false;
		}
		String PassStatement1 = "PASS >> Delete Button is visible;view Reports page displayed";
		String FailStatement1 = "FAIL >> Delete button is missing or View Reports page did not load successfully";
		ALib.AssertTrueMethod(isDeletebuttonVisible, PassStatement1, FailStatement1);
		sleep(3);
		OnDemandReportOption.click();
		sleep(3);
	}

	public void VerifyDeviceHealthReportHeaderText() throws InterruptedException {

		String Actualvalue = WindowDateText.getText();
		String Expectedvalue = "Device Health Report";
		String PassStatement = "PASS >> " + Expectedvalue + " is  displayed on the device health report window";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed on the device health report window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyFrontWindowElementsOfDeviceHealthReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='reportDetails']/div"));

		for (int i = 0; i < ls.size(); i++) {
			String a = ls.get(i).getText();
			Reporter.log("Window has " + a + " ");
		}
	}

	public void VerifyAttributeValuesOfDeviceHealthReportWindow() {

		java.util.ArrayList<String> attributeValuesOfAllFieldsString = new java.util.ArrayList<String>();
		int attributeValuesOfAllFields = 1;

		switch (attributeValuesOfAllFields) {
		case 1:
			attributeValuesOfAllFieldsString.add(date1.getAttribute("value"));
		case 2:
			attributeValuesOfAllFieldsString.add(BatteryPerformance.getAttribute("value"));
		case 3:
			attributeValuesOfAllFieldsString.add(storage.getAttribute("value"));
		case 4:
			attributeValuesOfAllFieldsString.add(physicalMemory.getAttribute("value"));
			break;
		default:
			break;

		}
		if (attributeValuesOfAllFieldsString.isEmpty()) {
			Reporter.log("No Text Values exist");
		} else {
			for (String textvalues : attributeValuesOfAllFieldsString) {
				Reporter.log("attribute is: " + textvalues + " ");
			}

		}
	}

	public void VerifyingDeviceHealthReportSelectGroupWidndow() throws InterruptedException {

		selectgroupbutton.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 10);
		wait1.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(".//*[@id='groupListModal']/div/div/div[3]/button")));
		sleep(2);
		String Actualvalue1 = GroupListWindow.getText();
		String Expectedvalue1 = "Group List";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " windows  displayed successfully";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		waitForPageToLoad();
		SelectaGroupOnGroupListWindow.click();// selecting the 1st group under
		// home
		waitForPageToLoad();
		OkbuttononOnWindow.click();
		sleep(2);

		String Actualvalue = DeviceHealthReportSelectGroupTextBox.getAttribute("value");
		String Expectedvalue = Config.groupName;
		String PassStatement = "PASS >> Group is selected and " + Expectedvalue
				+ " is  displayed in the Select Group Text box";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select Group Text box";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);

	}

	public void VerifyDeviceHealthReportGenerateDateWise()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 126, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 127, 1).equals("Today")) {
				datE.click();
				sleep(5);

				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 127, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 127, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);
				VerifyingDeviceHealthReportSelectGroupWidndow();

			}

			CountOfUnreadReports();
			sleep(4);
		}
		viewReportsOption.click();

		sleep(4);
		boolean isDeletebuttonVisible;
		try {
			deleteReportButton.isDisplayed();
			isDeletebuttonVisible = true;
		} catch (Throwable T) {
			isDeletebuttonVisible = false;
		}
		String PassStatement1 = "PASS >> Delete Button is visible;view Reports page displayed";
		String FailStatement1 = "FAIL >> Delete button is missing or View Reports page did not load successfully";
		ALib.AssertTrueMethod(isDeletebuttonVisible, PassStatement1, FailStatement1);
	}

	public void ClickOnDeviceHistoryReport() throws InterruptedException {
		DeviceHistoryReportOption.click();
		sleep(3);
		String Actualvalue = DeviceHistoryWindowHeaderText.getText();
		String Expectedvalue = "Device History";
		String PassStatement = "PASS >> " + Expectedvalue + " header is displayed on the device history report window";
		String FailStatement = "FAIL >> " + Expectedvalue
				+ " is not displayed on the device Device History report window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyDateText() throws InterruptedException {

		String Actualvalue1 = WindowDateText.getText();
		String Expectedvalue1 = "Date & Time";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed on the Device History window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed on the Device History window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(3);
	}

	public void VerifyDeviceHistoryReportSelectDeviceText() throws InterruptedException {

		String Actualvalue1 = DeviceHealthSelectDeviceText.getText();
		String Expectedvalue1 = "Select Device";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed on the Device History window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed on the Device History window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(2);
	}

	public void VerifyAttributeValuesReportWindow() throws InterruptedException {
		String Actualvalue1 = date1.getAttribute("value");
		String Expectedvalue1 = Config.date;
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed as date on the  window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is NOT displayed on the window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(2);

	}

	public void VerifyDeviceHistoryReportGenerateWithoutSelectingADevice() throws InterruptedException {

		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			warningmessageOnGenerateReportWhenNoDeviceisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Please select a device.' is displayed", true);
		sleep(3);
	}

	public void ClickOnSelectDeviceButton() throws InterruptedException {
		SelectDeviceOrGroupbuton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='adddevicebtn']")));
		sleep(3);
	}

	public void VerifyDeviceHistoryReportSelectDevice() throws InterruptedException {

		SelectDeviceOrGroupbuton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='adddevicebtn']")));
		sleep(3);
		String Actualvalue = isAddDeviceWindowdisplayed.getText();
		String Expectedvalue = "Add Device";
		String PassStatement = "PASS >>" + Expectedvalue + " windows  displayed successfully";
		String FailStatement = "FAIL >>" + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		waitForPageToLoad();
		Reporter.log("**Verify Error message if not device is selected**");
		AddButtoninWindow.click();
		boolean isdisplayed = true;

		try {
			DeviceHistoryWarningMessageWhenNoDeviceisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed- Clicked on Add button");
		Reporter.log(
				"PASS >> Alert Message : 'Please select a device from the list.' is displayed- Not Clicked on Add button",
				true);
		sleep(3);
		VerifyingProvidingWrongDeviceNameSearchGeneric();

	}

	// common method
	public void VerifyingProvidingWrongDeviceNameSearchGeneric() throws InterruptedException {
		Reporter.log("**Verify when wrong device name is searched***");
		DeviceHisrotySearchADevice.sendKeys("wrongDeviceName");
		sleep(4);
		boolean isdisplayed = true;

		try {
			DeviceHistoryWarningMessageWhenWrongDeviceSearchNameisGiven.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'No matching result found.' is displayed", true);
		sleep(4);
		DeviceHisrotySearchADevice.clear();
		sleep(4);
		Reporter.log("**Verify when correct device name is searched***");
		DeviceHisrotySearchADevice.sendKeys(Config.Device);
		sleep(6);
		Reporter.log("***Selecting the searched device***");
		DeviceHistoryJobReportChooseADeviceToAdd.click();
		Reporter.log("***Clicking on OK button***");
		AddButtoninWindow.click();
		sleep(2);
		Reporter.log("***Verifying the attribute of the selected device****");
		String Actualvalue = DeviceHistorySelectDeviceTextBox.getAttribute("value");
		String Expectedvalue = Config.Device;
		String PassStatement = "PASS >> Device is selected and device " + Expectedvalue
				+ " is  displayed in the Select device Text box";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select device Text box";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);

	}

	public void VerifyDeviceHistoryGenerateReportDateWise()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 140, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 141, 1).equals("Today")) {
				datE.click();
				sleep(5);

				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 141, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 141, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);
				VerifyDeviceHistoryReportSelectDevice();

			}

			CountOfUnreadReports();

		}

		Reporter.log("--Navigating to ViewReportOption-----");
		viewReportsOption.click();

		sleep(4);
		boolean isDeletebuttonVisible;
		try {
			deleteReportButton.isDisplayed();
			isDeletebuttonVisible = true;
		} catch (Throwable T) {
			isDeletebuttonVisible = false;
		}
		String PassStatement1 = "PASS >> Delete Button is visible;view Reports page displayed";
		String FailStatement1 = "FAIL >> Delete button is missing or View Reports page did not load successfully";
		ALib.AssertTrueMethod(isDeletebuttonVisible, PassStatement1, FailStatement1);
	}

	public void ClickOnDataUsageReport() throws InterruptedException {
		DataUsageReportOption.click();
		sleep(3);
	}

	public void VerifyFrontWindowElementsOfDatausageReportWindow() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='reportDetails']/div"));

		for (int i = 0; i < ls.size(); i++) {
			String a = ls.get(i).getText();
			Reporter.log("Window has " + a + " ");
		}
	}

	public void ClickOnDataUsageLegacyReport() throws InterruptedException {
		DataUsageLegacyReport.click();
		sleep(3);
	}

	public void VerifyFrontWindowElementsOfDeviceConnectedReportWindow() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='reportDetails']/div"));

		for (int i = 0; i < ls.size(); i++) {
			String a = ls.get(i).getText();
			Reporter.log("Window has " + a + " ");
		}
	}

	public void VerifyDeviceActivityReportSelectDeviceText() throws InterruptedException {

		String Actualvalue1 = DeviceHealthSelectDeviceText.getText();
		String Expectedvalue1 = "Select Device";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed on the Device Device Activity window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed on the Device Activity window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(2);
	}

	public void VerifyDataUsageGenerateReportDateWise() throws NumberFormatException, EncryptedDocumentException,
			InvalidFormatException, IOException, InterruptedException {

		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 153, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 154, 1).equals("Today")) {
				datE.click();
				sleep(5);

				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 154, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 154, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);

			}
			SelectGrouptButton.click();
			WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(".//*[@id='groupListModal']/div/div/div[3]/button")));
			String Actualvalue1 = GroupListWindow.getText();
			String Expectedvalue1 = "Group List";
			String PassStatement1 = "PASS >> " + Expectedvalue1 + " windows  displayed successfully";
			String FailStatement1 = "FAIL >> " + Expectedvalue1 + " window is not displayed";
			ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
			waitForPageToLoad();
			sleep(3);
			OkbuttononOnWindow.click();
			String Actualvalue = selectGroupTextBox.getAttribute("value");
			String Expectedvalue = "Home";
			String PassStatement = "PASS >> Group is selected and " + Expectedvalue
					+ " is  displayed in the Select Group Text box";
			String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select Group Text box";
			ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

			sleep(5);

			CountOfUnreadReports();

		}
		viewReportsOption.click();
		sleep(3);

	}

	public void VerifyDateText_DeviceActivity() throws InterruptedException {

		String Actualvalue1 = WindowDateText.getText();
		String Expectedvalue1 = "Date";
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed on the Device activity";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed on the Device Activitywindow";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(3);
	}

	public void ClickingOnDeviceActivityReport() throws InterruptedException {
		DeviceActivity.click();
		sleep(5);
		String ActualText = DeviceAcitivityText.getText();
		String ExpectedText = Config.DeviceActivitytext;
		String pass = "PASS >>  Device Activity text is correct: " + ActualText + " ";
		String fail = "FAIL >> Device Activity text is  not correct: " + ActualText + " ";
		ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);

		sleep(3);
	}

	public void ClickOnAssetTrackingReport() throws InterruptedException {
		AssetTracking.click();
		sleep(8);

	}

	public void VerifyAttributeValuesDeviceActivityReportWindow() throws InterruptedException {
		String Actualvalue1 = date1.getAttribute("value");
		LocalDate localDate = LocalDate.now();
		String date = localDate.toString();
		String[] str = date.split("-");
		String TodaysDate = str[2] + "/" + str[1] + "/" + str[0];
		String Expectedvalue1 = TodaysDate + " - " + TodaysDate;
		String PassStatement1 = "PASS >> " + Expectedvalue1 + " is  displayed as date on the  window";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is NOT displayed on the window";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(4);

	}

	public void VerifyDeviceActivityReportGenerateWithoutSelectingADevice() throws InterruptedException {

		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			warningmessageOnGenerateReportWhenNoDeviceisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Please select a device.' is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='addbtn']/i")
	private WebElement SelectDeviceDeviceActivityReport;

	public void ClickOnSelectDevice() throws InterruptedException {
		SelectDeviceDeviceActivityReport.click();
		sleep(10);
	}

	public void VerifyDeviceActivityReportSelectDevice() throws InterruptedException {

		SelectDeviceOrGroupbuton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='adddevicebtn']")));
		sleep(3);
		String Actualvalue = isAddDeviceWindowdisplayed.getText();
		String Expectedvalue = "Add Device";
		String PassStatement = "PASS >>" + Expectedvalue + " windows  displayed successfully";
		String FailStatement = "FAIL >>" + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		waitForPageToLoad();
		Reporter.log("**Verify Error message if not device is selected**");
		AddButtoninWindow.click();
		boolean isdisplayed = true;

		try {
			DeviceWarningMessageWhenNoDeviceisSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed- Clicked on Reply button");
		Reporter.log(
				"PASS >> Alert Message : 'Please select a device from the list.' is displayed- Not Clicked on Reply button",
				true);
		sleep(3);
		VerifyingProvidingWrongDeviceNameSearchGenericDeviceActivity();

	}

	public void VerifyingProvidingWrongDeviceNameSearchGenericDeviceActivity() throws InterruptedException {
		Reporter.log("**Verify when wrong device name is searched***");
		deviceActivitySearchBox.sendKeys("wrongDeviceName");
		sleep(3);
		boolean isdisplayed = true;

		try {

			WarningMessageWhenWrongDeviceSearchNameisGiven.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'No matching result found.' is displayed", true);
		sleep(3);
		deviceActivitySearchBox.clear();
		sleep(3);
		Reporter.log("**Verify when correct device name is searched***");
		deviceActivitySearchBox.sendKeys(Config.DeviceName);
		sleep(3);
		Reporter.log("***Selecting the searched device***");
		DeviceActivityReportChooseADeviceToAdd.click();

		Reporter.log("***Clicking on OK button***");
		AddButtoninWindow.click();
		sleep(2);
		Reporter.log("***Verifying the attribute of the selected device****");
		String Actualvalue = deviceActivitySearchBox.getAttribute("value");
		String Expectedvalue = Config.DeviceName;
		String PassStatement = "PASS >> Device is selected and device " + Expectedvalue
				+ " is  displayed in the Select device Text box";
		String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select device Text box";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='dGrid']/tbody/tr/td[1]")
	private WebElement SelectSearchedDevice;

	public void SelectSearchedDeviceInReport() throws InterruptedException {
		SelectSearchedDevice.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupList']/ul/li[text()='" + Config.GroupName_Reports + "']")
	private WebElement SelectSearchedGroup;

	public void SelectSearchedGroupInReport(String grp) throws InterruptedException {
		// SelectSearchedGroup.click();
		Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='" + grp + "']")).click();
		sleep(2);
	}

	public void SearchGroup() throws InterruptedException {
		SearchGroup.sendKeys(Config.SubGroupName);
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='CompJobDataGrid']/tbody/tr/td[2]/p")
	private WebElement SelectSearchedJob;

	public void SelectSearchedJobInReport() throws InterruptedException {
		SelectSearchedJob.click();
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='applicationname']")
	private WebElement ApplicationName;

	public void EnterApplicationNameInstalledJobReport(String AppName) {
		ApplicationName.clear();
		ApplicationName.sendKeys(AppName);
	}

	public void VerifyDeviceActivityGenerateReportDateWise()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		generateReportButton.click();
		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 166, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 167, 1).equals("Today")) {
				datE.click();
				sleep(5);
				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 167, 1));
				WebElement wb = Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 167, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb).click().perform();
				sleep(4);
				generateReportButton.click();
				captureReportIsInQueueMessage();
			}
		}

	}

	@FindBy(xpath = "//span[text()='Request added to queue. To view status, Please click on View Reports.']")
	private WebElement RequestAddedTOQueue;

	public void captureReportIsInQueueMessage() throws InterruptedException {
		boolean isdisplayed = true;
		try {
			RequestAddedTOQueue.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed,
				"Fail: Request added to queue to view report please click on view report is not displayed");
		Reporter.log(
				"PASS>> Alert Message : 'Request added to queue to view report please click on view report' is displayed",
				true);
		sleep(6);

	}

	public void VerifyDeviceConnectedGenerateReportDateWise() throws NumberFormatException, EncryptedDocumentException,
			InvalidFormatException, IOException, InterruptedException {

		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 166, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 167, 1).equals("Today")) {
				datE.click();
				sleep(5);

				Reporter.log(ELib.getDatafromExcel("Sheet1", i + 167, 1));
				List<WebElement> wb = Initialization.driver
						.findElements(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet1", i + 167, 1) + "']"));
				Actions act = new Actions(Initialization.driver);
				act.moveToElement(wb.get(i)).click().perform();
				sleep(5);

			}
			SelectGrouptButton.click();
			WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(".//*[@id='groupListModal']/div/div/div[3]/button")));
			String Actualvalue1 = GroupListWindow.getText();
			String Expectedvalue1 = "Group List";
			String PassStatement1 = "PASS >> " + Expectedvalue1 + " windows  displayed successfully";
			String FailStatement1 = "FAIL >> " + Expectedvalue1 + " window is not displayed";
			ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
			waitForPageToLoad();
			sleep(3);
			OkbuttononOnWindow.click();
			String Actualvalue = selectGroupTextBox.getAttribute("value");
			String Expectedvalue = "Home";
			String PassStatement = "PASS >> Group is selected and " + Expectedvalue
					+ " is  displayed in the Select Group Text box";
			String FailStatement = "FAIL >> " + Expectedvalue + " is not displayed in the Select Group Text box";
			ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

			sleep(5);

			CountOfUnreadReports();

		}
		viewReportsOption.click();
		sleep(3);

	}

	public void ClickOnDeviceConnectReport() throws InterruptedException {
		DeviceConnected.click();
		sleep(3);
	}

	public void VerifyClickingOnAppVersionReport() throws InterruptedException {
		AppVersionOption.click();
		sleep(3);

		if (AppVersionWindowHeaderText.isDisplayed()) {
			Reporter.log("PASS >> Clicked on App version option and Window launched successfully", true);
		} else {
			Reporter.log("FAIL >> Unable to launch window or Header is wrong", true);
		}
	}

	public void VerifyFrontWindowElementsOfAppVersionReportWindow() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='reportDetails']/div"));

		for (int i = 0; i < ls.size(); i++) {
			String a = ls.get(i).getText();
			Reporter.log("Window has: " + a + " ");
			sleep(1);
		}
	}

	public void VerifyDefaultApplicationTypeofAppVersionReport() {

		String Actualvalue = AppversionApplicationType.getAttribute("value");
		String Expectedvalue = "Downloaded";
		String PassStatement = "PASS >>App type is " + Expectedvalue + " apps ";
		String FailStatement = "FAIL >> " + Expectedvalue + " ";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
	}

	public void VerifyAppVersionReportGenerateReportWarningMessageWhenAPPNameIsLessThan3Chars()
			throws InterruptedException {
		AppVersionReportApplicationNameTextBox.sendKeys("Su");
		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			AppVersionWarningmessageOnGenerateReportWhenApplicationNameisLessThan3Chars.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Application name cannot be less than 3 characters.' is displayed", true);
		sleep(4);
		AppVersionReportApplicationNameTextBox.clear();
	}

	public void VerifyAppVersionReportGenerateWithoutProvidingApplicationName() throws InterruptedException {

		generateReportButton.click();
		boolean isdisplayed = true;

		try {
			AppVersionWarningmessageOnGenerateReportWhenNoApplicationNameIsSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Please provide application name.' is displayed", true);
		sleep(3);
	}

	public void VerifyAppVersionReportGenerateWithApplicationName(String AppName) throws InterruptedException {
		AppVersionReportApplicationNameTextBox.clear();
		AppVersionReportApplicationNameTextBox.sendKeys(AppName);
		sleep(3);
	}

	public void ClickOnViewReportsOption() throws InterruptedException {
		viewReportsOption.click();
		sleep(5);

	}

	public void VerifyIfAllTheReportsOptionsAvailable() {

		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath(".//*[@id='reportsGrid']/tbody/tr/td/div/span[1]"));

		for (int i = 0; i < 11; i++) {
			String a = ls.get(i).getText();

		}

	}

	public void VerifyAllReportNameAvailibitySearch()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {

		Reporter.log("==============Verifying Search Report Name====================");
		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 184, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 185, 1).equals("Sample")) {

				Reporter.log("Search Report Name using: " + (ELib.getDatafromExcel("Sheet1", i + 185, 1)));
				reportMainSearchButton.sendKeys(ELib.getDatafromExcel("Sheet1", i + 185, 1));
				sleep(3);
				reportMainSearchSelectReport.click();
				sleep(3);

				if (i == 0) {
					waitForidPresent("ShowAllLogs");

					if (ShowAllLogsTextBox.isEnabled()) {

						Reporter.log(
								"PASS >> Search Passed - Clicked on System logs and System Log window opened with show log option enabled",
								true);
						Reporter.log("===================================");
					} else

					{
						Reporter.log(
								"FAIL >> Search Fail - Either System Log window did not open or 'Show All Logs' is disabled",
								true);
						Reporter.log("===================================");
						sleep(3);
					}
				}

				if (i == 1) {
					if (SelectGroupText.isDisplayed()) {
						Reporter.log(
								"PASS >>  Search Passed - Clicked on Assert Tracking logs and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log("FAIL >> Either Assert Tracking logs window did not open", true);
						Reporter.log("===================================");
						sleep(3);
					}
				}

				if (i == 2) {
					if (CallLogTrackingOption.isDisplayed()) {
						Reporter.log(
								"PASS >>  Search Passed - Clicked on Call Log Tracking report and window opened successfully",
								true);
					} else {
						Reporter.log("FAIL >> Call Log Tracking logs window did not open", true);
						Reporter.log("===================================");
						sleep(3);
					}
				}

				if (i == 3) {
					if (SelectDeviceOrGroupTextInJobDeployed.isDisplayed() && JobsDepoyedWindowText.isDisplayed()) {
						Reporter.log("PASS >> Search passed- Clicked on Job Deployed and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log("FAIL>> Search Failed or Job Deployed window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}
				if (i == 4) {
					if (InstalledJobReportOption.isDisplayed()) {
						Reporter.log("PASS >> Search passed- Clicked on Installed job and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log("FAIL>> Search Failed or Installed job window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}
				if (i == 5) {
					if (DeviceHealthReportOption.isDisplayed()) {
						Reporter.log(
								"PASS >> Search passed- Clicked on device health report and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log(
								"FAIL>> Search Failed or evice health report window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}

				if (i == 6) {
					if (DeviceHistoryReportOption.isDisplayed()) {
						Reporter.log(
								"PASS >> Search passed- Clicked on device history report and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log(
								"FAIL>> Search Failed or device history report window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}

				if (i == 7) {
					if (DataUsageHeaderText.isDisplayed()) {
						Reporter.log(
								"PASS >> Search passed- Clicked on data usage report and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log(
								"FAIL>> Search Failed or data usage report window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}
				if (i == 8) {
					if (DeviceConnected.isDisplayed()) {
						Reporter.log(
								"PASS >> Search passed- Clicked on device connected report and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log(
								"FAIL>> Search Failed or device connected report window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}
				if (i == 9) {
					if (AppVersionWindowHeaderText.isDisplayed()) {
						Reporter.log(
								"PASS >> Search passed- Clicked on app version report and window opened successfully",
								true);
						Reporter.log("===================================");
					} else {
						Reporter.log("FAIL>> Search Failed or app version window did not open or elements missing",
								true);
						Reporter.log("===================================");
					}
				}

				reportMainSearchButton.clear();
				sleep(4);

			}

		}
	}

	public void searchDifferentReportTypes()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {

		OnDemandReportOption.click();
		sleep(3);
		viewReportsOption.click();
		Reporter.log("==============Verifying Search Different Report====================");
		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 196, 1));

		for (int i = 0; i < count1; i++) {
			if (!ELib.getDatafromExcel("Sheet1", i + 197, 1).equals("Sample")) {

				Reporter.log("Search Report: " + (ELib.getDatafromExcel("Sheet1", i + 197, 1)));
				ReportSearchButtonInViewReport.sendKeys(ELib.getDatafromExcel("Sheet1", i + 197, 1));
				sleep(2);

				// ****************search system log******8
				if (i == 0) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						SystemLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search system log report");
					Reporter.log("PASS >> 'System Log' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************search asset tracking report***************
				if (i == 1) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						AssettrackingLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search asset tracking report");
					Reporter.log("PASS >> 'Asset Tracking log' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************call log tracking report***************
				if (i == 2) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						CallLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search call log tracking report");
					Reporter.log("PASS >> 'Call Log Tracking Report' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************jobs deployed report***************
				if (i == 3) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						JobsDeployedLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search jobs deployed report");
					Reporter.log("PASS >> 'Jobs Deployed Report' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************Installed job report***************
				if (i == 4) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						InstalledJobLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to installed job deployed report");
					Reporter.log("PASS >> 'Installed Job Report' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************Device health report***************
				if (i == 5) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						DeviceHealthReportLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search device health report");
					Reporter.log("PASS >> 'Device Health Report' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************Device history Log search***************
				if (i == 6) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						DeviceHistoryLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search device history Log report");
					Reporter.log("PASS >> 'Device hisptry Log Report' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************App Version Log Search***************
				if (i == 7) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						AppVersionLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search device app version Log report");
					Reporter.log("PASS >> 'App Version Log' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************Device Connected Log search***************
				if (i == 8) {
					Reporter.log(SearchResultForAllLogsInViewReports.getText());
					boolean isdisplayed = true;

					try {
						DeviceConnectedLogSearch.isDisplayed();

					} catch (Exception e) {
						isdisplayed = false;

					}
					Assert.assertTrue(isdisplayed, "FAIL >> : Failed to search device connected Log report");
					Reporter.log("PASS >> 'Device Connected Log' searched successfully", true);
					sleep(3);
					ReportSearchButtonInViewReport.clear();
					sleep(3);
				}

				// ****************Data Usage Log search***************
				/*
				 * if(i==9) { Reporter.log(SearchResultForAllLogsInViewReports.getText()); /*
				 * String Actualvalue = SearchReportResultFirstRowReport.getText(); String
				 * Expectedvalue ="Data Usage";
				 * 
				 * //String PassStatement =
				 * "PASS>> "+Expectedvalue+" Report is searched successfully"; // String
				 * FailStatement =
				 * "FAIL >> Failed to search 'data usage report' with the keyword 'data'" ;
				 * //ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement,
				 * FailStatement);
				 * 
				 * s_assert.assertEquals(Actualvalue, Expectedvalue,
				 * "Failed to search 'Data Usage' report using keyword 'data'");
				 * s_assert.assertAll();
				 * 
				 * 
				 * boolean isdisplayed=true;
				 * 
				 * try{ SearchReportResultFirstRowDataUsage.isDisplayed();
				 * 
				 * }catch(Exception e) { isdisplayed=false;
				 * 
				 * } Assert.assertTrue(
				 * isdisplayed,"FAIL >> : Failed to search data usage Log report using keyword 'data'"
				 * );
				 * 
				 * Reporter.log("PASS >> 'Data Usage Log' searched successfully" ,true );
				 * 
				 * sleep(3); ReportSearchButtonInViewReport.clear(); sleep(3); }
				 * Reporter.log(""); sleep(2); ReportSearchButtonInViewReport.clear();
				 */
			}

		}
	}

	public void ViewReportDataUsage() throws InterruptedException {

		viewReportsOption.click();
		sleep(3);

		viewReport.click();

		sleep(3);
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1)); // it's related
		// items are on
		// top

		String Actualvalue = Initialization.driver.getTitle();
		String Expectedvalue = "SureMDM Report Viewer";
		String PassStatement = "PASS>> " + Expectedvalue
				+ " is displayed as title of the view report window-New Tab opened successfully";
		String FailStatement = "FAIL >>Wrong title or unable to view reports";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(5);
		Reporter.log("");
		Reporter.log("===Verifying Header of the viewing report and its columns=====");
		Reporter.log("");
		Reporter.log("The Report Name is--" + ReportNameinViewReportHeader.getText());
		Reporter.log("--------------------------------");
		/*
		 * String ColumnsContents =Initialization.driver.findElement(By.xpath(
		 * ".//*[@id='reportList_tableCont']/section[1]/div[2]")).getText();
		 * System.out.println("Columns Shown are=="+ColumnsContents);
		 */
		sleep(3);

		List<WebElement> ColumnHeaderCount = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int countOfColumns = ColumnHeaderCount.size();
		Reporter.log("Total Column Count is : " + countOfColumns);
		if (countOfColumns == 4 || countOfColumns == 7 || countOfColumns == 33 || countOfColumns == 5
				|| countOfColumns == 6 || countOfColumns == 3 || countOfColumns == 8) {
			Reporter.log("PASS >> Total Column Counts is correctly Displayed");
			Reporter.log("------------------------------");
		} else {
			Reporter.log("FAIL >> Total Column Counts is wrong");
			Reporter.log("------------------------------");
		}
		for (int i = 0; i < countOfColumns; i++) {
			String actual = ColumnHeaderCount.get(i).getText();
			Reporter.log("Column Name Found --" + actual);

			if (i == 0) {
				if (actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Name")
						|| actual.equalsIgnoreCase("Job Schedule Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
				}
			}
			if (i == 1) {
				if (actual.equalsIgnoreCase("Device Model") || actual.equalsIgnoreCase("Log Type")
						|| actual.equalsIgnoreCase("Last Connected") || actual.equalsIgnoreCase("Job Deployed Time")
						|| actual.equalsIgnoreCase("Mobile Data Usage (MB)") || actual.equalsIgnoreCase("Phone Number")
						|| actual.equalsIgnoreCase("SureFox") || actual.equalsIgnoreCase("Job Name")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}

			}
			if (i == 2) {
				if (actual.equalsIgnoreCase("OS Version") || actual.equalsIgnoreCase("Log Time")
						|| actual.equalsIgnoreCase("Battery Percent") || actual.equalsIgnoreCase("Job Status")
						|| actual.equalsIgnoreCase("Registered Date")
						|| actual.equalsIgnoreCase("Wi-Fi Data Usage (MB)") || actual.equalsIgnoreCase("Contact Name")
						|| actual.equalsIgnoreCase("SureFox Lite") || actual.equalsIgnoreCase("Job Schedule Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}

			if (i == 3) {
				if (actual.equalsIgnoreCase("OS Type") || actual.equalsIgnoreCase("Log Message")
						|| actual.equalsIgnoreCase("Available Storage") || actual.equalsIgnoreCase("Job Name")
						|| actual.equalsIgnoreCase("Device Status") || actual.equalsIgnoreCase("Call Type")
						|| actual.equalsIgnoreCase("Job Deployed Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}

			if (i == 4) {
				if (actual.equalsIgnoreCase("Registered Date") || actual.equalsIgnoreCase("Available Physical Memory")
						|| actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Time Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}
			if (i == 5) {
				if (actual.equalsIgnoreCase("Online Status") || actual.equalsIgnoreCase("User ID")
						|| actual.equalsIgnoreCase("Duration Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					;
					Reporter.log("");
				}
			}
			if (i == 6) {
				if (actual.equalsIgnoreCase("Last Connected") || actual.equalsIgnoreCase("SureFox")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 7) {
				if (actual.equalsIgnoreCase("Device Time") || actual.equalsIgnoreCase("SureFox Lite")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 8) {
				if (actual.equalsIgnoreCase("MAC Address")) {
					Reporter.log("PASS >> Column Name is correct");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}

			}

		}

		// **********verifying third column data data of data
		// usage***********************

		Reporter.log("**********verifying third column data data of data usage***********************");
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		String colName = ls.get(2).getText();
		if (colName.equalsIgnoreCase("Wi-Fi Data Usage (MB)")) {
			Reporter.log(
					"=================Verifying Call Log Tracking 3rd Column(Wi-Fi Data Usage (MB)) data============");
			Reporter.log("");
		}

		if (colName.equalsIgnoreCase("OS Version")) {
			Reporter.log("        =====Verifying Asset Tracking 3rd Column(OS Version) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Log Time")) {
			Reporter.log("        =====Verifying System Log 3rd Column(Log Time) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Contact Name")) {
			Reporter.log("        =====Verifying Call Log Tracking 3rd Column(Contact Name) Data ======");
		}
		Reporter.log("");

		if (colName.equalsIgnoreCase("Registered Date")) {
			Reporter.log("        =====Verifying Device Connected 3rd Column(Registered Date) Data ======");
		}
		Reporter.log("");

		List<WebElement> ThirdColumn = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr/td[3]"));
		int RowSize = ThirdColumn.size();
		System.out.println(RowSize);
		if (RowSize == 0) {
			Reporter.log("No Data Available");
		} else {
			for (int i = 0; i < RowSize; i++) {
				String Actualvalue1 = ThirdColumn.get(i).getText();
				Reporter.log(Actualvalue1);

				boolean isPresent = false;

				if (Actualvalue1.equalsIgnoreCase("Android") || Actualvalue1.equalsIgnoreCase("Android Wear")
						|| Actualvalue1.equalsIgnoreCase("PocketPC") || Actualvalue1.equalsIgnoreCase("Windows CE")
						|| Actualvalue1.equalsIgnoreCase("Windows") || Actualvalue1.equalsIgnoreCase("Offline")
						|| Actualvalue1.equalsIgnoreCase("NA") || Actualvalue1.equalsIgnoreCase("Charging")
						|| Actualvalue1.equalsIgnoreCase("Normal")) {
					s_assert.assertTrue(isPresent, "FAIL >> Wrong data found in the OS version Column");
					s_assert.assertAll();
				} else {
					Reporter.log("PASS >> Expected data found", true);
					Reporter.log("");
					sleep(3);

				}

			}

		}

		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);

	}

	public void ClickOnView() throws InterruptedException {
		viewReport.click();
		sleep(8);
	}

	CommonMethods_POM common = new CommonMethods_POM();

	public void VerifyDeviceActivityReportColumns() {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> First column is present";
				String fail = "FAIL>> First column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Time";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Activity";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	@FindBy(xpath = "//*[@id='reportList_tableCont']/section[1]/div[2]/div[1]/div[2]/input")
	private WebElement SearchText_SystemLogReports;

	// Validation of System Log Columns
	public void Search_SystemLogTextBox(String LogMessage) throws InterruptedException {
		SearchText_SystemLogReports.clear();
		sleep(2);
		SearchText_SystemLogReports.sendKeys(LogMessage);
		sleep(4);
	}

	public void VerifyOfSystemLogReportDeviceName(String DeviceName) throws InterruptedException {

		List<WebElement> DeviceName1 = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]"));
		if (DeviceName1.size() > 0) {
			for (int i = 0; i < DeviceName1.size(); i++) {
				String ActualValue = DeviceName1.get(i).getText();
				if (ActualValue.equals(DeviceName)) {
					Reporter.log("Pass >> DeviceName Is Diplayed Correctly", true);
				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> DeviceName Is Not Diplayed Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> DeviceName Column Is Not Diplayed Correctly");
		}
	}

	public void VerifyOfUserNameInSystemLogReport(String UserName) throws InterruptedException {
		List<WebElement> Username = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]"));
		if (Username.size() > 0) {
			for (int i = 0; i < Username.size(); i++) {
				String ActualValue = Username.get(i).getText();
				if (ActualValue.equals(UserName)) {

				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> UserName Is Not Diplayed Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> DeviceName Column Is Not Diplayed Correctly");
		}
	}

	public void VerifySystemLogReportColumns() {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Log Time";
				String pass = "PASS >> First column is present";
				String fail = "FAIL>> First column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Device Name";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "User Name";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Log Type";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Log Message";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyComplianceJobReportColumns() {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> First column is present";
				String fail = "FAIL>> First column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Compliance Policy";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Status";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Last Connected";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	@FindBy(xpath = ("//input[@class='form-control']"))
	private WebElement ClickOnSearchButtonInViewedReport;

	public void ClickOnSearchReportButtonInsidedViewedReport_SystemLog() throws InterruptedException {
		ClickOnSearchButtonInViewedReport.sendKeys(Config.Input_Val17);
		sleep(3);
		waitForXpathPresent("//p[contains(text(),'Device Started.')]");
		sleep(3);
		ClickOnSearchButtonInViewedReport.clear();
	}

	@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[3]"))
	private List<WebElement> SystemLogMessage;

	@FindBy(xpath = ("//p[contains(text(),'Device Started.')]"))
	private List<WebElement> Systemlog;

	public void VerificationOfMessagefield() {
		boolean value = false;
		String PassStatement = "PASS >> The report contains the details device which are having the message like Device Started.";
		String FailStatement = "Fail >> The report not contains the details device which are having the message like Device Started.";
		for (int i = 0; i < SystemLogMessage.size(); i++) {
			String ActualValue = Systemlog.get(i).getText();
			// System.out.println(ActualValue);
			value = ActualValue.equals("Device Started.");
			if (value == false) {
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void WindowHandle() throws InterruptedException {
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		sleep(10);
	}

	public void VerifyDeviceHealthReportColumns() {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> First column is present";
				String fail = "FAIL>> First column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Last Connected";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Battery Percent";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Available Storage";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Available Physical Memory";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void ViewReportAppVersion() throws InterruptedException {

		viewReport.click();

		sleep(3);
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1)); // it's related
		// items are on
		// top

		String Actualvalue = Initialization.driver.getTitle();
		String Expectedvalue = "SureMDM Report Viewer";
		String PassStatement = "PASS>> " + Expectedvalue
				+ " is displayed as title of the view report window-New Tab opened successfully";
		String FailStatement = "FAIL >>Wrong title or unable to view reports";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(5);
		Reporter.log("===Verifying Header of the viewing report and its columns=====", true);
		Reporter.log("The Report Name is--" + ReportNameinViewReportHeader.getText());
		Reporter.log("--------------------------------");
		sleep(3);

		List<WebElement> ColumnHeaderCount = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));

		int countOfColumns = ColumnHeaderCount.size();
		System.out.println(countOfColumns);
		Reporter.log("Total Column Count is : " + countOfColumns);
		if (countOfColumns == 2 || countOfColumns == 4 || countOfColumns == 7 || countOfColumns == 33
				|| countOfColumns == 5 || countOfColumns == 6 || countOfColumns == 3 || countOfColumns == 8) {
			Reporter.log("PASS >> Total Column Counts is correctly Displayed");
			Reporter.log("------------------------------");
		} else {
			Reporter.log("FAIL >> Total Column Counts is wrong");
			Reporter.log("------------------------------");
		}
		for (int i = 0; i < countOfColumns; i++) {
			String actual = ColumnHeaderCount.get(i).getText();
			System.out.println("Column Name Found --" + actual);
			Reporter.log("Column Name Found --" + actual);

			if (i == 0) {
				if (actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Name")
						|| actual.equalsIgnoreCase("Job Schedule Time")) {
					System.out.println("PASS >> Column Name is correct");
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
				}
			}
			if (i == 1) {
				if (actual.equalsIgnoreCase("ES File Explorer") || actual.equalsIgnoreCase("Device Model")
						|| actual.equalsIgnoreCase("Log Type") || actual.equalsIgnoreCase("Last Connected")
						|| actual.equalsIgnoreCase("Job Deployed Time")
						|| actual.equalsIgnoreCase("Mobile Data Usage (MB)") || actual.equalsIgnoreCase("Phone Number")
						|| actual.equalsIgnoreCase("SureFox") || actual.equalsIgnoreCase("Job Name")) {
					System.out.println("PASS >> Column Name is correct");
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					System.out.println("PASS >> Column Name is Wrong");
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 2) {
				if (actual.equalsIgnoreCase("OS Version") || actual.equalsIgnoreCase("Log Time")
						|| actual.equalsIgnoreCase("Battery Percent") || actual.equalsIgnoreCase("Job Status")
						|| actual.equalsIgnoreCase("Registered Date")
						|| actual.equalsIgnoreCase("Wi-Fi Data Usage (MB)") || actual.equalsIgnoreCase("Contact Name")
						|| actual.equalsIgnoreCase("SureFox Lite") || actual.equalsIgnoreCase("Job Schedule Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}

			if (i == 3) {
				if (actual.equalsIgnoreCase("OS Type") || actual.equalsIgnoreCase("Log Message")
						|| actual.equalsIgnoreCase("Available Storage") || actual.equalsIgnoreCase("Job Name")
						|| actual.equalsIgnoreCase("Device Status") || actual.equalsIgnoreCase("Call Type")
						|| actual.equalsIgnoreCase("Job Deployed Time")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}

			if (i == 4) {
				if (actual.equalsIgnoreCase("Registered Date") || actual.equalsIgnoreCase("Available Physical Memory")
						|| actual.equalsIgnoreCase("Device Name") || actual.equalsIgnoreCase("Time Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");

				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}
			}
			if (i == 5) {
				if (actual.equalsIgnoreCase("Online Status") || actual.equalsIgnoreCase("User ID")
						|| actual.equalsIgnoreCase("Duration Of Call")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					;
					Reporter.log("");
				}
			}
			if (i == 6) {
				if (actual.equalsIgnoreCase("Last Connected") || actual.equalsIgnoreCase("SureFox")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 7) {
				if (actual.equalsIgnoreCase("Device Time") || actual.equalsIgnoreCase("SureFox Lite")) {
					Reporter.log("PASS >> Column Name is correct");
					Reporter.log("");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
				}
			}
			if (i == 8) {
				if (actual.equalsIgnoreCase("MAC Address")) {
					Reporter.log("PASS >> Column Name is correct");
				} else {
					Reporter.log("FAIL >> Column Name is wrong");
					Reporter.log("");
					sleep(3);
				}

			}

		}
		PassScreenshot.captureScreenshot(Initialization.driver, "AppVersionScreenshot");
		sleep(4);
		Reporter.log("all cols and data shown - refer screenshot");
	}

	public void SearchDeviceNameInReports(String DeviceName) throws InterruptedException {
		SearchDeviceInReports.sendKeys(DeviceName);
		sleep(5);
	}

	public void SearchDeviceNameInAppVersionReports(String DeviceName) throws InterruptedException {
		SearchDeviceInAppVersionReport.sendKeys(DeviceName);
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[1]/p")
	private WebElement DeviceName1;

	public void verifyOfDeviceName(String device) {
		String ActualVersion = DeviceName1.getText();
		String ExpectedVersion = device;
		String pass = "PASS >> DeviceName in Report is correct: " + ActualVersion + " ";
		String fail = "FAIL >> DeviceName in Report is not correct: " + ActualVersion + "";
		ALib.AssertEqualsMethod(ExpectedVersion, ActualVersion, pass, fail);

	}

	@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[2]/p")
	private WebElement VersionOfTheApplication;

	public void CheckApplicationVersion(String version) {
		String ActualVersion = VersionOfTheApplication.getText();
		String ExpectedVersion = version;
		String pass = "PASS >> Version of the Downloaded Application is correct: " + ActualVersion + " ";
		String fail = "FAIL >> Version of the Download Application is NOT correct: " + ActualVersion + "";
		ALib.AssertEqualsMethod(ExpectedVersion, ActualVersion, pass, fail);

	}

	public void SwitchBackWindow() throws InterruptedException {

		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);
		sleep(3);
	}

	@FindBy(xpath = ("//button[text()='Request Report']"))
	private WebElement ClickRequestReport;

	public void ClickRequestReportButton() throws InterruptedException {
		ClickRequestReport.click();
		Reporter.log("Requested for custom report", true);
		sleep(2);
		waitForXpathPresent("//span[text()='Request added to queue. To view status, Please click on View Reports.']");
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='report_viewSect']/div/div[2]/div[1]/div[2]/input")
	private WebElement SearchReport;

	public void SelectReportDeviceActivity() throws InterruptedException {
		viewReportsOption.click();
		sleep(4);
		SearchReport.clear();
		SearchReport.sendKeys("Device Activity");
		sleep(3);
	}

	public void VerifyReportDeviceActivity() throws InterruptedException {
		SelectReportDeviceActivity();
		String originalHandle = Initialization.driver.getWindowHandle();
		List<WebElement> ReportList = Initialization.driver
				.findElements(By.xpath(".//*[@id='OfflineReportGrid']/tbody/tr"));
		int ReportCount = ReportList.size();
		for (int i = 0; i < ReportCount; i++) {
			if (i == 0) {
				ReportList.get(0).click();

				waitForXpathPresent("//*[@id=\"OfflineReportGrid\"]/tbody/tr[1]/td[5]/a[2]");
				sleep(4);
				View1.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndItsGroup();
				VerifyCoulmnsInDeviceActivityReport();
				sleep(3);

				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}

				Initialization.driver.switchTo().window(originalHandle);
			}

			if (i == 1) {
				sleep(2);
				ReportList.get(1).click();
				sleep(3);
				View2.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndItsGroup();
				VerifyCoulmnsInDeviceActivityReport();
				sleep(3);
				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}

				Initialization.driver.switchTo().window(originalHandle);
			}

			if (i == 2) {
				ReportList.get(2).click();
				sleep(3);
				View3.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndItsGroup();
				VerifyCoulmnsInDeviceActivityReport();
				sleep(3);
				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}
				Initialization.driver.switchTo().window(originalHandle);
			}

			if (i == 3) {
				ReportList.get(3).click();
				sleep(3);
				View4.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndItsGroup();
				VerifyCoulmnsInDeviceActivityReport();
				// VerifyDeviceActivityReportdatayesterday();
				sleep(3);
				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}

				Initialization.driver.switchTo().window(originalHandle);
			}

			if (i == 4) {
				sleep(5);
				ReportList.get(4).click();
				sleep(3);
				View5.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndItsGroup();
				VerifyCoulmnsInDeviceActivityReport();
				VerifyDeviceActivityReportdata_today();
				sleep(3);
				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}

				Initialization.driver.switchTo().window(originalHandle);
			}

		}
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='reportname']")
	private WebElement ReportName;

	public void VerifyDeviceActivityReportNameAndItsGroup() {
		String reportname = ReportName.getText();
		String[] st = reportname.split(",");
		for (int i = 0; i < st.length; i++) {
			if (i == 0) {
				boolean ActualText = st[i].contains("Device Activity");
				String pass = "PASS >> Report Name is correct: " + ActualText + "";
				String fail = "FAIL >> Report Name is not correct: " + ActualText + "";
				ALib.AssertTrueMethod(ActualText, pass, fail);
			}
			if (i == 1) {
				String ActualText = st[i];
				String ExpectedText = " Group Name: dont";
				String pass = "PASS >> Groups Name is correct: " + ActualText + "";
				String fail = "FAIL >> Group Name is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);
			}
		}
	}

	public void DeviceNameColumnVerificationDeviceActivityReport(String device) {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]/p"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Report is blank");
		}
		for (int i = 0; i <= ls.size(); i++) {
			try {
				if (ls.equals(device)) {
					Reporter.log("PASS >> Device Name in row is correct", true);
				}
			} catch (Exception e) {
				ALib.AssertFailMethod("Device Name in row is wrong");
			}
		}
	}

	public void DeviceNameColumnVerificationComplianceReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]/p"));

		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(Config.DeviceName);
			if (Expectedvalue == false) {
				String FailMessage = "Device Name in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				System.out.println("PASS >> Device Name in row is correct");
			}

		}

	}

	public void ActivityColumnVerificationDeviceActivityReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]"));
		String PassStatement = "PASS >> Data in Activity column's row is correct";
		String FailStatement = "Fail >> Data in Activity column's row is correct";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(Config.ActivityInfo1)
					|| ActualValue.equals(Config.ActivityInfo2);
			if (Expectedvalue == true) {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}

	}

	public void VerifyCoulmnsInDeviceActivityReport() throws InterruptedException {
		List<WebElement> Colunm = Initialization.driver.findElements(
				By.xpath(".//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int NumberOfColunms = Colunm.size();
		for (int i = 0; i < NumberOfColunms; i++) {
			if (i == 0) {
				String ActualText = Colunm.get(i).getText();
				String ExpectedText = Config.DeviceActivityReportcolunm1;
				String pass = "PASS >> Colunm1  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunm1 is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);

			}

			if (i == 1) {
				String ActualText = Colunm.get(i).getText();
				String ExpectedText = Config.DeviceActivityReportcolunm2;
				String pass = "PASS >> Colunm2  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunm2 is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);

			}

			if (i == 2) {
				String ActualText = Colunm.get(i).getText();
				String ExpectedText = Config.DeviceActivityReportcolunm3;
				String pass = "PASS >> Colunm3  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunm3 is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);

			}
		}

	}

	public void VerifyDeviceActivityReportdata_today() throws InterruptedException {

		List<WebElement> colunm_data = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr[10]/td"));
		for (int i = 0; i < colunm_data.size(); i++) {
			if (i == 0) {
				String ActualText = colunm_data.get(i).getText();
				// ActualText.equalsIgnoreCase("Honor");
				Reporter.log("PASS >> Column data1 is correct");
				String ExpectedText = Config.DeviceName;
				String pass = "PASS >> Colunmdata1  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunmdata1 is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);
			}
			if (i == 1) {
				Date date = new Date();
				String D = date.toString();
				String[] str = D.split(" ");
				int t = Integer.parseInt(str[2]) - 0;
				String s = Integer.toString(t);
				String Date = s + " " + str[1] + "t" + " " + str[5];
				boolean ActualText = colunm_data.get(i).getText().contains(Date);
				String pass = "PASS >> Colunmdata2  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunmdata2 is not correct: " + ActualText + "";
				ALib.AssertTrueMethod(ActualText, pass, fail);
			}

			if (i == 2) {

				String ActualText = colunm_data.get(i).getText();
				if (ActualText.equals("Device Came Online") || ActualText.equals("Device Updated its Info")
						|| ActualText.equals("Device Went Offline"))
					;
				Reporter.log("PASS >> Columndata3 is correct");

			}

		}

	}

	public void DeviceNameColumnVerificationDeviceHealthReport(String device) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(device);
			if (value == true) {

				String PassMessage = "PASS >> Device name in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "FAIL >> Device name in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void VerifyDeviceActivityReportdatayesterday() throws InterruptedException {

		List<WebElement> colunm_data = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr[5]/td"));
		for (int i = 0; i < colunm_data.size(); i++) {
			if (i == 0) {

				String ActualText = colunm_data.get(i).getText();
				ActualText.equalsIgnoreCase("Honor");
				Reporter.log("PASS >> Column data1 is correct");
				String ExpectedText = Config.DeviceActivityReportdata1;
				String pass = "PASS >> Colunmdata1  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunmdata1 is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);
			}
			if (i == 1) {
				Date date = new Date();
				String D = date.toString();
				String[] str = D.split(" ");
				int t = Integer.parseInt(str[2]) - 1;
				String s = Integer.toString(t);
				String Date = s + " " + str[1] + "t" + " " + str[5];
				boolean ActualText = colunm_data.get(i).getText().contains(Date);
				String pass = "PASS >> Colunmdata2  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunmdata2 is not correct: " + ActualText + "";
				ALib.AssertTrueMethod(ActualText, pass, fail);
			}

			if (i == 2) {
				String ActualText = colunm_data.get(i).getText();
				if (ActualText.equals("Device Came Online") || ActualText.equals("Device Updated its Info")
						|| ActualText.equals("Device Went Offline"))
					;
				Reporter.log("PASS >> Columndata3 is correct");
			}
		}

	}

	public void DeleteDeviceActivityReport() throws InterruptedException {
		reportButton.click();
		OnDemandReportOption.click();
		sleep(5);
		viewReportsOption.click();
		sleep(4);
		reportFirstRow.click();
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[text()='Device Activity']"));
		int deviceActivityReportCount = ls.size();
		System.out.println(deviceActivityReportCount);
		deleteReportButton.click();
		sleep(6);
		Deletyes.click();
		sleep(3);

	}

	public void JobStatusColumnVerificationJobDeployedReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.contains(Config.JobStatus) || ActualValue.contains(Config.JobStatus1)
					|| ActualValue.contains(Config.JobStatus2) || ActualValue.contains(Config.JobStatus3)
					|| ActualValue.contains(Config.JobStatus4);
			if (value == true) {

				String PassMessage = "Job Status in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "Job Status in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void JobNameColumnVerificationInstalledJobReport(String job) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(job);
			if (value == true) {

				String PassMessage = "Job Name in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "Job Name in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void UserIDColumnVerificationInstalledJobReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[6]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.UserID);
			if (value == true) {

				String PassMessage = "User ID in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "User ID in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void JobStatusColumnVerificationInstalledJobReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.JobStatus);
			if (value == true) {

				String PassMessage = "Job Status in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "Job Status in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void GPSTestColumnVerificationInstalledJobReport(String gpsVersion) {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[7]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(gpsVersion);
			if (value == true) {

				String PassMessage = "GPS Test Version in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "GPS Test Version in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void BatteryPercentColumnVerificationDeviceHealthReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("FAIL >> No Data is there in report");
		}
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.BatteryPercentageColumnValue1)
					|| ActualValue.equals(Config.BatteryPercentageColumnValue1);
			if (value == true) {

				String FailMessage = "Battery Percentage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Battery Percentage in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void CheckSIMSerialNumber() throws InterruptedException {
		WebElement scroll = Initialization.driver.findElement(By
				.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[19]/div[1]"));
		js.executeScript("arguments[0].scrollIntoView()", scroll);
		sleep(2);
		String Actual = SIMSerialNumber.getText();
		String Expected = Config.SIMSerialNumber;
		String pass = "PASS >> SIM serial Number is displayed correctly";
		String fail = "FAIL >> SIM serial number is not displayed";
		ALib.AssertEqualsMethod(Expected, Actual, pass, fail);

	}

	@FindBy(xpath = "//*[@id='reportList_tableCont']/section[1]/div[2]/div[1]/div[2]/input")
	private WebElement SearchDevice;

	public void SearchDeviceInReportsView(String DeviceName) throws InterruptedException {
		SearchDevice.clear();
		sleep(2);
		SearchDevice.sendKeys(DeviceName);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[20]")
	private WebElement SIMSerialNumber;

	public void AvailableStorageColumnVerificationDeviceHealthReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]/p"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("FAIL >> No Data is there in report");
		}
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStorageColumnValue1)
					|| ActualValue.equals(Config.AvailableStorageColumnValue2);
			if (value == true) {

				String FailMessage = "Available storage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Available storage in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void AvailablePhysicalMemoryColumnVerificationDeviceHealthReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[5]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailablePhyscialMemoryColumnValue1)
					|| ActualValue.equals(Config.AvailablePhyscialMemoryColumnValue2);
			if (value == true) {

				String FailMessage = "Available Physical Memory in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Available Physical Memory in row is correct";
				System.out.println(PassMessage);
			}

		}
	}

	public void VerifyDeviceHistoryReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Last Connected";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Device Time";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Battery Percent";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Backup Battery Percent";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Available Physical Memory Percent";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 5) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Available Storage Percent";
				String pass = "PASS >> 6th column is present";
				String fail = "FAIL>> 6th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 6) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Wi-Fi Signal Strength (%)";
				String pass = "PASS >> 7th column is present";
				String fail = "FAIL>> 7th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
			
			if (i == 7) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Wi-Fi Signal Strength (DBm)";
				String pass = "PASS >> 8th column is present";
				String fail = "FAIL>> 8th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
			
			if (i == 8) {
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[16]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Data Usage";
				String pass = "PASS >> 9th column is present";
				String fail = "FAIL>> 9th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 9) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Network Operator";
				String pass = "PASS >> 10th column is present";
				String fail = "FAIL>> 10th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 10) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Cell Signal Strength (%)";
				String pass = "PASS >> 11th column is present";
				String fail = "FAIL>> 11th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
			
			if (i == 11) {
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[16]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Cell Signal Strength (DBm)";
				String pass = "PASS >> 12th column is present";
				String fail = "FAIL>> 12th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 12) {

				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Roaming";
				String pass = "PASS >> 13th column is present";
				String fail = "FAIL>> 13th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 13) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "CPU Usage";
				String pass = "PASS >> 14th column is present";
				String fail = "FAIL>> 14th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 14) {
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[18]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "GPU Usage";
				String pass = "PASS >> 15th column is present";
				String fail = "FAIL>> 15th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 15) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Temperature(℃)";
				String pass = "PASS >> 16th column is present";
				String fail = "FAIL>> 16th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 16) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Last System Scan";
				String pass = "PASS >> 17th column is present";
				String fail = "FAIL>> 17th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 17) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Threats Count";
				String pass = "PASS >> 18th column is present";
				String fail = "FAIL>> 18th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
				sleep(2);
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[1]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				sleep(2);
			}
		}
	}

	public void BackupBatteryPercentColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]"));
		String PassStatement = "PASS >> Back Up Battery Percent in row is correct";
		String FailStatement = "Fail >> Back Up Battery Percent in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(Config.BackUpBatteryPercent);
			if (Expectedvalue == false) {
				String FailMessage = "Back Up Battery Percent in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}

		}

	}

	public void Validation_Download_Logs_DeviceActivityReport() throws InterruptedException {
		reportButton.click();
		OnDemandReportOption.click();
		sleep(5);
		viewReportsOption.click();
		sleep(6);
		reportFirstRow.click();
		sleep(2);
		reportDownloadButton.click();
		sleep(5);
		Reporter.log("PASS >> Downloaded report successfully - please refer screenshot");

	}

	public void VerifyDeviceActivityReportSelectGroupandDevice() throws InterruptedException {
		reportButton.click();
		sleep(3);
		OnDemandReportOption.click();
		sleep(5);
		DeviceActivity.click();
		sleep(3);

		SelectDeviceOrGroupbuton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='adddevicebtn']")));
		sleep(3);
		String Actualvalue = isAddDeviceWindowdisplayed.getText();
		String Expectedvalue = "Add Device";
		String PassStatement = "PASS >>" + Expectedvalue + " windows  displayed successfully";
		String FailStatement = "FAIL >>" + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		waitForPageToLoad();
		SelectedGroup.click();
		deviceActivitySearchBox.clear();
		sleep(3);
		Reporter.log("**Verify when correct device name is searched***");
		deviceActivitySearchBox.sendKeys(Config.DeviceName);
		sleep(3);
		Reporter.log("***Selecting the searched device***");
		DeviceActivityReportChooseADeviceToAdd1.click();
		Reporter.log("***Clicking on OK button***");
		AddButtoninWindow.click();
		sleep(2);
		Reporter.log("***Verifying the attribute of the selected device****");
		String Actualvalue1 = deviceActivitySearchBox.getAttribute("value");
		String Expectedvalue1 = Config.DeviceName;
		String PassStatement1 = "PASS >> Device is selected and device " + Expectedvalue1
				+ " is  displayed in the Select device Text box";
		String FailStatement1 = "FAIL >> " + Expectedvalue1 + " is not displayed in the Select device Text box";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(2);
		generateReportButton.click();
		captureReportIsInQueueMessage();
		VerifyReportDeviceActivitydifrentGroup();

	}

	public void WifiDataUsageColumnVerificationDataUsageReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();

			value = ActualValue.equalsIgnoreCase(Config.AvailableStorageColumnValue1)
					|| ActualValue.equalsIgnoreCase(Config.AvailableStorageColumnValue2)
					|| ActualValue.equalsIgnoreCase(Config.WifiDataUsageColumnNegativeValue)
					|| ActualValue.equalsIgnoreCase(Config.WifiDataUsageLegacyColumnNegativeValue);

			if (value == true) {

				String FailMessage = "Wifi Data Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Wifi Data Usage in row is correct";
				System.out.println(PassMessage + ActualValue);
			}

		}
	}

	public void MobileDataUsageColumnVerificationDataUsageReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStorageColumnValue1)
					|| ActualValue.equals(Config.AvailableStorageColumnValue2);
			if (value == true) {

				String FailMessage = "Mobile Data Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Mobile Data Usage in row is correct";
				System.out.println(PassMessage + ActualValue);
			}

		}

	}

	public void SelectBillingCycle() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='tagid']"));
		Select a = new Select(ls.get(0));
		a.selectByIndex(2);
		sleep(2);
	}

	public void NetworkOperatorColumnVerificationDeviceActivityReport(String operator) throws InterruptedException {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[10]/p"));
		String PassStatement = "PASS >> Network Operator in row is correct";
		String FailStatement = "FAIL >> Network Operator in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			System.out.println(ActualValue);
			boolean Expectedvalue = ActualValue.equals(operator);
			if (Expectedvalue == false) {
				String FailMessage = "Network Operator in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}

		}

	}

	public void VerifyReportDeviceActivitydifrentGroup() throws InterruptedException {
		SelectReportDeviceActivity();
		String originalHandle = Initialization.driver.getWindowHandle();
		List<WebElement> ReportList = Initialization.driver
				.findElements(By.xpath(".//*[@id='OfflineReportGrid']/tbody/tr"));
		int ReportCount = ReportList.size();
		for (int i = 0; i < ReportCount; i++) {
			if (i == 0) {
				ReportList.get(0).click();
				sleep(3);
				View1.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));
				VerifyDeviceActivityReportNameAndDifferentGroup();
				VerifyCoulmnsInDeviceActivityReport();
				VerifyDeviceActivityReportdata_different_today();
				sleep(3);
				for (String handle : Initialization.driver.getWindowHandles()) {
					if (!handle.equals(originalHandle)) {
						Initialization.driver.switchTo().window(handle);
						Initialization.driver.close();
					}
				}
				Initialization.driver.switchTo().window(originalHandle);
			}
		}
	}

	public void ClickOnAddButtonInReportsWindow() throws InterruptedException {
		AddButtoninWindow.click();
		sleep(2);
	}

	public void VerifyDeviceActivityReportNameAndDifferentGroup() {
		String reportname = ReportName.getText();
		String[] st = reportname.split(",");
		for (int i = 0; i < st.length; i++) {
			if (i == 0) {
				boolean ActualText = st[i].contains("Device Activity");
				String pass = "PASS >> Report Name is correct: " + ActualText + "";
				String fail = "FAIL >> Report Name is not correct: " + ActualText + "";
				ALib.AssertTrueMethod(ActualText, pass, fail);
			}
			if (i == 1) {
				String ActualText = st[i];
				String ExpectedText = " Group Name: 00";
				String pass = "PASS >> Groups Name is correct: " + ActualText + "";
				String fail = "FAIL >> Group Name is not correct: " + ActualText + " ";
				ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail);
			}
		}
	}

	public void VerifyDeviceActivityReportdata_different_today() throws InterruptedException {

		List<WebElement> colunm_data = Initialization.driver
				.findElements(By.xpath(".//*[@id='devicetable']/tbody/tr[10]/td"));
		for (int i = 0; i < colunm_data.size(); i++) {
			/*
			 * if(i==0) {
			 * 
			 * String ActualText = colunm_data.get(i).getText();
			 * ActualText.equalsIgnoreCase("motog2");
			 * Reporter.log("PASS >> Column data1 is correct"); String ExpectedText =
			 * Config.DeviceActivityReportdata1; String pass =
			 * "PASS >> Colunmdata1  is correct: "+ActualText+""; String fail =
			 * "FAIL >> Colunmdata1 is not correct: "+ActualText+" ";
			 * ALib.AssertEqualsMethod(ExpectedText, ActualText, pass, fail); }
			 */
			if (i == 1) {
				Date date = new Date();
				String D = date.toString();
				String[] str = D.split(" ");
				int t = Integer.parseInt(str[2]) - 0;
				String s = Integer.toString(t);
				String Date = s + " " + str[1] + "t" + " " + str[5];
				boolean ActualText = colunm_data.get(i).getText().contains(Date);
				String pass = "PASS >> Colunmdata2  is correct: " + ActualText + "";
				String fail = "FAIL >> Colunmdata2 is not correct: " + ActualText + "";
				ALib.AssertTrueMethod(ActualText, pass, fail);
			}

			if (i == 2) {
				String ActualText = colunm_data.get(i).getText();
				if (ActualText.equals("Device Came Online") || ActualText.equals("Device Updated its Info")
						|| ActualText.equals("Device Went Offline"))
					;
				Reporter.log("PASS >> Columndata3 is correct");
			}
		}
	}

	@FindBy(xpath = "//*[@id=\"report_gph_canvas\"]")
	private WebElement Graph;

	@FindBy(xpath = "//*[@id=\"gph_display_btn\"]/span")
	private WebElement ViewGraph;

	public void verifyGraphForDeviceActivityReport() throws InterruptedException {

		SelectReportDeviceActivity();
		String originalHandle = Initialization.driver.getWindowHandle();
		List<WebElement> ReportList = Initialization.driver
				.findElements(By.xpath(".//*[@id='OfflineReportGrid']/tbody/tr"));
		int ReportCount = ReportList.size();
		for (int i = 0; i < ReportCount; i++) {
			if (i == 0) {
				ReportList.get(0).click();

				waitForXpathPresent("//*[@id=\"OfflineReportGrid\"]/tbody/tr[1]/td[5]/a[2]");
				sleep(4);
				View1.click();
				sleep(3);
				ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
				Initialization.driver.switchTo().window(tabs.get(1)); // it's
				// related
				// items
				// are
				// on
				// top
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 40);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@class='form-control']")));

				ViewGraph.click();
				waitForXpathPresent("//*[@id=\"grid_display_btn\"]/span");
				sleep(3);
				boolean Actualvalue = Graph.isDisplayed();
				;
				String pass = "PASS >> Device Activty Graph is displayed";
				String fail = "FAIL >> Device Activty Graph is not displayed";
				ALib.AssertTrueMethod(Actualvalue, pass, fail);

			}
		}
	}

	public void WifiDataUsageColumnVerificationDataUsageLegacyReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.WifiDataUsageLegacyColumnNegativeValue)
					|| ActualValue.equals(Config.AvailableStorageColumnValue2)
					|| ActualValue.equals(Config.WifiDataUsageColumnNegativeValue)
					|| ActualValue.equals(Config.AvailableStorageColumnValue1);
			if (value == true) {

				String FailMessage = "Wifi Data Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Wifi Data Usage in row is correct";
				System.out.println(PassMessage + ActualValue);
			}

		}
	}

	public void InvalidSearchofReports() throws InterruptedException {

		viewReportsOption.click();
		sleep(3);
		ReportSearchButtonInViewReport.sendKeys("invalid");
		sleep(3);

		boolean isdisplayed = true;

		try {
			InvalidNameReportSearchWarning.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'No matching result found.' is displayed", true);
		sleep(3);
		ReportSearchButtonInViewReport.clear();
		sleep(5);
	}

	public boolean CheckRequestReportBtn() {
		boolean value = generateReportButton.isDisplayed();
		return value;
	}

	public void ClickOnSelectGroup() throws InterruptedException {
		SelectGroup.click();
		sleep(5);
	}

	public void DownloadReport() throws InterruptedException {

		reportFirstRow.click();
		sleep(2);
		reportDownloadButton.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//*[@id='downloadReports_modal']/div/div[1]/img")).click();
		waitForXpathPresent("//span[text()='On Demand Reports']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupList-searchCont']/input")
	private WebElement SearchGroup;

	public void SearchGroup(String Groupname) throws InterruptedException {
		SearchGroup.sendKeys(Groupname);
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='groupList']/ul/li[text()='dontouch']")
	private WebElement groupName;

	public void SelectTheGroup() throws InterruptedException {
		groupName.click();
		sleep(2);
	}

	/*
	 * public void DeviceRoamingColumnVerificationDeviceActivityReport(String
	 * dataroaming) { List<WebElement> ls =
	 * Initialization.driver.findElements(By.xpath(
	 * "//*[@id='devicetable']/tbody/tr/td[11]/p")); String PassStatement =
	 * "PASS >> Device Roaming in row is correct"; String FailStatement =
	 * "Fail >> Device Roaming in row is wrong"; for (int i = 0; i < ls.size(); i++)
	 * { String ActualValue = ls.get(i).getText(); boolean Expectedvalue =
	 * ActualValue.equals(dataroaming); if (Expectedvalue == false) { String
	 * FailMessage = "Device Roaming in row is wrong";
	 * ALib.AssertFailMethod(FailMessage); } else {
	 * ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement); } } }
	 */

	public void SelectGroupDeviceHealthReport() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='SelectGroup']"));
		ls.get(0).click();
		sleep(5);
	}

	public void ClickOnOnDemandReport() throws InterruptedException {
		OnDemandReportOption.click();
		sleep(4);
	}

	public void ClickOnViewReports() throws InterruptedException {
		viewReportsOption.click();
		sleep(5);
	}

	@FindBy(xpath = "//div[@id='groupListModal']//button[@type='button'][normalize-space()='OK']")
	private WebElement OkButtonGroupList;

	public void ClickOnOkButtonGroupList() throws InterruptedException {
		OkButtonGroupList.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='ApplicationType']")
	private WebElement AppType;

	public void SelectApplicationType(String ApplicationType) {
		Select application = new Select(AppType);
		application.selectByVisibleText(ApplicationType);
	}

	@FindBy(xpath = "//div[@id='selJob_addList_modal']/div[3]/button")
	private WebElement OkButton;

	public void ClickOnOkButtonSelectJobToAddInReport() throws InterruptedException {
		OkButton.click();
		sleep(2);
	}

	public void BatteryPercentColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.BatteryPercentageColumnValue1)
					|| ActualValue.equals(Config.BatteryPercentageColumnValue2);
			if (value == true) {

				String FailMessage = "Battery Percent in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Battery Percent in row is correct";
				System.out.println(PassMessage + ActualValue);
			}

		}

	}

	public void EnterStorageSpace() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='storagemb']"));
		ls.get(0).clear();
		ls.get(0).sendKeys("100000");

	}

	public void AvailablePhysicalMemoryColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[5]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailablePhysicalMemoryPercentColumnValue1)
					|| ActualValue.equals(Config.AvailablePhysicalMemoryPercentColumnValue2)
					|| ActualValue.equals(Config.AvailablePhysicalMemoryPercentColumnValue3)
					|| ActualValue.equals(Config.AvailablePhysicalMemoryPercentColumnValue4)
					|| ActualValue.equals(Config.AvailablePhysicalMemoryPercentColumnValue5);
			if (value == true) {

				String FailMessage = "Available Physical Memory Percent in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Available Physical Memory Percent in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void EnterPhysicalMemory() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='memorymb']"));
		ls.get(0).clear();
		ls.get(0).sendKeys("100000");

	}

	public void AvailableStoragePercentColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[6]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue5);
			if (value == true) {

				String FailMessage = "Available Storage Percent in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Available Storage Percent in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void JobNameColumnVerificationJobDeployedReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue5);
			if (value == true) {

				String FailMessage = "FAIL >> Job Name in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Job Name in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void DeviceNameColumnVerificationJobDeployedReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[5]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.DeviceName);
			if (value == true) {

				String PassMessage = "PASS >> Device Name in row is correct";
				System.out.println(PassMessage);

			}

		}

	}

	public void UserIDColumnVerificationJobDeployedReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[5]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue5);
			if (value == true) {

				String FailMessage = "FAIL >> User ID in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> User ID in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void WifiSignalStrengthColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[7]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4);
			if (value == true) {

				String FailMessage = "Wifi Signal Strength in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Wifi Signal Strength in row is correct";
				System.out.println(PassMessage);
			}

		}
	}

	public void DataUsageColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[8]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4);
			if (value == true) {

				String FailMessage = "Data Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "PASS >> Data Usage in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void TemperatureColumnVerificationDeviceHistoryReport() {
		List<WebElement> ls = Initialization.driver.findElements(By
				.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[14]/div[1]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4);
			if (value == true) {

				String FailMessage = "Temperature in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Temperature in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void CPUUsageColumnVerificationDeviceActivityReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[12]"));
		String PassStatement = "PASS >> CPU Usage in row is correct";
		String FailStatement = "Fail >> CPU Usage in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			System.out.println("CPU usage is: " + ActualValue);
			boolean Expectedvalue = ActualValue.equals(Config.CPUUsage);
			if (Expectedvalue == false) {
				String FailMessage = "CPU Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}
	}

	public void GPUUsageColumnVerificationDeviceActivityReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[13]"));
		String PassStatement = "PASS >> GPU Usage in row is correct";
		String FailStatement = "Fail >> GPU Usage in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(Config.GPUUsage);
			if (Expectedvalue == false) {
				String FailMessage = "GPU Usage in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}
	}

	public void LastSystemScanColumnVerificationDeviceActivityReport(String lastsystem) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[17]"));
		String PassStatement = "PASS >> Last System Scan in row is correct";
		String FailStatement = "Fail >> Last System Scan in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(lastsystem);
			if (Expectedvalue == false) {
				String FailMessage = "Last System Scan in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}
	}

	public void DeviceStatusColumnVerificationDeviceConnectedReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.DeviceStatusColumnValue)
					|| ActualValue.equals(Config.DeviceStatusColumnValue1);
			if (value == true) {
				String PassMessage = "PASS >> Device Staus in row is correct";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Device Staus in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void ClickOnDate() throws InterruptedException {
		datE.click();
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement SearchTheJob;

	public void SearchJobInReport(String JobName) throws InterruptedException {
		SearchTheJob.sendKeys(JobName);
		sleep(10);
	}

	@FindBy(xpath = "//*[@id='CompJobDataGrid']/tbody/tr/td[2]")
	private WebElement SelectTheJob;

	public void SelectTheSearchedJob() throws InterruptedException {
		SelectTheJob.click();
		sleep(2);
	}

	public void ClickOnPeriod(String Period) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("(//li[text()='" + Period + "'])[last()]")).click();
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='masterBody']/div[59]/div[3]/ul/li[text()='Custom Range']")
	private WebElement CustomRange;

	public void ClickOnCustomRange() throws InterruptedException {
		CustomRange.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='masterBody']/div[59]/div[3]/div/button[text()='Apply']")
	private WebElement Apply;

	public void ClickOnApplyButtonCustomRange() throws InterruptedException {
		Apply.click();
		sleep(2);
	}

	@FindBy(xpath = "//li[text()='This Month']")
	private WebElement ThisMonthInstallJob;

	public void ClickOnThisMonthInstallJob() throws InterruptedException {
		ThisMonthInstallJob.click();
		sleep(2);
	}

	public void VerifyDataUsageReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Wi Fi Data Usage (Bytes)";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Mobile Data Usage (Bytes)";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyDataUsageLegacyReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Wi-Fi Data Usage (MB)";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Mobile Data Usage (MB)";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyAssetTrackingReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				System.out.println(ActualSecondColumn);
				String ExpectedSecondColumn = "Device Model";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "OS Version";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "OS Type";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Registered Date";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 5) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Online Status";
				String pass = "PASS >> 6th column is present";
				String fail = "FAIL>> 6th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 6) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Last Connected";
				String pass = "PASS >> 7th column is present";
				String fail = "FAIL>> 7th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[12]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				sleep(5);
			}

			if (i == 7) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Time";
				String pass = "PASS >> 8th column is present";
				String fail = "FAIL>> 8th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 8) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Time Zone";
				String pass = "PASS >> 9th column is present";
				String fail = "FAIL>> 9th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 9) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Locale";
				String pass = "PASS >> 10th column is present";
				String fail = "FAIL>> 10th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 10) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "MAC Address";
				String pass = "PASS >> 10th column is present";
				String fail = "FAIL>> 10th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 11) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "IMEI";
				String pass = "PASS >> 11th column is present";
				String fail = "FAIL>> 11th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll1 = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[19]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll1);
				sleep(10);
			}

			if (i == 12) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "IMSI";
				String pass = "PASS >> 12th column is present";
				String fail = "FAIL>> 12th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

			}

			if (i == 13) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Battery Percent";
				String pass = "PASS >> 13th column is present";
				String fail = "FAIL>> 13th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 14) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Battery State";
				String pass = "PASS >> 14th column is present";
				String fail = "FAIL>> 14th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 15) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Wi-Fi SSID";
				String pass = "PASS >> 15th column is present";
				String fail = "FAIL>> 15th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 16) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Wi-Fi Signal Strength";
				String pass = "PASS >> 16th column is present";
				String fail = "FAIL>> 16th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 17) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Operator";
				String pass = "PASS >> 17th column is present";
				String fail = "FAIL>> 17th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

			}

			if (i == 18) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Phone Signal Strength";
				String pass = "PASS >> 18th column is present";
				String fail = "FAIL>> 18th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll2 = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[26]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll2);
				sleep(5);
			}

			if (i == 19) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Phone Serial Number";
				String pass = "PASS >> 19th column is present";
				String fail = "FAIL>> 19th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 20) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SIM Serial Number";
				String pass = "PASS >> 20th column is present";
				String fail = "FAIL>> 20th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 21) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Phone Number";
				String pass = "PASS >> 21st column is present";
				String fail = "FAIL>> 21st column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

			}

			if (i == 22) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Global IP Address";
				String pass = "PASS >> 22nd column is present";
				String fail = "FAIL>> 22nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 23) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Local IP Address";
				String pass = "PASS >> 23rd column is present";
				String fail = "FAIL>> 23rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 24) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Root Status";
				String pass = "PASS >> 24th column is present";
				String fail = "FAIL>> 24th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 25) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "KNOX Status";
				String pass = "PASS >> 25th column is present";
				String fail = "FAIL>> 25th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 26) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "KNOX Status";
				String pass = "PASS >> 26th column is present";
				String fail = "FAIL>> 26th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 27) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Nix Agent Version";
				String pass = "PASS >> 27th column is present";
				String fail = "FAIL>> 27th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 28) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureLock Version";
				String pass = "PASS >> 28th column is present";
				String fail = "FAIL>> 28th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 28) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureFox Version";
				String pass = "PASS >> 29th column is present";
				String fail = "FAIL>> 29th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 29) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureVideo Version";
				String pass = "PASS >> 30th column is present";
				String fail = "FAIL>> 30th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 30) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Bluetooth SSID";
				String pass = "PASS >> 31st column is present";
				String fail = "FAIL>> 31st column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll3 = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[37]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll3);
				sleep(5);

			}

			if (i == 31) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureLock Settings Identifier";
				String pass = "PASS >> 32nd column is present";
				String fail = "FAIL>> 32nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 32) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Bluetooth Status";
				String pass = "PASS >> 33rd column is present";
				String fail = "FAIL>> 33rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 33) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "USB Status";
				String pass = "PASS >> 34th column is present";
				String fail = "FAIL>> 34th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 34) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Default Launcher Application";
				String pass = "PASS >> 35th column is present";
				String fail = "FAIL>> 35th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 35) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group Name";
				String pass = "PASS >> 36th column is present";
				String fail = "FAIL>> 36th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 36) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group Path";
				String pass = "PASS >> 37th column is present";
				String fail = "FAIL>> 37th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 37) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Notes";
				String pass = "PASS >> 38th column is present";
				String fail = "FAIL>> 38th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll4 = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[43]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll4);
				sleep(5);
			}

			if (i == 38) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Supervised(IOS)";
				String pass = "PASS >> 39th column is present";
				String fail = "FAIL>> 39th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 39) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "OS Build Number";
				String pass = "PASS >> 40th column is present";
				String fail = "FAIL>> 40th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 40) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Free Storage Memory";
				String pass = "PASS >> 41st column is present";
				String fail = "FAIL>> 41st column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 41) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Free Program Memory";
				String pass = "PASS >> 42nd column is present";
				String fail = "FAIL>> 42nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 42) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureLock Default Launcher";
				String pass = "PASS >> 43rd column is present";
				String fail = "FAIL>> 43rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 43) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "SureLock Device Administrator";
				String pass = "PASS >> 44th column is present";
				String fail = "FAIL>> 44th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[51]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);

			}

			if (i == 44) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Samsung KNOX";
				String pass = "PASS >> 45th column is present";
				String fail = "FAIL>> 45th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 45) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Apps With Usage Access";
				String pass = "PASS >> 46th column is present";
				String fail = "FAIL>> 46th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 46) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Disable USB Debugging";
				String pass = "PASS >> 47th column is present";
				String fail = "FAIL>> 47th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 47) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Wi-Fi Hotspot Status";
				String pass = "PASS >> 48th column is present";
				String fail = "FAIL>> 48th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 48) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Encryption Status";
				String pass = "PASS >> 49th column is present";
				String fail = "FAIL>> 49th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 49) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "IMEI2";
				String pass = "PASS >> 50th column is present";
				String fail = "FAIL>> 50th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 50) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "CTS Verified";
				String pass = "PASS >> 51st column is present";
				String fail = "FAIL>> 51st column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 51) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Platform Integrity";
				String pass = "PASS >> 52nd column is present";
				String fail = "FAIL>> 52nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);

				WebElement scroll5 = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[58]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll5);
				sleep(5);
			}

			if (i == 52) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Threat Protection";
				String pass = "PASS >> 53rd column is present";
				String fail = "FAIL>> 53rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 53) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "USB Debugging";
				String pass = "PASS >> 54th column is present";
				String fail = "FAIL>> 54th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 54) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Unknown Source Apps";
				String pass = "PASS >> 55th column is present";
				String fail = "FAIL>> 55th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 55) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "AndroidID";
				String pass = "PASS >> 56th column is present";
				String fail = "FAIL>> 56th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 56) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "HashCode";
				String pass = "PASS >> 57th column is present";
				String fail = "FAIL>> 57th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 57) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Efota Registration Status";
				String pass = "PASS >> 58th column is present";
				String fail = "FAIL>> 58th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 58) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Current Firmware Version";
				String pass = "PASS >> 59th column is present";
				String fail = "FAIL>> 59th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

		}

	}

	public void ThreatsCountColumnVerificationDeviceActivityReport(String threatcount) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[18]"));
		String PassStatement = "PASS >> Threats Count in row is correct";
		String FailStatement = "Fail >> Threats Count in row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(threatcount);
			if (Expectedvalue == false) {
				String FailMessage = "Threats Count in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}
	}

	public void MobileSignalColumnVerificationDeviceHistoryReport() throws InterruptedException {
		WebElement scroll = Initialization.driver.findElement(By
				.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[16]/div[1]"));
		js.executeScript("arguments[0].scrollIntoView()", scroll);
		sleep(5);
		List<WebElement> ls = Initialization.driver.findElements(By
				.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[10]/div[1]"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equals(Config.AvailableStoragePercentColumnValue1)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue2)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue3)
					|| ActualValue.equals(Config.AvailableStoragePercentColumnValue4);
			if (value == true) {

				String FailMessage = "Mobile Signal in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				String PassMessage = "Mobile Signal in row is correct";
				System.out.println(PassMessage);
			}

		}

	}

	public void VerifyDeviceConnectedReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Last Connected";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Registered Date";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Status";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyJobsDeployedReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Job Schedule Time";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Job Deployed Time";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Status";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Name";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Name";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 5) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "User ID";
				String pass = "PASS >> 6th column is present";
				String fail = "FAIL>> 6th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyInstallJobReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Device Name";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Job Name";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Status";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Schedule Time";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Deployed Time";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 5) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "User ID";
				String pass = "PASS >> 6th column is present";
				String fail = "FAIL>> 6th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 15) {
				WebElement scroll = Initialization.driver.findElement(By.xpath(
						"//*[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[1]/table/thead/tr/th[22]/div[1]"));
				js.executeScript("arguments[0].scrollIntoView()", scroll);
				sleep(2);
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "GPS Test";
				String pass = "PASS >> 16th column is present";
				String fail = "FAIL>> 16th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void DeviceModelColumnVerificationAssetReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(Config.DeviceNameColumn)
					|| ActualValue.equalsIgnoreCase(Config.DeviceModelVersion);
			if (value == true) {

				String FailMessage = "Device Model in row is wrong: " + ActualValue;
				ALib.AssertFailMethod(FailMessage);
			} else {
				Reporter.log("Device Model in row is correct", true);
			}

		}

	}

	public void OSVersionColumnVerificationAssetReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(Config.DeviceNameColumn)
					|| ActualValue.equalsIgnoreCase(Config.DeviceModelVersion);
			if (value == true) {

				String FailMessage = "OS Version in row is wrong:" + ActualValue;
				ALib.AssertFailMethod(FailMessage);
			} else {
				Reporter.log("OS Version in row is correct", true);
			}

		}

	}

	public void OSTypeColumnVerificationAssetReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(Config.OSType1) || ActualValue.equalsIgnoreCase(Config.OSType2)
					|| ActualValue.equalsIgnoreCase(Config.OSType3) || ActualValue.equalsIgnoreCase(Config.OSType4)
					|| ActualValue.equalsIgnoreCase(Config.OSType5) || ActualValue.equalsIgnoreCase(Config.OSType6)
					|| ActualValue.equalsIgnoreCase(Config.OSType7) || ActualValue.equalsIgnoreCase(Config.OSType8)
					|| ActualValue.equalsIgnoreCase(Config.OSType9);
			if (value == true) {

				String PassMessage = "OS Type in row is correct";
				System.out.println(PassMessage);

			} else {
				Reporter.log("OS Type in row is wrong", true);
			}

		}

	}

	public void OnlineStatusColumnVerificationAssetReport() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[6]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(Config.OnlineStatus1)
					|| ActualValue.equalsIgnoreCase(Config.OnlineStatus2);
			if (value == true) {

				Reporter.log("Online Status in row is correct", true);

			} else {
				String FailMessage = "Online Status in row is wrong: " + ActualValue;
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void KnoxStatusColumnVerificationAssetReport() throws InterruptedException {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[28]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.equalsIgnoreCase(Config.KnoxStatus1)
					|| ActualValue.equalsIgnoreCase(Config.KnoxStatus2);
			if (value == true) {

				Reporter.log("Knox Status in row is correct", true);

			} else {
				String FailMessage = "Knox Status in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void VerifySessionLogFiltersInOnDemandReports() {
		String[] options = { "Show All Logs", "Show Job Logs", "Show App Install/Uninstall Logs",
				"Show User Session Logs" };
		for (int i = 0; i < options.length; i++) {
			try {
				Initialization.driver
						.findElement(By.xpath("//div[@id='reportDetails']//child::span[text()='" + options[i] + "']"))
						.isDisplayed();
				Reporter.log(options[i] + " Option Is  Displayed", true);
			}

			catch (Exception e) {
				ALib.AssertFailMethod(options[i] + "Option Is Not Displayed");
			}
		}
	}

	public void VerifySessionLogsFilterIsEnabledByDefaultInOnDemandReports() throws InterruptedException {
		String ShowUserSessionLogsStatus = Initialization.driver
				.findElement(By
						.xpath("//div[@id='reportDetails']//child::span[text()='Show User Session Logs']/parent::div"))
				.getAttribute("class");

		if (ShowUserSessionLogsStatus.contains("disabledclass")) {
			Reporter.log("Show User Session Logs Is Enabled By Default", true);
		}

		else {
			ALib.AssertFailMethod("Show User Session Logs Is Not Enabled By Default");
		}
	}

	ArrayList<String> al = new ArrayList<>();

	public void ReadingMACAddressInDeviceInfo() throws InterruptedException {
		// Initialization.driver.findElement(By.xpath("//span[@class='moreLess']")).click();
		// sleep(3);

		List<WebElement> Li = Initialization.driver.findElements(By.xpath("(//*[@id='networkinfo-card']/div/p)[1]"));
		List<WebElement> Li1 = Initialization.driver.findElements(By.xpath("(//*[@id='networkinfo-card']/div/p)[1]"));
		for (int i = 0; i < Li.size(); i++) {
			al.add(Li1.get(i).getText());
			System.out.println(al);
			al.add(Li.get(i).getText());
			System.out.println(al);
		}
	}

	String MacAddressIneport;

	public void ReadingMACAddressInReport() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		js.executeScript("arguments[0].scrollIntoView()",
				Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[13]/p")));
		sleep(5);
		WebElement ele = Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[13]/p"));
		act.moveToElement(ele).build().perform();
		MacAddressIneport = ele.getText();
	}

	public void VerifyMACAddressInReport() {
		for (int i = 0; i < al.size(); i++) {

			if (MacAddressIneport.contains(al.get(i))) {
				Reporter.log("PASS>> MAC Address Is Shown Correctly", true);
			} else {
				ALib.AssertFailMethod("FAIL>>> MAC Address Is Not Shown Correctly");
			}
		}
	}

	public void VerifyOfSystemLogReportLogType(String LogType) throws InterruptedException {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]/p"));
		if (ls.size() > 0) {
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				if (ActualValue.equals(LogType)) {
					Reporter.log("Pass>>> Log Type Is Shown Correctly", true);
				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail>>> Log Type Isn't Shown Correctly");
				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail>>> Log Type Column Isn't Shown Correctly");
		}

	}

	public void verifySelectedDateForLast30Days() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForToday() throws ParseException, InterruptedException {
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (EndRange.compareTo(actualDateInReport) == 0) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForToday_SystemLogRep() throws ParseException, InterruptedException {
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (EndRange.compareTo(actualDateInReport) == 0) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForYesterday() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast1week() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 6 * 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForThisMonth() throws ParseException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			int ActualMonthInReport = actualDateInReport.getMonth();
			if (month == ActualMonthInReport) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void SelectingDateRange() throws InterruptedException {
		Calendar cal = Calendar.getInstance();
		int Today = cal.get(Calendar.DATE);
		int CustomDate = Today - 1;
		Initialization.driver
				.findElement(
						By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + CustomDate + "])[last()]"))
				.click();
		System.out.println("Clicked On " + CustomDate);
		Initialization.driver
				.findElement(By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + Today + "])[last()]"))
				.click();
		System.out.println("Clicked On " + Today);
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//input[@placeholder='Home'])[1]")).click();
		sleep(2);
	}

	public void verifySelectedDateForCustomRange() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		String stringEndDate = DateFor.format(getdate);
		Date TodaysDate = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0 || TodaysDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void DisablingCheckBox(String CheckBox) throws InterruptedException {
		boolean CheckboxStatus = Initialization.driver
				.findElement(By.xpath("//div[@id='reportDetails']//child::input[@id='" + CheckBox + "']")).isSelected();
		if (CheckboxStatus == true) {
			Initialization.driver
					.findElement(By.xpath("//div[@id='reportDetails']//child::input[@id='" + CheckBox + "']")).click();
			sleep(2);
			Reporter.log("Disabled " + CheckBox, true);
		}
	}

	public void installingApplicationOnDevice(String Path) throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb install " + "\"" + Path + "\"");
		sleep(10);
		Reporter.log("Installed Application On Device", true);
	}

	public void RemoveAppFromDevice(String Package) throws InterruptedException {
		Initialization.driverAppium.removeApp(Package);
		sleep(3);
		Reporter.log("Uninstalled Application from Device", true);
	}

	public void VerifyOfSystemLogReportLogMessage(String LogMessage) throws InterruptedException {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[5]/p"));
		if (ls.size() > 0) {
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				System.out.println(ActualValue);
				if (ActualValue.contains(LogMessage)) {
					Reporter.log("Pass >> Log Message Is Shown Correctly", true);
				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> Log Message Is not Shown Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> Log Message Column Is not Shown Correctly");

		}
	}

	public void VarifyingCurrentDateInSystemLogOnDemandReport() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[1]"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Reports Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			System.out.println(hello);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully for current date";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully for current date";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	@FindBy(xpath = "//table[@id='dataGrid']/tbody/tr/td")
	private List<WebElement> gridcolumnsval;
	ArrayList<String> gridval = new ArrayList<String>();

	public void getDeviceValuefromgrid() {
		System.out.println(gridcolumnsval.size());
		for (int i = 1; i < gridcolumnsval.size(); i++) {
			js.executeScript("arguments[0].scrollIntoView()", gridcolumnsval.get(i));
			String colnames = gridcolumnsval.get(i).getText();
			String val = colnames.replaceAll("[^a-zA-Z0-9]", "");
			// System.out.println(colnames);
			gridval.add(val);

		}
		System.out.println(gridval);
	}

	@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td")
	private List<WebElement> reportcolumnsval;
	ArrayList<String> reportval = new ArrayList<String>();

	public void getDeviceValeFromAssetTrackingReport() {
		System.out.println(reportcolumnsval.size());
		for (int i = 0; i < reportcolumnsval.size(); i++) {
			js.executeScript("arguments[0].scrollIntoView()", reportcolumnsval.get(i));
			String reportcolvalues = reportcolumnsval.get(i).getText();
			String value = reportcolvalues.replaceAll("[^a-zA-Z0-9]", "");
			// System.out.println(reportcolvalues);
			reportval.add(value);
		}
		System.out.println(reportval);
	}

	public void compareValuesInReport() {

		for (int i = 0; i < gridval.size(); i++) {
			System.out.println(gridval.get(i));
			if (reportval.contains(gridval.get(i))) {
				System.out.println("Values are matching");
			} else {
				ALib.AssertFailMethod("Values are not matching");
			}
		}

	}

	// Kavya
	@FindBy(xpath = "//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
	private WebElement ApplyJobSearchButton;

	public void verifyOfOnDemandReports() {
		WebElement[] ApplyJobElement = { systemlogoption, AssetTrackingOption, DeviceActivity, CallLogTrackingOption,
				JobsDeployedOption, InstalledJobReportOption, DeviceHealthReportOption, DeviceHistoryReportOption,
				DatausageReportOption, DataUsageLegacyReportOption, DeviceConnected, AppVersionReportOption,
				ComplianceReportReportOption };
		String[] ApplyJobText = { "System Log", "Asset Tracking", "Device Activity", "Call Log Tracking",
				"Jobs Deployed", "Installed Job Report", "Device Health Report", "Device History", "Data Usage",
				"Data Usage Legacy", "Device Connected", "App Version", "Compliance Report" };
		for (int i = 0; i < ApplyJobElement.length; i++) {
			boolean value = ApplyJobElement[i].isDisplayed();
			String PassStatement = "PASS >> " + ApplyJobText[i]
					+ " is displayed successfully when clicked on Apply Job Button";
			String FailStatement = "Fail >> " + ApplyJobText[i]
					+ " is not displayed even when clicked on Apply Job Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			js.executeScript("arguments[0].scrollIntoView()", ApplyJobElement[i]);
		}
	}

	public void ClickOnCallLogTrackingReport() throws InterruptedException {
		CallLogTrackingOption.click();
		sleep(8);

	}

	public void VerifyOfCallLogTrackingReportDeviceName(String DeviceName) throws InterruptedException {

		List<WebElement> DeviceName1 = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]"));
		if (DeviceName1.size() > 0) {
			for (int i = 0; i < DeviceName1.size(); i++) {
				String ActualValue = DeviceName1.get(i).getText();
				if (ActualValue.equals(DeviceName)) {
					Reporter.log("Pass >> DeviceName Is Diplayed Correctly", true);
				} else {
					ALib.AssertFailMethod("Fail >> DeviceName Is Not Diplayed Correctly");
				}
			}
		} else {
			ALib.AssertFailMethod("Fail >> DeviceName Column Is Not Diplayed Correctly");
		}
	}

	public void VerifyOfCallType(String calltype) {
		List<WebElement> CallTypes = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]"));
		if (CallTypes.size() > 0) {
			for (int i = 0; i < CallTypes.size(); i++) {
				String ActualValue = CallTypes.get(i).getText();
				if (ActualValue.equalsIgnoreCase(calltype)) {
					Reporter.log("Pass >> CallType Is Diplayed as expected.", true);
				} else {
					ALib.AssertFailMethod("Fail >> CallType Is not Diplayed as expected.");
				}
			}
		} else {
			ALib.AssertFailMethod("Fail >> CallType Column Is Not Diplayed Correctly");
		}
	}

	public void VerifyOfReportOpenedInNextTab() {
		String URL = Initialization.driver.getCurrentUrl();
		if (URL.contains("ViewReportOnline.html")) {
			Reporter.log("Pass >>Report is getting opened in next tab successfully.", true);
		} else {
			ALib.AssertFailMethod("Fail >>Report is not getting opened in next tab.");
		}
	}

	public void VarifyingCurrentDateInCallLogTrackingOnDemandReport() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Reports Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully for current date";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully for current date";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void VerifyOfContactNumber(String number) {// Config.EnterContactNumber
		List<WebElement> ContactNumber = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td/p[text()='" + number + "']"));
		if (ContactNumber.size() > 0) {
			for (int i = 0; i < ContactNumber.size(); i++) {
				String ActualValue = ContactNumber.get(i).getText();
				if (ActualValue.equalsIgnoreCase(number)) {
					Reporter.log("Pass >> Contact Number Is Diplayed as expected.", true);
				} else {
					ALib.AssertFailMethod("Fail >> Contact Number Is not Diplayed as expected.");
				}
			}
		} else {
			ALib.AssertFailMethod("Fail >> Contact Number Column Is Not Diplayed Correctly");
		}
	}

	public void VerifyOfContactName(String name) {// Config.EnterContactNumber
		List<WebElement> ContactName = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td/p[text()='" + name + "']"));
		if (ContactName.size() > 0) {
			for (int i = 0; i < ContactName.size(); i++) {
				String ActualValue = ContactName.get(i).getText();
				if (ActualValue.equalsIgnoreCase(name)) {
					Reporter.log("Pass >> Contact Name Is Diplayed as expected.", true);
				} else {
					ALib.AssertFailMethod("Fail >> Contact Name Is not Diplayed as expected.");
				}
			}
		} else {
			ALib.AssertFailMethod("Fail >> Contact Name Column Is Not Diplayed Correctly");
		}
	}

	public void ClickOnCallType() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='reportDetails']//select[@id='CallType']")).click();
		sleep(2);
	}

	public void SelectCallTypeInOnDemandReport() throws InterruptedException {
		Select a = new Select(Initialization.driver.findElement(By.id("CallType")));
		sleep(4);
		a.selectByVisibleText("Incoming");
		sleep(4);
	}

	public void clickOnSelectGroupInODRep() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='reportDetails']//i[@class='fa fa-mobile']")).click();
		sleep(2);
	}

	public void selectGrpAndAdd(String grp) throws InterruptedException {
		js.executeScript("arguments[0].scrollIntoView()", Initialization.driver
				.findElement(By.xpath("//li[@class='list-group-item node-dtree'][normalize-space()='" + grp + "']")));
		sleep(2);
		Initialization.driver
				.findElement(By.xpath("//li[@class='list-group-item node-dtree'][normalize-space()='" + grp + "']"))
				.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//button[@id='adddevicebtn']")).click();
	}

	public void verifySelectedDateForLast30DaysInCallLogTrackingODRep() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[5]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void VarifyingobDeployedTimeInJobDeployedOnDemandReport() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Reports Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			System.out.println(hello);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully for current date";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully for current date";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void verifySelectedDateForYesterdayInJobDeloyedReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	@FindBy(xpath = "//a[text()='Home']")
	private WebElement Home;

	public void verifyofJobScheduleTime() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[4]"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Reports Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully for current date";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully for current date";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void verifyofJobDeployedTime() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
		if (ls.size() == 0) {
			ALib.AssertFailMethod("Reports Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully for current date";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully for current date";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void verifyOfUserId(String userid) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[6]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.contains(userid);
			if (value == true) {

				String PassMessage = "User ID in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "User ID in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void verifyOfSureMDMNix(String nix) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[7]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.contains(nix);
			if (value == true) {

				String PassMessage = "User ID in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "User ID in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void verifyOfSureMDMNixVR(String vr) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[8]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.contains(vr);
			if (value == true) {

				String PassMessage = "SureMDM Nix VR in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "SureMDM Nix VR in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}
	}

	public void verifyOfUninstallSureMDMNix(String nix) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[9]/p"));
		boolean value;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			value = ActualValue.contains(nix);
			if (value == true) {

				String PassMessage = "Uninstall SureMDM Nix in row is correct";
				System.out.println(PassMessage);

			} else {
				String FailMessage = "Uninstall SureMDM Nix in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			}

		}

	}

	public void verifySelectedDateForYesterday_InstalledJobReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForLast1week_InstalledJobReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 6 * 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast30Days_InstalledJobReport() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForThisMonth_InstalledJobReport() throws ParseException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			int ActualMonthInReport = actualDateInReport.getMonth();
			if (month == ActualMonthInReport) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForCustomRange_InstalledJobReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		String stringEndDate = DateFor.format(getdate);
		Date TodaysDate = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0 || TodaysDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void SelectingDateRange_InstalledJob() throws InterruptedException {
		Calendar cal = Calendar.getInstance();
		int Today = cal.get(Calendar.DATE);
		int CustomDate = Today - 1;
		Initialization.driver
				.findElement(
						By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + CustomDate + "])[last()]"))
				.click();
		System.out.println("Clicked On " + CustomDate);
		Initialization.driver
				.findElement(By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + Today + "])[last()]"))
				.click();
		System.out.println("Clicked On " + Today);
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//button[text()='Apply'])[last()]")).click();
		sleep(2);
	}

	public void verifySelectedDateForToday_DeviceActivityReport() throws ParseException, InterruptedException {
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");

		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		System.out.println(DateForMat);
		String stringEndDate = DateForMat.format(date);
		System.out.println(stringEndDate);
		Date EndRange = DateFor.parse(stringEndDate);
		System.out.println(EndRange);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			System.out.println(StringDat);
			List<String> al = new ArrayList<String>();
			System.out.println(al);
			al = Arrays.asList(StringDat);
			System.out.println(al);
			String[] array = al.toArray(new String[0]);
			System.out.println(array);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			System.out.println(actualDateInReport);
			if (EndRange.compareTo(actualDateInReport) == 0) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForYesterdayInDeviceActivityReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast1week_DeviceActivity() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 6 * 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast30Days_DeviceActivity() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForThisMonth_DeviceActivity() throws ParseException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			int ActualMonthInReport = actualDateInReport.getMonth();
			if (month == ActualMonthInReport) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[2]")
	private List<WebElement> colData;

	public void verifySelectedDateForCustomRange_DeviceActivity() throws ParseException, InterruptedException {
		if (colData.size() == 0) {
			ALib.AssertFailMethod("FAIL >> Report is blank");
		}
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		String stringEndDate = DateFor.format(getdate);
		Date TodaysDate = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("dd MMM yyyy").parse(array[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0 || TodaysDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForToday_DeviceConnectedReport() throws ParseException, InterruptedException {
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (EndRange.compareTo(actualDateInReport) == 0) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifyOfDeviceConnectedReport_CustomRange() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		String stringEndDate = DateFor.format(getdate);
		Date TodaysDate = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0 || TodaysDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void SelectingDateRange_DeviceconnectReport() throws InterruptedException {
		Calendar cal = Calendar.getInstance();
		int Today = cal.get(Calendar.DATE);
		int CustomDate = Today - 1;
		Initialization.driver
				.findElement(
						By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + CustomDate + "])[last()]"))
				.click();
		System.out.println("Clicked On " + CustomDate);
		Initialization.driver
				.findElement(By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()=" + Today + "])[last()]"))
				.click();
		System.out.println("Clicked On " + Today);
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//button[text()='Apply'])[last()]")).click();
		sleep(2);
	}

	public void verifySelectedDateForThisMonth_DeviceConnectedReport() throws ParseException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			int ActualMonthInReport = actualDateInReport.getMonth();
			if (month == ActualMonthInReport) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedYesterdayDate_DeviceConnected() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForLast1week_DeviceConnectedReport() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 6 * 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

				Reporter.log("PASS >> Date Range Is Displayed Correctly", true);
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("FAIL >> Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast30Days_DeviceConnectedReport() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy MMM dd");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("yyyy MMM dd");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForLast1week_DeviceHealth() throws ParseException, InterruptedException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 6 * 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(array[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForLast30DaysInDeviceHealthReport() throws InterruptedException, ParseException {
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 1000L * 60L * 60L * 24L * 30L);
		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		String stringDate = DateFor.format(strtdate);
		Date previouRange = DateFor.parse(stringDate);
		Date date = new Date();
		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		String stringEndDate = DateForMat.format(date);
		Date EndRange = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String[] StringDat = Dat.getText().split(" ");
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(StringDat[0]);
			if ((actualDateInReport.compareTo(previouRange) > 0) && ((actualDateInReport.compareTo(EndRange) < 0))
					|| (actualDateInReport.compareTo(EndRange) == 0)
					|| (actualDateInReport.compareTo(previouRange) == 0)) {

			} else {
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}
	}

	public void verifySelectedDateForThisMonth_DeviceHealthReport() throws ParseException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(array[0]);
			int ActualMonthInReport = actualDateInReport.getMonth();
			if (month == ActualMonthInReport) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public void verifySelectedDateForCustomRange_DeviceHealthReport() throws ParseException, InterruptedException {
		if (colData.size() == 0) {
			ALib.AssertFailMethod("FAIL >> Report is blank");
		}
		Date getdate = new Date();
		Date strtdate = new Date(getdate.getTime() - 24 * 60 * 60 * 1000);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = DateFor.format(strtdate);
		Date YesterdayDate = DateFor.parse(stringDate);
		String stringEndDate = DateFor.format(getdate);
		Date TodaysDate = DateFor.parse(stringEndDate);
		List<WebElement> Vals = Initialization.driver
				.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
		for (WebElement Dat : Vals) {
			String StringDat = Dat.getText().substring(0, 11);

			List<String> al = new ArrayList<String>();
			al = Arrays.asList(StringDat);
			String[] array = al.toArray(new String[0]);
			Date actualDateInReport = new SimpleDateFormat("yyyy-MM-dd").parse(array[0]);
			if (YesterdayDate.compareTo(actualDateInReport) == 0 || TodaysDate.compareTo(actualDateInReport) == 0) {
			} else {
				SwitchBackWindow();
				ALib.AssertFailMethod("Date Range Is Not Displayed Correctly");
			}
		}

	}

	public String lastDate;

	@FindBy(xpath = "//*[@class='LastTimeStamp']/p[1]")
	private WebElement DevicelastConnectedDate;

	public void GetDeviceLastConnectedDate() {
		lastDate = DevicelastConnectedDate.getText();
		System.out.println(lastDate);

	}

	public String a2, b2, d2, e2;
	public static String st2;

	public void splitDateAndTimeAsExpected(String s) {
		System.out.println("================Splitting date===================");
		String datetime = s;
		String[] arrOfStr1 = datetime.split("T");
		a2 = arrOfStr1[0];
		b2 = arrOfStr1[1];
		System.out.println(a2);
		System.out.println(b2);

		System.out.println("================Splitting time===================");
		String[] arrOfStr2 = b2.split("\\+");
		d2 = arrOfStr2[0];
		e2 = arrOfStr2[1];
		System.out.println(d2);
		System.out.println(e2);

		System.out.println("================Combine date and time ===================");

		st2 = a2 + " " + d2;
		System.out.println(st2);

	}

	public String lastconnecteddatefromreport;

	@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[2]")
	private WebElement DevicelastConnectedDateinRep;

	public void getLastConnectedDatetime_DeviceHealthReport(int ColumnNumber) {
		lastconnecteddatefromreport = Initialization.driver
				.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[" + ColumnNumber + "]/p")).getText();
		System.out.println(lastconnecteddatefromreport);

	}

	// Harpinder

	static String dt, dt1, dt2, dt3, s1, s2, a, b, c, d, e, f, g, h, last, i, j, output, k;

	// System.out.println("================Splitting date===================");
	static void splitDate(String a) {
		System.out.println("================Splitting date===================");
		String datetime = a;
		String[] arrOfStr1 = datetime.split(" ");
		a = arrOfStr1[0];
		b = arrOfStr1[1];
		c = arrOfStr1[2];
		d = arrOfStr1[3];
		e = arrOfStr1[4];
		a = arrOfStr1[0];
		if (a.length() == 1) {
			a = "0" + a;

		} else {
			a = arrOfStr1[0];

		}
		dt = a + " " + b + " " + c;
		System.out.println(dt);
		dt2 = arrOfStr1[3];
	}

	/*
	 * static void splitTime() {
	 * System.out.println("================Splitting time===================");
	 * String[] arrOfStr2 = dt2.split(":"); f = arrOfStr2[0]; g = arrOfStr2[1]; h =
	 * arrOfStr2[2];
	 * 
	 * if (f.length() == 1) { f = "0" + f; dt3 = f + ":" + g + ":" + h + " " + e;
	 * 
	 * System.out.println(dt3); // System.out.println(englishTime(dt3)); } else {
	 * dt3 = f + ":" + g + ":" + h + " " + e; System.out.println(dt3); //
	 * System.out.println(englishTime(dt3)); } }
	 */

	// Convert 24hr time format
	public static String englishTime(String input) throws ParseException {
		System.out.println("==============Converting 24hr time format================");
		// Format of the date defined in the input String
		DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss aa");

		// Change the pattern into 24 hour formate
		DateFormat formate = new SimpleDateFormat("HH:mm:ss");
		Date time = null;
		// String output = "";

		// Converting the input String to Date
		time = dateFormat.parse(input);

		// Changing the format of date
		// and storing it in
		// String
		output = formate.format(time);
		System.out.println(output);
		return output;

	}

	// Join time and date
	/*
	 * static void joinDateAndTime() {
	 * System.out.println("==============Join Date and time ================"); k =
	 * dt + " " + output; System.out.println(k); }
	 */

	// reading value from report
	public static void getTimeStampFromReport(String b) throws InterruptedException {

		String datetime = b;
		String[] arrOfStr = datetime.split("\\.", 2);
		s2 = arrOfStr[0];
		System.out.println("S2 : " + s2);
	}

	// Convert dta time format

	public static void convertDateTimeFormat(String c) throws ParseException {
		System.out.println("==============Convert date format as per report format ================");
		// date format 1
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");

		// date format 2
		DateTimeFormatter dateFormatterNew = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		// string to LocalDateTime
		LocalDateTime ldateTime = LocalDateTime.parse(c, dateFormatter);

		// System.out.println(dateFormatter.format(ldateTime));

		// change date format
		s1 = dateFormatterNew.format(ldateTime);

		System.out.println("S1 : " + s1);

	}

	public void getdatetimeFormatAsexpected(String datetimefromgrid) throws ParseException {
		splitDate(datetimefromgrid);
		splitTime();
		englishTime(dt3);
		joinDateAndTime();
		convertDateTimeFormat(k);
	}

	// Compare column value
	public void compareLastConnectedDate() {
		System.out.println("Comparing values");

		if (s1.equals(st2)) {
			System.out.println("Shown Correctly in Report");

		} else {
			System.out.println("Shown not Correctly in Report");
		}

	}

	// ***************************Harpinder test
	// cases**************************************************************************************

	// All coulmns should be enabled in device grid
	// Surelock , surefox and surevideo should be installed on device
	// SIM should be present in device for "IMSI"
	// Surelock should be launched in device for "Default Launcher Application"
	// column

	@FindBy(xpath = "//span[@id='deviceOwnerName']")
	public WebElement DeviceName;

	@FindBy(xpath = "//div[@class='content col-xs-6 col-md-6 dev_info_val'][1]")
	public WebElement DeviceModel;

	@FindBy(xpath = "//span[@id='deviceNotes']")
	public WebElement Notes;

	@FindBy(xpath = "//div[@class='content col-xs-6 col-md-6 dev_info_val'][2]")
	public WebElement OSVersion;

	@FindBy(xpath = "//div[@class='status_cont']//span[text()='Online']")
	public WebElement DeviceStatus;

	@FindBy(xpath = "//div[@class='content col-xs-6 col-md-6 vlu macAddr_valLine']")
	public WebElement MacAddress;

	@FindBy(xpath = "//div[@class='content col-xs-6 col-md-6 vlu'][5]")
	public WebElement CellSignal;

	@FindBy(xpath = "//td[@class='Battery']")
	public WebElement Battery;

	@FindBy(xpath = "//td[@class='DeviceGroupPath']")
	public WebElement DeviceGroupPath;

	@FindBy(xpath = "//td[@class='MemoryStorageAvailable']")
	public WebElement FreeStorageMemory;

	@FindBy(xpath = "//td[@class='PhysicalMemoryAvailable']")
	public WebElement FreeProgramMemory;

	@FindBy(xpath = "//div[@class='info_value infoBox_jobHistory_inprogress']")
	public WebElement InprogressJobs;

	@FindBy(xpath = "//div[@class='info_value infoBox_jobHistory_error']")
	public WebElement ErrorJobs;

	@FindBy(xpath = "//div[@class='info_value infoBox_jobHistory_success']")
	public WebElement SuccessJobs;

	@FindBy(xpath = "//div[@id='jobQueueButton']")
	public WebElement jobQueueButton;

	@FindBy(xpath = "//div[@id='device_jobQ_modal']//button[@aria-label='Close']")
	public WebElement CloseButton;

	@FindBy(xpath = "//a[@id='SureLockPermissionData']/../a")
	public WebElement SureLockPermissionShow;

	@FindBy(xpath = "//div[@id='SureLockPopupModal']//button[@class='close']")
	public WebElement ClosePop;

	@FindBy(xpath = "//input[@id='SureLockDefaultLauncher']")
	public WebElement SureLockDefaultLauncher;

	@FindBy(xpath = "//input[@id='SureLockAdmin']")
	public WebElement SureLockAdministrator;

	@FindBy(xpath = "//input[@id='SureLockUsageAccess']")
	public WebElement AppsWithUsageAccess;

	@FindBy(xpath = "//input[@id='SurelockKnox']")
	public WebElement SamsungKNOX;

	@FindBy(xpath = "//input[@id='UsbDebuggingDisabled']")
	public WebElement UsbDebuggingDisabled;

	@FindBy(xpath = "//div[@class='content col-xs-6 col-md-6 vlu'][7]")
	public WebElement IMSIValue;

	@FindBy(xpath = "//div[contains(text(),'IMSI')]")
	public WebElement IMSI;

	@FindBy(xpath = "//td[@class='DeviceRegistered']")
	public WebElement DeviceRegistered;

	// String dt,dt1 ,dt2 , dt3,a,b,c,d,e,f,g,h,last,i,j,output,k;
	public void getStringFromGridAndsplitDate(String columnName) {
		System.out.println("================Splitting date===================");
		String datetime = Initialization.driver.findElement(By.xpath("//td[@class='" + columnName + "']")).getText();
		String[] arrOfStr1 = datetime.split(" ");
		a = arrOfStr1[0];
		b = arrOfStr1[1];
		c = arrOfStr1[2];
		d = arrOfStr1[3];
		e = arrOfStr1[4];
		a = arrOfStr1[0];
		if (a.length() == 1) {
			a = "0" + a;

		} else {
			a = arrOfStr1[0];

		}
		dt = a + " " + b + " " + c;
		System.out.println(dt);
		dt2 = arrOfStr1[3];
	}

	public void splitTime() {
		System.out.println("================Splitting time===================");
		String[] arrOfStr2 = dt2.split(":");
		f = arrOfStr2[0];
		g = arrOfStr2[1];
		h = arrOfStr2[2];

		if (f.length() == 1) {
			f = "0" + f;
			dt3 = f + ":" + g + ":" + h + " " + e;

			System.out.println(dt3);
			// System.out.println(englishTime(dt3));
		} else {
			dt3 = f + ":" + g + ":" + h + " " + e;
			System.out.println(dt3);
			// System.out.println(englishTime(dt3));
		}
	}

	// Convert 24hr time format
	public String englishTime() throws ParseException {
		System.out.println("==============Converting 24hr time format================");
		// Format of the date defined in the input String
		DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss aa");

		// Change the pattern into 24 hour formate
		DateFormat formate = new SimpleDateFormat("HH:mm:ss");
		Date time = null;
		// String output = "";

		// Converting the input String to Date
		time = dateFormat.parse(dt3);

		// Changing the format of date
		// and storing it in
		// String
		output = formate.format(time);
		System.out.println(output);
		return output;

	}

	// Join time and date
	public void joinDateAndTime() {
		System.out.println("==============Join Date and time ================");
		k = dt + " " + output;
		System.out.println(k);
	}
	// Convert dta time format

	public void convertDateTimeFormat() throws ParseException {
		System.out.println("==============Convert date format as per report format ================");
		// date format 1
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");

		// date format 2
		DateTimeFormatter dateFormatterNew = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		// string to LocalDateTime
		LocalDateTime ldateTime = LocalDateTime.parse(k, dateFormatter);

		// System.out.println(dateFormatter.format(ldateTime));

		// change date format
		s1 = dateFormatterNew.format(ldateTime);

		System.out.println(s1);

	}

	public void getTimeStampFromReport(int i) throws InterruptedException {
		sleep(3);
		String datetime = Initialization.driver
				.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[" + i + "]/p")).getText();
		String[] arrOfStr = datetime.split("\\.", 2);
		s2 = arrOfStr[0];
		Reporter.log("Reading value from Report", true);
		System.out.println(s2);
	}

	public void readValueOfIMSIFromDeviceInfo() throws InterruptedException {
		try {
			if (IMSI.isDisplayed()) {
				s1 = IMSIValue.getText();
				Reporter.log("Read Value Of IMSI From Device Info", true);
				System.out.println(s1);
			}
		} catch (Exception e) {
			s1 = "Unknown";
			Reporter.log("IMSI is not present in grid", true);
			System.out.println(s1);
		}
	}
	
	@FindBy(xpath = ("//section[@id='report_viewSect']/div/div[2]/div[1]/div[2]/input[1]"))
	private WebElement ClickOnSearchReport;
	 public void SearchReport(String reportname) throws InterruptedException {
			ClickOnSearchReport.clear();
			sleep(2);
			ClickOnSearchReport.sendKeys(reportname);
//			sleep(3);
//			waitForXpathPresent("//p[@class='rpt_nme st-scrollTxt-left']");
		}

//		public void readingValueFromGrid() throws InterruptedException
//		{
//			sleep(3);
//		 a =Initialization.driver.findElements(By.xpath("//div[@id='SureLockPopupModal']//input")).get(0).getAttribute("value");
//		 System.out.println(a);
//		}
//		public void VerifyColumnValue() throws InterruptedException
//		{
//			sleep(3);
//			Reporter.log("Verify value in Report",true);
//			if(a.equals(s2))
//			{
//				System.out.println("ColumnName Is Shown Correctly in Report");
//				
//			}
//		else
//		{
//			ALib.AssertFailMethod("FAIL>>> ColumnName Is Not Shown Correctly in Report");
//		}
	// }

	public void clickOnSureLockPermissionShow() throws InterruptedException {
		waitForXpathPresent("//a[@id='SureLockPermissionData']/../a");
		if (SureLockPermissionShow.isDisplayed()) {
			SureLockPermissionShow.click();
			Reporter.log("Clicked on Surelock permission  button", true);
		} else {
			Reporter.log("SureLockPermission is not present in device info panel", true);
		}
	}

	public void closePopUp() throws InterruptedException {
		waitForXpathPresent("//div[@id='SureLockPopupModal']//button[@class='close']");
		ClosePop.click();
		Reporter.log("Close PopUp", true);
	}

	public void clickOnJobQueueButton() throws InterruptedException {
		waitForXpathPresent("//div[@id='jobQueueButton']");
		jobQueueButton.click();
		Reporter.log("Clicked on job queue button", true);
	}

	public void clickOnCloseButton() throws InterruptedException {
		waitForXpathPresent("//div[@id='device_jobQ_modal']//button[@aria-label='Close']");
		CloseButton.click();
		Reporter.log("Close the job queue screen", true);
	}

	// String s1 ,s2;
	public void ReadingValueInDeviceGrid(String column) throws InterruptedException {
		sleep(3);
		s1 = Initialization.driver.findElement(By.xpath("//td[@class='" + column + "']")).getText();
		Reporter.log("Reading value from Device Grid", true);
		System.out.println(s1);
	}

	public void ReadingValueInDeviceInfo(int i) throws InterruptedException {
		sleep(3);
		s1 = Initialization.driver.findElement(By.xpath("//div[@class='content col-xs-6 col-md-6 vlu'][" + i + "]"))
				.getText();
		Reporter.log("Reading value from Device Grid", true);
		System.out.println(s1);
	}

	public void ReadingValueInDeviceInfo(WebElement Value) throws InterruptedException {
		sleep(7);
		s1 = Value.getText();
		Reporter.log("Reading value from Device Grid", true);
		System.out.println(s1);
	}

	public void ReadingValueFromConfig(String Value) throws InterruptedException {
		sleep(3);
		s1 = Value;
		Reporter.log("Reading value from Config", true);
		System.out.println("'" + s1 + "'");
	}

	public void ReadingValueInReport(int i) throws InterruptedException {
		sleep(3);
		s2 = Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[" + i + "]/p"))
				.getText();
		Reporter.log("Reading value from Report", true);
		System.out.println(s2);
	}

	public void VerifyColumnInReport(String ColumnName) throws InterruptedException {
		sleep(3);
		Reporter.log("Verify value in Report", true);
		if (s1.equals(s2)) {
			System.out.println("'" + ColumnName + "' Is Shown Correctly in Report");

		} else {
			ALib.AssertFailMethod("FAIL>>> '" + ColumnName + "' Is Not Shown Correctly in Report");
		}
	}

	public void ReadingValueOfOSVersionInDeviceInfo() throws InterruptedException {
		sleep(3);
		String OS = OSVersion.getText();
		String a, b;
		if (OS.length() < 20) {
			String[] arrOfStr = OS.split("[Android]");
			s2 = arrOfStr[0];
			s1 = s2.substring(0, s2.length() - 1).trim();
			System.out.println(s1);
		} else {
			String[] arrOfStr = OS.split(" ", 3);
			a = arrOfStr[0];
			b = arrOfStr[1];
			s1 = a + " " + b;
			System.out.println(s1);
		}
		Reporter.log("Reading value from Device Grid", true);
	}

	public void ReadingValueOfOSTypeInDeviceInfo() throws InterruptedException {
		sleep(3);
		String OS = OSVersion.getText();
		try {
			String[] arrOfStr = OS.split(" ");
			for (int i = 0; i <= arrOfStr.length; i++) {
				if (arrOfStr[i].contains("[Android]")) {
					s2 = arrOfStr[i];
					s1 = s2.substring(1, s2.length() - 1);
					System.out.println(s1);
				}
			}
		} catch (Exception e) {
		}
	}

	public void ReadingValueInConsole(WebElement Value) throws InterruptedException {
		sleep(3);
		String column = Value.getText();
		String[] arrOfStr = column.split(" ", 2);
		s1 = arrOfStr[0];
		Reporter.log("Reading value from Device Grid", true);
		System.out.println(s1);
	}

	public void ReadingPercentageValueInDeviceGrid(WebElement Value) throws InterruptedException {
		System.out.println("Reading value from Device Grid");
		sleep(3);
		String battery = Value.getText();
		if (battery.contains("%")) {
			String[] arrOfStr = battery.split("%", 2);
			s1 = arrOfStr[0];
			System.out.println(s1);
		} else {
			s1 = Value.getText();
			System.out.println(s1);
		}
		Reporter.log("Reading value from Device Grid", true);
	}

	public void aa() {
		String datetime = Initialization.driver
				.findElement(By.xpath("//div[@id='SureLockPopupModal']//input[@id='SureLockDefaultLauncher']"))
				.getText();
		System.out.println(datetime);
	}

	public void ReadingValueOfGroupName() throws InterruptedException {
		sleep(3);
		String GroupName = DeviceGroupPath.getText();
		if (GroupName.contains("/")) {
			String[] arrOfStr = GroupName.split("/", 2);
			s1 = arrOfStr[1];

			System.out.println(s1);
		} else {
			s1 = DeviceGroupPath.getText();
			System.out.println(s1);
		}
		Reporter.log("Reading value from Device Grid", true);
	}

	// Madhu

	public void uncheck_CompRulesCheckBox() throws InterruptedException {
		List<WebElement> listOfCompCheckBox = Initialization.driver
				.findElements(By.xpath("(//*[@id='ComplianceReportCont'])[1]/div/span/input[@class='ct-selbox-in']"));
		for (WebElement compCheckBox : listOfCompCheckBox) {
			if (compCheckBox.isSelected()) {
				compCheckBox.click();
			} else if (!compCheckBox.isSelected()) {
				ALib.AssertFailMethod("FAIL >> checkbox is not enabled by default");
			}
		}
	}

//				Date date = new Date();
//				String D = date.toString();
//				String[] str = D.split(" ");
//				int t = Integer.parseInt(str[2]) - 0;
//				String s = Integer.toString(t);
//				String Date = s + " " + str[1] + "t" + " " + str[5];
//				boolean ActualText = CompliancePolicy_Status.get(i).getText().contains(Date);
//				String pass = "PASS >> Colunmdata2  is correct: " + ActualText + "";
//				String fail = "FAIL >> Colunmdata2 is not correct: " + ActualText + "";
//				ALib.AssertTrueMethod(ActualText, pass, fail);

	// Madhu

	public void ClickOnHomePage() throws InterruptedException {
		sleep(6);
		Home.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
		Reporter.log("Clicked On Home", true);
	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[2]/div[4]/span")
	private WebElement deviceCountOnGrid;
	int DeviceCountInGrid;

	public void GetDeviceCount() throws InterruptedException {
		String[] devicountOnlineOffline = deviceCountOnGrid.getText().split(",");
		DeviceCountInGrid = Integer.parseInt(devicountOnlineOffline[0].replaceAll("\\D", ""));
		System.out.println(DeviceCountInGrid);
	}

	public void ClickOnAllDevicesButton() throws InterruptedException {
		Initialization.driver.findElement(By.id("all_ava_devices")).click();
		sleep(3);
	}

	@FindBy(xpath = "(//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/ul/li)[last()]")
	private WebElement SelectLastPagination;

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
	private WebElement Pagination;

	public void SelectLastPagination() throws InterruptedException {
		if (Pagination.isDisplayed()) {
			Pagination.click();
			SelectLastPagination.click();
		}
	}

	int DeviceCountInAssetTracking;

	public void CompareDeviceCountInAsset() throws InterruptedException {

		String GetCount = Initialization.driver.findElement(By.xpath("(//span[@class='tableDevCount_line'])[1]"))
				.getText();
		System.out.println(GetCount);
		String Number = GetCount.substring(7);
		System.out.println(Number);
		SwitchBackWindow();
		ClickOnHomePage();
		ClickOnAllDevicesButton();
		SelectLastPagination();
		GetDeviceCount();

		if (Number.equals(DeviceCountInGrid)) {

			Reporter.log("Data is right", true);

		} else {
			ALib.AssertFailMethod("Fail");
		}
	}

	// Swati
	@FindBy(xpath = "//*[@id='reportList_tableCont']/section[1]/div[2]/div[1]/div[2]/input")
	private WebElement reportList_tableContSearchBar;

	public void SearchDeviceInReport(String Device) throws InterruptedException {

		reportList_tableContSearchBar.clear();
		sleep(2);
		reportList_tableContSearchBar.sendKeys(Device);
		sleep(2);

	}

	public void ClickOnComplianceReportReportOption() {
		ComplianceReportReportOption.click();
	}

	String ComplianceOptn;

	public void SelectComplianceOptns(String Option) throws InterruptedException {

		WebElement ComplianceOptn = Initialization.driver
				.findElement(By.xpath("(//*[@id='ComplianceReportCont'])[1]/div/span/input[@id='" + Option + "']"));
		ComplianceOptn.click();
		sleep(2);

	}

	public void VerifyComplianceDeviceNameReport(String DeviceName) throws InterruptedException {

		List<WebElement> DeviceName1 = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]/p"));

		if (DeviceName1.size() > 0) {
			for (int i = 0; i < DeviceName1.size(); i++) {
				String ActualValue = DeviceName1.get(i).getText();
				if (ActualValue.equals(DeviceName)) {
					Reporter.log("Pass >> DeviceName Is Diplayed Correctly", true);
				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> DeviceName Is Not Diplayed Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> DeviceName Column Is Not Diplayed Correctly");
		}
	}

	public void VerifyCompliancePolicy(String CompliancePolicy) throws InterruptedException {
		List<WebElement> CompliancePolicy_Status = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]/p"));
		if (CompliancePolicy_Status.size() > 0) {
			for (int i = 0; i < CompliancePolicy_Status.size(); i++) {
				String ActualValue = CompliancePolicy_Status.get(i).getText();
				if (ActualValue.equalsIgnoreCase(CompliancePolicy)) {
					Reporter.log("'" + CompliancePolicy + "'CompliancePolicy in row is correct", true);

				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> CompliancePolicy Is Not Diplayed Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> CompliancePolicy Column Is Not Diplayed Correctly");
		}
	}

	public void ComplianceStatusColumnVerification(String CompliancePolicy, String NonCompliance)
			throws InterruptedException {
		List<WebElement> CompliancePolicy_Status = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));
		if (CompliancePolicy_Status.size() > 0) {
			for (int i = 0; i < CompliancePolicy_Status.size(); i++) {
				String ActualValue = CompliancePolicy_Status.get(i).getText();
				if (ActualValue.equalsIgnoreCase(CompliancePolicy) || ActualValue.equalsIgnoreCase(NonCompliance)) {
					Reporter.log("CompliancePolicy_Status  in row is correct", true);

				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("Fail >> CompliancePolicy_Status Is Not Diplayed Correctly");

				}
			}
		} else {
			SwitchBackWindow();
			ALib.AssertFailMethod("Fail >> CompliancePolicy_Status Column Is Not Diplayed Correctly");
		}
	}

	public void LastConnectedDate() {
		List<WebElement> LastConnected = Initialization.driver
				.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[4]/p"));
		if (LastConnected.size() == 0) {
			ALib.AssertFailMethod("Reports LastConnected Is Empty");
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String todate = dateFormat.format(date);
		System.out.println("Actual date is:" + todate + " ");

		boolean value;
		for (int i = 0; i < LastConnected.size(); i++) {
			String ActualValue = LastConnected.get(i).getText();
			String hello = ActualValue.substring(0, 10);
			value = hello.equals(todate);
			if (value == true) {
				String PassMessage = "PASS >> Report get generated successfully with LastConnected";
				System.out.println(PassMessage);
			} else {
				String FailMessage = "FAIL >> Report not generated successfully with LastConnected";
				ALib.AssertFailMethod(FailMessage);
			}
		}

//			Date date = new Date();
//			String D = date.toString();
//			String[] str = D.split(" ");
//			int t = Integer.parseInt(str[2]) - 0;
//			String s = Integer.toString(t);
//			String Date = s + " " + str[1] + "t" + " " + str[5];
//			boolean ActualText = CompliancePolicy_Status.get(i).getText().contains(Date);
//			String pass = "PASS >> Colunmdata2  is correct: " + ActualText + "";
//			String fail = "FAIL >> Colunmdata2 is not correct: " + ActualText + "";
//			ALib.AssertTrueMethod(ActualText, pass, fail);

	}

	// Madhu
	public void verifyDeviceRoamingColInDevHistoryRep(String deviceroaming) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[11]"));
		String PassStatement = "PASS >> Device Roaming in device history report is correct";
		String FailStatement = "Fail >> Device Roaming in device history report row is wrong";
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(deviceroaming);
			if (Expectedvalue == false) {
				String FailMessage = "Device Roaming in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				ALib.AssertTrueMethod(Expectedvalue, PassStatement, FailStatement);
			}
		}
	}

	@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[13]/p")
	private WebElement gpuUsage;

	public void verifyGPUUsageInDevHistory() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[13]"));
		String PassStatement = "PASS >> GPU Usage device history report is correct";
		String FailStatement = "Fail >> GPU Usage device history report is not correct";
		for (WebElement we1 : ls) {
			String str = we1.getText();
			int num = Integer.parseInt(str);
			if (num >= 0) {
				Reporter.log(PassStatement, true);
			} else {
				Reporter.log(FailStatement, true);
			}
		}
	}

	@FindBy(xpath = "//span[text()='Compliance Report']")
	private WebElement complianceReport;

	public void clickOnComplianceReport() throws InterruptedException {
		complianceReport.click();
		sleep(3);
	}

	public void compliancePolicyColVerificationComplianceReport(String CompliancePolicy) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[2]/p"));

		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(CompliancePolicy);
			if (Expectedvalue == false) {
				String FailMessage = "FAIL >> Compliance Policy in compliance report is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				System.out.println("PASS >> Compliance Policy in compliance report is correct");
			}

		}

	}

	public void statusVerificationInComplianceReports(String status) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[3]/p"));// *[@id="devicetable"]/tbody/tr/td[3]/p
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			boolean Expectedvalue = ActualValue.equals(status);
			if (Expectedvalue == false) {
				String FailMessage = "FAIL >> Status in Compliance report in row is wrong";
				ALib.AssertFailMethod(FailMessage);
			} else {
				System.out.println("PASS >> Status in compliance report is correct");
			}

		}
	}
	
	//Harpinder 
	String originalHandle2;
	public void WindowHandle2() throws InterruptedException
	{
		originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		sleep(10);
	}

	public void SwitchBackWindow2() throws InterruptedException
	{

    //  Initialization.driver.close();
		Initialization.driver.switchTo().window(originalHandle);
		sleep(10); 
	}

}
