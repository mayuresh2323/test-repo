package PageObjectRepository;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class NixSignUpMailValidationPOM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib(); 
	

	@FindBy(id="m")
	private WebElement Textbox_EmailField;
	
	@FindBy(id="submitinbox")
	private WebElement CheckInboxButton;
	
	@FindBy(xpath="//a[text()='Verify your email address']")
	private WebElement verifyYourEmailAddress;
	

	/*public void EnterEmailId() throws InterruptedException
	{ 
		  pagemaximize();
		  System.out.println("Entering EmaiID of dispostable");
		  Textbox_EmailField.sendKeys(Config.EnterEmailID);
		  Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  sleep(2);
	}*/
	public void ClickOnCheckInboxButton() throws InterruptedException
	{
		CheckInboxButton.click();
		waitForidPresent("logo");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(5);
		System.out.println("Logged in to dispostable successfully");
	}
	
	
	
	
}
