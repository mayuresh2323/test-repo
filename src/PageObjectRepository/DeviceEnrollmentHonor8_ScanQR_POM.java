package PageObjectRepository;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;

public class DeviceEnrollmentHonor8_ScanQR_POM extends WebDriverCommonLib
{
	AssertLib ALib=new AssertLib();
	CommonMethods_POM common=new CommonMethods_POM();
	
	@FindBy(id="deletedSection")
    private WebElement PendingDeleteButton;
	
	@FindBy(id="generateQRCode")
	private WebElement ClickOnDeviceEnrollement;
	
	@FindBy(xpath="//a[text()='Device Enrollment Rules']")
	private WebElement DeviceEnrollmentRulesButton;
	
	@FindBy(xpath="//input[@id='deviceregauth']")
	private WebElement AdvancedDeviceAuthenticationCheckBox;
	
	@FindBy(xpath="//select[@id='device_Authenticate']")
	private WebElement DeviceAuthenticationType;
	
	@FindBy(xpath="//input[@id='device_password']")
	private WebElement passwordTetField;
	
	@FindBy(xpath="//input[@id='device_enrollement_apply']")
	private WebElement DeviceEnrollmentRulesAppliyButton;
	
	@FindBy(xpath="//span[text()='Settings updated successfully.']")
	private WebElement EnrollmentRulesUpdateMessageNotification;
	
	@FindBy(xpath="//input[@id='use_password_with_qr']")
	private WebElement IncludePasswordWithQRCodeCheckBox;
	
	@FindBy(xpath="//input[@id='webapp_ad_auth_endpoint']")
	private WebElement AuthEndpointTextField;
	
	@FindBy(xpath="//input[@id='webapp_ad_token_endpoint']")
	private WebElement TokenEndpoinTextField;
	
	@FindBy(xpath="//input[@id='webapp_oauth_client_id']")
	private WebElement ClientIDTextField;
	
	@FindBy(xpath="//button[@id='webApp_GenerateClientId']")
	private WebElement ClientID_GenerateButton;
	
	@FindBy(xpath="//input[@id='ClientSecret']")
	private WebElement ClientSecretEMMTextField;
	
	@FindBy(xpath="//a[text()='Native Application']")
	private WebElement NativeApplicationTab;
	
	@FindBy(xpath="//input[@id='ad_auth_endpoint']")
	private WebElement AuthEndpointTextField_NativeAppTab;
	
	@FindBy(xpath="//select[@id='oauth_type_id']")
	private WebElement OAuthTypeDropDown_NativeAppTab;
	
	@FindBy(xpath="//input[@id='ad_token_endpoint']")
	private WebElement TokenEndpointTextField_NativeAppTab;
	
	@FindBy(xpath="//input[@id='oauth_client_id']")
	private WebElement ClientIDTextField_NativeAppTab;
	
	@FindBy(xpath="//button[text()='Generate']")
	private WebElement ClientID_GenerateButton__NativeAppTab;
	
	@FindBy(xpath="//a[text()='Web Application']")
	private WebElement WebApplicationtab;
	
	
	public void ClickOnDeviceEnrollment() throws InterruptedException
	{
		ClickOnDeviceEnrollement.click();
		waitForidPresent("qrCodeImg_default");
		sleep(2);
	}
     public void ClickOnGetStartedButton() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Get Started']").click();
    	 sleep(4);
     }
     public void ClickOnProceedButton() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='PROCEED' and @index='1']").click();
         sleep(6);
     }
     public void ClickOnActivateButton() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='ACTIVATE' and @index='1']").click();
    	 sleep(5);
     }
     public void ClickOnContinueButton() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Continue' and @index='3']").click();
    	 sleep(3);
     }
     public void SendingAccountID(String AccountID) throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByClassName("android.widget.EditText").sendKeys(AccountID);
    	 sleep(2);
    	
     }
     public void ClickOnRegisterButton() throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 25);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Register' and @index='2']").click();
    	 sleep(5);
    	 if(Config.AccountId.equals("1"))
    	 {
    		 Initialization.driverAppium.findElement(By.xpath("//android.widget.EditText[@text='Enter Server Path']")).sendKeys(Config.ServerPath);
    		 sleep(2);
    		 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Server Path']").click();
    		 sleep(4);
    		 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Continue...' and @index='3']").click();
    		 sleep(2);
    	 }
    	 
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.RadioButton[@text='Set Device Name manually' and @index='0']")));
     } 
    	 
    	
     public void SelectingDeviceNametype() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.RadioButton[@text='Set Device Name manually' and @index='0']").click();
    	 sleep(5);
     }
     public void SendingDeviceName(String DeviceName) throws InterruptedException, IOException
     {
    	 SelectingDeviceNametype();
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[contains (@resource-id,'deviceName')]").sendKeys(DeviceName);
    	 Runtime.getRuntime().exec("adb shell input keyevent 4");
    	 sleep(5);
    	 ClickOnSetDeviceName();
     }
     public void ClickOnSetDeviceName() throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 60);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Device Name']").click();
    	 sleep(20);
     }
     public void ClikOnNixSettings() throws IOException, InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").click();
    	System.out.println("Clicked On Settings");
     }
     public void ClickingOnImportExportSettings(String Text) throws InterruptedException
     {
    	 common.commonScrollMethod(Text);
    	 common.ClickOnAppiumText(Text);
     }
     
     public void ClickingOnResetSettings() throws InterruptedException
     {
    
    	Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Reset Settings' and @index='0']").click();
    	sleep(2);
    	Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='RESET']").click();
    	sleep(4);
     }

     public void ClickOnDoneInNix() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains (@resource-id,'main_done_button')]").click();
    	 sleep(3);
    	 System.out.println("Clicked On Done Button");
     }
     public void  DelrtingDeviceFromPendingDelete(String Device) throws InterruptedException
     {
    	 PendingDeleteButton.click();
    	 waitForVisibilityOf("//div[@id='ddRefreshBtn']");
    	 sleep(3);
    	 Initialization.driver.findElement(By.xpath("//div[@id='ddRefreshBtn']")).click();
    	 sleep(4);
    	 Initialization.driver.findElement(By.xpath("//div[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).clear();
    	 sleep(2);                                   
    	 Initialization.driver.findElement(By.xpath("//div[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).sendKeys(Device);
    	 sleep(3);
    	 try
    	 {
    	      Initialization.driver.findElement(By.xpath("//td[text()='"+Device+"']")).isDisplayed();    		 
    		  Initialization.driver.findElement(By.xpath("//td[text()='"+Device+"']")).click();
    		  Initialization.driver.findElement(By.xpath("//div[@id='ddDeleteBtn']")).click();
    		  sleep(4);
    	      Reporter.log(">>>>>>>Clicked On Force Delete Button",true);
    	      Initialization.driver.findElement(By.xpath("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")).click();
    	      Reporter.log(">>>>>>>Clicked On Yes",true);
    	      while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed())
    	    	 {}
    	      sleep(3);
    	    	 Initialization.driver.findElement(By.xpath("(//span[@class='icon node-icon homeIcn glyphicon glyphicon-home'])[3]")).click();
    	    	 Reporter.log("Clicking on Home Group",true);
    	 
    	 }
    	 catch (Exception e)
    	 {
    		 Initialization.driver.findElement(By.xpath("(//span[@class='icon node-icon homeIcn glyphicon glyphicon-home'])[3]")).click();
	    	 Reporter.log("Clicking on Home Group",true);
    	 }
     }
     public void ClickOnScanQRButton() throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Scan QR' and @index='3']").click();
    	 sleep(25);
     }
     
     public void clicOnGridRefresh() throws InterruptedException
     {
    	 Initialization.driver.findElement(By.xpath("//div[@id='deviceGrid_info_header']/following-sibling::div[1]/div/div/button[@name='refresh']")).click();
    	 waitForidPresent("deleteDeviceBtn");
    	 sleep(4);
     }
     public void ClickOnDeviceEnrollmentRules() throws InterruptedException
     {
    	 DeviceEnrollmentRulesButton.click();
    	 waitForXpathPresent("//p[text()='Enrollment Authentication']");
    	 sleep(3);
     }
     public void CheckingAdvancedDeviceAuthentication() throws InterruptedException
     {
    	 boolean flag=AdvancedDeviceAuthenticationCheckBox.isSelected();
        if(flag==true)
        {
        	Reporter.log("AdvanceDeviceAuthentication Check Box Is Already Checked",true);
        }
        else
        {
        	AdvancedDeviceAuthenticationCheckBox.click();
        	Reporter.log("AdvanceDeviceAuthentication Check Box Is Checked",true);
        }
        sleep(4);
     }
     public void UncheckingAdvancedDeviceAuthentication() throws InterruptedException
     {
    	 boolean flag=AdvancedDeviceAuthenticationCheckBox.isSelected();
    	 if(flag==true)
         {
    		 AdvancedDeviceAuthenticationCheckBox.click();
         	Reporter.log("AdvanceDeviceAuthentication Check Box Is UnChecked",true);
         }
    	 else
         {
    		 Reporter.log("AdvanceDeviceAuthentication Check Box Is Already UnChecked",true);
         }
         sleep(4);
     }
        
     public void SettingDeviceAuthenticationType_RequirePassword() throws InterruptedException
     {
    	 Select sel=new Select(DeviceAuthenticationType);
    	 sel.selectByValue("1");
    	 sleep(3);
     }
     public void SettingPassword(String Password) throws InterruptedException
     {
    	 passwordTetField.clear();
    	 passwordTetField.sendKeys(Password);
    	 sleep(3);
     }
     public void ClickOnApplyButton() throws InterruptedException
     {
    	 DeviceEnrollmentRulesAppliyButton.click();
    	 waitForXpathPresent("//span[text()='Settings updated successfully.']");
      	 sleep(3);
    	 Reporter.log("Clicked on Apply Button",true);
     }
     public void SendPasswordForAuthentication_ADAChecked(String Password) throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,60);
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Enroll Device']")));
    	 sleep(4);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[contains(@resource-id,'password')]").sendKeys(Password);
    	 sleep(3);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Enroll Device']").click();
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains (@resource-id,'connectionStatus')]")));
    	 sleep(15);
     }
     public void SendPasswordForAuthentication_ADAUnchecked(String Password) throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,60);
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='OK' and @index='0']")));
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[contains(@resource-id,'enterPassword')]").sendKeys(Password);
    	 sleep(3);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK' and @index='0']").click();
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains (@resource-id,'connectionStatus')]")));
    	 sleep(60);
     }
     public void EnablingIncludingPasswordwithQR() throws InterruptedException
     {
               boolean flag=IncludePasswordWithQRCodeCheckBox.isSelected();
               if(flag==true)
               {
            	   Reporter.log("Include Password With QRCode CheckBox is Already Checked",true);
               }
               else
               {
            	   IncludePasswordWithQRCodeCheckBox.click();
            	   Reporter.log("Include Password With QRCode CheckBox is Checked",true);
               }
               sleep(3);
     }
     public void DisablingIncludePasswordWithQR() throws InterruptedException
     {
    	 boolean flag=IncludePasswordWithQRCodeCheckBox.isSelected();
         if(flag==true)
         {
        	 IncludePasswordWithQRCodeCheckBox.click();
        	 Reporter.log("Include Password With QRCode CheckBox is UnChecked",true);
         }
         else
         {
      	 Reporter.log("Include Password With QRCode CheckBox is Already UnChecked",true);
         }
         sleep(3);
     }
     public void SettingDeviceAuthenticationType_NoAuthentication() throws InterruptedException
     {
    	 Select sel=new Select(DeviceAuthenticationType);
    	 sel.selectByValue("0");
    	 sleep(3);
     }
     public void SettingDeviceAuthenticationType_OAuthAuthentication() throws InterruptedException
     {    sleep(2);
    	 Select sel=new Select(DeviceAuthenticationType);
    	 sel.selectByValue("2");
    	 sleep(3);
     }
     
     public void VerifyOAthUIByEnablingADA()
     {
    	 WebElement [] OAthWebelements= {AuthEndpointTextField,TokenEndpoinTextField,ClientIDTextField,ClientID_GenerateButton,ClientSecretEMMTextField};
    	 String [] text= {"AuthEndpointTextField","TokenEndpoinTextField","ClientIDTextField","ClientID_GenerateButton","ClientSecretEMMTextField"};
    	 for(int i=0;i<=OAthWebelements.length-1;i++)
    	 {
    		 boolean flag = OAthWebelements[i].isDisplayed();
    		 if(flag==true)
    		 {
    			 flag=true;
    		 }
    		 else
    		 {
    			 flag=false;
    		 }
    		 String PassStatement=">>PASS"+" "+text[i]+"Is Displayed When Advance Device Authentication Is Enabled";
    		 String FailStatement=">>FAIL"+" "+text[i]+"Is Not Displayed When Advance Device Authentication Is Enabled";
    		 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
    	 }
     }
     public void SendingAuthEndpoint(String AuthEndpoint)
     {
    	 AuthEndpointTextField.clear();
    	 AuthEndpointTextField.sendKeys(AuthEndpoint);
     }
     public void SendingTokenEndpoint(String TokenEndpoint)
     {
    	 TokenEndpoinTextField.clear();
    	 TokenEndpoinTextField.sendKeys(TokenEndpoint);
     }
     public void SendingClientID(String ClientID)
     {
    	 ClientIDTextField.clear();
    	 ClientIDTextField.sendKeys(ClientID);
     }
     public void SendingClientSecret(String ClientSecret)
     {
    	 ClientSecretEMMTextField.clear();
    	 ClientSecretEMMTextField.sendKeys(ClientSecret);
     }
     public void ClickOnSetDeviceNameForOAuth_Gsuite() throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Device Name']").click();
    //	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@text='' and @index='2']")));
    	 sleep(20);
     }
     public void SendingEmailIDForGsuite(String GsuiteEmailID) throws InterruptedException, IOException
     { 	
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='0']").sendKeys(GsuiteEmailID);
   // 	 Initialization.driverAppium.findElementById("Email").sendKeys(GsuiteEmailID);
    	 sleep(3);
     }
    	 
     public void ClickingOnNextButton_GSuite() throws InterruptedException 
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Next' and @index='0']").click();
    	 sleep(20);
     }
     public void SendingPasswordForGsuite(String GsuitePassword) throws InterruptedException
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='' and @index='0']").sendKeys(GsuitePassword);
    	 sleep(3);
     }
  /*  public void SendingEmailIDForGsuite(String GsuiteEmailID,String GsuitePassword) throws InterruptedException
     { 	
    	 try
    	 {
    		 System.out.println("TRY1");
    	 Initialization.driverAppium.findElementByXPath("//android.view.View[@index='2']").sendKeys(GsuiteEmailID);
    	 sleep(3);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Next' and @index='0']").click();
    	 sleep(15);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='' and @index='0']").sendKeys(GsuitePassword);
    	 sleep(3);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Next' and @index='0']").click();
    	 System.out.println("TRY2");
    	 }
    	 catch(Exception e)
    	 {
    		 System.out.println("Catch1");
    		 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 60);
    		 Initialization.driverAppium.findElementByXPath("//android.view.View[@text='42gears test 42test2018@gmail.com']").click();
    		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Online' and @index='1']")));
    	 }
    	 
    	 System.out.println("Hello");
    	 
     }*/
     
     @FindBy(xpath="//td[@class='DeviceUserName']")
     private WebElement DeviceUserNameColumn;
     
     
     public void VerifyingEnrolledDeviceUserNameInConsole(String UserName)
     {
    	 boolean flag;
    	 String UserName_Console = DeviceUserNameColumn.getText();
    	 if(UserName_Console.equals(UserName))
    	 {
    		 flag=true;
    	 }
    	 else
    	 {
    		 flag=false;
    	 }
    	 String Pass="PASS >>> Device Enrolled Successfully Through OAuth(GSuite) And Device Name Is Same As Expected"+" "+UserName_Console;
    	 String Fail="FAIL >>> Device Name Is Different From The Expected Name"+" "+UserName_Console;
    	 ALib.AssertTrueMethod(flag, Pass, Fail);
     }
     public void SendingEmailIDForAZURE_AD(String MailID) throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,45);
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("logonIdentifier")));
    	 sleep(4);
    	 Initialization.driverAppium.findElementById("logonIdentifier").sendKeys(MailID);
    	 sleep(5);
     }
     public void SendingEmailPasswordForAZURE_AD(String Password) throws InterruptedException
     {
    	 Initialization.driverAppium.findElementById("password").sendKeys(Password);
    	 sleep(4);
     }
     public void ClickOnSignIn() throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 60);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Sign in']").click();
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Online' and @index='1']")));
    	 sleep(3);
     }
     
     
     public void VerifyNativeAppTabUI()
     {
    	 boolean flag=NativeApplicationTab.isDisplayed();
    	 if(flag==true)
    	 {
    		 flag=true;
    	 }
    	 else
    	 {
    		 flag=false;
    	 }
    	 String Pass="PASS >>> Native Application Tab Is Opened When ADA Is Disabled And Authentication Type Set to OAuth";
    	 String Fail="FAIL >>> Native Application Tab Is Not Opened When ADA Is Disabled And Authentication Type Set to OAuth";
         ALib.AssertTrueMethod(flag, Pass, Fail);
         
         WebElement[] NativeAppTabWelelements= {AuthEndpointTextField_NativeAppTab,OAuthTypeDropDown_NativeAppTab,TokenEndpointTextField_NativeAppTab,ClientIDTextField_NativeAppTab,ClientID_GenerateButton__NativeAppTab};
         String[] Text= {"AuthEndpoint","OAuthTypeDropDown","TokenEndpoint","ClientID","GenerateButton"};
         for(int i=0;i<=NativeAppTabWelelements.length-1;i++)
         {
        	 boolean Var=NativeAppTabWelelements[i].isDisplayed();
        	 if(Var==true)
        	 {
        		 Var=true;
        	 }
        	 else
        	 {
        		 Var=false;
        	 }
        	 String PassStatement=">>PASS"+" "+Text[i]+"Is Displayed When Advance Device Authentication Is Disbled";
    		 String FailStatement=">>FAIL"+" "+Text[i]+"Is Not Displayed When Advance Device Authentication Is Disbled";
    		 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
         }
         
     }
     public void ClickingOnWebapplicationTab() throws InterruptedException
     {
    	 WebApplicationtab.click();
    	 sleep(3);
     }
     public void VerifyOAthUIByDisablingADA()
     {
    	 WebElement [] OAthWebelements= {AuthEndpointTextField,TokenEndpoinTextField,ClientIDTextField,ClientID_GenerateButton,ClientSecretEMMTextField};
    	 String [] text= {"AuthEndpointTextField","TokenEndpoinTextField","ClientIDTextField","ClientID_GenerateButton","ClientSecretEMMTextField"};
    	 for(int i=0;i<=OAthWebelements.length-1;i++)
    	 {
    		 boolean flag = OAthWebelements[i].isDisplayed();
    		 if(flag==true)
    		 {
    			 flag=true;
    		 }
    		 else
    		 {
    			 flag=false;
    		 }
    		 String PassStatement=">>PASS"+" "+text[i]+"Is Displayed When Advance Device Authentication Is Disabled";
    		 String FailStatement=">>FAIL"+" "+text[i]+"Is Not Displayed When Advance Device Authentication Is Disabled";
    		 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
    	 }
     }
     public void SettingOAuthType_NativeappTab(String Value,String OAthType) throws InterruptedException
     {
    	 Select sel=new Select(OAuthTypeDropDown_NativeAppTab);
    	 sel.selectByValue(Value);
    	 Reporter.log("Setting OAuth Type In Native Application Tab As"+" : "+OAthType,true);
    	 sleep(3);
     }
     public void SendingAuthEndpoint_NativeApp(String AuthEndpoint_NativeApp)
     {
    	 AuthEndpointTextField_NativeAppTab.clear();
    	 AuthEndpointTextField_NativeAppTab.sendKeys(AuthEndpoint_NativeApp);
     }
     public void SendingTokenEndPoint_NativeApp(String TokenEndPoint_NativeApp)
     {
    	 TokenEndpointTextField_NativeAppTab.clear();
    	 TokenEndpointTextField_NativeAppTab.sendKeys(TokenEndPoint_NativeApp);
     }
     public void SendingClientID_NativeApp(String ClientID_NativeApp)
     {
    	 ClientIDTextField_NativeAppTab.clear();
    	 ClientIDTextField_NativeAppTab.sendKeys(ClientID_NativeApp);
     }
     public void CliCkOnRegisterButton_DisableADA()
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Register' and @index='2']").click();
    	 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Authorize Me']")));
     }
     public void ClickOnAuthorizeMeButton() throws InterruptedException
     {
    	 WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Authorize Me']").click();
    	 sleep(15);
     }
     
     
     // Enrollment DesignChange to generate qr code
     
     @FindBy(xpath="//a[.='List View']")
 	private WebElement ListViewButton;
 	
 	@FindBy(xpath="//a[.='Default View']")
 	private WebElement DefaultViewButton;
 	
 	@FindBy(id="create_QREnroll_Setup")
 	private WebElement CreateQRCodeButton;
 	
 	@FindBy(id="setDefault_QREnroll")
 	private WebElement QrCode_SetAsDefaultButton;
 	
 	@FindBy(id="name_QRenroll")
 	private WebElement QRCodeNameTextBox;
 	
 	@FindBy(xpath="(//button[.='Next'])[2]")
 	private WebElement QrCode_NextButton;
 	
 	@FindBy(xpath="(//button[.='Next'])[2]/following-sibling::button")
 	private WebElement QrCode_SaveButton;
 	
 	@FindBy(xpath="//div[@id='qrCodeEnrollment']/div[3]/div[1]/div[3]/div[1]/div[1]/div/input")
 	private WebElement QrCode_SearchField;
 	
 	@FindBy(id="getStarted_QREnroll_Setup")
 	private WebElement QrCode_GetStartedButton;
 	
 	@FindBy(id="enrollmentSection")
 	private WebElement EnrollmentButton;
 	
 	
 	public void ClickOnEnrollment() throws InterruptedException
 	{
 		EnrollmentButton.click();
 		waitForXpathPresent("//span[.='QR Code Enrollment']");
 		sleep(3);
 	}
 	public void ClickOnCreateQRCodeButton() throws InterruptedException
 	{
 		CreateQRCodeButton.click();
 		waitForXpathPresent("//h4[.='Setup QR Code for Enrollment']");
 		sleep(2);
 	}
 	public void CreatingQRCode(String QRCodeName) throws InterruptedException
 	{
 		QRCodeNameTextBox.sendKeys(QRCodeName);
 		sleep(3);
 		QrCode_NextButton.click();
 		sleep(3);
 //		Initialization.driver.findElement(By.xpath("//div[@id='selGroup_QRenroll']/ul/li[text()='Home']")).click();
// 		sleep(2);
 		QrCode_NextButton.click();
 		sleep(3);
 		QrCode_SaveButton.click();
 		waitForXpathPresent("//span[.='QR code saved successfully']");
 		sleep(3);
 	}
 	
 	public void SettingQRCodeAsDefault(String QRCodeName) throws InterruptedException
 	{
 		QrCode_SearchField.sendKeys(QRCodeName);
 		waitForXpathPresent("//table[@id='qrCodeEnrollmentTable']/tbody/tr/td[.='"+QRCodeName+"']");
 		sleep(2);
 		Initialization.driver.findElement(By.xpath("//table[@id='qrCodeEnrollmentTable']/tbody/tr/td[.='"+QRCodeName+"']")).click();
 		sleep(2);
 		QrCode_SetAsDefaultButton.click();
 		waitForXpathPresent("//span[.='Changed default QR Code']");
 		sleep(2);
 	}
 	public void OpeningQRCode(String QRCodeName) throws InterruptedException
 	{
 		/*try
 		{*/
 			ListViewButton.isDisplayed();
 			sleep(3);
 			ListViewButton.click();
 			waitForidPresent("create_QREnroll_Setup");
 			ClickOnCreateQRCodeButton();
 			CreatingQRCode(QRCodeName);
 			SettingQRCodeAsDefault(QRCodeName);
 		/*}*/
 		
 		/*catch (Exception e) 
 		{
 			QrCode_GetStartedButton.click();
 			sleep(3);
 			CreatingQRCode(QRCodeName);
 		}*/
 	}
 	
     
     
     
     
     
     
     
     
     
     
}
