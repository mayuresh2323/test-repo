package PageObjectRepository;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.Helper;
import Library.WebDriverCommonLib;

public class LoginLogoutPOM extends WebDriverCommonLib{
	
	@FindBy(id="uName")
	private WebElement userNAmeEdt;
	
	@FindBy(id="pass")
	private WebElement passwordEdt;
	
	@FindBy(id="loginBtn")
	private WebElement loginBtn;
	
	@FindBy(id="userProfileButton")
	private WebElement settings;
	
	@FindBy(id="logoutButton")
	private WebElement logoutBtn;
	
	@FindBy(xpath="//div[@id='warningMsg_dropdown']")
	private WebElement trailMessage; 
	
	@FindBy(id="okbtn123")
	private WebElement AcceptBtn;
	
	//For NixSignUp
	@FindBy(id="m")
	private WebElement Textbox_EmailField;
	
	//for unit testing
	@FindBy(id="okbtn123")
	private WebElement LicenseAcceptButton;
	
	@FindBy(xpath="//div[@class='processStepHolder']/div[1]/a")
	private WebElement SureMDMSubscriptionCloseButton;
	
	AssertLib ALib=new AssertLib();
	
//	  public void loginToAPP(String username , String password, String url) throws InterruptedException{
//		Driver.driver.get(url);
//				
//		pagemaximize();
//		System.out.println("Entering UN PWD");
//		Helper.highLightElement(Initialization.driver, userNAmeEdt);
//		userNAmeEdt.sendKeys(username);
//		Helper.highLightElement(Initialization.driver, passwordEdt);
//		passwordEdt.sendKeys(password);
//		Helper.highLightElement(Initialization.driver, loginBtn);
//		loginBtn.click();
//		waitForidPresent("deleteDeviceBtn");
//		sleep(5);
//		System.out.println("Logged in to SureMDM successfully");
//		
//		try
//		{
//			Initialization.driver.findElement(By.xpath("//p[contains(text(),'SureMDM Trial account')]")).isDisplayed();
//			Initialization.driver.findElement(By.xpath("//i[@class='icn warningIcn fa fa-exclamation-triangle']")).click();
//			sleep(2);
//			
//		}
//		catch(Exception e)
//		{
//			System.out.println(" ");
//		}
//		if(Initialization.driver.findElement(By.xpath("(//p[@class='messLine'])[1]")).isDisplayed())
//		{
//		Initialization.driver.findElement(By.xpath("//a[text()='Dashboard']")).click();
//		waitForidPresent("osplatforms");
//		Reporter.log("Clicking On DashBoard, Navigated to DashBoard Page", true);
//		sleep(5);
//		Reporter.log("PASS >> Verification of clicking on Dashboard", true);
//		Initialization.driver.findElement(By.xpath("//a[text()='Home']")).click();
//		waitForidPresent("deleteDeviceBtn");
//		sleep(6);
//		
//		}
//	}
///////////////////////////	
	public void loginToAPP(String url) throws InterruptedException{
		Driver.driver.get(url);
		pagemaximize();	
//		pagemaximize();
//		System.out.println("Entering UN PWD");
//		Helper.highLightElement(Initialization.driver, userNAmeEdt);
//		userNAmeEdt.sendKeys(username);
//		Helper.highLightElement(Initialization.driver, passwordEdt);
//		passwordEdt.sendKeys(password);
//		Helper.highLightElement(Initialization.driver, loginBtn);
//		loginBtn.click();
//		waitForidPresent("deleteDeviceBtn");
//		sleep(5);
//		System.out.println("Logged in to SureMDM successfully");
		
//		try
//		{
//			Initialization.driver.findElement(By.xpath("//p[contains(text(),'SureMDM Trial account')]")).isDisplayed();
//			Initialization.driver.findElement(By.xpath("//i[@class='icn warningIcn fa fa-exclamation-triangle']")).click();
//			sleep(2);
//			
//		}
//		catch(Exception e)
//		{
//			System.out.println(" ");
//		}
//		if(Initialization.driver.findElement(By.xpath("(//p[@class='messLine'])[1]")).isDisplayed())
//		{
//		Initialization.driver.findElement(By.xpath("//a[text()='Dashboard']")).click();
//		waitForidPresent("osplatforms");
//		Reporter.log("Clicking On DashBoard, Navigated to DashBoard Page", true);
//		sleep(5);
//		Reporter.log("PASS >> Verification of clicking on Dashboard", true);
//		Initialization.driver.findElement(By.xpath("//a[text()='Home']")).click();
//		waitForidPresent("deleteDeviceBtn");
//		sleep(6);
//		
//		}
	}
	
/////////////////////////////	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void login(String username , String password) throws InterruptedException
	{
		userNAmeEdt.clear();
		sleep(2);
		userNAmeEdt.sendKeys(username);
		sleep(2);
		passwordEdt.sendKeys(password);
		sleep(2);
		loginBtn.click();
		waitForidPresent("deleteDeviceBtn");
		Reporter.log("Logged Into suremdm Successfully");
		sleep(5);
		
	}
	
	//for unit testing
	public void loginWithNewUser(String username , String password) throws InterruptedException{
		userNAmeEdt.sendKeys(username);
		passwordEdt.sendKeys(password);
		loginBtn.click();
		waitForidPresent("okbtn123");
		sleep(2);
		boolean value1=LicenseAcceptButton.isDisplayed();
		String PassStatement1="PASS >>'Licence agreement page displayed'";
		String FailStatement1="FAIL >> 'Licence agreement page displayed -NOT displayed";
		ALib.AssertTrueMethod(value1, PassStatement1, FailStatement1);
		sleep(4);
		LicenseAcceptButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("PASS >> Logged in to SureMDM with New User successfully");
	}
	
	public void loginError(String username , String password) throws InterruptedException{
		userNAmeEdt.sendKeys(username);
		passwordEdt.sendKeys(password);
		loginBtn.click();
		waitForidPresent("uName");
		sleep(2);
		
	}
	
	public void TrailMessageClick()
	{
		String TrialMessagePresent = trailMessage.getAttribute("style");
		if (TrialMessagePresent.contains("block")) {
			trailMessage.click();
		}
	}
	
	public void logoutfromApp() throws InterruptedException{
		settings.click();
		waitForidPresent("generateQRCode");
		sleep(3);
		logoutBtn.click();
		sleep(4);
		Reporter.log("Logged out from SureMDM", true);
	}
	
	  public void loginToAPP() throws InterruptedException{
			Driver.driver.get(Config.url); 
			pagemaximize();
			System.out.println("Entering UN PWD");
			userNAmeEdt.sendKeys(Config.userID);
			passwordEdt.sendKeys(Config.password);
			loginBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			System.out.println("Logged into SureMDM successfully");
		}
	  
	public void dismissPopUpOfNoDevice() throws InterruptedException
	{
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 600);
		 
		    wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='processStepHolder']//a[@title='Close']"))).isDisplayed();
		    
		    
		   Thread.sleep(5000);
		    Initialization.driver.findElement(By.xpath("//div[@class='processStepHolder']//a[@title='Close']")).click();
		    
		    Thread.sleep(3000);
	}
	
	public void loginClear()throws InterruptedException{
		userNAmeEdt.clear();
		passwordEdt.clear();
	}
	
	public void ClickOnLicenseAgreementPage() throws InterruptedException
	{
		AcceptBtn.click();
		Reporter.log("Accepted License Agreement Page", true);
		sleep(5);
	}
	
	 @FindBy(linkText="Login again")
	 private WebElement LoginAgain;

	 public void ClickOnLoginAgainLink() throws InterruptedException
	 {
		 waitForXpathPresent("//a[text()='Login again']");	
		 sleep(2);
		 LoginAgain.click();
		 waitForidPresent("uName");
		 System.out.println("Clicked on Login again link");
		 sleep(2);
	 }
	/*
	// For Nix Sign Up 
	
	public void loginToDispostable(String DispostableID ,String url) throws InterruptedException{
		Driver.driver.get(url);
		
		
		pagemaximize();
		System.out.println("Entering The user ID of dispostable");
		Helper.highLightElement(Initialization.driver, Textbox_EmailField);
		Textbox_EmailField.sendKeys(DispostableID);
		
		Helper.highLightElement(Initialization.driver, loginBtn);
		loginBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		System.out.println("Logged in to SureMDM successfully");
		
	}
	*/
}
