package PageObjectRepository;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;


public class JobsOnAndroidPOM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();

	
	String IdforPageToLoadJobs = "job_new_job";
	String IdforConsolePageToLoad = "deleteDeviceBtn";
	
	
	@FindBy(xpath="//a[text()='Jobs']")
	private WebElement Jobs;
	
	@FindBy(id="job_new_job")
	private WebElement NewJob;
	
	@FindBy(id="andIcon")
	private WebElement Android;
	
	@FindBy(id="text_message")
	private WebElement TextMessage;
	
	@FindBy(id="okbtn")
	private WebElement Okbtn;
	
	@FindBy(xpath="//h4[contains(text(),'Text')]/preceding-sibling::div")
	private WebElement CloseBtnTextMessage;
	
	@FindBy(id="job_msg_name_input")
	private WebElement TextMessageJobNametextfield;
	
	@FindBy(id="job_msg_subject_input")
	private WebElement TextMessageSubjecttextfield;
	
	@FindBy(id="job_msg_body_input")
	private WebElement TextMessageBodytextfield;
	
	@FindBy(id="job_msg_radio1")
	private WebElement GetReadNotificationRadioBtn;
	
	@FindBy(id="job_msg_radio2")
	private WebElement ForceReadRadioBtn;
	
    @FindBy(xpath="//span[ul[li[text()='Please fill out this field.']]]/preceding-sibling::div/span")
    private WebElement MandatoryFieldTextMessage;
    
   @FindBy(xpath="//a[text()='Select Operating System']")
   private WebElement XpathforPageToLoadOS;
   
   @FindBy(id="osType")
   private WebElement IdforPageToLoadAndroid;
   
   @FindBy(xpath="//h4[contains(text(),'Text')]")
   private WebElement XpathforPageToLoadTextMessage;
   
   @FindBy(xpath="//span[text()='Job created successfully.']")
   private WebElement ConfirmationMessage;
   
   @FindBy(xpath="//span[text()='Job modified successfully.']")
   private WebElement JobModifyMessage;
   
  @FindBy(id="cancelPanel1")
  private WebElement CancelAndroidJob; 
  
  @FindBy(xpath="//div[input[@id='filePathHolder']]/following-sibling::div/div/div/div/input[@placeholder='Search']")
  private WebElement SearchJob;
    
  @FindBy(id="job_modify")
  private WebElement ModifyJob; 
  
  @FindBy(id="job_new_folder")
  private WebElement NewFolder; 
  
  @FindBy(id="jobFolderName")
  private WebElement EnterFolderName;
  
  @FindBy(id="foldername_ok")
  private WebElement FolderOkBtn;
  
  @FindBy(xpath="//h4[text()='Create Folder']/preceding-sibling::div")
  private WebElement FolderCancelBtn;
  
  @FindBy(id="job_move")
  private WebElement MoveToFolder; 
  
  @FindBy(xpath="//h4[text()='Move To']/preceding-sibling::button")
  private WebElement MoveToFolderCancelBtn;
  
  @FindBy(id="job_copy")
  private WebElement CopyToFolder;
  
  @FindBy(xpath="//h4[text()='Copy Job']/preceding-sibling::button")
  private WebElement CopyToFolderCancelBtn;
  
  @FindBy(id="job_delete")
  private WebElement DeleteJobFolder;

  @FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
  private WebElement DeleteJobFolderNoBtn;
  
  @FindBy(xpath="//table[@id='jobDataGrid']/tbody/tr[1]")
  private WebElement JobFirstRow;
  
  @FindBy(xpath="//div[input[@id='filePathHolder']]/following-sibling::div/div/div/div/input[@placeholder='Search']")
  private WebElement SearchJobFolder;
  
  @FindBy(id="osPanel_cancel_btn")
  private WebElement OSCancelBtn;

  @FindBy(xpath="//div[@id='panelBackgroundDevice']/div/div/div/button[@name='refresh']")
  private WebElement RefreshJobFolder;
  
    public void jobCreatedMthd(String SheetNo,int JobNamerow1,int JobNamecol1,int TypeNamerow2,int TypeNamecol2) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	String PassStatement="PASS >> 'Job created successfully.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Job created successfully.' Message is not displayed";
    	Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, true, PassStatement, FailStatement, 3);
    	
    	boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+ELib.getDatafromExcel(SheetNo, JobNamerow1, JobNamecol1)+"']]")).isDisplayed();
    	PassStatement = "PASS>> Job "+ELib.getDatafromExcel(SheetNo, JobNamerow1, JobNamecol1)+" is created successfully";
    	FailStatement = "FAIL>> Job "+ELib.getDatafromExcel(SheetNo, JobNamerow1, JobNamecol1)+"is not created";
    	ALib.AssertTrueMethod(value, PassStatement, FailStatement);
        
        String ExpectedValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+ELib.getDatafromExcel(SheetNo, JobNamerow1, JobNamecol1)+"']]/following-sibling::td[1]")).getText();
        String ActualValue= ELib.getDatafromExcel(SheetNo, TypeNamerow2, TypeNamecol2);
        PassStatement = "PASS>> Job "+ELib.getDatafromExcel(SheetNo, JobNamerow1, JobNamecol1)+" is created successfully with type as "+ELib.getDatafromExcel(SheetNo, TypeNamerow2, TypeNamecol2);
    	FailStatement = "FAIL>> Job to create with type as "+ELib.getDatafromExcel(SheetNo, TypeNamerow2, TypeNamecol2)+"Failed";
    	ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
    }
	
    public void PageToLoadOSdMtd(){
		String Actualvalue=XpathforPageToLoadOS.getText();
		String Expectedvalue="Select Operating System";
		String PassStatement="PASS >> Navigated to 'Select operating System' page";
		String FailStatement="FAIL >> Navigation to 'Select Operating System' failed";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void PageToLoadAndroidtMtd(){
		String Actualvalue=IdforPageToLoadAndroid.getText().trim();
		String Expectedvalue="Create Job [Android]";
		String PassStatement="PASS >> Navigated to 'Create Job [Android]' page";
		String FailStatement="FAIL >> Navigation to 'Create Job [Android]' failed";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void PageToLoadTextMessageMtd(){
		String Actualvalue=XpathforPageToLoadTextMessage.getText();
		String Expectedvalue="Create Text Message";
		String PassStatement="PASS >> Navigated to 'Create Text Message' page";
		String FailStatement="FAIL >> Navigation to 'Create Text Message' failed";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void MandatoryFieldForJobNameMtd(){
		String Actualvalue=MandatoryFieldTextMessage.getText();
		String Expectedvalue="Job Name";
		String PassStatement="PASS >> "+Expectedvalue+ "is mandatory field. Highlighted with 'Please fill out this field.'";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Old Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void MandatoryFieldForBodyMtd(){
		String Actualvalue=MandatoryFieldTextMessage.getText();
		String Expectedvalue="Body";
		String PassStatement="PASS >> "+Expectedvalue+ "is mandatory field. Highlighted with 'Please fill out this field.'";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Old Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void MandatoryFieldForSubjectMtd(){
		String Actualvalue=MandatoryFieldTextMessage.getText();
		String Expectedvalue="Subject";
		String PassStatement="PASS >> "+Expectedvalue+ "is mandatory field. Highlighted with 'Please fill out this field.'";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Old Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}

	
	public void ClickOnJobs() throws InterruptedException{
		Jobs.click();
		waitForidPresent("job_new_job");
		sleep(15);
	}
	
	 public void SearchJob(String JobName) throws InterruptedException
		{
			SearchJob.sendKeys(JobName);
			waitForidPresent("job_new_job");
			sleep(10);
		}
	public void ClickOnNewJob() throws InterruptedException{
		NewJob.click();
		waitForidPresent("andIcon");
		sleep(2);
	}
	
	public void ClickOnOSCancelBtn() throws InterruptedException{
		OSCancelBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void ClickOnAndroid() throws InterruptedException{
		Android.click();
		sleep(2);
	}
	
	
	public void ClickOnTextMessage() throws InterruptedException{
		TextMessage.click();
		sleep(2);
	}
	
	public void ClickOnOkBtn() throws InterruptedException{
		Okbtn.click();
		sleep(2);
	}
	
	public void ClickOnCloseTextMessage() throws InterruptedException{
		CloseBtnTextMessage.click();
		sleep(2);
	}
	
	public void ClickOnCancelAndroidJob() throws InterruptedException{
		CloseBtnTextMessage.click();
		sleep(2);
	}
	
	public void ClickOnGetReadNotification(){
		GetReadNotificationRadioBtn.click();
		
	}
	
	public void ClickOnForceReadMessage(){
		ForceReadRadioBtn.click();
		
	}
	
	public void EnterTextMessageJobNameField(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageJobNametextfield.sendKeys(ELib.getDatafromExcel("Sheet5", row, col));
	}
	
	public void EnterTextMessageSubjectField(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageSubjecttextfield.sendKeys(ELib.getDatafromExcel("Sheet2", row, col));
	}
	
	public void EnterTextMessageBodyField(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageBodytextfield.sendKeys(ELib.getDatafromExcel("Sheet2", row, col));
	}
	
	public void ClearTextMessageJobNameField() throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageJobNametextfield.clear();
	}
	
	public void ClearTextMessageSubjectField() throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageSubjecttextfield.clear();
	}
	
	public void ClearTextMessageBodyField() throws EncryptedDocumentException, InvalidFormatException, IOException{
		TextMessageBodytextfield.clear();
	}

	public void TestFailureTextMessage() throws InterruptedException
	{
		try {
			ClearTextMessageJobNameField();
		} catch (Exception e) {
			refesh();
			waitForidPresent(IdforConsolePageToLoad);
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			ClickOnJobs();
			ClickOnNewJob();
			ClickOnAndroid();
			ClickOnTextMessage();
		}
		
	}
	
	public void TestFailureNewJob() throws InterruptedException
	{
		try {
		   NewJob.click();
		} catch (Exception e) {
			refesh();
			waitForidPresent(IdforConsolePageToLoad);
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			ClickOnJobs();
			ClickOnNewJob();
		}
		ClickOnAndroid();
		
	}
	
	public void ClickOnModifyJob() throws InterruptedException{
		ModifyJob.click();
		sleep(5);
	}
	
	
	public void ClickOnNewJob_UM() {
		NewJob.click();
	}
	
	public void ClickOnNewFolder_UM() {
		NewFolder.click();
	}
	
	public void ClickOnNewFolder() throws InterruptedException{
		NewFolder.click();
		waitForidPresent("jobFolderName");
		sleep(2);
	}
	
	public void ClickOnNewFolderCancelBtn() throws InterruptedException{
		FolderCancelBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void EnterNewFolderName(String FolderName) throws InterruptedException{
		EnterFolderName.sendKeys(FolderName);
	}
	
	public void ClickOnFolderOkBtn() throws InterruptedException{
		FolderOkBtn.click();
		waitForidPresent("job_new_folder");
		sleep(2);
	}
	
	public void ClickOnFolderCancelBtn() throws InterruptedException{
		FolderCancelBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void ClickOnMoveToFolder_UM() {
		MoveToFolder.click();
	}
	
	public void ClickOnMoveToFolder() throws InterruptedException{
		MoveToFolder.click();
		waitForXpathPresent("//div[@id='tree']/ul/li[text()='Home']");
		sleep(2);
	}
	
	public void ClickOnMoveToFolderCancelBtn() throws InterruptedException{
		MoveToFolderCancelBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void ClickOnCopyToFolder_UM(){
		CopyToFolder.click();
	}
	
	public void ClickOnCopyToFolder() throws InterruptedException{
		CopyToFolder.click();
		waitForidPresent("jobName");
		sleep(2);
	}
	
	public void ClickOnCopyToFolderCancelBtn() throws InterruptedException{
		CopyToFolderCancelBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void ClickOnDeleteJobFolder_UM() {
		DeleteJobFolder.click();
	}
	
	public void ClickOnDeleteJobFolder() throws InterruptedException{
		DeleteJobFolder.click();
        waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]");
		sleep(2);
	}
	
	public void ClickOnDeleteJobFolderNoBtn() throws InterruptedException{
		DeleteJobFolderNoBtn.click();
		waitForidPresent("job_new_job");
		sleep(2);
	}
	
	public void ClickOnModifyJob_UM() {
		ModifyJob.click();
	}
	
	public void ClickOnJobFirstRow() throws InterruptedException {
		JobFirstRow.click();
		sleep(2);
	}
	
	public void SearchJobFolder(String JobFolderName) throws InterruptedException
	{
		SearchJobFolder.sendKeys(JobFolderName);
		RefreshJobFolder.click();
		waitForidPresent("job_new_job");
		sleep(10);
	}
		
    public void ClearSearch() throws InterruptedException
	{
		SearchJobFolder.clear();
		sleep(2);
	}
    
    public void ModifyTextJob(int jobrow,int jobcol,int modifyrow,int modifycol) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		try {
			   SearchJob.sendKeys(ELib.getDatafromExcel("Sheet5", jobrow, jobcol));
			} catch (Exception e) {
				refesh();
				waitForidPresent(IdforConsolePageToLoad);
				sleep(5);
				Initialization.loginPage.TrailMessageClick();
				ClickOnJobs();
				SearchJob.sendKeys(ELib.getDatafromExcel("Sheet5", jobrow, jobcol));
			}
		try{
			Initialization.driver.findElement(By.xpath("//td[p[text()='"+ELib.getDatafromExcel("Sheet5", jobrow, jobcol)+"']]")).click();
		}catch(Exception e)
		{
			System.out.println(ELib.getDatafromExcel("Sheet5", jobrow, jobcol) + " :Job Not found");
		}
		ClickOnModifyJob();
		ClearTextMessageJobNameField();
		EnterTextMessageJobNameField(modifyrow, modifycol);
		ClickOnOkBtn();
		String PassStatement="PASS >> 'Job modified successfully.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Job modified successfully.' Message is not displayed";
    	Initialization.commonmethdpage.ConfirmationMessageVerify(JobModifyMessage, true, PassStatement, FailStatement, 3);
    	
    	boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+ELib.getDatafromExcel("Sheet5", modifyrow, modifycol)+"']]")).isDisplayed();
    	PassStatement = "PASS>> Job "+ELib.getDatafromExcel("Sheet5", jobrow, jobcol)+" is modified successfully to "+ELib.getDatafromExcel("Sheet5", modifyrow, modifycol);
    	FailStatement = "FAIL>> Job "+ELib.getDatafromExcel("Sheet5", jobrow, jobcol)+"is not modified";
    	ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	
    public void ClickOnJob(String JobFolderName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//td/p[text()='"+JobFolderName+"']")).click();
		sleep(2);
	}
    
	public boolean CheckAllowNewFolder(String JobFolderName)
	{
	boolean value;
		try{
			Initialization.driver.findElement(By.xpath("//td/p[contains(text(),'"+JobFolderName+"')]")).isDisplayed();
			value=true;
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	public boolean CheckJobsBtn()
	{
		boolean value = true;
		try{
			Jobs.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	@FindBy(xpath=".//*[@id='install_program']")
	private WebElement InstallApplication;
	
	public void ClickOnInstallApplication() throws InterruptedException
	{
		InstallApplication.click();
		waitForidPresent("funcHolder");
		sleep(3);
	}
	
	
	
	
	
	
	
	
}
