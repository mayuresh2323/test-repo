package PageObjectRepository;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;


import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class LocationTracking_DeviceInfo_POM extends WebDriverCommonLib
{
    WebDriverWait wait=new WebDriverWait(Initialization.driver,40);
    AssertLib Ali=new AssertLib();
	@FindBy(xpath="//a[@id='locationTrackingEditBtn']")
	private WebElement EditLocationButton;
	
	@FindBy(xpath="//*[@id='locationTrackingModal']/div/div/div[2]/div[1]/div/label")
	private WebElement LocationSwitch;
	
	@FindBy(xpath="//input[@id='job_text']")
	private WebElement TrackingPerodicity;
	
	@FindBy(xpath="//button[@onclick='locationTrackingOkClick()']")
	private WebElement ApplyLocationTracking;
	
	@FindBy(xpath="//span[text()='Device tracking updated successfully.']")
	private WebElement UpdateLocationTracking;
	
    @FindBy(xpath="//button[@name='refresh']")
    private WebElement GridRefreshButton;
    
    @FindBy(xpath="//i[@title='Tracking[ON], Location[Available]']")
    private WebElement LocationtrackingSymbol;
    
    @FindBy(xpath="//div[@id='locateBtn']")
    private WebElement LocateButton;
    
    @FindBy(xpath="//td[@class='LastTimeStamp']")
    private WebElement DeviceLastConnected;
    
    @FindBy(xpath="//i[@class='icn icn fa fa-refresh']")
    private WebElement DynamicRefreshButton;
    
    
	public void ClickingOnLocationTrackingEditButton() throws InterruptedException
	{		
		EditLocationButton.click();
		waitForidPresent("isLocationTrackingOn");
		sleep(7);
	}
	public void TurningOnLocation() throws InterruptedException
	{
		if(LocationSwitch.isEnabled())
		{
			Reporter.log("Location Tracking Is On");
			 ApplyLocationTracking.click();
		}
		else
		{
        LocationSwitch.click();
        sleep(8);
        TrackingPerodicity.clear();
        TrackingPerodicity.sendKeys("2");
        ApplyLocationTracking.click();
        sleep(3);
		}
	}
	public void VerifyLocationTrackingStatus() throws InterruptedException
	{
		boolean mark;
        waitForXpathPresent("//span[text()='Device tracking updated successfully.']");
        if(UpdateLocationTracking.isDisplayed())
        {
          mark=true;	
        }
        else
        {
        	mark=false;
        }
        String passStatement="PASS >> Device Tracking Updated Successfully";
   	   String failStatement="FAIL >>  Device Tracking is not Updated";
      	 Ali.AssertTrueMethod(mark, passStatement, failStatement); 
      	 sleep(10);
	}
      	 
      	 
      	 public void verifyLocationTrackingSymbolInGrid() throws InterruptedException
      	 {
      		boolean value;
         	GridRefreshButton.click();
         	waitForXpathPresent("//p[text()='"+Config.DeviceName+"']");
         	if(LocationtrackingSymbol.isDisplayed())
        	{
      		value=true;
         	}
        	else
        	{
      		value=false;
         	}
      	     String PassStatement="PASS >> Location Tracking Updated Successfully in the device Grid";
             String FailStatement="FAIL >>  Location Tracking is not Updated";
             Ali.AssertTrueMethod(value, PassStatement, FailStatement); 
             sleep(10);
      	 }
      	 public void LocatingDevice() throws InterruptedException
      	 {
      		LocateButton.click();
      		sleep(5);
      		String ParentWindow = Initialization.driver.getWindowHandle();
      		Set<String> AllWindows = Initialization.driver.getWindowHandles();
      		for(String handle:AllWindows)
      		{
      			if(!handle.equals(ParentWindow))
      			{
      				Initialization.driver.switchTo().window(handle);
      				waitForXpathPresent("//button[@id='refreshbtn']");
      				Initialization.driver.switchTo().window(ParentWindow);
      			}
      		}
      	 }
        
		public void DeviceLastConnectesTime() throws InterruptedException
		{
			boolean mark;
			String DeviceTimeBeforeRefresh = DeviceLastConnected.getText();
			System.out.println(DeviceTimeBeforeRefresh);
			sleep(3);
			DynamicRefreshButton.click();
			sleep(60);
			String DeviceTimeAfterRefresh = DeviceLastConnected.getText();
			System.out.println(DeviceTimeAfterRefresh);
			if(!DeviceTimeBeforeRefresh.equals(DeviceTimeAfterRefresh))
			{
				 mark=true;
			}
			else
			{
				 mark = false;
			}
			 String PassStatement="PASS >> Device Last Connected Time Updated Successfully";
             String FailStatement="FAIL >> Device Last Connected Time not Updated Successfully ";
             Ali.AssertTrueMethod(mark, PassStatement, FailStatement); 		
			
			}
	}
	
	
