package PageObjectRepository;

import Common.Initialization;
import Library.WebDriverCommonLib;

public class DeviceEnrollment_ScanQR_POM extends WebDriverCommonLib
{
     public void ClickOnGetStartedButton()
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Get Started']").click();
     }
}
