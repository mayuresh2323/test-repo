package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;

public class Profiles_Windows_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib(); 

	@FindBy(xpath="//span[text()='Password Policy']")
	private WebElement passwordPolicyOption;

	@FindBy(id="selectButtonWindows")
	private WebElement windowsOption;

	@FindBy(id="addPolicyBtn")
	private WebElement Addbutton;

	@FindBy(xpath="//p[text()='Windows Profile']")
	private WebElement WindowsMDMProfileText;

	@FindBy(id="WindowsPasswordProfileConfig")
	private WebElement ConfgureButton_PasswordPolicy;

	@FindBy(id="policyNameWindows")
	private WebElement profileName;

	@FindBy(id="saveWindowsProfile")
	private WebElement saveWindowsProfile;

	@FindBy(xpath="//span[text()='Please enter a name for the profile.']")
    private WebElement warningMessageOnSavingWithoutProfileName;

	@FindBy(xpath="//span[text()='Profile created successfully.']")
    private WebElement NotificationOnProfileCreated;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[2]")
	private WebElement MailConfigurationProfile;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[3]")
	private WebElement RestrictionPolicyProfile;

	@FindBy(id="windowsmailProfileConfig")
	private WebElement ConfigureButton_MailConfigurationProfile;

	@FindBy(id="windowsrestrictionProfileConfig")
	private WebElement ConfigureButton_RestrictionProfile;

	@FindBy(id="appLockerProfileConfig")
	private WebElement ConfigureButton_AppLocker;

	@FindBy(id="windowsApplicationProfileConfig")
	private WebElement ConfigureButton_ApplicationPolicy;

	@FindBy(xpath="//span[text()='Please complete all mandatory fields.']")
	private WebElement warningMessageOnSavingProfileWithoutAllFields;

	@FindBy(xpath="//span[text()='Please enter all required fields to save the profile']")
	private WebElement warningMessageOnSavingProfileWithoutAllRequiredFields;

	@FindBy(id="addWindowsMail")
	private WebElement AddButton_MailConfiguration;

	@FindBy(xpath="//h4[text()='Mail Configuration']")
	private WebElement MailConfigurationWindowHeader;

	@FindBy(id ="name")
	private WebElement YourName;

	@FindBy(xpath=".//*[@id='windowsMailConfig']/div/div/div[3]/button[text()='Cancel']")
	private WebElement CancelButton_MailConfigurationWindow;

	@FindBy(id="accountName")
	private WebElement AccountName;

	@FindBy(id="emailAddress")
	private WebElement EmailAddress;

	@FindBy(id="userName")
	private WebElement UserName;

	@FindBy(id= "password")
	private WebElement password;

	@FindBy(id="cellularNetwork")
	private WebElement Checkbox_useCellularNetworkOnly;

	@FindBy(id="incomingServer")
	private WebElement MailServer;

	@FindBy(xpath=".//*[@id='windowsMailConfig']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton_MailConfigurationWindow;

	@FindBy(xpath="//span[text()='Port is required']")
	private WebElement WarningWithoutportNumber;

	@FindBy(id="incomingPort")
	private WebElement incomingPort;

	@FindBy(xpath="//div[@id='windowsMailConfig']/div/div/div[2]/ul/li[2]") //correct absolute xpath
	private WebElement OutgoingMail;

	@FindBy(id="outgoingServer")
	private WebElement EnterOutgoingServer;

	@FindBy(id="outgoingPort")
	private WebElement EnterOutgoingPort;

	@FindBy(xpath="//span[text()='Please enter a valid email address']")
	private WebElement WarningMessage_InvaliEmailAddress_MailConfiguration;

	@FindBy(id="windowsUseCamera")
	private WebElement isAllowUseOfCameraEnabled;

	@FindBy(xpath="//h4[text()='Camera']")
	private WebElement CameraOption_RestrictionPolicy;

	@FindBy(xpath="//h4[text()='System']")
	private WebElement SystemOption_RestrictionPolicy;

	@FindBy(id="windowsFactoryReset")
	private WebElement CheckBox_AllowUseOfStorageCard;

	@FindBy(id="windowsUseStorageCard")
	private WebElement CheckBox_AllowtheUserToFactoryResetThePhone;

	@FindBy(id="windowsAllowStore")
	private WebElement Checkbox_AllowWindowsStore;

	@FindBy(id="windowsAllowUpdates")
	private WebElement Checkbox_AllowAutomaticUpdateofAppsFromWindowsStore;

	@FindBy(xpath="//h4[text()='Experience']")
	private WebElement Experience_RestrictionPolicy;

	@FindBy(id="windowsCortana")
	private WebElement CheckBox_AllowCortana;

	@FindBy(id="windowsDeviceDiscovery")
	private WebElement CheckBox_AllowDeviceDiscovery;

	@FindBy(id="windowsManualUnenroll")
	private WebElement CheckBox_windowsManualUnenroll;

	@FindBy(id="windowsCopyPaste")
	private WebElement CheckBox_windowsCopyPaste;

	@FindBy(id="windowsScreenCapture")
	private WebElement CheckBox_windowsScreenCapture;

	@FindBy(id="windowsSimCard")
	private WebElement CheckBox_windowsSimCard;

	@FindBy(id="windowsTaskSwitching")
	private WebElement CheckBox_windowsTaskSwitching;

	@FindBy(id="windowsVoiceRecording")
	private WebElement CheckBox_windowsVoiceRecording;

	@FindBy(xpath=".//*[@id='windowsLocation']/option[2]")
	private WebElement LocationFieldDefaultText_RestrictionPolicy;

	@FindBy(xpath=".//*[@id='windowsLocation']/option[1]")
	private WebElement ForceLocationOff_RestrictionPolicy;

	@FindBy(xpath=".//*[@id='windowsLocation']/option[3]")
	private WebElement ForceLocationOn_RestrictionPolicy;

	@FindBy(xpath=".//*[@id='windowsDevFeatures']/option[1]")
	private WebElement UseDeveloperFeaturesFieldDefaultText_RestrictionPolicy;

	@FindBy(xpath=".//*[@id='windowsDevFeatures']/option[2]")
	private WebElement SideloadApps_RestrictionPolicy;

	@FindBy(xpath=".//*[@id='windowsDevFeatures']/option[3]")
	private WebElement DeveloperMode_RestrictionPolicy;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[4]")
	private WebElement AppLockerPolicy;

	@FindBy(xpath="//h5[text()='Use this section to configure the list of apps that are allowed or blocked from running on a device.']")
    private WebElement AppLockerSummary;

	@FindBy(xpath="//h5[text()='Use this section to add application to install on your device.']")
	private WebElement ApplicationPolicySummary;

	@FindBy(id="addAppLocker")
	private WebElement AddButton_AppLocker;

	@FindBy(id="windows_addInstallApplicationPolicyBtn")
	private WebElement AddButton_ApplicaionPolicy;

	@FindBy(id="addWindowsWifi")
	private WebElement AddButton_WifiConfiguration;

	@FindBy(xpath="//h4[text()='App Locker']")
	private WebElement AppLockerWindow_HeaderText;

	@FindBy(xpath="//h4[text()='Add App']")
	private WebElement ApplicationPolicyWindow_HeaderText;

	@FindBy(xpath=".//*[@id='appLockermodal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton_AppLockerWindow;

	@FindBy(xpath=".//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton_ApplicationPolicyWindow;

	@FindBy(xpath=".//*[@id='appLockermodal']/div/div/div[3]/button[text()='Save']")
	private WebElement SaveButton_AppLockerWindow; // it shows on edit window

	@FindBy(xpath=".//*[@id='appLockermodal']/div/div/div[3]/button[text()='Cancel']")
	private WebElement CancelButton_AppLockerWindow;

	@FindBy(xpath=".//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[text()='Cancel']")
	private WebElement CancelButton_AddAppWindow;

	@FindBy(id="appLockerPublisher")
	private WebElement publisher_AppLocker;

	@FindBy(id="appLockerPackage")
	private WebElement PackageName_AppLocker;

	@FindBy(xpath=".//*[@id='appLockermodal']/div/div/div[2]/div[1]/div[2]/span")
	private WebElement Deny_RadioButton_AppLockerWindow;

	@FindBy(xpath=".//*[@id='appLockerTable']/tbody/tr[1]/td")
	private WebElement FirstRow_AppLocker;

	@FindBy(xpath=".//*[@id='windows_installApplicationPolicyTable']/tbody/tr/td[1]")
	private WebElement FirstRow_ApplicationPolicy;

	@FindBy(id="editAppLocker")
	private WebElement EditButton_AppLocker;

	@FindBy(id="windows_editInstallApplicationPolicyBtn")
	private WebElement EditButton_ApplicationPolicy;

	@FindBy(xpath="//td[text()='Rename']")
	private WebElement Rename_Publisher_FirstRow_AppLocker;

	@FindBy(id="deleteAppLocker")
	private WebElement DeleteButon_AppLocke;

	@FindBy(id="windows_deleteInstallApplicationPolicyBtn")
	private WebElement DeleteButon_ApplicationPolicy;

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement NoButton_DeleteWarningDialog;

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesButton_DeleteWarningDialog;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[5]")
	private WebElement WifiConfigurationPolicy;

	@FindBy(id="windowswifiProfileConfig")
	private WebElement ConfigureButton_WifiConfiguration;

	@FindBy(xpath="//h4[text()='Wi-Fi Configuration']")
	private WebElement WifiConfigurationWindow_HeaderText;

	@FindBy(xpath=".//*[@id='windowsWifiConfig']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton_WifiConfigurationWindow;

	@FindBy(xpath=".//*[@id='windowsWifiConfig']/div/div/div[3]/button[text()='Cancel']")
	private WebElement CancelButton_WifiConfigurationWindow;

	@FindBy(xpath=".//*[@id='windowsSecurityType']/option[1]")
	private WebElement SecurityType_Open_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsSecurityType']/option[2]")
	private WebElement SecurityType_WPAEnterprise_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsSecurityType']/option[3]")
	private WebElement SecurityType_WPAPersonal_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsSecurityType']/option[4]")
	private WebElement SecurityType_WPA2Enterprise_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsSecurityType']/option[5]")
	private WebElement SecurityType_WPA2Personal_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsEncryptionType']/option[1]")
	private WebElement EncryptionType_None_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsEncryptionType']/option[2]")
	private WebElement EncryptionType_WEP_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsEncryptionType']/option[3]")
	private WebElement EncryptionType_TKIP_WifiConfig;

	@FindBy(xpath=".//*[@id='windowsEncryptionType']/option[4]")
	private WebElement EncryptionType_AES_WifiConfig;

	@FindBy(id="windowsSSID")
	private WebElement SSID_WifiConfig;

	@FindBy(id="windowsWifiPassword")
	private WebElement Password_WifiConfig;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[6]")
	private WebElement VPNConfigurationPolicy;

	@FindBy(id="VPNProfileConfig")
	private WebElement ConfigureButton_VPNConfiguration;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[10]")
	private WebElement PeriodicAppLaunchPolicy;

	@FindBy(id="windowsAppLaunchConfig")
	private WebElement ConfigureButton_PeriodicAppLaunchConfiguration;

	@FindBy(id="WindowsPeriodicAppLaunchPeriodicity")
	private WebElement PeriodicityInMins_PeriodicAppLaunch;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[7]")
	private WebElement ExchangeActiveSyncPolicy;

	@FindBy(id="windowsExchangeProfileConfig")
	private WebElement ConfigureButton_ExchangeActiveSync;

	@FindBy(id="addWindowsExchange")
	private WebElement AddButton_ExchangeActive;

	@FindBy(xpath="//h4[text()='Exchange ActiveSync']")
	private WebElement ExchangeActiveSyncWindow_HeaderText;

	@FindBy(xpath=".//*[@id='windowsExchangeConfig']/div/div/div/button[text()='Add']")
	private WebElement AddButton_ExchangeActiveSyncWindow;

	@FindBy(xpath=".//*[@id='exchangeWindowsScheduleSync']/option[text()='Default']")
	private WebElement DefaultValue_SyncSchedule;

	@FindBy(xpath=".//*[@id='exchangeWindowsDaysSync']/option[text()='1 week']")
	private WebElement DefaultValue_DaysToSync;

	@FindBy(xpath=".//*[@id='windowsExchangeConfig']/div/div/div/button[text()='Cancel']")
	private WebElement CancelButton_ExchangeActiveSync;

	@FindBy(id="exchangeWindowsSSL")
	private WebElement CheckBox_UseSSL;

	@FindBy(id="exchangeWindowsAccountName")
	private WebElement AccountName_ExchangeActiveSync;

	@FindBy(id="exchangeWindowsHost")
	private WebElement ExchangeActiveSyncHost;

	@FindBy(id="exchangeWindowsUser")
	private WebElement Username_ExchangeActiveSync;

	@FindBy(id="exchangeWindowsEmail")
	private WebElement EmailAddress_ExchangeActiveSync;

	@FindBy(id="exchangeWindowsPassword")
	private WebElement Password_ExchangeActiveSync;

	@FindBy(xpath=".//*[@id='exchangeWindowsScheduleSync']/option[2]")
	private WebElement SyncSchedule_Manual;

	@FindBy(xpath=".//*[@id='exchangeWindowsScheduleSync']/option[3]")
	private WebElement SyncSchedule_Every15Minutes;

	@FindBy(xpath=".//*[@id='exchangeWindowsScheduleSync']/option[4]")
	private WebElement SyncSchedule_Every30Minutes;

	@FindBy(xpath=".//*[@id='exchangeWindowsScheduleSync']/option[4]")
	private WebElement SyncSchedule_Every60Minutes;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div[@id='policy_item_box']/div[3]/div/ul/li[8]")
	private WebElement ApplicationPolicy;

	@FindBy(id="WindowsApplicationpolicyAppName")
	private WebElement Name_AddAppWindow_ApplicationPolicy;

	@FindBy(id="WindowsApplicationpolicyURL")
	private WebElement EnterUrl_AddAppWindow_ApplicationPolicy;

	@FindBy(xpath="//td[text()='rename']")
	private WebElement Rename_Name_FirstRow_ApplicationPolicy;

	@FindBy(xpath=".//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[text()='Save']")
	private WebElement SaveButton_AddAppWindow_ApplicationPolicy;

	@FindBy(xpath="//span[text()='Action']")
	private WebElement ActionParameter;
	
	@FindBy(xpath="//div[@class='input-group ct-w100 dontallowaccess ifpath_Hide ifStoreApps_show']/span[text()='Publisher']")
	private WebElement PublisherParameter;
	
	@FindBy(id="packageName")
	private WebElement packageNameParameter;

	public void ClickOnWindowsOption() throws InterruptedException
	{
		windowsOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(3);
	}
	
	public void AddProfile() throws InterruptedException{
        Reporter.log("=======Verify Adding a Profile and check the availabitity of all the options in the window=======");
        Helper.highLightElement(Initialization.driver, Addbutton);
        Addbutton.click();
        sleep(3);
        boolean check = WindowsMDMProfileText.isDisplayed();
        String PassStatement = "PASS >> Clicked on Add profile button and window displayed successfully";
        String FailStatement = "FAIL >> Did not Click on Add profile button and window NOT displayed successfully";
        ALib.AssertTrueMethod(check,PassStatement, FailStatement);
        Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        sleep(1);
}


	@FindBy(xpath="//input[@id='RequireProtectionUnderLockConfig']")
	private WebElement EnablingPreventCorporateData;

	@FindBy(xpath="//div[@id='leftWindowsMenu']/div/div[3]/div/ul/li[14]")
	private WebElement EnterpriseDataProtectionOption;
	public void ClickOnEnterPriseDataProtection() throws InterruptedException
	{
		EnterpriseDataProtectionOption.click();
		Reporter.log("Clicking On EnterPrise Data Protection",true);
		sleep(2);  
		waitForidClickable(("edpProfileConfig"));
	}
	public void clickOnConfigureButton_EnterPriseDataProtection() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, ConfgureButton_PasswordPolicy);
		ConfgureButton_EnterPrisedataprotection.click();
		Reporter.log("Clicking On Configure button,Navigating to Enterprise Data Protection page",true);
		sleep(2);
	} 
	public void VarifyingEnterpriseApplicationIsPresent()
	{
		Reporter.log("=======Verify EnterPrise Appliactions is Available or not=======");
		String actual =EnterPriseApplications.getText();
		Reporter.log(actual);
		String expected = "Enterprise Applications";
		String PassStatement ="PASS >> Enterprise Applications is Available";
		String FailStatement ="FAIL >> Enterprise Applications is not Available";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

	}

	public void ClickOnBackBtn() throws InterruptedException
	{
		Backbtn.click();
		sleep(10);
		waitForXpathPresent("//a[text()='Home']");
	}

	public void VarifyAddedProfileOfTypeStoreAppsInEnterpriseApp()
	{

		boolean check;
		for(int i=0;i<AddedProfile.size();i++)
		{
			String ProfileNames = AddedProfile.get(i).getText();
			if(ProfileNames.equals(Config.Enter_PublisherNameForSkypeApplication))
			{
				check=true;
			}
			else
			{
				check=false;
			}
			String PassStatement = "PASS >> Adding Enterprise Appliction ";
			String FailStatement = "FAIL >> Did not Adding Enterprise Appliction";
			ALib.AssertTrueMethod(check,PassStatement, FailStatement);

		}
		waitForXpathPresent("//input[@id='primaryDomain']");
	} 

	@FindBy(xpath="//table[@id='enterpriseDataProtectionTable']/tbody/tr/td[1]")
	private List<WebElement> AddedProfile;
	public void VarifyAddedProfileForEXEapps()
	{

		boolean check;
		for(int i=0;i<AddedProfile.size();i++)
		{
			String ProfileNames = AddedProfile.get(i).getText();
			if(ProfileNames.equals(Config.Enter_PublisherNameForNotePad))
			{
				check=true;
			}
			else
			{
				check=false;
			}
			String PassStatement = "PASS >> Adding Enterprise Appliction ";
			String FailStatement = "FAIL >> Did not Adding Enterprise Appliction";
			ALib.AssertTrueMethod(check,PassStatement, FailStatement);

		}
		waitForXpathPresent("//input[@id='primaryDomain']");
	}

	public void EnterPrimarydomain()
	{		 		 
		Helper.highLightElement(Initialization.driver, primarydomain);
		primarydomain.click();
		boolean check = primarydomain.isDisplayed();
		String PassStatement = "PASS >> Primary domain field is  displayed successfully";
		String FailStatement = "FAIL >> Primary domain field is not displayed successfully";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		primarydomain.sendKeys(Config.Enter_PrimaryDomain);
		System.out.println("Primary domain is entered");  		 
	}

	@FindBy(xpath="//input[@id='primaryDomain']")
	private WebElement  primarydomain;
	
	public void EnterPublisherNameForExeemptApplication(String name)
    {
       Reporter.log("Enter PublisherName",true);
   	Helper.highLightElement(Initialization.driver, ExemptAppPublisherName);
   	ExemptAppPublisherName.sendKeys(name);
   	boolean check = ExemptAppPublisherName.isDisplayed();
   	String PassStatement = "PASS >> Publisher Name is entered";
   	String FailStatement = "FAIL >> Publisher Name is did not entered";
   	ALib.AssertTrueMethod(check,PassStatement, FailStatement);
    }
	
	public void EnablingPreventCorporateDataFromBeingAccessedByApps() throws InterruptedException
 	{
 		EnablingPreventCorporateData.click();
 		sleep(2);
 		
 	}
	
	 public void EnterPath()
     {
    	 Path.sendKeys(Config.EnterChromePath);
     }
	 
		public void ClickOnExemptAppAddBtn()
     	{
         	Helper.highLightElement(Initialization.driver, ExemptAppAddbtn);
         	ExemptAppAddbtn.click();
       		boolean check = ExemptAppAddbtn.isDisplayed();
       		String PassStatement = "PASS >> Clicked on Exempt Applications Add button";
       		String FailStatement = "FAIL >> Did not Clicked on Exempt Applications Add button";
       		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
     	}
		
	public void EnterProductNameForExemptApplication()
    {
	    Reporter.log("Enter Product Name",true);
	    Helper.highLightElement(Initialization.driver, ExemptAppProductName);
	    ExemptAppProductName.sendKeys(Config.Enter_ProductNameAsNotePad);
	    boolean check = ExemptAppProductName.isDisplayed();
	    String PassStatement = "PASS >> Product Name is entered";
	    String FailStatement = "FAIL >> Product Name is did not entered";
	    ALib.AssertTrueMethod(check,PassStatement, FailStatement);
    }
	
	public void ClickOnAddExemptApplicationBtn() throws InterruptedException
    {
	    Reporter.log("Adding Enterprise Appliction",true);
	    Helper.highLightElement(Initialization.driver, AddExemptApp);
	    AddExemptApp.click();
	    sleep(2);
    }
	
	 public void EnterBinaryNameForExemptApplication()
 	 {
 	    Reporter.log("Enter Binary Name",true);
 	    Helper.highLightElement(Initialization.driver, ExemptAppBinaryName);
 	    ExemptAppBinaryName.sendKeys(Config.Enter_BinaryName);
 	    boolean check = ExemptAppBinaryName.isDisplayed();
 	    String PassStatement = "PASS >> Binary Name is entered";
 	    String FailStatement = "FAIL >> Binary Name is did not entered";
 	    ALib.AssertTrueMethod(check,PassStatement, FailStatement);
 	  }
	 
	public void ClickontypeconditionStoreAppInExemptApplications(String val)
 	{ 
 	    Reporter.log("Click On Type Condition",true);
 	    ExemptApptypeCondition.click();
   	    boolean check = ExemptApptypeCondition.isDisplayed();
   	    String PassStatement = "PASS >> Clicked on Store Apps Type Condition";
   	    String FailStatement = "FAIL >> Did not Clicked on Store Apps Type Condition";
   	    ALib.AssertTrueMethod(check,PassStatement, FailStatement);
        Select var=new Select(StoreApps);
 	    var.selectByValue(val); 
 	    System.out.println("Clicked on Store Apps Type Condition");  	
 	 }
	
	public void SelectApplicationProtectionlevel(String var)
	{
		Reporter.log("Selecting Application Data Protection level",true);
		Appprotectionlevel.click();
		boolean check = Appprotectionlevel.isDisplayed();
		String PassStatement = "PASS >> Clicked on application level";
		String FailStatement = "FAIL >> Did not Clicked on application level";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		Select var1=new Select(Appprotectionlevel);
		var1.selectByValue(var); 		 
	} 

	@FindBy(xpath="//button[@id='saveWindowsProfile']")
	private WebElement SaveEnterpriseAppProfile;

	@FindBy(xpath="//select[@id='appDataProtection']")
	private WebElement  Appprotectionlevel;
	public void ClickOnSaveEnterpriseAppProfileBtn() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, Enterprofilename);
		SaveEnterpriseAppProfile.click();
		sleep(2);
		waitForXpathPresent("//span[text()='Profile created successfully.']");
	}
	@FindBy(xpath="//span[text()='Profile created successfully.']")
	private WebElement CreatedProfile;
	public void VarifingCreatedProfile()
	{
		boolean check = CreatedProfile.isDisplayed();
		String PassStatement = "PASS >> Profile Created";
		String FailStatement = "FAIL >> Did not Created Profile";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		waitForXpathPresent("//div[@id='WindowsTabProfileContent']/div/div/div/div/div/div[2]/button[@class='btn smdm_btns smdm_gry_clr editbutton backToPrevTabs']");	
	}

	public void ClickOnPathButton()
	{
		PathBtn.click();

	}

	@FindBy(xpath="//*[@id='enterpriseDataProtectionmodal']/div/div/div[3]/button[1]")
	private WebElement AddEnterpriseAppBtn;
	public void ClickOnAddEnterpriseAppBtn() throws InterruptedException
	{
		Reporter.log("Adding Enterprise Appliction",true);
		Helper.highLightElement(Initialization.driver, AddEnterpriseAppBtn);
		AddEnterpriseAppBtn.click();
		sleep(2);
	}

	@FindBy(xpath="//div[@id='edpProfileTail']/ul[5]/li[1]")
	private WebElement Advancedsettings;
	public void VarifyingAdvancesettings()
	{
		Reporter.log("=======Verify Advanced settings is Available or not=======");
		String actual =Advancedsettings.getText();
		Reporter.log(actual);
		String expected = "Advanced Settings";
		String PassStatement ="PASS >> Advanced settings  is Available";
		String FailStatement ="FAIL >> Advanced settings is not Available";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}
	public void EnterProfileName(String name)
	{
		Helper.highLightElement(Initialization.driver, Enterprofilename);
		Enterprofilename.click();
		boolean check = Enterprofilename.isDisplayed();
		String PassStatement = "PASS >> Profile Name field is  displayed successfully";
		String FailStatement = "FAIL >> Profile Name field is not displayed successfully";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		Enterprofilename.sendKeys(name);
		System.out.println("Profile Name is entered");	 
	}
	public void ClickOnEnterpriseAppAddBtn()
	{
		Helper.highLightElement(Initialization.driver, Enterprofilename);
		EnterpriseAppAddBtn.click();
		boolean check = EnterpriseAppAddBtn.isDisplayed();
		String PassStatement = "PASS >> Clicked on EnterPrise Applications Add button";
		String FailStatement = "FAIL >> Did not Clicked on EnterPrise Applications Add button";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);	 
	}
	public void ClickontypeconditionStoreApp(String val)
	{
		Reporter.log("Click On Type Condition",true);
		StoreApps.click();
		boolean check = StoreApps.isDisplayed();
		String PassStatement = "PASS >> Clicked on Store Apps Type Condition";
		String FailStatement = "FAIL >> Did not Clicked on Store Apps Type Condition";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		Select var=new Select(StoreApps);
		var.selectByValue(val); 
		System.out.println("Clicked on Store Apps Type Condition");  	
	}
	public void EnterPublisher_Name(String name)
	{
		Reporter.log("Enter PublisherName",true);
		Helper.highLightElement(Initialization.driver, Publishername);
		Publishername.sendKeys(name);
		boolean check = Publishername.isDisplayed();
		String PassStatement = "PASS >> Publisher Name is entered";
		String FailStatement = "FAIL >> Publisher Name is did not entered";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
	}

	@FindBy(xpath="//input[@id='edpPackage']")
	private WebElement ProductName;

	@FindBy(xpath="//input[@id='edpBinaryName']")
	private WebElement BinaryName;

	@FindBy(xpath="(//*[@id='conditionRadioApplocker']/div[2]/span/input)[2]")
	private WebElement PathBtn;

	@FindBy(xpath="//input[@id='edpPath']")
	private WebElement Path;

	@FindBy(xpath="//button[@id='addExemptEDP']")
	private WebElement ExemptAppAddbtn;

	@FindBy(xpath="//input[@id='edpPackage']")
	private WebElement Packagename;

	@FindBy(xpath="//input[@id='edpPublisher']")
	private WebElement Publishername;

	@FindBy(xpath="//select[@id='edpAppType']")
	private WebElement StoreApps;

	@FindBy(xpath="//button[@id='addEDP']/span[text()='Add']")
	private WebElement EnterpriseAppAddBtn;

	@FindBy(xpath="//input[@id='policyNameWindows']")
	private WebElement Enterprofilename;

	@FindBy(xpath="//div[@id='WindowsTabProfileContent']/div/div/div/div/div/div[2]/button[@class='btn smdm_btns smdm_gry_clr editbutton backToPrevTabs']")
	private WebElement Backbtn;


	public void EnterPackage_Name()
	{
		Reporter.log("Enter PackageName",true);
		Helper.highLightElement(Initialization.driver, Packagename);
		Packagename.sendKeys(Config.Enter_PackageNameAsMicrosoftSkypeApp);
		boolean check = Packagename.isDisplayed();
		String PassStatement = "PASS >> Package Name is entered";
		String FailStatement = "FAIL >> Package Name is did not entered";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);	 
	}  
	public void EnterProductNameEnterpriseApp()
	{
		Reporter.log("Enter Product Name",true);
		Helper.highLightElement(Initialization.driver, ProductName);
		ProductName.sendKeys(Config.Enter_ProductNameAsNotePad);
		boolean check = ProductName.isDisplayed();
		String PassStatement = "PASS >> Product Name is entered";
		String FailStatement = "FAIL >> Product Name is did not entered";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);	 
	}
	public void EnterBinaryNameEnterpriseApp()
	{
		Reporter.log("Enter Binary Name",true);
		Helper.highLightElement(Initialization.driver, BinaryName);
		BinaryName.sendKeys(Config.Enter_BinaryName);
		boolean check = BinaryName.isDisplayed();
		String PassStatement = "PASS >> Binary Name is entered";
		String FailStatement = "FAIL >> Binary Name is did not entered";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);

	}
	@FindBy(xpath="//select[@id='edpAppType']")
	private WebElement ExemptApptypeCondition;

	@FindBy(xpath="//input[@id='edpPublisher']")
	private WebElement ExemptAppPublisherName;

	@FindBy(xpath="//input[@id='edpPackage']")
	private WebElement ExemptAppPackageName;

	@FindBy(xpath="//*[@id='enterpriseDataProtectionmodal']/div/div/div[3]/button[1]")
	private WebElement AddExemptApp;

	@FindBy(xpath="//input[@id='edpPublisher']")
	private WebElement ExemptAppPublisheName;

	@FindBy(xpath="//input[@id='edpPackage']")
	private WebElement ExemptAppProductName;

	@FindBy(xpath="//input[@id='edpBinaryName']")
	private WebElement ExemptAppBinaryName;

	@FindBy(id="edpProfileConfig")
	private WebElement ConfgureButton_EnterPrisedataprotection;

	@FindBy(xpath="//h4[@class='tit edp_field mandatory']")
	private WebElement EnterPriseApplications;

	@FindBy(xpath="//h4[@class='tit edp_field']")
	private WebElement ExemptApplications;

	public void VarifyingExemptApplicationIsPresent()
	{
		Reporter.log("=======Verify Exempt Application is Available or not=======");
		String actual =ExemptApplications.getText();
		Reporter.log(actual);
		String expected = "Exempt Applications";
		String PassStatement ="PASS >> Exempt Application  is Available";
		String FailStatement ="FAIL >> Exempt Application  is not Available";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

	}

	@FindBy(xpath="//div[@id='edpProfileTail']/ul[3]/li/div[1]")
	private WebElement Primarydomain;

	@FindBy(xpath="//div[contains(text(),'Application Data Protection Level')]")
	private WebElement AppliactionDataProtectionlevel;
	public void VarifyingPrimarydomainIsPresent()
	{
		Reporter.log("=======Verify Primary domain is Available or not=======");
		String actual =Primarydomain.getText();
		Reporter.log(actual);
		String expected = "Primary Domain";
		String PassStatement ="PASS >> Primary domain  is Available";
		String FailStatement ="FAIL >> Primary domain   is not Available";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}
	public void VarifyingAppliactionDataProtectionlevel()
	{
		Reporter.log("=======Verify Appliaction Data Protection level is Available or not=======");
		String actual =AppliactionDataProtectionlevel.getText();
		Reporter.log(actual);
		String expected = "Application Data Protection Level";
		String PassStatement ="PASS >> Appliaction Data Protection level  is Available";
		String FailStatement ="FAIL >> Appliaction Data Protection level is not Available";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	} 


	public void clickOnConfigureButton_PasswordPolicy() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, ConfgureButton_PasswordPolicy);
		ConfgureButton_PasswordPolicy.click();

		waitForidPresent("addblacklistwhitelistAppsBtn");
	}

	public void EnterPasswordPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterPasswordPolicyName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void ClickOnSaveButton()
	{
		saveWindowsProfile.click();
	}
	public void WarningMessageSavingProfileWithoutName() throws InterruptedException
	{

		boolean value =true;

		try{
			warningMessageOnSavingWithoutProfileName.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Please enter a Profile Name.'is displayed",true );	
		sleep(3);
	}
	public void NotificationOnProfileCreated() throws InterruptedException{
		boolean value =true;

		try{
			NotificationOnProfileCreated.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> Profile Created Notification not displayed ");
		Reporter.log("PASS >> Notification 'Profile created successfully'is displayed",true );	
		waitForidPresent("addPolicyBtn");
		sleep(4);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}

	public void ClickOnMailConfigurationPolicy() throws InterruptedException
	{
		MailConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnConfigureButtonMailConfigurationProfile() throws InterruptedException
	{
		ConfigureButton_MailConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterMailConfigurationPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterMailConfigurationProfile);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void WarningMessageSavingProfileWithoutAllRequiredFields() throws InterruptedException
	{

		boolean value =true;

		try{
			warningMessageOnSavingProfileWithoutAllFields.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> warning message NOT displayed");
		Reporter.log("PASS >> Please enter all required fields to save profile'is displayed",true );	
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(3);
	}
	public void ClickOnAddButton() throws InterruptedException
	{
		AddButton_MailConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isMailConfigurationWindowDisplayed() throws InterruptedException
	{
		boolean isDisplayed = MailConfigurationWindowHeader.isDisplayed();
		String pass = "PASS >> Mail configuraton header displayed - Mail Configuration Window displayed successfully";
		String fail = "FAIl >> Mail configuration header not displayed - Mail configuration windows NOT displayed";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}


	public void VerifyAlltheOptionsOfMailConfigurationWindow() throws InterruptedException
	{


		Reporter.log("======Verify Total parameter count of Mail Configuration Policy======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='windowsMailConfig']/div/div/div[2]/div"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 8;
		String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify Windows Mail Configuration Policy window parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Account Name";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Your Name";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			/*if(i==2){	
		   			String actual =parameters.get(i).getText();
		   			Reporter.log(actual);
		   			String expected = "Account Type";

		   			String PassStatement ="PASS >> 3rd option is present - correct";
		   			String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
		   			ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
		   		}*/
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Email Address";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Username";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==5){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Password";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==6){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Use Cellular Network Only";
				String PassStatement ="PASS >> 7th option is present - correct";
				String FailStatement ="FAIL >> 7th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}

		}
	}
	public void DefaultTextOfYourName() throws InterruptedException
	{
		String actual = YourName.getAttribute("Value");
		System.out.println(actual);
		String expected = "We'll send your messages using this name";
		String pass = "PASS >> default text is correct";
		String fail = "FAIL >> default text is NOT correct";
		ALib.AssertEqualsMethod(expected, actual, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnCancelButtonMailConfigurationWindow() throws InterruptedException
	{
		CancelButton_MailConfigurationWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}

	public void EnterIncomingMailDetails() throws InterruptedException
	{
		AccountName.sendKeys(Config.EnterMailAccountName);
		YourName.sendKeys(Config.EnterYourName);
		EmailAddress.sendKeys(Config.Enter_Invalid_Email_Address);
		UserName.sendKeys(Config.Enter_UserName);
		password.sendKeys(Config.Enter_Password);
		Checkbox_useCellularNetworkOnly.click();
		MailServer.sendKeys(Config.Enter_MailServer);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}
	public void ClickOnAddButtonMailConfigWindow() throws InterruptedException
	{
		AddButton_MailConfigurationWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void WarningWithoutPort() throws InterruptedException
	{
		boolean warning = WarningWithoutportNumber.isDisplayed();
		String pass = "PASS >> warning 'Port is required' is displayed";
		String fail = "FAIL >> warning NOT displayed";
		ALib.AssertTrueMethod(warning, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterIncomingPort() throws InterruptedException
	{
		incomingPort.sendKeys(Config.Enter_IncomingPortNumber);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnOutgoingMail() throws InterruptedException
	{
		OutgoingMail.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterOutgoingServerAndPort() throws InterruptedException
	{
		EnterOutgoingServer.sendKeys(Config.Enter_OutgoingServer);
		EnterOutgoingPort.sendKeys(Config.Enter_OutgoingPort);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void WarningMessageOnInvalidEmailAddress_MailConfiguration() throws InterruptedException
	{

		boolean isdisplayed =true;

		try{
			WarningMessage_InvaliEmailAddress_MailConfiguration.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> the warning NOT displayed");
		Reporter.log("PASS >> 'Please enter a valid email address' is displayed",true );	
		waitForPageToLoad();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}



	public void EnterValidEmailAddress() throws InterruptedException
	{
		EmailAddress.clear();
		sleep(2);
		EmailAddress.sendKeys(Config.Enter_ValidEmailAddress);
		sleep(2);
	}

	public void VerifyAccountNameAccountTypeEmailAddressAfterAdding()
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='mailTable']/tbody/tr/td"));
		//verifying total parameters

		int totalDataCount = ls.size();
		int expectedCount = 3;
		String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
		String FailStatement2 = "FAIL >> Total Data count is Wrong";
		ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);

		//verifying data in the row
		Reporter.log("========Verify Account Name, Account Type, Email Address in the row=========");
		for(int i=0;i<totalDataCount;i++){
			if(i==0){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterMailAccountName;
				String PassStatement ="PASS >> 1st data in the row is correct";
				String FailStatement ="FAIL >> 1st data in the row is  wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.AccoutType;
				String PassStatement ="PASS >> 2nd data in the row is correct";
				String FailStatement ="FAIL >> 2nd data in the row is wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==2){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.Enter_ValidEmailAddress;
				String PassStatement ="PASS >> 3rd data in the row is correct";
				String FailStatement ="FAIL >> 3rd data in the row is wrong ";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
	}

	public void WarningMessageWithoutOutgoing_MailConfiguration() throws InterruptedException
	{

		boolean isdisplayed =true;

		try{
			warningMessageOnSavingProfileWithoutAllRequiredFields.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> d warning NOT displayed");
		Reporter.log("PASS >> 'Please enter a valid email address' is displayed",true );	
		waitForPageToLoad();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(3);
	}


	public void ClickOnRestrictionPolicy() throws InterruptedException
	{
		RestrictionPolicyProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButton_RestrictionProfile() throws InterruptedException
	{
		ConfigureButton_RestrictionProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterRestrictionPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterRestrictionProfile);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isCameraOptionAvailable()
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		boolean isPresent = CameraOption_RestrictionPolicy.isDisplayed();
		String pass = "PASS >> 'camera option' is available'";
		String fail = "FAIL >> 'Camera option' is NOT available";
		ALib.AssertTrueMethod(isPresent, pass, fail);
	}

	public void VerifyIfAllowUseOfCameraEnabled() throws InterruptedException
	{
		boolean isEnabled = isAllowUseOfCameraEnabled.isEnabled();
		String pass = "PASS >> 'Allow use of Camera is Enabled'";
		String fail ="FAIL >> 'Allow use of Camera is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isSystemOptionAvailable()
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		boolean isPresent = SystemOption_RestrictionPolicy.isDisplayed();
		String pass = "PASS >> 'camera option' is available'";
		String fail = "FAIL >> 'Camera option' is NOT available";
		ALib.AssertTrueMethod(isPresent, pass, fail);
	}


	public void VerifyIfAllOptionsofSystemEnabled() throws InterruptedException
	{
		boolean isEnabled = CheckBox_AllowUseOfStorageCard.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass = "PASS >> 'Allow use of storage card' is Enabled'";
		String fail ="FAIL >> 'Allow use of storage card' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled, pass, fail);


		boolean isEnabled1 = CheckBox_AllowtheUserToFactoryResetThePhone.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass1 = "PASS >> 'Allow the user to factory reset the phone' is Enabled'";
		String fail1 ="FAIL >> 'Allow the user to factory reset the phone' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled1, pass1, fail1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled2 = Checkbox_AllowWindowsStore.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass2 = "PASS >> 'Allow Windows Store' is Enabled'";
		String fail2 ="FAIL >> 'Allow Windows Store' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled2, pass2, fail2);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled3 = Checkbox_AllowAutomaticUpdateofAppsFromWindowsStore.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass3 = "PASS >> 'Allow automatic update of apps from Windows Store' is Enabled'";
		String fail3 ="FAIL >> 'Allow automatic update of apps from Windows Store' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled3, pass3, fail3);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isExperienceOptionAvailable() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		boolean isPresent = Experience_RestrictionPolicy.isDisplayed();
		String pass = "PASS >> 'Experience option' is available'";
		String fail = "FAIL >> 'Experience option' is NOT available";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyIfAllOptionsofExperienceEnabled() throws InterruptedException
	{
		boolean isEnabled = CheckBox_AllowCortana.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass = "PASS >> 'Allow Cortana' is Enabled'";
		String fail ="FAIL >> 'Allow Cortana' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled, pass, fail);


		boolean isEnabled1 = CheckBox_AllowDeviceDiscovery.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass1 = "PASS >> 'Allow device discovery' is Enabled'";
		String fail1 ="FAIL >> 'Allow device discovery' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled1, pass1, fail1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled2 = CheckBox_windowsManualUnenroll.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass2 = "PASS >> 'Allow manual MDM unenrollment' is Enabled'";
		String fail2 ="FAIL >> 'Allow manual MDM unenrollment' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled2, pass2, fail2);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled3 = CheckBox_windowsCopyPaste.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass3 = "PASS >> 'Allow copy and pastee' is Enabled'";
		String fail3 ="FAIL >> 'Allow copy and paste' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled3, pass3, fail3);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled4 = CheckBox_windowsScreenCapture.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass4 = "PASS >> 'Allow screen capture' is Enabled'";
		String fail4 ="FAIL >> 'Allow screen capture' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled4, pass4, pass4);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled5 = CheckBox_windowsSimCard.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass5 = "PASS >> 'Display dialog prompt when no SIM card is detected' is Enabled'";
		String fail5 ="FAIL >> 'Display dialog prompt when no SIM card is detected' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled5, pass5, fail5);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled6 = CheckBox_windowsTaskSwitching.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass6 = "PASS >> 'Allow task switching on the device' is Enabled'";
		String fail6 ="FAIL >> 'Allow task switching on the device' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled6, pass6, fail6);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		boolean isEnabled7 = CheckBox_windowsVoiceRecording.isEnabled();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String pass7 = "PASS >> 'Allow voice recording' is Enabled'";
		String fail7 ="FAIL >> 'Allow voice recording' is NOT enabled'";
		ALib.AssertTrueMethod(isEnabled7, pass7, fail7);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


	}

	public void GetDefaultLocationAttribute_RestrictionPolicy() throws InterruptedException
	{
		String actualValue =  LocationFieldDefaultText_RestrictionPolicy.getText();
		System.out.println(actualValue);
		String ExpectedValue = "Location Service is Allowed";
		String pass = "PASS >> Default text of location field is correct ";
		String fail = "FAIL >> Default text of locatioj field is NOT correct";
		ALib.AssertEqualsMethod(ExpectedValue, actualValue, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	//verify all options of location field in Restriction policy
	public void VerifyAllOptionsOfLocations() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify if all the options present in location dropdown of Restriction policy  ====");
		//verify if all the options present in dropdown
		for(int i=0; i<3;i++) // total 6 options
		{
			if(i==0){
				Initialization.driver.findElement(By.id("windowsLocation")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+219,2)+"']")).click();;
				sleep(1);
				boolean value = ForceLocationOff_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Force Location Off' is available";
				String fail="FAIL >> 'Force Location Off' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==1){
				Initialization.driver.findElement(By.id("windowsLocation")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+219,2)+"']")).click();;
				sleep(1);
				boolean value = LocationFieldDefaultText_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Location Field Default Text' is available";
				String fail="FAIL >> 'Location Field Default Text' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==2){
				Initialization.driver.findElement(By.id("windowsLocation")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+219,2)+"']")).click();;
				sleep(1);
				boolean value = ForceLocationOn_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Force Location On' is available";
				String fail="FAIL >> 'Force Location On' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}
		}
	}

	public void GetDefaultUseDeveloperFeatureAttribute_RestrictionPolicy() throws InterruptedException
	{
		String actualValue =  UseDeveloperFeaturesFieldDefaultText_RestrictionPolicy.getText();
		System.out.println(actualValue);
		String ExpectedValue = "Windows Store apps";
		String pass = "PASS >> Default text of location field is correct ";
		String fail = "FAIL >> Default text of locatioj field is NOT correct";
		ALib.AssertEqualsMethod(ExpectedValue, actualValue, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify all options of Use developer features field in Restriction policy
	public void VerifyAllOptionsOfUsedeveloperfeatures() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify if all the options present in Use developer features dropdown of Restriction policy  ====");
		//verify if all the options present in dropdown
		for(int i=0; i<3;i++) // total 6 options
		{
			if(i==0){
				Initialization.driver.findElement(By.id("windowsDevFeatures")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+224,2)+"']")).click();;
				sleep(1);
				boolean value = UseDeveloperFeaturesFieldDefaultText_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Windows Store apps' is available";
				String fail="FAIL >> 'Windows Store apps' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==1){
				Initialization.driver.findElement(By.id("windowsDevFeatures")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+224,2)+"']")).click();;
				sleep(1);
				boolean value = SideloadApps_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Sideload apps' is available";
				String fail="FAIL >> 'Sideload apps' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==2){
				Initialization.driver.findElement(By.id("windowsDevFeatures")).click();
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+224,2)+"']")).click();;
				sleep(1);
				boolean value = DeveloperMode_RestrictionPolicy.isDisplayed();
				String pass="PASS >> 'Developer mode' is available";
				String fail="FAIL >> 'Developer mode' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}
		}
	}

	public void ClickOnAppLockerPolicy() throws InterruptedException
	{
		AppLockerPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of AppLocker
	public void VerifySummaryof_AppLocker() throws InterruptedException{


		String appLockersummary =AppLockerSummary.getText();
		Reporter.log(appLockersummary);
		sleep(2);
		boolean value = AppLockerSummary.isDisplayed();
		String PassStatement1 = "PASS >> app locker summary is correct";
		String FailStatement1 = "FAIL >>app locker summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnConfigureButton_AppLockerProfile() throws InterruptedException
	{
		ConfigureButton_AppLocker.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnAppLocker() throws InterruptedException
	{
		AppLockerPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnAddButton_AppLocker() throws InterruptedException
	{
		AddButton_AppLocker.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		boolean value = AppLockerWindow_HeaderText.isDisplayed();
		String pass = "PASS >> App Locker window is displayed- Clicked on Add button";
		String fail = "FAIL >> App Locker window is not displayed or the title is wrong or Did not click on Add Button";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(2);

	}

	public void EnterAppLockerPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterAppLockerProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	
	public void VerifyAlltheParametersOfAppLockerWindow() throws InterruptedException
	{

		Reporter.log("======Verify Total parameter count of App Locker window======");
		WebElement[] AppLockerElement = {ActionParameter,PublisherParameter,packageNameParameter};
		String[] AppLockerText = { "Action", "Publisher", "Package Name"};
		for (int i = 0; i < AppLockerElement.length - 1; i++) 
		{
			boolean value=AppLockerElement[i].isDisplayed();
			String PassStatement = "PASS >> " + AppLockerText[i]+ " is displayed successfully when clicked on Apply Job Button";
			String FailStatement = "Fail >> " + AppLockerText[i] + " is not displayed even when clicked on Apply Job Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	public void AddButton_AppLockerWindow() throws InterruptedException
	{
		AddButton_AppLockerWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnCancelButton_AppLockerWindow()throws InterruptedException
	{
		CancelButton_AppLockerWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void EnterPublisherAndPackageName_AllowType() throws InterruptedException
	{
		publisher_AppLocker.sendKeys(Config.EnterPublisher);
		PackageName_AppLocker.sendKeys(Config.EnterPackageName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void VerifyPublisherPackageNameActionAfterAdding_Allow()
    {
List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='appLockerTable']/tbody/tr/td"));
//verifying total parameters
              
              int totalDataCount = ls.size();
              int expectedCount = 4;
              String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
              String FailStatement2 = "FAIL >> Total Data count is Wrong";
              ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);
              
              //verifying data in the row
              Reporter.log("========Verify publisher,package name, action in the row=========");
              for(int i=0;i<totalDataCount;i++){
              if(i==0){        
                  String actual =ls.get(i).getText();
                  Reporter.log(actual);
                  String expected = Config.EnterPublisher;
                  String PassStatement ="PASS >> 1st data in the row is correct";
                  String FailStatement ="FAIL >> 1st data in the row is  wrong";
                  ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
              }
              if(i==1){        
                      String actual =ls.get(i).getText();
                      Reporter.log(actual);
                      String expected = Config.EnterPackageName;
                      String PassStatement ="PASS >> 2nd data in the row is correct";
                      String FailStatement ="FAIL >> 2nd data in the row is wrong";
                      ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
              }
          
              if(i==2){        
                      String actual =ls.get(i).getText();
                      Reporter.log(actual);
                      String expected = Config.Action;
                      String PassStatement ="PASS >> 3rd data in the row is correct";
                      String FailStatement ="FAIL >> 3rd data in the row is wrong ";
                      ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
              }

    }
    }
	
	public void ClickOnDenyRadioButton() throws InterruptedException
	{
		Deny_RadioButton_AppLockerWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void EnterPublisherAndPackageName_DenyType() throws InterruptedException
	{
		publisher_AppLocker.sendKeys(Config.EnterPublisherDeny);
		PackageName_AppLocker.sendKeys(Config.EnterPackageNameDeny);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void VerifyPublisherPackageNameActionAfterAdding_Deny() throws InterruptedException
    {
List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='appLockerTable']/tbody/tr[2]/td"));
//verifying total parameters
              
              int totalDataCount = ls.size();
              int expectedCount = 4;
              String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
              String FailStatement2 = "FAIL >> Total Data count is Wrong";
              ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);
              
              //verifying data in the row
              Reporter.log("========Verify publisher,package name, action in the row=========");
              for(int i=0;i<totalDataCount;i++){
              if(i==0){        
                  String actual =ls.get(i).getText();
                  Reporter.log(actual);
                  String expected = Config.EnterPublisherDeny;
                  String PassStatement ="PASS >> 1st data in the row is correct";
                  String FailStatement ="FAIL >> 1st data in the row is  wrong";
                  ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
              }
              if(i==1){        
                      String actual =ls.get(i).getText();
                      Reporter.log(actual);
                      String expected = Config.EnterPackageNameDeny;
                      String PassStatement ="PASS >> 2nd data in the row is correct";
                      String FailStatement ="FAIL >> 2nd data in the row is wrong";
                      ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
              }
          
              if(i==2){        
                      String actual =ls.get(i).getText();
                      Reporter.log(actual);
                      String expected = Config.ActionDeny;
                      String PassStatement ="PASS >> 3rd data in the row is correct";
                      String FailStatement ="FAIL >> 3rd data in the row is wrong ";
                      ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
                      Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                     sleep(1);
              }

    }
    }
	
	public void ClickOnFirstRowDataOfAppLocker() throws InterruptedException
	{
		FirstRow_AppLocker.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);

	}
	public void ClickOnEditButtonAppLocker() throws InterruptedException
	{
		EditButton_AppLocker.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void RenamePublisher_AppLocker() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		publisher_AppLocker.clear();
		publisher_AppLocker.sendKeys(Config.RenamePublisher);
		sleep(2);
	}
	public void isRenamedPublisherNameSaved() throws InterruptedException
	{
		boolean isDisplayed =  Rename_Publisher_FirstRow_AppLocker.isDisplayed();
		String pass = "PASS >> New Name for Publisher Name saved - Editing of the First row data worked";
		String fail = "FAIL >> New Name for the Publisher did not saved -  Editing failed";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void SaveButton_AppLockerWindow() throws InterruptedException // Button is 'Save' on edit window
	{
		SaveButton_AppLockerWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnDeleteButton_AppLocker() throws InterruptedException
	{
		DeleteButon_AppLocke.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnNoButtonWarningDialog() throws InterruptedException
	{
		NoButton_DeleteWarningDialog.click();
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		boolean isDeleteDisplayed = DeleteButon_AppLocke.isDisplayed();
		String pass = "PASS >> Delete button is displayed - Warning pop up cancelled successfully";
		String fail = "FAIl >> Delete button is NOT displayed - warning pop up NOT cancelled successfully";
		ALib.AssertTrueMethod(isDeleteDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnYesButtonWarningDialog() throws InterruptedException
	{
		YesButton_DeleteWarningDialog.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		List<WebElement> countAfterDelete = Initialization.driver.findElements(By.xpath(".//*[@id='appLockerTable']/tbody/tr/td"));
		int Actualremaining = countAfterDelete.size();
		int expected = 4;
		String pass = "PASS >> Count is: '+expected+'- Deleted successfully ";
		String fail = "FAIL >> Count is wrong - Not Deleted the 1st row";
		ALib.AssertEqualsMethodInt(expected, Actualremaining, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//Click On Wifi Configuration
	public void ClickOnWifiConfigurationPolicy() throws InterruptedException
	{
		WifiConfigurationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButton_WifiConfigurationProfile() throws InterruptedException
	{
		ConfigureButton_WifiConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterWifiConfigurationPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterWifiConfigProfile);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnAddButton_WifiConfiguration() throws InterruptedException
	{
		AddButton_WifiConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		// boolean value = WifiConfigurationWindow_HeaderText.isDisplayed();
		//String pass = "PASS >> Wifi Configuration window is launched- Clicked on Add button";
		//String fail = "FAIL >> Wifi Configuration window is not launched or the title is wrong or Did not click on Add Button";
		//ALib.AssertTrueMethod(value, pass, fail);
		// sleep(2);
	}

	public void VerifyAlltheParametersOfWifiConfigurationWindow() throws InterruptedException
	{

		Reporter.log("======Verify Total parameter count of Wifi Configuration window======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='windowsWifiConfig']/div/div/div[2]/div/span[1]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 7; //at 4th position 'password' parameter is hidden.
		String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify App Locker window parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "SSID";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Security Type";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==2){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Encryption Type";
				String PassStatement ="PASS >> 3rd option is present - correct";
				String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			/*
		   	  if(i==3){	
		   	      String actual =parameters.get(i).getText();
		   		  Reporter.log(actual);
		   		  String expected = "Disable Internet Connectivity Checks";
		   		  String PassStatement ="PASS >> 4th option is present - correct";
		   		  String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
		   		  ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
		   		  Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  	  sleep(2);
		   		}
			 */

			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Disable Internet Connectivity Checks";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}

			if(i==5){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Hidden Network";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			if(i==6){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Auto Join";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
		}
	}

	public void AddButton_WifiConfigurationWindow() throws InterruptedException
	{
		AddButton_WifiConfigurationWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	 public void EnterSSID_WifiConfiguration() throws InterruptedException
     {
    	 SSID_WifiConfig.sendKeys(Config.EnterSSIDWifiConfig);
    	
     }
	 
	public void ClickOnCancelButton_WifiConfigurationWindow()throws InterruptedException
	{
		CancelButton_WifiConfigurationWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);

	}

	// Verifying all the Options of Security Type Dropdown of Wifi Configuration Profiles
	public void VerifyAllOptionsOfSecurityTypeDropDown() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
        Reporter.log("=======verify if all the options present in Security Type dropdown ====");
            //verify if all the options present in dropdown
           Initialization.driver.findElement(By.id("windowsSecurityType_chosen")).click();
           sleep(2);
           
           List<WebElement> a = Initialization.driver.findElements(By.xpath("//*[@id=\"windowsSecurityType_chosen\"]/div/ul/li"));
           int count = a.size();
           for(int i=0;i<count;i++)
           {
                   if(i==0)
                   {
                           String actual = a.get(i).getText();
                           String expected = "Open";
                           String pass = "First option is present: "+actual;
                           String fail = "First option is : "+actual;
                           ALib.AssertEqualsMethod(expected, actual, pass, fail);
                   }
                   
                   if(i==1)
                   {
                           String actual = a.get(i).getText();
                           String expected = "WPA-Enterprise";
                           String pass = "2nd option is present: "+actual;
                           String fail = "2nd option is : "+actual;
                           ALib.AssertEqualsMethod(expected, actual, pass, fail);
                   }
                   
                   if(i==2)
                   {
                           String actual = a.get(i).getText();
                           String expected = "WPA-Personal";
                           String pass = "3rd option is present: "+actual;
                           String fail = "3rd option is : "+actual;
                           ALib.AssertEqualsMethod(expected, actual, pass, fail);
                   }
                   
                   if(i==3)
                   {
                           String actual = a.get(i).getText();
                           String expected = "WPA2-Enterprise";
                           String pass = "4th option is present: "+actual;
                           String fail = "4th option is : "+actual;
                           ALib.AssertEqualsMethod(expected, actual, pass, fail);
                   }
                   
                   if(i==4)
                   {
                           String actual = a.get(i).getText();
                           String expected = "WPA2-Personal";
                           String pass = "5th option is present: "+actual;
                           String fail = "5th option is : "+actual;
                           ALib.AssertEqualsMethod(expected, actual, pass, fail);
                   }
                   sleep(2);
           }
}
	
	public void EnterSSID_Password_WifiConfiguration() throws InterruptedException
	{
		SSID_WifiConfig.sendKeys(Config.EnterSSIDWifiConfig);
		Password_WifiConfig.sendKeys(Config.EnterPasswordWifiConfig);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifySSID_SecurityTypeAfterAdding() throws InterruptedException
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='windowsWifiTable']/tbody/tr/td"));
		//verifying total parameters

		int totalDataCount = ls.size();
		int expectedCount = 3;
		String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
		String FailStatement2 = "FAIL >> Total Data count is Wrong";
		ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);

		//verifying data in the row
		Reporter.log("========Verify publisher,package name, action in the row=========");
		for(int i=0;i<totalDataCount;i++){
			if(i==0){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterSSIDWifiConfig;
				String PassStatement ="PASS >> 1st data in the row is correct";
				String FailStatement ="FAIL >> 1st data in the row is  wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterSecurityType;
				String PassStatement ="PASS >> 2nd data in the row is correct";
				String FailStatement ="FAIL >> 2nd data in the row is wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==2){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterEncryptionType;
				String PassStatement ="PASS >> 3rd data in the row is correct";
				String FailStatement ="FAIL >> 3rd data in the row is wrong ";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}

		}
	}

	public void ClickOnVPNConfiguration() throws InterruptedException
	{
		VPNConfigurationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButton_VPNConfigurationProfile() throws InterruptedException
    {
       
       List<WebElement> vpnConfigButton = Initialization.driver.findElements(By.xpath(".//*[@id='VPNProfileConfig']"));
       vpnConfigButton.get(1).click();
           Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
           sleep(2);
}

	public void EnterVPNConfigurationPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterVPNConfigProfile);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnPeriodicAppLaunch() throws InterruptedException
	{
		PeriodicAppLaunchPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnConfigureButton_PeriodicAppLaunchProfile() throws InterruptedException
	{
		ConfigureButton_PeriodicAppLaunchConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyAlltheParametersOfPeriodicAppLaunchWindow() throws InterruptedException
    {
    
            Reporter.log("======Verify Total parameter count of Periodic App Launch window======");
        List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='windowsAppLaunchTail']/ul/li/div[1]"));
            //verifying total parameters
                     
               int totalParameters = parameters.size();
               int expectedParameters = 2; 
               String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
               String FailStatement2 = "FAIL >> Total parameter count is Wrong";
               ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);
                     
               //verifying parameters one by one
               Reporter.log("========Verify periodic app launch window parameters one by one=========");
               for(int i=0;i<totalParameters;i++){
               if(i==0){        
                       String actual =parameters.get(i).getText();
                       Reporter.log(actual);
                       String expected = "Applications";
                       String PassStatement ="PASS >> 1st option is present - correct";
                       String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
                       ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
                     }
               if(i==1){        
                       String actual =parameters.get(i).getText();
                       Reporter.log(actual);
                       String expected = "Periodicity (In Mins.)";
                       String PassStatement ="PASS >> 2nd option is present - correct";
                       String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
                       ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
                       Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                    sleep(2);
                     }
               }
    }

	// Negative Test Case
	public void EnterPeriodAppLaunchLessThanOne() throws InterruptedException
	{
		PeriodicityInMins_PeriodicAppLaunch.clear();
		sleep(1);
		PeriodicityInMins_PeriodicAppLaunch.sendKeys(Config.EnterPeriodicAppLaunchTimeLessThan1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterPeriodAppLaunchTime() throws InterruptedException
	{
		PeriodicityInMins_PeriodicAppLaunch.clear();
		sleep(1);
		PeriodicityInMins_PeriodicAppLaunch.sendKeys(Config.EnterPeriodicAppLaunchTime);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterPeriodicAppLaunchProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterPeriodicAppLaunchProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnExchangeActiveSyncPolicy()
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ExchangeActiveSyncPolicy.click();
	}

	public void ClickOnConfigureButton_ExchangeActiveSyncProfile() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ConfigureButton_ExchangeActiveSync.click();
	}

	public void EnterExchangeActiveSyncProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterExchangeActiveSyncProfileNameWin);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnAddButton_ExchangeActiveSync() throws InterruptedException
	{
		AddButton_ExchangeActive.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		boolean value = ExchangeActiveSyncWindow_HeaderText.isDisplayed();
		String pass = "PASS >> Exchange ActiveSync window is launched- Clicked on Add button";
		String fail = "FAIL >> Exchange ActiveSync window is not launched or the title is wrong or Did not click on Add Button";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(2);
	}

	public void VerifyAlltheParametersOfExchangeActiveSyncWindow() throws InterruptedException
	{

		Reporter.log("======Verify Total parameter count of Exchange ActiveSync window======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='windowsExchangeConfig']/div/div/div[2]/div/span[1]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 8; //at 4th position 'password' parameter is hidden.
		String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify Exchange ActiveSync window parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Account Name";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Exchange ActiveSync Host";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==2){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Use SSL";
				String PassStatement ="PASS >> 3rd option is present - correct";
				String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Username";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Email Address";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}

			if(i==5){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Password";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			if(i==6){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Sync Schedule";
				String PassStatement ="PASS >> 7th option is present - correct";
				String FailStatement ="FAIL >> 7th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}

			if(i==7){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Days To Sync";
				String PassStatement ="PASS >> 8th option is present - correct";
				String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
		}
	}

	public void VerifyAlltheWarningToastsOfTheParametersWithoutTheirValue() throws InterruptedException
	{

		Reporter.log("======Verify Total warning count of all the parameters of Exchange ActiveSync window======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='windowsExchangeConfig']/div/div/div[2]/div/span[2]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 6; //at 4th position 'password' parameter is hidden.
		String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify warning toasts of Exchange ActiveSync parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Please fill this field";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Please fill this field";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==2){	
				String actual =parameters.get(i).getTagName();
				Reporter.log(actual);
				//String expected = "Please fill this field";
				//String PassStatement ="PASS >> 3rd option is present - correct";
				//String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				//ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				//Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				//sleep(2);
			}
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Please fill this field";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Please fill this field";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}


		}
	}
	public void ClickOnAddButton_ExchangeActiveSyncWindow() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		AddButton_ExchangeActiveSyncWindow.click();
		sleep(1);
	}

	public void verifyDefaultValuesofSyncScheduleAndDaysToSync() throws InterruptedException
    {
            // Default value of Sync Schedule in Exchange ActiveSync
   
            String ActualValue = Initialization.driver.findElement(By.id("exchangeWindowsScheduleSync_chosen")).getText();
            String ExpectedValue = "Default";
            String pass = "PASS >> Default value of 'sync schedule' is 'Default'";
            String fail = "FAIL >> Default value of 'sync schedule' is wrong";
            ALib.AssertEqualsMethod(ExpectedValue, ActualValue, pass, fail);
            sleep(2);
            
            String ActualValue1 = Initialization.driver.findElement(By.id("exchangeWindowsDaysSync_chosen")).getText();
            String ExpectedValue1 = "1 Week";
            String pass1 = "PASS >> Default value of 'Days to Sync' is '1 Week";
            String fail1 = "FAIL >> Default value of 'Days to Sync' is '1 Week";
            ALib.AssertEqualsMethod(ExpectedValue1, ActualValue1, pass1, fail1);
            sleep(2);

   }
	
	public void ClickOnCancelButton_ExchangeActiveSync() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CancelButton_ExchangeActiveSync.click();
		sleep(2);
		boolean isAddButtonDisplayed = AddButton_ExchangeActive.isDisplayed();
		String pass = "PASS >> Add button is displayed -  Cancelled Exchange ActiveSync window successfully";
		String fail = "FAIL >> Add button is NOT displayed - did not cancel Exchange ActiveSync Window successfully";
		ALib.AssertTrueMethod(isAddButtonDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyIfUseSSLEnabledByDefault() throws InterruptedException
	{
		boolean isEnabled = CheckBox_UseSSL.isEnabled();
		String pass = "PASS >> 'Use SSL' checkbox is enabled by default";
		String fail = "FAIL >> 'Use SSL' checkbox is disabled by default";
		ALib.AssertTrueMethod(isEnabled, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void VerifyAllOptionsOfSyncScheduleDropdown() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify if all the options present in sync schedule dropdown of Exchange ActiveSync policy  ====");
		//verify if all the options present in dropdown
		for(int i=0; i<4;i++) // total 5 options .. one is default not writing for that
		{
			if(i==0){
				Initialization.driver.findElement(By.id("exchangeWindowsScheduleSync")).click();
				sleep(1);
				Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+242,2)+"']")).click();

				/*
   			sleep(1);
 		    boolean value = SyncSchedule_Manual.isDisplayed();
 		    String pass="PASS >> 'Manual' is available";
       	    String fail="FAIL >> 'Manual' is NOT available";
       	    ALib.AssertTrueMethod(value, pass, fail);
   		    }

   		    if(i==1){
    		    Initialization.driver.findElement(By.id("exchangeWindowsScheduleSync")).click();
    		    sleep(1);
    			Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+241,2)+"']")).click();;
  		    sleep(1);
  		    boolean value = SyncSchedule_Every15Minutes.isDisplayed();
  		    String pass="PASS >> 'Every 15 Minutes' is available";
        	    String fail="FAIL >> 'Every 15 Minutes' is NOT available";
        	    ALib.AssertTrueMethod(value, pass, fail);
   		    }

   		   if(i==2){
 		    Initialization.driver.findElement(By.id("exchangeWindowsScheduleSync")).click();
 		    sleep(1);
 		    Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+241,2)+"']")).click();;
		    sleep(1);
		    boolean value = SyncSchedule_Every30Minutes.isDisplayed();
		    String pass="PASS >> 'Every 30 Minutes' is available";
     	    String fail="FAIL >> 'Every 30 Minutes' is NOT available";
     	    ALib.AssertTrueMethod(value, pass, fail);
		    }

   		  if(i==3){
  		    Initialization.driver.findElement(By.id("exchangeWindowsScheduleSync")).click();
  		    sleep(1);
  		    Initialization.driver.findElement(By.xpath("//option[text()='"+ELib.getDatafromExcel("Sheet15",i+241,2)+"']")).click();;
 		    sleep(1);
 		    boolean value = SyncSchedule_Every60Minutes.isDisplayed();
 		    String pass="PASS >> 'Every 60 Minutes' is available";
      	    String fail="FAIL >> 'Every 60 Minutes' is NOT available";
      	    ALib.AssertTrueMethod(value, pass, fail);
 		    }
   		    }
				 */

			}
		}
	}

	public void EnterExchangeActiveSyncDetails() throws InterruptedException
	{
		AccountName_ExchangeActiveSync.sendKeys(Config.EnterAccountName_ExchangeActiveSync);
		ExchangeActiveSyncHost.sendKeys(Config.EnterActiveSyncHost);
		Username_ExchangeActiveSync.sendKeys(Config.EnterActiveSyncHost);
		EmailAddress_ExchangeActiveSync.sendKeys(Config.EnterInvalidEmailAddressExchangeActiveSyncWin);
		Password_ExchangeActiveSync.sendKeys(Config.EnterPasswordExchangeActivceSync);
		sleep(4);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void EnterValidEmailAddressExchangeActiveSync() throws InterruptedException
	{
		EmailAddress_ExchangeActiveSync.clear();
		sleep(1);
		EmailAddress_ExchangeActiveSync.sendKeys(Config.EnterValidEmailAddressExchangeActiveSyncWin);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public void VerifyAccountName_EmailAddressAfterAdding_ExchangeActiveSync()
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='exchangeTable']/tbody/tr/td"));
		//verifying total parameters

		int totalDataCount = ls.size();
		int expectedCount = 2;
		String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
		String FailStatement2 = "FAIL >> Total Data count is Wrong";
		ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);

		//verifying data in the row
		Reporter.log("========Verify 'Account Name' and 'Email Address' in the row=========");
		for(int i=0;i<totalDataCount;i++){
			if(i==0){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterAccountName_ExchangeActiveSync;
				String PassStatement ="PASS >> 1st data in the row is correct";
				String FailStatement ="FAIL >> 1st data in the row is  wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterValidEmailAddressExchangeActiveSyncWin;
				String PassStatement ="PASS >> 2nd data in the row is correct";
				String FailStatement ="FAIL >> 2nd data in the row is wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
	}

	// Enter the VPN configuration Profile Name
	public void EnterVPNConfigurationProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterVPNConfigurationProfile);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	//Click on Application Policy
	public void ClickOnApplicationPolicy() throws InterruptedException
	{
		ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Application Policy
	public void VerifySummaryof_ApplicationPolicy() throws InterruptedException{

		List<WebElement> windowSummary = Initialization.driver.findElements(By.xpath("//h5[text()='Use this section to add application to install on your device.']"));

		boolean value = windowSummary.get(2).isDisplayed(); // getting the summary of Application policy
		String PassStatement1 = "PASS >> application policy summary is correct";
		String FailStatement1 = "FAIL >> application policy summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButton_ApplicationPolicyProfile() throws InterruptedException
	{
		ConfigureButton_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterApplicationPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterAppLockerProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnAddButton_ApplicationPolicy() throws InterruptedException
	{
		AddButton_ApplicaionPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(4);
	}

	public void ClickOnAddButtonInAddAppWindow() throws InterruptedException
	{
		AddButtonInAddApp.click();
		sleep(4);
	}

	@FindBy(xpath="//*[@id='windowsStore_appList_chosen']/div/ul/li")
	private WebElement ClickOnTheSearchedApp;

	@FindBy(xpath="//*[@id='windowsStore_appList_chosen']/div/div/input")
	private WebElement EnterAppName;
	public void EnterApplicationName() throws InterruptedException, AWTException
	{
		EnterAppName.sendKeys("Skype");
		sleep(2);
		ClickOnTheSearchedApp.click();
		sleep(4);
	}

	@FindBy(xpath="//*[@id='windowsStore_appList_chosen']/a/span[text()='Choose App']")
	private WebElement ChooseApp;
	@FindBy(xpath="//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButtonInAddApp;

	public void ClickOnChooseApp()
	{
		ChooseApp.click();
	}

	@FindBy(xpath="//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButtonAddAppPopUp;
	public void ClickOnAddButtonAddAppWindow() throws InterruptedException
	{
		AddButtonAddAppPopUp.click();
		sleep(2);
	}
	public void VerifyAlltheParametersOfApplicationPolicyWindow() throws InterruptedException
	{

		Reporter.log("======Verify Total parameter count of Application Policy window======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='Windows_addAppPolicyModal']/div/div/div[2]/div/span"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 2;
		String PassStatement2 = "PASS >> Total parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify Application Policy window parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Name";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Enter Url";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
	}

	public void AddButton_ApplicationPolicyWindow() throws InterruptedException
	{
		AddButton_ApplicationPolicyWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnCancelButton_AddAppWindow()throws InterruptedException
	{
		CancelButton_AddAppWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void EnterNameAndEnterUrl_AddAppWindowApplicationPolicy() throws InterruptedException
	{
		Name_AddAppWindow_ApplicationPolicy.sendKeys(Config.EnterName);
		AddButton_ApplicationPolicyWindow();
		WarningMessageWithoutOutgoing_MailConfiguration();
		Name_AddAppWindow_ApplicationPolicy.clear();
		EnterUrl_AddAppWindow_ApplicationPolicy.sendKeys(Config.EnterUrl);
		AddButton_ApplicationPolicyWindow();
		WarningMessageWithoutOutgoing_MailConfiguration();
		Name_AddAppWindow_ApplicationPolicy.sendKeys(Config.EnterName);
	}

	public void VerifyNameAppUrlAfterAdding()
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='windows_installApplicationPolicyTable']/tbody/tr/td"));
		//verifying total parameters

		int totalDataCount = ls.size();
		int expectedCount = 2;
		String PassStatement2 = "PASS >> Total total Data count is: "+expectedCount+"  ";
		String FailStatement2 = "FAIL >> Total Data count is Wrong";
		ALib.AssertEqualsMethodInt(expectedCount, totalDataCount, PassStatement2, FailStatement2);

		//verifying data in the row
		Reporter.log("========Verify Name,App Url in the row=========");
		for(int i=0;i<totalDataCount;i++){
			if(i==0){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterName;
				String PassStatement ="PASS >> 1st data in the row is correct- App got added successfully";
				String FailStatement ="FAIL >> 1st data in the row is  wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =ls.get(i).getText();
				Reporter.log(actual);
				String expected = Config.EnterUrl;
				String PassStatement ="PASS >> 2nd data in the row is correct - App got added successfully";
				String FailStatement ="FAIL >> 2nd data in the row is wrong";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
	}

	public void ClickOnFirstRowDataOfApplicationPolicy() throws InterruptedException
	{
		FirstRow_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnEditButtonApplicationPolicy() throws InterruptedException
	{
		EditButton_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		//List<WebElement> header =  Initialization.driver.findElements(By.xpath("//h4[text()='Add App']"));
		WebElement el = Initialization.driver.findElement(By.xpath(".//*[@id='Windows_addAppPolicyModal']/div/div/div[3]/button[1]"));
		boolean value = el.isDisplayed(); // Capturing the header text of the window
		String pass = "PASS >> Application Policy window is displayed- Clicked on Edit  button";
		String fail = "FAIL >> Application Policy window is not displayed or the title is wrong or Did not click on Edit Button";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(2);
	}

	public void RenameName_ApplicationPolicy() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Name_AddAppWindow_ApplicationPolicy.clear();
		Name_AddAppWindow_ApplicationPolicy.sendKeys(Config.EnterNewName);
		sleep(2);
	}

	public void isRenamedNameSaved_ApplicationPolicy() throws InterruptedException
	{
		boolean isDisplayed =  Rename_Name_FirstRow_ApplicationPolicy.isDisplayed();
		String pass = "PASS >> New Name for Name saved - Editing of the First row data worked";
		String fail = "FAIL >> New Name for Name did not save -  Editing failed";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnDeleteButton_ApplicationPolicy() throws InterruptedException
	{
		DeleteButon_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnSaveButton_AddAppWindow_ApplicationPolicy() throws InterruptedException
	{
		SaveButton_AddAppWindow_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnNoButtonWarningDialog_ApplicationPolicy() throws InterruptedException
	{
		NoButton_DeleteWarningDialog.click();
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		boolean isDeleteDisplayed = DeleteButon_ApplicationPolicy.isDisplayed();
		String pass = "PASS >> Delete button is displayed - Warning pop up cancelled successfully";
		String fail = "FAIl >> Delete button is NOT displayed - warning pop up NOT cancelled successfully";
		ALib.AssertTrueMethod(isDeleteDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

}

























