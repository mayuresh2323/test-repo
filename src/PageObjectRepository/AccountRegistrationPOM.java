package PageObjectRepository;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class AccountRegistrationPOM extends WebDriverCommonLib {
	AssertLib ALib=new AssertLib();
	String parentwinID;
	String childwinID;
	String URL;

	@FindBy(id="SignupBtn")
	private WebElement ClickOnNextStep;

	@FindBy(id="forgot_password")
	private WebElement forgotpasswordBtn;

	@FindBy(xpath="//section[@id='signupForm-widget2']//div[@class='mess_line warning-mess-area h5-txt-line font-normal']")
	private WebElement acceptlicenseagreementErrMsg;


	@FindBy(xpath="//input[@id='tel-phone-num']/../div")
	private WebElement phoneDropDown;

	@FindBy(id="user_email")
	private WebElement EnterEmailID;

	@FindBy(id="user_pass1")
	private WebElement EnterPassword;

	@FindBy(id="signup")
	private WebElement SignupBtn;

	@FindBy(xpath="//span[text()='Create a new account']")
	private WebElement ClickOnNewUserSignUpLink;

	@FindBy(id="tel-phone-num")
	private WebElement EnterPhoneNumber;

	@FindBy(xpath="//form[@id='reg_form1']/p")
	private WebElement trialMessage;

	@FindBy(id="userProfileButton")
	private WebElement settings;

	@FindBy(id="logoutButton")
	private WebElement logoutBtn;

	@FindBy(id="user_pass2")
	private WebElement ConfirmPassword;


	@FindBy(xpath="//input[@id='user_email']/..")
	private WebElement emailIDTickMark;

	@FindBy(xpath="//section[@id='signupForm-widget2']//div[@class='mess_line warning-mess-area h5-txt-line font-normal']")
	private WebElement ProvideRequiredFieldsErrMSg;


	@FindBy(xpath="//*[@id='pass_str_box']/div[2]/span")
	private WebElement passwordStrength;

	@FindBy(xpath="//div[@id='signup_mess']")
	private WebElement invalidEmailIDMessage;

	@FindBy(xpath="//*[@id='selectCountry']")
	private WebElement SelectCountryClick;

	@FindBy(xpath="//*[@id='selectState']")
	private WebElement SelectStateClick;


	@FindBy(xpath="//li[@data-value='India']")
	private WebElement SelectCountryIndia;

	@FindBy(xpath="//li[@data-value='Karnataka']")
	private WebElement SelectState;

	@FindBy(id="TextCompName")
	private WebElement companyName;

	//@FindBy(id="new_dns_customer")

	@FindBy(xpath="//*[@id='account_region']")
	private WebElement DomainName;

	@FindBy(id="TextFName")
	private WebElement Name;

	@FindBy(xpath="//*[@id=\"typeOfUse\"]")
	private WebElement typeOfUse;  

	@FindBy(xpath="//li[text()='Business']")
	private WebElement selectBusiness;

	@FindBy(id="TextStateName")
	private WebElement State;

	@FindBy(xpath="//label[@for='acceptCommunication']")
	private WebElement acceptCommunication;

	@FindBy(xpath="//label[@for='licenseagreement']")
	private WebElement licenseAgreement;

	@FindBy(xpath="//select[@id='account_region']/../input")
	private WebElement AccountRegion;

	@FindBy(xpath="//div[@id='account_region_dropdown']/ul/li[3]")
	private WebElement SelectAccountRegionIndia;

	@FindBy(xpath="//div[@id='account_region_dropdown']/ul/li[1]")
	private WebElement SelectAccountRegionUS;

	@FindBy(xpath="//span[text()='+91']/..")
	private WebElement SelectIndia;

	@FindBy(xpath="//div[@class='accept_terms_cont ifPartnership']/span[2]/a[1]")
	private WebElement termOfUseLink;

	@FindBy(xpath="//div[@class='accept_terms_cont ifPartnership']/span[2]/a[2]")
	private WebElement DataProcessorAddendumLink;

	@FindBy(xpath="//div[@class='accept_terms_cont ifPartnership']/span[2]/a[3]")
	private WebElement PrivacyPolicyLink;

	@FindBy(id="postalCode")
	private WebElement postalCode;

	@FindBy(name="intercom-launcher-frame")
	private WebElement intercomLauncherFrame;


	//AccountRegistration SignUp
	public void logoutfromApp() throws InterruptedException{
		waitForidPresent("userProfileButton");
		settings.click();
		waitForidPresent("generateQRCode");
		sleep(7);
		logoutBtn.click();
		sleep(2);
	}
	public void ClickOnNewUserSignUpLink() throws InterruptedException
	{
		ClickOnNewUserSignUpLink.click();

		sleep(3);
	}

	public void verifyFreeTrialText() throws InterruptedException
	{
		String FreeTrialMessage =trialMessage.getText();
		sleep(3);
		if(!(FreeTrialMessage.equalsIgnoreCase("Start Your Free 30-day Trial"))) {
			ALib.AssertFailMethod("30-day Free Trial message doesnt appear");
		}
		Reporter.log("30-day Free Trial messag has been displayed",true);
	}

	public void EnterInvalidEmailID() throws IOException{


		EnterEmailID.sendKeys(Config.invalidSignUPEnterEmailID);
		EnterPassword.sendKeys(Keys.TAB);
	}

	public void EnterPassword(){
		EnterPassword.sendKeys(Config.signUPEnterPassword);

	}

	public void EnterweakPassword(){
		EnterPassword.sendKeys(Config.signUPEnterInvalidPassword);
	}


	public void ConfirmPassword() throws InterruptedException{
		ConfirmPassword.sendKeys(Config.signUPConfirmPassword);
		sleep(6);
		ClickOnNextStep.click();
		sleep(3);
	}

	public void EnterEmailID() throws IOException{

		EnterEmailID.clear();
		EnterEmailID.sendKeys(Config.signUPEnterEmailID);
	}


	public void VerifySignupforNewDNS() throws IOException{
		boolean signup = SignupBtn.isDisplayed();
		if(signup) {
			ALib.AssertFailMethod("Sign up Button should not displayed");
		}else {
			Reporter.log("Pass >> Sign up Button did not displayed",true);}

		boolean ForgotPassword = forgotpasswordBtn.isDisplayed();
		if(ForgotPassword) {
			Reporter.log("Pass >> Forgot Password up Button is displayed",true);

		}else {
			ALib.AssertFailMethod("Forgot Password up Button is not displayed");}

	}


	public void verifyInvalidEmailidErrorMessage() throws InterruptedException{
		String invalidEmailIDErrorMessage = invalidEmailIDMessage.getText();
		sleep(3);
		Reporter.log("Invalid Email Id messag has been displayed",true);
		if(!(invalidEmailIDErrorMessage.equalsIgnoreCase("Invalid work email."))) {
			ALib.AssertFailMethod("Invalid Email Id message doesnt appear");
		}
	}	
	public void verifyStrengthofPassword() throws InterruptedException{
		if(passwordStrength.isDisplayed()) {
			Reporter.log("Password strength has been displayed",true);
		}else {
			ALib.AssertFailMethod("Password strength has not displayed");
		}
	}

	public void verifyEmailIDTickMark() throws InterruptedException{
		sleep(3);
		String tickMark = emailIDTickMark.getAttribute("class");
		if(tickMark.contains("success")) {
			Reporter.log("Email ID tick Mark is displayed",true);
		}else {
			ALib.AssertFailMethod("Email ID tick Mark is not displayed");
		}
	}

	public void navigateBacktoSureMDMLogin() throws InterruptedException{
		Initialization.driver.navigate().back();
		String SureMDMLoginTitle = Initialization.driver.getTitle();
		if(SureMDMLoginTitle.equalsIgnoreCase("SureMDM : Login")) {
			Reporter.log("Page is navigated back to SureMDM login page",true);
		}else {
			ALib.AssertFailMethod("Page is not navigated back to SureMDM login page");
		}
	}
	@FindBy(xpath="//div[@id='selectRegionConfirmation']//button[text()='Yes']")
    private WebElement SelectregionConfirmationYesButton;
	public void ClickOnSelectRegionConfirmationYesButton() throws InterruptedException
	{
		try
		{
		SelectregionConfirmationYesButton.click();
		sleep(3);
		}
		catch (Exception e) {
			Reporter.log("Confirmation Is Not Asked",true);
		}
	}
	public void enterDataInFewSignUPFields() throws InterruptedException{
		companyName.sendKeys(Config.companyName);
		Select region =new Select(DomainName);
		region.selectByVisibleText("APAC");
		sleep(2);
		ClickOnSelectRegionConfirmationYesButton();

	}


	public void verifyZipCodeFiledForUs() throws InterruptedException{
//		AccountRegion.click();
//		new Actions(Initialization.driver).moveToElement(SelectAccountRegionUS).click().perform();
		Select region =new Select(DomainName);
		region.selectByVisibleText("United States");
		sleep(2);
		ClickOnSelectRegionConfirmationYesButton();
		if  ( postalCode.isDisplayed()) {
			Reporter.log("Zip code is displayed",true);
		}
		else {
			ALib.AssertFailMethod("Zip code is not displayed");
		}
	}


	public void enableEULAcheckbox() throws InterruptedException
	{
		acceptCommunication.click();
		licenseAgreement.click();
	}

	public void EnterValuesinZipCode() throws InterruptedException{
		postalCode.clear();
		postalCode.sendKeys(Config.EnterZipcode);
	}


	public void EnterSpecialChaInZipCode() throws InterruptedException{
		postalCode.clear();
		postalCode.sendKeys(Config.EnterSpecialCharZipcode);
	}

	public void clickOnsignBtn() throws InterruptedException{
		SignupBtn.click();
		sleep(3);
	}

	public void enterDataInSignUPPage() throws InterruptedException
	{
		companyName.sendKeys(Config.companyName);
		DomainName.sendKeys(Config.DomainName);
		Select region =new Select(DomainName);
		region.selectByVisibleText("APAC");
		ClickOnSelectRegionConfirmationYesButton();
		Name.sendKeys(Config.EnterNamesignUP);
        Select country =new Select(SelectCountryClick);
		country.selectByValue("India");
		sleep(1);
         Select state =new Select(SelectStateClick);
		state.selectByVisibleText("Karnataka");
		sleep(1);
		phoneDropDown.click();
		new Actions(Initialization.driver).moveToElement(SelectIndia).click().perform();
		EnterPhoneNumber.click();
		EnterPhoneNumber.sendKeys(Config.EnterPhoneNumer);
		sleep(1);
		
	}

	public void verifyZipCodeFiledForIndia() throws InterruptedException{

		Select region =new Select(DomainName);
		region.selectByVisibleText("APAC");
		sleep(2);
		ClickOnSelectRegionConfirmationYesButton();
		Name.sendKeys(Config.EnterNamesignUP);
		
		Name.clear();
		Name.sendKeys(Config.EnterNamesignUP);

		Select country =new Select(SelectCountryClick);
		country.selectByValue("India");
		sleep(1);
		
		Name.sendKeys(Config.EnterNamesignUP);
		sleep(1);
		Select state =new Select(SelectStateClick);
		state.selectByVisibleText("Karnataka");
		sleep(1);

		if  ( postalCode.isDisplayed()) {
			Reporter.log("Zip code is displayed",true);
		}
		else {
			ALib.AssertFailMethod("Zip code is not displayed");
		}
	}

	public void verifyDataProcessorAddendum() throws InterruptedException
	{
		Initialization.driver.switchTo().window(childwinID);
		sleep(4);
		URL=Initialization.driver.getCurrentUrl();
		if(URL.contains("https://www.42gears.com/data-processing-addendum-agreement/")) {
			Reporter.log("Navigated to https://www.42gears.com/data-processing-addendum-agreement/ after clicking verify DataProcessor Addendum Link",true);
		}else {
			ALib.AssertFailMethod("Did not Navigate to https://www.42gears.com/data-processing-addendum-agreement/ after clicking verify DataProcessor Addendum Link");} 
	}

	public void verifyPrivacyPolicy() throws InterruptedException{
		Initialization.driver.switchTo().window(childwinID);
		URL=Initialization.driver.getCurrentUrl();
		if(URL.contains("https://www.42gears.com/privacy-policy/")) {
			Reporter.log("Navigated to https://www.42gears.com/privacy-policy/ after clicking privacy-policy Link",true);
		}else {
			ALib.AssertFailMethod("Did not Navigate to https://www.42gears.com/privacy-policy/ after clicking privacy-policy Link");} 
	}

	public void verifyTermsofUse() throws InterruptedException{
		sleep(2);
		Initialization.driver.switchTo().window(childwinID);
		URL=Initialization.driver.getCurrentUrl();
		if(URL.contains("SureMDM_SubscriptionLicenseAgreement_SaaS.pdf")) {
			Reporter.log("PDF has been Opened",true);
		}else {
			ALib.AssertFailMethod("PDF is not opened");} 
	}

	public void ClickOnTermUseLink() throws InterruptedException{
		termOfUseLink.click();
	}

	public void ClickOnDataProcessorAddendum() throws InterruptedException{
		DataProcessorAddendumLink.click();
	}

	public void ClickonPrivacyPolicyLink() throws InterruptedException{
		PrivacyPolicyLink.click();
	}

	public void windowhandlesTest()
	{
		Set<String> set=Initialization.driver.getWindowHandles();
		Iterator<String> id=set.iterator();
		parentwinID =id.next();
		childwinID =id.next();
	}	

	public void SwitchToMainWindow() throws InterruptedException{
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		sleep(5);
	}

	public void verifylicenseAgreementMsg() throws InterruptedException{
		if(acceptlicenseagreementErrMsg.getText().equalsIgnoreCase("Please accept the license agreement.")) {
			Reporter.log("Please accept license agreement Error Message is displayed",true);
		}else {
			ALib.AssertFailMethod("Please accept license agreement Error message is not displayed");}
	}



	public void verifyprovideAllfieldsErrorMsg() throws InterruptedException
	{	
		if(ProvideRequiredFieldsErrMSg.getText().equalsIgnoreCase("You still need to complete one or more mandatory fields.")) {
			Reporter.log("You still need to complete one or more mandatory fields Message Is Displayed.",true);
		}else
			ALib.AssertFailMethod("You still need to complete one or more mandatory fields Message Is Not Displayed.");
	}



	public void verifyInterComSection() throws InterruptedException{	

		try
		{
			if(intercomLauncherFrame.isDisplayed())
			{
				ALib.AssertFailMethod("Failed>> Intercom is Displayed");
			}
			ALib.AssertFailMethod("Failed>> Intercom is Displayed");
		}
		catch(Exception e)
		{
			Reporter.log("Pass>> Intercom is not Displayed",true);
		}


	}


//AccountRegistration Login methods
@FindBy(id="uName")
private WebElement enterUserName;

@FindBy(id="pass")
private WebElement enterPwd;

@FindBy(xpath="//div[text()='Your username and/or password do not match our records. ']")
private WebElement invalidCredentialsMsg;

@FindBy(id="loginBtn")
private WebElement loginButton;

@FindBy(id="homeSection")
private WebElement homeSectionIcon;

public void enterUsereName(String username) throws IOException{
	enterUserName.sendKeys(username);
}

public void enterPassword(String username) throws IOException{
	enterPwd.sendKeys(username);
}

public void pressEnterToLogin() {
	loginButton.sendKeys(Keys.ENTER);

}

public void verifyloginIsSuccessful() {
	//Verify logged in successfully
	waitForidPresent("homeSection");
	if((homeSectionIcon.isDisplayed())) {
		Reporter.log("Logged in successfully",true);
	}else {
		ALib.AssertFailMethod("Did not login successfully");
	}
}

public void verifyInvalidCredentialsMsg() throws InterruptedException {
	sleep(1);
	if((invalidCredentialsMsg.isDisplayed())) {
		Reporter.log("Invalid credential Error message is displayed",true);
	}else {
		ALib.AssertFailMethod("Invalid credential Error message is not displayed");
	}
}

//AccountRegistration Forgot Password methods
@FindBy(id="forgot_password")
private WebElement forgotPassword;

@FindBy(xpath="//div[contains(text(),'Already have an account?')]/a[text()='Log in']")
private WebElement alreadyUser;


public void clickOnAlreadyUserLink() throws InterruptedException 
{
	alreadyUser.click();
}

public void clickOnForgotPwd() throws InterruptedException 
{
	forgotPassword.click();
	waitForXpathPresent("//div[contains(text(),'Already have an account?')]");
	sleep(2);
}


public void verifynavigatedToSureMDM() {
	if (getTittle().equals("SureMDM : Login")) {
		Reporter.log("Pass>> Page is navigated to SureMDM : Login",true);
	}else{
		ALib.AssertFailMethod("Page is not navigated to SureMDM : Login");
	}

}

}
