package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;

import javax.security.auth.x500.X500Principal;
import javax.xml.xpath.XPath;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class Groups_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();

	@FindBy(xpath = "//div[@title='New']")
	private WebElement NewGroup;

	@FindBy(xpath = "//span[text()='New Group Name']/following-sibling::input")
	private WebElement NewGroupTextBox;

	@FindBy(id = "groupokbtn")
	private WebElement NewGroupOkBtn;

	@FindBy(xpath = "//h4[text()='Add New Group']/preceding-sibling::button")
	private WebElement NewGroupCloseBtn;

	@FindBy(xpath = "//h4[text()='Add New Group']")
	private WebElement Newgroupheading;

	@FindBy(xpath = "//div[@title='Rename']")
	private WebElement RenameGroup;

	@FindBy(xpath = "//h4[text()='Rename existing group']")
	private WebElement Renamegroupheading;

	@FindBy(xpath = "//h4[text()='Rename existing group']/preceding-sibling::button")
	private WebElement RenameGroupCloseBtn;

	@FindBy(xpath = "//div[@title='Delete']")
	private WebElement DeleteGroup;

	@FindBy(id = "deletegroupbtn")
	private WebElement DeleteGroupyesBtn;

	@FindBy(xpath = "//span[text()='Deleted group successfully.']")
	private WebElement Deletedgrpsuccessmsg;

	@FindBy(xpath = "//div[@id='deleteGroupConf']/div/div/div[2]/button[1]")
	private WebElement DeleteGroupNoBtn;

	@FindBy(xpath = "//div[@title='Properties']")
	private WebElement PropertiesGroup;

	@FindBy(xpath = "//h4[text()='Group Properties - \"']")
	private WebElement Propertiesheading;

	@FindBy(xpath = "//div[contains(text(),'Click add(+) button to add files.')]")
	private WebElement textingroupProperties;

	@FindBy(xpath = "//table[@id='jobGroupGrid']/tbody/tr[1]")
	private WebElement Defaultjobingrpppts;

	@FindBy(id = "properties_addBtn")
	private WebElement PropertiesAdd;

	@FindBy(id = "properties_dltbtn")
	private WebElement PropertiesDelete;

	@FindBy(xpath = "//table[@id='jobGroupGrid']/tbody/tr[1]")
	private WebElement GroupPropertiesFirstRow;

	@FindBy(id = "grouppropertiesokbtn")
	private WebElement PropertiesOkBtn;

	@FindBy(xpath = "//div [@id='groupstree']/ul/li[text()='Home']")
	private WebElement HomeGroup;

	@FindBy(xpath = "//table[@id='dataGrid']/tbody/tr[1]")
	private WebElement HomeGroupFirstRow;

	@FindBy(id = "selJob_Modal_close")
	private WebElement PropertiesSelectJobsClose;

	@FindBy(xpath = "//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement PropertiesDeleteNoBtn;

	@FindBy(xpath = "//li[text()='This is a mandatory field.']")
	private WebElement GroupErrorMessage;

	@FindBy(xpath = "//h4[text()='Select Jobs To Add']")
	private WebElement Selectjobstoaddtitle;

	@FindBy(id = "okbtn")
	private WebElement Selectjobstoaddokbtn;

	@FindBy(xpath = "//span[text()='Please select a job.']")
	private WebElement Selectjobstoadderrormsg;

	@FindBy(xpath = "//div[@id='compjob_JobGridContainer']/div[2]/div[1]/div[1]/div/input")
	private WebElement Selectjobstoaddsearchbox;

	@FindBy(xpath = "//table[@id='applyJobDataGrid']/tbody/tr/td/p[text()='Automationtestjob']")
	private WebElement Jobadded;

	@FindBy(xpath = "//p[text()='Automationtestjob']")
	private WebElement Searchdefaultjob;

	@FindBy(xpath = "//table[@id='jobGroupGrid']/tbody/tr/td/p[text()='Automationtestjob']")
	private WebElement defaultjob;

	@FindBy(xpath = "//span[text()='Job added successfully.']")
	private WebElement Jobaddedsuccessfullymsg;

	@FindBy(xpath = "//div[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement PropertiesDeleteyesBtn;

	@FindBy(xpath = "//span[text()='Job deleted successfully.']")
	private WebElement JobDeletedsuccessmessage;

	@FindBy(xpath = "//span[contains(@class,'applyJob_chkbox')]/input")
	private WebElement Forceapplyjobcheckbox;

	@FindBy(id = "pingallgcmdevicesbtn")
	private WebElement pingalldevicesbutton;

	@FindBy(xpath = "//span[text()='Total Devices']")
	private WebElement TotalDevicesgrppts;

	@FindBy(xpath = "//span[text()='Total Subgroups']")
	private WebElement Totalsubgrps;

	@FindBy(xpath = "//span[text()='Total Devices In Current Group']")
	private WebElement TotalDevicesincurrentgrp;

	@FindBy(xpath = "//span[text()='Total Online']")
	private WebElement Totalonline;

	@FindBy(xpath = "//span[text()='Total Online In Current Group']")
	private WebElement Totalonlineincurrentgrp;

	@FindBy(xpath = "//div[@id='ApplyJobtoGroup']")
	private WebElement Applyjobtogroup;

	@FindBy(xpath = "//div[@id='groupjobqueue']")
	private WebElement Jobqueue;

	@FindBy(xpath = ".//*[@id='grpOperationMenu']/li[1]")
	private WebElement RightclickHomeadd;

	@FindBy(xpath = "//li[@class='propOpt']")
	private WebElement RightclickHomeproperties;

	@FindBy(xpath = "//li[@class='aplyOpt']")
	private WebElement Rightclickapply;

	@FindBy(xpath = "//*[@id='JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement Applyjobtogrpsearchbox;

	@FindBy(xpath = "//button[@id='applyJob_okbtn']")
	private WebElement Applybutton;

	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[1]/p")
	private WebElement Applyjobtogrpmsg;

	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement Applyjobyesbutton;

	@FindBy(xpath = "//div [@id='groupstree']/ul/li[text()='Right click add group']")
	private WebElement Rightclicksubgrp;

	@FindBy(xpath = "//li[@class='renameOpt']")
	private WebElement Rightclickrename;

	@FindBy(xpath = "//div[@id='groupstree']/ul/li[text()='Right click add group']")
	private WebElement rightclicksubgrp;

	@FindBy(xpath = "//li[@class='queueOpt']")
	private WebElement rightclickjobqueue;

	@FindBy(xpath = "//li[@id='succes_job']")
	private WebElement Successtab;

	@FindBy(xpath = "//li[@id='failed_job']")
	private WebElement Failedtab;

	@FindBy(xpath = "//div[@id='groupJobQ_modal']/div[1]/button")
	private WebElement jobqueueclose;

	public void ClickOnNewGroup() throws InterruptedException {
		NewGroup.click();
		waitForidPresent("groupokbtn");
		sleep(3);
	}

	public void ClickOnRenameGroup() throws InterruptedException {
		RenameGroup.click();
		waitForidPresent("groupokbtn");
		sleep(2);
	}

	public void ClickOnDeleteGroup() throws InterruptedException {
		DeleteGroup.click();
		waitForidPresent("deletegroupbtn");
		sleep(2);
	}

	@FindBy(id = "deletegroupbtn")
	private WebElement DeleteAlongWithDevicesButton;

	public void ClickonYesDeleteAlongWithDevices() throws InterruptedException {
		DeleteAlongWithDevicesButton.click();
		sleep(3);
	}

	public void Verifydeletegroup() throws InterruptedException {
		DeleteGroupyesBtn.click();
		boolean b = true;
		try {
			Deletedgrpsuccessmsg.isDisplayed();

		} catch (Exception e) {
			b = false;

		}
		Assert.assertFalse(b, "Fail: Receiving of notification failed");
		System.out.println("Confirmation Message : 'Deleted group successfully.' is received");
		waitForidClickable("deleteDeviceBtn");
		sleep(10);

	}

	public void ClickOnProperties() throws InterruptedException {
		PropertiesGroup.click();
		waitForidPresent("properties_addBtn");
		sleep(10);
	}

	public void ClickOnProperties_Sub() throws InterruptedException {
		PropertiesGroup.click();
		waitForidPresent("properties_addBtn");
		sleep(3);
	}

	public void ClickOnPropertiesAddBtn() throws InterruptedException {
		PropertiesAdd.click();
		sleep(2);
	}

	public void ClickOnPropertiesOkBtn() throws InterruptedException {
		PropertiesOkBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(9);
	}

	public void ClickOnPropertiesDeleteBtn() {
		PropertiesDelete.click();
		waitForidPresent("properties_dltbtn");
	}

	public void ClickGroupPropertiesFirstRow() throws InterruptedException {
		GroupPropertiesFirstRow.click();
		sleep(2);
	}

	public void verifygrppropertiestitle() throws InterruptedException {
		String s1 = Propertiesheading.getText();
		Assert.assertEquals(s1, "Group Properties", "Fail>> Title of page is wrong");
		Reporter.log("Title of page : " + s1, true);
	}

	public void Verifytextinproperties() throws InterruptedException {
		boolean b = true;
		try {
			b = textingroupProperties.isDisplayed();
		} catch (Exception e) {
			String s = Defaultjobingrpppts.getAttribute("class");
			boolean b1 = s.contains("no-records-found");
			Assert.assertTrue(b1, "Jobs are not Present");
			Reporter.log("Jobs are Present", true);
			sleep(4);
		}
		sleep(5);
		Assert.assertTrue(b, "Grid View is not displayed ");
		Reporter.log("Grid View is displayed", true);
	}

	public void Verifydeletebutton() throws InterruptedException {

		String s = PropertiesDelete.getAttribute("class");
		boolean b2 = s.contains("disabled");
		Assert.assertTrue(b2, "Delete button is enabled");
		Reporter.log("Delete button is disabled", true);
		PropertiesOkBtn.click();
		sleep(10);

	}

	public void verifytitleselectjobstoadd() throws InterruptedException {
		sleep(10);
		String S = Selectjobstoaddtitle.getText();
		Assert.assertEquals(S, "Select Jobs To Add", "Fail>> Title of page is wrong");
		Reporter.log("Title of page : " + S, true);
	}

	public void Verifyerrorwhennojobselected() throws InterruptedException {
		Selectjobstoaddokbtn.click();
		boolean b = true;

		try {
			Selectjobstoadderrormsg.isDisplayed();

		} catch (Exception e) {
			b = false;

		}

		Assert.assertTrue(b, "Fail: Receiving of notification failed");
		Reporter.log("Alert Message : 'Please select a job.' is received", true);

		PropertiesSelectJobsClose.click();
		sleep(10);

	}

	public void Verifysearchandaddjobdefaultjobforhomegrp(String s) throws InterruptedException {
		sleep(15);
		Selectjobstoaddsearchbox.sendKeys(s);
		sleep(15);
		boolean b = true;

		try {
			Searchdefaultjob.click();

		} catch (Exception e) {
			b = false;

		}

		Assert.assertTrue(b, "Fail:job is not searched");
		Reporter.log("job is searched successfully ", true);
		Selectjobstoaddokbtn.click();
		sleep(15);
		boolean b1 = true;
		{

			try {
				Jobaddedsuccessfullymsg.isDisplayed();

			} catch (Exception e) {
				b1 = false;

			}
		}

		Assert.assertTrue(b1, "Fail: Receiving of notification failed");
		Reporter.log("Confirmation Message : 'Job added successfully.' is received", true);
		sleep(5);
		PropertiesOkBtn.click();
		sleep(5);

	}

	public void selectdefaultjob() throws InterruptedException {
		Reporter.log("Click on properties and select the job added", true);
		defaultjob.click();
		sleep(5);

	}

	public void Verifydefaultjobforsubgrp() throws InterruptedException {
		Reporter.log("Verify whether job added for home group is reflected for sub group", true);
		defaultjob.isDisplayed();
		PropertiesOkBtn.click();
		sleep(5);
	}

	public void Verifydeletingdefaultjob() throws InterruptedException {
		sleep(5);
		PropertiesDeleteyesBtn.click();
		String PassStatement = "PASS >> Confirmation Message : 'Job deleted successfully.' is not displayed successfully when clicked on Cancel button of Advanced Settings of Dynamic Apps";
		String FailStatement = "Fail >> Confirmation Message : 'Job deleted successfully.' is displayed even when clicked on Cancel button of Advanced Settings of Dynamic Apps";
		Initialization.commonmethdpage.ConfirmationMessageVerify(JobDeletedsuccessmessage, true, PassStatement,
				FailStatement, 2);

		PropertiesOkBtn.click();
		sleep(5);
	}

	public void ClickOnHomeGrup() throws InterruptedException {
		if (!(HomeGroup.getAttribute("class").contains("selected"))) {
			HomeGroup.click();
			Reporter.log("Clicked on Home Group", true);
			waitForidPresent("deleteDeviceBtn");
			sleep(8);
		}

	}

	public void ClickHomeGroupFirstRow() throws InterruptedException {
		HomeGroupFirstRow.click();
		sleep(2);
	}

	public void ClickOnGroup(int row, int col)
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Initialization.driver
				.findElement(By.xpath(
						"//div [@id='groupstree']/ul/li[text()='" + ELib.getDatafromExcel("Sheet9", row, col) + "']"))
				.click();
		sleep(10);
	}

	public void ClickOnGroup(String GroupName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div [@id='groupstree']/ul/li[text()='" + GroupName + "']"))
				.click();
		sleep(4);
	}

	public void EnterGroupName(String Name) {
		NewGroupTextBox.clear();
		NewGroupTextBox.sendKeys(Name);
	}

	public void ClickOnNewGroupOkBtn() throws InterruptedException {
		NewGroupOkBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}

	public void ClickOnNewGroupCloseBtn() throws InterruptedException {
		NewGroupCloseBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}

	public void ClickOnNewGroup_UM() throws InterruptedException {
		NewGroup.click();

	}

	public void ClickOnRenameGroup_UM() throws InterruptedException {
		RenameGroup.click();
	}

	public void ClickOnDeleteGroup_UM() throws InterruptedException {
		DeleteGroup.click();
	}

	public void ClickOnRenameGroupCloseBtn() throws InterruptedException {
		RenameGroupCloseBtn.click();
		sleep(2);
	}

	public void ClickOnDeleteGroupNoBtn() throws InterruptedException {
		DeleteGroupNoBtn.click();
		sleep(2);
	}

	public void ClickOnPropertiesSelectJobsClose() throws InterruptedException {
		PropertiesSelectJobsClose.click();
		sleep(2);
	}

	public void ClickOnPropertiesDeleteNoBtn() throws InterruptedException {
		PropertiesDeleteNoBtn.click();
		sleep(2);
	}

	public void verifyGroupErrorMessage() throws InterruptedException {
		sleep(5);
		boolean value = GroupErrorMessage.isDisplayed();
		String ActualValue = GroupErrorMessage.getText();
		String PassStatement = "PASS >> Error Message is displayed when group nameis not entered as " + ActualValue;
		String FailStatement = "FAIL >> Error Message is not displayed when group name  is not entered";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void Verifynewgroupheading() {
		boolean value = Newgroupheading.isDisplayed();
		String ActualValue = Newgroupheading.getText();
		String PassStatement = "PASS >> Group heading is displayed as " + ActualValue;
		String FailStatement = "FAIL >> Groupname heading is not displayed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyRenamegroupheading() {
		boolean value = Renamegroupheading.isDisplayed();
		String ActualValue = Renamegroupheading.getText();
		String PassStatement = "PASS >>Rename Group heading is displayed as " + ActualValue;
		String FailStatement = "FAIL >>Rename Group heading is not displayed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void Verifydeletisdisabledforsubgrp() throws InterruptedException {

		String s = PropertiesDelete.getAttribute("class");
		boolean b2 = s.contains("disabled");
		Assert.assertTrue(b2, "Delete button is enabled");
		Reporter.log("Delete button is disabled", true);
		PropertiesOkBtn.click();
		sleep(10);
		Reporter.log("Delete option is disabled", true);

	}

	public void VerifyForceapplyisdisabledforsubgrp() throws InterruptedException {
		String s = Forceapplyjobcheckbox.getAttribute("class");
		boolean b2 = s.contains("disabled");
		Assert.assertTrue(b2, "Force apply on re-registration is enabled");
		Reporter.log("Force apply on re-registration is disabled", true);
		PropertiesOkBtn.click();
		sleep(10);
		Reporter.log("Force apply on re-registration is disabled", true);
	}

	public void Verifypingalldevices() throws InterruptedException {
		pingalldevicesbutton.click();
		sleep(5);
		String s = pingalldevicesbutton.getAttribute("value");
		System.out.println(s);
		boolean b1 = s.contains("Ping in progress");
		System.out.println(b1);
		Assert.assertTrue(b1, "Ping in progress is not displayed");
		Reporter.log("Ping in progress is displayed", true);
	}

	public void Verifypingalldevicessubgrp() throws InterruptedException {
		// pingalldevicesbutton.click();
		sleep(5);
		String s = pingalldevicesbutton.getAttribute("value");
		System.out.println(s);
		boolean b1 = s.contains("Ping in progress");
		Assert.assertTrue(b1, "Ping in progress is not displayed");
		Reporter.log("Ping in progress is displayed", true);
	}

	public void Verifytotaldevicesandgrps() throws InterruptedException {
		boolean b2 = TotalDevicesgrppts.isDisplayed();
		Assert.assertTrue(b2, "Total devices are not displayed");
		Reporter.log("Total devices are displayed", true);
		boolean b3 = Totalsubgrps.isDisplayed();
		Assert.assertTrue(b3, "Total subgroups are not displayed");
		Reporter.log("Total subgroups are displayed", true);
		boolean b4 = TotalDevicesincurrentgrp.isDisplayed();
		Assert.assertTrue(b4, "Total devices in current group are not displayed");
		Reporter.log("Total devices in current group are displayed", true);
		boolean b5 = Totalonline.isDisplayed();
		Assert.assertTrue(b5, "Total number of online devices are not displayed");
		Reporter.log("Total number of online devices are displayed", true);
		boolean b6 = Totalonlineincurrentgrp.isDisplayed();
		Assert.assertTrue(b6, "Total number of online devices in current group are not displayed");
		Reporter.log("Total number of online devices in current group are displayed", true);
		PropertiesOkBtn.click();
	}

	public void Scripttoscrollup() throws InterruptedException {
		JavascriptExecutor jse = (JavascriptExecutor) Initialization.driver;
		jse.executeScript("arguments[0].scrollIntoView();", HomeGroup);
		sleep(2);

	}

	public void Verifytooltipofnewgroup() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(NewGroup).perform();
		Thread.sleep(2000);
		String S = NewGroup.getAttribute("title");
		Assert.assertEquals(S, "New", "New tooltip is not displayed");
		System.out.println("New Tooltip is displayed");
	}

	public void Verifytooltipofrenamegroup() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(RenameGroup).perform();
		Thread.sleep(2000);
		String S = RenameGroup.getAttribute("title");
		Assert.assertEquals(S, "Rename", "Rename tooltip is not displayed");
		System.out.println("Rename Tooltip is displayed");

	}

	public void Verifytooltipofdeletegroup() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(DeleteGroup).perform();
		Thread.sleep(2000);
		String S = DeleteGroup.getAttribute("title");
		Assert.assertEquals(S, "Delete", "Delete tooltip is not displayed");
		System.out.println("Delete Tooltip is displayed");

	}

	public void VerifytooltipofPropertiesGroup() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(PropertiesGroup).perform();
		Thread.sleep(2000);
		String S = PropertiesGroup.getAttribute("title");
		Assert.assertEquals(S, "Properties", "Properties tooltip is not displayed");
		System.out.println("Properties Tooltip is displayed");

	}

	public void VerifytooltipofApplyjobtogrp() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Applyjobtogroup).perform();
		Thread.sleep(2000);
		String S = Applyjobtogroup.getAttribute("title");
		Assert.assertEquals(S, "Apply job to group", "Apply job to group tooltip is not displayed");
		System.out.println("Apply job to group Tooltip is displayed");

	}

	public void Verifytooltipofjobqueue() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Jobqueue).perform();
		Thread.sleep(2000);
		String S = Jobqueue.getAttribute("title");
		Assert.assertEquals(S, "Job queue", "Job queue tooltip is not displayed");
		System.out.println("Job queue Tooltip is displayed");
	}

	public void Verifyrightclickaddforhomegroup() throws InterruptedException {
		sleep(10);
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(HomeGroup).contextClick(HomeGroup).perform();
		sleep(10);
		RightclickHomeadd.click();
		sleep(5);
	}

	public void Verifyrightclickpropertiesforhomegrp() throws InterruptedException {
		sleep(10);
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(HomeGroup).contextClick(HomeGroup).perform();
		sleep(10);
		RightclickHomeproperties.click();
		sleep(5);
	}

	public void Verifyrightclickapplyjobforhomegrp() throws InterruptedException {
		sleep(5);
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(HomeGroup).contextClick(HomeGroup).perform();
		sleep(10);
		Rightclickapply.click();
		sleep(15);

	}

	public void Verifyapplygroupjob() throws InterruptedException {

		Applyjobtogrpsearchbox.click();
		Applyjobtogrpsearchbox.sendKeys("Automationtestjob");
		sleep(10);
		System.out.println("job is searched");
		boolean b = true;

		try {
			Jobadded.click();

		} catch (Exception e) {
			b = false;

		}

		Assert.assertTrue(b, "Fail:job is not searched");
		Reporter.log("job is searched successfully ", true);
		Applybutton.click();
		sleep(5);
		boolean b1 = true;
		{

			try {
				Applyjobtogrpmsg.isDisplayed();

			} catch (Exception e) {
				b1 = false;

			}
		}

		Assert.assertTrue(b1, "Fail: Receiving of notification failed");
		Reporter.log(
				"Confirmation Message : 'Selected job(s) shall be applied to all devices in selected group(s). Do you wish to continue?' is received",
				true);
		Applyjobyesbutton.click();
		sleep(5);

	}

	public void Verifyrightclickrename() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(rightclicksubgrp).contextClick(rightclicksubgrp).perform();
		sleep(10);
		Rightclickrename.click();
		sleep(15);

	}

	public void Verifyjobqueue() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(HomeGroup).contextClick(HomeGroup).perform();
		rightclickjobqueue.click();
		sleep(10);

	}

	public void verifyclickingonjobqueuesuccess() throws InterruptedException {
		Successtab.click();
	}

	public void verifyclickingonjobqueuefailed() throws InterruptedException {
		sleep(3);
		Failedtab.click();
		sleep(4);
	}

	public void verifyclickingonjobqueueclose() throws InterruptedException {
		jobqueueclose.click();
	}

	@FindBy(xpath = "//*[@id='groupJobQ_modal']/div[1]/button")
	private WebElement CloseBtnCompletedJobSectionRtClick;

	public void RightClickOndeviceInsideGroups1(String name) throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		JavascriptExecutor js1 = (JavascriptExecutor) Initialization.driver;
		action.contextClick(Initialization.driver.findElement(
				By.xpath("//*[@id='groupstree']/ul/li[contains(text(),'" + Config.GroupNameRightClick + "')]")))
				.perform();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='" + Config.FileTransferJobTAGS + "']")
	private WebElement DefaultJobGroups;

	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement OkBtnDefaultJob;

	public void DefaultJobSelect() throws InterruptedException {
		DefaultJobGroups.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupAddJob_modal']/div/button[contains(text(),'OK')]")
	private WebElement OkBtnGroups;

	public void OkBtnDefaultJob_Click() throws InterruptedException {
		OkBtnGroups.click();
		waitForXpathPresent("//span[contains(text(),'The job called [" + Config.FileTransferJobTAGS
				+ "] has been added to [Home] and will automatically deploy on each device in [Home].')]");
		Reporter.log("//span[text()='Job FileJob added successfully to Home']", true);
		sleep(2);
	}

	@FindBy(xpath = "//input[@class='dummy ct-selbox-in']")
	private WebElement CheckBoxForceApplyOnRe_Registration;

	@FindBy(xpath = "//*[@id='grouppropertiesokbtn']")
	private WebElement grouppropertiesokbtn;

	public void ForceApplyOnRe_Registration() throws InterruptedException {

		CheckBoxForceApplyOnRe_Registration.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='groupProperties']")
	private WebElement groupProperties;

	@FindBy(xpath = "//*[@id='properties_addBtn']")
	private WebElement properties_addBtn;

	public void ClickOnGrpProperties() throws InterruptedException {

		groupProperties.click();
		waitForXpathPresent("//*[@id='groupPropertiesDialog']/div/div/div[1]/h4[contains(text(),'Group Properties ')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupjobQueueDataGrid']/tbody/tr/td[2][contains(text(),'AllJobStatusJobs')]")
	private WebElement AllJobStatusJobs_Name;

	public void SelectAllJobStatusJobs() throws InterruptedException {

		AllJobStatusJobs_Name.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupJobQ_modal']/div[2]/ul/li/a[contains(text(),'Pending')]")
	private WebElement PendingSection;

	public void VerifyJobPendingJobSectionGroupRtClick() throws InterruptedException {
		sleep(5);
		PendingSection.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + Config.FailJob + "']")
	private WebElement FileTransfer_FileNameFail;

	public void SearchTextFieldFailJob(String JobNameToApplyOnGroups) throws InterruptedException {

		applyJob_table_coverSearchTextField.sendKeys(JobNameToApplyOnGroups);
		sleep(2);
		FileTransfer_FileNameFail.click();
		sleep(2);
		applyJob_okbtnGroups.click();
		// waitForXpathPresent("//*[@id='deviceConfirmationDialog']/div/div/div[1]/p[contains(text(),'
		// Selected FailJob job shall be applied to all devices in group
		// @SourceDonotTouch. Do you wish to continue?')]");
		sleep(2);
		YesBtnGroupsApplyJob.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='compjob_JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchInputTextField;

	public void SearchJob(String JobNameDefaultForGroups) throws InterruptedException {

		SearchInputTextField.sendKeys(JobNameDefaultForGroups);
		sleep(4);
	}

	public void RightClickOndeviceInsideGroups2(String name) throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		JavascriptExecutor js1 = (JavascriptExecutor) Initialization.driver;
		action.contextClick(Initialization.driver
				.findElement(By.xpath("//*[@id='groupstree']/ul/li[contains(text(),'" + Config.SubGroupDevice + "')]")))
				.perform();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupjobqueueremovebtn']")
	private WebElement groupjobqueueremovebtn;

	public void ClickOnRemoveBtnJob() throws InterruptedException {
		groupjobqueueremovebtn.click();
		waitForXpathPresent("//span[text()='Job deleted successfully.']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupJobQ_modal']/div[2]/ul/li/a[contains(text(),'In Progress')]")
	private WebElement InProgressSection;

	public void VerifyJobInProgressJobSectionGroupRtClick() throws InterruptedException {
		sleep(5);
		InProgressSection.click();
		sleep(2);
	}

	public void clickOnGrpJobQeueSubGroup() throws InterruptedException {
		groupjobqueue.click();
		waitForXpathPresent("//h4[contains(text(),'Job History - @SubGroupDonotTouch')]");
		sleep(2);
	}

	public void ClickOnAddPropertiesBtn() throws InterruptedException {
		properties_addBtn.click();
		waitForXpathPresent("//h4[text()='Select Jobs To Add']");
		sleep(4);

	}

	public void OkBtnDefaultJob_Click2() throws InterruptedException {
		OkBtnGroups.click();
		waitForXpathPresent("//span[contains(text(),'The job called [" + Config.JobNew + "] has been added to ["
				+ Config.SubGroupDevice + "] and will automatically deploy on each device in [" + Config.SubGroupDevice
				+ "].')]");
		sleep(4);
		sleep(4);
	}

	public void OkBtnDefaultJob_Click3() throws InterruptedException {
		OkBtnGroups.click();
		waitForXpathPresent("//span[contains(text(),'The job called [" + Config.JobNew + "] has been added to ["
				+ Config.AnyOsJobGrpName + "] and will automatically deploy on each device in ["
				+ Config.AnyOsJobGrpName + "].')]");
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='groupstree']/ul/li[contains(text(),'" + Config.SourceGroupName + "')]/span[2]")
	private WebElement DropDown;

	public void ClickOnDropDownGroups(String GroupName) throws InterruptedException {
		DropDown.click();
		waitForXpathPresent("//div [@id='groupstree']/ul/li[text()='" + GroupName + "']");
		sleep(2);
	}

	public void ClickOnResetBtn1() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Set to defaults']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Reset']").click();
		sleep(4);
	}

	public void ClickOnGetStartedBtnDeviceSide(String AccountId, String EnterServerPath) throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Get Started']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Continue']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='1']").sendKeys(AccountId);
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Register']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='Enter Server Path']")
				.sendKeys(EnterServerPath);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Server Path']").click();
		sleep(6);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Continue...']").click();
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.RadioButton[@text='Use System Generated Name']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Device Name']").click();
		sleep(3);
	}

	public void ClickOnImport_ExportSettingsOnDevice(String Text) throws InterruptedException { // Import/Export
																								// Settings
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + Text + "']").click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='" + Config.JobNew + "']")
	private WebElement DefaultJobGroups2;

	public void DefaultJobSelect2() throws InterruptedException {
		DefaultJobGroups2.click();
		sleep(2);
	}

	public void grouppropertiesokbtnClick() throws InterruptedException {

		grouppropertiesokbtn.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='grpOperationMenu']/li[6]/span[contains(text(),'Job History')]")
	private WebElement JobHistoryGroup;

	public void ClickOnJobQueueGroup() throws InterruptedException {
		JobHistoryGroup.click();
		waitForXpathPresent("//h4[contains(text(),'Job History - @SourceDonotTouch')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupJobQ_modal']/div[2]/ul/li/a[contains(text(),'Failed')]")
	private WebElement FailedSection;

	public void VerifyJobFailedJobSectionGroupRtClick() throws InterruptedException {
		sleep(5);
		FailedSection.click();
		sleep(2);
	}

	public void CloseBtnRtClickJobHistory() throws InterruptedException {
		CloseBtnCompletedJobSectionRtClick.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='" + Config.JobNew + "']")
	private WebElement ImageFileJobInCompletedSection;

	public void VerifyJobCompletedJobSectionGroupRtClick2() throws InterruptedException {
		CompletedSection.click();
		sleep(4);
		boolean JobPresent = ImageFileJobInCompletedSection.isDisplayed();
		String pass = "PASS >> ImageFile Job is displayed ";
		String fail = "FAIL >> ImageFile Job is NOT displayed";
		ALib.AssertTrueMethod(JobPresent, pass, fail);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='groupjobQueueDataGrid']/tbody/tr/td[2][contains(text(),'" + Config.EnterJobWNameFileApk
			+ "')]")
	private WebElement PendingJobStatusJobs_Name;

	public void SelectPendingJobStatusJobs() throws InterruptedException {
		PendingJobStatusJobs_Name.click();
		sleep(2);
	}

	@FindBy(xpath = "//li[@class='queueOpt']")
	private WebElement queueOpt;

	public void ClickOnJobHistoryOptnRtClickOnGroup() throws InterruptedException {
		queueOpt.click();
		waitForXpathPresent("//h4[contains(text(),'Job History - @SourceDonotTouch')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id=\"grpOperationMenu\"]/li[3]/span[text()='Delete']")
	private WebElement DeleteGroup1;

	public void VerifySelectingHomeGroupAndDisabledOptionsForDeleteOption() throws InterruptedException

	{
		try {
			if (DeleteGroup1.isDisplayed()) {

				Reporter.log("Delete Option is displayed", false);
				ALib.AssertFailMethod("Delete Option is displayed");
			}
		} catch (Exception e) {
			Reporter.log("Delete Option is not displayed", true);
		}
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupjobQueueDataGrid']/tbody/tr/td[2][contains(text(),'" + Config.FailJob + "')]")
	private WebElement FileTransferFailJob;

	public void VerifyInsideJobQeueFailSection() throws InterruptedException {

		String JobFailed = FileTransferFailJob.getText();
		String Expected = Config.FailJob;
		String pass = "PASS >> FileTransfer Job is displayed in Failed section ";
		String fail = "FAIL >> FileTransfer Job is NOT displayed in Failed section";
		ALib.AssertEqualsMethod(Expected, JobFailed, pass, fail);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='ApplyJobtoGroup']")
	private WebElement ApplyJobtoGroup;

	public void ClickOnApplyGroups() throws InterruptedException {

		ApplyJobtoGroup.click();
		waitForXpathPresent("//h4[contains(text(),'Apply Job/Profile To Group')]");
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
	private WebElement applyJob_table_coverSearchTextField;
	@FindBy(xpath = "//*[@id='applyJob_okbtn']")
	private WebElement applyJob_okbtnGroups;

	@FindBy(xpath = "//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement YesBtnGroupsApplyJob;

	@FindBy(xpath = "//*[@id='applyJobDataGrid']/tbody/tr/td[2]/p[text()='" + Config.FileTransferJobName + "']")
	private WebElement FileTransfer_FileName;

	public void SearchTextField(String JobNameToApplyOnGroups) throws InterruptedException {

		applyJob_table_coverSearchTextField.sendKeys(JobNameToApplyOnGroups);
		sleep(2);
		FileTransfer_FileName.click();
		sleep(2);
		applyJob_okbtnGroups.click();
		// waitForXpathPresent("//p[contains(text(),'The job called
		// ["+JobNameToApplyOnGroups+"] will be applied to all devices in
		// "+Config.SourceGroupName+". Do you wish to continue?')]");
		sleep(2);
		YesBtnGroupsApplyJob.click();
		sleep(2);

	}

	public void VerifyRenameByrightclickforhomegroup() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.contextClick(HomeGroup).perform();
		sleep(10);
	}

	@FindBy(xpath = "//*[@id='grpOperationMenu']/li[2]/span[text()='Rename']")
	private WebElement RenameGroup1;

	public void VerifySelectingHomeGroupAndDisabledOptionsForRenameOption() throws InterruptedException {
		try {
			if (RenameGroup1.isDisplayed()) {
				Reporter.log("Rename Option is displayed", false);
				ALib.AssertFailMethod("Rename Option is displayed");
			}
		} catch (Exception e) {
			Reporter.log("Rename Option is not displayed", true);
		}
		sleep(2);
	}

	public void ClikOnNixSettings() throws IOException, InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").click();
		sleep(2);
	}

	public void RightClickOndeviceInsideGroups(String name) throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		JavascriptExecutor js1 = (JavascriptExecutor) Initialization.driver;
		action.contextClick(
				Initialization.driver.findElement(By.xpath("//p[text()='" + Config.DeviceInsideGroups + "']")))
				.perform();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='applyJobDataGrid']/tbody/tr/td[2]/p[text()='" + Config.EnterJobWNameFileApk + "']")
	private WebElement AllJobStatus_Jobs;

	public void SearchTextFieldAllJobStatus(String AllJobStatus,String grp) throws InterruptedException {

		applyJob_table_coverSearchTextField.sendKeys(AllJobStatus);
		sleep(2);
		AllJobStatus_Jobs.click();
		sleep(2);
		applyJob_okbtnGroups.click();
		waitForXpathPresent("//p[contains(text(),'The job called [" + AllJobStatus+ "] will be applied to all devices in [" + grp + "]. Do you wish to continue?')]");
		sleep(2);
		YesBtnGroupsApplyJob.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='name_prefix']")
	private WebElement name_prefix;

	@FindBy(xpath = "//*[@id='name_suffix']")
	private WebElement name_suffix;

	@FindBy(xpath = "//*[@id='nameCount_length']")
	private WebElement nameCount_length;

	public void EnterPrefix(String InputPrefix) throws InterruptedException {

		name_prefix.clear();
		sleep(2);
		name_prefix.sendKeys(InputPrefix);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr/td[2]/p[text()='" + Config.DeviceName + "']")
	private WebElement DragDevice;

	@FindBy(xpath = "//div[@id='groupstree']/ul/li[text()='" + Config.SourceGroupName + "']")
	private WebElement DropDeviceToGroup;

	@FindBy(xpath = "//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesBtnDragDrop;

	public void DragAndDropDevice() throws InterruptedException {

		Actions builder = new Actions(Initialization.driver);
		Actions dragAndDrop = builder.clickAndHold(DragDevice);
		dragAndDrop.moveToElement(DropDeviceToGroup).release(DropDeviceToGroup).build().perform();
		sleep(2);
		YesBtnDragDrop.click();
		sleep(2);
		WaitforMoveTogroupMsg();
	}

	public void WaitforMoveTogroupMsg() throws InterruptedException {

		boolean MoveToGroupConfirmMsg = Initialization.driver
				.findElement(
						By.xpath("//span[contains(text(),'Device moved successfully to Group @SourceDonotTouch')]"))
				.isDisplayed();
		String pass = "PASS>>Device is Moved to group";
		String fail = "FAIL>>Device Did not Moved to group";
		ALib.AssertTrueMethod(MoveToGroupConfirmMsg, pass, fail);
		sleep(2);

	}

	public void MoveToGroupMultiple1() throws InterruptedException {
		MovetoGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SourceGroupName);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='@SourceDonotTouch']");
		sleep(4);
		SourceGroupInDeviceGrid.click();
		sleep(5);
		MoveButtonInsideMoveToGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}

	public void EnterSuffix(String InputSuffix) throws InterruptedException {

		name_suffix.clear();
		sleep(2);
		name_suffix.sendKeys(InputSuffix);
		sleep(2);
	}

	public void EnterLengthCount(String InputLength) throws InterruptedException {

		nameCount_length.clear();
		sleep(2);
		nameCount_length.sendKeys(InputLength);
		sleep(2);
	}

	public void clickingOnDoneButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 40);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Done']").click();
		Reporter.log("Clicked on Done button", true);
		wait.until(ExpectedConditions
				.presenceOfAllElementsLocatedBy(By.xpath("//android.widget.TextView[@text='Online']")));
		sleep(50);
	}

	@FindBy(xpath = "//*[@id='DeletedDeviceGrid']/tbody/tr/td[text()='" + Config.DeviceName + "']")
	private WebElement SelectDeviceInPendingDelete;

	@FindBy(xpath = "//*[@id='ddDeleteBtn']")
	private WebElement ForceDelete;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement ForceDeleteConfirmationPopUp;

	public void SelectDevicePendingDelete() throws InterruptedException {
		SelectDeviceInPendingDelete.click();
		sleep(1);
		ForceDelete.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']");
		sleep(1);
		ForceDeleteConfirmationPopUp.click();
		waitForXpathPresent("//span[text()='Device(s) deleted successfully.']");
		sleep(2);
		RefreshPendingDelete.click();
		waitForVisibilityOf("//*[@id='panelBackgroundDevice']/div[4]/div/div[contains(text(),'No Device available.')]");
		sleep(10);
	}

	@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")
	private WebElement pendingdeleteDeviceSearch;

	public void SearchPendingDeleteDeviceInput(String DeviceName) throws InterruptedException {
		pendingdeleteDeviceSearch.clear();
		pendingdeleteDeviceSearch.sendKeys(DeviceName);
		try {
			waitForXpathPresent("//p[text()='" + DeviceName + "']");
			sleep(10);
		} catch (Exception e) {
			Reporter.log(" Device Is Not Present In Console", true);
		}

	}

	@FindBy(xpath = "//*[@id='deletedSection']")
	private WebElement PendingDelete;

	@FindBy(xpath = "//*[@id='ddRefreshBtn']")
	private WebElement RefreshPendingDelete;

	public void PendingDeleteSection() throws InterruptedException {
		PendingDelete.click();
		waitForXpathPresent("//*[@id='ddDeleteBtn']");
		sleep(2);
		RefreshPendingDelete.click();
		sleep(2);
	}

	public void ConfirmationPopUpDeleteDevice() throws InterruptedException {
		deviceConfirmationDialogDeleteDevice.click();
		waitForXpathPresent("//span[text()='Device(s) deleted successfully.']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='deleteDeviceBtn']")
	private WebElement deleteDeviceBtn;
	@FindBy(xpath = "//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement deviceConfirmationDialogDeleteDevice;

	public void DeleteDeviceFromConsole() throws InterruptedException {
		deleteDeviceBtn.click();
		waitForXpathPresent("//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']");
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='0FileTransfer']")
	private WebElement FileTransferJobInCompletedSection;

	@FindBy(xpath = "//*[@id='groupJobQ_modal']/div[2]/ul/li/a[contains(text(),'Completed')]")
	private WebElement CompletedSection;

	public void VerifyJobCompletedJobSectionGroupRtClick() throws InterruptedException {
		CompletedSection.click();
		sleep(2);
		boolean JobPresent = FileTransferJobInCompletedSection.isDisplayed();
		String pass = "PASS >> FileTransfer Job is displayed ";
		String fail = "FAIL >> FileTransfer Job is NOT displayed";
		ALib.AssertTrueMethod(JobPresent, pass, fail);
		sleep(3);
	}

	public void EnableNix() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").click();
		sleep(2);

	}

	public void SearchDeviceInconsoleWithUpdatedDeviceName() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='tableContainer']/div[4]/div[1]/div[3]/input"))
				.sendKeys(Config.DeviceName);
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 40);
		wait1.until(ExpectedConditions
				.presenceOfAllElementsLocatedBy(By.xpath("//p[text()='" + Config.DeviceNameChange + "']")));
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr/td[2]/p[text()='Auto']")
	private WebElement VerifyNameOfDevice;

	public void VerifyDeviceName() throws InterruptedException {

		String DeviceName = VerifyNameOfDevice.getText();
		Reporter.log("DeviceName", true);
		String ExpectedName = "DeviceName";
		String Pass = "PASS>> Device name Changed";
		String Fail = "FAIL>>Device name didnot change";
		ALib.AssertEqualsMethod(ExpectedName, DeviceName, Pass, Fail);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupjobqueue']")
	private WebElement groupjobqueue;

	public void clickOnGrpJobQeue() throws InterruptedException {
		groupjobqueue.click();
		waitForXpathPresent("//h4[contains(text(),'Job History - @SourceDonotTouch')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='gridMenu']/li[5]/span[text()='Move to Group']")
	private WebElement MovetoGroup;

	@FindBy(xpath = "//*[@id='groupList-searchCont']/input")
	private WebElement SearchByGroupName;

	@FindBy(xpath = ".//*[@id='groupList']/ul/li[text()='@SourceDonotTouch']")
	private WebElement SourceGroupInDeviceGrid;

	@FindBy(xpath = ".//*[@id='groupList']/ul/li[text()='@SubGroupDonotTouch']")
	private WebElement SubGroupInDeviceGrid;

	@FindBy(xpath = "//*[@id='groupListModal']/div/div/div[3]/button")
	private WebElement MoveButtonInsideMoveToGroup;

	public void MoveToGroupMultipleSubGroup() throws InterruptedException {
		MovetoGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SubGroupDevice);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='@SubGroupDonotTouch']");
		sleep(4);
		SubGroupInDeviceGrid.click();
		sleep(5);
		MoveButtonInsideMoveToGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}

	public void SearchDeviceInconsoleGroups(String Devicename) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='tableContainer']/div[4]/div[1]/div[3]/input"))
				.sendKeys(Devicename);
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 40);
		wait1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//p[text()='" + Devicename + "']")));
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='groupjobqueueretrybtn']")
	private WebElement groupjobqueueretrybtn;

	public void SelectFailJob() throws InterruptedException {
		FileTransferFailJob.click();
		sleep(2);
	}

	public void ClickOnRetryBtn() throws InterruptedException {
		groupjobqueueretrybtn.click();
		waitForXpathPresent("//span[contains(text(),'Reapplied job successfully.')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='groupstree']/ul/li[18]/span[2]")
	private WebElement DropDownArrowGroup;

	public void ClickOnDropDownArrow() throws InterruptedException {

		DropDownArrowGroup.click();
		sleep(2);
	}
	// new Methods

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr[1]/td[2]")
	private WebElement AnyFirstDevice;

	public void RightClickMultipleDevice() throws InterruptedException, AWTException {
		/*
		 * Actions actionObj = new Actions(Initialization.driver);
		 * actionObj.keyDown(Keys.CONTROL) .sendKeys(Keys.chord("A"))
		 * .keyUp(Keys.CONTROL) .perform();
		 */

		Actions act = new Actions(Initialization.driver);
		act.contextClick(AnyFirstDevice).perform();
		sleep(4);
	}

	Actions action = new Actions(Initialization.driver);

	public void SelectingMultipleDevices() throws AWTException, InterruptedException {
		WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
		sleep(2);
	}

	@FindBy(xpath = "//div[@id='deleteGroupConf']//button[text()='Cancel']")
	private WebElement DeleteGroupCancelButton;

	public void ClickOnDeleteGroupCancelButton() throws InterruptedException {
		DeleteGroupCancelButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}

	// Madhu
	@FindBy(xpath = "//*[@id='moreBtn']")
	private WebElement moreButton;

	@FindBy(id = "movetogroup")
	private WebElement moveToGroup;

	public void clickOnMoreButton() throws InterruptedException {
		moreButton.click();
		Reporter.log("Clicked on More button of groups", true);
		sleep(2);
	}

	@FindBy(xpath = "(//input[@placeholder='Search By Group Name'])[2]")
	private WebElement SearchGroupTF;

	@FindBy(xpath = "//button[.='Move']")
	private WebElement MoveBtnGroupListPopUp;

	public void clickOnMoveToGroupSuccessMsg(String grp) throws InterruptedException {
		moveToGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys(grp);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='" + grp + "']");
		Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='" + grp + "']")).click();
		sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(4);
		Initialization.driver
				.findElement(By.xpath(
						"//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']"))
				.click();
		Reporter.log("Devices moved successfully", true);
		verifySuccessMsg(Config.AutoGroup, Config.AutoGroup1);

	}

	public void verifySuccessMsg(String sourceGrp, String destnationGrp) throws InterruptedException {
		boolean isdisplayed = true;
		try {
			waitForElementPresent(
					"//span[text()='Group " + sourceGrp + " moved successfully to Group " + destnationGrp + "']");
			Initialization.driver.findElement(By.xpath(
					"//span[text()='Group " + sourceGrp + " moved successfully to Group " + destnationGrp + "']"))
					.isDisplayed();
			sleep(2);
		} catch (Exception e) {
			isdisplayed = false;
		}

		Assert.assertTrue(isdisplayed, "FAIL >> Group moved successfully message is not displayed");
		Reporter.log("PASS >> Group moved successfully message is displayed", true);
		sleep(5);
	}

	public void clickOnMoveToGroupFailureMsg(String grp) throws InterruptedException {
		moveToGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys(grp);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='" + grp + "']");
		Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='" + grp + "']")).click();
		sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(4);
		Initialization.driver
				.findElement(By.xpath(
						"//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']"))
				.click();
		Reporter.log("Devices moved successfully", true);
		verifyFailureMsg(Config.AutoGroup, Config.homeGroup);

	}

	public void verifyFailureMsg(String sourceGrp, String destnationGrp) throws InterruptedException {
		boolean isdisplayed = true;
		try {
			waitForElementPresent(
					"//span[text()='Group " + sourceGrp + " could not be moved to Group " + destnationGrp + "']");
			Initialization.driver.findElement(By.xpath(
					"//span[text()='Group " + sourceGrp + " could not be moved to Group " + destnationGrp + "']"))
					.isDisplayed();
			sleep(2);
		} catch (Exception e) {
			isdisplayed = false;
		}

		Assert.assertTrue(isdisplayed, "FAIL >> Group could not be moved message is not displayed");
		Reporter.log("PASS >> Group could not be moved message is displayed", true);
		sleep(5);
	}

	public void verifyMoveToGroupFunc(String sourceGrp, String destnationGrp) throws InterruptedException {
		try {
			Initialization.driver
					.findElement(By.xpath("//*[@id='groupstree']/ul/li[contains(text(),'" + sourceGrp + "')]/span[2]"))
					.click();
			waitForXpathPresent("//div [@id='groupstree']/ul/li[text()='" + destnationGrp + "']");
			Initialization.driver
					.findElement(
							By.xpath("//*[@id='groupstree']/ul/li[contains(text(),'" + destnationGrp + "')]/span[2]"))
					.isDisplayed();
			Reporter.log("Expected group is displayed.", true);
			sleep(2);
		} catch (Exception e) {
			ALib.AssertFailMethod("Expected group is not displayed.");
		}
	}

	public void rightClickOnGroup(String group) throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		action.contextClick(
				Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='" + group + "']")))
				.perform();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='grpOperationMenu']/li/span[text()='Move to Group']")
	private WebElement moveToGroup_RightClick;

	public void clickOnMoveToGrp_RightClick() throws InterruptedException {
		moveToGroup_RightClick.click();
		Reporter.log("Clicked on move to group through right click", true);
		sleep(2);
	}

	@FindBy(xpath = "//li[@id='MoveDeviceWhileDeletingGroup']")
	private WebElement deleteGrpBtn_MoveDeviceToHome;

	public void clickOnDeleteGrpBtn_MoveDeviceToHome() throws InterruptedException {
		ClickOnDeleteGroup();
		sleep(2);
		deleteGrpBtn_MoveDeviceToHome.click();
		sleep(7);
	}
	
	//Keshav POM
	
			@FindBy(xpath="//h4[text()='Apply Job/Profile To Group']")
			private WebElement ApplyJobGroupToGroup;

			@FindBy(xpath = "//div[@id='applyJob_header']//button[@aria-label='Close']")
			private WebElement CloseButton;
			
			@FindBy(xpath="//div[@id = 'groupNote']/span[@class='mess']")
			private WebElement GroupNoteDisplayed;
			
			@FindBy(xpath="//div[@id='groupListModal']//div[@class='close']")
			private WebElement CloseBtn;
			
			@FindBy(xpath="//*[@id='grpOperationMenu']/li/span[text()='Move to Group']")
			private WebElement VerifyMoveToGroupUIByRightClick;
			
			public void verifyOfApplyJobGroupToGroup() throws InterruptedException
			{
				try {
					ApplyJobGroupToGroup.isDisplayed();
					Reporter.log("PASS >> 'Apply Job/Profile To Group' is displayed correctly....",true);
				}catch (Exception e) {
					ALib.AssertFailMethod("FAIL >> 'Apply Job/Profile To Group' is not displayed correctly....");
					sleep(2);
					CloseButton.click();
				}
				sleep(2);
				CloseButton.click();
			}

			public void VerifyMoveToGroupUIFunctionality() throws InterruptedException
			{

							try {
								moveToGroup.isDisplayed();
								sleep(3);
								moreButton.click();
								sleep(3);
								Reporter.log("PASS >> 'Move to Group' is displayed correctly...",true);
							}catch (Exception e) {
								ALib.AssertFailMethod("FAIL >> Move to Group is not displayed correctly...");
								
							}
						}
			
			public void VerifyMoveToGroupUIByRightClick(String group) throws InterruptedException
			{
				Actions action=new Actions(Initialization.driver);
				action.contextClick(Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+group+"']"))).perform();
				sleep(2);
				try {
					moveToGroup_RightClick.isDisplayed();
					sleep(5);
					Reporter.log("PASS >> 'Move to Group' is displayed correctly...",true);
				}catch (Exception e) {
					ALib.AssertFailMethod("FAIL >> Move to Group is not displayed correctly...");
					sleep(6);
				}
			}

			public void VerifyNoteDisplayedOnClickingMore() throws InterruptedException
			{
					sleep(2);
					moveToGroup.click();
					sleep(4);
					String actual= GroupNoteDisplayed.getText();
					String expected = Config.GroupNote;
					String PassStatement = "PASS>> Group Note "+expected+" displayed successfully";
					String FailStatement = "FAIL >>Group Note wrongly displayed or Not displayed" ;
					ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
					sleep(2);
					CloseBtn.click();
				}
			
			public void VerifyNoteDisplayedOnRightClick(String group) throws InterruptedException {
				sleep(2);
				Actions action=new Actions(Initialization.driver);
				action.contextClick(Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+group+"']"))).perform();
				sleep(2);
				VerifyMoveToGroupUIByRightClick.click();
				sleep(4);
				String actual= GroupNoteDisplayed.getText();
				String expected = Config.GroupNote;
				String PassStatement = "PASS>> Group Note "+expected+" displayed successfully";
				String FailStatement = "FAIL >>Group Note wrongly displayed or Not displayed" ;
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				sleep(2);
				CloseBtn.click();
			}

}