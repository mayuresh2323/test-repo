package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;

public class DeviceInfoPanel_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	CommonMethods_POM common=new CommonMethods_POM();

	@FindBy(xpath="//span[@id='deviceOwnerName']/following-sibling::a")
	private WebElement EditdeviceOwnerName;

	@FindBy(xpath="//span[@id='deviceNotes']/following-sibling::a")
	private WebElement EditdeviceNotes;

	@FindBy(xpath="//button[@type='submit']/following-sibling::button")
	private WebElement DeviceOwner_NotesNameCancelBtn;

	@FindBy(xpath="//h3[text()='Add Notes']")
	private WebElement AddNotesWindowTitle;

	@FindBy(xpath =".//*[@class='editable-input']/textarea" )
	private WebElement textAreaAddNotesWindow;

	@FindBy(xpath="//button[@type='submit']")
	private WebElement SaveButtonAddNotes;
	
	@FindBy(xpath="//span[contains(text(),'Device notes have been added for the device named "+"\""+Config.OfflineAndroid_Device+"\"')]")
	private WebElement deviceNotesChangedMessage;

	@FindBy(xpath="//span[text()='Maximum 256 characters are allowed in device notes.']")
	private WebElement warningMessageWhenNotesExceeds;

	@FindBy(xpath="//p[text()='rocking']")
	private WebElement isNotesDisplayed;

	//@FindBy(id="modal___b477d335-17d5-4927-988c-53c26e2e6fc6")
	//private WebElement iPhone6Plus;

	@FindBy(id=".//*[@id='modal___f36cc4e5-0409-40ad-8d69-eb184306e7d8']")
	private WebElement iPadMini4;

	@FindBy(id="notes___b477d335-17d5-4927-988c-53c26e2e6fc6") //iPhone6Plus
	private WebElement isiOSNotesDisplayed;



	public void ClickOnEditdeviceNotes_UM() throws InterruptedException
	{
		EditdeviceNotes.click();
		sleep(2);
	}

	public void ClickOnEditdeviceOwnerName_UM() throws InterruptedException
	{
		EditdeviceOwnerName.click();
		sleep(5);
	}

	public void ClickOnEditdeviceNotes() throws InterruptedException
	{
		EditdeviceNotes.click();
		waitForXpathPresent("//button[@type='submit']");
		sleep(2);
	}

	public void ClickOnEditdeviceOwnerName() throws InterruptedException
	{
		EditdeviceOwnerName.click();
		waitForidPresent("device_name_input");
		sleep(2);
	}

	@FindBy(xpath="//*[@id='dis-card']/div/span[contains(text(),'"+Config.DeviceName+"')]")
	private WebElement DeviceModelNameAndroid;

	public void VerifyDeviceNameAndroid() throws InterruptedException {
		String ActualResult3=DeviceModelNameAndroid.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=ActualResult3 ;
		String Pass2="PASS>> "+ExpectedResult3+" Device Name For Android is Displayed";
		String Fail2="FAIL>> "+ExpectedResult3+"  Device Name For Android is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail2);
		sleep(2);
	}

	public void ClickOnDeviceOwner_NotesNameCancelBtn() throws InterruptedException
	{
		DeviceOwner_NotesNameCancelBtn.click();
		sleep(5);
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}

	public void SelectAllNotesAndEnterNotes(String note) throws InterruptedException
	{
		textAreaAddNotesWindow.sendKeys(Keys.CONTROL + "a");
		textAreaAddNotesWindow.sendKeys(note);
		sleep(1);

	}

	public void TitleAddNotesWindow() throws InterruptedException
	{
		boolean isTitleCorrect = AddNotesWindowTitle.isDisplayed();
		String pass = "PASS >> 'Add Notes' window is displayed";
		String fail = "FAIL >> 'Add Notes' window is NOT displayed";
		ALib.AssertTrueMethod(isTitleCorrect, pass, fail);
		sleep(2);
	}

	public void SelectAllNotesAndEnterNotes() throws InterruptedException
	{
		textAreaAddNotesWindow.sendKeys(Keys.CONTROL + "a");
		textAreaAddNotesWindow.sendKeys(Config.EnterNotes);
		sleep(1);

	}

	public void WarningMessageWhenAddNotesExceeds() throws InterruptedException
	{
		textAreaAddNotesWindow.sendKeys(Keys.CONTROL + "a");
		textAreaAddNotesWindow.sendKeys(Config.NotesExceeds256Characters);
		sleep(2);
		ClickOnSaveButtonAddNotes();
		sleep(1);
		boolean isWarningMessageDisplayed = warningMessageWhenNotesExceeds.isDisplayed();
		String pass = "PASS >> Warning message 'Maximum 256 characters are allowed in device notes.' is displayed";
		String fail = "FAIL >> Warning message is not displayed";
		ALib.AssertTrueMethod(isWarningMessageDisplayed, pass, fail);

	}

	public void ClickOnSaveButtonAddNotes() throws InterruptedException
	{
		SaveButtonAddNotes.click();
		sleep(4);
	}
	public void VerifyEditedNote() throws InterruptedException
	{
		boolean value = deviceNotesChangedMessage.isDisplayed();
		String pass = "PASS >> Device Notes changed successfully displayed";
		String fail = "FAIL >> Device Notes changed successfully NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	sleep(2);
	}

	public void isNotesdisplayed()
	{
		boolean isDisplayed = isNotesDisplayed.isDisplayed();
		String pass = "PASS >> Notes displayed correctly";
		String fail = "FAIL >> Notes displayed is wrong";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
	}

	public void EnteriOSNotes() throws InterruptedException{
		iPadMini4.click();
		sleep(4);
	}

	public void isiOSNotesDisplayed()
	{
		String expected = isiOSNotesDisplayed.getText();
		String actual = Config.EnterNotes;
		String pass = "PASS >> Notes for iOS is "+expected+" ";
		String fail = "FAIL >> Notes for iOS is not displayed";
		ALib.AssertEqualsMethod(expected, actual, pass, fail);

	}

	public void DisableNotesColumn()
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='tableContainer']/div[3]/div[1]/div[1]/div/div/ul[1]/li/label/span/input"));
		ls.get(26).click(); // disabling Notes

	}


	////////Nix Permission CheckList///////////////////////

	@FindBy(id="NixPermissionData")
	private WebElement DeviceInfo_NixPermissionShowButton;

	public void ClickOnNixPermissionCheckListOption() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Nix Permission Checklist']").click();
		sleep(3);
	}

	public void DisablingDeviceAdminInNix(String Text) throws InterruptedException
	{
		common.commonScrollMethod(Text);
		String Status=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']/../../android.widget.LinearLayout/android.widget.CheckBox").getAttribute("checked");
		if(Status.equals("false"))
		{
			Reporter.log("Device Admin Is Already Disabled",true);
		}
		else
		{
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']/../../android.widget.LinearLayout/android.widget.CheckBox").click();
		}
		sleep(2);
	}

	String nixPermissionStatus_Nix;

	public void ReadingStatusInNixPermissionCheckList(String PermissionName) throws InterruptedException
	{
		nixPermissionStatus_Nix=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+PermissionName+"']/../android.widget.CheckBox").getAttribute("checked");
		sleep(2);
	}

	public void VerifyingNixPermissionCheckList(String PermissionNameInConsole)
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//div[@id='SureMDMNixpermissionList']//span[text()='"+PermissionNameInConsole+"']/following-sibling::input"));
		js.executeScript("arguments[0].scrollIntoView()",ele);
		String nixPermissionStatus_Console=ele.getAttribute("value");
		if(nixPermissionStatus_Nix.equals("true"))
		{
			if(nixPermissionStatus_Console.equals("Granted"))
			{
				Reporter.log("PASS>>> "+PermissionNameInConsole+" Is Granted",true);
			}
			else
			{
				ALib.AssertFailMethod("FAIL>>> Nix Permission Is Not Shown Correctly");
			}
		}

		else if(nixPermissionStatus_Nix.equals("false"))
		{
			if(nixPermissionStatus_Console.equals("Denied"))
			{
				Reporter.log("PASS>>> Nix Permission Is Shown Correctly",true);
			}
			else
			{
				ALib.AssertFailMethod("FAIL>>> Nix Permission Is Not Shown Correctly");
			}
		}

		else
		{
			ALib.AssertFailMethod("FAIL>>> Nix Permission Is Not Shown");
		}
	}

	public void ClickOnDeviceInfoNixPermissionShowButton() throws InterruptedException
	{
		DeviceInfo_NixPermissionShowButton.click();
		waitForXpathPresent("//h4[text()='SureMDM Nix Permissions Status']");
		sleep(3);
	}

	public void VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix(String PermissionNameInConsole) throws InterruptedException
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//div[@id='SureMDMNixpermissionList']//span[text()='"+PermissionNameInConsole+"']/following-sibling::input"));
		js.executeScript("arguments[0].scrollIntoView()",ele);
		String nixPermissionStatus_Console=ele.getAttribute("value");
		if(nixPermissionStatus_Console.equals("Denied"))
		{
			Reporter.log("PASS>>> Nix Permission Modified Successfully",true);
		}
		else
		{
			ClosingNixPermissionStatusInConsole();
			ALib.AssertFailMethod("FAIL>>> Nix Permission Is Not Modified");
		}
	}
	public void VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix(String PermissionNameInConsole) throws InterruptedException
	{ 
		WebElement ele = Initialization.driver.findElement(By.xpath("//div[@id='SureMDMNixpermissionList']//span[text()='"+PermissionNameInConsole+"']/following-sibling::input"));
		js.executeScript("arguments[0].scrollIntoView()",ele);
		String nixPermissionStatus_Console=ele.getAttribute("value");
		if(nixPermissionStatus_Console.equals("Granted"))
		{
			Reporter.log("PASS>>> "+PermissionNameInConsole+" Is Granted",true);
		}
		else
		{
			ClosingNixPermissionStatusInConsole();
			ALib.AssertFailMethod("FAIL>>>"+PermissionNameInConsole+" Is Denied Even It Is Allowed In Nix");
		}
	}

	public void AllowingPermissionsInNixpermissionCheckList(String PermissionName) throws InterruptedException
	{
		String Status=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+PermissionName+"']/../android.widget.CheckBox").getAttribute("checked");
		sleep(4);
		if(Status.equals("false"))
		{
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+PermissionName+"']/../android.widget.CheckBox").click();
		}
		else
		{
			Reporter.log("Permission Already Allowed",true);
		}
		sleep(2);
	}


	public void ClosingNixPermissionStatusInConsole() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//h4[text()='SureMDM Nix Permissions Status']/parent::div/button")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}

	public void ClickOnAllowButtonInBatteryUsagePopup() throws InterruptedException
	{
		try
		{
			Initialization.driverAppium.findElementById("android:id/button1").click();
			sleep(2);
		}
		catch (Exception e) 
		{
			System.out.println("Permission Is Allowed Already");
		}
	}

	public void ClickOnConfigUnsourceToggleButton() throws InterruptedException, IOException
	{
		try
		{
			Initialization.driverAppium.findElementById("android:id/switch_widget").click();
			sleep(2);
			Runtime.getRuntime().exec("adb shell input keyevent 4");
		}
		catch (Exception e)
		{
			Runtime.getRuntime().exec("adb shell input keyevent 4");
		}

	}

	public void UnInstallingNix() throws InterruptedException, IOException
	{
		Runtime.getRuntime().exec("adb uninstall com.nix");
		sleep(5);
	}

	public void InstallingNix(String command) throws InterruptedException, IOException
	{
		Runtime.getRuntime().exec(command);
		sleep(5);
	}
	public void AllowNixRunPermission() 
	{
		WebElement allowBtn = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']");

		try
		{
			for(int i=0;i<7;i++)
			{
				allowBtn.click();
				sleep(2);
			}
		}
		catch(Exception e) {
			System.out.println("Clicked on all allow button");
		}
	}

	public void ClickOnCancelButtonIAcctivateDeviceAdminPrompt() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Cancel']").click();
		sleep(3);
	}

	public void AllowingUsageAccessPermission() throws InterruptedException
	{
		Initialization.driverAppium.findElementById("android:id/button1").click();
		sleep(2);
		common.commonScrollMethod("SureMDM Nix");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureMDM Nix']").click();
		sleep(2);
		Initialization.driverAppium.findElementById("android:id/switch_widget").click();
		sleep(2);
	}

	public void AllowingBatteryUsagePermission() throws InterruptedException
	{
		Initialization.driverAppium.findElementById("android:id/button1").click();
		sleep(3);
	}

	@FindBy(xpath="//button[@id='addTF_rowbtn']")
	private WebElement AddFenceButton;

	@FindBy(xpath="(//article[@id='timeFence_mainContainer']//span[contains(text(),'End Time')]/following-sibling::input)[2]")
	public WebElement TimeFenceEndTime2;

	public void ClickOnAddFenceButton() throws InterruptedException {
		AddFenceButton.click();
		Reporter.log("Clicked on Add Fence Button", true);
		sleep(2);
	}

	@FindBy(xpath="//span[contains(text(), 'All Days')]")
	private WebElement TimeFenceAllDays;

	@FindBy(xpath="(//span[contains(text(), 'All Days')])[2]")
	private WebElement TimeFenceAllDays2;
	public void ClickOnTimeFenceAllDaysOption() throws InterruptedException {
		action.click(TimeFenceAllDays).build().perform();
		Reporter.log("Clicked on All Days Option", true);
		sleep(4);
	}
	
	public void ClickOnTimeFenceAllDaysOption2() throws InterruptedException {
		action.click(TimeFenceAllDays2).build().perform();
		Reporter.log("Clicked on All Days Option in 2nd Fence", true);
		sleep(4);
	}

	@FindBy(xpath="//button[@id='timeFenc_save']")
	private WebElement TimeFenceSaveButton;
	public void ClickOnTimeFenceSaveButton() throws InterruptedException {
		TimeFenceSaveButton.click();
		Reporter.log("Clicked on Save Button in Time Fence", true);
		sleep(2);
	}


	@FindBy(xpath="//input[@id='SendDeviceAlretWarningTimeJobOut']")
	private WebElement SendDeviceAlretWarningTimeJobOut;



	@FindBy(xpath="//input[@id='SendMDMAlertWarningTimeJobOut']")
	private WebElement SendMDMAlertWarningTimeJobOut;


	@FindBy(xpath="//input[@id='SendEmailWarningTimeJobOut']")
	private WebElement SendEmailWarningTimeJobOut;

	@FindBy(xpath="//div[@id='EmailAddressPopTimeJobOut']//input[@id='EmailId']")
	private WebElement EnterEmailIdForFenceExied;

	@FindBy(xpath="//div[@id='EmailAddressPopTimeJobOut']/div[1]/div[1]/div[3]/button")
	private WebElement FenceExiedEmailDoneButton;


	@FindBy(xpath="//button[@id='timeFenc_save']")
	private WebElement TimeFenceEditSaveButton;


	@FindBy(xpath="//input[@id='job_name_fence']")
	private WebElement JobNameFence;

	@FindBy(xpath="//ul[@id='timefencing_tabs']/li[2]")
	private WebElement FenceEntered;
	public void ClickOnFenceEnteredTab() throws InterruptedException {
		FenceEntered.click();
		Reporter.log("Clicked on Fence Entered tab", true);
		sleep(2);
	}

	@FindBy(xpath="//ul[@id='networkfencing_tabs']/li[3]")
	private WebElement NetworkFenceExited;

	@FindBy(xpath="//ul[@id='timefencing_tabs']/li[3]")
	private WebElement FenceExited;
	public void ClickOnFenceExiedTab() throws InterruptedException {
		FenceExited.click();
		Reporter.log("Clicked on Fence Exied tab", true);
		sleep(2);
	}

	@FindBy(xpath="//div[@id='time_fencing_job']")
	private WebElement Timefencebutton; 
	public void ClickOnTimeFence() throws InterruptedException {
		waitForidPresent("time_fencing_job");
		Timefencebutton.click();
		waitForXpathPresent("//input[@id='enableTimeFence']");
		Reporter.log("Clicked On Time Fence", true);
		sleep(2);
	} 

	public void VerifyEnableTimeFenceIsSelected(boolean flag) throws InterruptedException {
		boolean selected = EnableTimefence.isSelected();
		if(flag==selected) 
		{
			Reporter.log("Enable Time Fence is selected by default", true);
		}
		else {
			ALib.AssertFailMethod("Enable Time Fence is not selected by default");
		}

		sleep(1);
	}

	@FindBy(xpath="//ul[@id='timefencing_tabs']/li[1]")
	private WebElement SelectFence;

	public void VerifyTimeFenceTabs() {
		if(SelectFence.isDisplayed() && FenceEntered.isDisplayed() && FenceExited.isDisplayed())
		{
			Reporter.log("Time Fence Tabs are displaying successfully", true);
		}
		else
		{
			ALib.AssertFailMethod("Time Fence Tabs are not displaying");
		}
	}

	@FindBy(xpath="//span[text()='Atleast one job or alert should be added for fence entered or fence exited.']")
	private WebElement AlertJobsErrorMsg;

	public void VerifyErrorMessageWithoutJobAndAlert() throws InterruptedException 
	{
		TimeFenceEditSaveButton.click();
		waitForXpathPresent("//span[text()='Please add at least one job or alert tied to entering or exiting the Time Fence.']");
		Reporter.log("PASS >> Please add at least one job or alert tied to entering or exiting the Time Fence. Message is displayed.", true);
		sleep(5);
	}

	public void ClickedOnTimeFenceJobSave() throws InterruptedException {
		TimeFenceEditSaveButton.click();
		waitForXpathPresent("//input[@id='job_name_fence']");
		Reporter.log("Clicked on Time Fence save button", true);
		sleep(3);
	}


	public void UpdatedTimeFence() throws InterruptedException {
		
		String TimeFenceDeviceInfo=TimeFenceWindows.getText();
		String Expected=TimeFenceDeviceInfo;
		String pass="PASS>> TimeFenceDeviceInfo is updated as "+TimeFenceDeviceInfo+"";
		String fail="FAIL>> TimeFenceDeviceInfo is NOT updated as "+TimeFenceDeviceInfo+"";
		ALib.AssertEqualsMethod(Expected, TimeFenceDeviceInfo, pass, fail);
		sleep(2);
	}
	
	@FindBy(xpath="//*[@id='dis-card']/div[25]/span")
	private WebElement AndroidTimeFence;
	
	public void AndroidUpdatedTimeFence() throws InterruptedException {
		
		String TimeFenceDeviceInfo=AndroidTimeFence.getText();
		String Expected=TimeFenceDeviceInfo;
		String pass="PASS>> TimeFenceDeviceInfo is updated as "+TimeFenceDeviceInfo+"";
		String fail="FAIL>> TimeFenceDeviceInfo is NOT updated as "+TimeFenceDeviceInfo+"";
		ALib.AssertEqualsMethod(Expected, TimeFenceDeviceInfo, pass, fail);
		sleep(2);
	}

	public void ClickAndEnterEmailForFenceExited(boolean value, String Email) throws InterruptedException {
		boolean flag = SendEmailWarningTimeJobOut.isSelected();
		if(flag==value) 
		{
			SendEmailWarningTimeJobOut.click();
			Reporter.log("Clicked on Send Email Alert For FenceExited", true);
			waitForXpathPresent("//div[@id='EmailAddressPopTimeJobOut']//input[@id='EmailId']");
			sleep(2);
			EnterEmailIdForFenceExied.clear();
			EnterEmailIdForFenceExied.sendKeys(Email);
			Reporter.log("Entered EmailID for Fence Exied", true);
			sleep(2);
			FenceExiedEmailDoneButton.click();
			Reporter.log("Clicked on Done button", true);
		}
		Reporter.log("Email Fence Exited is selected", true);
		sleep(1);
	}
	
	@FindBy(xpath="//*[@id='NetworkFenceEditBtn']")
	private WebElement EditNtwkFence;
	
	@FindBy(xpath="//*[@id='networkfencing_modal']/div/div/div[1]/button")
	private WebElement ClosePopUpnetworkFence;
	
	public void clickOnEditNetworkFenceDeviceInfo() throws InterruptedException {
		EditNtwkFence.click();
		waitForXpathPresent("//*[@id='networkfencing_modal']/div/div/div[1]/h4");
		sleep(2);
		
	}
	
	public void ClickOnMailBox() throws InterruptedException {
		Initialization.driverAppium.findElement(By.id("com.nix:id/mail")).click();
		Reporter.log("Clicked on MailBox", true);
		sleep(2);
	}
	
	public void VerifyTextMessage(String sub) throws InterruptedException {
		String Subject = Initialization.driverAppium.findElement(By.id("com.nix:id/subject")).getText();
		if(Subject.equals(sub)) 
		{
			Reporter.log("Text Message is recieved successfully.", true);
		}
		else
		{
			ALib.AssertFailMethod("Text Message is not recieved.");
		}
	}
	
	@FindBy(xpath="//*[@id='networkFence_mainContainer']/section/div[3]/div[1]/ul/li[2]/div/div[1]")
	private WebElement DeleteNetworkFence;
	
	@FindBy(xpath="//*[@id='ConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement YesDeleteBtnNetworkFEnce;
	
	public void DeleteNF() throws InterruptedException {
		DeleteNetworkFence.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[1]/p[text()='Are you sure you want to remove the fence?']");
		sleep(2);
		YesDeleteBtnNetworkFEnce.click();
		sleep(4);
	}
	
	@FindBy(xpath="//*[@id='networkinfo-card']/div[3]/p")
	private WebElement MacEMM;
	
	@FindBy(xpath="(//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='MAC Address']/following-sibling::div/p)[2]")
	private WebElement MacEMM_Ethernet;
	
	public void VerifyEMMmacAddress() throws InterruptedException {
		String expected = MacEMM.getText();
		String actual = expected;
		String pass = "PASS >> EMM Mac Address is "+expected+" ";
		String fail = "FAIL >> EMM Mac Address is not displayed";
		ALib.AssertEqualsMethod(expected, actual, pass, fail);
		sleep(2);
		
		String expected1 = MacEMM_Ethernet.getText();
		String actual1 = expected1;
		String pass1 = "PASS >> EMM Mac Address is "+expected1+" ";
		String fail1 = "FAIL >> EMM Mac Address is not displayed";
		ALib.AssertEqualsMethod(expected1, actual1, pass1, fail1);
		sleep(2);
	}
	
	@FindBy(xpath="//*[@id='storagemem']/p[text()=' free of ']")
	private WebElement StorageMemoryLINUX;
	
	@FindBy(xpath="//*[@id='programmem']/p[text()=' free of ']")
	private WebElement programmem;
	
	public void VerifyMemoryOptn() throws InterruptedException {
			String expected = StorageMemoryLINUX.getText();
			String actual = expected;
			String pass = "PASS >> StorageMemory LINUX is "+expected+" ";
			String fail = "FAIL >> StorageMemory LINUX is not displayed";
			ALib.AssertEqualsMethod(expected, actual, pass, fail);
			sleep(2);
			
			String expected1 = programmem.getText();
			String actual1 = expected1;
			String pass1 = "PASS >> programmem LINUX is "+expected+" ";
			String fail1 = "FAIL >> programmem LINUX is not displayed";
			ALib.AssertEqualsMethod(expected1, actual1, pass1, fail1);
			sleep(2);
}
	
	@FindBy(xpath="//*[@id='dis-card']/div[7]/a[text()='Edit']")
	private WebElement EditButtonClickParentDevice;
	
	@FindBy(xpath="//*[@id='device_name_input']")
	private WebElement device_name_input;
	public void VerifyChangingParentDeviceNameToChangeNameOfChildIoTdevices() throws InterruptedException {
		EditButtonClickParentDevice.click();
		sleep(2);
		waitForXpathPresent("//*[@id='device-nme-editable']/span[1][text()='Device Name']");
		sleep(2);
		device_name_input.clear();
		sleep(2);
		device_name_input.sendKeys(Config.EditingParentDeviceName);
		sleep(2);
		SaveButtonAddNotes.click();
		waitForXpathPresent("//span[text()='Device name changed successfully']");
		sleep(30);
	}
	
	@FindBy(xpath="//input[@id='SendEmailWarningNetworkJobOut']")
	private WebElement SendEmailWarningNetworkJobOut;
	
	@FindBy(xpath="//div[@id='EmailAddressPopNetworkJobOut']//input[@id='EmailId']")
	private WebElement EnterEmailIdForNetworkFenceExied;
	
	@FindBy(xpath="//div[@id='EmailAddressPopNetworkJobOut']/div[1]/div[1]/div[3]/button")
	private WebElement NetworkFenceExiedEmailDoneButton;
	public void ClickAndEnterEmailForNetworkFenceExited(boolean value, String Email) throws InterruptedException {
		boolean flag = SendEmailWarningNetworkJobOut.isSelected();
		if(flag==value) 
		{
			SendEmailWarningNetworkJobOut.click();
			Reporter.log("Clicked on Send Email Alert For FenceExited", true);
			waitForXpathPresent("//div[@id='EmailAddressPopNetworkJobOut']//input[@id='EmailId']");
			sleep(2);
			EnterEmailIdForNetworkFenceExied.clear();
			EnterEmailIdForNetworkFenceExied.sendKeys(Email);
			Reporter.log("Entered EmailID for Fence Exied", true);
			sleep(2);
			NetworkFenceExiedEmailDoneButton.click();
			Reporter.log("Clicked on Done button", true);
		}
		Reporter.log("Email Fence Exited is selected", true);
		sleep(1);
	}
	public void ClickOnFenceDeleteButton() throws InterruptedException 
	{
		FenceDeleteButton.click();
		Reporter.log("Clicked on Fence Delete button", true);
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]");
		sleep(2);
	}

	public void ClickOnCloseButtonOfAlertMsg() throws InterruptedException 

	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='CLOSE']").click();
		sleep(4);

	}

	public void CheckFenceExitedMDMAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendMDMAlertWarningTimeJobOut.isSelected();
		if(flag==value) {
			SendMDMAlertWarningTimeJobOut.click();
		}
		Reporter.log("Clicked on Fence exited MDM Alert", true);
		sleep(1);
	}

	public void ClickOnFenceExitedEmailDoneButton() throws InterruptedException {
		FenceExiedEmailDoneButton.click();
		Reporter.log("Clicked on Fence Exited Email Done Button", true);
		sleep(1);
	}

	public void EnterJobnameFence(String value) throws InterruptedException {
		JobNameFence.sendKeys(value);
		Reporter.log("Entered Fence job name", true);
		sleep(1);
	}

	public void CheckFenceExitedDeviceAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendDeviceAlretWarningTimeJobOut.isSelected();
		try 
		{
			if(flag==value) {
				SendDeviceAlretWarningTimeJobOut.click();

			}
			Reporter.log("Clicked on Fence exited Device Alert", true);
			sleep(1);
		}
		catch(Exception e) {
			if(flag==value) {
				SendDeviceAlretWarningTimeJobOut.click();

			}
			Reporter.log("Clicked on Fence exited Device Alert", true);
			sleep(1);
		}
	}
	public void VerifyNetworkFenceExitAlertForDevice(String devicename, String Fence, int waitTime) throws InterruptedException 
	{
		sleep(4);

		try 
		{

			WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Network fence exit alert for device "+Config.DeviceName+"']"))).isDisplayed();
			sleep(5);
			String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text, '"+Fence+" exit alert for device "+devicename+"')]").getText();
			System.out.println("Alert Message for Time Fence Exit: " +alertMessage);
			String ExpectedAlertMessage = "Network fence exit alert for device "+Config.DeviceName+"";
			String PassStatement=""+alertMessage+": Alert messeage is displayed.";
			String FailStatement=""+alertMessage+": Alert messeage is not displayed.";
			ALib.AssertEqualsMethod(ExpectedAlertMessage, alertMessage, PassStatement, FailStatement);

		}
		catch(Exception e) 
		{
			System.out.println("error");
		}

	}
	public void VerifyNetworkFenceEntryAlertForDevice(String devicename, String Fence, int waitTime) throws InterruptedException 
	{
		sleep(4);

		try 
		{

			WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Network fence entry alert for device "+Config.DeviceName+"']"))).isDisplayed();
			sleep(5);
			String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text, '"+Fence+" entry alert for device "+devicename+"')]").getText();
			System.out.println("Alert Message for Time Fence Entry: " +alertMessage);
			String ExpectedAlertMessage = "Network fence entry alert for device "+Config.DeviceName+"";
			String PassStatement=""+alertMessage+": Alert messeage is displayed.";
			String FailStatement=""+alertMessage+": Alert messeage is not displayed.";
			ALib.AssertEqualsMethod(ExpectedAlertMessage, alertMessage, PassStatement, FailStatement);

		}
		catch(Exception e) 
		{
			System.out.println("error");
		}

	}


	public void VerifyFenceEntryAlertForDevice(String devicename, String Fence, int waitTime) {

		try 
		{
			WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Time fence entry alert for device "+Config.DeviceName+"']"))).isDisplayed();
			sleep(5);

			String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Fence+" entry alert for device "+devicename+"']").getText();
			System.out.println(alertMessage);
			String expectedMessage = "Time fence entry alert for device "+Config.DeviceName+"";
			String PassStatement=""+alertMessage+": Alert messeage is displayed.";
			String FailStatement=""+alertMessage+": Alert messeage is not displayed.";
			ALib.AssertEqualsMethod(expectedMessage, alertMessage, PassStatement, FailStatement);
		}
		catch(Exception e) 
		{
			System.err.println("error");
		}
	}

	@FindBy(xpath="//div[@id='notify_policy']")
	private WebElement Notify_policy;
	public void ClickOnNotificationPolicyJob() throws InterruptedException 
	{
		Notify_policy.click();
		waitForXpathPresent("//input[@id='DisableNotificationPolicy']");
		sleep(2);
	}

	@FindBy(xpath="//input[@id='DisableNotificationPolicy']")
	private WebElement DisableNotificationPolicy;
	public void CheckDisableNotificationPolicy(boolean flag) throws InterruptedException 
	{
		boolean value = DisableNotificationPolicy.isSelected();
		if(value==flag)
		{
			DisableNotificationPolicy.click();
		}
		sleep(2);
	}

	@FindBy(id="jobFolderName")
	private WebElement jobFolderName;

	@FindBy(id="foldername_ok")
	private WebElement foldername_ok;

	public void EnterAndSaveNewFolder(String name) throws InterruptedException 
	{
		jobFolderName.sendKeys(name);
		sleep(1);
		foldername_ok.click();
		waitForXpathPresent("//span[text()='New folder created.']");
		waitForXpathPresent("//p[text()='"+name+"']");
		sleep(6);
	}

	@FindBy(id="job_new_folder")
	private WebElement NewFolder;
	public void ClickOnNewFolder() throws InterruptedException 
	{
		NewFolder.click();
		waitForidPresent("jobFolderName");
		sleep(2);
	}

	@FindBy(xpath="//textarea[@id='messg_textarea']")
	private WebElement InboxBodyConsole;

	@FindBy(xpath="//div[@id='mail_main_pg']/div[2]/div[1]/div[1]/div[1]/button")
	private WebElement RefreshInbox;

	@FindBy(xpath="//div[@id='mail_main_pg']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement InboxSearch;

	@FindBy(xpath="//table[@id='mailGrid']/tbody/tr/td[2]")
	private List<WebElement> InboxFromConsoleData;

	@FindBy(xpath="//table[@class='table-striped jambo_table']/tbody/tr[1]/td[3]")
	private List<WebElement> message;

	@FindBy(xpath="//table[@id='mailGrid']/tbody/tr/td[3]")
	private List<WebElement> InboxSubConsoleData;
	public void VerifyFenceAlertFromConsole(String devicename, String value, String Fence) throws InterruptedException {
//		InboxSearch.clear();
//		InboxSearch.sendKeys(Config.DeviceName);
		sleep(5);
		action.click(RefreshInbox).build().perform();

		sleep(4);
		boolean flag;
		for(int i=0;i<1;i++) 
		{
			String InboxFromConsoleDevice = InboxFromConsoleData.get(i).getText();
			String InboxdeviceSubject = InboxSubConsoleData.get(i).getText();			
			Reporter.log("Inbox Subject: "+InboxdeviceSubject, true);
			waitForXpathPresent("//table[@id='mailGrid']/tbody/tr[1]/td[3]");
			sleep(2);
			InboxSubConsoleData.get(i).click();
			sleep(2);
			waitForXpathPresent("//textarea[@id='messg_textarea']");
			//		String[] mailDetails= {"Limit2", "Limit1"};
			if(InboxdeviceSubject.contains(value) && InboxdeviceSubject.contains(Fence) && InboxFromConsoleDevice.equals(devicename))
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			sleep(2);
			String PassStatement="PASS >> TextArea: "+InboxBodyConsole.getText()+" << Message is Recieved from the console";
			String FailStatement="FAIL >> TextArea: "+InboxBodyConsole.getText()+" << Message is not Recieved from the console";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
	}

	@FindBy(xpath="//table[@class='table-striped jambo_table']/tbody/tr[1]/td[3]")
	private WebElement fenceSubText;

	public void VerifyFenceAlertFromEmail(String devicename, String value, String Fence) throws InterruptedException, AWTException {
		JavascriptExecutor jse = (JavascriptExecutor)Initialization.driver;
		jse.executeScript("window.open()");
		String Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get(Config.NotifiedUrl_Misc);
		sleep(5);
		Initialization.driver.findElement(By.xpath("//input[@id='addOverlay']")).sendKeys(Config.NotifiedEmail_Misc);
		sleep(3);

		Actions action = new Actions(Initialization.driver);
		action.sendKeys(Keys.ENTER).build().perform();

		waitForXpathPresent("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[3]");
		System.out.println(fenceSubText.getText());

		boolean flag;
		for(int i=0;i<1;i++) 
		{	
//			Initialization.driver.findElement(By.id("go_inbox")).click();
			WebDriverWait wait = new WebDriverWait(Driver.driver, 100);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[3][contains(text(),'"+value+"')]")));			
			String msg= message.get(i).getText();

			if(msg.contains(devicename) && msg.contains(Fence))
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			String PassStatement="PASS >> "+msg+" << Message is Recieved in the registered email";
			String FailStatement="FAIL >> "+msg+" << Message is not Recieved in the registered email";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			sleep(3);
		}
		Initialization.driver.close();
		Initialization.driver.switchTo().window(Mainwindow);
	}

	@FindBy(xpath="//*[@id='TimeFenceEditBtn']")
	private WebElement TimeFenceWindwsEdit;
	public void VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows() throws InterruptedException {
		TimeFenceWindwsEdit.click();
		waitForXpathPresent("//*[@id='optiontimetimefence']/span[text()='Optional Time']");
		sleep(2);
	}

	@FindBy(xpath="(//div[text()='"+Config.ColumnName+"'])[1]")
	private WebElement FOTA;

	@FindBy(xpath="//p[contains(text(),'Success')]")
	private WebElement Success;

	public void VerifyColOfEFOTA() {

		if(Success.equals(Success))
		{ 
			String PassStatement="PASS >> EFOTA Success Status";
			System.out.println(PassStatement);
		}else
		{	
			String FailStatement="FAIL >>  EFOTA Success Status not displayed";
			ALib.AssertFailMethod(FailStatement);
		}
	}

	@FindBy(xpath="//*[@id='networkinfo-card']/span[@class='moreLess']") 
	private WebElement MoreMacAddress;
	@FindBy(xpath="//*[@id='networkinfo-card']/div[5]/p")
	private WebElement ScrollLocalAreaConnection;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[1]/div[@class='mac_address']")
	private WebElement showMore_macAddress1;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[1]/div[2]/p")
	private WebElement showMore_macAddressLocal;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[2]/div[1]")
	private WebElement showMore_macAddress2;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[2]/div[2]/p/b[contains(text(),'Wi-Fi')]")
	private WebElement showMore_macAddressWifi;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[3]/div[1]")
	private WebElement showMore_macAddress3;
	@FindBy(xpath="//*[@id='showMore_macAddress']/div[3]/div[2]/p/b[contains(text(),'Bluetooth Network Connection')]")
	private WebElement showMore_macAddress_BluetoothNetworkConnection;

	public void ClickOnMoreMacAddress() throws InterruptedException {
		MoreMacAddress.click();
		sleep(1);

	}


/*	@FindBy(xpath="//*[@id='dis-card']/div[5]/p[contains(text(),'"+Config.Printer_OperatingSystem+"')]")
	private WebElement OperatingSystemPRINTER;
   @FindBy(xpath="//*[@id='dis-card']/div[9]/p[contains(text(),'"+Config.Printer_AgentVersion+"')]")
	private WebElement Things_AgentVersionPRINTER;

	@FindBy(xpath="//*[@id='CustomColumn2_name']/span[contains(text(),'"+Config.ColumnThings+"')]")
	private WebElement CustomColumn2_nameThingsPRINTER;

	@FindBy(xpath="//*[@id='CustomColumn3_name']/span[contains(text(),'"+Config.ColumnThings+"')]")
	private WebElement CustomColumn3_nameThingsPRINTER;
*/
	@FindBy(xpath="//*[@id='dis-card']/div[7]/p[contains(text(),'"+Config.DeviceNamePrinter+"')]")
	private WebElement DeviceNamePRINTER;

	@FindBy(xpath="//*[@id='dis-card']/div[11]/p[contains(text(),'')]")
	private WebElement Things_LastDeviceTimePRINTER;

	@FindBy(xpath="//*[@id='deviceNotes']")
	private WebElement DeviceNotesThingsPRINTER;
	@FindBy(xpath="//*[@id='dis-card']/div[3]/p[contains(text(),'"+Config.DeviceModelNameSacnnerAndPrinter+"')]")
	private WebElement DeviceModelNamePRINTER;

	@FindBy(xpath="//*[@id='CustomColumn2_name']/span[contains(text(),'"+Config.ColumnThings+"')]")
	private WebElement CustomColumn2_nameThingsPRINTER;


	public void DeviceInfo_ThingsScanner_PrinterDevice() throws InterruptedException {


		String ActualResult1=DeviceModelName.getText();
		String ExpectedResult1=Config.DeviceModelNameSacnnerAndPrinter;
		String Pass="PASS>> "+ExpectedResult1+" Device Model For Things is Displayed";
		String Fail="FAIL>> "+ExpectedResult1+" Device Model For Things is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass, Fail);
		sleep(2);

		String ActualResult2=OperatingSystem.getText();
		String ExpectedResult2=Config.Printer_OperatingSystem;
		String Pass2="PASS>> "+ExpectedResult2+" Operating System For Things is Displayed";
		String Fail2="FAIL>> "+ExpectedResult2+" Operating System For Things is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass2, Fail2);
		sleep(2);

		String ActualResult3=DeviceNameLinux.getText();
		String ExpectedResult3=Config.DeviceNamePrinter;
		String Pass3="PASS>> "+ExpectedResult3+" is Device Name for PRINTER is Displayed";
		String Fail3="FAIL>> "+ExpectedResult3+"is Device Name for PRINTER is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3,ActualResult3,Pass3,Fail3);
		sleep(2);

		String ActualResult4=AgentVersion.getText();
		String ExpectedResult4=Config.Printer_AgentVersion;
		String Pass4="PASS>> "+ExpectedResult4+" is Agent Version for PRINTER is Displayed";
		String Fail4="FAIL>> "+ExpectedResult4+"is Agent Version for PRINTER is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4,ActualResult4,Pass4,Fail4);
		sleep(2);

//		String ActualResult5=LastDeviceTime.getText();
//		String ExpectedResult5=Config.Things_LastDeviceTime;
//		String Pass5="PASS>> "+ExpectedResult5+" is Things_LastDeviceTime is Displayed";
//		String Fail5="FAIL>> "+ExpectedResult5+"is Things_LastDeviceTime is not Displayed";
//		ALib.AssertEqualsMethod(ExpectedResult5,ActualResult5,Pass5,Fail5);
//		sleep(2);

		String ActualResult6=AddNotes.getText();
		String ExpectedResult6=Config.Things_DeviceNotes;
		String Pass6="PASS>> "+ExpectedResult6+" is Things_DeviceNotes is Displayed";
		String Fail6="FAIL>> "+ExpectedResult6+"is Things_DeviceNotes is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass6,Fail6);
		sleep(2);

/*		String ActualResult7=CustomColumn2_nameThingsPRINTER.getText();
		String ExpectedResult7=ActualResult7;
		String Pass7="PASS>> Column is "+ExpectedResult7+" for Things is Displayed";
		String Fail7="FAIL>> Column is "+ExpectedResult7+" for Things is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
*/
	}	


	@FindBy(xpath="(//span[text()='SSID']/following-sibling::input)[2]")
	private WebElement SSIDField2;

	public void EnterWifiSSID2(String value) throws InterruptedException {
		SSIDField2.sendKeys(value);
		Reporter.log("Entered Wifi SSID", true);
		sleep(2);
	}

	@FindBy(xpath="//input[@id='FenceJobInDeployDelay']")
	private WebElement NetworkFenceJobInDelay;

	public void EnterJobInDelay() throws InterruptedException {
		NetworkFenceJobInDelay.sendKeys("1");
		sleep(2);
	}
	@FindBy(xpath="//input[@id='FenceJobOutDeployDelay']")
	private WebElement NetworkFenceJobOutDelay;


	public void EnterJobOutDelay() throws InterruptedException {
		NetworkFenceJobOutDelay.sendKeys("1");
		sleep(2);
	}

	@FindBy(xpath="//button[@id='addNF_rowbtn']")
	private WebElement NetworkAddFence;

	public void ClickOnNetWorkAddFence() throws InterruptedException {
		NetworkAddFence.click();
		sleep(2);
	}

	@FindBy(xpath="//input[@id='enableNetworkFence']")
	private WebElement EnableNetworkFence;

	public void ClickOnEnableNetwork(boolean flag) throws InterruptedException {
		boolean selected = EnableNetworkFence.isSelected();
		if(flag==selected) {
			EnableNetworkFence.click();
		}
		Reporter.log("Clicked on Enable Network Fence", true);
		sleep(4);
	}

	@FindBy(xpath="//div[@id='network_fencing_job']")
	private WebElement NetworkFence;
	
	public void ClickOnNetworkFence() throws InterruptedException {
		NetworkFence.click();
		Reporter.log("Clicked On Network Fence", true);
		sleep(2);
	} 
	
	@FindBy(xpath="//div[@id='NetworkFenceAddJobOut']")
	private WebElement NetworkFenceAddJobOut;

	public void ClickOnNetworkFenceAddJobOutButton() throws InterruptedException {
		NetworkFenceAddJobOut.click();
		Reporter.log("Clicked on Network Fence Exited Add job Button", true);
		sleep(2);
	}

	@FindBy(xpath="//*[@id='dis-card']/div[7]/p[contains(text(),'"+Config.EditingChildDeviceName+"')]")
	private WebElement DeviceNameIOT;

	public void verifyWithChildIOTdeviceName(String DeviceName) {

		String ChildDeviceNameIOT = DeviceNameIOT.getText();
		if(ChildDeviceNameIOT.equals(DeviceName)) 
		{
			Reporter.log("Device Name IOT changed Sucessfully", true);
		}
		else
		{
			ALib.AssertFailMethod("Device Name IOT NOT changed Sucessfully");
		}
	}

	@FindBy(xpath="//td[@class='JobsFailed']/div/div/i[7]")
	private WebElement ColorIotDevicesPrinter;
	public void VerfyDeviceIconInDeviceColumnForIoTdevices() {
		boolean flag = false;
		String ColourOFIotDevice=ColorIotDevicesPrinter.getAttribute("class");
		if(ColourOFIotDevice.equals("fa fa-mobile dev_icn type_iot")) {
			Reporter.log("ColourOFIotDevice is not equal to Colour of Normal Device", true);
		}
		else {
			ALib.AssertFailMethod("ColourOFIotDevice is equal to Colour of Normal Device");
		}
	}

	@FindBy(xpath="//*[@id='windowsstoragemem']")
	private WebElement windowsstoragememEMM;
	
	@FindBy(xpath="//*[@id='windowsprogrammem']")
	private WebElement windowsprogrammemEMM;
	
	public void VerifyEMMmemory() throws InterruptedException {
		
		String expected2 = windowsstoragememEMM.getText();
		String actual2 = expected2;
		String pass2 = "PASS >> EMM windows storage memory EMM is "+expected2+" ";
		String fail2 = "FAIL >>EMM windows storage memory EMM is not displayed";
		ALib.AssertEqualsMethod(expected2, actual2, pass2, fail2);
		sleep(2);
		
		String expected3= windowsprogrammemEMM.getText();
		String actual3 = expected3;
		String pass3= "PASS >> EMM windows storage program EMM is "+expected3+" ";
		String fail3= "FAIL >>EMM windows storage program EMM is not displayed";
		ALib.AssertEqualsMethod(expected3, actual3, pass3, fail3);
		sleep(2);
	}
	
	@FindBy(xpath="//*[@id='networkinfo-card']/div[3]/p")
	private WebElement MACaddressNixWindows;
	
	@FindBy(xpath="//*[@id='networkinfo-card']/div[5]/p/b[text()='Ethernet']")
	private WebElement EthernetNixWin;
	
	public void VerifyMACadressNIxWin(String NixWinMac) throws InterruptedException {//
		
		String expected4= MACaddressAndroid.getText();
		String actual4 = NixWinMac;
		String pass4= "PASS >> NIX windows MAC Address is "+expected4+" ";
		String fail4= "FAIL >>NIX windows MAC Address is not displayed";
		ALib.AssertEqualsMethod(expected4, actual4, pass4, fail4);
		sleep(2);
		
		String expected5= MacEMM_Ethernet.getText();
		String actual5 = expected5;
		String pass5= "PASS >> NIX windows Ethernet is "+expected5+" ";
		String fail5= "FAIL >>NIX windows Ethernet is not displayed";
		ALib.AssertEqualsMethod(expected5, actual5, pass5, fail5);
		sleep(2);
	}
	
	@FindBy(xpath="//*[@id='storagemem']/p[text()=' free of ']")
	private WebElement storagememAndroid;
	
	@FindBy(xpath="//*[@id='programmem']/p[text()=' free of ']")
	private WebElement programmemAndroid;
	
	public void MemManagementAndroid() throws InterruptedException {
		
		String expected= storagememAndroid.getText();
		String actual = expected;
		String pass= "PASS >>Android Storage Memory is "+expected+" ";
		String fail= "FAIL >>Android Storage Memory is not displayed";
		ALib.AssertEqualsMethod(expected, actual, pass, fail);
		sleep(2);
		
		String expected1= programmemAndroid.getText();
		String actual1 = expected1;
		String pass1= "PASS >>Android programmem Storage is "+expected1+" ";
		String fail1= "FAIL >>Android programmem Storage is not displayed";
		ALib.AssertEqualsMethod(expected1,actual1,pass1,fail1);
		sleep(2);
	}
	
	@FindBy(xpath="//*[@id='deleteDeviceBtn']")
	private WebElement deleteDeviceBtn;
	
	@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement deviceConfirmationDialogYesBtn;
	@FindBy(xpath="//p[contains(text(),'has failed. The device(s) has a \"Things\" device associated with it. Delete the associated \"Things\" device first.')]")
	private WebElement ErrorinformationDialog;
	@FindBy(xpath="//*[@id='informationDialog']/div/div/div[2]/button")
	private WebElement OkBtnIotDeviceDelete;
	public void VerifyDeleteParentDeviceErrorMessage() throws InterruptedException {
		
		deleteDeviceBtn.click();
		waitForXpathPresent("//*[@id='deviceConfirmationDialog']/div/div/div[1]");
		sleep(2);
		
		deviceConfirmationDialogYesBtn.click();
		waitForXpathPresent("//p[contains(text(),'has failed. The device(s) has a \"Things\" device associated with it. Delete the associated \"Things\" device first.')]");
		String ErrorMessageDisplayed=ErrorinformationDialog.getText();
		String ExpectedErrorMessage=ErrorMessageDisplayed;
		String Pass8="PASS>> Error Option For Deleting Parent Device is displayed";
		String Fail8="FAIL>> Error Option For Deleting Parent Device is NOT displayed";
		ALib.AssertEqualsMethod(ExpectedErrorMessage, ErrorMessageDisplayed, Pass8, Fail8);
		sleep(2);

		OkBtnIotDeviceDelete.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[2]/div/div/div[text()='Health']/following-sibling::div[1]")
	private WebElement Health;
	
	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[2]/div/div/div[text()='Connector name']/following-sibling::div[1]")
	private WebElement ConnectorName_ThingsInfo;
	
	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[2]/div/div/div[text()='Battery Remaining (mAh)']/following-sibling::div[1]")
	private WebElement BatteryRemaining_ThingsInfo;
	
	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[2]/div/div/div[text()='Time Remaining']/following-sibling::div[1]")
	private WebElement TimeRemaining_ThingsInfo;

	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[2]/div/div/div[text()='Battery Capacity (mAh)']/following-sibling::div[1]")
	private WebElement BatteryCapacity_ThingsInfo;

	
	public void VerifyThingsInfo_DataLogicDevice() throws InterruptedException {
		
		String ActualResult1=Health.getText();
		String ExpectedResult1=Config.HealthDataLogicSmartBattery_ThingsInfo ;
		String Pass="PASS>>Health of DataLogic Smart Battery Device is Displayed "+ExpectedResult1;
		String Fail="FAIL>>Health of DataLogic Smart Battery Device is not Displayed "+ExpectedResult1;
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass, Fail);
		sleep(2);
		
		String ActualResult2=BatteryRemaining_ThingsInfo.getText();
		String ExpectedResult2=Config.BatteryRemainingDataLogicSmartBattery_ThingsInfo;
		String Pass1="PASS>> "+ExpectedResult2+" Battery Remaining ThingsInfo of DataLogic Smart Battery Device is Displayed";
		String Fail1="FAIL>> "+ExpectedResult2+" Battery Remaining ThingsInfo of DataLogic Smart Battery Device is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass1, Fail1);
		sleep(2);
		
		String ActualResult3=TimeRemaining_ThingsInfo.getText();
		String ExpectedResult3=Config.TimeRemainingDataLogicSmartBattery_ThingsInfo;
		String Pass2="PASS>> "+ExpectedResult3+" Time Remaining ThingsInfo of DataLogic Smart Battery Device is Displayed";
		String Fail2="FAIL>> "+ExpectedResult3+" Time Remaining ThingsInfo of DataLogic Smart Battery Device is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail2);
		sleep(2);
		
		String ActualResult4=BatteryCapacity_ThingsInfo.getText();
		String ExpectedResult4=Config.BatteryCapacityDataLogicSmartBattery_ThingsInfo ;
		String Pass3="PASS>> "+ExpectedResult4+" Battery Capacity ThingsInfo of DataLogic Smart Battery Device is Displayed";
		String Fail3="FAIL>> "+ExpectedResult4+" Battery Capacity ThingsInfo of DataLogic Smart Battery Device is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
		sleep(2);
		
	}

	public void VerifySimSerialNumber(String DeviceAndroid) throws InterruptedException {
		String ActualResult2=SimSerialNumberAndroid.getText();
		String ExpectedResult2=DeviceAndroid  ;
		String Pass1="PASS>> "+ExpectedResult2+" Sim Serial For Android is Displayed";
		String Fail1="FAIL>> "+ExpectedResult2+" Sim Serial For Android is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass1, Fail1);
		sleep(2);

}
	
/*	@FindBy(xpath="//*[@id='dis-card']/div[3]/p[contains(text(),'"+Config.VRdeviceModel+"')]")
	private WebElement DeviceModelNameVR;
	
	@FindBy(xpath="//*[@id='dis-card']/div[5]/p[contains(text(),'"+Config.VRoperatingSystem+"')]")
	private WebElement OperatingSystemVR;
*/	
	@FindBy(xpath="//*[@id='dis-card']/div[7]/span[contains(text(),'"+Config.DeviceNameVR+"')]")
	private WebElement DeviceNameVR;
	
	@FindBy(xpath="//div[@id='dis-card']/div[text()='Bluetooth Name']/following-sibling::div[1]")
	private WebElement VR_BluetoothName;
	
//	@FindBy(xpath="//*[@id='dis-card']/div[11]/p[contains(text(),'"+Config.VR_AgentVersion+"')]")
//	private WebElement VR_AgentVersion;
	
	@FindBy(xpath="//*[@id='dis-card']/div[13]/p[contains(text(),'')]")
	private WebElement VR_LastDeviceTime;
	
	@FindBy(xpath="//*[@id='deviceNotes']")
	private WebElement DeviceNotesVR;
	
	@FindBy(xpath="//*[@id='dis-card']/div[17]/span[text()]")
	private WebElement TimeFenceVR;
	
	@FindBy(xpath="//*[@id='CustomColumn1_name']/span[contains(text(),'"+Config.ColumnVR+"')]")
	private WebElement CustomColumn1_nameVR;
	
	public void VerifyDeviceInfoVR()throws InterruptedException {
		
		String ActualResult1=DeviceModelName.getText();
		String ExpectedResult1=Config.VRdeviceModel;
		String Pass="PASS>> "+ExpectedResult1+" Device Model For VR is Displayed";
		String Fail="FAIL>> "+ExpectedResult1+" Device Model For VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass, Fail);
		sleep(2);
		
		String ActualResult2=OperatingSystem.getText();
		String ExpectedResult2=Config.VRoperatingSystem;
		String Pass2="PASS>> "+ExpectedResult2+" Operating System For VR is Displayed";
		String Fail2="FAIL>> "+ExpectedResult2+" Operating System For VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass2, Fail2);
		sleep(2);
		
		String ActualResult3=DeviceNameLinux.getText();
		String ExpectedResult3=Config.DeviceNameVR;
		String Pass3="PASS>> "+ExpectedResult3+" is Device Name for VR is Displayed";
		String Fail3="FAIL>> "+ExpectedResult3+"is Device Name for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3,ActualResult3,Pass3,Fail3);
		sleep(2);
		
		String ActualResult4=VR_BluetoothName.getText();
		String ExpectedResult4=Config.VR_BluetoothName;
		String Pass4="PASS>> "+ExpectedResult4+" is Bluetooth Name for VR is Displayed";
		String Fail4="FAIL>> "+ExpectedResult4+"is Bluetooth Name for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4,ActualResult4,Pass4,Fail4);
		sleep(2);
		
		String ActualResult5=AgentVersion.getText();
		String ExpectedResult5=Config.VR_AgentVersion;
		String Pass5="PASS>> "+ExpectedResult5+" is Agent Version for VR is Displayed";
		String Fail5="FAIL>> "+ExpectedResult5+"is Agent Version for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5,ActualResult5,Pass5,Fail5);
		sleep(2);
		
//		String ActualResult6=LastDeviceTime.getText();
//		String ExpectedResult6=Config.VR_LastDeviceTime;
//		String Pass6="PASS>> "+ExpectedResult6+" is VR_LastDeviceTime for VR is Displayed";
//		String Fail6="FAIL>> "+ExpectedResult6+"is VR_LastDeviceTime for VR is not Displayed";
//		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass6,Fail6);
//		sleep(2);
		
		String ActualResult7=AddNotes.getText();
		String ExpectedResult7=Config.VR_DeviceNotes;
		String Pass7="PASS>> "+ExpectedResult7+" is VR_DeviceNotes for VR is Displayed";
		String Fail7="FAIL>> "+ExpectedResult7+"is VR_DeviceNotes for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
		
		String ActualResult8=TimeFenceWindows.getText();
		String ExpectedResult8=Config.VR_TimeFence;
		String Pass8="PASS>> Time Fence is "+ExpectedResult8+" for VR is Displayed";
		String Fail8="FAIL>>  Time Fence is "+ExpectedResult8+" for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult8,ActualResult8,Pass8,Fail8);
		sleep(2);
		
		String ActualResult9=CustomColumn1_nameVR.getText();
		String ExpectedResult9=Config.ColumnVR;
		String Pass9="PASS>> Column is "+ExpectedResult9+" for VR is Displayed";
		String Fail9="FAIL>> Column is "+ExpectedResult9+" for VR is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult9,ActualResult9,Pass9,Fail9);
		sleep(2);
	}
	
/*	  @FindBy(xpath="//*[@id='dis-card']/div[3]/p[contains(text(),'"+Config.WindowsMobileDeviceModelName+"')]")
	  private WebElement DeviceModelNameWindowsMobileDevice;
	 
	   @FindBy(xpath="//*[@id='dis-card']/div[5]/p[contains(text(),'"+Config.WindowsMobileOS+"')]")
		private WebElement MicrosoftWindowsMobileOS; 
		
		
		@FindBy(xpath="//*[@id='dis-card']/div[9]/p[contains(text(),'"+Config.AgentVersionWindowsMobile+"')]")
		private WebElement AgentVersionWindowsMobile;


		@FindBy(xpath="//*[@id='dis-card']/div[13]/div/span[contains(text(),'"+Config.AddNotesofWindowsMobile+"')]")
		private WebElement AddNotesofWindowsMobile;
	 
		@FindBy(xpath="//*[@id='dis-card']/div[15]/span[contains(text(),'"+Config.LocTrackingWindowsMobile+"')]") //OFF
		private WebElement LocTrackingWindowsMobile;
*/		
		 @FindBy(xpath="//*[@id='CustomColumn1_name']/span[contains(text(),'')]")
		 private WebElement CustomColumn1_nameWindowsMobile;
	 
		@FindBy(xpath="//*[@id='dis-card']/div[7]/span[contains(text(),'"+Config.WindowsMobileDeviceName+"')]")
		private WebElement DeviceNameWindowsMobile;
			
		@FindBy(xpath="//*[@id='dis-card']/div[11]/p[contains(text(),'')]")
		private WebElement LastDeviceTimeOfWindowsMobile;

	 
        public void VerifyDeviceInfoForWindowsMobile() throws InterruptedException {
			
			String ActualResult1=DeviceModelName.getText();
			String ExpectedResult1=Config.WindowsMobileDeviceModelName ;
			String Pass="PASS>>Device Model For Windows Mobile Device is Displayed "+ExpectedResult1;
			String Fail="FAIL>>Device Model For Windows Mobile Device is not Displayed "+ExpectedResult1;
			ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass, Fail);
			sleep(2);
		
			String ActualResult2=OperatingSystem.getText();
			String ExpectedResult2=Config.WindowsMobileOS;
			String Pass1="PASS>> "+ExpectedResult2+" Operating System For Windows Mobile is Displayed";
			String Fail1="FAIL>> "+ExpectedResult2+" Operating System For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass1, Fail1);
			sleep(2);
			
			String ActualResult3=DeviceNameLinux.getText();  //  //DeviceNameWindowsMobile
			String ExpectedResult3=Config.WindowsMobileDeviceName ;
			String Pass2="PASS>> "+ExpectedResult3+" Device Name For Windows Mobile is Displayed";
			String Fail2="FAIL>> "+ExpectedResult3+"  Device Name For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail2);
			sleep(2);
			
			String ActualResult4=AgentVersionWindows.getText();
			String ExpectedResult4=Config.AgentVersionWindowsMobile ;
			String Pass3="PASS>> "+ExpectedResult4+" AgentVersion For Windows Mobile is Displayed";
			String Fail3="FAIL>> "+ExpectedResult4+" AgentVersion For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
			sleep(2);
			
			String ActualResult5=LastDeviceTime.getText();
			String ExpectedResult5=Config.LastDeviceTimeOfWindowsMobile ;
			String Pass4="PASS>> "+ExpectedResult5+" LastDeviceTime For Windows Mobile is Displayed";
			String Fail4="FAIL>> "+ExpectedResult5+" LastDeviceTime For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass4, Fail4);
			sleep(2);
			
			String ActualResult6=AddNotes.getText();
			String ExpectedResult6=Config.AddNotesofWindowsMobile ;
			String Pass5="PASS>> Notes "+ExpectedResult6+" For Windows Mobile is Displayed";
			String Fail5="FAIL>> Notes "+ExpectedResult6+" For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass5,Fail5);
			sleep(2);
 	
			String ActualResult7=LocTrackingWindows.getText();
			String ExpectedResult7=Config.LocTrackingWindowsMobile ;
			String Pass6="PASS>> Loctaion Tracking is "+ExpectedResult7+" For Windows Mobile is Displayed";
			String Fail6="FAIL>> Loctaion Tracking is "+ExpectedResult7+" For Windows Mobile is not Displayed";
			ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass6,Fail6);
			sleep(2);
			
			String ActualResult8=CustomColumn1_nameWindowsMobile.getText();
			
			if(ActualResult8.length()>0)
			{
		    String ExpectedResult8= Config.CustomColumnWindowsMobile;
	        String Pass7="PASS>> CustomColumn1_name For Windows Mobile is Displayed as " +ExpectedResult8;
			String Fail7="FAIL>> CustomColumn1_name For Windows Mobile is not Displayed as " +ExpectedResult8;
			ALib.AssertEqualsMethod(ExpectedResult8,ActualResult8,Pass7,Fail7);
			sleep(2);
			
			}	
			
        }

    	public void MemManagementWindowsMobile() throws InterruptedException {
			String expected= storagememAndroid.getText();
			String actual = expected;
			String pass= "PASS >>Windows Mobile Storage Memory is "+expected+" ";
			String fail= "FAIL >>Windows Mobile Storage Memory is not displayed";
			ALib.AssertEqualsMethod(expected, actual, pass, fail);
			sleep(2);
			
			String expected1= programmemAndroid.getText();
			String actual1 = expected1;
			String pass1= "PASS >>Windows Mobile programmem Storage is "+expected1+" ";
			String fail1= "FAIL >>Windows Mobile programmem Storage is not displayed";
			ALib.AssertEqualsMethod(expected1,actual1,pass1,fail1);
			sleep(2);
		}
    	
    	
    	@FindBy(xpath="//*[@id='things_device_info_wrapper']/div/div[2]/a[text()='Apply']")
		private WebElement ApplyDataLogicSmartBattery;
		
		@FindBy(xpath="//*[@id='things_device_info_wrapper']/div/div[2]/a[text()='Save']")
		private WebElement SaveDataLogicSmartBattery;
    	public void DATAlogicDevices() {
    		boolean ApplyOptionForDataLogicSmart=ApplyDataLogicSmartBattery.isDisplayed();
    		String Pass7="PASS>> Apply Option For DataLogicSmart Battery is displayed";
    		String Fail7="FAIL>> Apply Option For DataLogicSmart Battery is NOT displayed";
    		ALib.AssertTrueMethod(ApplyOptionForDataLogicSmart, Pass7, Fail7);
    		
    		boolean SaveOptionForDataLogicSmart=SaveDataLogicSmartBattery.isDisplayed();
    		String Pass8="PASS>> Save Option For DataLogicSmart Battery is displayed";
    		String Fail8="FAIL>> Save Option For DataLogicSmart Battery is NOT displayed";
    		ALib.AssertTrueMethod(SaveOptionForDataLogicSmart,Pass8,Fail8);
    		
    		}
    	
	@FindBy(xpath="//p[contains(text(),'Sim Serial Number (ICCID)')]/../following-sibling::div")
	private WebElement SimSerialNumberAndroid;
	
	@FindBy(xpath="//p[text()='Sim Serial Number (ICCID)']")
	private WebElement SerialNumber;
	public void VerifySimSerialNumberForNonInsertedSimAndroidDevice() 
	{
	boolean flag = false;
	try 
	{
		if(SimSerialNumberAndroid.isDisplayed())
		{
			flag=true;
		}
		
	}
	catch(Exception e) 
	{
		flag=false;
	}
	String PassStatement="PASS >> Sim Serial Number Non Sim Android Device is not displayed";
	String FailStatement="FAIL >> Sim Serial Number Non Sim Android Device is displayed";
	ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
}

	@FindBy(xpath="//input[@id='SendMDMAlertWarningNetworkJobOut']")
	private WebElement SendMDMAlertWarningNetworkJobOut;
	public void CheckNetworkFenceExitedMDMAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendMDMAlertWarningNetworkJobOut.isSelected();
		if(flag==value) {
			SendMDMAlertWarningNetworkJobOut.click();
		}
		Reporter.log("Clicked on Fence exited MDM Alert", true);
		sleep(1);
	}

	@FindBy(xpath="//input[@id='SendDeviceAlretWarningNetworkJobOut']")
	private WebElement SendDeviceAlretWarningNetworkJobOut;
	public void CheckNetworkFenceExitedDeviceAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendDeviceAlretWarningNetworkJobOut.isSelected();
		if(flag==value) {
			SendDeviceAlretWarningNetworkJobOut.click();

		}
		Reporter.log("Clicked on Fence exited Device Alert", true);
		sleep(1);
	}
	public void ClickOnNetworkFenceExiedTab() throws InterruptedException {
		NetworkFenceExited.click();
		Reporter.log("Clicked on Fence Exied tab", true);
		sleep(2);
	}

	@FindBy(xpath="//span[text()='SSID']/following-sibling::input")
	private WebElement SSIDField;
	public void EnterWifiSSID(String value) throws InterruptedException {

		SSIDField.clear();
		sleep(2);
		SSIDField.sendKeys(value);
		Reporter.log("Entered Wifi SSID", true);
		sleep(4);
	}
	@FindBy(xpath="//*[@id='isDeviceTimeflag']/option[text()=' Device Time']")
	private WebElement DeviceTime_OptionalTime;

	public void SelectOptionalTime() throws InterruptedException {

		DeviceTime_OptionalTime.click();
		sleep(4);

	}

	@FindBy(xpath="//*[@id='dis-card']/div[25]/a[text()='Edit']")
	private WebElement EditTimeFence;

	@FindBy(xpath="//*[@id='timefencing_modal']/div/div/div[2]/div[1]/span/input")
	private WebElement timefencing_modalEnableCheckBox;

	public void VerifyCreatingTimeFenceJob_selectingDeviceTime() throws InterruptedException {
		EditTimeFence.click();
		waitForXpathPresent("//*[@id='optiontimetimefence']/span[text()='Optional Time']");
		sleep(2);
	}
	public void VerifyDUALenrollmentMACaddress(String AndroidMACaddress,String LocalAreaConnection,String showMoremacAddress1,String showMoremacAddressLocal,String showMoremacAddress2,String showMoremacAddressWifi,String showMoremacAddress3,String showMoremacAddress_BluetoothNetworkConnection) throws InterruptedException { //String AndroidMACaddress,String LocalAreaConnection,String showMoremacAddress1,String showMoremacAddressLocal,String showMoremacAddress2,String showMoremacAddressWifi,String showMoremacAddress3,String showMoremacAddress_BluetoothNetworkConnection
		
		String ActualResult1=MACaddressAndroid.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=AndroidMACaddress ;
		String Pass1="PASS>> MAC Address for DualEnrollment "+ExpectedResult1+" is Displayed";
		String Fail1="FAIL>> MAC Address for DualEnrollment "+ExpectedResult1+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass1, Fail1);
		sleep(2);

		//  js.executeScript("arguments[0].scrollIntoView()", ScrollLocalAreaConnection);
		//	sleep(2);
		String ActualResult2=ScrollLocalAreaConnection.getText();
		Reporter.log(ActualResult2,true);
		String ExpectedResult2=LocalAreaConnection ;
		String Pass="PASS>> MAC Address for DualEnrollment "+ExpectedResult2+" is Displayed";
		String Fail="FAIL>> MAC Address for DualEnrollment "+ExpectedResult2+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass, Fail);
		sleep(2);

		String ActualResult3=showMore_macAddress1.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=showMoremacAddress1 ;
		String Pass2="PASS>> MAC Address for DualEnrollment "+ExpectedResult3+" is Displayed";
		String Fail2="FAIL>> MAC Address for DualEnrollment "+ExpectedResult3+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail);
		sleep(2);

		String ActualResult4=showMore_macAddressLocal.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=showMoremacAddressLocal ;
		String Pass3="PASS>> MAC Address for DualEnrollment "+ExpectedResult4+" is Displayed";
		String Fail3="FAIL>> MAC Address for DualEnrollment "+ExpectedResult4+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
		sleep(2);

		String ActualResult5=showMore_macAddress2.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=showMoremacAddress2 ;
		String Pass4="PASS>> MAC Address for DualEnrollment "+ExpectedResult5+" is Displayed";
		String Fail4="FAIL>> MAC Address for DualEnrollment "+ExpectedResult5+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass4, Fail4);
		sleep(2);
		String ActualResult6=showMore_macAddressWifi.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6=showMoremacAddressWifi ;
		String Pass5="PASS>> MAC Address for DualEnrollment "+ExpectedResult6+" is Displayed";
		String Fail5="FAIL>> MAC Address for DualEnrollment "+ExpectedResult6+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6, ActualResult6, Pass5, Fail5);
		sleep(2);
		js.executeScript("arguments[0].scrollIntoView()", showMore_macAddress_BluetoothNetworkConnection);
		String ActualResult7=showMore_macAddress3.getText();
		Reporter.log(ActualResult7,true);
		String ExpectedResult7=showMoremacAddress3 ;
		String Pass6="PASS>> MAC Address for DualEnrollment "+ExpectedResult7+" is Displayed";
		String Fail6="FAIL>> MAC Address for DualEnrollment "+ExpectedResult7+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7, ActualResult7, Pass6, Fail6);
		sleep(2);

		String ActualResult8=showMore_macAddress_BluetoothNetworkConnection.getText();
		Reporter.log(ActualResult8,true);
		String ExpectedResult8=showMoremacAddress_BluetoothNetworkConnection ;
		String Pass7="PASS>> MAC Address for DualEnrollment "+ExpectedResult8+" is Displayed";
		String Fail7="FAIL>> MAC Address for DualEnrollment "+ExpectedResult8+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult8, ActualResult8, Pass7, Fail7);
		sleep(2);
	}	


	public void ScrollToColumn(String ColumnName) throws InterruptedException
	{

		WebElement scroll = Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])"));

		js.executeScript("arguments[0].scrollIntoView()", scroll);
		sleep(4);
	}

	public void VerifyFenceExitAlertForDevice(String devicename, String Fence, int waitTime) throws InterruptedException 
	{
		sleep(4);

		try 
		{

			WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Time fence exit alert for device "+Config.DeviceName+"']"))).isDisplayed();
			sleep(5);
			String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text, '"+Fence+" exit alert for device "+devicename+"')]").getText();
			System.out.println("Alert Message for Time Fence Exit: " +alertMessage);
			String ExpectedAlertMessage = "Time fence exit alert for device "+Config.DeviceName+"";
			String PassStatement=""+alertMessage+": Alert messeage is displayed.";
			String FailStatement=""+alertMessage+": Alert messeage is not displayed.";
			ALib.AssertEqualsMethod(ExpectedAlertMessage, alertMessage, PassStatement, FailStatement);

		}
		catch(Exception e) 
		{
			System.out.println("error");
		}

	}



	public void SleepFor40Seconds() throws InterruptedException {
		sleep(30);
		System.out.println("Waiting");
	}

	public void ModifyTimeFenceAndJobName(String modifyjob, String ModifiedJob) throws InterruptedException {
		JobNameFence.sendKeys(modifyjob);
		Reporter.log("Modified Fence job name", true);
		sleep(1);
		JobNameFenceSaveButton.click();
		Reporter.log("Clicked on Fence jobname save button", true);
		waitForXpathPresent("//span[text()='Job modified successfully.']");
		waitForXpathPresent("//p[text()='"+ModifiedJob+"']");
		sleep(6);
	}

	public void CheckFenceEnteredMDMAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendMDMAlertWarningTimeJobIn.isSelected();
		System.out.println(flag);
		if(flag==value) {
			SendMDMAlertWarningTimeJobIn.click();

		}
		Reporter.log("Clicked on Time Fence MDM Alert", true);
		sleep(1);
	}
	public void ClickOnBackButton() throws IOException, InterruptedException 
	{
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		System.out.println("Clicked on Back button");
		sleep(4);
	}
	@FindBy(xpath="//div[@id='saveTimeJobModal']/div[1]/div[1]/div[3]/button")
	private WebElement JobNameFenceSaveButton;
	public void EnterJobnameFenceOkButton(String job) throws InterruptedException {
		JobNameFence.sendKeys(job);
		Reporter.log("Entered Fence job name", true);
		sleep(1);
		JobNameFenceSaveButton.click();
		Reporter.log("Clicked on Fence jobname save button", true);
		waitForXpathPresent("//span[text()='Job created successfully.']");
		waitForXpathPresent("//p[text()='"+job+"']");
		sleep(6);
	}
	@FindBy(xpath="//div[@id='notification_modal']/div[3]/button")
	private WebElement NotificationPolicyOkbutton;

	@FindBy(xpath="//input[@id='job_notify_name_input']")
	private WebElement Notify_policy_name;

	public void EnterAndSaveNotificationPolicyJob(String NotificationName) throws InterruptedException 
	{
		Notify_policy_name.sendKeys(NotificationName);
		sleep(1);
		NotificationPolicyOkbutton.click();
		waitForXpathPresent("//span[text()='Job created successfully.']");
		waitForXpathPresent("//p[text()='"+NotificationName+"']");
		sleep(6);
	}


	@FindBy(xpath="//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]")
	private WebElement FenceDeleteConfirmationButton;
	public void ClickOnFenceDeleteConfirmationYesButton() throws InterruptedException 
	{
		FenceDeleteConfirmationButton.click();
		Reporter.log("Clicked on Yes button", true);
		waitForidPresent("addTF_rowbtn");
		sleep(2);
	}
	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	@FindBy(xpath="//article[@id='timeFence_mainContainer']//span[contains(text(),'Start Time')]/following-sibling::input")
	public WebElement TimeFenceStartTime1;

	@FindBy(xpath="//article[@id='timeFence_mainContainer']//span[contains(text(),'End Time')]/following-sibling::input")
	public WebElement TimeFenceEndTime1;
	public void VerifyDeletedFence() {
		boolean flag;
		try 
		{
			if(TimeFenceStartTime1.isDisplayed() || TimeFenceEndTime1.isDisplayed())
			{
				flag=true;
			}
			else 	
			{
				flag=false;
			}
		}
		catch(Exception e) 
		{
			flag=false;
		}
		String PassStatement="PASS >> Fence is Deleted successfully";
		String FailStatement="FAIL >> Fence is not Deleted successfully";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}


	@FindBy(xpath="//div[@id='timefencing_modal']/div[1]/div[1]/div[1]/button")
	private WebElement ClickOnClosebuttonTimeFence;
	public void ClickOnCloseButtonOfTimeFence() throws InterruptedException 
	{
		ClickOnClosebuttonTimeFence.click();
		Reporter.log("Clicked on Close button of Time Fence", true);
		sleep(3);
	}

	@FindBy(xpath="//a[@id='TimeFenceEditBtn']//preceding-sibling::span")
	private WebElement TimeFenceStatus;
	public void VerifyTimeFenceStatus(String value) {
		if(TimeFenceStatus.getText().equals(value)) {
			Reporter.log("PASS >>> Time Fence status: "+value+"", true);
		}
		else {
			ALib.AssertFailMethod("TimeFence Status is not matched");
		}
	}

	@FindBy(xpath="//span[text()='Adding Notification Policy Job, Folder, Tags and other Fence Job is not allowed.']")
	private WebElement ErrorMsgNotificationNCompositeNFence;
	public void VerifyErrorMsgForNotificationNCompositeNFenceJobs() throws InterruptedException 
	{
		if(ErrorMsgNotificationNCompositeNFence.isDisplayed()) 
		{
			Reporter.log("PASS >> Adding Composite Job, Notification Policy Job, Folder and other Fence Job is not allowed. Message is displayed.", true);
		}
		else {
			ALib.AssertFailMethod("Adding Composite Job, Notification Policy Job, Folder and other Fence Job is not allowed. Message is not displayed.");
		}
		sleep(3);
	}

	@FindBy(xpath="//div[@class='timFen_row_bar']/a/i")
	private WebElement FenceDeleteButton;
	public void VerifySelectFenceUIValidation2()
	{
		if(FenceDeleteButton.isDisplayed() && AddFenceButton.isDisplayed())
		{
			Reporter.log("Fence Add and Fence Delete buttons are displaying in Select Fence Tab", true);
		}
		else 
		{
			ALib.AssertFailMethod("Fence Add and Fence Delete buttons are not displaying in Select Fence Tab");
		}
	}

	@FindBy(xpath="//div[@id='optiontimetimefence']/following-sibling::button")
	private WebElement RightTopCornerSaveButton;
	public void VerifySaveButtonInRightTopCorner()
	{
		if(RightTopCornerSaveButton.isEnabled())
		{
			Reporter.log("Save Button is displaying in Top Right Corner", true);
		}
		else 
		{
			ALib.AssertFailMethod("Save Button is not displaying in Top Right Corner");
		}
	}

	@FindBy(xpath="//input[@id='enableTimeFence']")
	private WebElement EnableTimefence;
	public void ClickOnEnableTimeFence(boolean flag) throws InterruptedException {
		boolean selected = EnableTimefence.isSelected();
		if(flag==selected) {
			EnableTimefence.click();
		}
		Reporter.log("Clicked on Enable Time Fence", true);
		sleep(4);
	}

	@FindBy(xpath="//select[@id='isDeviceTimeflag']")
	private WebElement DeviceTime; 

	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;
	public void SelectDeviceTime(String value) throws InterruptedException
	{
		Select select=new Select(DeviceTime);
		select.selectByVisibleText(value);
		Reporter.log("Selected "+value+"");
		sleep(4);

	}

	@FindBy(xpath="//button[@id='backToPanel1']")
	private WebElement SelectJobTypeBackButton;

	@FindBy(xpath="//button[@id='osPanel_cancel_btn']")
	private WebElement SelectOSCancelButton;
	public void ClickOnSelectOSCancelButton() throws InterruptedException 
	{
		SelectOSCancelButton.click();
		Reporter.log("Clicked on cancel button of Select OS", true);
		waitForidPresent("job_new_job");
		sleep(1);
	}


	public void ClickOnSelectJobTypeBackButton() throws InterruptedException 
	{
		SelectJobTypeBackButton.click();
		Reporter.log("Clicked on Back button of Select Job Type", true);
		sleep(1);
	}

	String AddedTime;

	@FindBy(id="job_modify")
	private WebElement ModifyJob; 
	public void ClickOnModifyJob_TimeFence() throws InterruptedException {
		ModifyJob.click();
		Reporter.log("Clicked on Modify button", true);
		waitForXpathPresent("//input[@id='enableTimeFence']");
		sleep(2);
	}
	public void InboxExitMessage() {
		
//		waitForVisibilityOf("//*[@id='mailGrid']/tbody/tr/td/span[contains(text(),'"+FenceMessage+"')]");
		InboxSearch.clear();
		InboxSearch.sendKeys("Time fence exit alert for device "+Config.DeviceName);
		

	}
	public void InboxEntryMessageNetworkFence() {
		
//		waitForVisibilityOf("//*[@id='mailGrid']/tbody/tr/td/span[contains(text(),'"+FenceMessage+"')]");
		InboxSearch.clear();
		InboxSearch.sendKeys("Network fence entry alert for device "+Config.DeviceName);
		

	}
	public void InboxExitMessageNetworkFence() {
		
//		waitForVisibilityOf("//*[@id='mailGrid']/tbody/tr/td/span[contains(text(),'"+FenceMessage+"')]");
		InboxSearch.clear();
		InboxSearch.sendKeys("Network fence exit alert for device "+Config.DeviceName);
		

	}

	public void InboxEnteryMessage() {
		
//		waitForVisibilityOf("//*[@id='mailGrid']/tbody/tr/td/span[contains(text(),'"+FenceMessage+"')]");
		InboxSearch.clear();
		InboxSearch.sendKeys("Time fence entry alert for device "+Config.DeviceName);
		

	}

	public void ClearInbox() {
		InboxSearch.clear();

	}

	public void ClickAndSetStartTime(WebElement TimeFencestartTime, int minute, String value) throws InterruptedException 
	{
		sleep(2);
		TimeFencestartTime.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);
		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[5]/div[2]/div[1]/div[1]/div[text()='"+AddedTime+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		sleep(6);
		ele.click();
		
		System.out.println("Start time is set as "+AddedTime+"");
	}

	public void ClickAndSetStartTimeForMultifence(WebElement TimeFencestartTime2, int minute, String value) throws InterruptedException 
	{
		sleep(2);
		TimeFencestartTime2.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);
		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[7]/div[2]/div[1]/div[1]/div[text()='"+AddedTime+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		sleep(6);
		ele.click();
		
		System.out.println("Start time is set as "+AddedTime+"");
	}
	public void ClickAndSetEndTime(WebElement TimeFenceEndTime, int minute, String value) throws InterruptedException {
		TimeFenceEndTime.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);
		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[6]/div[2]/div[1]/div[1]/div[text()='"+AddedTime+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		ele.click();
		System.out.println("End time is set as "+AddedTime+"");
	}
	public void ClickAndSetEndTimeForMultiFence(WebElement TimeFenceEndTime2, int minute, String value) throws InterruptedException {
		TimeFenceEndTime2.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);
		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[8]/div[2]/div[1]/div[1]/div[text()='"+AddedTime+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		ele.click();
		System.out.println("End time is set as "+AddedTime+"");
	}

	@FindBy(xpath="(//article[@id='timeFence_mainContainer']//span[contains(text(),'Start Time')]/following-sibling::input)[2]")
	public WebElement TimeFenceStartTime2;
	public void ClickAndSetStartTime2(int minute, String value) throws InterruptedException 
	{
		TimeFenceStartTime2.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);

		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[contains(text(),'"+AddedTime+"')])["+value+"]"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		ele.click();
		System.out.println("Start time is set as "+AddedTime+"");
	}

	public void ClickAndSetEndTime2(int minute, String value) throws InterruptedException {
		TimeFenceEndTime2.click();
		sleep(2);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		String dNow = calendar.getTime().toString();
		String AddedTime = dNow.substring(11, 16);
		System.out.println(AddedTime);

		WebElement ele = Initialization.driver.findElement(By.xpath("(//div[contains(text(),'"+AddedTime+"')])["+value+"]"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		ele.click();
		System.out.println("Start time is set as "+AddedTime+"");
	}
	@FindBy(xpath="//div[@id='TimeFenceAddJobIn']")
	private WebElement TimeFenceAddJobIn;
	public void ClickOnTimeFenceAddJobInButton() throws InterruptedException {
		try 
		{
			TimeFenceAddJobIn.click();
			Reporter.log("Clicked on Fence Entered Add job Button", true);
			sleep(6);
		}
		catch(Exception e) 
		{
			action.click(TimeFenceAddJobIn).build().perform();
			Reporter.log("Clicked on Fence Entered Add job Button", true);
			sleep(2);
		}
	}

	Actions action=new Actions(Initialization.driver);
	@FindBy(xpath="//div[@id='TimeFenceAddJobOut']")
	private WebElement TimeFenceAddJobOut;
	public void ClickOnTimeFenceAddJobOutButton() throws InterruptedException {
		try 
		{
			TimeFenceAddJobOut.click();
			Reporter.log("Clicked on Fence Exited Add job Button", true);
			sleep(2);
		}
		catch(Exception e) 
		{
			action.click(TimeFenceAddJobOut).build().perform();
			Reporter.log("Clicked on Fence Exited Add job Button", true);
			sleep(2);
		}
	}

	@FindBy(xpath="//div[@id='conmptableContainer']/div[1]/div[1]/div[1]/button[1]")
	private WebElement RefreshFenceJob;

	@FindBy(xpath="//div[@id='busyIndicator']//div[text()='Loading...']")
	private WebElement FenceLoading;


	public void SearchFenceJob(String value) throws InterruptedException {
		while(FenceLoading.isDisplayed()) 
		{
			sleep(10);	
		}
		TimeFenceJobSearch.clear();
		sleep(2);
		TimeFenceJobSearch.sendKeys(value);
		waitForXpathPresent("//table[@id='CompJobDataGrid']//p[text()='"+value+"']");
		Reporter.log("Entered job name", true);
		action.click(RefreshFenceJob).build().perform();
		sleep(7);

	}

	@FindBy(xpath="//div[@id='saveNetworkJobModal']/div[1]/div[1]/div[3]/button")
	private WebElement JobNameNetworkFenceSaveButton;

	public void EnterJobnameNetworkFenceOkButton(String job) throws InterruptedException {
		JobNameFence.sendKeys(job);
		Reporter.log("Entered Fence job name", true);
		sleep(2);
		JobNameNetworkFenceSaveButton.click();
		Reporter.log("Clicked on Fence jobname save button", true);
		waitForXpathPresent("//span[text()='Job created successfully.']");
		waitForXpathPresent("//p[text()='"+job+"']");
		sleep(6);
	}

	public void SelectFenceJob(String value) throws InterruptedException {
		while(FenceLoading.isDisplayed()) 
		{
			sleep(2);	
		}
		Initialization.driver.findElement(By.xpath("//table[@id='CompJobDataGrid']//p[text()='"+value+"']")).click();
		sleep(4);
	}

	@FindBy(xpath="//input[@id='SendDeviceAlretWarningNetworkJobIn']")
	private WebElement SendDeviceAlretWarningNetworkJobIn;

	public void CheckNetworkFenceEnteredDeviceAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendDeviceAlretWarningNetworkJobIn.isSelected();
		System.out.println(flag);
		if(flag==value) {
			SendDeviceAlretWarningNetworkJobIn.click();
		}

		Reporter.log("Clicked on Network Fence entered Device Alert", true);
		sleep(1);
	}

	@FindBy(xpath="//ul[@id='networkfencing_tabs']/li[2]")
	private WebElement NetworkFenceEntered;

	public void ClickOnNetworkFenceEnteredTab() throws InterruptedException {
		NetworkFenceEntered.click();
		Reporter.log("Clicked on Fence Entered tab", true);
		sleep(2);
	}

	@FindBy(xpath="//button[@id='networkFenc_save']")
	private WebElement NetworkFenceSaveButton;
	public void ClickedOnNetworkFenceJobSave() throws InterruptedException {
		NetworkFenceSaveButton.click();
		waitForXpathPresent("//input[@id='job_name_fence']");
		Reporter.log("Clicked on Network Fence save button", true);
		sleep(3);
	}

	@FindBy(xpath="//div[@id='NwtworkFenceAddJobIn']")
	private WebElement NetworkFenceAddJobIn;

	public void ClickOnNetworkFenceAddJobInButton() throws InterruptedException {
		NetworkFenceAddJobIn.click();
		Reporter.log("Clicked on Network Fence Entered Add job Button", true);
		sleep(2);
	}

	@FindBy(xpath="//input[@id='SendMDMAlertWarningNetworkJobIn']")
	private WebElement SendMDMAlertWarningNetworkJobIn;

	@FindBy(xpath="//input[@id='SendEmailWarningNetworkJobIn']")
	private WebElement SendEmailWarningNetworkJobIn;

	@FindBy(xpath="//div[@id='EmailAddressPopNetworkJobIn']//input[@id='EmailId']")
	private WebElement EnterEmailIdForNetworkFenceEntered;

	@FindBy(xpath="//div[@id='EmailAddressPopNetworkJobIn']/div[1]/div[1]/div[3]/button")
	private WebElement NetworkFenceEnteredEmailDoneButton;

	public void ClickAndEnterEmailForNetworkFenceEntered(boolean value, String Email) throws InterruptedException {
		boolean flag = SendEmailWarningNetworkJobIn.isSelected();
		if(flag==value) 
		{
			SendEmailWarningNetworkJobIn.click();
			Reporter.log("Clicked on Send Email Alert For Network FenceEntered", true);
			waitForXpathPresent("//div[@id='EmailAddressPopNetworkJobIn']//input[@id='EmailId']");
			sleep(2);
			EnterEmailIdForNetworkFenceEntered.clear();
			EnterEmailIdForNetworkFenceEntered.sendKeys(Email);
			Reporter.log("Entered EmailID for Network Fence Entered", true);
			sleep(2);
			NetworkFenceEnteredEmailDoneButton.click();
			Reporter.log("Clicked on Done button", true);
		}
		Reporter.log("Email Fence Entered is selected", true);
		sleep(1);
	}

	public void CheckNetworkFenceEnteredMDMAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendMDMAlertWarningNetworkJobIn.isSelected();
		System.out.println(flag);
		if(flag==value) {
			SendMDMAlertWarningNetworkJobIn.click();

		}
		Reporter.log("Clicked on Network Fence MDM Alert", true);
		sleep(1);
	}

	public void ClickOnTimeFenceAddJobOkButton() throws InterruptedException {
		TimeFenceAddJobOkButton.click();
		Reporter.log("Clicked on Add job Ok Button", true);
		sleep(4);
	}
	@FindBy(xpath="//div[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement TimeFenceJobSearch;

	@FindBy(xpath="//a[@id='NetworkFenceEditBtn']//preceding-sibling::span")
	private WebElement NetworkFenceStatus;


	public void VerifyNetworkFenceStatus(String value) {
		if(NetworkFenceStatus.getText().equals(value)) {
			Reporter.log("PASS >>> Network Fence status: "+value+"", true);
		}
		else {
			ALib.AssertFailMethod("Network Fence Status is not matched");
		}
	}


	@FindBy(xpath="//div[@id='selJob_addList_modal']/div[3]/button")
	private WebElement TimeFenceAddJobOkButton;


	@FindBy(xpath="//input[@id='SendDeviceAlretWarningTimeJobIn']")
	private WebElement SendDeviceAlretWarningTimeJobIn;



	@FindBy(xpath="//input[@id='SendMDMAlertWarningTimeJobIn']")
	private WebElement SendMDMAlertWarningTimeJobIn;



	@FindBy(xpath="//div[@id='EmailAddressPopTimeJobIn']/div[1]/div[1]/div[3]/button")
	private WebElement FenceEnteredEmailDoneButton;


	@FindBy(xpath="//input[@id='SendEmailWarningTimeJobIn']")
	private WebElement SendEmailWarningTimeJobIn;

	@FindBy(xpath="//div[@id='EmailAddressPopTimeJobIn']//input[@id='EmailId']")
	private WebElement EnterEmailIdForFenceEntered;

	public void ClickAndEnterEmailForFenceEntered(boolean value, String Email) throws InterruptedException {
		boolean flag = SendEmailWarningTimeJobIn.isSelected();
		if(flag==value) 
		{
			SendEmailWarningTimeJobIn.click();
			Reporter.log("Clicked on Send Email Alert For FenceEntered", true);
			waitForXpathPresent("//div[@id='EmailAddressPopTimeJobIn']//input[@id='EmailId']");
			sleep(2);
			EnterEmailIdForFenceEntered.clear();
			EnterEmailIdForFenceEntered.sendKeys(Email);
			Reporter.log("Entered EmailID for Fence Entered", true);
			sleep(2);
			FenceEnteredEmailDoneButton.click();
			Reporter.log("Clicked on Done button", true);
		}
		Reporter.log("Email Fence Entered is selected", true);
		sleep(1);
	}

	public void CheckFenceEnteredDeviceAlert(boolean value) throws InterruptedException
	{
		boolean flag =SendDeviceAlretWarningTimeJobIn.isSelected();
		System.out.println(flag);
		if(flag==value) {
			SendDeviceAlretWarningTimeJobIn.click();
		}

		Reporter.log("Clicked on Fence entered Device Alert", true);
		sleep(1);
	}

//	@FindBy(xpath="//*[@id='timeFence_mainContainer']/section/div[2]/div[1]/ul/li/div/div[3]/span[text()='End Time']")
	@FindBy(xpath="(//span[contains(text(),'End Time')])[1]")
    private WebElement TimeFenceEndTime;

//	@FindBy(xpath="//*[@id='timeFence_mainContainer']/section/div[2]/div[1]/ul/li/div/div[2]/input")
	@FindBy(xpath="(//span[contains(text(),'Start Time')])[1]")
	private WebElement TimeFenceStartTime;
	public void VerifySelectFenceUIValidation1()
	{
		if(TimeFenceStartTime.isDisplayed() && TimeFenceEndTime.isDisplayed())
		{
			Reporter.log("Fence Start time and end time is displaying in Select Fence Tab", true);
		}
		else 
		{
			ALib.AssertFailMethod("Fence Start time and end time is not displaying in Select Fence Tab");
		}
	}

	@FindBy(xpath="//div[@id='dis-card']/div[text()='Device Model']/following-sibling::div[1]")
	private WebElement DeviceModelName;

	@FindBy(xpath="//div[@id='dis-card']/div[text()='Operating System']/following-sibling::div[1]")
	private WebElement OperatingSystem;

	@FindBy(xpath="(//div[@id='dis-card']/div[text()='Device Name']/following-sibling::div/span)[1]")
	private WebElement DeviceNameLinux;

	@FindBy(xpath="//div[@id='dis-card']/div[text()='Agent Version']/following-sibling::div[1]")
	private WebElement AgentVersion;

	@FindBy(xpath="//div[@id='dis-card']/div[text()='Last Device Time']/following-sibling::div[1]")
	private WebElement LastDeviceTime;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Notes']/following-sibling::div/div/span)[1]")
	private WebElement AddNotes;

	@FindBy(xpath="//*[@id='CustomColumn1_name']/span[contains(text(),'')]")
	private WebElement CustomColumn1_name;


	public void VerifyDeviceModelName_Linux(String DeviceModelLinux) throws InterruptedException {

		String ExpectedResult=DeviceModelName.getText();
		Reporter.log(ExpectedResult,true);
		String ActualResult=DeviceModelLinux;                             //DeviceModelLinux ;
		String Pass="PASS>> "+ExpectedResult+" Device Model For Linux is Displayed";
		String Fail="FAIL>> Device Model For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, Pass, Fail);
		sleep(2);
	}

	public void VerifyOperatingSystem(String OperatingSys) throws InterruptedException {  
		String ExpectedResult2=OperatingSystem.getText();
		Reporter.log(ExpectedResult2,true);
		String ActualResult2=OperatingSys;                          //OperatingSys ;
		String Pass1="PASS>> "+ExpectedResult2+" Operating System For Linux is Displayed";
		String Fail1="FAIL>> Operating System For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass1, Fail1);
		sleep(2);


	}
	public void VerifyDeviceName(String DeviceNameForLinux) throws InterruptedException {
		String ExpectedResult3=DeviceNameLinux.getText();
		Reporter.log(ExpectedResult3,true);
		String ActualResult3=DeviceNameForLinux ;                  //DeviceNameForLinux ;
		String Pass2="PASS>> "+ExpectedResult3+" Device Name For Linux is Displayed";
		String Fail2="FAIL>>  Device Name For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail2);
		sleep(2);
	}
	public void VerifyAgentVersion(String AgentVersionLinux) throws InterruptedException {
		String ExpectedResult4=AgentVersion.getText();
		Reporter.log(ExpectedResult4,true);
		String ActualResult4=AgentVersionLinux;                    //AgentVersionLinux ;
		String Pass3="PASS>> "+ExpectedResult4+" AgentVersion For Linux is Displayed";
		String Fail3="FAIL>> AgentVersion For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
		sleep(2);
	}
	public void VerifyLastDeviceTime(String LastDeviceTimeLinux) throws InterruptedException {
		String ActualResult5=LastDeviceTime.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=LastDeviceTimeLinux;               //LastDeviceTimeLinux;
		String Pass4="PASS>> "+ActualResult5+" LastDeviceTime For Linux is Displayed";
		String Fail4="FAIL>>  LastDeviceTime For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass4, Fail4);
		sleep(2);
	}	
	public void VerifyAddNotes(String AddNotesLinux) throws InterruptedException {
		String ActualResult6=AddNotes.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6= AddNotesLinux;             //AddNotesLinux;
		String Pass5="PASS>> "+ActualResult6+" For Linux is Displayed";
		String Fail5="FAIL>> Notes For Linux is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass5,Fail5);
		sleep(2);
	}	
	public void VerifyCustomColInDeviceinfoPanelLinux(String CustomColInDeviceinfoPanel ) throws  IOException, InterruptedException{
		String ActualResult7=CustomColumn1_name.getText();

		if(ActualResult7.length()>0)
		{
			String ExpectedResult7= CustomColInDeviceinfoPanel;
			String Pass6="PASS>> CustomColumn1_name For Linux is Displayed as " +ActualResult7;
			String Fail6="FAIL>> CustomColumn1_name For Linux is not Displayed as ";
			ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass6,Fail6);
			sleep(2);	


		}	

	}
	//WINDOWS DEVICE

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Agent Version']/following-sibling::div/p)[1]")
	private WebElement DeviceModelNameWindows;

	@FindBy(xpath="//*[@id='dis-card']/div[5]/p[contains(text(),'')]")
	private WebElement MicrosoftWindowsOS; 

	@FindBy(xpath="(//*[@id='dis-card']/div[7]/span[text()])")
	private WebElement DeviceNameWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Agent Version']/following-sibling::div/p)[1]")
	private WebElement AgentVersionWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Last Device Time']/following-sibling::div/p)[1]")
	private WebElement LastDeviceTimeOfWindows;

	@FindBy(xpath="//*[@id='dis-card']/div[13]/div/span[text()]")
	private WebElement AddNotesofWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Location Tracking']/following-sibling::div/span)[1]")
	private WebElement LocTrackingWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Telecom Management']/following-sibling::div/span)[1]")
	private WebElement TelecomManagementWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Geo Fence']/following-sibling::div/span)[1]")
	private WebElement GeoFenceWindows;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Time Fence']/following-sibling::div/span)[1]")
	private WebElement TimeFenceWindows;

	public void VerifyDeviceModelName_Windows(String DeviceModelWindows) throws InterruptedException { //DeviceModelWindows

		String ActualResult1=DeviceModelName.getText();
		String ExpectedResult1=DeviceModelWindows ;
		String Pass="PASS>>Device Model For Windows Device and Windows EMM Device is Displayed "+ActualResult1;
		String Fail="FAIL>>Device Model For Windows Device and Windows EMM Device is not Displayed "+ActualResult1;
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass, Fail);
		sleep(2);
	}
	public void VerifyOperatingSystem_Windows(String DeviceOSWindows) throws InterruptedException { //String DeviceOSWindows
		String ActualResult2=OperatingSystem.getText();
		String ExpectedResult2=DeviceOSWindows ;
		String Pass1="PASS>> "+ActualResult2+" Operating System For Windows is Displayed";
		String Fail1="FAIL>> "+ActualResult2+" Operating System For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass1, Fail1);
		sleep(2);

	}
	public void VerifyDeviceNameWindows(String DeviceName_Windows) throws InterruptedException { //String DeviceName_Windows
		String ActualResult3=DeviceNameLinux.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=DeviceName_Windows ;
		String Pass2="PASS>> "+ActualResult3+" Device Name For Windows is Displayed";
		String Fail2="FAIL>> "+ActualResult3+" Device Name For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass2, Fail2);
		sleep(2);
	}

	public void VerifyAgentVersionWindows(String AgentVersion_Windows) throws InterruptedException { //String AgentVersion_Windows
		String ActualResult4=AgentVersionWindows.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=AgentVersion_Windows ;
		String Pass3="PASS>> "+ActualResult4+" AgentVersion For Windows is Displayed";
		String Fail3="FAIL>> "+ActualResult4+" AgentVersion For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
		sleep(2);
	}

	public void VerifyLastDeviceTimeWindows(String LastDeviceTime_Windows) throws InterruptedException {  //String LastDeviceTime_Windows
		String ActualResult5=LastDeviceTime.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=LastDeviceTime_Windows ;
		String Pass4="PASS>> "+ActualResult5+" LastDeviceTime For Windows is Displayed";
		String Fail4="FAIL>> "+ActualResult5+" LastDeviceTime For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass4, Fail4);
		sleep(2);
	}	

	public void VerifyAddNotesForWindows(String AddNotes_Windows) throws InterruptedException {  //String AddNotes_Windows
		String ActualResult6=AddNotes.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6=AddNotes_Windows ;
		String Pass5="PASS>> Notes "+ActualResult6+" For Windows is Displayed";
		String Fail5="FAIL>> Notes "+ActualResult6+" For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass5,Fail5);
		sleep(2);
	}	
	public void VerifyCustomColInDeviceinfoPanelWindows(String CustomColInDeviceinfoPanelWindows) throws IOException, InterruptedException{    //String CustomColInDeviceinfoPanelWindows
		String ActualResult7=CustomColumn1_name.getText();

		if(ActualResult7.length()>0)
		{
			String ExpectedResult7= CustomColInDeviceinfoPanelWindows;
			String Pass6="PASS>> CustomColumn1_name For Linux is Displayed as " +ActualResult7;
			String Fail6="FAIL>> CustomColumn1_name For Linux is not Displayed as " +ActualResult7;
			ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass6,Fail6);
			sleep(2);	

		}
	}
	public void VerifyLocTrackingForWindows(String LocTrackingForWindows) throws InterruptedException {  //String LocTrackingForWindows
		String ActualResult8=LocTrackingWindows.getText();
		Reporter.log(ActualResult8,true);
		String ExpectedResult8=LocTrackingForWindows ;
		String Pass8="PASS>> Loctaion Tracking is "+ActualResult8+" For Windows is Displayed";
		String Fail8="FAIL>> Loctaion Tracking is "+ActualResult8+" For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult8,ActualResult8,Pass8,Fail8);
		sleep(2);
	}	

	public void VerifyTelecomManagementForWindows(String TelecomManagementForWindows) throws InterruptedException {  //String TelecomManagementForWindows
		String ActualResult9=TelecomManagementWindows.getText();
		Reporter.log(ActualResult9,true);
		String ExpectedResult9=TelecomManagementForWindows ;
		String Pass9="PASS>> TelecomManagement is "+ActualResult9+" For Windows is Displayed";
		String Fail9="FAIL>> TelecomManagement is "+ActualResult9+" For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult9,ActualResult9,Pass9,Fail9);
		sleep(2);
	}	
	public void VerifyGeoFenceForWindows(String GeoFenceForWindows) throws InterruptedException {  //String GeoFenceForWindows
		String ActualResult10=GeoFenceWindows.getText();
		Reporter.log(ActualResult10,true);
		String ExpectedResult10=GeoFenceForWindows ;
		String Pass10="PASS>> GeoFence is "+ActualResult10+" For Windows is Displayed";
		String Fail10="FAIL>> GeoFence is "+ActualResult10+" For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult10,ActualResult10,Pass10,Fail10);
		sleep(2);
	}	
	public void VerifyTimeFenceForWindows(String TimeFenceForWindows) throws InterruptedException {  //String TimeFenceForWindows
		String ActualResult11=TimeFenceWindows.getText();
		Reporter.log(ActualResult11,true);
		String ExpectedResult11=TimeFenceForWindows ;
		String Pass11="PASS>> TimeFence is "+ActualResult11+" For Windows is Displayed";
		String Fail11="FAIL>> TimeFence is "+ActualResult11+" For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult11,ActualResult11,Pass11,Fail11);
		sleep(2);
	}	

	//EMM WINDOWS

	@FindBy(xpath="//*[@id='dis-card']/div[5]/p[contains(text(),'')]")
	private WebElement MicrosoftWindowsOSEMM; 

	@FindBy(xpath="//*[@id='dis-card']/div[13]/span[text()]")
	private WebElement LocTrackingWindowsEMM;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Installed Profile']/following-sibling::div/span)[1]")
	private WebElement InstalledProfile;

	public void VerifyOperatingSystem_WindowsEMM(String DeviceOSWindowsEMM) throws InterruptedException {   //String DeviceOSWindowsEMM
		String ActualResult1=OperatingSystem.getText();
		String ExpectedResult1=DeviceOSWindowsEMM ;
		String Pass1="PASS>> "+ActualResult1+" Operating System For Windows EMM is Displayed";
		String Fail1="FAIL>> "+ActualResult1+" Operating System For Windows EMM is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass1, Fail1);
		sleep(2);

	}
	public void VerifyLocTrackingForWindowsEMM(String LocTrackingForWindowsEMM) throws InterruptedException {  //String LocTrackingForWindowsEMM
		String ActualResult2=LocTrackingWindows.getText();
		Reporter.log(ActualResult2,true);
		String ExpectedResult2=LocTrackingForWindowsEMM ;
		String Pass2="PASS>> LocTrackingWindowsEMM "+ActualResult2+" For Windows EMM is Displayed";
		String Fail2="FAIL>> LocTrackingWindowsEMM "+ActualResult2+" For Windows EMM is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2,ActualResult2,Pass2,Fail2);
		sleep(2);
	}	
	public void VerifyInstalledProfileForWindowsEMM(String InstalledProfileForWindowsEMM) throws InterruptedException {  //String InstalledProfileForWindowsEMM
		String ActualResult3=InstalledProfile.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=InstalledProfileForWindowsEMM ;
		String Pass3="PASS>> InstalledProfile "+ActualResult3+" For Windows EMM is Displayed";
		String Fail3="FAIL>> InstalledProfile "+ActualResult3+" For Windows EMM is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3,ActualResult3,Pass3,Fail3);
		sleep(2);
	}	

	//MAC

	@FindBy(xpath="//*[@id='dis-card']/div[5]/p[text()]")
	private WebElement MicrosoftWindowsOSMAC; 

	@FindBy(xpath="(//*[@id='dis-card']/div[17]/span)")
	private WebElement InstalledProfileMac;

	public void VerifyOperatingSystem_MAC(String DeviceOSMAC) throws InterruptedException {
		String ActualResult1=OperatingSystem.getText();
		String ExpectedResult1=DeviceOSMAC ;
		String Pass1="PASS>> "+ActualResult1+" Operating System For MACOS is Displayed";
		String Fail1="FAIL>> "+ActualResult1+" Operating System For MACOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ExpectedResult1, Pass1, Fail1);
		sleep(2);

	}
	public void VerifyInstalledProfileForMAC(String InstalledProfileForMAC) throws InterruptedException {
		String ActualResult2=InstalledProfile.getText();
		String ExpectedResult2=InstalledProfileForMAC ;
		String Pass2="PASS>> InstalledProfile "+ActualResult2+" For MAC is Displayed";
		String Fail2="FAIL>> InstalledProfile "+ActualResult2+" For MAC is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2,ActualResult2,Pass2,Fail2);
		sleep(2);
	}	

	//IOS


	@FindBy(xpath="//*[@id='dis-card']/div[9]/p[text()]")	
	private WebElement LastDeviceTimeIOS;

	@FindBy(xpath="//*[@id='deviceNotes']")
	private WebElement deviceNotesIos;

	@FindBy(xpath="//*[@id='dis-card']/div[13]/span[text()]")
	private WebElement LocTrackingIos;

	@FindBy(xpath="//*[@id='dis-card']/div[15]/span[text()]")
	private WebElement TelecomIOS;

	@FindBy(xpath="//*[@id='dis-card']/div[17]/span[text()]")
	private WebElement InstalledProfileIOS;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Supervised']/following-sibling::div/p)[1]")
	private WebElement SupervisedIOS;

	@FindBy(xpath="(//*[@id='dis-card']/div[text()='Activation Lock Bypass Code']/following-sibling::div/p)[1]")
	private WebElement ActivationLockBypassCodeIOS;




	public void VerifyLastDeviceTimeIOS(String LastDeviceTime_IOS) throws InterruptedException {
		String ActualResult1=LastDeviceTime.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=LastDeviceTime_IOS ;
		String Pass1="PASS>> "+ExpectedResult1+" LastDeviceTime For IOS is Displayed";
		String Fail1="FAIL>> "+ExpectedResult1+" LastDeviceTime For IOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass1, Fail1);
		sleep(2);
	}	
	public void VerifyAddNotesForIOS(String AddNotes_IOS) throws InterruptedException {
		String ActualResult2=AddNotes.getText();
		Reporter.log(ActualResult2,true);
		String ExpectedResult2=AddNotes_IOS ;
		String Pass2="PASS>>Notes "+ExpectedResult2+" For IOS is Displayed";
		String Fail2="FAIL>>Notes "+ExpectedResult2+" For IOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2,ActualResult2,Pass2,Fail2);
		sleep(2);
	}	

	public void VerifyLocTrackingForIOS(String LocTrackingForIOS) throws InterruptedException {
		String ActualResult3=LocTrackingWindows.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=LocTrackingForIOS ;
		String Pass3="PASS>>Location Tracking is "+ExpectedResult3+" For IOS is Displayed";
		String Fail3="FAIL>>Location Tracking is "+ExpectedResult3+" For IOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3,ActualResult3,Pass3,Fail3);
		sleep(2);
	}	

	public void VerifyTelecomManagementForIOS(String TelecomManagementForIOS) throws InterruptedException {
		String ActualResult4=TelecomManagementWindows.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=TelecomManagementForIOS ;
		String Pass4="PASS>>Telecom Management is "+ExpectedResult4+" For IOS is Displayed";
		String Fail4="FAIL>>Telecom Management is "+ExpectedResult4+" For IOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4,ActualResult4,Pass4,Fail4);
		sleep(2);
	}	
	public void VerifyInstalledProfileForIOS(String InstalledProfileForIOS) throws InterruptedException {
		String ActualResult5=InstalledProfile.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=InstalledProfileForIOS ;
		String Pass5="PASS>> InstalledProfile "+ExpectedResult5+" For IOS is Displayed";
		String Fail5="FAIL>> InstalledProfile "+ExpectedResult5+" For IOS is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5,ActualResult5,Pass5,Fail5);
		sleep(2);
	}	

	public void VerifySupervisedForIOS(String SupervisedForIOS) throws InterruptedException {
		String ActualResult6=SupervisedIOS.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6=SupervisedForIOS ;
		String Pass6="PASS>> IOS is supervised "+ExpectedResult6+" is Displayed";
		String Fail6="FAIL>> IOS is supervised "+ExpectedResult6+" is NOT Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass6,Fail6);
		sleep(2);
	}	

	public void VerifyActivationLockBypassCodeForIOS(String ActivationLockBypassCodeForIOS) throws InterruptedException {
		String ActualResult7=ActivationLockBypassCodeIOS.getText();
		Reporter.log(ActualResult7,true);
		String ExpectedResult7=ActivationLockBypassCodeForIOS ;
		String Pass7="PASS>> IOS has Activation Lock Bypass Code "+ExpectedResult7+" is Displayed";
		String Fail7="FAIL>> IOS has Activation Lock Bypass Code "+ExpectedResult7+" is NOT Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
	}	

	//ANDROID NETWORK PANEL

	@FindBy(xpath="(//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='MAC Address']/following-sibling::div/p)[1]")
	private WebElement MACaddressAndroid;

	@FindBy(xpath="(//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='IP Address']/following-sibling::div/p)[1]")
	private WebElement IPaddressAndroid;

	@FindBy(xpath="(//*[@id='networkinfo-card']/div[7]/p[text()])")
	private WebElement LocalIPAddressAndroid;

	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='Connection']/following-sibling::div[1]")
	private WebElement ConnectionAndroid;

	@FindBy(xpath="//*[@id='networkinfo-card']/div[10]/following-sibling::div[1]")
	private WebElement WiFiSignalAndroid;

	@FindBy(xpath="//*[@id='networkinfo-card']/div[11]/following-sibling::div[2]")
	private WebElement MobileSignalAndroid;

	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='IMEI']/following-sibling::div[1]")
	private WebElement IMEIAndroid;

   @FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='Serial Number']/following-sibling::div[1]")
	private WebElement SerialNumberAndroid;

	@FindBy(xpath="//*[@id='networkinfo-card1']/div[1]/span/div[text()]")
	private WebElement WiFiSSIDAndroid;

	@FindBy(xpath="//*[@id='networkinfo-card1']/div[2]/span/div[text()]")
	private WebElement MobileNetworkAndroid;

	@FindBy(xpath="//*[@id='deviceInfoCard']/div[3]/div[3]/div/div/div[text()='IMSI']/following-sibling::div[1]")
	private WebElement IMSIAndroid;

	public void VerifyAndroidMACaddress(String AndroidMACaddress) throws InterruptedException {
		String ActualResult1=MACaddressAndroid.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=AndroidMACaddress ;
		String Pass1="PASS>> MAC Address for Android "+ExpectedResult1+" is Displayed";
		String Fail1="FAIL>> MAC Address for Android "+ExpectedResult1+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1, ActualResult1, Pass1, Fail1);
		sleep(2);
	}	
	public void VerifyAndroidIPaddress(String AndroidIPaddress) throws InterruptedException {
		String ActualResult2=IPaddressAndroid.getText();
		Reporter.log(ActualResult2,true);
		String ExpectedResult2=AndroidIPaddress ;
		String Pass2="PASS>> IP Address for Android "+ExpectedResult2+" is Displayed";
		String Fail2="FAIL>> IP Address for Android "+ExpectedResult2+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2, ActualResult2, Pass2, Fail2);
		sleep(2);
	}	

	public void VerifyAndroidLocalIPaddress(String AndroidLocalIPaddress) throws InterruptedException {
		String ActualResult3=LocalIPAddressAndroid.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=AndroidLocalIPaddress ;
		String Pass3="PASS>> Local IP Address for Android "+ExpectedResult3+" is Displayed";
		String Fail3="FAIL>> Local IP Address for Android "+ExpectedResult3+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3, ActualResult3, Pass3, Fail3);
		sleep(2);
	}	
	public void VerifyAndroidConnection(String AndroidConnection) throws InterruptedException {
		String ActualResult4=ConnectionAndroid.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=AndroidConnection ;
		String Pass4="PASS>> Connection for Android "+ExpectedResult4+" is Displayed";
		String Fail4="FAIL>> Connection for Android "+ExpectedResult4+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass4, Fail4);
		sleep(2);
	}	
	public void VerifyAndroidWifiSignal(String AndroidWifiSignal) throws InterruptedException {
		String ActualResult5=WiFiSignalAndroid.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=AndroidWifiSignal ;
		String Pass5="PASS>> Wifi Signal for Android "+ExpectedResult5+" is Displayed";
		String Fail5="FAIL>> Wifi Signal for Android "+ExpectedResult5+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass5, Fail5);
		sleep(2);
	}	
	public void VerifyAndroidMobileSignal(String AndroidMobileSignal) throws InterruptedException {
		String ActualResult6=MobileSignalAndroid.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6=AndroidMobileSignal ;
		String Pass6="PASS>> Mobile Signal for Android "+ExpectedResult6+" is Displayed";
		String Fail6="FAIL>> Mobile Signal for Android "+ExpectedResult6+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6, ActualResult6, Pass6, Fail6);
		sleep(2);
	}	
	public void VerifyAndroidIMEI(String AndroidIMEI) throws InterruptedException {
		String ActualResult7=IMEIAndroid.getText();
		Reporter.log(ActualResult7,true);
		String ExpectedResult7=AndroidIMEI ;
		String Pass7="PASS>> IMEI Number for Android "+ExpectedResult7+" is Displayed";
		String Fail7="FAIL>> IMEI Number for Android "+ExpectedResult7+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
	}	

	public void VerifyAndroidSerialNumber(String AndroidSerialNumber) throws InterruptedException {
		String ActualResult8=SerialNumberAndroid.getText();
		Reporter.log(ActualResult8,true);
		String ExpectedResult8=AndroidSerialNumber ;
		String Pass8="PASS>> Serial Number for Android "+ExpectedResult8+" is Displayed";
		String Fail8="FAIL>> Serial Number for Android "+ExpectedResult8+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult8,ActualResult8,Pass8,Fail8);
		sleep(2);
	}	
	public void VerifyAndroidIMSI(String AndroidIMSI) throws InterruptedException {
		String ActualResult9=IMSIAndroid.getText();
		Reporter.log(ActualResult9,true);
		String ExpectedResult9=AndroidIMSI ;
		String Pass9="PASS>> IMSI Number for Android "+ExpectedResult9+" is Displayed";
		String Fail9="FAIL>> IMSI Number for Android "+ExpectedResult9+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult9,ActualResult9,Pass9,Fail9);
		sleep(2);
	}	
	public void VerifyAndroidWIFI_SSID(String AndroidWIFI_SSID) throws InterruptedException {
		String ActualResult10=WiFiSSIDAndroid.getText();
		Reporter.log(ActualResult10,true);
		String ExpectedResult10=AndroidWIFI_SSID ;
		String Pass10="PASS>> WIFI_SSID for Android "+ExpectedResult10+" is Displayed";
		String Fail10="FAIL>> WIFI_SSID for Android "+ExpectedResult10+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult10,ActualResult10,Pass10,Fail10);
		sleep(2);
	}	
	public void VerifyAndroidMobileNetwork(String AndroidMobileNetwork) throws InterruptedException {
		String ActualResult11=MobileNetworkAndroid.getText();
		Reporter.log(ActualResult11,true);
		String ExpectedResult11=AndroidMobileNetwork ;
		String Pass11="PASS>> MobileNetwork for Android "+ExpectedResult11+" is Displayed";
		String Fail11="FAIL>> MobileNetwork for Android "+ExpectedResult11+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult11,ActualResult11,Pass11,Fail11);
		sleep(2);
	}	

	// Linux NETWORK PANEL

	@FindBy(xpath="//*[@id='networkinfo-card']/div[9][contains(text(),'')]")
	private WebElement ConnectionLinux;

	public void VerifyLinuxConnection(String LinuxConnection) throws InterruptedException {
		String ActualResult1=ConnectionAndroid.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=LinuxConnection ;
		String Pass1="PASS>> Connection for Linux "+ExpectedResult1+" is Displayed";
		String Fail1="FAIL>> Connection for Linux "+ExpectedResult1+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1,ActualResult1,Pass1,Fail1);
		sleep(2);
	}	
	//EMM Device

	@FindBy(xpath="(//*[@id='networkinfo-card']/div[8]/p[text()])")
	private WebElement IPaddressEMM;

	public void VerifyEMMIPaddress(String EMMIPaddress) throws InterruptedException {
		String ActualResult1=IPaddressAndroid.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=EMMIPaddress ;
		String Pass1="PASS>> IP Address for EMM "+ExpectedResult1+" is Displayed";
		String Fail1="FAIL>> IP Address for EMM "+ExpectedResult1+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1,ActualResult1,Pass1,Fail1);
		sleep(2);
	}	


	//WINDOWS Network Panel

	@FindBy(xpath="(//*[@id='networkinfo-card']/div[7]/p[text()])")
	private WebElement IPaddressWINDOWS;

	@FindBy(xpath="(//*[@id='networkinfo-card']/div[10]/p[text()])")
	private WebElement LocalIPaddressWINDOWS;

	@FindBy(xpath="//*[@id='networkinfo-card']/div[11]")
	private WebElement ConnectionWindows;

	@FindBy(xpath="//*[@id='networkinfo-card']/div[13]/following-sibling::div[1]")
	private WebElement WiFiSignalWindows;

	@FindBy(xpath="//*[@id='networkinfo-card']/div[15]")
	private WebElement MobileSignalWindows;

	//	@FindBy(xpath="//*[@id='networkinfo-card']/div[15]")
	//	private WebElement IMEIAndroid;

	@FindBy(xpath="(//*[@id='networkinfo-card']/div[17]/p[text()])")
	private WebElement SerialNumberWindows;

	@FindBy(xpath="//*[@id='networkinfo-card1']/div[1]/span/div[text()]")
	private WebElement WiFiSSIDWindows;

	//	@FindBy(xpath="//*[@id='networkinfo-card1']/div[2]/span/div[text()]")
	//	private WebElement MobileNetworkAndroid;

	//	@FindBy(xpath="//*[@id='networkinfo-card']/div[17]")
	//	private WebElement IMSIAndroid;



	public void VerifyWindowsIPaddress(String WindowsIPaddress) throws InterruptedException {
		String ActualResult1=IPaddressAndroid.getText();
		Reporter.log(ActualResult1,true);
		String ExpectedResult1=WindowsIPaddress ;
		String Pass1="PASS>> IP Address for Windows "+ExpectedResult1+" is Displayed";
		String Fail1="FAIL>> IP Address for Windows "+ExpectedResult1+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult1,ActualResult1,Pass1,Fail1);
		sleep(2);
	}	

	public void VerifyWindowsLocalIPaddress(String WindowsLocalIPaddress) throws InterruptedException {
		String ActualResult2=LocalIPaddressWINDOWS.getText();
		Reporter.log(ActualResult2,true);
		String ExpectedResult2=WindowsLocalIPaddress ;
		String Pass2="PASS>> Local IP Address for Windows "+ExpectedResult2+" is Displayed";
		String Fail2="FAIL>> Local IP Address for Windows "+ExpectedResult2+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult2,ActualResult2,Pass2,Fail2);
		sleep(2);
	}
	public void VerifyWindowsConnection(String WindowsConnection) throws InterruptedException {
		String ActualResult3=ConnectionAndroid.getText();
		Reporter.log(ActualResult3,true);
		String ExpectedResult3=WindowsConnection ;
		String Pass3="PASS>> Connection for Windows "+ExpectedResult3+" is Displayed";
		String Fail3="FAIL>> Connection for Windows "+ExpectedResult3+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult3,ActualResult3,Pass3,Fail3);
		sleep(2);
	}	

	public void VerifyWindowsWifiSignal(String WindowsWifiSignal) throws InterruptedException {
		String ActualResult4=WiFiSignalWindows.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=WindowsWifiSignal ;
		String Pass4="PASS>> Wifi Signal for Windows "+ExpectedResult4+" is Displayed";
		String Fail4="FAIL>> Wifi Signal for Windows "+ExpectedResult4+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4,ActualResult4,Pass4,Fail4);
		sleep(2);
	}	
	public void VerifyWindowsMobileSignal(String WindowsMobileSignal) throws InterruptedException {
		String ActualResult5=MobileSignalAndroid.getText();
		Reporter.log(ActualResult5,true);
		String ExpectedResult5=WindowsMobileSignal ;
		String Pass5="PASS>> Mobile Signal for Windows "+ExpectedResult5+" is Displayed";
		String Fail5="FAIL>> Mobile Signal for Windows "+ExpectedResult5+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult5, ActualResult5, Pass5, Fail5);
		sleep(2);
	}	
	/*public void VerifyAndroidIMEI(String AndroidIMEI) throws InterruptedException {
		String ActualResult7=IMEIAndroid.getText();
		Reporter.log(ActualResult7,true);
		String ExpectedResult7=AndroidIMEI ;
		String Pass7="PASS>> IMEI Number for Android "+ExpectedResult7+" is Displayed";
		String Fail7="FAIL>> IMEI Number for Android "+ExpectedResult7+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
	}	*/

	public void VerifyWindowsSerialNumber(String WindowsSerialNumber) throws InterruptedException {
		String ActualResult6=SerialNumberAndroid.getText();
		Reporter.log(ActualResult6,true);
		String ExpectedResult6=WindowsSerialNumber ;
		String Pass6="PASS>> Serial Number for Windows "+ExpectedResult6+" is Displayed";
		String Fail6="FAIL>> Serial Number for Windows "+ExpectedResult6+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult6,ActualResult6,Pass6,Fail6);
		sleep(2);
	}	
	/*public void VerifyAndroidIMSI(String AndroidIMSI) throws InterruptedException {
		String ActualResult9=IMSIAndroid.getText();
		Reporter.log(ActualResult9,true);
		String ExpectedResult9=AndroidIMSI ;
		String Pass9="PASS>> IMSI Number for Android "+ExpectedResult9+" is Displayed";
		String Fail9="FAIL>> IMSI Number for Android "+ExpectedResult9+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult9,ActualResult9,Pass9,Fail9);
		sleep(2);
	}	*/
	public void VerifyWindowsWIFI_SSID(String WindowsWIFI_SSID) throws InterruptedException {
		String ActualResult7=WiFiSSIDWindows.getText();
		Reporter.log(ActualResult7,true);
		String ExpectedResult7=WindowsWIFI_SSID ;
		String Pass7="PASS>> WIFI_SSID for Windows "+ExpectedResult7+" is Displayed";
		String Fail7="FAIL>> WIFI_SSID for Windows "+ExpectedResult7+" is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult7,ActualResult7,Pass7,Fail7);
		sleep(2);
	}	
	public void VerifyAgentVersionForAndroidDevice(String AgentVersionForAndroidDevice) throws InterruptedException {
		String ActualResult4=AgentVersionWindows.getText();
		Reporter.log(ActualResult4,true);
		String ExpectedResult4=AgentVersionForAndroidDevice ;
		String Pass3="PASS>> "+ActualResult4+" AgentVersion For Windows is Displayed";
		String Fail3="FAIL>> "+ActualResult4+" AgentVersion For Windows is not Displayed";
		ALib.AssertEqualsMethod(ExpectedResult4, ActualResult4, Pass3, Fail3);
		sleep(2);
	}

//TIME FENCE
	@FindBy(xpath="//*[@id='comp_addDelayBtn']/span")
	private WebElement comp_addDelayBtn;
	
	@FindBy(xpath="//*[@id='delay_interval']")
	private WebElement delay_interval;
	
	
	public void ClickOnDelayCompositeJob(String DelayTime) throws InterruptedException {
		
		sleep(2);
		comp_addDelayBtn.click();
		waitForXpathPresent("//h4[contains(text(),'Delay')]");
		delay_interval.clear();
		sleep(1);
		delay_interval.sendKeys(DelayTime);
		sleep(1);
		Initialization.driver.findElement(By.xpath("//*[@id='DelayJobModal']/div/div/div[3]/button")).click();
		sleep(1);
	}
	
	
	public void SelectDifferentNetwork(String HotspotInitial, String Hotspot) throws InterruptedException 
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
		 sleep(2);
		 Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Network & Internet'] | //android.widget.TextView[contains(@resource-id, 'title') and @text='Network & internet']").click();
		 Reporter.log("Clicked on Network & Internet", true);
		 sleep(2);
		 Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+HotspotInitial+"']").click();
		 Reporter.log("Clicked on Wifi", true);
		 sleep(2);
	//	 Initialization.driverAppium.scrollToExact(Hotspot);
		 Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + Hotspot + "\").instance(0))"));
		 Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Hotspot+"']").click();
		 System.out.println("Clicked on Different wi-fi");
		 sleep(7);
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Connected']")));
		 String TitleText = Initialization.driverAppium.findElement(By.id("android:id/title")).getText();
		 String SummaryText = Initialization.driverAppium.findElement(By.id("android:id/summary")).getText();
		 if(TitleText.equals(Hotspot) && SummaryText.equals("Connected")) 
		 {
			 System.out.println("Connected successfully to different network");
		 }
		 else 
		 {
			 ALib.AssertFailMethod("Connection failed");
		 }
	}
	

	@FindBy(xpath="//div[@id='data_usage_policy']")
	private WebElement TelecomManagementPolicy;

	
	public void ClickOnTelecomManagentPolicy() throws InterruptedException {
		try 
		{
			waitForidPresent("data_usage_policy");
			TelecomManagementPolicy.click();
			Reporter.log("Clicked On Telecom Management Policy", true);
			sleep(4);
		}
		catch(Exception e)
		{
			action.click(TelecomManagementPolicy).build().perform();
			Reporter.log("Clicked On Telecom Management Policy", true);
			sleep(4);
		}
	} 
	@FindBy(xpath="//input[@id='MobileEnableDataUsage']")
	private WebElement EnableTelecomManagement;

	public void ClickOnEnableTelecomManagement(boolean flag) throws InterruptedException {
		boolean selected = EnableTelecomManagement.isSelected();
		if(flag==selected) {
		EnableTelecomManagement.click();
		}
		Reporter.log("Clicked on Enable Telecom Management", true);
		sleep(2);
	}
	@FindBy(xpath="//input[@id='MobileDataUsageWarningLimit']")
	private WebElement MobileDataUsageWarningLimit;

	public void EnterDataLimitWarningReset()
	{
		MobileDataUsageWarningLimit.clear();
		MobileDataUsageWarningLimit.sendKeys("0.00");
		Reporter.log("Cleared First Data limit", true);
	} 

	
	@FindBy(xpath="//input[@id='limit1_block1']")
	private WebElement BlockData1;
	
	@FindBy(xpath="//input[@id='limit2_block2']")
	private WebElement BlockData2;
	
	@FindBy(xpath="//div[@id='BlockdataPopup1']/div[1]/div[1]/div[3]/button[1]")
	private WebElement OkBlockData1;

	@FindBy(xpath="//div[@id='BlockdataPopup1']/div[1]/div[1]/div[1]/button")
	private WebElement BlockdataPopup1Close;

	public void CheckBlockDataWarning(boolean value) throws InterruptedException
	{
		boolean flag = BlockData1.isSelected();
		if(flag==value) {
			try
			{
				BlockData1.click();
				Reporter.log("Clicked on BlockData2", true);
				waitForXpathPresent("//div[@id='BlockdataPopup2']//p[text()='Do you want to proceed?']");
				sleep(5);
				OkBlockData1.click();
				Reporter.log("Clicked on Ok button in BlockData", true);
				sleep(2);
			}
			catch(Exception e)
			{
				BlockdataPopup1Close.click();
				sleep(2);
				action.click(BlockData1).build().perform();
				Reporter.log("Clicked on BlockData2", true);
				waitForXpathPresent("//div[@id='BlockdataPopup2']//p[text()='Do you want to proceed?']");
				sleep(5);
				action.click(OkBlockData1).build().perform();
				Reporter.log("Clicked on Ok button in BlockData", true);
				sleep(2);
			}
		}
		Reporter.log("Clicked on block data", true);
	}
	public void VerifyRoamingDataUI () throws InterruptedException {

		MobileDataUsageWarningDataType.click();
		String GetDataType=MobileDataUsageWarningDataType.getText();
		System.out.println(GetDataType);
			
		if(GetDataType.contains("Roaming data "))
		{
		ALib.AssertFailMethod(" Roaming data  is displayed ");	
			
		}	
		
	else {
			Reporter.log("PASS>>> Roaming data  is not displayed", true);
		}
	
	}
	public void closetelecompopup() throws InterruptedException {
		
		Initialization.driver.findElement(By.xpath("//*[@id='telecom_mgnment_modal']/div/div/div[1]/button")).click();
		sleep(2);
	}
		
		





	public void EnterDataLimitWarning() throws InterruptedException 
	{
		MobileDataUsageWarningLimit.clear();
		MobileDataUsageWarningLimit.sendKeys(DataUsageLimit1);
		Reporter.log("Entered First Data limit", true);
		sleep(2);
	} 
	@FindBy(xpath="//select[@id='MobileDataUsageWarningData']")
	private WebElement MobileDataUsageWarningData;

	public void SelectDataWarning() 
	{
		try {
			MobileDataUsageWarningData.click();
			Select select=new Select(MobileDataUsageWarningData);
			select.selectByVisibleText("MB");
			Reporter.log("Selected Mobile Data Usage Warning Data", true);
		}
		catch(Exception e) 
		{
			action.click(MobileDataUsageWarningData).build().perform();
			Select select=new Select(MobileDataUsageWarningData);
			select.selectByVisibleText("MB");
			Reporter.log("Selected Mobile Data Usage Warning Data", true);
		}
	} 

	@FindBy(xpath="//select[@id='MobileDataUsageWarningDataType']")
	private WebElement MobileDataUsageWarningDataType;

	
	public void SelectDataTypeWarning() {
		try 
		{
			MobileDataUsageWarningDataType.click();
			Select select=new Select(MobileDataUsageWarningDataType);
			select.selectByVisibleText("Overall Data");
			Reporter.log("Selected Mobile Data Usage Warning Data Type", true);
		}
		catch(Exception e) 
		{
			action.click(MobileDataUsageWarningDataType).build().perform();
			Select select=new Select(MobileDataUsageWarningDataType);
			select.selectByVisibleText("Overall Data");
			Reporter.log("Selected Mobile Data Usage Warning Data Type", true);
		}
	}
	
	@FindBy(xpath="//input[@id='SendDeviceAlretWarning']")
	private WebElement SendDeviceAlretWarning;


	public void CheckSendDeviceAlretWarning(boolean value)
	{
		try 
		{
			boolean flag = SendDeviceAlretWarning.isSelected();
			if(flag==value) {
				SendDeviceAlretWarning.click();
			}
			Reporter.log("Clicked on SendDeviceAlretWarning", true);
		}
		catch(Exception e) 
		{
			boolean flag = SendDeviceAlretWarning.isSelected();
			if(flag==value) {
				action.click(SendDeviceAlretWarning).build().perform();
			}
			Reporter.log("Clicked on SendDeviceAlretWarning", true);
		}
		
	}

	
	public void VerifyAlertMessageFromDeviceLimit1(String value, int waitTime) {
		
		WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@resource-id, 'textViewSubject')]")));
		String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'textViewSubject')]").getText();
		System.out.println(alertMessage);
		boolean flag;
		if(alertMessage.contains(Config.Limit1) && alertMessage.contains(value)) {
			flag=true;
		}
		else {
			flag=false;
		}
		String PassStatement="Alert messeage is displayed in device";
		String FailStatement="Alert messeage is not displayed in device";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	public void VerifyNixStatus(String value) {
		WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, 360);
		try {
			wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='"+value+"']")));
			Reporter.log("Device is in "+value+"", true);
		}
		catch(Exception e) {
			ALib.AssertFailMethod("Device is not in "+value+"");
		}
	}

	
	public void ClickOnConnections() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Connections']").click();
		Reporter.log("Clicked on Connections", true);
		sleep(2);
	}
	public void ClickOnDataUsage() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Data usage']").click();
		Reporter.log("Clicked on Data usage", true);
		sleep(2);
	}
	public void VerifyMobileDataIsEnabled(boolean value) throws InterruptedException 
	{
		boolean MobiledataStatus = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Mobile data']").isEnabled();
		if(value==MobiledataStatus) {
			Reporter.log("Mobile data: Is Enabled>> "+MobiledataStatus+"", true);
		}
		else {
			ALib.AssertFailMethod("Mobile data: Is Enabled>> "+MobiledataStatus+"");
		}
	}
	@FindBy(id="appBtn2")
	private WebElement morebutton;
	
	@FindBy(xpath="//a[@id='dataUsageEditBtn']//preceding-sibling::span")
	private WebElement TelecomStatus;
	
	public void ClickOnMorebutton() throws InterruptedException 
	{
		morebutton.click();
		waitForXpathPresent("//div[@id='button_menu_holder']/div[1]");
		sleep(2);
		Reporter.log("Clicked on more button of dynamic job", true);
	}
	@FindBy(xpath="//p[contains(text(),'Telecom Management is turned off for device ')]")
	private WebElement Telecomconfirmation;
	
	@FindBy(xpath="//div[@id='dataUsage']")
	private WebElement DataUsage;

	@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div[1]/div[1]/div[2]/button[1]")
	private WebElement DialogueTelecomOff;

	
	public void ClickODataUsage() throws InterruptedException 
	{
		DataUsage.click();
		sleep(5);
		
		try{
		if(Telecomconfirmation.isDisplayed()) {
			DialogueTelecomOff.click();
			Reporter.log("Clicked on No button of Telecom Management is turned off for device");
			waitForXpathPresent("//div[@id='data_usage_refresh']");
		}
		}
		catch(Exception e) {
			Reporter.log("Clicked on DataUsage", true);
			waitForXpathPresent("//div[@id='data_usage_refresh']");
			sleep(2);
		}
	}
	@FindBy(xpath="//input[@id='SendMDMAlertWarning']")
	private WebElement SendMDMAlertWarning;

	public void CheckSendMDMAlertWarning(boolean value)
	{
		try 
		{
			boolean flag = SendMDMAlertWarning.isSelected();
			if(flag==value) {
				SendMDMAlertWarning.click();
			}
			Reporter.log("Clicked on SendMDMAlertWarning", true);
		}
		catch(Exception e) 
		{
			boolean flag = SendMDMAlertWarning.isSelected();
			if(flag==value) {
				action.click(SendMDMAlertWarning).build().perform();
			}
			Reporter.log("Clicked on SendMDMAlertWarning", true);
		}
		
	}
	
	@FindBy(id="SendEmailWarning")
	private WebElement EmailAlertWarning;

	@FindBy(xpath="//div[@id='EmailAddressPop1']//input[@id='EmailId']")
	private WebElement EnterEmailForAlertLimit1;

	
	@FindBy(xpath="//div[@id='EmailAddressPop1']/div[1]/div[1]/div[3]/button[1]")
	private WebElement DoneButtonEmailAddress1;
	
	@FindBy(xpath="//div[@id='EmailAddressPop1']/div[1]/div[1]/div[1]/button")
	private WebElement EmailAddressPop1Close;


	public void ClickAndEnterEmailForAlertLimit1(boolean value, String Email) throws InterruptedException {
		boolean flag = EmailAlertWarning.isSelected();
		if(flag==value) {
			try
				{
				EmailAlertWarning.click();
				Reporter.log("Clicked on Send Email Alert For Limit1", true);
				waitForXpathPresent("//div[@id='EmailAddressPop2']//input[@id='EmailId']");
				sleep(2);
				EnterEmailForAlertLimit1.clear();
				EnterEmailForAlertLimit1.sendKeys(Email);
				Reporter.log("Entered EmailID for Limit1", true);
				sleep(2);
				DoneButtonEmailAddress1.click();
				Reporter.log("Clicked on Done button", true);
				}
			catch(Exception e) 
			{
				EmailAddressPop1Close.click();
				sleep(2);
				action.click(EmailAlertWarning).build().perform();
				Reporter.log("Clicked on Send Email Alert For Limit1", true);
				waitForXpathPresent("//div[@id='EmailAddressPop2']//input[@id='EmailId']");
				sleep(2);
				EnterEmailForAlertLimit1.clear();
				EnterEmailForAlertLimit1.sendKeys(Email);
				Reporter.log("Entered EmailID for Limit1", true);
				sleep(2);
				action.click(DoneButtonEmailAddress1).build().perform();
				Reporter.log("Clicked on Done button", true);
			}
			Reporter.log("Email alert1 is selected", true);
		}

	}

	@FindBy(xpath="//input[@id='MobileDataUsageThresholdLimit']")
	private WebElement MobileDataUsageThresholdLimit;

	public void EnterDataLimitThresholdReset() {
		MobileDataUsageThresholdLimit.clear();
		MobileDataUsageThresholdLimit.sendKeys("0.00");
		Reporter.log("Cleared second Data limit", true);
	} 
	@FindBy(xpath="//div[@id='BlockdataPopup2']/div[1]/div[1]/div[3]/button[1]")
	private WebElement OkBlockData2;
	@FindBy(xpath="//div[@id='BlockdataPopup2']/div[1]/div[1]/div[1]/button")
	private WebElement BlockdataPopup2Close;

	
	public void CheckBlockDataThreshold(boolean value) throws InterruptedException
	{
		boolean flag = BlockData2.isSelected();
		if(flag==value) 
		{
			try 
			{
				BlockData2.click();
				Reporter.log("Clicked on BlockData2", true);
				waitForXpathPresent("//div[@id='BlockdataPopup2']//p[text()='Do you want to proceed?']");
				sleep(5);
				OkBlockData2.click();
				Reporter.log("Clicked on Ok button in BlockData", true);
				sleep(2);
			}
			catch(Exception e)
			{
				BlockdataPopup2Close.click();
				sleep(2);
				action.click(BlockData2).build().perform();
				Reporter.log("Clicked on BlockData2", true);
				waitForXpathPresent("//div[@id='BlockdataPopup2']//p[text()='Do you want to proceed?']");
				sleep(5);
				action.click(OkBlockData2).build().perform();
				Reporter.log("Clicked on Ok button in BlockData", true);
				sleep(2);
			}
		}
	}
	@FindBy(xpath="//input[@id='SendDeviceAlretThreshold']")
	private WebElement SendDeviceAlretThreshold;

	public void CheckSendDeviceAlretThreshold(boolean value)
	{
		try 
		{
			boolean flag = SendDeviceAlretThreshold.isSelected();
			if(flag==value) {
				SendDeviceAlretThreshold.click();
			}
			Reporter.log("Clicked on SendDeviceAlretThreshold", true);
		}
		catch(Exception e) {
			boolean flag = SendDeviceAlretThreshold.isSelected();
			if(flag==value) {
				action.click(SendDeviceAlretThreshold).build().perform();
			}
			Reporter.log("Clicked on SendDeviceAlretThreshold", true);
		}
	}
	@FindBy(xpath="//input[@id='SendMDMAlertThreshold']")
	private WebElement SendMDMAlertThreshold;

	
	public void CheckSendMDMAlertThreshold(boolean value)
	{
		try {
			boolean flag = SendMDMAlertThreshold.isSelected();
			if(flag==value) {
				SendMDMAlertThreshold.click();
			}
			Reporter.log("Clicked on SendMDMAlertThreshold", true);
		}
		catch(Exception e) {
			boolean flag = SendMDMAlertThreshold.isSelected();
			if(flag==value) {
				action.click(SendMDMAlertThreshold).build().perform();
			}
			Reporter.log("Clicked on SendMDMAlertThreshold", true);
		}
	}
	@FindBy(xpath="//input[@id='SendEmailThreshold']")
	private WebElement EmailAlertThreshold;

	@FindBy(xpath="//div[@id='EmailAddressPop2']//input[@id='EmailId']")
	private WebElement EnterEmailForAlertLimit2;

	@FindBy(xpath="//div[@id='EmailAddressPop2']/div[1]/div[1]/div[3]/button[1]")
	private WebElement DoneButtonEmailAddress2;

	@FindBy(xpath="//div[@id='EmailAddressPop2']/div[1]/div[1]/div[1]/button")
	private WebElement EmailAddressPop2Close;

	public void ClickAndEnterEmailForAlertLimit2(boolean value, String Email) throws InterruptedException
	{
		boolean flag = EmailAlertThreshold.isSelected();
		if(flag==value) 
		{
			try	
			{
				EmailAlertThreshold.click();
				Reporter.log("Clicked on Send Email Alert For Limit2", true);
				waitForXpathPresent("//div[@id='EmailAddressPop2']//input[@id='EmailId']");
				sleep(2);
				EnterEmailForAlertLimit2.clear();
				EnterEmailForAlertLimit2.sendKeys(Email);
				Reporter.log("Entered EmailID for Limit2", true);
				sleep(2);
				DoneButtonEmailAddress2.click();
				Reporter.log("Clicked on Done button", true);
			}
			catch(Exception e)
			{
				EmailAddressPop2Close.click();
				sleep(2);
				action.click(EmailAlertThreshold).build().perform();
				Reporter.log("Clicked on Send Email Alert For Limit2", true);
				waitForXpathPresent("//div[@id='EmailAddressPop2']//input[@id='EmailId']");
				sleep(2);
				EnterEmailForAlertLimit2.clear();
				EnterEmailForAlertLimit2.sendKeys(Email);
				Reporter.log("Entered EmailID for Limit2", true);
				sleep(2);
				action.click(DoneButtonEmailAddress2).build().perform();
				Reporter.log("Clicked on Done button", true);
			}
		}
		Reporter.log("Email alert2 is selected", true);
	}

	@FindBy(xpath="//div[@id='data_usage_refresh']")
	private WebElement RefreshDataUsage;

	public void RefreshDataUsage() throws InterruptedException 
	{
		Actions action=new Actions(Initialization.driver);
		action.click(RefreshDataUsage).build().perform();
		Reporter.log("Clicked on DataUsage Refresh button", true);
		while(Initialization.driver.findElement(By.xpath("//div[@class='load-bar']")).isDisplayed()){
			sleep(2);
			System.out.println("loading");
		}
		sleep(5);
	}
	String DataUsageUsedData;
	String DataUsageLimit1;
	String DataUsageLimit2;
	
	@FindBy(xpath="//div[@id='datausage_useddata']")
	private WebElement UsedData;

	public void GetUsedData() throws InterruptedException 
	{
		String DataUsageUseData = UsedData.getText();
		String[] arrSplit_1 = DataUsageUseData.split("\\s");
		String value = null;
		    for (int i=0; i < 1; i++)
		    {
		    	value = arrSplit_1[i];
		    }
//		    int i=Integer.parseInt(value);  
		float DataUsageUsedData=Float.parseFloat(value);
		System.out.println(DataUsageUsedData);
		Reporter.log("Get the Data usage", true);
//		int UsedDataInint=Integer.parseInt(DataUsageUsedData); 
		float Limit1 = DataUsageUsedData+Config.DataLimit1;
		float Limit2 = DataUsageUsedData+Config.DataLimit2;
		DataUsageLimit1=String.valueOf(Limit1);
		DataUsageLimit2=String.valueOf(Limit2);
	}
	@FindBy(xpath="//div[@id='datausage_modal']/div[1]/button")
	private WebElement CloseDataUsage;

	public void ClickOnCloseDataUsage() throws InterruptedException 
	{
		CloseDataUsage.click();
		Reporter.log("Clicked on close button of data usage");
		sleep(2);
	}
	public void InstallApplicationsFromPlayStore(String value, String ScrollToSubTitle,String NameText) throws IOException, InterruptedException
	{
//		Initialization.driverAppium.scrollTo("Install");
		WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, 300);
		Runtime.getRuntime().exec("adb shell am start -a android.intent.action.VIEW -d 'market://details?id="+value+"'");
		sleep(4);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[contains(@resource-id, 'right_button') and @text='Install']")));
		System.out.println("success");
		try 
		{
			System.out.println("Try");
			Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + NameText + "\").instance(0))"));//("Install");
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@resource-id, 'right_button') and @text='Install']").click();
//			wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[contains(@resource-id, 'left_button') and @text='Uninstall']")));
//			System.out.println("Download completed");
		}
		catch(Exception e) 
		{
			System.out.println("Catch");
			Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + ScrollToSubTitle + "\").instance(0))"));//(ScrollToSubTitle);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@resource-id, 'right_button') and @text='Install']").click();
//			wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[contains(@resource-id, 'left_button') and @text='Uninstall']")));
//			System.out.println("Download completed");
		}	
	}
	
	@FindBy(xpath="//div[@id='telecom_mgnment_modal']/div[1]/div[1]/div[3]/button")
	private WebElement TelecomOkbutton;

	
	public void ClickOnOKButtonInTelecomManagementFromJobCreate(String job) throws InterruptedException {
		js.executeScript("arguments[0].scrollIntoView()",TelecomOkbutton);
		sleep(2);
		TelecomOkbutton.click();
		Reporter.log("Clicked on OK Button in Telecom Management policy", true);
		waitForXpathPresent("//span[text()='Job created successfully.']");
		sleep(2);
		waitForXpathPresent("//p[text()='"+job+"']");
		sleep(4);
	}
	@FindBy(xpath="//select[@id='confidBillCycle']")
	private WebElement ConfiguringBillingCycle;


	public void SelectConfigureBilligCycle(String value) throws InterruptedException {
		Select select=new Select(ConfiguringBillingCycle);
		select.selectByVisibleText(value);
		Reporter.log("Selected Configuring Billing Cycle as "+value+"", true);
		sleep(2);
	}
	@FindBy(xpath="//input[@id='job_name_input']")
	private WebElement TelecomManagementJobName;

	public void EnterTelecomManagementName(String value) throws InterruptedException {
		waitForXpathPresent("//input[@id='job_name_input']");
		TelecomManagementJobName.sendKeys(value);
		Reporter.log("Entered Telecom Management Policy Job name", true);
		sleep(2);
} 
	public void VerifyAppIsInstalled(String value, boolean flag) {
		System.out.println(Initialization.driverAppium.isAppInstalled(value));
		if(flag==Initialization.driverAppium.isAppInstalled(value)) {
			Initialization.driverAppium.removeApp(value);
			System.out.println(""+value+": App is Removed Successfully");
		}
		else {
			System.out.println(""+value+": App is not installed in device");
		}
//		String PassStatement="PASS >> App is Installed successfully.";
//		String FailStatement="FAIL >> App is Installed successfully.";
//		ALib.AssertTrueMethod(isInstalled, PassStatement, FailStatement);
	}
	public void DeleteFileFromDevice() throws IOException {
		try
		{
			Runtime.getRuntime().exec("adb shell rm /sdcard/Enrolment-QR.jpg");
			System.out.println("File removed successfully.");
		}
		catch(Exception e) 
		{
			System.out.println("File is not Present in Device");
		}
	}
	@FindBy(xpath="//input[@id='MobileMonthlyStartDate']")
	private WebElement BillingCycleForMonth;

	@FindBy(xpath="//div[@id='monthlyConfig']/div[1]/div[2]/ul/li[1]")
	private WebElement selectDay1;

	public void SelectDateFromDatePickerForMonthly() throws InterruptedException 
	{
		try
		{
			BillingCycleForMonth.click();
			Reporter.log("Clicked on Billing Cycle", true);
			sleep(3);
			selectDay1.click();
			Reporter.log("Selected First date of the month");
			sleep(2);
		}
		catch(Exception e)
		{
			action.click(BillingCycleForMonth).build().perform();
			Reporter.log("Clicked on Billing Cycle", true);
			sleep(3);
			action.click(selectDay1).build().perform();
			Reporter.log("Selected First date of the month");
			sleep(2);
		}
	}
	public void EnterDataLimitThreshold() throws InterruptedException {
		MobileDataUsageThresholdLimit.clear();
		MobileDataUsageThresholdLimit.sendKeys(DataUsageLimit2);
		Reporter.log("Entered second Data limit", true);
		sleep(2);
	} 
	@FindBy(xpath="//select[@id='MobileThresholdLimitData']")
	private WebElement MobileThresholdLimitData;

	public void SelectDataThreshold() {
		try 
		{
			MobileThresholdLimitData.click();
			Select select=new Select(MobileThresholdLimitData);
			select.selectByVisibleText("MB");
			Reporter.log("Selected Mobile Data Usage Threshold Data", true);
		}
		catch(Exception e) {
			action.click(MobileThresholdLimitData).build().perform();
			Select select=new Select(MobileThresholdLimitData);
			select.selectByVisibleText("MB");
			Reporter.log("Selected Mobile Data Usage Threshold Data", true);
		}
		
	} 
	@FindBy(xpath="//select[@id='MobileThresholdLimitDataType']")
	private WebElement MobileThresholdLimitDataType;

	public void SelectDataTypeThreshold() {
		try
		{
			MobileThresholdLimitDataType.click();
			Select select=new Select(MobileThresholdLimitDataType);
			select.selectByVisibleText("Overall Data");
			Reporter.log("Selected Mobile Data Usage Threshold Data Type", true);
		}
		catch(Exception e) 
		{
			action.click(MobileThresholdLimitDataType).build().perform();
			Select select=new Select(MobileThresholdLimitDataType);
			select.selectByVisibleText("Overall Data");
			Reporter.log("Selected Mobile Data Usage Threshold Data Type", true);
		}
	}
	
	public void VerifyTelecomManagementStatus(String value) {
		if(TelecomStatus.getText().equals(value)) {
			Reporter.log("PASS >> Telecom Telement status: "+value+"", true);
		}
		else {
			ALib.AssertFailMethod("TelecomMangement Status is not matched");
		}
	}
	public void VerifyAlertMessageFromDeviceLimit2(String value, int waitTime) {
		
		WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@resource-id, 'textViewSubject')]")));
		String alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'textViewSubject')]").getText();
		System.out.println(alertMessage);
		boolean flag;
		if(alertMessage.contains(Config.Limit2) && alertMessage.contains(value)) {
			flag=true;
		}
		else {
			flag=false;
		}
		String PassStatement="Alert messeage is displayed in device";
		String FailStatement="Alert messeage is not displayed in device";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	@FindBy(xpath="//input[@id='MobileCustomStartDate']")
	private WebElement BillingCycle;

	@FindBy(xpath="(//table[@class='table-condensed']/thead/tr/th[2]//select[@class='monthselect'])[1]")
	private WebElement Monthselect;
	
	@FindBy(xpath="(//table[@class='table-condensed']/thead/tr/th[2]//select[@class='yearselect'])[1]")
	private WebElement Yearselect;

	public void SelectCurrentDateFromDatePickerForCustomize() throws InterruptedException 
	{
		try 
		{
			BillingCycle.click();
			Reporter.log("Clicked on Billing Cycle", true);
			sleep(2);
		}
		catch(Exception e)
		{
			action.click(BillingCycle).build().perform();
			Reporter.log("Clicked on Billing Cycle", true);
			sleep(2);
		}
		
		SimpleDateFormat dateformat=new SimpleDateFormat("MMMddyyyy");
		Date date=new Date();
		String TodaysDate = dateformat.format(date);
	
		Select select=new Select(Monthselect);
		System.out.println(TodaysDate.substring(0, 3));
		select.selectByVisibleText(TodaysDate.substring(0, 3));
		Reporter.log("Showing current month", true);
		sleep(2);
		
		Select select1=new Select(Yearselect);
		System.out.println(TodaysDate.substring(5, 9));
		select1.selectByVisibleText(TodaysDate.substring(5, 9));
		Reporter.log("Showing current year", true);
		sleep(2);
		
	     System.out.println("Today's date: "+TodaysDate.substring(3, 5)+"");
	     WebElement dateWidgetFrom = Initialization.driver.findElement(By.xpath("//table[@class='table-condensed']/tbody"));
	     List<WebElement> columns = dateWidgetFrom.findElements(By.tagName("td"));
	     for (WebElement cell: columns) {
	            if (cell.getText().equals(TodaysDate.substring(3, 5))) {
	                cell.click();
	                break;
	            }
	     }
	}
	public void ClickAnyElement() throws InterruptedException {
		
		Initialization.driver.findElement(By.xpath("//h3[text()='Configure Mobile Data Limits']")).click();
    	System.out.println("clicked");
    	sleep(2);
	}
	@FindBy(xpath="//input[@id='MobileCustomNoOfDays']")
	private WebElement MobileCustomNoOfDays;

	public void EnterNumberOfDays(String value) throws InterruptedException {
		try 
		{
			MobileCustomNoOfDays.click();
			MobileCustomNoOfDays.clear();
			MobileCustomNoOfDays.sendKeys(value);
			Reporter.log("Entered Number of days", true);
			sleep(1);
		}
		catch(Exception e)
		{
			action.click(MobileCustomNoOfDays).build().perform();
			MobileCustomNoOfDays.clear();
			MobileCustomNoOfDays.sendKeys(value);
			Reporter.log("Entered Number of days", true);
			sleep(1);
		}
	}
	@FindBy(xpath="//input[@id='warning_applyJob']")
	private WebElement warning_applyJob;

	public void CheckOnApplyJobOrProfileWarning(boolean value) throws InterruptedException
	{
		try 
		{
			boolean flag = warning_applyJob.isSelected();
			if(flag==value) {
				warning_applyJob.click();
			}
			Reporter.log("Clicked on Apply job(s)/profile(s)", true);
			sleep(2);
		}
		catch(Exception e) 
		{
			boolean flag = warning_applyJob.isSelected();
			if(flag==value) {
				action.click(warning_applyJob).build().perform();
			}
			Reporter.log("Clicked on Apply job(s)/profile(s)", true);
			sleep(2);
		}
	}
	@FindBy(xpath="//div[@id='addJob_btn']")
	private WebElement AddbutonJobTelecom;
	public void ClickOnAddButtonjobsTelecom() throws InterruptedException {
		AddbutonJobTelecom.click();
		Reporter.log("Clicked on Add button of job in Telecom", true);
		waitForXpathPresent("//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input");
		sleep(2);
	}
	@FindBy(xpath=".//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement SearchJobTelecom;
	public void SearchJob(String name) throws InterruptedException
	{
		SearchJobTelecom.sendKeys(name);
		waitForXpathPresent("//p[text()='"+name+"']");
		System.out.println("Job is searched");
		sleep(8);
	}
	@FindBy(xpath="//*[@id='CompJobDataGrid']/tbody/tr/td[2]")
	private WebElement SelectSerchedJob;

	public void SelectsearchedJob(String name) throws InterruptedException
	{
		SelectSerchedJob.click();
		waitForXpathPresent("//p[text()='"+name+"']");
		System.out.println("Searched job is Selected");
		sleep(8);
	}
	@FindBy(xpath="(//button[@id='okbtn'])[1]")
	private WebElement OkButtonAfterSearch;

	
	public void OkButtonAfterSearch() throws InterruptedException
	{
		try
		{
			OkButtonAfterSearch.click();
			Reporter.log("Clicked OK button after search", true);
			sleep(5);
		}
		catch(Exception e) {
			action.click(OkButtonAfterSearch).build().perform();
			Reporter.log("Clicked OK button after search", true);
			sleep(5);
		}
	}
	@FindBy(xpath="//div[@id='dataUsage_selJob_modal']/div[1]/div[1]/div[3]/button[1]")
	private WebElement OkButtonDevInfoJob;
	public void OkButtonDevInfoJob() throws InterruptedException
	{
		try
		{
			OkButtonDevInfoJob.click();
			Reporter.log("Clicked OK button on DeviceInfo job", true);
			sleep(5);
		}
		catch(Exception e)
		{
			action.click(OkButtonDevInfoJob).build().perform();
			Reporter.log("Clicked OK button on DeviceInfo job", true);
			sleep(5);
		}
	}
	@FindBy(xpath="//input[@id='threshold_applyJob']")
	private WebElement threshold_applyJob;
	public void CheckOnApplyJobOrProfileThreshold(boolean value) throws InterruptedException
	{
		try
		{
			boolean flag = threshold_applyJob.isSelected();
			if(flag==value) {
				threshold_applyJob.click();
			}
			Reporter.log("Clicked on Apply job(s)/profile(s)", true);
			sleep(2);
		}
		catch(Exception e) 
		{
			boolean flag = threshold_applyJob.isSelected();
			if(flag==value) {
				action.click(threshold_applyJob).build().perform();
			}
			Reporter.log("Clicked on Apply job(s)/profile(s)", true);
			sleep(2);
		}
	}
	@FindBy(xpath="//label[contains(text(),'"+Config.Zip_Filename1+"')]")
	private WebElement VerifyFileInRemote;
	public void VerifyFileNameInRemoteSupport() throws InterruptedException {
		boolean flag;
		try 
		{
			flag = VerifyFileInRemote.isDisplayed();
		}
		catch (Exception e) 
		{
			System.out.println("catched");
		    flag=false;
		}
		String Pass="PASS >>> Job is successfully installed on Device";
		String Fail="FAIL >>> Job is not installed on Device";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		sleep(2);
	}
	String alertMessage;
	public void VerifyAlertMessageFromDeviceLimit2ApplyJob(String value, int waitTime) 
	{
		WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, waitTime);
		boolean flag;
		try 
		{
			wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/cancel_button")));
			Initialization.driverAppium.findElementById("com.android.packageinstaller:id/ok_button").click();
			wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
			sleep(4);
			Initialization.driverAppium.findElementById("com.android.packageinstaller:id/done_button").click();
			sleep(2);
			alertMessage = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'textViewSubject')]").getText();
			if(alertMessage.contains(Config.Limit2) && alertMessage.contains(value)) {
				flag=true;
			}
			else {
				flag=false;
			}
		}
		catch(Exception e) 
		{
			flag=false;
		}
//		String 
		String PassStatement=""+alertMessage+": Alert messeage is displayed.";
		String FailStatement=""+alertMessage+": Alert messeage is not displayed.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	
}
