package PageObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class ProfilesPOM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(xpath="//a[text()='Profiles']")
	private WebElement Profiles;
	
	@FindBy(id="addPolicyBtn")
	private WebElement AddProfile;
	
	@FindBy(id="editPolicyBtn")
	private WebElement EditProfile;
	
	@FindBy(id="deletePolicyBtn")
	private WebElement DeleteProfile;
	
	@FindBy(id="setDefaultPolicy")
	private WebElement SetAsDefaultProfile;
	
	@FindBy(id="enrollafwaccount")
	private WebElement EnrollAFW;
	
	@FindBy(id="managedGooglePlayAccount")
	private WebElement EnrollAFWUsingGmail;
	
	@FindBy(id="enrollgoogleplayaccount")
	private WebElement Enroll;
	
	@FindBy(xpath="//button[@id='saveAfwProfile']/preceding-sibling::button")
	private WebElement AndroidProfileBackButton;
	
	@FindBy(id="selectButtoniOS")
	private WebElement iOSProfile;
	
	@FindBy(xpath="//button[@id='saveiOSProfile']/preceding-sibling::button")
	private WebElement iOSProfileBackButton;
	
	@FindBy(id="selectButtonWindows")
	private WebElement WindowsProfile;
	
	@FindBy(xpath="//button[@id='saveWindowsProfile']/preceding-sibling::button")
	private WebElement WindowsProfileBackButton;
	
	@FindBy(xpath="//table[@id='policyTable']/tbody/tr[1]")
	private WebElement ProfileFirstRow;
	
	@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement DeleteProfileNoButton;
	
	@FindBy(xpath="//h4[text()='AFW Enrollment']/preceding-sibling::button")
	private WebElement EnrollAFWCloseButton;
	
	
	public void ClickOnProfile() throws InterruptedException
	{
		Profiles.click();
		waitForidPresent("addPolicyBtn");
		Reporter.log("Clicking On Profiles, Navigated to Profiles Page",true);
		sleep(5);
	}
	
	public void ClickOniOSProfile() throws InterruptedException
	{
		iOSProfile.click();
		waitForidPresent("addPolicyBtn");
		Reporter.log("Clicking On iOS Profiles, Navigated to iOS Profiles Page",true);
		sleep(5);
	}
	
	
	public void ClickOnWindowsProfile() throws InterruptedException
	{
		WindowsProfile.click();
		waitForidPresent("addPolicyBtn");
		Reporter.log("Clicking On Windows Profiles, Navigated to Windows Profiles Page",true);
		sleep(10);
	}
	
	public void ClickOnAddProfile_UM()
	{
		AddProfile.click();
	}
	
	public void ClickOnAddProfile() throws InterruptedException
	{
		AddProfile.click();
		waitForidPresent("policyName");
		sleep(3);
	}
	
	public void ClickOnEditProfile_UM()
	{
		EditProfile.click();
	}
	
	public void ClickOnEditProfile() throws InterruptedException
	{
		EditProfile.click();
		waitForidPresent("policyName");
		sleep(3);
	}
	
	public void ClickOnDeleteProfile_UM()
	{
		DeleteProfile.click();
	}
	
	public void ClickOnDeleteProfile() throws InterruptedException
	{
		DeleteProfile.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]");
		sleep(3);
	}
	
	public void ClickOnDeleteProfileNoButton() throws InterruptedException
	{
		DeleteProfileNoButton.click();
		waitForidPresent("addPolicyBtn");
		sleep(5);
	}
	
	public void ClickOnBackButtonAndroidProfile() throws InterruptedException
	{
		AndroidProfileBackButton.click();
		waitForidPresent("addPolicyBtn");
		sleep(5);
	}
	
	public void ClickOnBackButtoniOSProfile() throws InterruptedException
	{
		iOSProfileBackButton.click();
		waitForidPresent("addPolicyBtn");
		sleep(5);
	}
	
	public void ClickOnBackButtonWindowsProfile() throws InterruptedException
	{
		WindowsProfileBackButton.click();
		waitForidPresent("addPolicyBtn");
		sleep(5);
	}
	
	public void ClickOnSetAsDefaultProfile() throws InterruptedException
	{
		SetAsDefaultProfile.click();
	}
	
	public void ClickOnEnrollAFW() throws InterruptedException
	{
		EnrollAFW.click();
		waitForXpathPresent("//h4[text()='AFW Enrollment']/preceding-sibling::button");
		sleep(5);
	}
	
	public void ClickOnEnrollAFWUsingGmail() throws InterruptedException
	{
		EnrollAFWUsingGmail.click();
	}
	
	public void ClickOnEnroll() throws InterruptedException
	{
		Enroll.click();
	}
	
	public void ClickOnEnrollAFWCloseButton() throws InterruptedException
	{
		EnrollAFWCloseButton.click();
		waitForidPresent("addPolicyBtn");
		sleep(5);
	}
	
	public void ClickOnFirstRowOfProfile_UM() throws InterruptedException
	{
		ProfileFirstRow.click();
		Reporter.log("Clicking On First Row Of Profiles",true);
		sleep(3);
	}
	
	public boolean CheckProfilesBtn()
	{
		boolean value = true;
		try{
			Profiles.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	@FindBy(id="selectButtonLinux")
	private WebElement LinuxProfiles;
	
	@FindBy(id="addPolicyBtn")
	private WebElement AddProfileButton;
	
	@FindBy(id="blacklisturlLinuxConfig")
	private WebElement BlackListURLConfigbutton_Linux;
	
	@FindBy(id="addblacklisturlBtnLinux")
	private WebElement AddURLButton_Linux;
	
	@FindBy(id="blacklisturlNameLinux")
	private WebElement BlockListURLName_Linux;
	
	@FindBy(id="blacklisturlLinux")
	private WebElement BlackListURL_Linux;
	
	@FindBy(xpath="//div[@id='blacklisturlPopupLinux']//button[text()='Add']")
	private WebElement 	AddURLNameAddButton;
	
	@FindBy(id="policyNameLinux")
	private WebElement ProfileNameTextField;
	
	@FindBy(id="saveLinuxProfile")
	private WebElement LinuxProfileSaveButton;
	
	@FindBy(xpath="(//div[@id='profileList_tableCont']//input[@class='form-control'])[1]")
	private WebElement SearchCreatedProfile;
	
	@FindBy(id="deletePolicyBtn")
	private WebElement DeleteProfileButton;
	
	@FindBy(id="setDefaultPolicy")
	private WebElement SetAsDefaultButton;
	
	@FindBy(id="selectButtonmacOS")
	private WebElement MacOSProfile;
	
	@FindBy(id="restrictionmacOSProfileConfig")
	private WebElement RestrictionProfileConfigurationButton;
	
	@FindBy(id="policyNameMacOS")
	private WebElement ProfileNameTextField_MacOs;
	
	@FindBy(id="saveMacOSProfile")
	private WebElement MacOsprofileSavebutton;
	
	public void ClickOnLinuxProfiles() throws InterruptedException
	{
		LinuxProfiles.click();
		waitForidPresent("addPolicyBtn");
		sleep(3);
		Reporter.log("Clicked On Linux Profile",true);
	}
	
	public void ClickOnAddButton_Linux() throws InterruptedException
	{
		AddProfileButton.click();
		waitForidPresent("policyNameLinux");
		sleep(2);
		Reporter.log("Clicked On Add Profile Button",true);
	}
	
	public void ClickOnProfileType(String ProfileType) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='policy_item_box']//span[text()='"+ProfileType+"']")).click();
		sleep(2);
	}
	
	public void ClickOnBlockListURLConfigButton() throws InterruptedException
	{
		BlackListURLConfigbutton_Linux.click();
		waitForidPresent("addblacklisturlBtnLinux");
		sleep(1);
	}
	
	public void ClickOnAddBlackURLList() throws InterruptedException
	{
		AddURLButton_Linux.click();
		waitForidPresent("blacklisturlNameLinux");
		sleep(2);
	}
	
	public void SendingURL_Name_Linux(String Name,String URL) throws InterruptedException
	{
		BlockListURLName_Linux.sendKeys(Name);
		sleep(1);
		BlackListURL_Linux.sendKeys(URL);
	}
	
	public void ClickOnNameURLAddButton_Linux() throws InterruptedException
	{
		AddURLNameAddButton.click();
		sleep(2);
	}
	
	public void SendingLinuxProfileName(String profileName) throws InterruptedException
	{
		ProfileNameTextField.sendKeys(profileName);
		sleep(2);
	}
	
	public void ClickOnLinuxProfileSaveButton() throws InterruptedException
	{
		LinuxProfileSaveButton.click();
		waitForXpathPresent("//span[text()='Profile created successfully.']");
		sleep(2);
	}
	
	public void Searchprofile(String ProfileName) throws InterruptedException
	{
		SearchCreatedProfile.sendKeys(ProfileName);
		waitForXpathPresent("//table[@id='policyTable']//td[text()='"+ProfileName+"']");
	    sleep(2);
		Initialization.driver.findElement(By.xpath("//table[@id='policyTable']//td[text()='"+ProfileName+"']")).click();
		sleep(2);
	}
	
	public void ClickOnProfiledeleteButton() throws InterruptedException
	{
		DeleteProfileButton.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']//button[text()='Yes']");
		sleep(2);
	}
	
	public void ClickOnDeleteProfileYesButton()
	{
		Initialization.driver.findElement(By.xpath("//div[@id='ConfirmationDialog']//button[text()='Yes']")).click();
	    waitForXpathPresent("//span[text()='Profile deleted successfully.']");
	}
	
	public void ClickOnSetAsDefaultButton(String Profilename) throws InterruptedException
	{
		SetAsDefaultButton.click();
		waitForXpathPresent("//span[text()='Changed default profile. The default profile is now "+"\""+Profilename+"\".']");
		sleep(2);
	}
	
	public void ClikconMacOSProfiles() throws InterruptedException
	{
		MacOSProfile.click();
		waitForidPresent("addPolicyBtn");
		sleep(3);
		Reporter.log("Clicked On macOS Profile",true);
	}
	
	public void ClickOnAddButton_MacOS() throws InterruptedException
	{
		AddProfileButton.click();
		waitForidPresent("policyNameMacOS");
		sleep(2);
		Reporter.log("Clicked On MacOS Add Profile Button",true);
	}
	
	public void ClickONRestrictionProfileConfigureButton() throws InterruptedException
	{
		RestrictionProfileConfigurationButton.click();
		waitForidPresent("saveMacOSProfile");
		sleep(2);
	}
	
	public void SendingMacOsProfileName(String ProfileName) throws InterruptedException
	{
		ProfileNameTextField_MacOs.sendKeys(ProfileName);
		sleep(2);
	}
	
	public void ClickOnmacOSprofileSaveButton() throws InterruptedException
	{
		MacOsprofileSavebutton.click();
		waitForXpathPresent("//span[text()='Profile created successfully.']");
		sleep(2);
	}
	
	
	
}
