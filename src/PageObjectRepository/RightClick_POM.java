package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.seleniumhq.jetty9.server.handler.ContextHandler.AliasCheck;
import org.testng.Assert;
import org.testng.Reporter;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;

public class RightClick_POM extends WebDriverCommonLib {
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(xpath="//li[@data-action='refresh']")
	private WebElement refreshoptionRightClick;
	
	@FindBy(xpath="//li[@data-action='remote']")
	private WebElement remoteOptionRightClick;
	
	@FindBy(xpath="(//span[text()='Move to Group'])[2]")
	private WebElement MovetoGroupRightClickSingleDevice;
	
	@FindBy(xpath="(//span[text()='Move to Group'])[1]")
	private WebElement MovetoGroupRightClickMultipleDevice;
	
	@FindBy(xpath="(//span[text()='Push Application'])[2]")
	private WebElement PushApplicationRightClick;
	
	@FindBy(xpath="(//li[@data-action='sl_rc'])[2]")
	private WebElement SureLockRightClick;
	
	@FindBy(xpath="(//li[@data-action='sf_rc'])[2]")
	private WebElement SureFoxRightClick;
	
	@FindBy(id="browseFiles")
	private WebElement BrowsefileBtn;
	
	@FindBy(xpath="(//input[@value='Next'])[1]")
	private WebElement NextBtnPushApp;
	
	@FindBy(xpath="//h5[text()='Success']")
	private WebElement SuccessMsg;
	
	@FindBy(xpath="(//button[@class='close'])[1]")
	private WebElement CloseBtnPushFilePopUp;
	
	@FindBy(id="job_filepath_input")
	private WebElement UrlTextField;
	
	@FindBy(xpath="//input[@value='Add']")
	private WebElement AddbtnUrl;
	
	@FindBy(xpath="(//span[text()='Push File'])[2]")
	private WebElement PushFileRightClick;
	
	@FindBy(xpath="(//span[text()='Install'])[3]")
	private WebElement InstallOptionSL;
	
	@FindBy(xpath="(//span[text()='Install'])[1]")
	private WebElement InstallOptionSF;
	
	@FindBy(xpath="(//span[text()='Refresh'])[1]")
	private WebElement RefreshBtn;
	
	@FindBy(xpath="(//span[text()='Reboot'])[2]")
	private WebElement Reboot;
	
	@FindBy(xpath="(//span[text()='Delete'])[2]")
	private WebElement Delete;
	
	@FindBy(xpath="(//span[text()='Activate License'])[3]")
	private WebElement ActivateLicenseOptionSL;
	
	@FindBy(xpath="//span[text()='Launch SureLock']")
	private WebElement LaunchSureLockOptionSL;
	
	@FindBy(xpath="(//span[@class='txt' and text()='Deactivate License'])[3]")
	private WebElement SureLockDeactivateLicenseoption;
	
	@FindBy(xpath="//span[@class='txt' and text()='Exit SureLock']")
	private WebElement ExitSureLockoption;
	
	//@FindBy(id="refreshButton")
	@FindBy(xpath="//div[@id='refreshButton']/i")
	private WebElement refreshdeviceinfo;
	
	@FindBy(id="activation_code")
	private WebElement ActvationCodeTextFieldSL;
	
	@FindBy(id="activationcode_okbtn")
	private WebElement ActvationCodeOkbtn;
	
	@FindBy(xpath="(//span[text()='Upgrade'])[1]")
	private WebElement UpgradeOptionSF;
	
	@FindBy(xpath="(//span[text()='Activate License'])[1]")
	private WebElement ActivateLicenseOptionSF;
	
	@FindBy(xpath="//span[text()='Launch SureFox']")
	private WebElement LaunchSureLockOptionSF;
	
	
	
	@FindBy(xpath="//span[text()='SureFox Settings']")
	private WebElement SFSettingsJobs;
	
	@FindBy(id="surefoxAllowedWebsite")
	private WebElement surefoxAllowedWebsite;
	
	@FindBy(id="SaveasBtn")
	private WebElement SaveBtnSureFoxSettingsInJobs;	
	
	@FindBy(id="SaveBtnSureFoxSettingsInJobs")
	private WebElement JobNameTF;
	
	@FindBy(xpath="(//button[text()='Save'])[1]")
	private WebElement SaveBtnJobDetails;
	
	@FindBy(xpath="(//span[text()='Deactivate License'])[1]")
	private WebElement DeactivateOptionSureFox;
	
	@FindBy(id="applyJob_okbtn")
	private WebElement ApplyButtonJobWindow;
	
	@FindBy(id="activation_code_pswd")
	private WebElement ExitSureLockPWTF;
	
	@FindBy(id="activationcode_okbtn")
	private WebElement OkBtnExitSureLockCode;
	
	@FindBy(id="activation_code")
	private WebElement ActivationCodeSureLockTF;
	
	@FindBy(xpath="(//span[text()='Refresh'])[1]")
	private WebElement RefreshButtonRightClick;
	
	@FindBy(xpath="//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']")
	private WebElement SuccesfullyDeployedMsg;
	
	@FindBy(linkText="Quick Action")
	private WebElement QuickActionJobtab;
	
	@FindBy(xpath="//tr[@data-index='0']/td[p[text()='Refresh']]/following-sibling::td/p[text()='Successful']")
    private WebElement SucessfulIndicatorRefreshJob;
	
	@FindBy(xpath="(//span[text()='Remote'])[1]")
	private WebElement RemoteBtnRightClick;
	
	@FindBy(xpath="//div[@id='fountainG']")
	private WebElement LoadingBarOfRemoteSupport;
	
	@FindBy(xpath="//label/input")
	private WebElement FolderNameTitle;

	@FindBy(xpath="//li[@title='Reload']")
	private WebElement Reload;
	
	@FindBy(xpath="(//button[@class='close'])[1]")
	private WebElement CloseBtnJobWindow;
	
	@FindBy(id="newGroup")
	private WebElement NewGroupbtn;
	
	@FindBy(xpath="//input[@class='form-control ct-model-input  ct-select-ele']")
	private WebElement TextFiledNewGroup;
	
	@FindBy(xpath ="//button[@id='addingGroupokbtn']")
	private WebElement NewGroupsavebtn;
	
	@FindBy(xpath="//li[@class='list-group-item node-groupstree' and text()='Home']")
	private WebElement HomeGroup;
	
	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;
	
	@FindBy(xpath="(//li[@data-nodeid='0'])[3]")
	private WebElement HomeElement;
	
	@FindBy(xpath="(//span[.='Move to Group'])[2]")
	private WebElement Movetogroup;
	
	@FindBy(xpath="//*[@id=\"groupList\"]/ul/li[56]/span[2]")
	private WebElement ExpandGroupAutomation;
	
	@FindBy(xpath="//*[@id='groupList']/ul/li[text()='Automation']")
	private WebElement AutomationGroup;
	
	@FindBy(xpath="//button[.='Move']")
	private WebElement MoveBtnGroupListPopUp;
	
	@FindBy(xpath="(//input[@placeholder='Search By Group Name'])[2]")
	private WebElement SearchGroupTF;
	
	@FindBy(xpath="//*[@id='groupList']/ul/li[text()='AutoSubGroup']")
	private WebElement SubGroupvinod;
	
	@FindBy(id="deleteGroup")
	private WebElement deleteGroupBtn;
	
	@FindBy(xpath="//a[text()='Yes, delete ["+Config.GroupName+"] along with associated devices and QR codes.']")
	private WebElement deletegroupalongwithalldevicesoption;
	
	@FindBy(xpath="//a[text()='Yes, but first move all devices in ["+Config.GroupName+"] to the Home group. Then delete ["+Config.GroupName+"] and QR codes.']")
	private WebElement Movealldevicestohomegroupthendeletegroupoption;
	
	@FindBy(xpath="(//span[text()='Delete'])[2]")
	private WebElement DeleteRightClick;
	
	@FindBy(xpath="//div[@id='deviceConfirmationDialog']//button[text()='Yes']")
    private WebElement YesBtnDeletedevicePopUp;
	
	@FindBy(xpath="//a[@href='#tagsListMainBlock']")
	private WebElement Tag;
	
	@FindBy(id="newGroup")
    private WebElement NewTag;	
	
	@FindBy(xpath="//button[@name='refresh']")
	private WebElement RefreshBtnDevicegrid;
	
	@FindBy(xpath="(//span[text()='Upgrade'])[3]")
	private WebElement UpgradeOptionSL;
	
	@FindBy(xpath="(//span[text()='SureVideo'])[2]")
    private WebElement SureVideoRightclick;	
	
	@FindBy(xpath="(//span[text()='Install'])[2]")
	private WebElement InstallOptionSureVideo;
	
	@FindBy(xpath="(//span[text()='Upgrade'])[2]")
	private WebElement UpgradeOptionSureVideo;
	
	@FindBy(xpath="(//span[text()='Activate License'])[2]")
	private WebElement ActvateLicenseSureVideo;
	
	@FindBy(xpath="(//span[text()='Deactivate License'])[2]")
	private WebElement DeactivateLicenseOptionSureVideo;
	
	@FindBy(xpath="(//span[text()='Launch SureVideo'])[1]")
	private WebElement LaunchOptionSureVideo;
	
	@FindBy(id="activation_code")
	private WebElement ActivationCodeTfSureVideo;
	
	@FindBy(id="activation_code_pswd")
	private WebElement ActivationCodepwTfSureVideo;
	
	@FindBy(id="activationcode_okbtn")
	private WebElement ActivationCodeOkBtnsureVideo;
	
	@FindBy(id="activation_code_pswd")
	private WebElement DeactivationPwdTfSureVideo;
	
	@FindBy(id="activationcode_okbtn")
	private WebElement DeactivationPwdOkSureVideo;
	
	@FindBy(id="job_filepath_input")
	private WebElement PushFileFilepathTF;
	
	@FindBy(xpath="//input[@value='Add']")
	private WebElement AddBtnPushFileUpload;
	
	@FindBy(id="groupProperties")
	private WebElement GroupPropertiesbtn;
	
	@FindBy(id="input_totDev")
	private WebElement TotalDevicesInGroup;
	
	@FindBy(id="grouppropertiesokbtn")
	private WebElement CloseBtnGroupPropertiesPoUp;
	
	@FindBy(xpath="(//li[.='Automation'])[3]")
	private WebElement AutomationGroupGroupList;
	
	@FindBy(id="input_totSubGrp")
	private WebElement SubGroupValue;
	
	@FindBy(id="input_totDevInGrp")
	private WebElement CurrentGroupDev;
	
	Actions action=new Actions(Initialization.driver);
	JavascriptExecutor js=(JavascriptExecutor)Initialization.driver;
	
	public void RightClickOndevice(String name) throws InterruptedException {
		action.contextClick(Initialization.driver.findElement(By.xpath("//p[text()='"+name+"']"))).perform();
		sleep(4);
	}

	public void ClickOnRefresh() throws InterruptedException {
		RefreshButtonRightClick.click();
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {};
		sleep(2);
	}
	
	public void VerifyRefresJobDeployedSuccesfully() throws InterruptedException {
		SuccesfullyDeployedMsg.click();
		waitForidPresent("jobqueuetitle");
		sleep(2);
		QuickActionJobtab.click();
		waitForidPresent("jobQueueRefreshDynamicBtn");
		sleep(2);
		boolean status= SucessfulIndicatorRefreshJob.isDisplayed();
		String pass="PASS >> 'RefreshJob' Successfully Deployed";
		String fail="FAIL >> 'RefreshJob' Not Deployed";
		ALib.AssertTrueMethod(status, pass, fail);
		CloseBtnJobWindow.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}
	
	public void VerifyClickOnRemote() {
		RemoteBtnRightClick.click();	
	}
	
	public String parentwinID;
	public String childwinID;
	public String URL;
	
	public void windowhandles()
	{
		Set<String> set=Initialization.driver.getWindowHandles();
	    Iterator<String> id=set.iterator();
	    parentwinID =id.next();
	    childwinID =id.next();
	}
	
	public void SwitchToRemoteWindow(){
		Initialization.driver.switchTo().window(childwinID);
		URL=Initialization.driver.getCurrentUrl();
	}
	
	public void VerifyOfRemoteSupportLaunched() throws InterruptedException{
		while(LoadingBarOfRemoteSupport.isDisplayed()){}
		waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
		sleep(8);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Reporter.log("Remote support launched "+dateFormat.format(date),true);
	}
	
	public void DeviceNameVerification(String deviceName )
	{
		boolean devicenamevalue = Initialization.driver.findElement(By.xpath("//div[text()='Device: "+deviceName+"']")).isDisplayed();
		String PassStatement1 = "PASS >> device name is displayed as expected ";
		String FailStatement1 = "FAIL >> device name is not displayed as expected";
		ALib.AssertTrueMethod(devicenamevalue, PassStatement1, FailStatement1);
	}
	
	public void resumeButtonDisplayed()
	{
		boolean value=true;
		try{
		Initialization.driver.findElement(By.xpath(".//*[@id='dimiss_pause']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		Assert.assertTrue(value,"FAIL: 'Resume button not displayed'");
		  Reporter.log("PASS>> 'Resume button is displayed",true );	
	}
	
	public void SwitchToPrentwindow() {
		
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		URL=Initialization.driver.getCurrentUrl();
	}
	
	public void ClickonNewGroup() throws InterruptedException {
		NewGroupbtn.click();
        waitForXpathPresent("//h4[.='Add New Group']");
        sleep(2);
	}
	
	public void CreateNewGroup(String Groupname) throws InterruptedException {
		TextFiledNewGroup.sendKeys(Groupname);
		NewGroupsavebtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
	}
	
	public void  CreatedGroupisEmpty() throws InterruptedException 
	{
		GroupPropertiesbtn.click();
		waitForidPresent("pingallgcmdevicesbtn");
		sleep(2);
		String Actual=TotalDevicesInGroup.getAttribute("value");
		String Expected="0";
		String PassStatement1 = "PASS >> Group Has "+Actual+" devices";
		String FailStatement1 = "FAIL >> Group Has Devices";
		ALib.AssertEqualsMethod(Expected, Actual, PassStatement1, FailStatement1);
		CloseBtnGroupPropertiesPoUp.click();
		waitForidClickable("deleteDeviceBtn");
		sleep(4);
	}
	
	public void VerifyCreatingSubGroup(String GroupName) throws InterruptedException {
		ClickonNewGroup();
		CreateNewGroup(GroupName);
	}
	
	public void MoveToGroup(String grp) throws InterruptedException {
		MovetoGroupRightClickSingleDevice.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys(grp);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='"+grp+"']");
	    Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='"+grp+"']")).click();
	    sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(4);
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(10);
		Reporter.log("Devices moved successfully",true);
	}
	
	public void VerifyGroupPresence(String Groupname) throws InterruptedException
	{
		try {
			Initialization.driver.findElement(By.xpath("(//li[text()='"+Groupname+"'])[last()]")).isDisplayed();
			Reporter.log("Group is already created.",true);
		}catch (Exception e) {
			ClickonNewGroup();
			TextFiledNewGroup.sendKeys(Groupname);
			NewGroupsavebtn.click();
			waitForidPresent("deleteDeviceBtn");
			Reporter.log("Group created.",true);
			sleep(5);
		}
	}
	public void VerifyDeviceMoved() throws InterruptedException 
	{
		GroupPropertiesbtn.click();
		waitForidPresent("pingallgcmdevicesbtn");
		sleep(2);
		String Actual=TotalDevicesInGroup.getAttribute("value");
		String Expected="1";
		String PassStatement1 = "PASS >> Group Has "+Actual+" devices";
		String FailStatement1 = "FAIL >> Group Has Devices";
		ALib.AssertEqualsMethod(Expected, Actual, PassStatement1, FailStatement1);
		CloseBtnGroupPropertiesPoUp.click();
		waitForidClickable("deleteDeviceBtn");
		sleep(7);
	}
	
	public void VerifyCreatedGroupAndSubGroupProperties() throws InterruptedException {
		AutomationGroupGroupList.click();
		sleep(2);
		GroupPropertiesbtn.click();
		waitForidPresent("pingallgcmdevicesbtn");
		sleep(2);
		String Actual=TotalDevicesInGroup.getAttribute("value");
		String Expected="2";
		String PassStatement1 = "PASS >> Group Has "+Actual+" devices";
		String FailStatement1 = "FAIL >> Group Has Devices";
		ALib.AssertEqualsMethod(Expected, Actual, PassStatement1, FailStatement1);
		
		String Actual1=SubGroupValue.getAttribute("value");
		String Expected1="1";
		String PassStatement2 = "PASS >> SubGroup has "+Actual1+" devices";
		String FailStatement2 = "FAIL >> SubGroup Has Devices";
		ALib.AssertEqualsMethod(Expected1, Actual1, PassStatement2, FailStatement2);
		
		String Actual2=CurrentGroupDev.getAttribute("value");
		String Expected2="1";
		String PassStatement3 = "PASS >> Current Group Has "+Actual2+" devices";
		String FailStatement3 = "FAIL >> Current Group Has Has no Devices";
		ALib.AssertEqualsMethod(Expected2, Actual2, PassStatement3, FailStatement3);
		CloseBtnGroupPropertiesPoUp.click();
		sleep(3);
	}
	

	public void VerifyMultipleDevicesAdded() throws InterruptedException {
		GroupPropertiesbtn.click();
		waitForidPresent("pingallgcmdevicesbtn");
		sleep(2);
		String Actual=TotalDevicesInGroup.getAttribute("value");
		String Expected="2";
		String PassStatement1 = "PASS >> Group Has "+Actual+" devices";
		String FailStatement1 = "FAIL >> Group Has Devices";
		ALib.AssertEqualsMethod(Expected, Actual, PassStatement1, FailStatement1);
		String Actual2=CurrentGroupDev.getAttribute("value");
		String Expected2="2";
		String PassStatement3 = "PASS >> Current Group Has "+Actual2+" devices";
		String FailStatement3 = "FAIL >> Current Group Has Has no Devices";
		ALib.AssertEqualsMethod(Expected2, Actual2, PassStatement3, FailStatement3);
		CloseBtnGroupPropertiesPoUp.click();
		sleep(3);
	}
	
	

	public void ScrollToHome() throws InterruptedException {
		js.executeScript("arguments[0].scrollIntoView();",HomeElement);
		waitForidClickable("deleteDeviceBtn");
		sleep(6);
	}
    
	public void SearchDevice(String devicename) throws InterruptedException {
		HomeGroup.click();
		waitForidClickable("deleteDeviceBtn");
		sleep(6);
		SearchDeviceTextField.sendKeys(devicename);
		waitForidClickable("deleteDeviceBtn");
		sleep(6);
	}
	
	public void SearchDeviceInHome(String devicename) throws InterruptedException {
		SearchDeviceTextField.sendKeys(devicename);
		sleep(4);
		waitForXpathPresent("//p[text()='"+devicename+"']");
		waitForXpathPresent("//p[text()='"+devicename+"']");
		sleep(4);
	}
	
	public void MoveToGroup() throws InterruptedException {
		MovetoGroupRightClickSingleDevice.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys("Automation");
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='Automation']");
	    AutomationGroup.click();
	    sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(4);
	}
	
	public void MoveToGroupMultiple() throws InterruptedException {
		MovetoGroupRightClickMultipleDevice.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys("Automation");
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='Automation']");
	    AutomationGroup.click();
	    sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(4);
	}
	
	public void MoveToGroupMultiple(String GroupName) throws InterruptedException {
		MovetoGroupRightClickMultipleDevice.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(2);
		SearchGroupTF.sendKeys(GroupName);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='"+GroupName+"']");
	    Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='"+GroupName+"']")).click();
	    sleep(2);
		MoveBtnGroupListPopUp.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(4);
	}
	public void MoveToSubGroup(String GroupName) throws InterruptedException {
		Movetogroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(3);
		SearchGroupTF.sendKeys(GroupName);
		waitForXpathPresent("//*[@id='groupList']/ul/li[text()='"+GroupName+"']");
		Initialization.driver.findElement(By.xpath("(//li[text()='"+GroupName+"'])[1]")).click();
	    MoveBtnGroupListPopUp.click();
	    sleep(2);
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(4);
	}
	
	public void ClickOnGroup(String GroupName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul//li[text()='"+GroupName+"']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}
	
	public void ClickOnDeleteGroup(String GroupName) throws InterruptedException {
		deleteGroupBtn.click();
		waitForXpathPresent("//p[text()='Are you sure you want to delete ["+GroupName+"]?']");
		sleep(3);
	}
	
	public void DeleteGroupByMovingDeviceToHome() throws InterruptedException 
	{
		boolean bln=deletegroupalongwithalldevicesoption.isDisplayed();
		String pass="PASS >> 'Yes, delete group along with all devices.' option is present";
		String Fail= "FAIL >> 'Yes, delete group along with all devices.' option is Not present";
		ALib.AssertTrueMethod(bln, pass, Fail);
		
		boolean bln1=Movealldevicestohomegroupthendeletegroupoption.isDisplayed();
		String pass1="PASS >> 'Move all devices to home group then delete group.' option is present";
		String Fail1= "FAIL >> 'Move all devices to home group then delete group.' option is Not present";
		ALib.AssertTrueMethod(bln1, pass1, Fail1);
		
		Movealldevicestohomegroupthendeletegroupoption.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed()) {}
		waitForidClickable("deleteDeviceBtn");
		sleep(4);
	}
	
	public void GroupDeletedSuccessfullyMsg() throws InterruptedException {
		boolean bln=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'deleted successfully.')]")).isDisplayed();
		}catch (Exception e) {
			bln=false;
		}
		ALib.AssertTrueMethod(bln,"PASS >> Group deleted successfully.. Message displayed","Fail >> Group deleted successfully. Message not displayed");
		sleep(8);
	}
	
	public void SelectMultipleDevice() throws AWTException, InterruptedException {
		WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
 		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
 		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
 		sleep(2);
	}
	
	public void DeleteDeviceFromRightClick() throws InterruptedException
	{
        YesBtnDeletedevicePopUp.click();
        waitForidPresent("deleteDeviceBtn");
        sleep(2);
	}
	
	public void VerifyAfterDeletingDevice() {
		boolean value = Initialization.driver.findElement(By.xpath("//div[.='No device available in this group.']")).isDisplayed();
		String PassStatement1 = "PASS >> device deleted";
		String FailStatement1 = "FAIL >> device not deleted";
		ALib.AssertTrueMethod(value, PassStatement1, FailStatement1);
	}
	
	public void ClickOnTag() throws InterruptedException {
		Tag.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed()) {
		}
		waitForidPresent("newGroup");
		sleep(2);
	}
	
	public void ClickonPushApplication() throws InterruptedException {
		PushApplicationRightClick.click();
		waitForXpathPresent("//h4[text()='Push Application']");
		sleep(2);
	}

	public void UploadAPK(String ApkFile,String Appname) throws IOException, InterruptedException {
		BrowsefileBtn.click();
		sleep(8);
		Runtime.getRuntime().exec(ApkFile);
		waitForXpathPresent("//span[text()='"+Appname+"']");
		sleep(4);
	}
	
	public void ClickOnNextBtnPushApplicationPopup() throws InterruptedException {
		NextBtnPushApp.click();
		while(Initialization.driver.findElement(By.xpath("//div[text()='Loader']")).isDisplayed()) 
		{}
		sleep(2);
		while(Initialization.driver.findElement(By.xpath("//div[text()='Device is downloading application']")).isDisplayed()) 
		{}
		sleep(2);
	}
	
	public void VerifyingSuccessMsg() {
		boolean status=SuccessMsg.isDisplayed();
		String Pass="PASS >> 'Sucess' Message is displayed after uploading pushfile";
		String fail="FAIL >> 'Sucess' Message is not displayed after uploading pushfile";
		ALib.AssertTrueMethod(status, Pass, fail);
	}
	
	public void ClickOnCloseBtnpushFilePopUp() throws InterruptedException {
		CloseBtnPushFilePopUp.click();
		//waitForXpathPresent("//p[text()= "+Config.DeviceName+"]");
		sleep(5);
	}
	
	public void PushApplicationURL(String url) throws InterruptedException {
		UrlTextField.sendKeys(url);
		sleep(2);
		AddbtnUrl.click();
		//waitForXpathPresent("//span[text()='https://mars.42gears.com/support/inout/surevideo.apk']");
		sleep(2);
	}
	
	public void ClickOnPushFile() throws InterruptedException {
		PushFileRightClick.click();
		waitForXpathPresent("//h4[text()='Push File']");
		sleep(2);
	}
	
	public void UploadFile(String Filepath,String FileName) throws IOException, InterruptedException {
		BrowsefileBtn.click();
		sleep(8);
		Runtime.getRuntime().exec(Filepath);
		waitForXpathPresent("//span[text()='"+FileName+"']");
		sleep(2);
	}
	
	public void ClickOnReboot() throws InterruptedException {
		Reboot.click();
		sleep(2);
	}
	
	public void PushFileUploadFilePath() throws InterruptedException {
		PushFileFilepathTF.sendKeys("https://mars.42gears.com/support/inout/surelock.apk");
		AddBtnPushFileUpload.click();
		waitForXpathPresent("//span[text()='https://mars.42gears.com/support/inout/surelock.apk']");
		sleep(2);
	}
	
	public void ClickOnDelete() throws InterruptedException {
		Delete.click();
		Reporter.log("Device Deleted Successfully");
		sleep(5);
		
	}
	
	@FindBy(xpath="")
	private WebElement WarningMessageOnDeviceDelete;
	
	public void WarningMessageOnDeviceDelete(String DeviceName)
	{
		try {
			Initialization.driver.findElement(By.xpath("//p[contains(text(),'The device named "+"\""+DeviceName+"\" will be deleted from SureMDM, and you will no longer be able to remotely access the device.')]")).isDisplayed();
			Reporter.log("PASS >> Warning message on device delete from right click is displayed",true);
			sleep(2);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Warning message on device delete from right click is Not Displayed");
		}
	}
	
	@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement NoButtonDeleteDeviceDialog;
	
	public void ClickOnNoButtonDeleteDeviceDialog() throws InterruptedException
	{
		NoButtonDeleteDeviceDialog.click();
		sleep(2);
	}
	
	public void ClickOnNextBtnPushFilePopup() throws InterruptedException {
		NextBtnPushApp.click();
		while(Initialization.driver.findElement(By.xpath("//div[text()='Loader']")).isDisplayed()) 
		{}
		sleep(1);
		while(Initialization.driver.findElement(By.xpath("//div[text()='Device is downloading file']")).isDisplayed()) 
		{}
		sleep(2);
	}
	
	public void VerifySuccessfullyDeployedStatus() {
		  WebDriverWait wait2 = new WebDriverWait(Initialization.driver, 1200); 
			 try{
		         wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
			     System.out.println("PASS>>> Job is deployed successfully to device");
				  }   
			 catch(Throwable e){
			       System.out.println(" job in queue do device refresh");
			      Initialization.driver.findElement(By.id("refreshButton")).click();
			       Initialization.driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);  
			 try{
			      wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
			     System.out.println("PASS >> profile is deployed successfully to device after device refresh");
			 }catch(Throwable e1)
			 {
			      System.out.println("FAIL >> profile not deployed to device");
			 }
	 }}
	
	public void ClickOnSureLock() throws InterruptedException {
		action.moveToElement(SureLockRightClick).click().perform();
		sleep(4);
	}

	public void VerifyInstallOptionWhenSLnotInstalled() throws InterruptedException {
		
		boolean bln=InstallOptionSL.isDisplayed();
		String Pass="PASS >> 'Install' Option is present when SureLock is Not Installed";
		String Fail="FAIL >> 'Install' Option is Not presnt when SureLock is Not In stalled"; 
		ALib.AssertTrueMethod(bln, Pass, Fail);	
	}
	
	public void DecativateLicenseAndExitSureLockOption() {
		
		boolean bln=SureLockDeactivateLicenseoption.isDisplayed();
		String Pass="PASS >> 'DeactivateLicense' Option is present when SureLock is Not Installed";
		String Fail="FAIL >> 'DeactivateLicense' Option is Not present when SureLock is Not In stalled"; 
		ALib.AssertTrueMethod(bln, Pass, Fail);	
		boolean bln1=ExitSureLockoption.isDisplayed();
		String Pass1="PASS >> 'ExitSureLock' Option is present when SureLock is Not Installed";
		String Fail1="FAIL >> 'ExitSureLock' Option is Not present when SureLock is Not In stalled"; 
		ALib.AssertTrueMethod(bln1, Pass1, Fail1);	
		
		Initialization.driver.findElement(By.xpath("(//div[@class='fixed-table-body'])[2]")).click();
		
	}
	
	public void DeactivationSureLock() throws InterruptedException {
		SureLockDeactivateLicenseoption.click();
		while(Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		sleep(1);
		boolean bln=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Deactivated successfully.']")).isDisplayed();
		}catch (Exception e) {
			bln=false;
		}
		ALib.AssertTrueMethod(bln,"PASS >> Deactivated successfully. Message displayed","Fail >> Deactivated successfully. Message not displayed");
		sleep(3);
	}
	
	public void ExitSureLockOptionFunctionality() throws InterruptedException {
		ExitSureLockoption.click();
		waitForXpathPresent("(//h4[text()='Enter Password'])[2]");
		sleep(1);
		ExitSureLockPWTF.sendKeys("0000");
		OkBtnExitSureLockCode.click();
		sleep(5);
		   WebDriverWait wait2 = new WebDriverWait(Initialization.driver, 240); 
			 try{
		         wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
			     System.out.println("PASS>>> Job is deployed successfully to device");
				  }   
			 catch(Throwable e){
			       System.out.println(" job in queue do device refresh");
			      Initialization.driver.findElement(By.id("refreshButton")).click();
			       Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  
			 try{
			      wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
			     System.out.println("PASS >> profile is deployed successfully to device after device refresh");
			 }catch(Throwable e1)
			 {
			      System.out.println("FAIL >> profile not deployed to device");
			 }}
	   }
	
	public void VerifyActivateLicenseLaunchSureLockOptionAndUpgradeOption() {
	
		boolean bln2=UpgradeOptionSL.isDisplayed();
		String Pass2="PASS >> 'Upgrade' Option is present when SureLock is  Installed";
		String Fail2="FAIL >> 'Upgrade' Option is Not presnt when SureLock is Not Installed"; 
		ALib.AssertTrueMethod(bln2, Pass2, Fail2);
		boolean bln=ActivateLicenseOptionSL.isDisplayed();
		String Pass="PASS >> 'ActivateLicense' Option is present when SureLock is  Installed";
		String Fail="FAIL >> 'ActivateLicense' Option is Not presnt when SureLock is Not Installed"; 
		ALib.AssertTrueMethod(bln, Pass, Fail);
		boolean bln1=LaunchSureLockOptionSL.isDisplayed();
		String Pass1="PASS >> 'LaunchSureLock' Option is present when SureLock is Installed";
		String Fail1="FAIL >> 'LaunchSureLock' Option is Not presnt when SureLock is Not Installed"; 
		ALib.AssertTrueMethod(bln1, Pass1, Fail1);
	}
	
	public void ActivateLicenseOptionFunctionalitySureLock() throws InterruptedException {
		ActivateLicenseOptionSL.click();
		waitForXpathPresent("//h4[text()='Enter Activation Code']");
		sleep(2);
		OkBtnExitSureLockCode.click();
		sleep(2);
		boolean bln=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Please enter activation code']")).isDisplayed();
		}catch (Exception e) {
			bln=false;
		}
		ALib.AssertTrueMethod(bln,"PASS >> 'Please enter activation code' Message displayed","Fail >> 'Please enter activation code' Message not displayed");
		ActivationCodeSureLockTF.sendKeys("909686");
		OkBtnExitSureLockCode.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		boolean bln1=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Activation Initiated']")).isDisplayed();
		}catch (Exception e) {
			bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Activation Initiated' Message displayed","Fail >> 'Activation Initiated' Message not displayed");
	}
	
	public void ClickOnLaunchSureLockOption() throws InterruptedException {
		LaunchSureLockOptionSF.click();
		sleep(2);
		waitForXpathPresent("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']");
		sleep(2);
	}
	
	public void ClickOnrefreshButtonDeviceInfo() throws InterruptedException {
		WebDriverWait wait=new WebDriverWait(Initialization.driver,300);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("refreshButton")));
		refreshdeviceinfo.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='lds-ring1'])[1]")));
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {
		sleep(3);
		}
	}
	
	public void ClickOnRefreshDeviceGrid() throws InterruptedException {
		RefreshBtnDevicegrid.click();
		waitForXpathPresent("(//div[@class='th-inner sortable'])[2]");
		sleep(4);
	}
		
    public void ClickOnUpgrade() throws InterruptedException {
    	UpgradeOptionSL.click();
		sleep(2);
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		boolean bln1=true;
		try {
		 Initialization.driver.findElement(By.xpath("//span[text()='Initiated SureFox upgrade on device.']")).isDisplayed();
		}catch (Exception e) {
		  bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Initiated SureFox upgrade on device.' Message displayed","Fail >> 'Initiated SureFox upgrade on device.' Message not displayed");		
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 800);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/install_confirm_question")));
    }
	
	public void ClickOnSureFox() throws InterruptedException {
		sleep(1);
		action.moveToElement(SureFoxRightClick).perform();	
		sleep(1);
	}
	public void clickOnInstallBtnInSureFox() throws InterruptedException
	{
		InstallOptionSF.click();
		sleep(3);
	}
	@FindBy(xpath="//input[@value='Install Now']")
	private WebElement clickOnInstallNow;
	public void clickOnInstallNow()throws InterruptedException
	{
		clickOnInstallNow.click();
		sleep(5);
	}
	public void clickOnAllowsubdomains()throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='AllowSubDomain']//input[@type='checkbox']")).click();
		sleep(4);
	}
   public void VerifyInstallOptionWhenSFnotInstalled() {
		boolean bln=InstallOptionSF.isDisplayed();
		String Pass="PASS >> 'Install' Option is present when SureFox is Not Installed";
		String Fail="FAIL >> 'Install' Option is Not presnt when SureFox is Not Installed"; 
		ALib.AssertTrueMethod(bln, Pass, Fail);		
	
	}
	
	public void VerifyActivateLicenseLaunchSureFoxAndUpgradeOption() {
		
		boolean bln2=UpgradeOptionSF.isDisplayed();
		String Pass2="PASS >> 'Upgrade' Option is present when SureFox is  Installed";
		String Fail2="FAIL >> 'Upgrade' Option is Not present when SureFox is Not Installed"; 
		ALib.AssertTrueMethod(bln2, Pass2, Fail2);
		
		boolean bln=ActivateLicenseOptionSF.isDisplayed();
		String Pass="PASS >> 'ActivateLicenseOption' Option is present when SureFox is  Installed";
		String Fail="FAIL >> 'ActivateLicenseOption' Option is Not present when SureFox is Not Installed"; 
		ALib.AssertTrueMethod(bln, Pass, Fail);
		
		boolean bln1=LaunchSureLockOptionSF.isDisplayed();
		String Pass1="PASS >> 'LaunchSureLockOption' Option is present when SureFox is  Installed";
		String Fail1="FAIL >> 'LaunchSureLockOption' Option is Not present when SureFox is Not Installed"; 
		ALib.AssertTrueMethod(bln1, Pass1, Fail1);
	}
	
	public void ActivationOfSureFoxRightClick(String ActivationCode) throws InterruptedException {
		ActivateLicenseOptionSF.click();
		waitForXpathPresent("//h4[text()='Enter Activation Code']");
		sleep(1);
		ActvationCodeTextFieldSL.sendKeys(ActivationCode);
		ActvationCodeOkbtn.click();
		sleep(2);
		boolean bln1=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Activation Initiated.']")).isDisplayed();
		}catch (Exception e) {
			bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Activation Initiated' Message displayed","Fail >> 'Activation Initiated' Message not displayed");
		waitForXpathPresent("(//div[@class='lds-ring1'])[1]");
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {}
	}
	
	public void ClickOnsurefoxSettings() throws Throwable 
	{
	    SFSettingsJobs.click();
		waitForidPresent("surefoxAllowedWebsite");
		sleep(2);
    }
	@FindBy(xpath="//*[@id='SLSubMenu']/li[3]/span[text()='Activate License']")
	private WebElement ActivateLicenseSL;
	public void ActivationOfSureLockRightClick(String ActivationCode) throws InterruptedException {
		ActivateLicenseSL.click();
		waitForXpathPresent("//h4[text()='Enter Activation Code']");
		sleep(1);
		ActvationCodeTextFieldSL.sendKeys(ActivationCode);
		ActvationCodeOkbtn.click();
		sleep(2);
		boolean bln1=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Activation Initiated.']")).isDisplayed();
		}catch (Exception e) {
			bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Activation Initiated' Message displayed","Fail >> 'Activation Initiated' Message not displayed");
		waitForXpathPresent("(//div[@class='lds-ring1'])[1]");
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {}
	}
	public void ClickOnInstallBtnRightClickSL() throws InterruptedException {
		
		
		InstallOptionSL.click();
		waitForXpathPresent("//input[@type='button' and @value='Install Now']");
		sleep(2);
	}
    public void ClickOnSaveBtnSurefoxSettings() throws InterruptedException {
    	SaveBtnSureFoxSettingsInJobs.click();
    	waitForXpathPresent("//h4[text()='Job Details']");
    	sleep(2);
    }
	
	public void EnterJobDetails() throws InterruptedException {
		JobNameTF.sendKeys("SurefoxSettings");
		sleep(2);
		SaveBtnJobDetails.click();
		sleep(3);
	}

	public void ClickOnApplyButtonApplyJobWindow() throws InterruptedException
	{
		ApplyButtonJobWindow.click();
		sleep(30);
	
	}
	
	public void VerifyDeactivateSureFoxRightClick() {
		
		boolean bln=DeactivateOptionSureFox.isDisplayed();
		String pass="PASS >> 'Deactivate License' Option is present";
		String fail="FAIL >> 'Deactivate License' Option is not present";
		ALib.AssertTrueMethod(bln, pass, fail);
	}
	
    	
	public void DeactivateSureFoxRightClick() throws InterruptedException {
		DeactivateOptionSureFox.click();
		sleep(4);
		boolean bln1=true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Deactivation initiated on the device.']")).isDisplayed();
		}catch (Exception e) {
			bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Deactivation initiated on the device.' Message displayed","Fail >> 'Deactivation initiated on the device.' Message not displayed");
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()){}
	}
	
	@FindBy(xpath="//*[@id='SLSubMenu']/li[4]/span[text()='Deactivate License']")
	private WebElement DeactivateOptionSureLock;
	
public void VerifyDeactivateSureLockRightClick() {
		
		boolean bln=DeactivateOptionSureLock.isDisplayed();
		String pass="PASS >> 'Deactivate License' Option is present";
		String fail="FAIL >> 'Deactivate License' Option is not present";
		ALib.AssertTrueMethod(bln, pass, fail);
	}


public void DeactivateSureLockRightClick() throws InterruptedException {
	DeactivateOptionSureLock.click();
	sleep(4);
	boolean bln1=true;
	try {
		Initialization.driver.findElement(By.xpath("//span[text()='Deactivation initiated on the device.']")).isDisplayed();
	}catch (Exception e) {
		bln1=false;
	}
	ALib.AssertTrueMethod(bln1,"PASS >> 'Deactivation initiated on the device.' Message displayed","Fail >> 'Deactivation initiated on the device.' Message not displayed");
	while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()){}
}
	
	public void UpgradeOptionNotpresentSurefox() {
		boolean bln=false;
	    try {
		   Initialization.driver.findElement(By.xpath("(//span[text()='Upgrade'])[1]")).isDisplayed();
		}catch (NoSuchElementException e) {
			bln=true;
		}
		ALib.AssertTrueMethod(bln,"PASS >> 'Upgrade' option is not displayed","Fail >> 'Upgrade' option is displayed ");
	}
	
	public void ClickOnLaunchSureFox() throws InterruptedException {
		LaunchSureLockOptionSF.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		waitForXpathPresent("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']");
		sleep(15);
	}
	
	
	public void ClickOnLaunchSureLock() throws InterruptedException {
		LaunchSureLockOptionSL.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		waitForXpathPresent("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']");
		sleep(15);
	}
	
	public void ClickOnUpgradeSureox() throws InterruptedException {
		UpgradeOptionSF.click();
		sleep(2);
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		boolean bln1=true;
		try {
		 Initialization.driver.findElement(By.xpath("//span[text()='Initiated SureFox upgrade on device.']")).isDisplayed();
		}catch (Exception e) {
		  bln1=false;
		}
		ALib.AssertTrueMethod(bln1,"PASS >> 'Initiated SureFox upgrade on device.' Message displayed","Fail >> 'Initiated SureFox upgrade on device.' Message not displayed");		
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 800);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/install_confirm_question")));

	}
		
	public void ClickOnSureVideo() throws InterruptedException {
	      action.moveToElement(SureVideoRightclick).perform();
	      sleep(2);
	}
	
	public void InstallOptionSureVideo() {
		boolean bln=InstallOptionSureVideo.isDisplayed();
		String Pass="PASS >> 'Install' Option is Displayed";
		String fail="FAIL >> 'Install' Option is not Displayed";
		ALib.AssertTrueMethod(bln, Pass, fail);
	}
	
	public void VerifyUpgradeActvateLicenseAndLaunchSureVideoOption() {
		boolean bln=UpgradeOptionSureVideo.isDisplayed();
		String Pass="PASS >> 'Upgrade' Option is Displayed";
		String fail="FAIL >> 'Upgrade' Option is not Displayed";
		ALib.AssertTrueMethod(bln, Pass, fail);
		boolean bln1=ActvateLicenseSureVideo.isDisplayed();
		String Pass1="PASS >> 'ActvateLicense' Option is Displayed";
		String fail1="FAIL >> 'ActvateLicense' Option is not Displayed";
		ALib.AssertTrueMethod(bln1, Pass1, fail1);
		boolean bln2=LaunchOptionSureVideo.isDisplayed();
		String Pass2="PASS >> 'LaunchSureVideo' Option is Displayed";
		String fail2="FAIL >> 'LaunchSureVideo' Option is not Displayed";
		ALib.AssertTrueMethod(bln2, Pass2, fail2);
	}
	
	public void ActivateSureVideo(String ActivationCode) throws InterruptedException {
		ActvateLicenseSureVideo.click();
		waitForXpathPresent("//h4[.='Enter Activation Code']");
		sleep(2);
		ActivationCodeTfSureVideo.sendKeys(ActivationCode);
		ActivationCodeOkBtnsureVideo.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}
	
	public void DeactivateOptionSureVideo() {
		boolean bln2=DeactivateLicenseOptionSureVideo.isDisplayed();
		String Pass2="PASS >> 'DeactivateLicenseSureVideo' Option is Displayed";
		String fail2="FAIL >> 'DeactivateLicenseSureVideo' Option is not Displayed";
		ALib.AssertTrueMethod(bln2, Pass2, fail2);
	}
	
	public void  DeactivateSureVideo() throws InterruptedException {
		DeactivateLicenseOptionSureVideo.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
		
	}
	
	public void LaunchSureVideo() throws InterruptedException {
		LaunchOptionSureVideo.click();
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed()) {}
		waitForXpathPresent("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']");
		sleep(2);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	   //***************** Appium Part *********************

	
	public void LaunchNix() throws Exception{
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm");
		sleep(2);
	}
	
	public void VerifyDeviceStopped() {
		boolean bln=Initialization.driver.findElement(By.xpath("//android.widget.TextView[@text='Stopped']")).isDisplayed();
		String pass="PASS >> Device Stopped ";
		String fail="FAIL >> Device Not Stopped";
		ALib.AssertTrueMethod(bln, pass, fail);
	}

	public boolean isknox=true;
	public boolean VerifyIsDeviceKnox(){
		Initialization.driverAppium.findElementById("com.nix:id/button2").click();
	//	Initialization.driverAppium.scrollToExact("Use https");
		try {
		Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Enable KNOX Features']")).isDisplayed();
		}catch (Exception e) {
			isknox=false;
		}
		System.out.println("Device is Knox : "+isknox);
		Initialization.driverAppium.findElementById("com.nix:id/main_done_button").click();
		return isknox;
	}
	
	public void ClickonInstallBtnDevice() throws InterruptedException, IOException {
		if(isknox==true) {
		
		}else {
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,240);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);
		}}
	
	public void ClickonInstallBtnDevice1() throws InterruptedException {
		if(isknox==true) {
			
		}else {
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,400);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);}
	}
	
	public void ClickonInstallBtnDevice2() throws InterruptedException {
		if(isknox==true){}else {
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,400);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);}
	}
	
	public void InstallationMultipleApk() throws InterruptedException, IOException {
        if(isknox==true) {}else {
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,300);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();}
	}
	
	public void InstallationMultipleApk1() throws InterruptedException, IOException {
        if(isknox==true) {
			
		}else { 
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,300);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();}
		
	}
	
	public void InstallationMultipleApk2() throws InterruptedException, IOException {
        if(isknox==true) {
		}else {
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,300);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(1);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();}
	}
	
	public void InstallationofSurevideo() throws InterruptedException, IOException {
     if(isknox==true) {}else {
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);}
		
	}
	
   public void InstallationofSurevideo1() throws InterruptedException, IOException {
	   
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
	    sleep(1);
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,240);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);
		
	}
   
   public void InstallationofSurevideo2() throws InterruptedException, IOException {
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
	    sleep(1);
	    Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,240);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
		sleep(2);
	}
	
   public void AllowPermission() throws InterruptedException {
	   Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
	   sleep(2);
   }
   
   public void AllowPermissionBtnInSurefox()throws InterruptedException
   {
	   Initialization.driverAppium.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click();
	   sleep(3);
   }
	    //****************PUSH FILE*****************************
	
	
	public void LaunchFileManager() throws Exception {
		Runtime.getRuntime().exec("adb shell am start -n com.mauriciotogneri.fileexplorer/com.mauriciotogneri.fileexplorer.app.MainActivity");
		sleep(2);
	}
	
	public void CloseRecentApp() throws IOException, InterruptedException {
		
		Runtime.getRuntime().exec("adb shell input keyevent KEYCODE_HOME");
		sleep(2);
		Runtime.getRuntime().exec("adb shell input keyevent KEYCODE_APP_SWITCH");
		sleep(2);
		Runtime.getRuntime().exec("adb shell input keyevent DEL");
		sleep(2);
	}
	

	
	
	
	public void InstallationSureLockLowerversion() throws Exception{
		Runtime.getRuntime().exec("adb install ./Uploads/apk/\\surelockv9.61.apk");
		sleep(30);
	}
	
	public void LaunchSureLock() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start -n com.gears42.surelock/com.gears42.surelock.ClearDefaultsActivity");
		sleep(4);
	}
	
    public void VerifyAllowingSureLockSettingPermission() throws InterruptedException {
		Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Set SureLock As Default Launcher']")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/nextButton")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("android:id/button_always")).click();
		sleep(2);
		Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Continue']")).click();
		sleep(2);
	}
	
    public void ClickOnSkipBtnEula() throws InterruptedException {
    	Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/skipButton")).click();
		sleep(2);
    }
	
    public void VerifySureLockActivation() throws InterruptedException {
    	boolean bln=false;
		try
		{
		Initialization.driverAppium.findElement(By.id("com.gears42.surefox:id/ivsurefoxIconTrial")).isDisplayed();
		}catch (Exception e) {
			bln=true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> SureLock is Activated","FAIL >> SureLock is Not Activated");
    }
	
	public void InstallationSureFoxOlder() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb install ./Uploads/apk/surefoxv7.84.apk");
		sleep(30);
	}
	
	public void LaunchSureFox() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start -n \"com.gears42.surefox/com.gears42.surefox.SurefoxBrowser\" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER");
		sleep(4);
	}
	
	public void VerifySureFoxActivationOndevice() {
		boolean bln=false;
		try
		{
			Initialization.driverAppium.findElement(By.id("com.gears42.surefox:id/ivsurefoxIconTrial")).isDisplayed();
		}catch (Exception e) {
			bln=true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> Surefox is Activated","FAIL >> Surefox is Not Activated");
	 }
	
	public void LaunchSureFoxSettings() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am broadcast -a com.gears42.surefox.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" 0000");
		sleep(4);
	}

	public void CheckSureFoxDeactivatedOnDevice() {
		boolean status=Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Sign up for FREE full feature trial' and @index='3']")).isDisplayed();
		String pass="PASS >> 'SureFox' is Deactivated On Device";
		String fail="FAIL >> 'SureFox' is Not Deactivated On Device";
		ALib.AssertTrueMethod(status, pass, fail);
	}
	
	public void InstallUpgradedVersionSureFoxOndevice() throws InterruptedException {
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		sleep(2);
		WebDriverWait wait =new WebDriverWait(Initialization.driverAppium,400);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
	}
	
	public void ClickOnHomeButtonDevice() throws Exception {
		Runtime.getRuntime().exec("adb shell input keyevent KEYCODE_HOME");
		sleep(2);
	}
	
	public void VerifySureFoxLaunchedOnDevice() throws InterruptedException {
		boolean status=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Tap the screen 5 times' and @index='1']")).isDisplayed();
		String pass="PASS >> 'SureFox' is Launched On Device";
		String fail="FAIL >> 'SureFox' is Not Launched On Device";
		ALib.AssertTrueMethod(status, pass, fail);
	}
	
	
	public void VerifySureLockLaunchedOnDevice() throws InterruptedException {
		boolean status=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Tap the screen 5 times' and @index='1']")).isDisplayed();
		String pass="PASS >> 'SureFox' is Launched On Device";
		String fail="FAIL >> 'SureFox' is Not Launched On Device";
		ALib.AssertTrueMethod(status, pass, fail);
	}
	
	
	public void UnistallSureFox() throws IOException, InterruptedException {
        Initialization.driverAppium.removeApp("com.gears42.surefox");
        Runtime.getRuntime().exec("adb shell rm -r /sdcard/surefox.apk");
        sleep(5);
	}
	
	public void ClickInstallSureFox() throws InterruptedException {
		InstallOptionSF.click();
		waitForXpathPresent("//h5[text()='Welcome to SureFox Setup.']");
		sleep(2);
	}
	
	public void InstallSureVideoLower() throws IOException, InterruptedException {
		System.out.println("Installing SureVideo Lower version");
		Runtime.getRuntime().exec("adb install ./Uploads/apk/surevideov3.07.apk");
		sleep(5);
	}
	


	//new methods

	@FindBy(xpath=".//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")
	private WebElement ClickOnSearchInPendingDelete;
	
	public void clickOnPendingDelete() throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//span[@id='uaBadgeDelete']")).click();
			sleep(3);
		}
	public void VerifyOfDeletedDeviceInPendingDelete(String device) throws InterruptedException
		{
			ClickOnSearchInPendingDelete.clear();
			sleep(2);
			ClickOnSearchInPendingDelete.sendKeys(device);
			boolean isdisplayed = Initialization.driver.findElement(By.xpath("//td[text()='"+device+"']")).isDisplayed();
			String deviceName = device;
			String pass = "PASS >> Device "+device+" is searched successfully";
			String fail = "FAIl >> Device is not searched";
			ALib.AssertTrueMethod(isdisplayed, pass, fail);
			sleep(1);
		}
		public void clicOnDeleteForMultipleDevice() throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//ul[@id='customMenu']//span[text()='Delete']")).click();
			sleep(3);
		}
	
		public void clickOnGroup() throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//div[@id='groupsbuttonpanel']/div/ul/li/a/span[text()='Groups']")).click();
			sleep(2);
		}
	
		public void NotificationOnMoveDevice(String Grp)
		{
			try {
				System.out.println(Initialization.driver.findElement(By.xpath("//span[contains(text(),'Device moved successfully to Group "+Grp+"')]")).getText());
				Initialization.driver.findElement(By.xpath("//span[contains(text(),'Device moved successfully to Group "+Grp+"')]")).isDisplayed();
				Reporter.log("PASS >> Device moved successfully to Group is displayed successfully.",true);
			} catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Device moved successfully to Group isn't displayed successfully.");
			}
		}
		

	public void VerifyInstallOptionForUEMnix()
	{
		try {
		if (!Initialization.driver.findElement(By.xpath("(//span[text()='Install'])[3]")).isDisplayed()) {
			System.out.println("Install Option is not shown for SL");
		}
	}catch (Exception e) {
		ALib.AssertFailMethod("Install Option is shown for SL");
	}
	}
		@FindBy(xpath="//ul[@id='gridMenu']/li//span[text()='Tags']")
		private WebElement RightClick_Tags;
		
		public void ClickOnTagOptions_RightClick(String Option) throws InterruptedException
		{
			action.moveToElement(RightClick_Tags).build().perform();
			sleep(2);
			Initialization.driver.findElement(By.xpath("//ul[@id='TagOptMenu']//span[text()='"+Option+"']")).click();
			waitForXpathPresent("//h4[text()='Tag List']");
			sleep(3);
		}
		
		public void moveGrpToGrp_RightClick(String grp) throws InterruptedException {
			waitForXpathPresent("(//h4[.='Group List'])[2]");
			sleep(2);
			SearchGroupTF.sendKeys(grp);
			waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='"+grp+"']");
		    Initialization.driver.findElement(By.xpath("//*[@id='groupList']/ul/li[text()='"+grp+"']")).click();
		    sleep(2);
			MoveBtnGroupListPopUp.click();
			sleep(4);
			Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
			sleep(10);
			Reporter.log("Devices moved successfully",true);
		}
	
}
