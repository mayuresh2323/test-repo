package PageObjectRepository;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class DynamicRefresh_JobQueueQuickAction_POM extends WebDriverCommonLib 
{
	AssertLib Ali=new AssertLib();
    @FindBy(xpath="//div[@id='refreshButton']")
    private WebElement Refreshbutton;
    
    @FindBy(xpath="(//div[@class='lds-ring1'])[2]")
    private WebElement loadingele;
    
    @FindBy(xpath="//div[@id='jobQueueButton']")
    private WebElement QueueButton;
    
    @FindBy(xpath="//li[@id='dynamicJObqueueSection']")
    private WebElement QuickActionButton;
    
    @FindBy(xpath="//*[@id='jobQueueDataDynamicGrid']/tbody/tr[1]/td[3]")
    private WebElement Status;
    
    public void verifyDeviceRefresh() throws InterruptedException
    {
    	
    	Refreshbutton.click();
		sleep(60);
    	QueueButton.click();
    	waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
    	sleep(5);
    	QuickActionButton.click();
    	waitForXpathPresent("//div[@id='jobQueueRefreshDynamicBtn']");
    	sleep(5);
    	String status = Status.getText();
    	System.out.println(status);
    	boolean mark=true;
    	if(status.contains("Successful"))
    	{
    		
    		mark=true;
    	}
    	else if (status.contains("TimedOut")) 
    	{
    		mark=false;
		}
    	 String PassStatement="PASS >> Device Refreshed Successfully";
    	 String FailStatement="FAIL >> Device not Refreshed";
    	 Ali.AssertTrueMethod(mark, PassStatement, FailStatement);
     }
}
