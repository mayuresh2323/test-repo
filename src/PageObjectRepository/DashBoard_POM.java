package PageObjectRepository;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class DashBoard_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();

	@FindBy(xpath = "//a[text()='Dashboard']")
	private WebElement DashBoard;

	@FindBy(xpath = "//a[@id='osplatforms']/i")
	// checkbox of os platform
	private WebElement OSPlatformscheckbox;

	@FindBy(xpath = "//h3[contains(text(),'OS Platforms')]")
	// dashboard of os platform
	private WebElement OSPlatformsdashboard;

	@FindBy(xpath = "//span[@id='osplatformrefresh']/i")
	// refresh button in os platform
	private WebElement OSrefresh;

	@FindBy(xpath = "//span[@id='oscontainer1']/i")
	// close button
	private WebElement OSclose;

	@FindBy(xpath = "//a[@id='battery']/i")
	private WebElement AvailableBatterycheckbox;// checkbox of available battery

	@FindBy(xpath = "//h3[contains(text(),'Available Battery')]")
	// dashboard of available battery
	private WebElement AvailableBatterydashboard;

	@FindBy(xpath = "//span[@id='batteryrefresh']/i")
	// available battery refresh
	private WebElement Availablebatteryrefresh;

	@FindBy(xpath = "//span[@id='batterycontainer1']/i")
	// available battery close
	private WebElement Availablebatteryclose;

	@FindBy(xpath = "//a[@id='ram']/i")
	// availableRAM checkbox
	private WebElement AvailableRAMcheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Available RAM')]")
	// dashboard available RAM
	private WebElement AvailableRAMDashboard;

	@FindBy(xpath = "//span[@id='ramrefresh']/i")
	// availableRAMrefresh
	private WebElement AvailableRAMrefresh;

	@FindBy(xpath = "//span[@id='ramcontainer1']/i")
	// available RAM close
	private WebElement AvailableRAMclose;

	@FindBy(xpath = "//a[@id='storage']/i")
	// check box available storage
	private WebElement AvailableStoragecheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Available Storage')]")
	// available battery dashboard
	private WebElement AvailableStoragedashboard;

	@FindBy(xpath = "//span[@id='storagerefresh']/i")
	// availableStoragerefresh
	private WebElement Availablestoragerefresh;

	@FindBy(xpath = "//span[@id='intstoragecontainer1']/i")
	// available storage close
	private WebElement Availablestorageclose;

	@FindBy(xpath = "//a[@id='lastseen']/i")
	// lastconnectedcheckbox
	private WebElement LastConnectedcheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Last Connected')]")
	// lastconnecteddashboard
	private WebElement LastConnecteddashboard;

	@FindBy(xpath = "//span[@id='lastseenrefresh']/i")
	// Lastconnectedrefresh
	private WebElement Lastconnectedrefresh;

	@FindBy(xpath = "//span[@id='lastseencontainer1']/i")
	// Lastconnectedclose
	private WebElement Lastconnectedclose;

	@FindBy(xpath = "//a[@id='onlinestatus']/i")
	// Online offline checkbox
	private WebElement OnlineOfflinecheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Online / Offline')]")
	// Online offline dashboard
	private WebElement OnlineOfflinedashboard;

	@FindBy(xpath = "//span[@id='onlinerefresh']/i")
	// Onlineofflinerefresh
	private WebElement Onlineofflinerefresh;

	@FindBy(xpath = "//span[@id='onlinestatuscontainer1']/i")
	// Onlineofflineclose
	private WebElement Onlineofflineclose;

	@FindBy(xpath = "//a[@id='devicesim']/i")
	// device sim checkbox
	private WebElement DeviceSIMStatuscheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Device SIM Status')]")
	// device sim dashboard
	private WebElement DeviceSIMStatusDashboard;

	@FindBy(xpath = "//span[@id='simrefresh']/i")
	// Devicesimrefresh
	private WebElement Devicesimrefresh;

	@FindBy(xpath = "//span[@id='devicesimcontainer1']/i")
	// Devicesimclose
	private WebElement Devicesimclose;

	@FindBy(xpath = "//a[@id='roaming']/i")
	// roaming status checkbox
	private WebElement RoamingStatuscheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Roaming Status')]")
	// roaming status dashboard
	private WebElement RoamingStatusdashboard;

	@FindBy(xpath = "//span[@id='roamingrefresh']/i")
	// Roaming status refresh
	private WebElement Roamingstatusrefresh;

	@FindBy(xpath = "//span[@id='roamingcontainer1']/i")
	// Roaming status close
	private WebElement RoamingStatusclose;

	@FindBy(xpath = "//a[@id='unreadmail']/i")
	// unread mail checkbox
	private WebElement UnreadMailcheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Unread Mail')]")
	// UnreadMail dashboard
	private WebElement UnreadMaildashboard;

	@FindBy(xpath = "//span[@id='mailrefresh']/i")
	// Unreadmail refresh
	private WebElement Unreadmailrefresh;

	@FindBy(xpath = "//span[@id='unreadmailcontainer1']/i")
	// Unreadmail close
	private WebElement Unreadmailclose;

	@FindBy(xpath = "//a[@id='unapproveddevices']/i")
	// Unapproved Devices checkbox
	private WebElement UnapprovedDevicescheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Unapproved Devices')]")
	// Unapproved Devices dashboard
	private WebElement UnapprovedDevicesdashboard;

	@FindBy(xpath = "//span[@id='unapproveddevicerefresh']/i")
	// unapproveddevicerefresh
	private WebElement Unapproveddevicerefresh;

	@FindBy(xpath = "//span[@id='unapproveddevicescontainer1']/i")
	// unapproveddevicesclose
	private WebElement Unapproveddevicesclose;

	@FindBy(xpath = "//a[@id='pendingjobs']/i")
	// Jobscheckbox
	private WebElement Jobscheckbox;

	@FindBy(xpath = "//h3[contains(text(),'Jobs')]")
	// Jobsdashboard
	private WebElement Jobsdashboard;

	@FindBy(xpath = "//span[@id='jobsrefresh']/i")
	// Jobs refresh
	private WebElement Jobsrefresh;

	@FindBy(xpath = "//span[@id='pendingjobscontainer1']/i")
	// Jobs close
	private WebElement JobsClose;

	@FindBy(id = "devicedetails")
	private WebElement DeviceDetails;

	@FindBy(xpath = "//a[@id='devicedetails']/i[2]")
	private WebElement DeviceDetailsArrow;

	@FindBy(xpath = "//ul[@id='devicedetails1']/li/a/i")
	private List<WebElement> DeviceDetailsElements;

	@FindBy(id = "devicestatus")
	private WebElement DeviceStatus;

	@FindBy(xpath = "//a[@id='devicestatus']/i[2]")
	private WebElement DeviceStatusArrow;

	@FindBy(xpath = "//ul[@id='devicestatus1']/li/a/i")
	private List<WebElement> DeviceStatusElements;

	@FindBy(id = "alerts")
	private WebElement Alerts;

	@FindBy(xpath = "//a[@id='alerts']/i[2]")
	private WebElement AlertsArrow;

	@FindBy(xpath = "//ul[@id='alerts1']/li/a/i")
	private List<WebElement> AlertsElements;

	public void ClickOnDashBoard() throws InterruptedException {
		DashBoard.click();
		waitForidPresent("add-chart");
		Reporter.log("Clicking On DashBoard, Navigated to DashBoard Page", true);
		sleep(5);
	}

	public void UncheckDeviceDetails() throws InterruptedException {
		String ExpectedValue = DeviceDetails.getAttribute("class");
		if (ExpectedValue.contains("collapsed")) {
			DeviceDetailsArrow.click();
		}
		for (int i = 0; i < DeviceDetailsElements.size(); i++) {
			String ElementsUncheck = DeviceDetailsElements.get(i).getAttribute(
					"class");
			if (ElementsUncheck.contains("check-square"))
				DeviceDetailsElements.get(i).click();
			sleep(2);
		}
	}

	public void UncheckDeviceStatus() throws InterruptedException {
		String ExpectedValue = DeviceStatus.getAttribute("class");
		if (ExpectedValue.contains("collapsed")) {
			DeviceStatusArrow.click();
		}
		for (int i = 0; i < DeviceStatusElements.size(); i++) {
			String ElementsUncheck = DeviceStatusElements.get(i).getAttribute(
					"class");
			if (ElementsUncheck.contains("check-square"))
				DeviceStatusElements.get(i).click();
			sleep(2);
		}
	}

	public void UncheckAlerts() throws InterruptedException {
		String ExpectedValue = Alerts.getAttribute("class");
		if (ExpectedValue.contains("collapsed")) {
			AlertsArrow.click();
		}
		for (int i = 0; i < AlertsElements.size(); i++) {
			String ElementsUncheck = AlertsElements.get(i)
					.getAttribute("class");
			if (ElementsUncheck.contains("check-square"))
				AlertsElements.get(i).click();
			sleep(2);
		}
	}

	public void ClickOnOSPlatforms_UM() throws InterruptedException {
		OSPlatformscheckbox.click();
	}

	public void ClickOnOSPlatforms() throws InterruptedException {
		OSPlatformscheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'OS Platforms')]");//done by bhakti remove later
		sleep(2);
	}

	public void VerifyOSPlatforms() throws InterruptedException {
		boolean b = OSPlatformsdashboard.isDisplayed();
		Assert.assertTrue(b, "OS Platforms DashBoard is not displayed");
		System.out.println("OS Platforms DashBoard is displayed");
		// move to refresh button in os platform dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(OSrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in os platform dashboard
		String S = OSrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on os refresh
		OSrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(OSclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = OSclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		OSclose.click();
		Thread.sleep(10000);
		// verify whether os dashboard is closed

		Assert.assertFalse(CheckOSPlatformspopup(),
				"OS Platforms DashBoard is not closed");
		System.out.println("OS Platforms DashBoard is closed successfully");
		Thread.sleep(10000);

	}

	public boolean CheckOSPlatformspopup() throws InterruptedException {
		boolean value;
		try {
			OSPlatformsdashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnAvailableBattery_UM() throws InterruptedException {
		AvailableBatterycheckbox.click();

	}

	public void ClickOnAvailableBattery() throws InterruptedException {
		AvailableBatterycheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Available Battery')]");// done by bhakti remove later
		sleep(2);
	}

	public void VerifyAvailableBattery() throws InterruptedException {
		boolean b = AvailableBatterydashboard.isDisplayed();
		Assert.assertTrue(b, "Availablebattery DashBoard is not displayed");
		System.out.println("Availablebattery DashBoard is displayed");
		// move to refresh button in available battery dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Availablebatteryrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in availablebattery dashboard
		String S = Availablebatteryrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on available battery refresh
		Availablebatteryrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Availablebatteryclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Availablebatteryclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Availablebatteryclose.click();
		Thread.sleep(10000);
		// click on close
		// verify whether available battery dashboard is closed
		Assert.assertFalse(CheckAvailableBatterypopup(),
				"available battery DashBoard is not closed");
		System.out
				.println("available battery DashBoard is closed successfully");
		Thread.sleep(10000);

	}

	public boolean CheckAvailableBatterypopup() throws InterruptedException {
		boolean value;
		try {
			AvailableBatterydashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnAvailableRAM_UM() throws InterruptedException {
		AvailableRAMDashboard.click();
	}

	public void ClickOnAvailableRAM() throws InterruptedException {
		AvailableRAMcheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Available RAM')]"); //done by bhakti remove later
		sleep(2);
	}

	public void VerifyAvailableRAM() throws InterruptedException {
		boolean b = AvailableRAMDashboard.isDisplayed();
		Assert.assertTrue(b, "AvailableRAM DashBoard is not displayed");
		System.out.println("AvailableRAM DashBoard is displayed");
		// move to refresh button in available RAM dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(AvailableRAMrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in availableRAM dashboard
		String S = AvailableRAMrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on availableRAM refresh
		AvailableRAMrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(AvailableRAMclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = AvailableRAMclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		AvailableRAMclose.click();
		Thread.sleep(10000);
		// click on close
		// verify whether available RAM dashboard is closed

		Assert.assertFalse(CheckAvailableRAMpopup(),
				"available RAM DashBoard is not closed");
		System.out.println("available RAM DashBoard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckAvailableRAMpopup() throws InterruptedException {
		boolean value;
		try {
			AvailableRAMDashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnAvailableStorage_UM() throws InterruptedException {
		AvailableStoragecheckbox.click();
	}

	public void ClickOnAvailableStorage() throws InterruptedException {
		AvailableStoragecheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Available Storage')]");//done by bhakti remove later 
		sleep(2);
	}

	public void VerifyAvailableStorage() throws InterruptedException {
		boolean b = AvailableStoragedashboard.isDisplayed();
		Assert.assertTrue(b, "Availablestorage DashBoard is not displayed");
		System.out.println("Availablestorage DashBoard is displayed");
		// move to refresh button in available storage dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Availablestoragerefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in availablestorage dashboard
		String S = Availablestoragerefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on availablestorage refresh
		Availablestoragerefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Availablestorageclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Availablestorageclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Availablestorageclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether available stoorage dashboard is closed
		Assert.assertFalse(CheckAvailableStoragepopup(),
				"available storage DashBoard is not closed");
		System.out
				.println("available storage DashBoard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckAvailableStoragepopup() throws InterruptedException {
		boolean value;
		try {
			AvailableStoragedashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnLastConnected_UM() throws InterruptedException {
		LastConnectedcheckbox.click();
	}

	public void ClickOnLastConnected() throws InterruptedException {
		LastConnectedcheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Last Connected')]");// done by bhakti remove later
		sleep(2);
	}

	public void VerifyLastConnected() throws InterruptedException {
		boolean b = LastConnecteddashboard.isDisplayed();
		Assert.assertTrue(b, "LastConnected DashBoard is not displayed");
		System.out.println("LastConnected DashBoard is displayed");
		// move to refresh button in last connected dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Lastconnectedrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in lastconnected dashboard
		String S = Lastconnectedrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on lastconnected refresh
		Lastconnectedrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Lastconnectedclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Lastconnectedclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Lastconnectedclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether lastconnected dashboard is closed

		Assert.assertFalse(CheckLastConnectedpopup(),
				"lastconnected DashBoard is not closed");
		System.out.println("lastconnected DashBoard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckLastConnectedpopup() throws InterruptedException {
		boolean value;
		try {
			LastConnecteddashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnOnlineOffline_UM() throws InterruptedException {
		OnlineOfflinecheckbox.click();
	}

	public void ClickOnOnlineOffline() throws InterruptedException {
		OnlineOfflinecheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Online / Offline')]");//done by bhakti remove later 
		sleep(2);
	}

	public void Verifyonlineoffline() throws InterruptedException {
		boolean b = OnlineOfflinedashboard.isDisplayed();
		Assert.assertTrue(b, "OnlineOffline dashboard is not displayed");
		System.out.println("OnlineOffline dashboard is displayed");
		// move to refresh button in Online Offline dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Onlineofflinerefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in Online Offline dashboard
		String S = Onlineofflinerefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Onlineoffline refresh
		Onlineofflinerefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Onlineofflineclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Onlineofflineclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Onlineofflineclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether Onlineoffline dashboard is closed
		Assert.assertFalse(CheckOnlineOfflinepopup(),
				"available storage DashBoard is not closed");
		System.out.println("Onlineoffline dashboard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckOnlineOfflinepopup() throws InterruptedException {
		boolean value;
		try {
			OnlineOfflinedashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnDeviceSIMStatus_UM() throws InterruptedException {
		DeviceSIMStatuscheckbox.click();
	}

	public void ClickOnDeviceSIMStatus() throws InterruptedException {
		DeviceSIMStatuscheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Device SIM Status')]");//done by bhakti remove later
		sleep(2);
	}

	public void VerifyDeviceSIMStatus() throws InterruptedException {
		boolean b = DeviceSIMStatusDashboard.isDisplayed();
		Assert.assertTrue(b, "DeviceSIMStatusDashboard is not displayed");
		System.out.println("DeviceSIMStatus Dashboard is displayed");
		// move to refresh button in DeviceSIMStatus Dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Devicesimrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in DeviceSIMStatus Dashboard
		String S = Devicesimrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Devicesimrefresh
		Devicesimrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Devicesimclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Devicesimclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Devicesimclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether DeviceSIMStatus dashboard is closed
		Assert.assertFalse(CheckDeviceSIMStatuspopup(),
				"available storage DashBoard is not closed");
		System.out.println("DeviceSIMStatus dashboard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckDeviceSIMStatuspopup() throws InterruptedException {
		boolean value;
		try {
			DeviceSIMStatusDashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnRoamingStatus_UM() throws InterruptedException {
		RoamingStatuscheckbox.click();
	}

	public void ClickOnRoamingStatus() throws InterruptedException {
		RoamingStatuscheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Roaming Status')]");//done by bhakti remove later
		sleep(2);
	}

	public void VerifyRoamingStatus() throws InterruptedException {
		boolean b = RoamingStatusdashboard.isDisplayed();
		Assert.assertTrue(b, "RoamingStatusdashboard is not displayed");
		System.out.println("RoamingStatus Dashboard is displayed");
		// move to refresh button in RoamingStatusdashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Roamingstatusrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in Roamingstatus Dashboard
		String S = Roamingstatusrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Roamingstatusrefresh
		Roamingstatusrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(RoamingStatusclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = RoamingStatusclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		RoamingStatusclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether RoamingStatus dashboard is closed
		Assert.assertFalse(CheckRoamingStatuspopup(),
				"available storage DashBoard is not closed");
		System.out.println("RoamingStatus dashboard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckRoamingStatuspopup() throws InterruptedException {
		boolean value;
		try {
			RoamingStatusdashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnUnreadMail_UM() throws InterruptedException {
		UnreadMailcheckbox.click();
	}

	public void ClickOnUnreadMail() throws InterruptedException {
		UnreadMailcheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Unread Mail')]");//done by bhakti remove later
		sleep(2);
	}

	public void verifyUnreadMail() throws InterruptedException {
		boolean b = UnreadMaildashboard.isDisplayed();
		Assert.assertTrue(b, "UnreadMaildashboard is not displayed");
		System.out.println("UnreadMaildashboard Dashboard is displayed");
		// move to refresh button in UnreadMaildashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Unreadmailrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in Unreadmail Dashboard
		String S = Unreadmailrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Unreadmailrefresh
		Unreadmailrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Unreadmailclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Unreadmailclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Unreadmailclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether Unreadmail dashboard is closed
		Assert.assertFalse(CheckUnreadMailpopup(),
				"Unreadmail dashboard is not closed");
		System.out.println("Unreadmail dashboard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckUnreadMailpopup() throws InterruptedException {
		boolean value;
		try {
			UnreadMaildashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnUnapprovedDevices_UM() throws InterruptedException {
		UnapprovedDevicescheckbox.click();
	}

	public void ClickOnUnapprovedDevices() throws InterruptedException {
		UnapprovedDevicescheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Unapproved Devices')]");//done by bhakti remove later 
		sleep(2);
	}

	public void verifyUnapprovedDevices() throws InterruptedException {
		boolean b = UnapprovedDevicesdashboard.isDisplayed();
		Assert.assertTrue(b, "UnapprovedDevicesdashboard is not displayed");
		System.out.println("UnapprovedDevicesdashboard Dashboard is displayed");
		// move to refresh button in UnapprovedDevices dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Unapproveddevicerefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in UnapprovedDevices Dashboard
		String S = Unapproveddevicerefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Unapproveddevicerefresh
		Unapproveddevicerefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(Unapproveddevicesclose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = Unapproveddevicesclose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		Unapproveddevicesclose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether UnapprovedDevicesdashboard dashboard is closed
		Assert.assertFalse(CheckUnapprovedDevicespopup(),
				"Unapproved devices DashBoard is not closed");
		System.out
				.println("Unapproved devices DashBoard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckUnapprovedDevicespopup() throws InterruptedException {
		boolean value;
		try {
			UnapprovedDevicesdashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void ClickOnJobs_UM() throws InterruptedException {
		Jobscheckbox.click();
	}

	public void ClickOnJobs() throws InterruptedException {
		Jobscheckbox.click();
//		waitForXpathPresent("//h3[contains(text(),'Jobs')]");//remove later done by bhakti
		sleep(2);
	}

	public void verifyJobsDashboard() throws InterruptedException {
		boolean b = Jobsdashboard.isDisplayed();
		Assert.assertTrue(b, "Jobs dashboard is not displayed");
		System.out.println("jobs Dashboard is displayed");
		// move to refresh button in jobs dashboard
		System.out.println("Click on Refresh");

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(Jobsrefresh).perform();
		Thread.sleep(2000);
		// verify tooltip of refresh in jobs Dashboard
		String S = Jobsrefresh.getAttribute("title");
		Assert.assertEquals(S, "Refresh", "Refresh tooltip is not displayed");
		System.out.println("Refresh Tooltip is displayed");
		// click on Jobsrefresh
		Jobsrefresh.click();
		Thread.sleep(10000);
		System.out.println("Refreshed successfully");

		// move to close button
		System.out.println("Click on Close");
		Actions act1 = new Actions(Initialization.driver);
		act1.moveToElement(JobsClose).perform();
		Thread.sleep(10000);
		// verify tool tip of close button
		String S1 = JobsClose.getAttribute("title");
		Assert.assertEquals(S1, "Close", "Close tooltip is not displayed");
		System.out.println("Close Tooltip is displayed");
		JobsClose.click();
		Thread.sleep(10000);
		// click on close

		// verify whether jobs dashboard is closed
		Assert.assertFalse(CheckJobspopup(), "jobs DashBoard is not closed");
		System.out.println("jobs DashBoard is closed successfully");
		Thread.sleep(10000);
	}

	public boolean CheckJobspopup() throws InterruptedException {
		boolean value;
		try {
			Jobsdashboard.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public boolean CheckDashBoardBtn() {
		boolean value = true;
		try {
			DashBoard.isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}
	
	public void ClickOnAvailableBatteryForErrorCheck() throws InterruptedException {
		AvailableBatterycheckbox.click();
		sleep(2);
	}
	public void ClickOnAvailableRAMForErrorCheck() throws InterruptedException {
		AvailableRAMcheckbox.click();
		
		sleep(2);
	}
	public void ClickOnAvailableStorageForErrorCheck() throws InterruptedException {
		AvailableStoragecheckbox.click();
		sleep(2);
	}
	
	public void ClickOnLastConnectedForErrorCheck() throws InterruptedException {
		LastConnectedcheckbox.click();
		sleep(2);
	}
	
	public void ClickOnOSPlatformsForErrorCheck() throws InterruptedException {
		OSPlatformscheckbox.click();
		sleep(2);
	}
	
	public void ClickOnOnlineOfflineForErrorCheck() throws InterruptedException {
		OnlineOfflinecheckbox.click();
		sleep(2);
	}
	public void ClickOnDeviceSIMStatusForErrorCheck() throws InterruptedException {
		DeviceSIMStatuscheckbox.click();
		sleep(2);
	}
	
	public void ClickOnRoamingStatusForErrorCheck() throws InterruptedException {
		RoamingStatuscheckbox.click();;
		sleep(2);
	}
	public void ClickOnUnreadMailForErrorCheck() throws InterruptedException {
		UnreadMailcheckbox.click();
		sleep(2);
	}
	public void ClickOnUnapprovedDevicesForErrorCheck() throws InterruptedException {
		UnapprovedDevicescheckbox.click();
		sleep(2);
	}
	
	public void ClickOnJobsForErrorCheck() throws InterruptedException {
		Jobscheckbox.click();
		sleep(2);
	}

}
