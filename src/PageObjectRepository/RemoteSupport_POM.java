package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.xmlbeans.impl.xb.xsdownload.DownloadedSchemaEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.clipboard.ClipboardContentType;

public class RemoteSupport_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	String parentwinID;
	public String PARENTWINDOW;
	String childwinID;
	String URL;
	WebDriverWait wait = new WebDriverWait(Initialization.driver,30);

	@FindBy(id="remoteButton")
	private WebElement RemoteSupportBtn;
	
	@FindBy(id="homeSection")
	private WebElement HomeBtton;
	
	@FindBy(xpath="//li[@title='New folder']")
	private WebElement CreateFolder;

	@FindBy(xpath="//li[@title='Reload']")
	private WebElement Reload;

	@FindBy(xpath="//li[@title='Back']")
	private WebElement Back;

	@FindBy(xpath="//li[@title='Remove']")
	private WebElement Remove;

	@FindBy(xpath="//li[@title='Upload files']")
	private WebElement UploadFiles;

	@FindBy(xpath="//li[@title='Download']")
	private WebElement Download;

	@FindBy(xpath="//li[@title='View as icons']")
	private WebElement ViewAsIcons;

	@FindBy(xpath="//li[@title='View as list']")
	private WebElement ViewAsList;

	@FindBy(id="device_name")
	private WebElement DeviceName;

	@FindBy(xpath="//div[@id='powerButton']")
	private WebElement PowerButton;

	@FindBy(id="back_button")
	private WebElement BackButton;

	@FindBy(id="home_button")
	private WebElement HomeButton;

	@FindBy(id="menu_button")
	private WebElement MenuButton;

	@FindBy(id="onlineOffline")
	private WebElement OnlineOfflineStatus;

	@FindBy(xpath="//div[@id='onlineOffline']/span")
	private WebElement DeviceOnlineOfflineStatus;

	@FindBy(id="configurationButton")
	private WebElement SettingsBtn;

	@FindBy(id="playPauseButton")
	private WebElement PlayPauseBtn;

	@FindBy(id="volume_up")
	private WebElement VolumeUpBtn;

	@FindBy(id="volume_down")
	private WebElement VolumeDownBtn;

	@FindBy(id="search_button")
	private WebElement Searchbutton;

	@FindBy(xpath="//div[@id='captureScreenshot']")
	private WebElement CaptureScreenshotBtn;

	@FindBy(id="controlAltDel")
	private WebElement ControlALTDElBtn;

	@FindBy(xpath="//a[text()='File']")
	private WebElement FileTab;

	@FindBy(xpath="//a[text()='Clipboard']")
	private WebElement ClipBoardTab;

	@FindBy(id="rclipboard")
	private WebElement ClipboardTextBox;

	@FindBy(id="readClipboard")
	private WebElement ReadClipboardBtn;

	@FindBy(id="writeClipboard")
	private WebElement WriteClipboardBtn;

	@FindBy(id="clearFromDeviceClipboard")
	private WebElement ClearFromDeviceClipboardBtn;

	@FindBy(xpath="//a[text()='Task Manager']")
	private WebElement TaskManagerTab;

	@FindBy(id="refreshProcess")
	private WebElement refreshProcessBtn;

	@FindBy(id="killProcess")
	private WebElement KillProcessBtn;

	@FindBy(id="killAllProcess")
	private WebElement killAllProcessBtn;

	@FindBy(xpath="//div[@id='fountainG']")
	private WebElement LoadingBarOfRemoteSupport;

	@FindBy(xpath="//label/input")
	private WebElement FolderNameTitle;

	@FindBy(xpath="//div[contains(@class,'directory')]")
	private List<WebElement> NoOfFolders;

	@FindBy(xpath="//div[@class='el-finder-workzone']/div[contains(@class,'el-finder-cwd')]")
	private WebElement RemoteSupportFileSpace;

	@FindBy(xpath="//strong[text()='File or folder with the same name already exists!']")
	private WebElement SameFolderNameError;

	@FindBy(xpath="//div[@id='progressbar2']/i")
	private WebElement LoadingFolder;

	@FindBy(xpath="//div[@class='el-finder-path']")
	private WebElement FolderPath;

	@FindBy(xpath="//span[text()='Upload file']")
	private List<WebElement> UploadHeader;

	@FindBy(xpath="//span[text()='Cancel']")
	private List<WebElement> CancelButtonUploadFile;

	@FindBy(xpath="//span[text()='Ok']")
	private List<WebElement> OkButtonUploadFile;

	@FindBy(xpath="//div[input[@id='fileSelector']]")
	private List<WebElement> BrowseFileButtonUploadFile;

	@FindBy(xpath="//div[text()='Select the file to upload']")
	private List<WebElement> ErrorMessageUploadFile;

	@FindBy(xpath="//th[@data-field='Name']")
	private WebElement ListViewName;

	@FindBy(xpath="//th[@data-field='Permissions']")
	private WebElement ListViewPermissions;

	@FindBy(xpath="//th[@data-field='Modified']")
	private WebElement ListViewDate;

	@FindBy(xpath="//th[@data-field='Size']")
	private WebElement ListViewSize;

	@FindBy(xpath="//th[@data-field='Kind']")
	private WebElement ListViewType;

	@FindBy(xpath="//span[text()='Confirmation required']")
	private List<WebElement> DeleteHeader;

	@FindBy(xpath="(//button[text()='Cancel'])[last()]")
	private WebElement CancelButtonDeleteFile;

	@FindBy(xpath="(//button[text()='Ok'])[last()]")
	private WebElement OkButtonDeleteFile;

	@FindBy(xpath="//strong[text()='Selected File(s) will be deleted. Do you wish to continue?']")
	private List<WebElement> DeleteSummary;

	@FindBy(xpath="//div[@class='ui-dialog-buttonset']/button[text()='Ok']")
	private WebElement FileUploadOKButton;
	
	@FindBy(id="rclipboard")
	private WebElement ClipBoardTextArea;

	public void ClickOnRemoteSupport() throws InterruptedException
	{   HomeBtton.click();
		PARENTWINDOW=Initialization.driver.getWindowHandle();
		RemoteSupportBtn.click();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Reporter.log("PASS >> Clicked On Remote Support at: "+dateFormat.format(date),true);
		//sleep(5);
	}	
	public void windowhandles()
	{
		try {
			Set<String> set=Initialization.driver.getWindowHandles();
			Iterator<String> id=set.iterator();
			parentwinID =id.next();
			childwinID =id.next();
		} catch (Exception e) {
			WebElement OfflineMessage = Initialization.driver.findElement(By.xpath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']"));
			if(OfflineMessage.isDisplayed())
			{
				String FailMessage="Could not take remote support as the Device is offline";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}

	public void SwitchToRemoteWindow() throws InterruptedException
	{
		Initialization.driver.switchTo().window(childwinID);
		try
		{
		while(Initialization.driver.findElement(By.xpath("//div[@id='fountainG']")).isDisplayed())
		{
			sleep(1);
		}
		}
		catch (Exception e) {
		}
		//waitForXpathPresent("//a[text()='File']");
		sleep(10);
		URL=Initialization.driver.getCurrentUrl();

	}

	public void SwitchToMainWindow() throws InterruptedException
	{
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		sleep(10);
	}

	public void ExpandFileExplorerIE() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("expandButton")).click();
		sleep(4);
	}

	public void FailureRecover() throws Throwable
	{
		if (Initialization.driver.getWindowHandle().equals(childwinID)) {
			SwitchToMainWindow();
			Initialization.commonmethdpage.refreshpage();
			Initialization.commonmethdpage.SearchDevice(Config.DeviceName);
			ClickOnRemoteSupport();
			windowhandles();
			SwitchToRemoteWindow();
			VerifyOfRemoteSupportLaunched();
		} else {
			Initialization.commonmethdpage.refreshpage();
			Initialization.commonmethdpage.SearchDevice(Config.DeviceName);
			ClickOnRemoteSupport();
			windowhandles();
			SwitchToRemoteWindow();
			VerifyOfRemoteSupportLaunched();
		}
	}

	public void VerifyOfRemoteSupportLaunched() throws InterruptedException
	{
		try
		{
			while(LoadingBarOfRemoteSupport.isDisplayed())
			{

			}
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='rexplorer']/div[1]/ul/li[4]")));
			sleep(2);
			//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			//Date date = new Date();
			//Reporter.log("Remote support launched at: "+dateFormat.format(date),true);
			Reporter.log("PASS >> Remote Support Launched Successfully",true);
		}
		catch(Exception e)
		{
			String ErrorMessage = Initialization.driver.findElement(By.xpath("//p[@id='errorMessage']")).getText();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println("FAIL >> Error message occured: "+ErrorMessage +"at: " +dateFormat.format(date));

			if(ErrorMessage.equalsIgnoreCase("Error establishing remote connection with device. Please try again."));
			{
				String FailMessage = "Failed to take remote support as it throws the above error";
				ALib.AssertFailMethod(FailMessage);
			} 


		}
	}

	public void VerifyOfSwitchToOldRemoteSupport()
	{
		boolean value = URL.contains("OldRemoteSupport.aspx");
		String PassStatement="PASS >> Old Remote Support is opened successfully when Use Old Remote Support option is checked ";
		String FailStatement="FAIL >> Old Remote Support is not opened when Use Old Remote Support option is checked ";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfSwitchToNewRemoteSupport()
	{
		boolean value = URL.contains("OldRemoteSupport.aspx");
		String PassStatement="PASS >> New Remote Support is opened successfully when Use Old Remote Support option is Unchecked ";
		String FailStatement="FAIL >> New Remote Support is not opened when Use Old Remote Support option is Unchecked ";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRemoteSupportwindow() throws Throwable
	{
		boolean value=DeviceName.isDisplayed();
		String ActualValue=DeviceName.getText();
		String ExpectedValue="Device: "+Config.DeviceName;
		value= value && (ActualValue.equals(ExpectedValue));
		String PassStatement="PASS >> Device Name is displayed successfully as "+ExpectedValue+" on Device screen when clicked on Remote Support";
		String FailStatement="PASS >> Device Name is not displayed as "+ExpectedValue+" on Device screen even when clicked on Remote Support";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		ActualValue=DeviceOnlineOfflineStatus.getText();
		ExpectedValue="Online";
		PassStatement="PASS >> "+ActualValue+" status is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
		FailStatement="PASS >> "+ActualValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

		WebElement[] RemoteElement={FileTab,ClipBoardTab,TaskManagerTab,PowerButton,BackButton,HomeButton,MenuButton,OnlineOfflineStatus,SettingsBtn,PlayPauseBtn,VolumeUpBtn,VolumeDownBtn,
				Searchbutton,CaptureScreenshotBtn,CreateFolder,Reload,Back,Remove,UploadFiles,Download,ViewAsIcons,ViewAsList};
		String[] Remotetext= {"File Tab","ClipBoard Tab","Task Manager Tab","Power Button","Back Button","Home Button","Menu Button","Online Offline Status","Settings Button","Play/Pause Button","Volume Up Button","Volume Down Button",
				"Search Button","Capture Screenshot Button","Create Folder Button","Reload Button","Back Button","Remove Button","UploadFiles Button","Download Button","ViewAsIcons Button","ViewAsList Button"};
		for(int i=0;i<RemoteElement.length;i++)
		{
			value=RemoteElement[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext[i]+" is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
			FailStatement="PASS >> "+Remotetext[i]+" is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnClipBoardTab();
		WebElement[] RemoteElement1={ReadClipboardBtn,WriteClipboardBtn,ClearFromDeviceClipboardBtn};
		String[] Remotetext1= {"Read from Device ClipBoard button","Send To Device ClipBoard Button","Clear from Device ClipBoard Button"};
		for(int i=0;i<RemoteElement1.length;i++)
		{
			value=RemoteElement1[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext1[i]+" is displayed successfully on Remote screen when clicked on ClipBoard Tab";
			FailStatement="PASS >> "+Remotetext1[i]+" is not displayed on Device screen even when clicked on ClipBoard Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnTaskManagerTab();
		WebElement[] RemoteElement2={refreshProcessBtn,KillProcessBtn,killAllProcessBtn};
		String[] Remotetext2= {"Refresh List Button","Kill Process Button","Kill All Process Button"};
		for(int i=0;i<RemoteElement2.length;i++)
		{
			value=RemoteElement2[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext2[i]+" is displayed successfully on Remote screen when clicked on Task Manager Tab";
			FailStatement="PASS >> "+Remotetext2[i]+" is not displayed on Device screen even when clicked on Task Manager Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnFileTab();
	}


	public void ClickOnClipBoardTab() throws Throwable
	{
		ClipBoardTab.click();
		System.out.println("Clicked on Clipboard'");
		sleep(2);
		waitForidPresent("readClipboard");
		Reporter.log("Navigates successfully to ClipBoard Tab",true);
	}

	public void ClickOnTaskManagerTab() throws Throwable
	{
		TaskManagerTab.click();
		sleep(2);
		while(Initialization.driver.findElement(By.xpath("//div[@id='progressbar2']/i")).isDisplayed())
		{
			sleep(2);
		}
		waitForidPresent("killProcess");
		Reporter.log("Clicked on TaskManager Tab , Navigates successfully to TaskManager Tab",true);
	}

	public void ClickOnFileTab() throws Throwable
	{
		FileTab.click();
		sleep(6);
		//waitForidPresent("device_name");
		Reporter.log("Clicked on File Tab , Navigates successfully to File Tab",true);

	}

	public int CountNoOfFolders()
	{
		return NoOfFolders.size();
	}

	public void ClickingOnCDrive() throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//a[text()='C:\\']")).click();
		ClickOnReload();
	}
	public void ClickOnNewFolder() throws Throwable
	{

		CreateFolder.click();
		waitForXpathPresent("//li[@title='New folder']");
		sleep(3);
		Reporter.log("Clicking on New Folder",true);
	}

	public void CreateNewFolder() throws Throwable
	{
		int initialFoldervalue=CountNoOfFolders();
		System.out.println(initialFoldervalue);
		ClickOnNewFolder();
		RemoteSupportFileSpace.click();
		sleep(5);
		int ActualfinalFoldervalue=CountNoOfFolders();
		System.out.println(ActualfinalFoldervalue);
		int ExpectedFinalFoldervalue=initialFoldervalue+1;
		boolean value=(ActualfinalFoldervalue==ExpectedFinalFoldervalue);
		String PassStatement="PASS >> New folder is created successfully when clicked on NewFolder Button";
		String FailStatement="FAIL >> New folder is not created even when clicked on NewFolder Button";;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void RenameNewFolder(String FolderName) throws Throwable
	{
		ClickOnNewFolder();
		sleep(5);
		FolderNameTitle.sendKeys(FolderName);
		sleep(5);
		RemoteSupportFileSpace.click();
		waitForXpathPresent("//li[@title='New folder']");
		sleep(5);
		boolean value = Initialization.driver.findElement(By.xpath("//label[text()='" + FolderName + "']")).isDisplayed();
		String PassStatement = "PASS >> Folder is renamed successfully as " + FolderName+" when Newfolder is created";
		String FailStatement = "FAIL >> Folder is not renamed as " + FolderName+ " even when Newfolder is created";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void RenameFolder_Error(String FolderName) throws Throwable
	{
		ClickOnNewFolder();
		sleep(5);
		FolderNameTitle.sendKeys(FolderName);
		RemoteSupportFileSpace.click();
		Reporter.log("Renaming New Folder",true);
		boolean value=SameFolderNameError.isDisplayed();
		String text=SameFolderNameError.getText();
		String PassStatement="PASS >> Error Message is displayed successfully when Creating folders with name already exist as "+text;
		String FailStatement="FAIL >> Error Message is not displayed even when Creating folders with name already exist as "+text;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		FolderNameTitle.sendKeys("SameFolder");
		Reporter.log("Renaming New Folder",true);
		sleep(5);
		RemoteSupportFileSpace.click();
		sleep(5);
	}

	public void ClickOnFile(String FolderName) throws Throwable
	{
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[label[text()='"+FolderName+"']]"));
		Folder.click();
		sleep(5);
	}

	public void DoubleClickOperation(String FolderName) throws Throwable
	{
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[label[text()='"+FolderName+"']]"));
		ClickOnFile(FolderName);
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		while(LoadingFolder.isDisplayed()){}
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
		String ActualValue = FolderPath.getText();
		String ExpectedValue = "/storage/emulated/0/" + FolderName;
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnBack() throws Throwable
	{
		Back.click();
		while(LoadingFolder.isDisplayed()){}
		waitForXpathPresent("//li[@title='New folder']");
		Reporter.log("Click on Back Button", true);
		sleep(5);
		String ActualValue = FolderPath.getText();
		String ExpectedValue = "/storage/emulated/0";
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully to previous folder when Clicked on Back button";
		String FailStatement = "FAIL >> Navigation to previous folder is failed even when Clicked on Back button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnReload() throws Throwable
	{
		while(Initialization.driver.findElement(By.xpath("//i[@class='spinIcn fa fa-spinner fa-spin']")).isDisplayed())
		{
		}
		Reload.click();
		while(LoadingFolder.isDisplayed()){}
		waitForXpathPresent("//li[@title='New folder']");
		Reporter.log("Click on Reload Button", true);
		sleep(10);
	}

	public void VerifyOfUploadFileHeader()
	{
		int UploadFilecount=UploadHeader.size()-1;
		boolean value=UploadHeader.get(UploadFilecount).isDisplayed();
		String PassStatement = "PASS >> Upload File Header is displayed successfully when clicked on Upload File Button";
		String FailStatement = "FAIL >> Upload File Header is not displayed even when clicked on Upload File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRemoteSupportwindowTest() throws Throwable
	{
		String ActualValue=DeviceOnlineOfflineStatus.getText();
		String ExpectedValue="Online";
		String PassStatement="PASS >> "+ExpectedValue+" status is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
		String FailStatement="FAIL >> "+ExpectedValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
		if(ActualValue.equalsIgnoreCase("Offline"))
		{
			String FailMessage ="FAIL >> "+ExpectedValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
			ALib.AssertFailMethod(FailMessage);
		}else {

			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		}

		WebElement[] RemoteElement={FileTab,ClipBoardTab,TaskManagerTab,OnlineOfflineStatus,SettingsBtn,PlayPauseBtn,VolumeUpBtn,VolumeDownBtn,
				Searchbutton,CaptureScreenshotBtn,CreateFolder,Reload,Back,Remove,UploadFiles,Download,ViewAsIcons,ViewAsList};
		String[] Remotetext= {"File Tab","ClipBoard Tab","Task Manager Tab","Online Offline Status","Settings Button","Play/Pause Button","Volume Up Button","Volume Down Button",
				"Search Button","Capture Screenshot Button","Create Folder Button","Reload Button","Back Button","Remove Button","UploadFiles Button","Download Button","ViewAsIcons Button","ViewAsList Button"};
		for(int i=0;i<RemoteElement.length;i++)
		{
			boolean value=RemoteElement[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext[i]+" is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
			FailStatement="FAIL >> "+Remotetext[i]+" is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			sleep(2);
		}
		ClickOnClipBoardTab();
		WebElement[] RemoteElement1={ReadClipboardBtn,WriteClipboardBtn,ClearFromDeviceClipboardBtn};
		String[] Remotetext1= {"Read from Device ClipBoard button","Send To Device ClipBoard Button","Clear from Device ClipBoard Button"};
		for(int i=0;i<RemoteElement1.length;i++)
		{
			boolean value=RemoteElement1[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext1[i]+" is displayed successfully on Remote screen when clicked on ClipBoard Tab";
			FailStatement="PASS >> "+Remotetext1[i]+" is not displayed on Device screen even when clicked on ClipBoard Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnTaskManagerTab();
		WebElement[] RemoteElement2={refreshProcessBtn,KillProcessBtn,killAllProcessBtn};
		String[] Remotetext2= {"Refresh List Button","Kill Process Button","Kill All Process Button"};
		for(int i=0;i<RemoteElement2.length;i++)
		{
			boolean value=RemoteElement2[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext2[i]+" is displayed successfully on Remote screen when clicked on Task Manager Tab";
			FailStatement="PASS >> "+Remotetext2[i]+" is not displayed on Device screen even when clicked on Task Manager Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnFileTab();
		System.out.println("");
	}

	public void VerifyOfCancelButton_UploadFile()
	{
		int CancelButtoncount=CancelButtonUploadFile.size()-1;
		boolean value=CancelButtonUploadFile.get(CancelButtoncount).isDisplayed();
		String PassStatement = "PASS >> Cancel Button is displayed successfully when clicked on Upload File Button";
		String FailStatement = "FAIL >> Cancel Button is not displayed even when clicked on Upload File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnCancelButton_UploadFile()
	{
		int CancelButtoncount=CancelButtonUploadFile.size()-1;
		CancelButtonUploadFile.get(CancelButtoncount).click();
		waitForXpathPresent("//li[@title='Upload files']");
		Reporter.log("Clicked on Cancel Button of Upload File",true);
	}

	public void VerifyOfOkButton_UploadFile()
	{
		int OkButtoncount=OkButtonUploadFile.size()-1;
		boolean value=OkButtonUploadFile.get(OkButtoncount).isDisplayed();
		String PassStatement = "PASS >> Ok Button is displayed successfully when clicked on Upload File Button";
		String FailStatement = "FAIL >> Ok Button is not displayed even when clicked on Upload File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnOkButton_UploadFile()
	{
		int OkButtoncount=OkButtonUploadFile.size()-1;
		OkButtonUploadFile.get(OkButtoncount).click();
		waitForXpathPresent("//li[@title='Upload files']");
		Reporter.log("Clicked on Ok Button of Upload File",true);
	}

	public void ClickOnOkButton_UploadFile_Error()
	{
		int OkButtoncount=OkButtonUploadFile.size()-1;
		OkButtonUploadFile.get(OkButtoncount).click();
		Reporter.log("Clicked on Ok Button of Upload File",true);
	}

	public void VerifyOfBrowseFileButton()
	{
		int BrowseFilecount=BrowseFileButtonUploadFile.size()-1;
		boolean value=BrowseFileButtonUploadFile.get(BrowseFilecount).isDisplayed();
		String PassStatement = "PASS >> Browse File Button is displayed successfully when clicked on Upload File Button";
		String FailStatement = "FAIL >> Browse File Button is not displayed even when clicked on Upload File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnBrowseFileButton(String FileName) throws Throwable
	{
		int BrowseFilecount=BrowseFileButtonUploadFile.size()-1;
		BrowseFileButtonUploadFile.get(BrowseFilecount).click();
		sleep(5);
		Runtime.getRuntime().exec(FileName);
		sleep(5);
	}

	public void VerifyOfUploadFileErrorMessage()
	{
		int ErrorMessageUploadFilecount=ErrorMessageUploadFile.size()-1;
		boolean value=ErrorMessageUploadFile.get(ErrorMessageUploadFilecount).isDisplayed();
		String text=ErrorMessageUploadFile.get(ErrorMessageUploadFilecount).getText();
		String PassStatement = "PASS >> Error Message "+text+" is displayed successfully when clicked on Ok Button of Upload file without browsing any file";
		String FailStatement = "FAIL >>Error Message "+text+" is not displayed even when clicked on Ok Button of Upload file without browsing any file";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnUploadFiles() throws Throwable
	{
		UploadFiles.click();
		sleep(5);
		Reporter.log("Clicked on Upload File Button of Remote Support",true);
	}

	public boolean VerifyOfUploadFile(String FileName)
	{
		boolean value=true;
		try{
			Initialization.driver.findElement(By.xpath("//div[div[contains(text(),'"+FileName+"')]]")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyOfCancelUploadFile(String FileName)
	{
		boolean value=VerifyOfUploadFile(FileName);
		String PassStatement = "PASS >> File is not uploaded when clicked on Cancel Button of Upload file";
		String FailStatement = "FAIL >> File is uploaded even when clicked on Cancel Button of Upload file";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfOkUploadFile(String FileName) throws Throwable
	{
		boolean value = false;
		int i = 10;
		while (i >= 10) {
			try {
				WebElement UploadElement = Initialization.driver.findElement(By.xpath("//div[text()='" + FileName
						+ "']/following-sibling::div/span[@class='progress-label' and text()='Uploaded!']"));
				UploadElement.isDisplayed();
				System.out.println(UploadElement.getText());
				i = 0;
				value = true;
			} catch (Exception e) {
				value = false;
				i++;
				if (i == 115500) {
					i = 0;
				}
			}
		}
		value = VerifyOfUploadFile(FileName);
		String PassStatement = "PASS >> File is uploaded successfully when clicked on Ok Button of Upload file";
		String FailStatement = "FAIL >> File is not uploaded even when clicked on Ok Button of Upload file";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		waitForXpathPresent("//li[@title='Reload']");
		sleep(10);
	}

	public void CheckDownloads(String FileName,String DownloadPath) throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath =DownloadPath ;
		String fileName = FileName;
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		Download.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> File is Downloaded successfully when clicked on Download button";
		String FailStatement="Fail >> File is not Downloaded when clicked on Download button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}

	public void MultipleDownloads(String[] Downloadtext) throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = Config.downloadpath;
		String fileName = ".zip";
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().endsWith(fileName)) {
				initial++;
			}
		}

		for (int i = 0; i < Downloadtext.length; i++) {
			WebElement Folder = Initialization.driver.findElement(By.xpath("//div[label[text()='" + Downloadtext[i] + "']]"));
			((JavascriptExecutor) Initialization.driver).executeScript("arguments[0].scrollIntoView(true);", Folder);
			sleep(2);
			new Actions(Initialization.driver).keyDown(Keys.CONTROL).click(Folder).keyUp(Keys.CONTROL).perform();
		}

		Download.click();
		while(LoadingFolder.isDisplayed())
		{}
		waitForXpathPresent("//li[@title='Upload files']");
		waitForXpathPresent("//li[@title='Upload files']");
		sleep(15);
		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(fileName)) {
				result++;
			}
		}
		boolean value=(result==initial);
		String PassStatement="PASS >> Multiple File is Downloaded successfully in zip format when clicked on Download button";
		String FailStatement="Fail >> Multiple File is not Downloaded even when clicked on Download button";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnListView() throws Throwable
	{
		ViewAsList.click();
		while(LoadingFolder.isDisplayed()){}
		sleep(5);
		Reporter.log("Clicked on View as List button",true);
	}

	public void VerifyOfListViewColumn()
	{
		WebElement[] RemoteSupportElement={ListViewName,ListViewPermissions,ListViewDate,ListViewSize,ListViewType};
		String[] ElementsText={"Name","Permissions","Modified","Size","Type"};
		for(int i=0;i<RemoteSupportElement.length;i++)
		{
			boolean value=RemoteSupportElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on List View Button of File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on List View Button of File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnIconView() throws Throwable
	{
		ViewAsIcons.click();
		while(LoadingFolder.isDisplayed()){}
		sleep(5);
		Reporter.log("Clicked on View as Icon button",true);
	}

	public void VerifyOfIconViewColumn()
	{
		WebElement[] RemoteSupportElement={ListViewName,ListViewPermissions,ListViewDate,ListViewSize,ListViewType};
		String[] ElementsText={"Name","Permissions","Modified","Size","Type"};
		for(int i=0;i<RemoteSupportElement.length;i++)
		{
			boolean value = true;
			try{
				RemoteSupportElement[i].isDisplayed();
			}catch(Exception e)
			{
				value=false;
			}
			String PassStatement="PASS >> "+ElementsText[i]+" is not displayed when clicked on List Icon Button of Remote Support";
			String FailStatement="FAIL >> "+ElementsText[i]+" is displayed even when clicked on List Icon Button of Remote Support";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnDelete() throws Throwable
	{
		Remove.click();
		sleep(5);
		Reporter.log("Clicked on Remove Button of Remote Support",true);
	}

	public void VerifyOfDeleteFileHeader()
	{
		int Deletecount=DeleteHeader.size()-1;
		boolean value=DeleteHeader.get(Deletecount).isDisplayed();
		String PassStatement = "PASS >> Delete File Header is displayed successfully when clicked on Delete File Button";
		String FailStatement = "FAIL >> Delete File Header is not displayed even when clicked on Delete File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfCancelButton_DeleteFile()
	{
		boolean value=CancelButtonDeleteFile.isDisplayed();
		String PassStatement = "PASS >> Cancel Button is displayed successfully when clicked on Delete File Button";
		String FailStatement = "FAIL >> Cancel Button is not displayed even when clicked on Delete File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnCancelButton_DeleteFile()
	{
		CancelButtonDeleteFile.click();
		waitForXpathPresent("//li[@title='Upload files']");
		Reporter.log("Clicked on Cancel Button of Upload File",true);
	}

	public void VerifyOfOkButton_DeleteFile()
	{
		boolean value=OkButtonDeleteFile.isDisplayed();
		String PassStatement = "PASS >> Ok Button is displayed successfully when clicked on Delete File Button";
		String FailStatement = "FAIL >> Ok Button is not displayed even when clicked on Delete File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnOkButton_DeleteFile() throws Throwable
	{
		OkButtonDeleteFile.click();
		while(LoadingFolder.isDisplayed()){}
		waitForXpathPresent("//li[@title='Upload files']");
		sleep(5);
		Reporter.log("Clicked on Ok Button of Delete File",true);
	}

	public void VerifyOfDeleteSummary()
	{
		int DeleteSummarycount=DeleteSummary.size()-1;
		boolean value=DeleteSummary.get(DeleteSummarycount).isDisplayed();
		String PassStatement = "PASS >> Delete Summary is displayed successfully as "+DeleteSummary.get(DeleteSummarycount).getText()+" when clicked on Delete File Button";
		String FailStatement = "FAIL >> Delete Summary is not displayed even when clicked on Delete File Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public boolean VerifyOfFileOrFolder(String FileName)
	{
		boolean value=true;
		try{
			Initialization.driver.findElement(By.xpath("//div/label[text()='"+FileName+"']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyOfDeleteFile_True(String FileName)
	{
		boolean value=VerifyOfFileOrFolder(FileName);
		String PassStatement="PASS >> "+FileName+" is not deleted when Clicked on Cancel Button of Delete Popup";
		String FailStatement="FAIL >> "+FileName+" is deleted even when Clicked on Cancel Button of Delete Popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfDeleteFile_False(String FileName)
	{
		boolean value=VerifyOfFileOrFolder(FileName);
		String PassStatement="PASS >> "+FileName+" is deleted successfully when Clicked on Ok Button of Delete Popup";
		String FailStatement="FAIL >> "+FileName+" is not deleted even when Clicked on Ok Button of Delete Popup";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void MultipleDelete(String[] Deletetext) throws Throwable
	{
		for (int i = 0; i < Deletetext.length; i++) {
			WebElement wb = Initialization.driver.findElement(By.xpath("//div/label[text()='"+Deletetext[i]+"']"));
			sleep(2);
			new Actions(Initialization.driver).keyDown(Keys.CONTROL).click(wb).keyUp(Keys.CONTROL).perform();
		}
		sleep(2);
	}

	public void VerifyOfGrayedOut()
	{
		WebElement[] RemoteElement={Back,Remove,Download};
		String[] Remotetext= {"Back button","Remove Button","Download Button"};
		for(int i=0;i<RemoteElement.length;i++)
		{
			boolean value=RemoteElement[i].isDisplayed();
			String PassStatement="PASS >> "+Remotetext[i]+" is grayed out successfully on Remote screen when no file is selected";
			String FailStatement="PASS >> "+Remotetext[i]+" is not grayed out on Device screen even when no file is selected";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void EnterClipBoardTextbox()
	{
		ClipboardTextBox.sendKeys("sandhya");
	}

	public void ClearClipBoardTextbox()
	{
		ClipboardTextBox.clear();
	}

	public void ClickOnSendToDeviceClipboard() throws Throwable
	{
		WriteClipboardBtn.click();
		sleep(2);
		Reporter.log("Clicked on Send To Device ClipBoard", true);
	}

	public void ClickOnReadfromClipboard() throws Throwable
	{
		ReadClipboardBtn.click();
		sleep(2);
		Reporter.log("Clicked on Read from Device ClipBoard", true);
	}

	public void ClickOnClearFromDeviceClipboard() throws Throwable
	{
		ClearFromDeviceClipboardBtn.click();
		sleep(2);
		Reporter.log("Clicked on Clear From Device Clipboard", true);
	}

	public void VerifyOfReadFromDeviceClipBoard(String Value)
	{
		String ActualValue=ClipboardTextBox.getAttribute("value");
		String ExpectedValue=Value;
		String PassStatement="PASS >> The Text is read successfully from device when Clicked on Read from Device ClipBoard";
		String FailStatement="FAIL >> The Text is not read from device even when Clicked on Read from Device ClipBoard";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfClearFromDeviceClipBoard()
	{
		String ActualValue=ClipboardTextBox.getAttribute("value");
		boolean value=ActualValue.equals("");
		String PassStatement="PASS >> The Text is cleared successfully from device when Clicked on Clear from Device ClipBoard";
		String FailStatement="FAIL >> The Text is not cleared from device even when Clicked on Clear from Device ClipBoard";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnApplication(String AppName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//td[text()='"+AppName+"']")).click();
		sleep(5);
	}

	public void ClickOnKillProcess() throws Throwable
	{
		KillProcessBtn.click();
		while(LoadingFolder.isDisplayed()){}
		sleep(5);
		Reporter.log("Clicked on Kill Process button", true);
	}

	public void ClickOnRefreshList() throws Throwable
	{
		refreshProcessBtn.click();
		while(LoadingFolder.isDisplayed()){}
		sleep(5);
		Reporter.log("Clicked on Refresh List button", true);
	}

	public boolean VerifyOfApplication(String AppName) throws Throwable
	{
		boolean value=true;
		try{
			Initialization.driver.findElement(By.xpath("//td[text()='"+AppName+"']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyOfKillProcessApplication(String FileName)
	{
		boolean value=VerifyOfUploadFile(FileName);
		String PassStatement = "PASS >> Application is Killed successfully when clicked on Kill Process Button";
		String FailStatement = "FAIL >> Application is not Killed even when clicked on Kill Process Button";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRefreshProcessApplication(String FileName)
	{
		boolean value=VerifyOfUploadFile(FileName);
		String PassStatement = "PASS >> Task Manager is refreshed successfully when clicked on Refresh List Button";
		String FailStatement = "FAIL >> Task Manager is not refreshed even when clicked on Refresh List Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void CheckScreenshots(String FileName,String DownloadPath) throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = DownloadPath;
		String fileName =FileName;
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		CaptureScreenshotBtn.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> screenshot.webp Downloaded successfully";
		String FailStatement="Fail >> screenshot.webp is not Downloaded";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	@FindBy(xpath="//i[@class='spinIcn fa fa-spinner fa-spin']")
	private WebElement LoadingSymbol1;

	@FindBy(xpath="(//div[text()='Loading...'])[2]")
	private WebElement LoadingSymbol2;
	public void VerifyOfRemoteSupportwindow_Windows() throws Throwable
	{


		boolean value=DeviceName.isDisplayed();
		String ActualValue=DeviceName.getText();
		String ExpectedValue="Device: "+Config.Windows_DeviceName1;
		value= value && (ActualValue.equals(ExpectedValue));
		String PassStatement="PASS >> Device Name is displayed successfully as "+ExpectedValue+" on Device screen when clicked on Remote Support";
		String FailStatement="PASS >> Device Name is not displayed as "+ExpectedValue+" on Device screen even when clicked on Remote Support";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		ActualValue=DeviceOnlineOfflineStatus.getText();
		ExpectedValue="Online";
		PassStatement="PASS >> "+ActualValue+" status is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
		FailStatement="PASS >> "+ActualValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

		WebElement[] RemoteElement={FileTab,ClipBoardTab,TaskManagerTab,HomeButton,OnlineOfflineStatus,PlayPauseBtn,VolumeUpBtn,VolumeDownBtn,
				ControlALTDElBtn,CreateFolder,Reload,Back,Remove,UploadFiles,Download,ViewAsIcons,ViewAsList};
		String[] Remotetext= {"File Tab","ClipBoard Tab","Task Manager Tab","Home Button of Windows","Menu Button","Online Offline Status","Play/Pause Button","Volume Up Button","Volume Down Button",
				"Control ALT DEL Btn","Create Folder Button","Reload Button","Back Button","Remove Button","UploadFiles Button","Download Button","ViewAsIcons Button","ViewAsList Button"};
		for(int i=0;i<RemoteElement.length;i++)
		{
			value=RemoteElement[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext[i]+" is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
			FailStatement="PASS >> "+Remotetext[i]+" is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnClipBoardTab();
		WebElement[] RemoteElement1={ReadClipboardBtn,WriteClipboardBtn,ClearFromDeviceClipboardBtn};
		String[] Remotetext1= {"Read from Device ClipBoard button","Send To Device ClipBoard Button","Clear from Device ClipBoard Button"};
		for(int i=0;i<RemoteElement1.length;i++)
		{
			value=RemoteElement1[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext1[i]+" is displayed successfully on Remote screen when clicked on ClipBoard Tab";
			FailStatement="PASS >> "+Remotetext1[i]+" is not displayed on Device screen even when clicked on ClipBoard Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnTaskManagerTab();
		WebElement[] RemoteElement2={refreshProcessBtn,KillProcessBtn};
		String[] Remotetext2= {"Refresh List Button","Kill Process Button"};
		for(int i=0;i<RemoteElement2.length;i++)
		{
			value=RemoteElement2[i].isDisplayed();
			PassStatement="PASS >> "+Remotetext2[i]+" is displayed successfully on Remote screen when clicked on Task Manager Tab";
			FailStatement="PASS >> "+Remotetext2[i]+" is not displayed on Device screen even when clicked on Task Manager Tab";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		ClickOnFileTab();
	}

	public void DoubleClickOperation_Windows(String FolderName) throws Throwable
	{
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[label[text()='"+FolderName+"']]"));
		ClickOnFile(FolderName);
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		while(LoadingFolder.isDisplayed()){}
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
		String ActualValue = FolderPath.getText();
		String ExpectedValue = "C:\\"+ FolderName;
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnBack_Windows() throws Throwable
	{
		Back.click();
		while(LoadingFolder.isDisplayed()){}
		waitForXpathPresent("//li[@title='New folder']");
		Reporter.log("Click on Back Button", true);
		sleep(5);
		String ActualValue = FolderPath.getText();
		String ExpectedValue = "C:\\";
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully to previous folder when Clicked on Back button";
		String FailStatement = "FAIL >> Navigation to previous folder is failed even when Clicked on Back button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnFile_Windows(String FolderName) throws Throwable
	{
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[label[text()='"+FolderName+"']]"));
		for(int i=1;i<=10;i++){
			try{
				Folder.click();
				break;
			}catch(Exception e)
			{
				((JavascriptExecutor) Initialization.driver).executeScript("arguments[0].scrollIntoView(true);", Folder);
				Folder.click();
				sleep(5);
			}
		}
	}

	public void VerifyDeviceOnline()
	{
		String Actual=onlineofflinestatus.getText();
		String Expected="Online";
		ALib.AssertEqualsMethod(Expected, Actual, "PASS >> Device is Online in Remote Support Window", "FAIL >> Device is went Offline");
	}

	@FindBy(xpath="(//td[@class='ConnectionStatus']/div/span)[2]")
	private WebElement onlineofflinestatus;

	@FindBy(xpath="//div[@class='dataUsage_display_row']/p/span[1]")
	private WebElement RemoteSupportPageDataUsageRow;

	@FindBy(xpath="//div[@class='dataUsage_display_row']/p/span[2]")
	private WebElement RemoteSupportPageDataUsageRowDeviceUpload;

	@FindBy(xpath="//div[@class='dataUsage_display_row']/p/span[3]")
	private WebElement RemoteSupportPageDataUsageRowDeviceDownload;

	@FindBy(xpath="//div[@class='dataUsage_display_row']/p/span[4]")
	private WebElement RemoteSupportPageDataUsageRowDeviceTotal;

	@FindBy(xpath="//div[@id='powerButton']")
	private WebElement RemoteSupportPageDevicePowerButton;

	@FindBy(xpath="//p[text()='Remote device screen capture paused. Click to resume.']")
	private WebElement RemoteScreenPauseMessage;

	@FindBy(xpath="//div[@id='playPauseButton']/i")
	private WebElement PlayPasueButtonClass;

	@FindBy(xpath="//span[text()='�']")
	private WebElement UploadCancelButton;

	@FindBy(xpath="//div[@class='file-upload-container']")
	private WebElement UploadStatusTab;

	@FindBy(name="icons")
	private WebElement ViewAsIconButton;

	@FindBy(id="nevershow_again")
	private WebElement DontPauseScreenCheckBox;

	@FindBy(xpath="//button[text()='Resume']")
	private WebElement ScreenCaptureResumeButton;

	@FindBy(xpath="//input[@id='IsZipAllDownloadsRemote']")
	private WebElement ZipAllDownloadCheckBox;

	@FindBy(xpath="//input[@id='miscellaneous_apply']")
	private WebElement MiscellaneousApplyButton;

	@FindBy(xpath="//span[text()='Uploaded!']")
	private WebElement UploadedStatus;

	@FindBy(id="refreshProcess")
	private WebElement RemoteSupportTaskManagerRefreshButton;




	public void VerifyDataUsageInRemoteSupport()
	{
		boolean flag;
		WebElement[] RemoteSupportPageDataUsageElements= {RemoteSupportPageDataUsageRowDeviceUpload,RemoteSupportPageDataUsageRowDeviceDownload,RemoteSupportPageDataUsageRowDeviceTotal};
		for(int i=0;i<RemoteSupportPageDataUsageElements.length;i++)
		{
			if(RemoteSupportPageDataUsageElements[i].isDisplayed())
			{

				String DataUsageElements=RemoteSupportPageDataUsageElements[i].getText();
				if(DataUsageElements.matches("[0-9]+") && DataUsageElements.length() >2)
				{
					flag=false;
					Reporter.log(DataUsageElements,true);
				}
				else
				{
					flag=true;
					Reporter.log(DataUsageElements,true);
				}
				String Pass="Data Usage Row Is Displayed In Remote Support Page";
				String Fail="Data Usage Row Is Not Displayed In Remote Support Page";
				ALib.AssertTrueMethod(flag, Pass, Fail);
			}

		}
	}

	public void ClickingOnPowerButtonInRemoteScreen() throws InterruptedException
	{
		waitForXpathPresent("//div[@id='powerButton']");
		sleep(2);
		Actions act=new Actions(Initialization.driver);
		act.click(PowerButton).build().perform();
		Reporter.log("Clicked On Power BUtton",true);
	}

	public void WakingUpDevice() throws InterruptedException, IOException
	{
		Runtime.getRuntime().exec("adb shell input keyevent KEYCODE_WAKEUP");
		sleep(2);
	}

	public void SwipingTheScreenUp() throws Exception
	{
		Runtime.getRuntime().exec("adb shell input keyevent 82");
		sleep(2);
	}

	public void UnlockingDeviceThroughPassword() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell input keyevent 82 && adb shell input text 00000 && adb shell input keyevent 66");
	}
	
	public void VerifydeviceUnlocked()
	{
		try
		{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").isDisplayed();
	    Reporter.log("Device Is Unlocked Successfully",true);
		}
		catch (Exception e) 
		{
			ALib.AssertFailMethod("Device Is Not UnLocked");
		}
	}

	public void ClickingOnPlayPauseButton() throws InterruptedException
	{
		waitForidClickable("playPauseButton");
		sleep(5);
		PlayPauseBtn.click();
		sleep(3);
		waitForXpathPresent("//p[text()='Remote device screen capture paused. Click to resume.']");
		Reporter.log("Clicked On PlayPause Button",true);
	}

	public void VerifyScreenAfterClickingPauseButton()
	{
		String PalyButtonAttribute = PlayPasueButtonClass.getAttribute("Class");
		System.out.println(PalyButtonAttribute);
		boolean flag;
		try
		{
			flag=RemoteScreenPauseMessage.isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS>>>Remote Screen Pause Message Is Displayed On Clicking On Pause Button i.e We Cannot Perform Any Action On Remote Screen";
		String Fail="FAIL>>>Remote Screen Pause Message Is Not Displayed On Clicking On Pause Button";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyPlayPauseSymbol()
	{
		boolean flag;
		String PalyButtonAttribute = PlayPasueButtonClass.getAttribute("Class");
		if(PalyButtonAttribute.equals("icn fa fa-pause"))
		{
			flag=true;
			Reporter.log("PASS>>>Play Button Symbol Is Changed To Pause Button After Clicking On Play Button",true);
		}
		else if(PalyButtonAttribute.equals("icn fa fa-play"))
		{
			flag=true;
			Reporter.log("PASS>>>Pause Button Symbol Is Changed To Play Button After Clicking On Play Button",true);
		}
		else
		{
			flag=false;
		}
		String Pass="PASS>>>Play Button Action Working Correctly";
		String Fail="FAIL>>>Play Button Action Not Working Correctly";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}
	public void VerifyRemoteScreenAfterClickingOnPlayButton()
	{
		boolean flag;
		try
		{
			flag=RemoteScreenPauseMessage.isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS>>>Remote Screen Pause Message Is Not Displayed On Clicking On Play Button i.e Performed Actions Can Be Captured";
		String Fail="FAIL>>>Remote Screen Pause Message Is Displayed On Clicking On Play Button i.e We Cannot Perform Any Action On Remote Screen";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	public void ClickingOnUploadButtonInRemoteSupport() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//li[@title='Upload files']")).click();
		System.out.println("Clicked on Upload files button");
		sleep(3);
	}

	public void ClickingOnFileBrowseButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[text()='Browse file']")).click();
		System.out.println("Clicked on browse file button");
		sleep(4);
	}

	public void UploadingFileInRemoteSupport(String FilePath) throws InterruptedException, IOException
	{
		boolean flag;
		Runtime.getRuntime().exec(FilePath);
		waitForidPresent("fileSelector");
		System.out.println("File is browsed and selected");
		sleep(2);
		FileUploadOKButton.click();
		System.out.println("Clicked on OK of the upload file window");
		sleep(4);
		waitForXpathPresent("//div[@class='file-upload-container']");
		try
		{
			flag=UploadStatusTab.isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS>>>Upload Status Tab Is Displayed Successfully On Clicking File Upload Ok Button";
		String Fail="FAIL>>>Upload Status Tab Is Not Displayed Successfully On Clicking File Upload Ok Button";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyUploadStatusAfetrUploadingTheFile() throws InterruptedException
	{
		boolean flag;
		waitForXpathPresent("//span[text()='Uploaded!']");
		try
		{
			flag=UploadedStatus.isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String downloadStatus = Initialization.driver.findElement(By.xpath("//span[@class='progress-label']")).getText();
		String Pass="Uploaded Status Is Displayed On Completion Of File Uploading";
		String Fail="Uploaded Status Is Not Displayed On Completion Of File Uploading An dStatus Is" + " "+downloadStatus;
		ALib.AssertTrueMethod(flag, Pass, Fail);
		sleep(5);
	}
	public void ClickingOnUploadCancelButton()
	{
		boolean flag;
		UploadCancelButton.click();
		Reporter.log("Clicked On Upload Cancel Button",true);
		try 
		{
			flag=UploadStatusTab.isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS>>>Upload Status Tab Is Not Displayed On Clicking Upload Cancel Button";
		String Fail="FAIL>>>Upload Status Tab Is Displayed On Clicking Upload Cancel Button";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	public void ClickOnViewAsIconButton()
	{
		boolean flag;
		String ViewAsIconClass = ViewAsIconButton.getAttribute("Class");
		if(ViewAsIconClass.equals("icons disabled"))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="PASS>>>View As Icon Button Is Already Clicked";
		String Fail="FAIL>>>View As Icon Button Is Not Clicked";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}

	public void VerifyViewAsIconButtonInRemoteSupport()
	{
		boolean flag;
		String FolderClass = Initialization.driver.findElement(By.xpath("//div[@class='el-finder-cwd ui-selectable']/div[1]")).getAttribute("Class");
		System.out.println(FolderClass);
		if(FolderClass.equals("directory"))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="PASS>>>File Information Is Displayed In IconFormat";
		String Fail="FAIL>>>File Information Is Not Displayed In IconFormat";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyTasksInsideTaskManager()
	{
		boolean flag;
		List<WebElement> Tasks = Initialization.driver.findElements(By.xpath("//table[@id='plist']/tbody/tr"));
		List<WebElement> TaskNames = Initialization.driver.findElements(By.xpath("//table[@id='plist']/tbody/tr/td[1]"));
		if(Tasks.size()>1)
		{
			flag=true;
			for(int i=0;i<Tasks.size();i++)
			{
				String TaskName = TaskNames.get(i).getText();
				Reporter.log("Current Running Task Is"+" : "+TaskName,true);
			}
		}
		else
		{
			flag=false;
		}
		String Pass="PASS>>>Running Tasks Are Being Shown In Task Manager Tab";
		String Fail="FAIL>>>Running Tasks Are Not Being Shown In Task Manager Tab";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyClickOnTaskManagerRefreshButton() throws InterruptedException
	{
		RemoteSupportTaskManagerRefreshButton.click();
		sleep(5);
		Reporter.log("Clicked On Task Manager Refresh Button");
	}

	public void WaitingForScreenPauseMessage() throws InterruptedException
	{
		boolean flag;
		WebDriverWait wait=new WebDriverWait(Initialization.driver,50);
		try
		{
			Reporter.log("Waiting For Ideal TimeOut Screen Pause Message",true);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[contains(text(),'capture paused')]")));
			flag=true;
		}
		catch (Exception e) 
		{
			flag=false;
		}
		String Pass="PASS>>>Screen Pause Message Is Displayed Successfully";
		String Fail="FAIL>>>Screen Pasue Message Isn't Displayed";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		sleep(2);
	}

	public void CheckingDontPauseScreenCheckBox() throws InterruptedException
	{
		DontPauseScreenCheckBox.click();
		Reporter.log("Don't Pause Screen Capture Check Box Is Selected",true);
		sleep(2);
	}

	public void ClickingOnResumeScreenCaptureButton() throws InterruptedException
	{
		boolean flag;
		ScreenCaptureResumeButton.click();
		Reporter.log("Clicked On Resume Button",true);
		while(Initialization.driver.findElement(By.xpath("//i[@class='icn fa fa-spinner fa-spin']")).isDisplayed())
		{
		}
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//p[contains(text(),'capture paused')]")).isDisplayed();
		}
		catch (Exception e) 
		{
			flag=false;
		}
		String Pass="PASS>>>Screen Pause Message Is Not Displayed After Clicking On Resume Button";
		String Fail="FAIL>>>Screen Pause Message Is Displayed Even After Clicking On Resume Button";
		ALib.AssertFalseMethod(flag, Pass, Fail);
		sleep(4);
	}

	public void WaitingForScreenPauseMessageAferClickingOnResumeButton()
	{
		boolean flag;
		WebDriverWait wait1=new WebDriverWait(Initialization.driver,45);
		try
		{
			flag=wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[contains(text(),'capture paused')]"))).isDisplayed();  
		}
		catch (Exception e) 
		{
			flag=false;
		}
		String Pass="PASS>>>Screen Pause Message Is Not Displayed After Clicking On Resume Button And Waiting For 30 Seconds";
		String Fail="FAIL>>>Screen Pasue Message Is Displayed Even After Clicking On Resume Button";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	public void CheckingZipAllDownloadCheckBox()
	{
		if(ZipAllDownloadCheckBox.isSelected())
		{
			Reporter.log("Zip All Download Check Box Is Already Selected",true);
		}
		else
		{
			ZipAllDownloadCheckBox.click();
			Reporter.log("Zip All Download Check Box Is Checked",true);
		}
	}

	public void UncheckingZipAllDowlaodCheckBox()
	{
		if(ZipAllDownloadCheckBox.isSelected())
		{
			ZipAllDownloadCheckBox.click();
			Reporter.log("Zip All Download Check Box Is UnChecked",true);

		}
		else
		{
			Reporter.log("Zip All Download Check Box Is Already UnChecked",true);
		}
	}

	public void UncheckingDontPauseScreenCheckBox()
	{
		boolean Val = Initialization.driver.findElement(By.cssSelector("input#DoNotPauseRemoteScreen")).isSelected();
		if(Val)
		{
			Initialization.driver.findElement(By.cssSelector("input#DoNotPauseRemoteScreen")).click();
			Reporter.log("Don't Pause Screen CheckBox Is UnChecked",true);
		}
		else
		{
			Reporter.log("Don't Pause Screen CheckBox Is Already UnChecked",true);
		}
	}
	public void ClickingOnAccSettingsApply() throws InterruptedException
	{
		MiscellaneousApplyButton.click();
		waitForXpathPresent("//span[text()='Settings updated successfully.']");
		sleep(3);
	}

	public void ClickOnDownloadButton() throws InterruptedException
	{
		String downloadFilepath = "D:\\tesssssssssssst";
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		Download.click();
		sleep(4);
	}
	public void SelectingSingleFile(String FolderName1) throws AWTException, InterruptedException
	{
		waitForXpathPresent("//label[contains(text(),'"+FolderName1+"')]");
		sleep(2);
		Robot r=new Robot();

		Initialization.driver.findElement(By.xpath("//label[contains(text(),'"+FolderName1+"')]")).click();
	}

	public void SelectingMultipleFilesInRemoteSupport(String FolderName1,String FolderName2)throws Throwable
	{
		waitForXpathPresent("//label[contains(text(),'"+FolderName1+"')]");
		sleep(2);
		Robot r=new Robot();

		Initialization.driver.findElement(By.xpath("//label[contains(text(),'"+FolderName1+"')]")).click();
		r.keyPress(KeyEvent.VK_CONTROL);
		sleep(2);
		Initialization.driver.findElement(By.xpath("//label[contains(text(),'"+FolderName2+"')]")).click();
		r.keyRelease(KeyEvent.VK_CONTROL);
		Download.click();

	}

	public void VerifyWindowsDeviceOnlineStatus()
	{
		String ActualValue=DeviceOnlineOfflineStatus.getText();
		String ExpectedValue="Online";
		String PassStatement="PASS >> "+ExpectedValue+" status is displayed successfully on Remote screen when clicked on Remote Support of Dynamic Jobs";
		String FailStatement="FAIL >> "+ExpectedValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
		if(ActualValue.equalsIgnoreCase("Offline"))
		{
			String FailMessage ="FAIL >> "+ExpectedValue+" status is not displayed on Device screen even when clicked on Remote Support of Dynamic Jobs";
			ALib.AssertFailMethod(FailMessage);
		}else {

			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		}
	}

	public void VeirfyFileDownloadingInWindowsRemoteSuppport(String FileName,String DownloadPath) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{

		String downloadPath = DownloadPath;
		String fileName = FileName;
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		Download.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> user.xlsx Downloaded successfully";
		String FailStatement="Fail >> user.xlsx is not Downloaded";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void LaunchingNotePadApplication() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("C:\\Windows\\System32\\notepad.exe");
		sleep(2);
	}

	public void VerifyCurrentRunningTaskInsideRemoteSupportTaskManager(String Task)
	{
		boolean flag;
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//td[text()='"+Task+"']")).isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS >> Current Running Tasks Displayed In The Task Manager Tab";
		String Fail="FAIL >> Current Running Tasks Are Not Displayed In The Task Manager Tab";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}

	public void VerifyKillProcessTaskInsideRemoteSupportTaskManager(String Task)
	{
		boolean flag;
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//td[text()='"+Task+"']")).isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS >> Kill Process Task Is Not Displayed In The Task Manager Tab";
		String Fail="FAIL >> Kill Process Task Is Displayed In The Task Manager Tab";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}





























	//For Live automation 


	public void DownLoadFile(String FileName) throws AWTException, InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//label[text()='" + FileName + "']")).click();
		Initialization.driver.findElement(By.xpath("//li[@title='Download']")).click();
		waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
		sleep(5);

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

	}

	public void RemoteScreenIdle() throws AWTException, InterruptedException {

		for (;;) {
			for (;;) {
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
					String reconnecting = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
					DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					final String start = dateFormat1.format(new Date());
					Reporter.log(start,true);
					Reporter.log("Reconnecting issue : " + reconnecting,true);
					try {
						while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
						}
					} catch (Exception e1) {
						String end = dateFormat1.format(new Date());
						Reporter.log(end,true);
						Reporter.log("Reconnecting stopped ",true);
					}
				} catch (Exception e) {
					DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date1 = new Date();
					Reporter.log("No Reconnecting Issue till " + dateFormat1.format(date1),true);
				}

			}

		}
	}


	public void CreateFolder() throws InterruptedException{

			 for(int i=0;i<2;i++)
		 {
			 CreateFolder.click();//Click on New Folder
			 sleep(4);
			 Reporter.log("New Folder created successfully",true);
			 waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
		 }


	} 




	/* try{
			 for(int i=0;i<2;i++)
			    {
				 boolean a = CreateFolder.isDisplayed();
				 String pass = "Create Folder option is displayed";
				 String fail = "FAIL >> Create folder option is not displayed";
				 if(a==false)
				 {
					 String FailMessage ="FAIL >> CreateFolder Option is not displayed ";
					 ALib.AssertFailMethod(FailMessage);
				 }else {

				 ALib.AssertTrueMethod(a, pass, fail);
				 }

			   CreateFolder.click();//Click on New Folder
			   sleep(2);
			   Reporter.log("New Folder created successfully",true);
			   waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
			    }

         }catch(Exception e)
		 { 
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {

			}
		}
	System.out.println();
}*/




	public void OpenFolder(){

		try{
			for(int i=0;i<7;i++)
			{
				WebElement wb = Initialization.driver.findElement(
						By.xpath("//div[label[text()='" + ELib.getDatafromExcel("Sheet1",4, 4) + "']]"));
				Actions action = new Actions(Initialization.driver);
				action.moveToElement(wb).doubleClick().build().perform();
				sleep(3);
				System.out.println("Folder opened successfully");
				sleep(1);
				Back.click();

			}

		}catch(Exception e)
		{
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {

			}
		}
	}

	public void Upload() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		int count1 = Integer.parseInt(ELib.getDatafromExcel("Sheet1", 9, 4));
		try{
			for (int i = 0; i < count1; i++) 
			{
				sleep(1);
				System.out.println("upload1");
				Initialization.driver.findElement(By.xpath("//li[@title='Upload files']")).click();
				sleep(5);
				System.out.println("upload2");
				if (i > 0) {
					List<WebElement> wb = Initialization.driver.findElements(By.id("fileSelector"));
					int j = wb.size() - 1;
					wb.get(j).click();
					sleep(3);
				} else {
					Initialization.driver.findElement(By.id("fileSelector")).click();
				}

				Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet1", 10, 4));
				sleep(5);
				if (i > 0) {
					List<WebElement> wb = Initialization.driver.findElements(
							By.xpath("//body[@id='remote_support']/div[contains(@class,'ui-dialog')]/div/div/button[2]"));
					int j = wb.size() - 1;
					wb.get(j).click();
					sleep(3);
				} else {
					Initialization.driver.findElement(
							By.xpath("//body[@id='remote_support']/div[contains(@class,'ui-dialog')]/div/div/button[2]")).click();// Ok button present on upload browser prompt
					sleep(4);
				}
				sleep(100);
			}	
		}catch(Exception e)
		{
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {

			}
		}

	}

	public void UploadingFile(String Path) throws IOException, InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//li[@title='Upload files']")).click();
		System.out.println("Clicked on Upload files button");
		sleep(3);
		Initialization.driver.findElement(By.name("upload[]")).click();
		System.out.println("Clicked on browse file button");
		sleep(4);
		Runtime.getRuntime().exec(Path);
		waitForidPresent("fileSelector");
		System.out.println("File is browsed and selected");
		sleep(2);
		FileUploadOKButton.click();
		System.out.println("Clicked on OK of the upload file window");
		sleep(4);
		WebDriverWait wait2 = new WebDriverWait(Initialization.driver, 1200); 
		try{    
			wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[div[contains(text(),'testing.pdf')]]/div/span[text()='Uploaded!']")));
			System.out.println("PASS>>> File uploaded successfully");  
		}
		catch(Throwable e){
			System.out.println("FAIL>> File upload is failed");
		}
	}

	public void VerifyUploadingFile(String FileName) throws InterruptedException
	{

		boolean mark;
		waitForXpathPresent("//div[@id='uploadprogress']");
		String FileUploaded = Initialization.driver.findElement(By.xpath("//div[@class='file-name']")).getText();
		System.out.println(FileUploaded);
		if(FileUploaded.equals(FileName))
		{
			mark=true;
		}
		else
		{
			mark=false;
		}
		String PassStatement="Uploaded File Is Same As The Specified File";
		String FailStatement="Uploaded File Is Different From Specified File";
		ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
		sleep(10);
	}





	public void Download() throws AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		try{
			System.out.println("Hello");
			Initialization.driver.findElement(By.xpath("//div[label[text()='Test.txt']]")).click();
			sleep(5);
			Initialization.driver.findElement(By.xpath("//li[@title='Download']")).click();
			sleep(3);
			System.out.println("File downloaded successfully");

		}catch(Exception e)
		{
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {

			}

		}


	}

	public void VerifyOfRemoteSupportLaunchedMaCOS() throws InterruptedException
	{
		while(LoadingBarOfRemoteSupport.isDisplayed()){}
		waitForXpathPresent("//div[@id='playPauseButton']");
		sleep(8);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Reporter.log("Remote support launched "+dateFormat.format(date),true);
	}
	public void DeviceNameVerificationMacOS(String deviceName )
	{
		boolean devicenamevalue = Initialization.driver.findElement(By.xpath("//div[text()='Device: "+deviceName+"']")).isDisplayed();

		String PassStatement1 = "PASS >> device name is displayed as expected ";
		String FailStatement1 = "FAIL >> device name is not displayed as expected";
		ALib.AssertTrueMethod(devicenamevalue, PassStatement1, FailStatement1);

	}

	public void DeviceNameVerification(String deviceName )
	{
		boolean devicenamevalue = Initialization.driver.findElement(By.xpath("//div[text()='Device: "+deviceName+"']")).isDisplayed();

		String PassStatement1 = "PASS >> device name is displayed as expected ";
		String FailStatement1 = "FAIL >> device name is not displayed as expected";
		ALib.AssertTrueMethod(devicenamevalue, PassStatement1, FailStatement1);

	}

	public void resumeButtonDisplayed()
	{
		boolean value=true;
		try{
			Initialization.driver.findElement(By.xpath(".//*[@id='dimiss_pause']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		Assert.assertTrue(value,"FAIL: 'Resume button not displayed'");
		Reporter.log("PASS>> 'Resume button is displayed",true );	
	}

	public static void cleanDir() throws IOException
	{
		File f=new File("D:\\tesssssssssssst");
		FileUtils.cleanDirectory(f);
		
	}
	
	public void ReadingTextFromDevice(String Text) throws InterruptedException
	{
		Initialization.driverAppium.setClipboardText(Text);
		sleep(3);
		
	}
	
	public void VerifySendToDeviceClipBoard(String ClipBoardText)
	{
		String DeviceText = Initialization.driverAppium.getClipboardText();	
		System.out.println(DeviceText);
		if(ClipBoardText.equals(DeviceText))
		{
			Reporter.log("PASS>>> Send To Device ClipBoard Is Working",true);
		}
		else
			ALib.AssertFailMethod("FAIL>>> Send To Device ClipBoard Is Not Working");
		
	}
	
	public void SendigTextToDeviceClipBoard(String Text) throws InterruptedException
	{
		ClipBoardTextArea.clear();
		ClipBoardTextArea.sendKeys(Text);
		sleep(2);
		WriteClipboardBtn.click();
		sleep(2);
	}
	
	
	
	
	








}



