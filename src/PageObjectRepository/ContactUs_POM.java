package PageObjectRepository;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class ContactUs_POM extends WebDriverCommonLib {
	
	AssertLib ALib=new AssertLib();
	
	@FindBy(xpath = "//*[@id=\"userProfile\"]/div[3]/ul/li[9]/a")
	private WebElement ContactUsLink;
	
    @FindBy(xpath="//span[text()='Contact us']")
    private WebElement ContactUsNewTabPage;
	
	
	public void ClickOnContactus() throws InterruptedException{
	
	 String originalHandle = Initialization.driver.getWindowHandle();
	 sleep(10);
     ContactUsLink.click();
     sleep(5);
     Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
     ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
     Initialization.driver.switchTo().window(tabs.get(1));
     
     boolean isdisplayed=true;
	 
	  try{
		  ContactUsNewTabPage.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	   Assert.assertTrue(isdisplayed,"FAIL: 'Contact Us page did not launch or text is missing'");
	   Reporter.log("PASS>> 'Contact Us' page launched succesfully",true );		
     
	   for(String handle : Initialization.driver.getWindowHandles()) {
	        if (!handle.equals(originalHandle)) {
	        	Initialization.driver.switchTo().window(handle);
	        	Initialization.driver.close();
	        }
	    }

	  Initialization.driver.switchTo().window(originalHandle);
      
	  sleep(3);
		
		
		
}
	
}