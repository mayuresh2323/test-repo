package PageObjectRepository;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class BuySubscription_POM extends WebDriverCommonLib {

	@FindBy(xpath = "//span[text()='Buy Subscription']")
	private WebElement buySubscription;
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	WebDriverWait wait;
	
    public void VerifyBuySubscriptionPageLaunch() throws InterruptedException{
    	String originalHandle = Initialization.driver.getWindowHandle();
    	buySubscription.click();
    	ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
 	    Initialization.driver.switchTo().window(tabs.get(1));
 	     
    	Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
	    String ActualValue = Initialization.driver.getTitle();
	    Reporter.log("Title of the launched Buy Subscription page: "+ActualValue);
	    String ExpectedValue = "Purchase Monthly Subscription";
	    String PassStatement1 = "PASS >> "+ExpectedValue+" window  launched successfully and PageTitle is correct";
		String FailStatement1 = "FAIL >> "+ExpectedValue+ " window is not launched or Title is wrong" ;
		ALib.AssertEqualsMethod(ActualValue, ActualValue, PassStatement1, FailStatement1);
	    
	   
	     for(String handle : Initialization.driver.getWindowHandles()) {
		        if (!handle.equals(originalHandle)) {
		        	Initialization.driver.switchTo().window(handle);
		        	Initialization.driver.close();
		        }
		    }

		  Initialization.driver.switchTo().window(originalHandle);
	      sleep(3);
	      
		
	}


	

}
