package PageObjectRepository;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class IOS_DeviceInfo_POM extends WebDriverCommonLib
{
	 AssertLib ALib=new AssertLib();
	@FindBy(xpath="//*[@id='dis-card']/div[3]/p")
    private WebElement DeviceInfoDeviceModel;
	
	@FindBy(xpath="//*[@id='dis-card']/div[5]/p")
    private WebElement DeviceInfoDeviceOS;
	
	@FindBy(xpath="//span[@id='deviceOwnerName']")
    private WebElement DeviveInfoDeviceName;
	
	@FindBy(xpath="//*[@id='dis-card']/div[19]/p")
	private WebElement SupervisedStatus;
	
	@FindBy(xpath="//*[@id='networkinfo-card']/div[3]")
    private WebElement DeviceInfoMAC;
	
	@FindBy(xpath="//td[@class='DeviceIPAddress']")
	private WebElement DeviceIPInDeviceGrid;
	
	@FindBy(xpath="(//div[@id='networkinfo-card']//p)[1]")
	private WebElement DeviceInfoIP;
	
	@FindBy(xpath="(//div[@id='networkinfo-card']//p)[2]")
	private WebElement DeviceInfoSerialNumber;
	
	@FindBy(xpath="//*[@id='dis-card']/div[7]/a")
	private WebElement EditDeviceName;
	
	@FindBy(xpath="//span[@class='editable-clear-x']")
	private WebElement ClearDevicenameButton;
	
	@FindBy(xpath="//input[@id='device_name_input']")
	private WebElement DeviceNameInput;
	
	@FindBy(xpath="//button[@type='submit']")
	private WebElement SaveButton;
	
	@FindBy(xpath="//td[@class='DeviceName']")
	private WebElement DeviceGridDeviceName;
	
	 public void VerifyDeviceModelInDeviceInfoSection()
	  {
		  String ActualModel=DeviceInfoDeviceModel.getText();
		  String ExpectedModel=Config.IOS_DeviceModel;
		  String PassStatement ="PASS >> Actual Device Model and Expected Device Model are matching";
		  String FailStatement ="FAIL >> Actual Device Model and Expected Device Model are not matching";
		  ALib.AssertEqualsMethod(ActualModel, ExpectedModel, PassStatement, FailStatement);
	  }
	 public void VerifyDeviceOSInDeviceInfoSection()
	  {
		  String ActualOS=DeviceInfoDeviceOS.getText();
		  String ExpectedOS=Config.IOS_DeviceOS;
		  String PassStatement ="PASS >> Actual Device OS and Expected Device OS are matching";
		  String FailStatement ="FAIL >> Actual Device OS and Expected Device OS are not matching";
		  ALib.AssertEqualsMethod(ActualOS, ExpectedOS, PassStatement, FailStatement);	  
	  }
		
		public void VerifyDeviceNameChanginginDeviceInfoSection() throws InterruptedException
		{
			EditDeviceName.click();
			waitForXpathPresent("//button[@type='submit']");
			ClearDevicenameButton.click();
			DeviceNameInput.sendKeys(Config.MacOS_ChangedDeviceName);
			SaveButton.click();
			waitForXpathPresent("//span[text()='Device name changed successfully.']");
			String DeviceNameInGrid = DeviceGridDeviceName.getText();
			if(DeviceNameInGrid.equals(Config.MacOS_ChangedDeviceName))
			{
				Reporter.log(">>>>Device Name is Same As Changed In Device Info Section",true);
			}
			else
			{
				Reporter.log(">>>>Device Name is Different From Changed In Device Info Section",true);
			}
			EditDeviceName.click();
			waitForXpathPresent("//button[@type='submit']");
			sleep(3);
			ClearDevicenameButton.click();
			sleep(5);
			DeviceNameInput.sendKeys(Config.IOS_DeviceName);
			SaveButton.click();			
		}
	 
	 public void VerifyDeviceSupervisedStatusInDeivceInfoSection()
	 {
		 String Supervisedstatus=SupervisedStatus.getText();
		 if(Supervisedstatus.equals("YES"))
		 {
			 Reporter.log("This Device is Supervised Device",true);
		 }
		 else if (Supervisedstatus.equals("NO"))
		 {
			Reporter.log("This Device is not a SuperVised Device",true);
		 }
		 else
		 {
			 Reporter.log(">>>>>>>>Enrolled Device is not an IOS Device",true);
		 }

		}
	 public void VerifyDeviceMACAddressInDeviceInfoSection()
	  {
		  String ActualDeivceMAC=DeviceInfoMAC.getText();
		  String ExpectedDeviceMAC=Config.IOS_DeviceMAC;
		  String PassStatement ="PASS >> Actual Device MAC and Expected Device MAC are matching";
		  String FailStatement ="FAIL >> Actual Device MAc and Expected Device MAc are not matching";
		  ALib.AssertEqualsMethod(ActualDeivceMAC, ExpectedDeviceMAC, PassStatement, FailStatement);
	  }
	 public void VerifyDeviceIPAddressInDeivceInfoSection()
	 {
		 boolean flag;
		 String DeviceGridIP=DeviceIPInDeviceGrid.getText();
		 String DeviceinfoIP=DeviceInfoIP.getText();
		 if(DeviceGridIP.equals(DeviceinfoIP))
		 {
			 flag=true;
		 }
		 else
		 {
			 flag=false;
		 }
		 String PassStatement ="PASS >> DeviceGridIP and DeviceinfoIP are matching";
		 String FailStatement ="FAIL >> DeviceGridIP and DeviceinfoIP are not matching";
		 ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	 }
	 public void VerifyDeviceSerialNumberInDeivceInfoSection()
	 {
		 String ActualDeviceSerialNumber=DeviceInfoSerialNumber.getText();
		 String ExpectedDeviceSerialNumber=Config.IOS_DeviceSerialNumber;
		 String PassStatement ="PASS >> Actual Device Serial Number and Expected Device Serial Number are matching";
		 String FailStatement ="FAIL >> Actual Device Serial Number and Expected Device Serial Number are not matching";
		 ALib.AssertEqualsMethod(ActualDeviceSerialNumber, ExpectedDeviceSerialNumber, PassStatement, FailStatement);
		 
	 }
	 
	 
	 public void CheckStatusOfappliedJob(int DeploymentTime,String Jobname) throws InterruptedException
		{
			
			WebDriverWait wait3 = new WebDriverWait(Initialization.driver,DeploymentTime); 
			try{
				boolean JobSuccess = wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
				Reporter.log(" Main job staus is deployed checking for sub job status",true);
				if(JobSuccess)
				{
					Reporter.log("Clicking on job Queue",true);
					Initialization.driver.findElement(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']")).click();
					sleep(2);
					waitForidPresent("jobqueuetitle");
					sleep(2);
//					Initialization.driver.findElement(By.xpath("//*[@id=\"jobQueueDataGrid\"]/tbody/tr/td/p[text()='"+Jobname+"']")).click();
					
					Initialization.driver.findElement(By.xpath("//*[@id=\"jobQueueDataGrid\"]/tbody/tr/td/p/i[contains(@data-name,'"+Jobname+"') and @title='View sub job status']")).click();
					sleep(3);
					ArrayList<WebElement> List =(ArrayList<WebElement>) Initialization.driver.findElements(By.xpath("//*[@id=\"subjobQueueDataGrid\"]/tbody/tr/td[5]/p"));
					for( WebElement e:List )
					{
						System.out.println(e.getText());
						if(e.getText().contains("Error"))
						{
							String FailMessage="Main job status is Deployed sub job Status is Error";
							ALib.AssertFailMethod(FailMessage);
						}
						else if(e.getText().contains("In progress"))
						{
							String FailMessage="Main job status is Deployed sub job Status is Inprogressr";
							ALib.AssertFailMethod(FailMessage);
						}
						else
						{
							Reporter.log("Sub Job staus is Deployed");
						}
						
						Initialization.driver.findElement(By.xpath("//*[@id=\"subjobQueueModel\"]/div/div/div[1]/button")).click();
						sleep(3);
						Initialization.driver.findElement(By.xpath("//*[@id=\"device_jobQ_modal\"]/div[1]/button")).click();
						sleep(2);
					}
				}
			}
			catch(Throwable e){
				String FailMessage ="Job Not deployed on the device in given time interval";
				ALib.AssertFailMethod(FailMessage);
				 
			}
	 
		
		}
	 }
	 


