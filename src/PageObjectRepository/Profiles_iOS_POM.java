package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;

public class Profiles_iOS_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib(); 

	@FindBy(xpath="//span[text()='iOS/iPadOS']")
	private WebElement ClickOniOSOption;

	@FindBy(xpath="//p[text()='iOS/iPadOS MDM Profile']")
	private WebElement iOSMDMProfileText;

	@FindBy(id="addPolicyBtn")
	private WebElement Addbutton;

	@FindBy(xpath="//h5[text()='Use this section to add apps to blocklist or allowlist.']")
    private WebElement BlackListWhitelistAppsSummary;

	@FindBy(xpath="//h5[text()='Use this section to block websites, add bookmarks or block contents.']")
	private WebElement WebContentFilterSummary;

	@FindBy(xpath="//h5[text()='Use this section to block websites, add bookmarks or block contents.']")
	private WebElement WebContentSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure wallpaper for Home Screen and Lock Screen.']")
	private WebElement BrandingSummary;

	@FindBy(xpath="//h5[text()='Use this section to specify passcode policies to be enforced on the device.']")
	private WebElement PasscodePolicySummary;

	@FindBy(xpath="//h5[text()='Use this section to enable single app mode profile.']")
	private WebElement SingleAppModeProfileSummary;

	@FindBy(xpath="//h5[text()='Use this section to restrict which apps, device functionality and media content are available to user.']")
	private WebElement RestrictionProfileSummary;

	@FindBy(xpath=".//*[@id='mam_installAppProfileHead']/div/div/h5[text()='Use this section to add application to install on your device.']")
	private WebElement ApplicationPolicySummary;

	@FindBy(xpath=".//*[@id='configurationProfileHead']/div/div/h5") // could not handle so giving like this
	private WebElement ConfigurationProfileSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure your device to connect to wireless network.']")
	private WebElement WifiConfgurationSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure settings for connecting to your POP or IMAP email accounts.']")
	private WebElement MailConfigurationSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure settings for a proxy server.']")
	private WebElement GlobalHTTPProxySummary;

	@FindBy(xpath="//h5[text()='Use this section to configure your device to connect to wireless network via VPN.']")
	private WebElement VPNSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure settings for connecting to your POP or IMAP email accounts.']")
	private WebElement CertificateSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure settings for a proxy server.']")
	private WebElement ExchangeActiveSyncSummary;

	@FindBy(xpath="//h5[text()='Use this section to configure your device to connect to wireless network via VPN.']")
	private WebElement FileSharingPolicySummary;

	@FindBy(id="BcklstAppsProfileConfig")
	private WebElement BlackListAppProfileConfigButton;

	@FindBy(id="blacklistwhitelistUrlsProfileConfig")
	private WebElement webContentFilterConfigureButton;

	@FindBy(id="brandingProfileConfig")
	private WebElement BrandingConfigureButton;

	@FindBy(id="WtlstAppsProfileConfig")
	private WebElement WhiteListAppProfileConfigButton;

	@FindBy(xpath="//div[text()='Please click Add(+) to add application.']")
	private WebElement TextMessageClickToAddApplication;

	@FindBy(id="addblacklistwhitelistAppsBtn")
	private WebElement ClickOnAddButton;

	@FindBy(xpath=".//*[@id='blacklistwhitelistAppNameListIOS_chosen']/a")
	private WebElement ClickOnChooseApp;

	@FindBy(xpath=".//*[@id='appId']")
	private WebElement appIDPackage;

	@FindBy(xpath=".//*[@id='blacklistwhitelistAppsPopup']/div/div/div[3]/button")
	private WebElement AddButton_AppList_BlackList_WhiteList;

	@FindBy(xpath=".//*[@id='applicationsConfigurationPopup']/div/div/div/button[text()='Save']")
	private WebElement AddButton_ApplicationList_ConfigurationProfile;

	@FindBy(xpath=".//*[@id='blacklistwhitelistTable']/tbody/tr[1]/td[2]")
	private WebElement DeleteFirstAppinRow;

	@FindBy(xpath="//p['Are you sure you want to delete?']")
	private WebElement WarningMessageOnDeleteApp;

	@FindBy(id="deleteblacklistwhitelistAppsBtn")
	private WebElement DeleteButton;

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement no_button;

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement yes_button;

	@FindBy(xpath="//span[text()='Please select one or more applications.']")
	private WebElement WarningMessage_ClickOnAdd_WithoutChoosingApp;

	@FindBy(id="saveiOSProfile")
	private WebElement saveButton;


@FindBy(xpath="//span[text()='Please enter a name for the profile.']")
        private WebElement warningMessageOnSavingWithoutProfileName;

	@FindBy(id="policyNameIOS")
	private WebElement profileName;

	@FindBy(xpath="//span[text()='Please enter all required fields to save the profile']")
	private WebElement warningMessageOnSavingProfileWithoutAllFields;

	@FindBy(xpath="//span[text()='Web Content Filter']")
	private WebElement ClickOnWebContentFilter;

	@FindBy(xpath=".//*[@id='urlBlacklist']/div[1]/button[1]")
	private WebElement AddButton_BlacklistURL;


	@FindBy(xpath=".//*[@id='urlWhitelist']/div[1]/button[1]")
	private WebElement AddButton_WhitelistURL;

	@FindBy(id="url_addBtn")
	private WebElement urlAddbutton;

	@FindBy(xpath="//span[text()='Please enter a valid URL.']")
	private WebElement warningMessage_WithoutBlacklistURL;

	@FindBy(xpath="//span[text()='Please enter a title for the bookmark.']")
	private WebElement warningMessage_WithoutWhiteListbookmark;

	@FindBy(xpath="//span[text()='Enter a path for the bookmark.']")
	private WebElement warningMessage_WithoutBookmarkPath;

	@FindBy(id="url_BW_input")
	private WebElement blacklistURL;

	@FindBy(xpath="//td[text()='http://www.42gears.com']")
	private WebElement isBlacklistURLDisplayed;

	@FindBy(xpath="//td[text()='http://www.qspiders.com']")
	private WebElement isWhitelistedBookmarkDisplayed;

	@FindBy(xpath="//a[text()='Allowlisted Bookmarks']")
    private WebElement WhitelistBookmarks;

	@FindBy(xpath="//a[text()='Auto Filter']")
	private WebElement AutoFilter;

	@FindBy(id="url_BW_tit")
	private WebElement Title_WhitelistBookmark;

	@FindBy(id="url_BW_input")
	private WebElement autofilterURL;

	@FindBy(id="url_BW_input")
	private WebElement URL_WhitelistBookmark;

	@FindBy(id="url_BW_path")
	private WebElement BookmarkPath_WhitelistBookmark;

	@FindBy(id="ios_web_content_filter_checkbox")
	private WebElement AutoFilter_CheckBox;

	@FindBy(xpath=".//*[@id='urlFilter']/div[1]/div[2]/button[1]")
	private WebElement Addbutton_AutoFilter;

	@FindBy(xpath="//td[text()='http://www.gmail.com']")
	private WebElement AutoFilterURLDisplayed;

	@FindBy(xpath="//span[text()='Branding']")
	private WebElement BandingOption;

	@FindBy(id="ios_image_picker_input_ele_1")
	private WebElement iOSImagePicker_HomeScreenWallpaper;

	@FindBy(xpath="//span[text()='Passcode Policy']")
	private WebElement passcodePolicyOption;

	@FindBy(id="passcodeProfileConfig")
	private WebElement ConfigureButton_PasscodePolicy;

	@FindBy(xpath="//div[text()='Force User to Set Passcode ']")
	private WebElement FirstParameterPasscodePolicy;

	@FindBy(id="ios_passcode_forcePIN")
	private WebElement CheckBox_ForceUserToSetPasscode;

	@FindBy(id="ios_passcode_alphanumeric_value")
	private WebElement CheckBox_RequireAlphanumericValue;

	@FindBy(id="ios_passcode_length_chosen")
	private WebElement ChooseMinimumPasscodeLength;

	@FindBy(xpath="//span[text()='4']")
	private WebElement isFourDisplayed;

	@FindBy(id="ios_passcode_complex_characters_chosen")
	private WebElement ChoosePasscodeComplextCharacter;

	@FindBy(id="ios_passcode_age")
	private WebElement passcodeAge;

	@FindBy(id="ios_passcode_autolock_chosen")
	private WebElement MaximuAutoLock;

	@FindBy(id="//span[text()='2 minutes']")
	private WebElement TwoMinutes;

	@FindBy(id="ios_passcode_history")
	private WebElement PasscodeHistory;

	@FindBy(id="ios_passcode_grace_period_chosen")
	private WebElement MaxGracePeriodForDeviceLock;

	@FindBy(xpath="//span[text()='Immediately']")
	private WebElement Immediately;

	@FindBy(xpath="//span[text()='Single App Mode Profile']")
	private WebElement SingleAppModeProfile;

	@FindBy(xpath="//h5[text()='Use this section to enable single app mode profile.']")
	private WebElement Summary_SAMModeProfile;

	@FindBy(id="singleAppModeConfig")
	private WebElement ConfigureButton_SAMProfile;

	@FindBy(id="installedAppNameListIOS_chosen")
	private WebElement LockdownApplicationListiOS;

	@FindBy(id="launchPeriodically")
	private WebElement Checkbox_launchPeriodically;

	@FindBy(id="launchPeriod")
	private WebElement launchPeriod;

	@FindBy(id="singleAppMode")
	private WebElement singleappMode;

	@FindBy(xpath="//span[text()='Lockdown works only for the apps installed on the device.']")
	private WebElement summary_LockDownApplication;

	@FindBy(xpath="//span[text()='Restriction Profile']")
	private WebElement restrictionProfile;

	@FindBy(id="restrictionProfileConfig")
	private WebElement ConfigureButton_RestrictionProfile;

	@FindBy(id="allowCamera")
	private WebElement CheckBox_AllowUseOfCamera;

	@FindBy(id="allowScreenShot")
	private WebElement CheckBox_AllowScreenshotsAndScreenRecording;

	@FindBy(id="allowAssistant")
	private WebElement CheckBox_AllowSiri;

	@FindBy(xpath="//label[text()='Force WiFi Whitelisting (supervised only, iOS 10.3 onwards)']")
	private WebElement LastParameter_ForceWifiWhiteListing_Functionality;

	@FindBy(xpath="//label[text()='Allow use of camera']")
	private WebElement FirstParameter_AllowUseOfCamera_Functionality;

	@FindBy(xpath="//a[text()='Apps']")
	private WebElement AppsTab;

	@FindBy(id="allowNews")
	private WebElement CheckBox_AllowUseOfNews;

	@FindBy(id="allowSafari")
	private WebElement CheckBox_AllowUseOfSafari;

	@FindBy(id="apps")
	private WebElement ScrollDown_AppsBottom;

	@FindBy(xpath="//span[text()='Always']")
	private WebElement AllowCookiesOption;

	@FindBy(xpath="//a[text()='Media Content']")
	private WebElement MediaContentTab;

	@FindBy(xpath="//span[@title='Application Policy']") //other xpath did not work here
    private WebElement ApplicationPolicy;
	
	@FindBy(xpath="//span[@title='Exchange ActiveSync']")
    private WebElement ExchangeActiveSyncOption;

	@FindBy(id="mam_installAppProfileConfig")
	private WebElement ConfigureButton_ApplicationPolicy;

	@FindBy(id="configurationProfileConfig")
	private WebElement ConfigureButton_ConfigurationProfile;

	@FindBy(id="mam_addInstallApplicationPolicyBtn") 
	private WebElement AddButton_ApplicationPolicy;

	@FindBy(id="ios_eam_appList_chosen")
	private WebElement ChooseApp_ApplicationPolicy;

	@FindBy(id="ios_eam_installAutoApp")
	private WebElement Checkbox_InstallSilently;

	@FindBy(xpath=".//*[@id='ios_addEAMAppModal']/div/div/div/button[text()='Add']")
	private WebElement AddButton_AddApp;

	@FindBy(xpath=".//*[@id='ios_addEAMAppModal']/div/div/div/button[text()='Cancel']")
	private WebElement CancelButton_AddApp;

	@FindBy(xpath=".//*[@id='mam_installAppProfileTail']/ul/li/div/div/div/div/div/table/thead/tr/th/div[text()='Name']")
	private WebElement NameColumnName;;

	@FindBy(xpath="//td[text()='SureFox']")
	private WebElement isFirstAppPresent; // taking fb as choosing application

	@FindBy(xpath="//td[text()='123 Movies']")
	private WebElement is2ndPresentPresent; // taking fb as choosing application

	@FindBy(xpath="//span[text()='This App is already added']")
	private WebElement warningMessageAddingtheSameApplication_AppPolicy;

	@FindBy(id="mam_configInstallApplicationPolicyBtn")
	private WebElement ConfigButton_AppPolicy;

	@FindBy(id="keyAppConfig")
	private WebElement key_AppConfiguration_AppPolicy;
	
	@FindBy(xpath="//*[@id='applicationsConfigurationPopup']/div/div/div[2]/div[2]/div/i")
	private WebElement Addconfig;
	
	@FindBy(id="valueAppConfig")
	private WebElement Value_AppConfiguration_AppPolicy;
	
	@FindBy(xpath="//*[@id='appConfigAddFileds']/div/div/div[3]/button")
	private WebElement AddKeyValue;

	
	//@FindBy(id="saveiOSProfile")
	@FindBy(xpath=".//*[@id='applicationsConfigurationPopup']/div/div/div/button[text()='Save']")
	private WebElement SaveButton_ApplicationConfiguration;

	@FindBy(id="mam_editInstallApplicationPolicyBtn")
	private WebElement EditButton;

	@FindBy(xpath="//h4[text()='Edit Enterprise App Store App']")
	private WebElement EditEnterpriseAppStoreAppWindow;

	@FindBy(xpath=".//*[@id='mam_installApplicationPolicyTable']/tbody/tr/td[text()='123 Movies']")
	private WebElement ClickOnMyntraInTheList;

	@FindBy(id="mam_deleteInstallApplicationPolicyBtn")
	private WebElement ClickOnDeleteButton;

	@FindBy(xpath="//p[text()='Selected Application will be deleted. Do you wish to continue?']")
	private WebElement WarningMessageDeletingApp_ApplicationPolicy;

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']")
	private WebElement YesButton_DeleteWarningDialog;

	@FindBy(xpath="//span[text()='Configuration Profile']")
	private WebElement ConfigurationProfile;

	@FindBy(id="addApplicationsConfigurationBtn")
	private WebElement ClickOnAddButton_ConfigurationProfile;

	@FindBy(xpath="//span[text()='All fields are necessary']")
	private WebElement WarningWhenClickingAddButtonWithoutApplication_ConfigurationProfile;

	@FindBy(id="configAppNameListIOS_chosen")
	private WebElement ClickOnChooseApp_ConfigurationProfile;

	@FindBy(xpath=".//*[@id='configAppNameListIOS_chosen']/div/div/input")
	private WebElement TextField_EnterAppName;

	@FindBy(id="keyAppStoreApp")
	private WebElement Key_ApplicationConfiguration;

	@FindBy(id="valueAppStoreApp")
	private WebElement Value_ApplicationConfiguration;

	@FindBy(xpath="//td[text()='Calculator']")
	private WebElement isCalculatorAdded;

	@FindBy(xpath="//td[text()='Calendar']")
	private WebElement isCalendarAdded;

	@FindBy(id="deleteApplicationsConfigurationBtn")
	private WebElement DeleteButton_ConfigurationProfile;

	@FindBy(id="wifiProfileConfig")
	private WebElement ConfigureButton_WifiConfiguration;

	@FindBy(id="globalHTTPProfileConfig")
	private WebElement ConfigureButton_GlobalHTTPProxy;

	@FindBy(id="mailProfileConfig")
	private WebElement ConfiguraButton_MailConfiguration;

	@FindBy(id="ios_wificonfig_table_add_btn")
	private WebElement AddButton_WifiConfiguration;

	@FindBy(xpath="//h4[text()='Wi-Fi Configuration']")
	private WebElement wifiConfigurationWindow;

	@FindBy(id="wifiSSID_config_ios")
	private WebElement SSIDTextBox;

	@FindBy(xpath=".//*[@id='iosWifiConfig_modal']/div/div/div/button[text()='Save']")
	private WebElement SaveButton_WifiConfigurationWindow;

	@FindBy(xpath="//span[text()='Please fill this field']")
	private WebElement warningWithoutSSIDorServerURL;

	@FindBy(id="securityWiFiSelect_chosen")
	private WebElement SecurityTypeDropdown;

	@FindBy(id="autoJoin")
	private WebElement autoJoin_CheckBox;

	@FindBy(id="proxyWiFiSelect_chosen")
	private WebElement ProxySetupDropdown;

	@FindBy(id="securityproxyPassword")
	private WebElement password_WiFiConfiguration;

	@FindBy(xpath="//span[text()='Server URL and Port are required']")
	private WebElement warningWithoutServerAndPort;

	@FindBy(id="serverProxy")
	private WebElement ServerAndPortTextField;

	@FindBy(id="portProxy")
	private WebElement PortTextField;

	@FindBy(id="accountmail")
	private WebElement AccountDescription;

	@FindBy(id="ios_mail_emailaddress")
	private WebElement EmailAddress;

	@FindBy(id="ios_mail_preventmove")
	private WebElement Checkbox_AllowUserToMoveMessageFromThisAccount;

	@FindBy(id="ios_mail_disablesync")
	private WebElement Checkbox_AllowRecentAddressesToBeSynced;

	@FindBy(id="ios_mail_preventapp")
	private WebElement Checkbox_UseOnlyInMail;

	@FindBy(id="ios_mail_in_server")
	private WebElement URL_MailServerAndPort;

	@FindBy(id="ios_mail_in_port")
	private WebElement Port_MailConfiguration;

	@FindBy(id="ios_mail_in_username")
	private WebElement Username_MailConfiguration;

	@FindBy(id="ios_mail_in_authtype_chosen")
	private WebElement AuthenticationType;

	@FindBy(id="ios_mail_in_ssl")
	private WebElement SSL_Incoming;

	@FindBy(xpath="(//a[text()='Outgoing Mail'])[2]")
	private WebElement OutgoingMailOption;

	@FindBy(id="ios_mail_out_server")
	private WebElement MailServer_Outgoing;

	@FindBy(id="ios_mail_out_port")
	private WebElement port_Outgoing;

	@FindBy(id="ios_mail_out_username")
	private WebElement username_outgoing;

	@FindBy(id="ios_mail_out_ssl")
	private WebElement SSL_Outgoing;

	@FindBy(xpath="//span[text()='Global HTTP Proxy']")
	private WebElement GlobalHTTPProxy;

	@FindBy(id="ios_proxy_port")
	private WebElement port_GlobalHTTPProxy;

	@FindBy(id="ios_proxy_server")
	private WebElement serverURL_GlobalHTTPProxy;

	@FindBy(id="ios_proxy_bypass")
	private WebElement checkbox_AllowBypassingProxyToAccessCaptiveNetworks;

	@FindBy(xpath="//span[text()='Server URL is required']")
	private WebElement warningWithoutProxyServerURL_GlobalHTTPPort;

	@FindBy(id="VPNProfileConfig")
	private WebElement ConfigureButton_VPN;

	@FindBy(id="exchangeProfileConfig")
	private WebElement ConfigureButton_ExchangeActiveSync;

	@FindBy(xpath="//div[@id='iOSPolicyDetails']/div/div/div/div/div[@id='certificateProfileHead']/div/div/button'")
	private WebElement ConfigureButton_CertificateiOS;

	@FindBy(id="connectName")
	private WebElement connectionName_VPN;

	@FindBy(xpath="//span[text()='L2TP']")
	private WebElement chooseConnectiontype;

	@FindBy(id="serverVPN")
	private WebElement Server_VPN;

	@FindBy(id="ios_vpn_password")
	private WebElement password_VPN;

	@FindBy(xpath=".//*[@id='policy_item_box']/div[3]/div/ul/li[14]") //other xpath did not work
	private WebElement CertificateProfile;

	@FindBy(id="ios_upload_cert_error")
	private WebElement errorWithoutCertificateUpload;

	@FindBy(xpath="//span[text()='1 day']")
	private WebElement OneDay;

	@FindBy(xpath="//span[text()='3 days']")
	private WebElement ThreeDays;

	@FindBy(xpath="//span[text()='1 week']")
	private WebElement OneWeek;

	@FindBy(xpath="//span[text()='2 weeks']")
	private WebElement TwoWeeks;

	@FindBy(xpath="//span[text()='1 month']")
	private WebElement OneMonth;

	@FindBy(xpath=".//*[@id='exchangeProfileTail']/ul/li[8]/div[2]/div/div[2]/span/i") //other xpath did not work
	private WebElement chooseCertificate_PlusButon;

	@FindBy(xpath=".//*[@id='certificateAddPopup']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton_CertificateWindow;

	@FindBy(id="ios_upload_cert_error")
	private WebElement certficateError;

	@FindBy(xpath=".//*[@id='certificateAddPopup']/div/div/div/div/span[text()='Please fill this field.']")
	private WebElement passwordError;

	@FindBy(id="passwordCertificate")
	private WebElement CertificatePassword;

	@FindBy(xpath=".//*[@id='ios_exchange_certificate_select_chosen']/a/span[text()='cert.p12']")//other xpath does not work
	private WebElement isUploadedCertDisplayed;

	@FindBy(id="ios_exchnage_account_name")
	private WebElement AccountName;

	@FindBy(id="ios_exchange_host")
	private WebElement ExchangeActiveSyncHost;

	@FindBy(id="ios_exchange_use_ssl")
	private WebElement checkbox_UseSSL;

	@FindBy(id="ios_exchange_user")
	private WebElement User_ExchangeActiveSync;

	@FindBy(id="ios_exchange_email")
	private WebElement EmailAddress_ExchangeActiveSync;

	@FindBy(id="ios_exchange_password")
	private WebElement Password_ExchangeActiveSync;

	@FindBy(id="ios_exchange_prevent_move")
	private WebElement checkbox_AllowMessagesToBeMoved;

	@FindBy(id="ios_exchange_address_sync")
	private WebElement checkbox_AllowRecentAddressesToBeSynced;

	@FindBy(id="ios_exchange_allow_inmail")
	private WebElement checkbox_UseOnlyInMail;

	@FindBy(xpath="//span[text()='Please add at least one payload to save the profile.']")
	private WebElement warningMessageOnSavingCertificateprofileWithoutPayload;


	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	public void clickOniOSOption() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ClickOniOSOption.click();
		sleep(2);

	}

	public void AddProfile() throws InterruptedException{
		Reporter.log("=======Verify Adding a Profile and check the availabitity of all the options in the window=======");
		Helper.highLightElement(Initialization.driver, Addbutton);
		Addbutton.click();
		waitForidPresent("BcklstAppsProfileConfig");
		boolean check = iOSMDMProfileText.isDisplayed();
		String PassStatement = "PASS >> Clicked on Add profile button and window displayed successfully";
		String FailStatement = "FAIL >> Did not Click on Add profile button and window NOT displayed successfully";
		ALib.AssertTrueMethod(check,PassStatement, FailStatement);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	//verify summary of all the policies
	public void VerifySummaryof_BlackListWhiteListApps() throws InterruptedException{


		String blacklistWhitelistSummary =BlackListWhitelistAppsSummary.getText();
		Reporter.log(blacklistWhitelistSummary);
		sleep(2);
		boolean value = BlackListWhitelistAppsSummary.isDisplayed();
		String PassStatement1 = "PASS >> BlackList Whitelist AppsS ummary is correct";
		String FailStatement1 = "FAIL >> BlackList Whitelist AppsS ummary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}


	/*
   		 if(i==1){
   			typesOfiOSPolicies.get(i).click();
       	     String webContentSummary =WebContentSummary.getText();
       	     Reporter.log(webContentSummary);
       	     boolean value = WebContentSummary.isDisplayed();
       	     String PassStatement1 = "PASS >> Web Content Summary is correct";
    		     String FailStatement1 = "FAIL >> Web Content Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }

   		 if(i==2){
   			 typesOfiOSPolicies.get(i).click();
       	     String Branding_Summary =BrandingSummary.getText();
       	     Reporter.log(Branding_Summary);
       	     boolean value = BrandingSummary.isDisplayed();
       	     String PassStatement1 = "PASS >> branding Summary is correct";
    		     String FailStatement1 = "FAIL >> branding Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }
   		 if(i==3){
   			 typesOfiOSPolicies.get(i).click();
       	     String passcodePolicySummary =PasscodePolicySummary.getText();
       	     Reporter.log(passcodePolicySummary);
       	     boolean value = PasscodePolicySummary.isDisplayed();
       	     String PassStatement1 = "PASS >> passcode policy Summary is correct";
    		     String FailStatement1 = "FAIL >> passcode policy Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }
   		 if(i==4){
   			 typesOfiOSPolicies.get(i).click();
       	     String singleAppModeProfileSummary =SingleAppModeProfileSummary.getText();
       	     Reporter.log(singleAppModeProfileSummary);
       	     boolean value = SingleAppModeProfileSummary.isDisplayed();
       	     String PassStatement1 = "PASS >> single app mode profile policy Summary is correct";
    		     String FailStatement1 = "FAIL >> single app mode profile policy Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }
   		 if(i==5){
   			 typesOfiOSPolicies.get(i).click();
       	     String restrictionProfileSummary =RestrictionProfileSummary.getText();
       	     Reporter.log(restrictionProfileSummary);
       	     boolean value = RestrictionProfileSummary.isDisplayed();
       	     String PassStatement1 = "PASS >> restriction profile Summary is correct";
    		     String FailStatement1 = "FAIL >> restriction profile Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);


       	 }

   		 if(i==6){
   			 typesOfiOSPolicies.get(i).click();
       	     String applicationPolicySummary =ApplicationPolicySummary.getText();
       	     System.out.println(applicationPolicySummary);
       	     boolean value = ApplicationPolicySummary.isDisplayed();
       	     String PassStatement1 = "PASS >> application policy Summary is correct";
    		     String FailStatement1 = "FAIL >> application policy Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }


   		 if(i==7){
   			 typesOfiOSPolicies.get(i).click();
       	     String configurationProfileSummary =ConfigurationProfileSummary.getText();
       	     Reporter.log(configurationProfileSummary);
       	     boolean value = ConfigurationProfileSummary.isDisplayed();
       	     String PassStatement1 = "PASS >> Configuration Profile Summary is correct";
    		     String FailStatement1 = "FAIL >> Configuration Profile Summary is NOT correct";
    		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
       	 }


   		if(i==8){
  			 typesOfiOSPolicies.get(i).click();
      	     String wifiConfigurationSummary =WifiConfgurationSummary.getText();
      	     Reporter.log(wifiConfigurationSummary);
      	     boolean value = WifiConfgurationSummary.isDisplayed();
      	     String PassStatement1 = "PASS >> wifi configuration Summary is correct";
   		     String FailStatement1 = "FAIL >> wifi configuration Summary is NOT correct";
   		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
      	 }
   		if(i==9){
 			 typesOfiOSPolicies.get(i).click();
     	     String mailConfigurationSummary =MailConfigurationSummary.getText();
     	     Reporter.log(mailConfigurationSummary);
     	     boolean value = MailConfigurationSummary.isDisplayed();
     	     String PassStatement1 = "PASS >> mail configuration Summary is correct";
  		     String FailStatement1 = "FAIL >> mail configuration Summary is NOT correct";
  		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
     	 }

   		if(i==10){
			 typesOfiOSPolicies.get(i).click();
    	     String mailConfigurationSummary =MailConfigurationSummary.getText();
    	     Reporter.log(mailConfigurationSummary);
    	     boolean value = MailConfigurationSummary.isDisplayed();
    	     String PassStatement1 = "PASS >> mail configuration Summary is correct";
 		     String FailStatement1 = "FAIL >> mail configuration Summary is NOT correct";
 		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
    	 }

   		if(i==10){
			 typesOfiOSPolicies.get(i).click();
   	     String globalHTTPProxySummary =GlobalHTTPProxySummary.getText();
   	     Reporter.log(globalHTTPProxySummary);
   	     boolean value = GlobalHTTPProxySummary.isDisplayed();
   	     String PassStatement1 = "PASS >> global http proxy Summary is correct";
		     String FailStatement1 = "FAIL >> global http proxy Summary is NOT correct";
		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
   	 }
   		if(i==11){
			 typesOfiOSPolicies.get(i).click();
  	     String VPN_Summary =VPNSummary.getText();
  	     Reporter.log(VPN_Summary);
  	     boolean value = VPNSummary.isDisplayed();
  	     String PassStatement1 = "PASS >> VPN Summary is correct";
		     String FailStatement1 = "FAIL >> VPN Summary is NOT correct";
		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
  	 }
   		if(i==12){
			 typesOfiOSPolicies.get(i).click();
 	     String certificateSummary =CertificateSummary.getText();
 	     Reporter.log(certificateSummary);
 	     boolean value = CertificateSummary.isDisplayed();
 	     String PassStatement1 = "PASS >> certificate Summary is correct";
		     String FailStatement1 = "FAIL >> certificate Summary is NOT correct";
		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
 	 }
   		if(i==13){
			 typesOfiOSPolicies.get(i).click();
	     String exchangeActiveSummary =ExchangeActiveSyncSummary.getText();
	     Reporter.log(exchangeActiveSummary);
	     boolean value = ExchangeActiveSyncSummary.isDisplayed();
	     String PassStatement1 = "PASS >> Exchange Active Summary is correct";
		     String FailStatement1 = "FAIL >> Exchange Active Summary is NOT correct";
		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	 }

   		if(i==14){
			 typesOfiOSPolicies.get(i).click();
	     String fileSharingSummary =FileSharingPolicySummary.getText();
	     Reporter.log(fileSharingSummary);
	     boolean value = FileSharingPolicySummary.isDisplayed();
	     String PassStatement1 = "PASS >> File Sharing Summary is correct";
		     String FailStatement1 = "FAIL >> File Sharing Summary is NOT correct";
		     ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	 */


	public void clickOnBlackListAppConfigure() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, BlackListAppProfileConfigButton);
		BlackListAppProfileConfigButton.click();

		waitForidPresent("addblacklistwhitelistAppsBtn");
	}

	public void clickOnWhiteListAppConfigure() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, BlackListAppProfileConfigButton);
		BlackListAppProfileConfigButton.click();

		waitForidPresent("addblacklistwhitelistAppsBtn");
	}

	public void ClickOnAddButton() throws InterruptedException
	{

		ClickOnAddButton.click();
		waitForXpathPresent(".//*[@id='blacklistwhitelistAppsPopup']/div/div/div[3]/button");
		sleep(1);
	}

	public void ClickChooseApp() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		//selecting 4 Apps 
		for(int i=0; i<4;i++)
		{
			if(i==0)
			{

				ClickOnChooseApp.click(); 

				//choosing  Calculator
				Initialization.driver.findElement(By.xpath("//li[text()='Calculator']")).click();
				sleep(1);
				ClickOnAddButtonInBlackListWhiteListApplicationList();


			}

			if(i==1)
			{
				ClickOnAddButton();
				ClickOnChooseApp.click(); 

				//choosing  Calendar
				Initialization.driver.findElement(By.xpath("//li[text()='Calendar']")).click();;
				sleep(1);
				ClickOnAddButtonInBlackListWhiteListApplicationList();

			}
			if(i==2)
			{
				ClickOnAddButton();
				ClickOnChooseApp.click(); 

				//choosing  Camera
				Initialization.driver.findElement(By.xpath("//li[text()='Camera']")).click();
				sleep(1);
				ClickOnAddButtonInBlackListWhiteListApplicationList();

			}
			if(i==3)
			{
				ClickOnAddButton();
				ClickOnChooseApp.click(); 

				//choosing  Clock
				Initialization.driver.findElement(By.xpath("//li[text()='Clock']")).click();
				sleep(1);
				ClickOnAddButtonInBlackListWhiteListApplicationList();       
			}
		}
	}

	//Click NO 
	public void ClickOnNoButonOnDeleteWarningPopUp() throws InterruptedException{
		no_button.click();
		boolean displayed;

		try {
			DeleteButton.isDisplayed();
			displayed = true;
		} catch (Throwable T) 
		{
			displayed = false;

		}
		String PassStatement = "PASS >> Clicked on NO and Pop up dismissed";
		String FailStatement = "FAIL >> Delete warning message unable to click NO";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(2);

	}

	public void ClickOnAddButtonInBlackListWhiteListApplicationList() throws InterruptedException
	{
		AddButton_AppList_BlackList_WhiteList.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='saveiOSProfile']")));
		sleep(2);
	}

	public void SelectFirstRowApp() throws InterruptedException
	{
		DeleteFirstAppinRow.click();
		sleep(2);
	}

	public void ClickOnDeleteButton() throws InterruptedException
	{
		DeleteButton.click();
		sleep(2);
	}

	public void WarningMessageOnApplicationDelete()
	{

		boolean isdisplayed =true;

		try{
			WarningMessageOnDeleteApp.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> delete app warning NOT displayed");
		Reporter.log("PASS >> 'Are you sure you want to delete?' is received",true );	
		waitForPageToLoad();
	}


	//Click YES
	public void ClickOnYesDeleteApplication() throws InterruptedException{
		yes_button.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='blacklistwhitelistTable']/tbody/tr/td[2]"));
		int Actualcount = ls.size();
		int expectedCount=3;
		String pass = "PASS >> Count after Delete is 3";
		String fail = "FAIL >> Count is not correct- failed to delete Application";
		ALib.AssertEqualsMethodInt(expectedCount, Actualcount, pass, fail);
	}

	public void WarningMessageOnClickingAddButtonWithoutChoosingApp()
	{

		boolean isdisplayed =true;

		try{
			WarningMessage_ClickOnAdd_WithoutChoosingApp.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'Please select an application.' is received",true );	
		waitForPageToLoad();
	}

	public void ClickOnSavebutton() throws InterruptedException
	{
		saveButton.click();
		  sleep(3);
	}

	public void WarningMessageSavingProfileWithoutName() throws InterruptedException
	{

		boolean value =true;

		try{
			warningMessageOnSavingWithoutProfileName.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Please enter a Profile Name.'is displayed",true );	
		sleep(3);
	}

	public void EnterBlacklistProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.BlacklistProfileName);
		sleep(1);
	}


	public void EnterWhitelistProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.WhitelistProfileName);
		sleep(1);
	}

	public void EnterWebContentFilterProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.WebContentFilterProfileName);
		sleep(1);
	}


	public void WarningMessageSavingProfileWithoutAllRequiredFields() throws InterruptedException
	{
		sleep(1);
		boolean value =true;

		try{
			warningMessageOnSavingProfileWithoutAllFields.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> warning message NOT displayed");
		Reporter.log("PASS >> Please enter all required fields to save profile.'is displayed",true );	
		sleep(3);
	}

	public void ClickOnWebContentFilter()
	{
		ClickOnWebContentFilter.click();
	}

	//verify summary of Web Content Filter
	public void VerifySummaryof_WebContentFilter() throws InterruptedException{


		String WebContentFiltersummary =WebContentFilterSummary.getText();
		Reporter.log(WebContentFiltersummary);
		sleep(2);
		boolean value = WebContentFilterSummary.isDisplayed();
		String PassStatement1 = "PASS >> web content filter summary is correct";
		String FailStatement1 = "FAIL >>web content filter  summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void clickOnConfigureButton() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, webContentFilterConfigureButton);
		webContentFilterConfigureButton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}


	public void ClickOnAddButtonWebContentFilter() throws InterruptedException
	{
		AddButton_BlacklistURL.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnAddButtonURL() throws InterruptedException
	{
		urlAddbutton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void WarningMessageOnClickingAddButtonWithoutBlacklistedURL()
	{

		boolean isdisplayed =true;

		try{
			warningMessage_WithoutBlacklistURL.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'Enter a valid URL.' is received",true );	
		waitForPageToLoad();
	}

	public void EnterBlacklistURL() throws InterruptedException
	{
		blacklistURL.sendKeys(Config.BlacklistURL);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterAutoFilterURL() throws InterruptedException
	{
		autofilterURL.sendKeys(Config.AutoFilterURL);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void isBlacklistedURL_displayed() throws InterruptedException
	{
		boolean value = isBlacklistURLDisplayed.isDisplayed();
		String pass ="PASS >> blacklisted url is displayed";
		String fail = "FAIl >> blacklisted url is NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void isWhitelistBookmark_displayed() throws InterruptedException
	{
		boolean value = isWhitelistedBookmarkDisplayed.isDisplayed();
		String pass ="PASS >>whitelisted bookmark is displayed";
		String fail = "FAIl >> whitelisted bookmark is NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnWhitelistBookmarks() throws InterruptedException
	{
		WhitelistBookmarks.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void WarningMessageOnClickingAddButtonWithoutWhitelistBookmark()
	{

		boolean isdisplayed =true;

		try{
			warningMessage_WithoutWhiteListbookmark.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'Enter title of the bookmark.' is received",true );	
		waitForPageToLoad();
	}

	public void ClickOnAddButtonWhitelistURL() throws InterruptedException
	{
		AddButton_WhitelistURL.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterTitleWhitelistBookmarkTITLE()
	{
		Title_WhitelistBookmark.sendKeys(Config.Title_WhitelistBookmark);
	}

	public void EnterWhitelistBookmark_URL()
	{
		URL_WhitelistBookmark.sendKeys(Config.URL_WhitelistBookmark);
	}

	public void EnterWhitelistBookmark_Bookmarkpath() throws InterruptedException
	{
		BookmarkPath_WhitelistBookmark.sendKeys(Config.BookmarkPath_WhitelistBookmark);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterFilteredURL() throws InterruptedException
	{
		BookmarkPath_WhitelistBookmark.sendKeys(Config.BookmarkPath_WhitelistBookmark);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void WarningMessageOnClickingAddButtonWhitelistBookmarkPath()
	{

		boolean isdisplayed =true;

		try{
			warningMessage_WithoutBookmarkPath.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'Enter bookmark path.' is received",true );	
		waitForPageToLoad();
	}

	public void WarningMessageOnClickingAddButtonWithoutBookmarkPath()
	{

		boolean isdisplayed =true;

		try{
			warningMessage_WithoutBookmarkPath.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'Enter bookmark path.' is received",true );	
		waitForPageToLoad();
	}

	public void ClickOnAutoFilter() throws InterruptedException
	{
		AutoFilter.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void CheckEnableAutoFiltering()
	{
		AutoFilter_CheckBox.click();
	}

	public void ClickOnAddButton_AutoFilter() throws InterruptedException
	{
		Addbutton_AutoFilter.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void isFilteredURL_displayed() throws InterruptedException
	{
		boolean value = isWhitelistedBookmarkDisplayed.isDisplayed();
		String pass ="PASS >>filtered URL is displayed";
		String fail = "FAIl >>filtered URL is NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void isAutoFilterURL_displayed() throws InterruptedException
	{
		boolean value = AutoFilterURLDisplayed.isDisplayed();
		String pass ="PASS >> AutoFilter url is displayed";
		String fail = "FAIl >> AutoFilter url is NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait	(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnBrandingOption() throws InterruptedException
	{
		BandingOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Branding
	public void VerifySummaryof_Branding() throws InterruptedException{


		String Brandingsummary =BrandingSummary.getText();
		Reporter.log(Brandingsummary);
		sleep(2);
		boolean value = BrandingSummary.isDisplayed();
		String PassStatement1 = "PASS >> branding summary is correct";
		String FailStatement1 = "FAIL >>branding summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void clickOnBranding_ConfigureButton() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, BrandingConfigureButton);
		BrandingConfigureButton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterBrandingProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.BrandingProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnHomeScreenWallpaperImagePicker() throws InterruptedException
	{
		iOSImagePicker_HomeScreenWallpaper.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void RunAutoIt() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("D:\\UplaodFiles\\BrandingiOSProfile\\brandinghomepage.exe");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnPasscodePolicyOption() throws InterruptedException
	{
		passcodePolicyOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of passcode policy
	public void VerifySummaryof_PasscodePolicy() throws InterruptedException{


		String passcodePolicyummary =PasscodePolicySummary.getText();
		Reporter.log(passcodePolicyummary);
		sleep(2);
		boolean value = PasscodePolicySummary.isDisplayed();
		String PassStatement1 = "PASS >> passcode policy summary is correct";
		String FailStatement1 = "FAIL >> passcode policy summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButton_PasscodePolicy() throws InterruptedException
	{
		ConfigureButton_PasscodePolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterPasscodePolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.PasscodePolicyProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}


	public void VerifyPasscodePolicyParameters(){
		Reporter.log("======Verify Total parameter count of Passcode Policy======");
		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='passcodeProfileTail']/ul/li/div[1]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 10;
		String PassStatement2 = "PASS >> Total passcode parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total passcode parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify Passcode Policy parameters one by one=========");
		for(int i=0;i<totalParameters;i++){
			if(i==0){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				boolean value = FirstParameterPasscodePolicy.isDisplayed();
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Allow Simple Value";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==2){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Require Alphanumeric Value";
				String PassStatement ="PASS >> 3rd option is present - correct";
				String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Minimum Passcode Length";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Minimum Number Of Complex Characters";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==5){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Maximum Passcode Age (1-730 Days, Or None)";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==6){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Maximum Auto-Lock";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==7){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Passcode History (1-50 Passcodes, Or None)";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==8){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Maximum Grace Period For Device Lock";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==9){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Maximum Number Of Failed Attempts";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
	}

	public void ClickOnForceUserToSetPasscode()
	{
		CheckBox_ForceUserToSetPasscode.click();
	}
	public void ClickOnRequireAlphanumericValue()
	{
		CheckBox_RequireAlphanumericValue.click();
	}

	public void VerifyChoosingMinimumPasscodeLength() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify choosing minimum passcode length as 4 ====");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ChooseMinimumPasscodeLength.click();
		sleep(1);
		Initialization.driver.findElement(By.xpath("//li[text()='4']")).click();
		sleep(1);
		boolean value = isFourDisplayed.isDisplayed();
		String pass="PASS >> '4' is choosen as length";
		String fail="FAIL >> '4' is NOT choosen as length";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(2);


	}
	public void VerifyChoosingMinimumNumberOfComplexCharacters() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify choosing minimum number of Complex Characters====");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ChoosePasscodeComplextCharacter.click();
		sleep(1);
		Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",167,2)+"']")).click();;
		sleep(2);
	}

	public void EnterPasscodeAge() throws InterruptedException
	{
		passcodeAge.sendKeys(Config.PasscodeAge);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void VerifyChoosingMaxiumAutoLock() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify choosing maximum Auto Lock as 2 minutes===");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		MaximuAutoLock.click();
		sleep(1);
		Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",173,2)+"']")).click();;
		sleep(2);


	}

	public void EnterPasscodeHistory() throws InterruptedException
	{
		PasscodeHistory.sendKeys(Config.PasscodeHistory);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyChoosingMaxiumGracePeriodForDeviceLock() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify choosing maximum Grace Period For Device Lock===");

		MaxGracePeriodForDeviceLock.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",181,2)+"']")).click();;
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClicOnSingleAppModeProfile() throws InterruptedException
	{
		SingleAppModeProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButtonSAMProfile() throws InterruptedException
	{
		ConfigureButton_SAMProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Single App mode Profile
	public void VerifySummaryof_SingleAppModeProfile() throws InterruptedException{


		String SingleAppModeProfilesummary =Summary_SAMModeProfile.getText();
		Reporter.log(SingleAppModeProfilesummary);
		sleep(2);
		boolean value = Summary_SAMModeProfile.isDisplayed();
		String PassStatement1 = "PASS >> Single App Mode Profile summary is correct";
		String FailStatement1 = "FAIL >>Single App Mode Profile summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterSAMProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.SAMProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void VerifyChoosingLockDownApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify choosing Lockdown applicaion===");

		LockdownApplicationListiOS.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//li[text()='Calculator']")).click();;
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnLaunchPeriodically() throws InterruptedException
	{
		Checkbox_launchPeriodically.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}
	public void ClearLaunchPeriodTime() throws InterruptedException
	{
		launchPeriod.clear();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterLaunchPeriodTime() throws InterruptedException
	{
		launchPeriod.sendKeys(Config.LaunchPeriodTime);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnSingleAppMode() throws InterruptedException
	{
		singleappMode.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void LockDownApplicationSummary() throws InterruptedException
	{
		boolean value = summary_LockDownApplication.isDisplayed();
		String pass = "PASS >> summary is 'Lockdown works only for the apps installed on the device.'";
		String fail = "FAIL >> summary is wrong";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnRestrictionProfile()
	{
		restrictionProfile.click();
	}

	//verify summary of Restriction Profile
	public void VerifySummaryofRestrictionProfile() throws InterruptedException{


		String RestrictionProfilesummary =RestrictionProfileSummary.getText();
		Reporter.log(RestrictionProfilesummary);
		sleep(2);
		boolean value = RestrictionProfileSummary.isDisplayed();
		String PassStatement1 = "PASS >> Restriction Profile summary is correct";
		String FailStatement1 = "FAIL >> Restriction Profile summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButtonRestrictionProfile()
	{
		ConfigureButton_RestrictionProfile.click();
	}
	public void DisableThreeFunctinalitySettings()
	{
		CheckBox_AllowUseOfCamera.click();
		CheckBox_AllowScreenshotsAndScreenRecording.click();
		CheckBox_AllowSiri.click();
	}

	public void VerifyScrollDown_Functionality() throws InterruptedException{


		Reporter.log("To verify Scroll down to the last Element of Functionality");
		WebElement scrollDown = LastParameter_ForceWifiWhiteListing_Functionality;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		boolean isScrolledDown = LastParameter_ForceWifiWhiteListing_Functionality.isDisplayed();
		String pass = "PASS >> 'Force WiFi Whitelisting (supervised only, iOS 10.3 onwards)' is displayed - Scrolled Down to the last element succesfully";
		String fail = "FAIL >> 'Force WiFi Whitelisting (supervised only, iOS 10.3 onwards)' is NOT displayed - Failed to Scroll Down to the last element succesfully";
		ALib.AssertTrueMethod(isScrolledDown, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyScrollUP_Functionality() throws InterruptedException{
		Reporter.log("To verify Scroll up to the first Element of Functionality");

		WebElement scrollUp = FirstParameter_AllowUseOfCamera_Functionality;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollUp);
		boolean isScrolledUP = FirstParameter_AllowUseOfCamera_Functionality.isDisplayed();
		String pass1 = "PASS >> 'Allow use of Camera' is displayed - Scrolled UP to the first element succesfully";
		String fail1 = "FAIL >> 'Allow use of Camera' is NOT displayed - NOT Scrolled UP to the first element";
		ALib.AssertTrueMethod(isScrolledUP, pass1, fail1);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnAppsTab() throws InterruptedException
	{
		AppsTab.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnAllowUseOfNews() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CheckBox_AllowUseOfNews.click();
		sleep(1);
	}
	public void ClickOnAllowUseOfSafari() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CheckBox_AllowUseOfSafari.click();
		sleep(1);
	}
	public void VerifyScrollDownApps() throws InterruptedException{


		Reporter.log("To verify Scroll down to the bottm of Apps");
		WebElement scrollDown = ScrollDown_AppsBottom;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		boolean isScrolledDown = ScrollDown_AppsBottom.isDisplayed();
		String pass = "PASS >> Scrolled to bottom";
		String fail = "FAIL >> Not Scrolled to bottom";
		ALib.AssertTrueMethod(isScrolledDown, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnAllowCookiesOption() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		AllowCookiesOption.click();
		sleep(2);
	}

	public void ClickOnMediaContentTab() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		MediaContentTab.click();
		sleep(2);
	}

	public void EnteMediaContentProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.RestrictionProfileName);
		sleep(1);
	}

	public void ClickOnApplicationPolicy() throws InterruptedException
	{
		ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	//verify summary of Application Profile
	public void VerifySummaryofApplicationPolicyProfile() throws InterruptedException{


		String AppPolicysummary =ApplicationPolicySummary.getText();
		Reporter.log(AppPolicysummary);
		sleep(2);
		boolean value = ApplicationPolicySummary.isDisplayed();
		String PassStatement1 = "PASS >> Application Policy summary is correct";
		String FailStatement1 = "FAIL >> Application Policy summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButtonApplicationPolicyProfile() throws InterruptedException
	{
		ConfigureButton_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnAddButon_ApplicationPolicy() throws InterruptedException
	{
		AddButton_ApplicationPolicy.click();
		waitForXpathPresent(".//*[@id='ios_addEAMAppModal']/div/div/div/button[text()='Add']");
		sleep(6);
	}

	public void EnterApplicationPolicyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.ApplicationPolicyProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ChooseAppApplicationPolicyProfile() throws InterruptedException
	{
		ChooseApp_ApplicationPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void SelectingApplicationmFromtheAddAppDropdown() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=======verify selecting the App from the dropdown ====");
		//Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",191,2)+"']")).click();;
		Initialization.driver.findElement(By.xpath("//li[contains(text(),'SureFox')]")).click();;
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);   
	}

	public void ClickOnInstallSilently() throws InterruptedException
	{
		Checkbox_InstallSilently.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClicOnCancelButtonAddAppWindow() throws InterruptedException
	{
		CancelButton_AddApp.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(3);
	}
	public void isColumnNamePresent() throws InterruptedException
	{
		boolean value = NameColumnName.isDisplayed();
		String pass = "PASS >> Name Column is displayed- Cancelled 'Add App' window";
		String fail ="FAIL >>  Name Columnis NOT displayed- NOT Cancelled 'Add App' window";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnAddButtonAddAppWindow() throws InterruptedException
	{
		AddButton_AddApp.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(3);
	}


	public void isAddedApp1DisplayedinTheList() throws InterruptedException
	{
		boolean value = isFirstAppPresent.isDisplayed();
		String pass = "PASS >>  facebook is displayed in the list - App got addedd successfully";
		String fail = "FAIL >>  facebook is NOT displayed in the list - App NOT added ";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void WarningMessageOnAddingTheSameApplcation()
	{

		boolean isdisplayed =true;

		try{
			warningMessageAddingtheSameApplication_AppPolicy.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> 'This App is already added' NOT displayed");
		Reporter.log("PASS >> 'This App is already added' is received",true );	
		waitForPageToLoad();
	}

	public void SelectingDifferentApplicationmFromtheAddAppDropdown() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=======verify selecting Myntra App from the dropdown ====");
		Initialization.driver.findElement(By.xpath("//li[contains(text(),'123 Movies')]")).click();;
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isAddedApp2DisplayedinTheList() throws InterruptedException
	{
		boolean value = is2ndPresentPresent.isDisplayed();
		String pass = "PASS >>  myntra is displayed in the list - App got addedd successfully";
		String fail = "FAIL >>  myntra is NOT displayed in the list - App NOT added ";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void VerifyConfig_ApplicationPolicy() throws InterruptedException
	{
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		isFirstAppPresent.click();
		sleep(1);
		ConfigButton_AppPolicy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		Addconfig.click();
		
		key_AppConfiguration_AppPolicy.sendKeys(Config.EnterKeyApplicationPolicy);
		Value_AppConfiguration_AppPolicy.sendKeys(Config.EnterValueApplicationPolicy);
		sleep(1);
		AddKeyValue.click();
		SaveButton_ApplicationConfiguration.click();
		Reporter.log("PASS >> Entered key and value configuration ");
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnEditButton() throws InterruptedException
	{
		EditButton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isEditWindowDisplayed() throws InterruptedException
	{
		boolean value = EditEnterpriseAppStoreAppWindow.isDisplayed();
		String pass = "PASS >> 'Edit Enterprise App Store App' window displayed";
		String fail = "FAIL >> 'Edit Enterprise App Store App' window NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnMyntraApp() throws InterruptedException
	{
		ClickOnMyntraInTheList.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void ClickOnDeleteButtonApplicationPolicy()
	{
		ClickOnDeleteButton.click();
	}

	public void WarningMessageOnApplicationDelete_ApplicationPolicy() throws InterruptedException
	{

		boolean isdisplayed =true;

		try{
			WarningMessageDeletingApp_ApplicationPolicy.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> delete app warning NOT displayed");
		Reporter.log("PASS >> 'Selected Application will be deleted. Do you wish to continue?' is displayed",true );	
		waitForPageToLoad();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnYesButton_DeleteWarning() throws InterruptedException
	{
		YesButton_DeleteWarningDialog.click();
		sleep(2);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		List<WebElement> totalAppsAddded = Initialization.driver.findElements(By.xpath(".//*[@id='mam_installApplicationPolicyTable']/tbody/tr"));
		int expected = totalAppsAddded.size();
		int actual =1; 
		String pass = "PASS >> Total count is:1 , App delete successfully";
		String fail ="FAIL >> Total count is not 1, APP NOT deleted";
		ALib.AssertEqualsMethodInt(expected, actual, pass, fail);
	}

	public void ClickOnConfigurationProfile() throws InterruptedException
	{
		ConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Application Profile
	public void VerifySummaryofConfigurationProfile() throws InterruptedException{


		String ConfigurationProfilesummary =ConfigurationProfileSummary.getText();
		Reporter.log(ConfigurationProfilesummary);
		sleep(2);
		boolean value = ConfigurationProfileSummary.isDisplayed();
		String PassStatement1 = "PASS >> Configuration Profile summary is correct";
		String FailStatement1 = "FAIL >> Configuration Profile summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButtonConfigurationProfile() throws InterruptedException
	{
		ConfigureButton_ConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterConfigurationProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.ConfigurationProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnAddButtonConfigurationProfiles() throws InterruptedException
	{

		ClickOnAddButton_ConfigurationProfile.click();
		waitForXpathPresent(".//*[@id='applicationsConfigurationPopup']/div/div/div[3]/button");
		sleep(1);
	}

	public void ClickOnAddButtonInConfigurationProfileApplicationList() throws InterruptedException
	{
		AddButton_ApplicationList_ConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='saveiOSProfile']")));
		sleep(2);
	}

	public void WarningMessageOnClickingAddButtonWithoutChoosingApp_ConfigurationProfile() throws InterruptedException
	{

		boolean isdisplayed =true;

		try{
			WarningWhenClickingAddButtonWithoutApplication_ConfigurationProfile.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'All fields are necessary' is received",true );	
		waitForPageToLoad();
		sleep(2);

	}


	public void ChooseApp_ConfigurationProfile() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException, AWTException
	{
		//selecting 4 Apps 
		for(int i=0; i<2;i++)
		{
			if(i==0)
			{

				ClickOnChooseApp_ConfigurationProfile.click(); 
				sleep(2);
				//choosing  Calendar
				TextField_EnterAppName.sendKeys("Calculator");
				sleep(2);
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				sleep(2);
				Key_ApplicationConfiguration.sendKeys("key");
				Value_ApplicationConfiguration.sendKeys("value");
				ClickOnAddButtonInConfigurationProfileApplicationList();
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);

				//verify if the Calculator App got added
				boolean value = isCalculatorAdded.isDisplayed();
				String pass ="PASS >>  Calculator App is added";
				String fail ="FAIL >> Calculator App is NOT Added";
				ALib.AssertTrueMethod(value, pass, fail);
			}
			if(i==1)
			{
				ClickOnAddButtonConfigurationProfiles();
				ClickOnChooseApp_ConfigurationProfile.click(); 
				sleep(2);
				//choosing  Calendar
				TextField_EnterAppName.sendKeys("Calendar");
				sleep(2);
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				sleep(2);
				Key_ApplicationConfiguration.sendKeys("key");
				Value_ApplicationConfiguration.sendKeys("value");
				ClickOnAddButtonInConfigurationProfileApplicationList();
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);

				//Verify if the Calendar App got added
				boolean value = isCalendarAdded.isDisplayed();
				String pass ="PASS >>  Calendar App is added";
				String fail ="FAIL >> Calendar App is NOT Added";
				ALib.AssertTrueMethod(value, pass, fail);
			}
		}
	}

	public void FirstApplicationFromList() throws InterruptedException
	{
		isCalculatorAdded.click(); //clicking on the Calculator App in the list.
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnDeleteButtonConfigureProfile() throws InterruptedException
	{
		DeleteButton_ConfigurationProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickYesButtonOnWarningDialog_DeleteApp_ConfigurationProfile() throws InterruptedException
	{
		yes_button.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
		List<WebElement> totalAppsAddded = Initialization.driver.findElements(By.xpath(".//*[@id='applicationsConfigurationTable']/tbody/tr"));
		int expected = totalAppsAddded.size();
		int actual =1; 
		String pass = "PASS >> Total count is:1 , App deleted successfully";
		String fail ="FAIL >> Total count is not 1, APP NOT deleted";
		ALib.AssertEqualsMethodInt(expected, actual, pass, fail);
	}

	public void ClickOnWifiConfigurationProfile() throws InterruptedException
	{
		WifiConfigurationProfileOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Wi-Fi Configuration
	public void VerifySummaryofWifiConfigurationProfile() throws InterruptedException{


		String WifiConfigurationprofileSummary =WifiConfgurationSummary.getText();
		System.out.println(WifiConfigurationprofileSummary);
		sleep(2);
		boolean value = WifiConfgurationSummary.isDisplayed();
		String PassStatement1 = "PASS >> Wifi Configuration Profile summary is correct";
		String FailStatement1 = "FAIL >> Wifi Configuration Profile summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButtonWiFiConfigurationProfile() throws InterruptedException
	{
		ConfigureButton_WifiConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterWifiConfigurationProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterWifiConfigurationProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnAddButton_WifiConfiguration() throws InterruptedException
	{
		AddButton_WifiConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void isWifiConfigurationWindowDisplayed()
	{
		boolean value = wifiConfigurationWindow.isDisplayed();
		String pass ="PASS >> Wifi Configuration window displayed - Clicked on ADD button";
		String fail = "FAIL >> Wifi Configuration window NOT displayed - Did not clicked on Add button";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void EnterSSID_WifiConfiguration() throws InterruptedException
	{
		SSIDTextBox.sendKeys(Config.EnterSSID);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void ClickOnSaveButton_WifiConfiguration() throws InterruptedException
	{
		SaveButton_WifiConfigurationWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void warningMessageWithoutSSIDorPasswordorServerURL() throws InterruptedException
	{

		boolean isdisplayed =true;

		try{
			warningWithoutSSIDorServerURL.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >>  warning NOT displayed");
		Reporter.log("PASS >> 'Please fill this field' is received",true );	
		waitForPageToLoad();
		sleep(2);
	}

	public void ClickOnSecuritytypeDropDownWifiConfigurationandChoose() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		SecurityTypeDropdown.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
		//choosing  OpenVPN
		Initialization.driver.findElement(By.xpath("//li[text()='WEP']")).click();;
		sleep(1);
		autoJoin_CheckBox.click();
	}
	
	public void ChooseProxySetupDropDown() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		ProxySetupDropdown.click();
		Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",203,2)+"']")).click();;
		sleep(1);
	}

	public void EnterPassword_WiFiConfiguration() throws InterruptedException
	{
		password_WiFiConfiguration.sendKeys(Config.EnterWifiConfigurationPassword);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void warningMessageWithoutServerorPort()
	{

		boolean isdisplayed =true;

		try{
			warningWithoutServerAndPort.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >>  warning NOT displayed");
		Reporter.log("PASS >> 'Server URL and Port are required' is received",true );	
		waitForPageToLoad();
	}

	public void EnterServerAndPort() throws InterruptedException
	{
		ServerAndPortTextField.sendKeys(Config.EnterServer);
		PortTextField.sendKeys(Config.EnterPort);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnMailConfigurationProfile() throws InterruptedException
	{
		mailConfigurationOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnConfigureButtonMailConfigurationProfile() throws InterruptedException
	{
		ConfiguraButton_MailConfiguration.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterMailConfigurationProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterMailConfigurationProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void IncomingMail() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		AccountDescription.sendKeys(Config.EnterAccountDescription);
		EmailAddress.sendKeys(Config.EnterEmailAddress);
		Checkbox_AllowUserToMoveMessageFromThisAccount.click();
		Checkbox_AllowRecentAddressesToBeSynced.click();
		Checkbox_UseOnlyInMail.click();
		URL_MailServerAndPort.sendKeys(Config.EnterMailServerURL);
		Port_MailConfiguration.sendKeys(Config.EnterPort);
		Username_MailConfiguration.sendKeys(Config.EnterUsername);
		AuthenticationType.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
		//Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",206,2)+"']")).click();;
		//sleep(1);
		//SSL.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnOutogingMail() throws InterruptedException
	{
		OutgoingMailOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterOutgoingMailDetail() throws InterruptedException
	{
		MailServer_Outgoing.sendKeys(Config.EnterMailServerURL);
		port_Outgoing.sendKeys(Config.EnterPort);
		username_outgoing.sendKeys(Config.EnterUsername);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
		SSL_Outgoing.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnGlobalHTTPProxy() throws InterruptedException
	{
		GlobalHTTPProxy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//verify summary of Global HTTP Proxy
	public void VerifySummaryofGlobalHTTPProxy() throws InterruptedException{


		String GlobalHTTPproxySummary =GlobalHTTPProxySummary.getText();
		System.out.println(GlobalHTTPproxySummary);
		sleep(2);
		boolean value = GlobalHTTPProxySummary.isDisplayed();
		String PassStatement1 = "PASS >> Global HTTP Proxy summary is correct";
		String FailStatement1 = "FAIL >> Global HTTP Proxy summary is NOT correct";
		ALib.AssertTrueMethod(value,PassStatement1, FailStatement1);
	}

	public void ClickOnConfigureButtonGlobalHTTPProxy() throws InterruptedException
	{
		ConfigureButton_GlobalHTTPProxy.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterGlobalHTTPProxyProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterGlobalHTTPProxyProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void ProxyServerURL_GlobalHTTPProxy()
	{
		serverURL_GlobalHTTPProxy.sendKeys(Config.EnterProxyServerURL);
	}

	public void Checkbox_AllowBypassingProxy()
	{
		checkbox_AllowBypassingProxyToAccessCaptiveNetworks.click();
	}
	public void EnterProxyServerPort_GlobalHTTPProxy()
	{
		port_GlobalHTTPProxy.sendKeys(Config.EnterPort);
	}
	public void warningWithoutProyServerAndPort()
	{
		boolean value = warningWithoutProxyServerURL_GlobalHTTPPort.isDisplayed();
		String pass = "PASS >> warning 'Server URL is required' displayed";
		String fail="FAIL >> warning not displayed";
		ALib.AssertTrueMethod(value, pass, fail);

	}
	public void ClickOnVPN() throws InterruptedException
	{
		VPNOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void ClickOnConfigureButtonVPNProfile() throws InterruptedException
	{
		ConfigureButton_VPN.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void EnterVPNProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterVPNProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	//Verify all parameters of VPN profile

	public void VerifyVPNParameters(){
		Reporter.log("======Verify Total parameter count of VPN profile======");

		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='VPNProfileTail']/ul/li/div[1]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		System.out.println(totalParameters);
		int expectedParameters =23;
		String PassStatement2 = "PASS >> Total VPN parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total VPN parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one only upto password
		Reporter.log("========Verify VPN parameters one by one=========");

		for(int i=0;i<totalParameters;i++){
			if(i==0){

				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Connection Name";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Connection Type";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==2){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Server";
				String PassStatement ="PASS >> 3rd option is present - correct";
				String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Account";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
	}

	public void EnterConnectionName()
	{
		connectionName_VPN.sendKeys(Config.EnterConnectionName);
	}
	public void ChooseConnectionType() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		chooseConnectiontype.click();
		Initialization.driver.findElement(By.xpath("//li[text()='Pulse Secure']")).click();
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();

		}
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void EnterServer_VPN()
	{
		Server_VPN.sendKeys(Config.EnterServerVPN);
	}

	public void EnterPasswordVPN()
	{
		password_VPN.sendKeys(Config.EnterPasswordVPN);
	}

	public void ClickOnCertificate() throws InterruptedException
	{
		CertificateProfile.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}
	public void isErrorDisplayedWithoutCertificateUpload() throws InterruptedException
	{
		boolean isErrorDisplayed = errorWithoutCertificateUpload.isDisplayed();
		String pass = "PASS >> error 'Please upload certificate file.' is displayed";
		String fail = "FAIL >> error is NOT displayed";
		ALib.AssertTrueMethod(isErrorDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnExchangeActiveSync() throws InterruptedException
	{
		ExchangeActiveSyncOption.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void ClickOnConfigureButtonExchangeActiveSyncProfile() throws InterruptedException
	{
		ConfigureButton_ExchangeActiveSync.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	//Verify all parameters of Exchange Active Sync

	public void VerifyExchangeActiveSyncParameters(){
		Reporter.log("======Verify Total parameter count of Exchange Active Sync profile======");

		List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='exchangeProfileTail']/ul/li/div[1]"));
		//verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters =23;
		String PassStatement2 = "PASS >> Total Exchange Active Sync parameter count is: "+expectedParameters+"  ";
		String FailStatement2 = "FAIL >> Total Exchange Active Sync parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify Exchange Active Sync parameters one by one=========");

		for(int i=0;i<totalParameters;i++){
			if(i==0){

				String actual =parameters.get(i).getText();	
				Reporter.log(actual);
				String expected = "Account Name";
				String PassStatement ="PASS >> 1st option is present - correct";
				String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==1){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Exchange ActiveSync Host";
				String PassStatement ="PASS >> 2nd option is present - correct";
				String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==2){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Use SSL";
				String PassStatement ="PASS >> 3rd option is present - correct";
				String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==3){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "User";
				String PassStatement ="PASS >> 4th option is present - correct";
				String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==4){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Email Address";
				String PassStatement ="PASS >> 5th option is present - correct";
				String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if(i==5){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Password";
				String PassStatement ="PASS >> 6th option is present - correct";
				String FailStatement ="FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if(i==6){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Days To Sync";
				String PassStatement ="PASS >> 7th option is present - correct";
				String FailStatement ="FAIL >> 7th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);		
			}
			if(i==7){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Authentication";
				String PassStatement ="PASS >> 8th option is present - correct";
				String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);		
			}
			if(i==8){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Allow Messages To Be Moved";
				String PassStatement ="PASS >> 8th option is present - correct";
				String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);		
			}
			if(i==9){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Allow Recent Addresses To Be Synced";
				String PassStatement ="PASS >> 8th option is present - correct";
				String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);		
			}
			if(i==10){	
				String actual =parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Use Only In Mail";
				String PassStatement ="PASS >> 8th option is present - correct";
				String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);		
			}
		}
	}

	public void EnterExchangeActiveSyncProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnterExchangeActiveSyncProfileName);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}

	public void VerifyAllOptionsOfDaysToSync() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======verify if all the options present in Days to sync dropdown ====");
		//verify if all the options present in dropdown
		for(int i=0; i<6;i++) // total 5 options
		{
			if(i==0){
				Initialization.driver.findElement(By.id("ios_exchange_days_chosen")).click();
				Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",i+211,2)+"']")).click();;
				sleep(1);
				boolean value = OneDay.isDisplayed();
				String pass="PASS >> '1 Day' is available";
				String fail="FAIL >> '1 Day' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==1){
				Initialization.driver.findElement(By.id("ios_exchange_days_chosen")).click();
				Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",i+211,2)+"']")).click();;
				sleep(1);
				boolean value = ThreeDays.isDisplayed();
				String pass="PASS >> '3 days' is available";
				String fail="FAIL >> '3 days' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==2){
				Initialization.driver.findElement(By.id("ios_exchange_days_chosen")).click();
				Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",i+211,2)+"']")).click();;
				sleep(1);
				boolean value = OneWeek.isDisplayed();
				String pass="PASS >> '1 week' is available";
				String fail="FAIL >> '1 week' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if(i==3){
				Initialization.driver.findElement(By.id("ios_exchange_days_chosen")).click();
				Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",i+211,2)+"']")).click();;
				sleep(1);
				boolean value = TwoWeeks.isDisplayed();
				String pass="PASS >> '2 weeks' is available";
				String fail="FAIL >> '2 weeks' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}
			if(i==4){
				Initialization.driver.findElement(By.id("ios_exchange_days_chosen")).click();
				Initialization.driver.findElement(By.xpath("//li[text()='"+ELib.getDatafromExcel("Sheet15",i+211,2)+"']")).click();;
				sleep(1);
				boolean value = OneMonth.isDisplayed();
				String pass="PASS >> '1 month' is available";
				String fail="FAIL >> '1 month' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				sleep(2);
			}
		}
	}

	public void ClickOnPlusButton_ChooseAuthenticationCertificate() throws InterruptedException
	{
		chooseCertificate_PlusButon.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void ClickOnAddButtonOfCertificateWindow() throws InterruptedException
	{
		AddButton_CertificateWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(1);
	}
	public void CertificateErrorWhenCertificateIsNotUploaded()
	{
		String ActualError = certficateError.getText();
		String ExpectedError = "Please upload certificate file.";
		String pass = "PASS >> Error displayed is "+ActualError+" ";
		String fail = "FAIL >> Errot displayed is "+ActualError+" ";
		ALib.AssertEqualsMethod(ExpectedError, ActualError, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public void CloseCertificatePopUp() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//*[@id=\"certificateAddPopup\"]/div/div/div[1]/div")).click();
		sleep(2);
	}
	public void CertificateErrorWhenCertificatePassworIsNotProvided()
	{
		String ActualError = certficateError.getText();
		String ExpectedError = "Please upload certificate file.";
		String pass = "PASS >> Error displayed is "+ActualError+" ";
		String fail = "FAIL >> Errot displayed is "+ActualError+" ";
		ALib.AssertEqualsMethod(ExpectedError, ActualError, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			sleep(1);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public void WarningMessageSavingCertificateWithoutPassword()
	{

		boolean isdisplayed =true;

		try{
			passwordError.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >>  password error NOT displayed");
		Reporter.log("PASS >> 'Please fill this field.' is received",true );	
		waitForPageToLoad();
	}

	public void EnterPasswordForCertificate() throws InterruptedException
	{
		CertificatePassword.sendKeys(Config.EnterCertificatePassword);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void isCertificateDisplayedInAuthenticationFieldAfterUpload() throws InterruptedException
	{
		boolean isDisplayed = isUploadedCertDisplayed.isDisplayed();
		String pass = "PASS >> Uploaded certificate is displayed- cert uploaded successfully";
		String fail = "FAIL >> Uploaded certificate is NOT displayed";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void EnterExchangeactiveSyncDetails()
	{
		AccountName.sendKeys(Config.EnterAccountName);
		ExchangeActiveSyncHost.sendKeys(Config.EnterExchangeActiveSyncHost);
		checkbox_UseSSL.click();
		User_ExchangeActiveSync.sendKeys(Config.EnterUser);
		EmailAddress_ExchangeActiveSync.sendKeys(Config.EnterEmailAddressExchangeActiveSync);
		Password_ExchangeActiveSync.sendKeys(Config.EnterPasswordExchangeActiveSync);
		checkbox_AllowMessagesToBeMoved.click();
		checkbox_AllowRecentAddressesToBeSynced.click();
		checkbox_UseOnlyInMail.click();
	}

	public void ClickOniOSCertificatePolicyConfigButton() throws InterruptedException
	{
		ConfigureButton_CertificateiOS.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}

	public void EnterCertificateiOSProfileName() throws InterruptedException
	{
		profileName.sendKeys(Config.EnteriOSCertificateProfileName);
		sleep(1);
	}

	public void VerifyWarningMessageWithoutAddingCertificatePayload() throws InterruptedException{

		boolean value =true;

		try{
			warningMessageOnSavingCertificateprofileWithoutPayload.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}

		Assert.assertTrue(value,"FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Please add at least one payload to save the profile.'is displayed",true );	
		sleep(3);
	}

	@FindBy(xpath="//span[@title='Wi-Fi Configuration']") //other xpath did not work
	private WebElement WifiConfigurationProfileOption;
	
	@FindBy(xpath="//*[@id='policy_item_box']/div[4]/div/ul/li[11]/span[text()='Mail Configuration']") //other xpath did not work
	private WebElement mailConfigurationOption;
	
	@FindBy(xpath="//span[@title='VPN']")//other test case did not work
	private WebElement VPNOption;
	
	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;
	
	public void SearchMultipleDevice() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
            SearchDeviceTextField.clear();
            SearchDeviceTextField.sendKeys(Config.MultipleDeviceName);
            sleep(10);
    }
}

























































