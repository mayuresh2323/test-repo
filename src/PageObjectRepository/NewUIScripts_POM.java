package PageObjectRepository;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class NewUIScripts_POM extends WebDriverCommonLib {
	AssertLib ALib = new AssertLib();

	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	@FindBy(id = "swichtoNewConsole")
	private WebElement TryNewConsoleBtn;

	@FindBy(id = "uName")
	private WebElement EnterUserNameInNewUI;

	@FindBy(id = "pass")
	private WebElement EnterPwdInNewUI;

	@FindBy(id = "loginBtn")
	private WebElement NewUILoginBtn;

	@FindBy(xpath = "//div[@id='warningArea']")
	private List<WebElement> WrongMessage;

	public void ClickOnTryNewConsole() throws InterruptedException {
		try {
			TryNewConsoleBtn.isDisplayed();
			TryNewConsoleBtn.click();
			Reporter.log("PASS>>Navigated To New UI Console...", true);
			waitForidPresent("swichtoOldConsole");

		} catch (Exception e) {
			if (Initialization.driver.findElement(By.id("swichtoOldConsole")).isDisplayed()) {
				Reporter.log("PASS>>Already present in  New UI Console...", true);
			}
		}
	}

	public void EnterUserNameInNewUIConsole(String un) throws InterruptedException {
		EnterUserNameInNewUI.clear();
		sleep(2);
		EnterUserNameInNewUI.sendKeys(un);
	}

	public void EnterPwdNameInNewUIConsole(String pwd) throws InterruptedException {
		EnterPwdInNewUI.clear();
		sleep(2);
		EnterPwdInNewUI.sendKeys(pwd);
	}

	public void ClickOnNewUILoginBtn() throws InterruptedException {
		NewUILoginBtn.click();
		sleep(5);
	}

	public void VerifyOfWarningMsgInNewUI() {
		for (int i = 0; i < WrongMessage.size(); i++) {
			String WrongMsg = WrongMessage.get(i).getText();
			if (WrongMsg.contains("Wrong Credentials!")) {
				String PassStatement = "PASS >> It display error message successfully";
				System.out.println(PassStatement);
			} else {
				String FailStatement = "Fail >> It didn't display error message successfully";
				ALib.AssertFailMethod(FailStatement);
			}
		}
	}

	public void VerifyOfLogoutInNewUI() {
		String ActualValue = Initialization.driver.findElement(By.id("loginBtn")).getText();
		String ExpectedValue = "Login";
		String PassStatement = "PASS >> Console logged out successfully" + ActualValue;
		String FailStatement = "Fail >> Unable to logout from console " + ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void clickOnYesBtnInApplyJobToGroup() throws InterruptedException {
		Initialization.driver.findElement(By.cssSelector("div[id='deviceConfirmationDialog'] button:nth-child(2)")).click();
		sleep(2);
		
	}

	public void VerifyOfappliedJobOnGroup(String job, String device1, String device2) throws InterruptedException {
		boolean ExpectedStatus = Initialization.driver
				.findElement(By.xpath("//p[contains(text(),'The job named "+"\""+job+"\" has been applied to the device known as "+"\""+device1+"\"')]"))
				.isDisplayed();
		String pass = "PASS >> Job Deployed on devices is displayed";
		String fail = "FAIL >> Job Deployed on devices isn't displayed";
		ALib.AssertTrueMethod(ExpectedStatus, pass, fail);
		sleep(2);
		boolean ExpectedStatus1 = Initialization.driver
				.findElement(By.xpath("//p[contains(text(),'The job named "+"\""+job+"\" has been applied to the device known as "+"\""+device2+"\"')]"))
				.isDisplayed();
		String pass1 = "PASS >> Job Deployed on devices is displayed";
		String fail1 = "FAIL >> Job Deployed on devices isn't displayed";
		ALib.AssertTrueMethod(ExpectedStatus1, pass1, fail1);
		sleep(2);

	}
	public void ReadingConsoleMessageForJobDeployment(String JobName,String DeviceName) throws InterruptedException
	{
		waitForLogXpathPresent("//p[contains(text(),'The job named "+"\""+JobName+"\" was successfully deployed on the device named "+"\""+DeviceName+"\"')]");
		Reporter.log("Job applied Message Is Displayed In The Console",true);
		sleep(5);
	}
	public void ApplyJobtoGroupBtn() throws InterruptedException {
		Initialization.driver.findElement(By.id("ApplyJobtoGroup")).click();
		sleep(2);
	}

	@FindBy(id = "mailDeleteButton")
	private WebElement deleteButton;

	public void ClickOnDeleteButton() throws InterruptedException {
		if (Initialization.driver.findElement(By.xpath("//div[@id='inbox-actionBar']//i[@class='icn fa fa-list']"))
				.isDisplayed()) {
			Initialization.driver.findElement(By.xpath("//div[@id='inbox-actionBar']//i[@class='icn fa fa-list']"))
					.click();
			sleep(2);
			deleteButton.click();
			sleep(2);
		} else {
			deleteButton.click();
			sleep(2);
		}

	}

	// DELETE Existing Roles
	@FindBy(xpath = "//div[@class='sc-deleteTemplate-btn actionbutton ct-menuBtn']//span[text()='Delete']")
	private WebElement DeleteRole;

	public void DeleteExistingRolesInUM() {

		DeleteRole.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
		waitForXpathPresent("//span[contains(text(),'Roles deleted successfully.')]");
		Reporter.log("'Roles deleted successfully.'", true);

	}

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesDelete;

	@FindBy(xpath = "//*[@id='featPerm_tabCont']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement RolesSearchTextBox;

	public void SearchRole(String InputRole) throws InterruptedException {

		RolesSearchTextBox.clear();
		sleep(2);
		RolesSearchTextBox.sendKeys(InputRole);
		sleep(2);
	}

	public void DeletingExistingRole(String InputRole) throws InterruptedException {
		SearchRole(InputRole);
		try {
			Initialization.driver
					.findElement(
							By.xpath("//*[@id='PermissionsRolesGrid']/tbody/tr/td[2]/p[text()='" + InputRole + "']"))
					.isDisplayed();
			Reporter.log("Role Have been Created Already", true);
			Initialization.driver.findElement(By.xpath("//*[@id='PermissionsRolesGrid']/tbody/tr/td[2]/p[text()='" + InputRole + "']"))
					.click();

			DeleteExistingRolesInUM();
		}

		catch (Exception e) {
			Reporter.log("No Role Is Found With Searched Name", true);
		}
	}

	@FindBy(xpath = "//*[@id='selectAllPermissions']")
	private WebElement selectAllRolesPermissions;

	@FindBy(xpath = "//div[@class='sc-addTemplate-btn actionbutton ct-menuBtn']//i[@class='icn smdm_ct_icn ct-add-icn']")
	private WebElement clickOnAddButtonUserManagement;

	@FindBy(xpath = ".//*[@id='templateName']")
	private WebElement NameTextField;

	@FindBy(xpath = ".//*[@id='TemplateDescription']")
	private WebElement descriptionTestField;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[14]")
	private WebElement ScrollDownToElement;

	@FindBy(xpath = ".//*[@id='SavePermissions']")
	private WebElement saveButton;

	@FindBy(xpath = "//span[text()='Roles created successfully.']")
	private WebElement RoleCreatedSuccessfully;

	public void RoleWithAllPermissions(String RoleName, String RoleDescriptionName) throws InterruptedException {
		// clickOnRolesOptions.click();
		// sleep(2)
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);

		descriptionTestField.sendKeys(RoleDescriptionName);
		sleep(2);

		selectAllRolesPermissions.click();
		sleep(2);

		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Roles created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);
	}
	public void NotificationOnProfileDefualt(String profile) throws InterruptedException {
		boolean flag = Initialization.driver.findElement(By.xpath("//span[text()='Changed default profile.']")).isDisplayed();
		String PassStatement = "PASS >> Notification 'Changed default profile.'is displayed";
		String FailStatement = "FAIL >> Profile set as defualt Notification not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	
		sleep(2);
	}
	public void VerifyOfDefaultProfile(String profile)
	{
		try
		{
			String a = Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td/div/p[text()='"+profile+"']/following-sibling::div/ul/li[text()='Default']")).getText();
			System.out.println(a);
			Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td/div/p[text()='"+profile+"']/following-sibling::div/ul/li[text()='Default']")).isDisplayed();
			Reporter.log("PASS >> Profile has been changed to default profile successfully.",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Profile didn't changed to default profile successfully.");
		}
	}
	public void VerifyOfSearchOptionWithSingleChar(String profile)
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='policyTable']/tbody/tr/td[2]"));
		boolean flag;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			String ActualValue1 = ActualValue.toLowerCase();
			if (ActualValue1.contains(profile)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStmt = "PASS >> Results are displayed according to the entered text.";
			String FailStmt = "Fail >> Results are not displayed according to the entered text.";
			ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
		}
	}
	@FindBy(xpath = "//button[@id='okbtn']")
	private WebElement okBtnOnFileTransferJobWindow;
	
	public void SelectFolderInMoveTo(String folder) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("(//*[@id='tree']/ul/li[text()='"+folder+"'])[last()]")).click();
		sleep(2);
		okBtnOnFileTransferJobWindow.click();
		sleep(3);
		waitForXpathPresent("//span[contains(text(),'Moved to folder successfully')]");
	}
	public void VerifyOfMoveJobToFolderPopUp(String job,String folder) throws InterruptedException
	{	
	try {
		Initialization.driver.findElement(By.xpath("//span[contains(text(),'Moved to folder successfully.')]")).isDisplayed();
		
		Reporter.log("PASS >>Moved to folder successfully PopUp is displayed.",true);
		sleep(3);
		}catch (Exception e) {
		ALib.AssertFailMethod("Fail >>  Moved to folder successfully PopUp isn't displayed.");
		sleep(3);
		}
	}
	@FindBy(xpath="//span[contains(text(),'Initiated ')]")
	private WebElement InitiateJob;
	public void JobInitiatedOnDevice() throws InterruptedException
	{

		if(InitiateJob.isDisplayed()) {
			Reporter.log("PASS >> Job is initiated message is displayed",true);
		}else {
			ALib.AssertFailMethod("FAIL >> Install job is initiated message is not displayed" );
		}
		sleep(3);

	}
	
	@FindBy(xpath="//a[@id='locationTrackingEditBtn']/preceding-sibling::span")
	private WebElement DeviceINfoLocationTrackingStatus;

	@FindBy(xpath="//a[@id='locationTrackingEditBtn']")
	private WebElement EditLocationButton;

	@FindBy(xpath="//button[@onclick='locationTrackingOkClick()']")
	private WebElement ApplyLocationTracking;

	@FindBy(xpath="//input[@id='isLocationTrackingOn']")
	private WebElement LocationSwitch;
	
	public void CheckingLocationTrackingStatus() throws InterruptedException
	{
		if(DeviceINfoLocationTrackingStatus.getText().equals("OFF"))
		{
			Reporter.log("Location Tracking Is OFF ",true);
			sleep(2);
		}
		else
		{	
			sleep(2);
			EditLocationButton.click();
			waitForidPresent("isLocationTrackingOn");
			sleep(3);
			LocationSwitch.click();
			ApplyLocationTracking.click();
			sleep(4);
			//waitForXpathPresent("//span[text()='Location tracking of "+Config.DeviceName+" turned ON.']");
			Reporter.log("Location Tacking Updated Message Displayed Successfully",true);
			
		}
	}
	@FindBy(xpath="//span[text()='Device(s) blacklisted.']")
	private WebElement deviceBlacklistedSuccessfulMessage;
	
	@FindBy(xpath=".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement ClickOnYesButtonBlacklistWarningDialog;
	
	public void ClickOnYesButtonWarningDialogBlacklist() throws InterruptedException
	 {
		 ClickOnYesButtonBlacklistWarningDialog.click();
		 sleep(2);
		 boolean isdisplayed =true;
		 
		 try{
			 deviceBlacklistedSuccessfulMessage.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Successfull Message displayed");
	   Reporter.log("PASS >> 'Device(s) blacklisted.' is received",true );	
	   
	   sleep(7);
	   
	 }
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesButton_WarningDialogWhitelistDevice;
	
	@FindBy(xpath="//span[text()='Device(s) whitelisted successfully']")
	private WebElement deviceWhitelistSuccessfulMessage;
	
	 public void ClickOnYesButtonWarningDialogWhitelist() throws InterruptedException
	 {
		 YesButton_WarningDialogWhitelistDevice.click();
		 sleep(1);
		 boolean isdisplayed =true;
		 
		 try{
			 deviceWhitelistSuccessfulMessage.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Successfull Message displayed");
	   Reporter.log("PASS >> 'Device(s) whitelisted successfully' is received",true );	
	   waitForPageToLoad();
	   sleep(2);
	   Initialization.driver.findElement(By.xpath("//button[@class='closeDeviceEnrollPop']")).click();
	   sleep(2);
	 }
	 @FindBy(id="deleteGroup")
	 private WebElement DeletTag;
	 
	 @FindBy(xpath=".//*[@id='deletegroupbtn']/a")
	 private WebElement DeleteTagYes;
	 
	 public void DeleteTag(String tag) throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='"+tag+"']")).click();
			sleep(4);
			DeletTag.click();
			waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
			sleep(2);
			DeleteTagYes.click();
			sleep(2);
			waitForXpathPresent("//span[contains(text(),'Deleted tag successfully.')]");
			try {
				Initialization.driver.findElement(By.xpath("//span[contains(text(),'Deleted tag successfully.')]")).isDisplayed();
				Reporter.log("PASS >> Deleted tag successfully. is  displayed",true);
			}catch (Exception e) {
				ALib.AssertFailMethod("Deleted tag successfully. is not displayed");
			}
		}
	 public void VerifyOnDeletedTag(String tag)
	 {
		 boolean TagFound=false;
		 try {
			 Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='"+tag+"']")).isDisplayed();
			 TagFound=true;
		 }catch (Exception e) {
			Assert.assertFalse(false, "Tag isn't present......");
			Reporter.log("Tag isn't present......",true);
		}
	 }
	 public void clickOkBtnOnAndroidVRInstallJob() throws InterruptedException{
			Initialization.driver.findElement(By.xpath("//button[@id='okbtn']")).click();
			sleep(2);
		}
	 @FindBy(xpath="//p[contains(text(),'Selected "+Config.DeviceName1+" will be deleted from SureMDM and you can no longer connect to device unless you have physical access to the device.Do you wish to continue?')]")
		private WebElement WarningMessageOnDeviceDelete;
		
		public void WarningMessageOnDeviceDelete()
		{
			try {
				 WarningMessageOnDeviceDelete.isDisplayed();
				 Reporter.log("PASS >> Warning message on device delete from right click is displayed",true);
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Warning message on device delete from right click is Not Displayed");
			}
		}
		
		@FindBy(xpath = "(//input[@placeholder='Search'])[last()-2]")
		private WebElement SearchJobInJobList;
		
		public void SearchJobInList(String JobName) throws InterruptedException {
			SearchJobInJobList.clear();
			sleep(2);
			SearchJobInJobList.sendKeys(JobName);
			waitForidPresent("job_new_job");
			sleep(5);
			Initialization.driver.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr/td[1]")).click();
			sleep(2);
		}
		public void ValidatingCreatedRole(String InputRole)
		{
			try {
				Initialization.driver
				.findElement(
						By.xpath("//*[@id='PermissionsRolesGrid']/tbody/tr/td[2]/p[text()='" + InputRole + "']"))
				.isDisplayed();
				Reporter.log("PASS >> Role is created successfully and displayed in Roles list",true);
			}catch (Exception e) {
				Reporter.log("PASS >> Role isn't created successfully and didn't displayed in Roles list",true);
			}
		}
		Actions action=new Actions(Initialization.driver);
		public void SelectMultipleDevicesToWhitelist() throws InterruptedException
		 {
			 WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='blacklistGrid']/tbody/tr[1]/td[2]"));
		 	 WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='blacklistGrid']/tbody/tr[2]/td[2]"));
		 	 action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
		 	 sleep(2);
		 }
		public void ReadingConsoleMessageForJob_Deployment(String JobName,String DeviceName) throws InterruptedException
		{
			waitForLogXpathPresent("//p[contains(text(),'has applied a job named "+"\""+JobName+"\" to the device known as "+"\""+DeviceName+"\"')]");
			Reporter.log("Job applied Message Is Displayed In The Console",true);
			sleep(5);
		}

		
}
