package PageObjectRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;


public class CommonMethods_POM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
   JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
	
	@FindBy(id="userProfileButton")
	private WebElement settings;
	
	@FindBy(xpath="//button[@id='advanceSettings']")
	private WebElement AccountSettings;
	
	
	@FindBy(xpath="//a[text()='Home']")
	private WebElement Home;
	
	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;
	
	@FindBy(xpath="//p[text()='"+Config.DeviceName+"']")
	private WebElement mydevice;
	
	@FindBy(xpath="//input[@id='useadvgrid']")
	private WebElement MongoDBCheckBox;
	
	public void ConfirmationMessageVerify(WebElement ConfirmationMessage,boolean value,String PassStatement,String FailStatement,int time) throws InterruptedException{
		int i=5;
	while(i>=5)
		{
		try {
			ConfirmationMessage.isDisplayed();
			ConfirmationMessage.click();
			i=0;
			value = true;
		}catch (Exception e) {
			value = false;
			i++;
			if(i==3500)
			{
			i=0;
			}
		}
		}
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(time);
	}
	
	public void ClickOnSettings() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, settings);
		sleep(4);
		settings.click();
		waitForidPresent("generateQRCode");
		Reporter.log("PASS >> Click on Settings", true);
		sleep(5);
	}
	
	public void ClosingSettings() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, settings);
		sleep(4);
		settings.click();
		waitForidPresent("deleteDeviceBtn");
	    sleep(2);
	}
	public void ClickonAccsettings() throws InterruptedException
	{
		AccountSettings.click();
		waitForVisibilityOf("//span[text()='Account Settings']");
		sleep(5);
		
	}
	public void VerifyAccSettingpage()
	{
		boolean flag=Initialization.driver.findElement(By.xpath("//span[text()='Account Settings']")).isDisplayed();
		String Pass="Account Settings Page is displayed";
		String fail="Account Settings page is not displayed";
		ALib.AssertTrueMethod(flag,Pass, fail);
		
	}
	public void EnablingMongoDB()
	{
		if(MongoDBCheckBox.isSelected())
		{
			Reporter.log("MongoDB Is Enabled Already",true);
		}
		else
		{
			MongoDBCheckBox.click();
			Reporter.log("MongoDB Is Enabled",true);
		}
	}
	
	public void SearchLinuxDevice() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.DeviceNameLinux);
		waitForXpathPresent("//p[contains(text(),'"+Config.DeviceNameLinux+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.DeviceNameLinux+"')]");
		sleep(5);
	}
	
//	public void SearchDeviceKnox() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
//	{
//		SearchDeviceTextField.sendKeys(Config.KnoxDeviceName);
//		waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName+"']");
//		waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName+"']");
//		sleep(15);
//	}
	
	public void SearchWindowsDeviceName() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.WindowsDeviceName);
		waitForXpathPresent("//p[contains(text(),'"+Config.WindowsDeviceName+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.WindowsDeviceName+"')]");
		sleep(5);
	}
	
	public void SearchIOSDevice() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.iOS_DeviceName);
		waitForXpathPresent("//p[contains(text(),'"+Config.iOS_DeviceName+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.iOS_DeviceName+"')]");
		sleep(5);
	}
	public void SearchDualEnrollmentDeviceModel() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.DualEnrollmentDeviceModel);
		waitForXpathPresent("//p[contains(text(),'"+Config.DualEnrollmentDeviceModel+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.DualEnrollmentDeviceModel+"')]");
		sleep(5);
	}
	public void SearchDevice_WindowsEMM(String EMM) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.Windows_DeviceNameEMM);
		waitForXpathPresent("//p[text()='"+Config.Windows_DeviceNameEMM+"']");
		waitForXpathPresent("//p[text()='"+Config.Windows_DeviceNameEMM+"']");
		sleep(15);
	}
	public void SearchWindowsMobileDeviceName(String WindowsMobile) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.WindowsMobileDeviceName);
		waitForXpathPresent("//p[contains(text(),'"+Config.WindowsMobileDeviceName+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.WindowsMobileDeviceName+"')]");
		sleep(5);
	}
	
	public void SearchDeviceFOTA(String FotaDevcieName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys("hello");
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(FotaDevcieName);
		waitForXpathPresent("//p[text()='"+Config.DeviceNameFOTA+"']");
		waitForXpathPresent("//p[text()='"+Config.DeviceNameFOTA+"']");
		sleep(30);
	}
	
	public void SearchDualEnrollmentDeviceName(String DualWinDeviceName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(DualWinDeviceName);
		waitForXpathPresent("//p[contains(text(),'"+Config.DualEnrollmentDeviceName+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.DualEnrollmentDeviceName+"')]");
		sleep(5);
	}
@FindBy(xpath="//div[@id='main_div_id']/div[3]/div[4]/div[1]/div[1]/button")
private WebElement gridrefreshbutton;
	
 public void clickOnGridrefreshbutton() throws InterruptedException {
	 gridrefreshbutton.click();
	 waitForXpathPresent("//table[@id='dataGrid']/tbody/tr");
	 sleep(4);
	 Reporter.log("Clicked on grid refreshbutton", true);
 }
public void SearchDeviceKnox1() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.KnoxDeviceName1);
		waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName1+"']");
		waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName1+"']");
		sleep(15);
	}

public void SearchVRDevice(String VRname) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys("hello");
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(Config.DeviceNameVR);
		waitForXpathPresent("//p[text()='"+Config.DeviceNameVR+"']");
		waitForXpathPresent("//p[text()='"+Config.DeviceNameVR+"']");
		sleep(30);
	}
	
public void SearchMacOSDevice(String MAC) throws InterruptedException 
	{

		SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
		waitForXpathPresent("//p[contains(text(),'"+Config.Macos_DeviceName+"')]");
		waitForXpathPresent("//p[contains(text(),'"+Config.Macos_DeviceName+"')]");
		sleep(10);
	}
	
public void SearchDeviceDataLogicDevice(String DataLogic) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys("hello");
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(Config.DeviceNameDataLogic);
		waitForXpathPresent("//p[text()='"+Config.DeviceNameDataLogic+"']");
		waitForXpathPresent("//p[text()='"+Config.DeviceNameDataLogic+"']");
		sleep(20);
	}
public void SearchParentDevice(String ParentDevice) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
{
	SearchDeviceTextField.sendKeys("hello");
	SearchDeviceTextField.clear();
	SearchDeviceTextField.sendKeys(Config.ParentDeviceName);
	waitForXpathPresent("//p[text()='"+Config.ParentDeviceName+"']");
	waitForXpathPresent("//p[text()='"+Config.ParentDeviceName+"']");
	sleep(20);
}

	
public void SearchDevicePrinterDevice(String Printer) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys("hello");
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(Config.DeviceNamePrinter);
		waitForXpathPresent("//p[text()='"+Config.DeviceNamePrinter+"']");
		waitForXpathPresent("//p[text()='"+Config.DeviceNamePrinter+"']");
		sleep(20);
	}
	public void DisablingMongoDB()
	{
		if(MongoDBCheckBox.isSelected())
		{
			MongoDBCheckBox.click();
			Reporter.log("MongoDB Is Disabled",true);
		}
		else
		{
			
			Reporter.log("MongoDB Is Already Disabled",true);
		}
	}
	
	@FindBy(xpath="//div[@id='searchOptions_cont']/../div[3]/input")
	private WebElement SearchDevice;
	public void SearchDeviceInconsole() throws InterruptedException {
		SearchDevice.clear();
		SearchDevice.sendKeys(Config.DeviceName);
		sleep(10);
	}
	
	@FindBy(xpath = "//li[@id='jobSection']")
	private WebElement jobtab;

	public void clickonjobtab() throws InterruptedException {
		jobtab.click();
		waitForidPresent("job_new_job");
		sleep(2);
		Reporter.log("Clickig on Job section", true);
	}
	
	@FindBy(xpath="//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")
	private WebElement pendingdeleteDeviceSearch;
	public void SearchPendingDeleteDeviceInput(String DeviceName) throws InterruptedException
	{
 		pendingdeleteDeviceSearch.clear();
 		pendingdeleteDeviceSearch.sendKeys(DeviceName);
		try
		{
		waitForXpathPresent("//p[text()='"+DeviceName+"']");
		//sleep(10);
		}
		catch(Exception e)
		{
			Reporter.log(" Device Is Not Present In Console",true);
		}
		
	}
	
	
	public void ClickOnHomePage() throws InterruptedException
	{   sleep(6);
		Home.click();   
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
		Reporter.log("PASS >> Clicked On Home",true);
	}
	
	@FindBy(xpath = "//div[@id='job_delete']")
	private WebElement deletetab;

	public void clickingondeletejob() throws InterruptedException {
		deletetab.click();
		waitForXpathPresent("(//p[contains(text(),'Are you sure you want to delete')])[1]");
		sleep(2);
	}


@FindBy(xpath = "//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]")
	private WebElement JobDeleteYesButton;

	public void clickingonYesbuttonDeletejob() throws InterruptedException {
		JobDeleteYesButton.click();
		waitForXpathPresent("//p[contains(text(),'Unable to delete job')]");
		sleep(2);
	}


@FindBy(xpath = "//div[contains(text(), 'Please remove')]")
	private WebElement secondlinemessage;

	public void verifyerrormessageWhileDeletingJob() {
		boolean flag = secondlinemessage.isDisplayed();

	}
	public void Selectjobpageholderjob(String job) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id=\"jobDataGrid\"]/tbody/tr/td[2]/p[text()='" + job + "']"))
				.click();
		sleep(4);
	}

	@FindBy(linkText="Login again")
	private WebElement LoginAgain;

	public void ClickOnLoginAgainLink() throws InterruptedException {
	LoginAgain.click();
	System.out.println("Clicked on Login again link");
	sleep(10);
	}
	@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
		private WebElement Searchjobpageholder;

		public void jobpageholder(String name) throws InterruptedException {
			Searchjobpageholder.sendKeys(name);
			sleep(15);
		}
	@FindBy(xpath = "//div[@id='informationDialog']/div[1]/div[1]/div[2]/button")
	private WebElement informationOkbutton;

	public void clickingoninformationOkbuttonDeletejob() throws InterruptedException {
		informationOkbutton.click();
		sleep(2);
	}
	public void ClickOnMDMTrailAccountWarning() throws InterruptedException
	{
		
		if(Initialization.driver.findElement(By.xpath("//p[@class='messLine']")).isDisplayed())
		{
			Initialization.driver.findElement(By.xpath("//li[@id='trialMessage']")).click();
			sleep(5);
		}
	}
		
	public void SearchDevice(String DeviceName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.clear();
		sleep(3);
		SearchDeviceTextField.sendKeys(DeviceName);
		//waitForXpathPresent("//p[text()='"+DeviceName+"']");
		sleep(3);
	}
	
	public void SearchDeviceMultipleDevices() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.MultipleDeviceName);
		sleep(15);
	}
	
	@FindBy(id="refreshButton")
	private WebElement DynamicRefresh;
	public void ClickOnDynamicRefresh() throws InterruptedException
	{
		DynamicRefresh.click();
		sleep(30);
	}
	
	public void ClickOnAllDevicesButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("all_ava_devices")).click();
		sleep(3);
	}
	public void SearchDevice_Enrollment(String DeviceName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		
		boolean flag;
		SearchDeviceTextField.sendKeys("Hello");
		SearchDeviceTextField.clear();
		sleep(3);
		SearchDeviceTextField.sendKeys(DeviceName);
		waitForXpathPresent("//p[contains(text(),'"+DeviceName+"')]");
		sleep(6);
		if(Initialization.driver.findElement(By.xpath("//p[contains(text(),'"+DeviceName+"')]")).isDisplayed())
		{
			flag=true;
		}
		else
		
			flag=false;
		String Pass="Enrolled Device Is Present in The Console";
		String Fail="Enrolled Device Is Not Present in The Console";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}
	
	public void ClearSearchDeviceWithEnter() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.clear();
		sleep(2);
		SearchDeviceTextField.sendKeys(Keys.ENTER);
		sleep(15);
		
	}
	
	public void SearchForMultipleDevice(String Device1,String Device2) throws InterruptedException 
	{
		SearchDeviceTextField.sendKeys("Hello");
		SearchDeviceTextField.clear();
		sleep(3);
		SearchDeviceTextField.sendKeys(Device1+","+Device2);
		//waitForXpathPresent("//p[text()='"+Device1+"']");
		//waitForXpathPresent("//p[text()='"+Device2+"']");
		sleep(6);
	}
		
	public void Selectdevice() throws InterruptedException {
		mydevice.click();
		sleep(3);
	}
	public void ReadfromFile(String FilePath,WebElement EditXMLTextArea) throws Throwable
	{
		 FileReader fr=new FileReader(FilePath);
		 BufferedReader br= new BufferedReader(fr);
		 String x="";
		 while ((x=br.readLine()) != null)
		 {
			 EditXMLTextArea.sendKeys(x+"\n");
		 }
		 br.close();
	}
	
	public void SearchDevice_Approve() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.Unapprove_ApproveDeviceName);
		waitForXpathPresent("//p[text()='"+Config.Unapprove_ApproveDeviceName+"']");
		waitForXpathPresent("//p[text()='"+Config.Unapprove_ApproveDeviceName+"']");
		sleep(15);
	}
	
	public void refreshpage_remote() throws InterruptedException
	{
		refesh();
		Initialization.remotesupportpage.VerifyOfRemoteSupportLaunched();
	}
	
	public void SearchDevice_Delete() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys(Config.Unapprove_DeleteDeviceName);
		sleep(5);
	}
	
	public void SearchDevice_Unknown() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.sendKeys("ffffffff");
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}
	
	public void SearchBlackListedDevice() throws InterruptedException
	{
		SearchDeviceTextField.clear();
		sleep(4);
		boolean flag;
		SearchDeviceTextField.sendKeys("Hello");
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(Config.DeviceName);
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//span[text()='No matching result found.']")).isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="Device Is Not Present In Console";
		String Fail="Deivce Is Present In Console";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClearSearchDevice() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		SearchDeviceTextField.clear();
		sleep(5);
		
	}
	
	public void SearchDevice() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{  sleep(10);
		SearchDeviceTextField.clear();
		sleep(10);
		SearchDeviceTextField.sendKeys(Config.DeviceName);
		sleep(20);
	}
	
	public void HardWait(int wait) throws IOException, InterruptedException
	{
		Reporter.log("Waiting...",true);
		sleep(wait);
		Reporter.log("Wait over..!!",true);
	}
	
	public void BasicSearch(String searchProperty) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{sleep(10);
		SearchDeviceTextField.clear();
		sleep(10);
		SearchDeviceTextField.sendKeys(searchProperty);
	}
	
	public void loginAgain() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//a[text()='Login again']")).click();
		sleep(10);
	}
	public void ConfirmationMessageVerifyFalse(WebElement ConfirmationMessage,boolean value,String PassStatement,String FailStatement,int time) throws InterruptedException{
		int i=5;
		sleep(time);
		while(i>=5)
		{
		try {
			ConfirmationMessage.isDisplayed();
			ConfirmationMessage.click();
			i=0;
			value = true;
		}catch (Exception e) {
			value = false;
			i++;
			if(i==300)
			{
			i=0;
			}
		}
		}
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
}
	
	public void refreshpage() throws InterruptedException
	{
		refesh();
		waitForidPresent("deleteDeviceBtn");
		sleep(7);
		if(Initialization.driver.findElement(By.xpath("(//p[@class='messLine'])[1]")).isDisplayed())
		{
		Initialization.driver.findElement(By.xpath("//a[text()='Dashboard']")).click();
		waitForidPresent("osplatforms");
		Reporter.log("Clicking On DashBoard, Navigated to DashBoard Page", true);
		sleep(5);
		Reporter.log("PASS >> Verification of clicking on Dashboard", true);
		Initialization.driver.findElement(By.xpath("//a[text()='Home']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
		}
	}
	@FindBy(id="applyJobButton")
	private WebElement ApplyButton;
	
	public void ClickOnApplyButton() throws InterruptedException
	{
		sleep(2);
		ApplyButton.click();
		waitForidPresent("scheduleJob");
		sleep(5);
		Reporter.log("PASS >> Clicked On Apply Button",true);
	}
	
	
	@FindBy(id="applyJob_okbtn")
	private WebElement ApplyButtonJobWindow;

	public void ClickOnApplyButtonApplyJobWindow() throws InterruptedException
	{
		
			ApplyButtonJobWindow.click();
			sleep(4);
	}
	public void WaitforJobDeployment() throws InterruptedException
	{
		sleep(40);
	}
	
	public void ClickOnrefreshButtonDeviceInfo() throws InterruptedException {
		WebDriverWait wait=new WebDriverWait(Initialization.driver,300);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("refreshButton")));
		refreshdeviceinfo.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='lds-ring1'])[1]")));
		while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {
		sleep(3);
		}
	}
	@FindBy(id="refreshButton")
	private WebElement refreshdeviceinfo;
		
	@FindBy(xpath="//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextBoxApplyJob;
	
	@FindBy(xpath="(//div[text()='Loading...'])[2]")
	private WebElement Loader;
		
	public void SearchJob(String name) throws InterruptedException
	{				
		SearchTextBoxApplyJob.sendKeys(name);
		waitForXpathPresent("//p[text()='"+name+"']");
		sleep(15);		
	}
	
	public void Selectjob(String job) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//p[text()='"+job+"']")).click();
		sleep(5);
	}
	
	 @FindBy(xpath = "//*[@id=\"conmptableContainer\"]/div[1]/div[1]/div[2]/input")
		private WebElement SearchTextBoxSelectJobToAdd;
	 
	 
	public void SearchJobToAddInCustomizeToolbar(String name) throws InterruptedException {
		SearchTextBoxSelectJobToAdd.sendKeys(name);
		sleep(8);
		Initialization.driver.findElement(By.xpath("//*[@id=\"conmptableContainer\"]/div[1]/div[1]/div[1]/button"))
				.click();
		waitForXpathPresent("//p[text()='" + name + "']");
		System.out.println("Job is searched");
		sleep(15);
	}

	@FindBy(xpath = "//div[@id='selJob_addList_modal']/div[3]/button")
	private WebElement OkButtonInJob;

	public void SelectjobOkButton() throws InterruptedException {
		OkButtonInJob.click();
		waitForXpathPresent("//input[@id='customjobname']");
		sleep(3);
	}	

	
	public void SelectGenerateQRCode() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//button[@id='generateQRCode']")).click();
		waitForidPresent("generateQRCodeTail");
		sleep(7);
		System.out.println("QR Code Has Been Displayed");
	}
	
	
	public void ClickOnMailBoxOfNix() throws InterruptedException
	{
	   Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Mailbox']").click();	
	   sleep(4);
	}
	
	 public void ClickOnGetStartedButtonCheck()
     {
    	 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Get Started' and index='1']").click();
     }
	 
		public void InstallApplicationsFromPlayStore(String AppPackageName, String ScrollToSubTitle) throws IOException, InterruptedException
		{
			AppiumConfigurationCommonMethod("com.android.vending","com.android.vending.AssetBrowserActivity");
			WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 60);
			WebDriverWait wait1=new WebDriverWait(Initialization.driverAppium, 300);
			Runtime.getRuntime().exec("adb shell am start -a android.intent.action.VIEW -d 'market://details?id="+AppPackageName+"'");
			 sleep(4);
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Install']")));
				try 
				{
					Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Install']").click();
					wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Uninstall']")));
					System.out.println("Download completed");
				}
				catch(Exception e) 
				{
					Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@resource-id, 'right_button') and @text='Install']").click();
					wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[contains(@resource-id, 'left_button') and @text='Uninstall']")));
					System.out.println("Download completed");
				}
			}
			catch(Exception e) {
				Reporter.log("App is already installed", true);
			}
		}

	 public void AppiumConfigurationCommonMethod(String appPackage,String activity) throws IOException, InterruptedException
		{
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "My Phone");
			caps.setCapability("platformName", "Android");
			caps.setCapability("appPackage",appPackage);
			caps.setCapability("appActivity",activity);
			caps.setCapability("noReset", "true");
			System.out.println("No Reset happened");
			caps.setCapability("adbExecTimeout",60000);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,10000);
			System.out.println("about to launch");
			try {
			   Initialization.driverAppium=new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			sleep(5);
		}
	 
	 public void AppiumConfigurationCommonMethodWithUDID(String appPackage,String activity,String UDID) throws IOException, InterruptedException
		{
		 
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "My Phone");
			caps.setCapability("platformName", "Android");
			caps.setCapability("appPackage",appPackage);
			caps.setCapability("appActivity",activity);
			caps.setCapability("udid",UDID);
			caps.setCapability("noReset", "true");
			System.out.println("No Reset happened");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,4000);
			System.out.println("about to launch");
			try {
			   Initialization.driverAppium=new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			sleep(5);
		}
	 
	 public void SearchDeviceModel(String DeviceModelName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
		{
			SearchDeviceTextField.sendKeys("hello");
			SearchDeviceTextField.clear();
			SearchDeviceTextField.sendKeys(Config.DeviceModel);
			waitForXpathPresent("//*[@id='dataGrid']/tbody/tr/td[3]/p[text()='"+Config.DeviceModel+"']")  ;                                          //("//p[text()='"+Config.DeviceModel+"']");
			waitForXpathPresent("//*[@id='dataGrid']/tbody/tr/td[3]/p[text()='"+Config.DeviceModel+"']")  ;                                          //("//p[text()='"+Config.DeviceModel+"']");
			sleep(30);
		}
	 public void UnlockingDeviceThroughPassword() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb shell input keyevent KEYCODE_WAKEUP");
			sleep(2);
			Runtime.getRuntime().exec("adb shell input keyevent 82");
			sleep(2);
			Runtime.getRuntime().exec("adb shell input text 0000");
			sleep(1);
			Runtime.getRuntime().exec("adb shell input keyevent 66");
		}
	 
	 public boolean commonScrollMethod(String NameText) throws InterruptedException {
			boolean Element=Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + NameText + "\").instance(0))")).isDisplayed();
			String pass = NameText+"PASS >> is displayed";
			String fail = NameText+" is not displayed";
			ALib.AssertTrueMethod(Element, pass, fail);
			return Element;
		}
	 
	 public void ClickOnAppiumText(String Text) throws InterruptedException
	 {
		 Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']").click();
		 sleep(4);
		 Reporter.log("Clicked On "+Text);
	 }
	 
	 public void ClickOnAppiumButtons(String Text) throws InterruptedException
	 {
		 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='"+Text+"']").click();
		 sleep(3);
		 Reporter.log("Clicked On "+Text+" Button");
	 }
	 //new Methods 
	 @FindBy(xpath="//a[normalize-space()='Login again']")
		private WebElement LoginAgainLink;
		 
		@FindBy(id="logoutButton")
		private WebElement LogoutBtn;
		

		 
		 public void ClickOnLoginAgain()
			{
				try {
					Initialization.driver.findElement(By.xpath("//a[normalize-space()='Login again']")).isDisplayed();
					LoginAgainLink.click();
					Reporter.log("Clicked On LoginAgain Link....",true);
				}catch (Exception e) {
					Reporter.log("LoginAgain Link is not displayed....",true);
				}
			}
			public void ClickOnLogout() throws InterruptedException
			{
				LogoutBtn.click();
				sleep(15);
				ClickOnLoginAgain();
			}
			public void SearchMultipleDevice(String devices) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.clear();
				SearchDeviceTextField.sendKeys(devices);
				sleep(10);
			}	
			public void clickOnInboxEditBtn() throws InterruptedException
			{
				Initialization.driverAppium.findElementByXPath("//android.widget.ImageButton[@index='2']").click();
				sleep(2);
				Reporter.log("Clicked on Edit Button in Inbox",true);
			}
			public void enterSubInComposeMessage(int n,String msg,String body) throws InterruptedException
			{
				for(int i=0;i<=n;i++) {
				Initialization.driverAppium.findElementByXPath("//android.widget.ImageButton[@index='2']").click();
				sleep(2);
				Reporter.log("Clicked on Edit Button in Inbox",true);	
				Initialization.driverAppium.findElementById("com.nix:id/editTextSub").sendKeys(msg);
				Reporter.log("Entered Subject",true);
				sleep(2);
				Initialization.driverAppium.findElementById("com.nix:id/editTextMsg").sendKeys(body);
				Reporter.log("Entered Body",true);
				Initialization.driverAppium.findElementById("com.nix:id/SendMsgButton").click();
				Reporter.log("Clicked On Send Button",true);
				sleep(2);
				}
			}
			public void clicOnSendBtnInMailBox() throws InterruptedException
			{
				Initialization.driverAppium.findElementById("com.nix:id/SendMsgButton").click();
				Reporter.log("Clicked On Send Button",true);
				sleep(2);
			}
			public void enterSubInComposeMessage(String msg,String body) throws InterruptedException
			{
				Initialization.driverAppium.findElementById("com.nix:id/editTextSub").sendKeys(msg);
				Reporter.log("Entered Subject",true);
				sleep(2);
				Initialization.driverAppium.findElementById("com.nix:id/editTextMsg").sendKeys(msg);
				Reporter.log("Entered Body",true);
			}
			

		public void ClickOnMailBoxNix()
		{
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Mailbox' and @index='2']").click();
		}

		public void VerifyIsDeviceLocked() throws InterruptedException
		{
			sleep(10);
			try
			{
			//	AppiumConfigurationforNIX();
				ClickOnMailBoxNix();
				ALib.AssertFailMethod("FAIL>>> Device IS Not Locked Even After Applying Lock Job");

			}
			catch (Exception e) 
			{
				Reporter.log("PASS>>> Device is locked ",true);
			} 
		}
		 public void SearchDeviceModel2() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.sendKeys("hello");
				SearchDeviceTextField.clear();
				SearchDeviceTextField.sendKeys(Config.DeviceModel2);
				waitForXpathPresent("//p[text()='"+Config.DeviceModel2+"']");
				waitForXpathPresent("//p[text()='"+Config.DeviceModel2+"']");
				sleep(30);
			}
		 
		 public void clickOnProceedIfAvailable() throws InterruptedException
		 {
			 sleep(10);
			 try {
				 Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Proceed']")).isDisplayed();
				 Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Proceed']")).click();
				 sleep(3);
			 }catch (Exception e) {
				Reporter.log("Proceed Option is not available",true);
			}
		 }
			public void clickOnActivateIfAvailable() throws InterruptedException
			 {
				 try {
					 Initialization.driverAppium.findElement(By.id("//android.widget.Button[@text='ACTIVATE']")).isDisplayed();
					 Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='ACTIVATE']").click();
					 sleep(1);
					 Reporter.log("ACTIVATE Option is available and clicked on ACTIVATE",true);
				 }catch (Exception e) {
					Reporter.log("ACTIVATE Option is not available",true);
				}
			 }
			@FindBy(xpath="//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
			private WebElement SearchJobField;
			@FindBy(xpath="//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[1]/button[@title='Refresh']")
			private WebElement Refresh;


			public void SearchJobInSearchField(String JobToBeSearched) throws InterruptedException
			{
				Refresh.click();
				sleep(2);
		        SearchJobField.clear();
				sleep(2);
				SearchJobField.sendKeys(JobToBeSearched);
				waitForXpathPresent("(//p[contains(text(),'"+JobToBeSearched+"')])[1]");
				sleep(3);
			}
			public void VerifyAlertRunscriptInJob(String JObName) throws InterruptedException {
				  
//				Refresh.click();
				sleep(2);


			if(Initialization.driver.findElement(By.xpath("//table[@id='jobDataGrid']/tbody/tr/td/p[contains(text(),'"+JObName+"')]")).isDisplayed())
				{
					Reporter.log("'"+JObName+"' is displayed",true);
					ClickOnHomePage();

				}
				else
				{
					Reporter.log("'"+JObName+"' is Not displayed",true);

					ClickOnHomePage();
				}
				}

			public void SearchLinuxDevice(String LinuxName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.sendKeys(Config.DeviceNameLinux);
				waitForXpathPresent("//p[contains(text(),'"+Config.DeviceNameLinux+"')]");
				waitForXpathPresent("//p[contains(text(),'"+Config.DeviceNameLinux+"')]");
				sleep(5);
			}
			
			public void SearchDeviceKnox() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.sendKeys(Config.KnoxDeviceName);
				waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName+"']");
				waitForXpathPresent("//p[text()='"+Config.KnoxDeviceName+"']");
				sleep(15);
			}
			
			public void SearchWindowsDeviceName(String WindowsName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.sendKeys(Config.WindowsDeviceName);
				waitForXpathPresent("//p[contains(text(),'"+Config.WindowsDeviceName+"')]");
				waitForXpathPresent("//p[contains(text(),'"+Config.WindowsDeviceName+"')]");
				sleep(5);
			}
			
			public void SearchIOSDevice(String IosName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			{
				SearchDeviceTextField.sendKeys(Config.iOS_DeviceName);
				waitForXpathPresent("//p[contains(text(),'"+Config.iOS_DeviceName+"')]");
				waitForXpathPresent("//p[contains(text(),'"+Config.iOS_DeviceName+"')]");
				sleep(5);
			}


	}


