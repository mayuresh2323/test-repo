package PageObjectRepository;

import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class FileStoreDevice_POM extends WebDriverCommonLib{
	 public static AppiumDriver<WebElement> driverAppium;
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();

		 
	public void LaunchFileStore() throws InterruptedException, IOException    
	{            
	System.out.println("\nTest Case 0 : Launching File Store");                 
	Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.efss.splashscreen.EFSSSplashScreen");// to launch FileStore on Device                       
	sleep(10);                  
	}          

   
	@FindBy(xpath="//android.widget.TextView[@text='Image.jpg']")
	private WebElement image;
      
     
    public void  ClickOnIMG() {
    	
    	image.click();
    //Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Image.jpg']").click();
    }
    
  

}

