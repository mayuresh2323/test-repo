package PageObjectRepository;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class AppStore_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	public static String Expected =null;
	public static String AppTitle=null;
	public static String ApkPackage=null;
	public static String ApkVersion=null;
	public static String ApkVersionCode=null;
	public static String ApkCategory=null;
	public static String ApkDescription=null;
	public static String ApkLink=null;

	public static String EditAppTitle=null;
	public static String EditApkPackage=null;
	public static String EditApkVersion=null;
	public static String EditApkVersionCode=null;
	public static String EditApkCategory=null;
	public static String EditApkDescription=null;

	public static String WebAppTitle=null;
	public static String WebAppLink=null;
	public static String WebAppCategory=null;
	public static String WebAppDescription=null;

	public static String EditWebAppTitle=null;
	public static String EditWebAppLink=null;
	public static String EditWebAppCategory=null;
	public static String EditWebAppDescription=null;

	@FindBy(id="mamSection")
	private WebElement AppStore;

	@FindBy(id="andriodEAM")
	private WebElement AndroidAppStore;

	@FindBy(id="addNewAppBtn")
	private WebElement AddApp;
	
	@FindBy(id="//*[@id='UploadWebAppPopup']/div/div/div[1]/button")
	private WebElement CloseWebAppPopup;
	

	@FindBy(xpath="//*[@id='AddNewAppPopup']/div/div/div[1]/h4")
	private WebElement AddNewPopupAndroidHeader;

	@FindBy(xpath="//div[@id='AddNewAppPopup']/div/div/div/div/p[@class='titleNewAppOptions']")
	private WebElement AddNewPopupAndroidSection;

	@FindBy(id="UploadApkMam")
	private WebElement UploadAPKAndroid;

	@FindBy(xpath="//div[@id='dropZone']/p")
	private WebElement UploadAPKAndroidSection;

	@FindBy(xpath="//div[@id='dropZone']/button")
	private WebElement UploadAPKAndroidBrowseFile;

	@FindBy(xpath="//input[@id='uploadAPKinputMam']/following-sibling::div/div/div/div/div[text()='Uploading']")
	private WebElement UploadAPKAndroidUploading;

	@FindBy(xpath="//input[@id='uploadAPKinputMam']/following-sibling::div/div/div/div/span/span")
	private WebElement UploadAPKAndroidPercentage;

	@FindBy(xpath="//input[@id='uploadAPKinputMam']/following-sibling::div/div/div/div/div[text()='Extracting App Details']")
	private WebElement UploadAPKAndroidExtracting;

	@FindBy(xpath="//div[@id='UploadAPKPopup']/div/div/div/h4")
	private WebElement UploadAPKHeader;

	@FindBy(xpath="//span[text()='Please fill the required fields.']")
	private WebElement ErrorMessageToFillDetails;

	@FindBy(id="uploadApkPopupBack")
	private WebElement UploadApkPopupBackButton;

	@FindBy(id="uploadApkPopupAdd")
	private WebElement UploadApkPopupAddButton;

	@FindBy(id="appTitleMam")
	private WebElement AppTitleAndroid;

	@FindBy(id="apkPackageIdMam")
	private WebElement AppPackageAndroid;

	@FindBy(id="apkVersionMam")
	private WebElement AppVersionAndroid;

	@FindBy(id="apkVersionCodeMam")
	private WebElement AppVersionCodeAndroid;

	@FindBy(id="apkCategoryMam")
	private WebElement AppCategoryAndroid;

	@FindBy(id="apkDescriptionMam")
	private WebElement AppDescriptionAndroid;

	@FindBy(id="UploadApkLinkMam")
	private WebElement APKLinkAndroid;

	@FindBy(id="UploadWebAppMam")
	private WebElement WebAppAndroid;

	@FindBy(id="editBtn")
	private WebElement AppDetailsEditButton;

	@FindBy(xpath="//div[@id='AddNewAppPopup']/div/div/div/button[@class='close']")
	private WebElement CloseAddNewAppPopupAndroid;

	@FindBy(xpath="//div[@id='UploadAPKPopup']/div/div/div/button[@class='close']")
	private WebElement CloseUploadAPKPopupAndroid;

	@FindBy(id="AppDetailsPopUp_AppVersion")
	private WebElement AppVersion;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/button[@class='close']")
	private WebElement CloseEditAppDetails;

	@FindBy(xpath="//div[@id='editAppLinkPopup']/div/div/div/button[@class='close']")
	private WebElement CloseEditAppLinkDetails;

	@FindBy(xpath="//div[@id='EditWebAppPopup']/div/div/div/button[@class='close']")
	private WebElement CloseEditWebAppDetails;

	@FindBy(id="app_title")
	private WebElement AppDetailsAppTitleAndroid;

	@FindBy(id="app_packageid")
	private WebElement AppDetailsPackageAndroid;

	@FindBy(id="app_version")
	private WebElement AppDetailsVersionAndroid;

	@FindBy(id="app_cateogory")
	private WebElement AppDetailsCategoryAndroid;

	@FindBy(id="app_description")
	private WebElement AppDetailsDescriptionAndroid;

	@FindBy(xpath="//div[@id='appDet_modal']/div[1]/h4")
	private WebElement AppDetailsHeader;

	@FindBy(xpath="//div[@id='iOSUploadAppPopup']//button[@aria-label='Close']")
	private WebElement AppDetailsClose;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div[1]/h4")
	private WebElement EditAppDetailsHeader;

	@FindBy(xpath="//div[@id='EditWebAppPopup']/div/div/div[1]/h4")
	private WebElement EditWebAppDetailsHeader;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/input[@id='appTitleMam']")
	private WebElement EditAppTitleAndroid;


	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/input[@id='apkPackageIdMam']")
	private WebElement EditAppPackageAndroid;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/input[@id='apkVersionMam']")
	private WebElement EditAppVersionAndroid;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/input[@id='apkVersionCodeMam']")
	private WebElement EditAppVersionCodeAndroid;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/input[@id='apkCategoryMam']")
	private WebElement EditAppCategoryAndroid;

	@FindBy(xpath="//div[@id='editAppPopup']/div/div/div/div/div/textarea[@id='apkDescriptionMam']")
	private WebElement EditAppDescriptionAndroid;

	@FindBy(id="editAppPopupBack")
	private WebElement EditAppDetailsCancelButton;

	@FindBy(id="editAppPopupAdd")
	private WebElement EditAppDetailsSaveButton;

	@FindBy(id="uploadAnotherApk")
	private WebElement EditUploadAPk;

	@FindBy(xpath="//span[text()='Cannot upload apk with different package name.']")
	private WebElement ErrorMessageUploadDifferentPackage;

	@FindBy(xpath="//span[text()='APK has been already added.']")
	private WebElement ErrorMessageUploadSameAPK;
	//apk link
	@FindBy(xpath="//div[@id='APKLinkPopup']/div/div/div/h4")
	private WebElement APkLinkHeader;

	@FindBy(xpath="//div[@id='APKLinkPopup']/div/div/div/div/div/input[@id='apkLinkMam']")
	private WebElement APKLinkTextBox;

	@FindBy(xpath="//h4[text()='APK Link']/preceding-sibling::button")
	private WebElement APKLinkCloseButton;

	@FindBy(id="appApkLinkBackBtn")
	private WebElement APKLinkBackButton;

	@FindBy(id="appApkLinAddBtn")
	private WebElement APKLinkAddButton;

	@FindBy(xpath="//span[text()='Enter valid APK link.']")
	private WebElement APKLinkErrorMessage;

	@FindBy(xpath="//div[@id='editAppLinkPopup']/div/div/div[1]/h4")
	private WebElement EditAppLinkDetailsHeader;

	@FindBy(id="apkLinkTitleMam")
	private WebElement EditAppLinkTitleAndroid;

	@FindBy(id="apkLinkPackageMam")
	private WebElement EditAppLinkBundleIdAndroid;

	@FindBy(id="apkLinkVersionMam")
	private WebElement EditAppLinkVersionAndroid;

	@FindBy(id="apkLinkCategoryMam")
	private WebElement EditAppLinkCategoryAndroid;

	@FindBy(id="apkLinkDescriptionMam")
	private WebElement EditAppLinkDescriptionAndroid;

	@FindBy(id="editAppLinkPopupBack")
	private WebElement EditAppLinkDetailsCancelButton;

	@FindBy(id="editAppLinkPopupAdd")
	private WebElement EditAppLinkDetailsSaveButton;

	@FindBy(xpath="//div[@id='editAppLinkPopup']/div/div/div/div/div/input[@id='apkLinkMam']")
	private WebElement EditAppLinkTextBox;

	@FindBy(xpath="//span[text()='Invalid APK link.']")
	private WebElement EditAPKLinkErrorMessage;
	//web app
	@FindBy(xpath="//div[@id='UploadWebAppPopup']/div/div/div/h4")
	private WebElement WebAppAndroidHeader;

	@FindBy(id="appWebAppTitle")
	private WebElement WebAppTitleAndroid;

	@FindBy(id="appWebAppWebLink")
	private WebElement WebAppLinkAndroid;

	@FindBy(id="appWebAppCategory")
	private WebElement WebAppCategoryAndroid;

	@FindBy(id="appWebAppDescription")
	private WebElement WebAppDescriptionAndroid;

	@FindBy(xpath="//img[@id='andr_webapp_img_preview']/following-sibling::div/input[@title='Upload Icon']")
	private WebElement WebAppImageUploadAndroid;

	@FindBy(xpath="//div[@id='UploadWebAppPopup']/div/div/div[1]/button")
	private WebElement WebAppCloseButtonAndroid;

	@FindBy(id="appWebAppAddBtn")
	private WebElement WebAppSaveButtonAndroid;

	@FindBy(id="appWebAppBackBtn")
	private WebElement WebAppBackButtonAndroid;

	@FindBy(xpath="//span[text()='Web App Added Successfully.']")
	private WebElement ConfirmationMessageWebApp;

	@FindBy(xpath="//span[text()='Image width should be greater than 128px and less than 1024px.']")
	private WebElement ErrorMessageWebAppImage;

	@FindBy(id="appEditWebAppTitle")
	private WebElement EditWebAppTitleAndroid;

	@FindBy(id="appEditWebAppWebLink")
	private WebElement EditWebAppLinkAndroid;

	@FindBy(id="appEditWebAppCategory")
	private WebElement EditWebAppCategoryAndroid;

	@FindBy(id="appEditWebAppDescription")
	private WebElement EditWebAppDescriptionAndroid;

	@FindBy(id="appWebAppEditCancelWebAppBtn")
	private WebElement EditWebAppDetailsCancelButton;

	@FindBy(id="appWebAppEditSaveWebAppBtn")
	private WebElement EditWebAppDetailsSaveButton;

	@FindBy(xpath="//span[text()='App has been already added!']")
	private WebElement ErrorMessageWebApp;

	@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/p")
	private WebElement RemoveSummary;

	@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/button[text()='No']")
	private WebElement RemoveNoButton;

	@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']")
	private WebElement RemoveYesButton;

	@FindBy(id="searchAppMam")
	private WebElement SearchAndroid;

	@FindBy(xpath="//span[text()='No matching result found.']")
	private WebElement SearchErrorMeesage;

	@FindBy(xpath="//ul[@class='applistMAM']/li")
	private List<WebElement> SearchAppAndroid;

	@FindBy(xpath="//ul[@class='applistMAM']/li/div/div/div/p[contains(@class,'apptitleMAM')]")
	private List<WebElement> SearchAppNameAndroid;

	@FindBy(xpath="//ul[@class='applistMAM']/li/div/div/div/p[contains(@class,'categoryAppMAM')]")
	private List<WebElement> SearchCategoryAndroid;

	@FindBy(xpath="//span[@text()='App Added Successfully.']")
	private WebElement NotificationOnProfileCreated;

	public void ClickOnAppStore() throws InterruptedException
	{    
		try {
			AppStore.click();
			waitForidPresent("addNewAppBtn");
			sleep(2);
			Reporter.log("Clicking On App Store, Navigated to App Store Page",true);
		} catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//li[@class='nav-subgrp actAsSubGrp']/p/span[text()='More']"))
			.click();
			sleep(2);
			AppStore.click();
			waitForidPresent("addNewAppBtn");
			sleep(2);
			Reporter.log("Clicking On App Store, Navigated to App Store Page",true);
		}
	}

	@FindBy(xpath="//p[contains(text(),'"+Config.AppStoreEdit+"')]")
	private WebElement Lite;

	@FindBy(xpath="//div[p[text()='"+Config.AppStoreEdit+"']]/following-sibling::div/div/span")
	private WebElement LiteNEWMoreBtn;
	public void ClickingOnEditBtnOfLiteApkUpdate() throws InterruptedException {

		if(Lite.isDisplayed())
		{
			LiteNEWMoreBtn.click();
			sleep(2);
			Reporter.log("Clicked on LiteMoreBtn", true);

		}
		else {
			ALib.AssertFailMethod("Didnt click on Lite Apk More Btn ");
		}
		sleep(2);
	}
	public void ClickOnEditSymbol(String App) throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+App+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void ClickOnEditUploadAPkAndroid_DifferentPackage(String ApkPath) throws Throwable
	{
		EditUploadAPk.click();
		sleep(3);
		Runtime.getRuntime().exec(ApkPath);

	}

	public void EnterSearchAndroid(String AppName) throws Throwable
	{   SearchAndroid.clear();
		SearchAndroid.sendKeys(AppName);
	}
	
	//Madhu 
	@FindBy(xpath = "//*[@id='informationDialog']/div/div/div[2]/button")
	private WebElement clickOnOKBn;
	
	public void DeleteSearchedApp(String AppName) throws Throwable
	{ sleep(10);
		int apps = Initialization.driver.findElements(By.xpath("//p[text()='"+AppName+"']")).size();
	     for (int i=0 ;i<apps;i++) {
	    	 sleep(2);try {
	    	 ClickOnMoreSymbolLite(AppName);
	    	 ClickOnWebAppRemoveSymbol(AppName);
	    	 } catch(Exception e) {
	    		
	    	 }
	}
	}

	public void ClickOnMoreSymbolLite(String AppName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+AppName+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+AppName+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void VerifyOfAppStoreWindow() throws InterruptedException
	{
		boolean value=AndroidAppStore.isDisplayed();
		String PassStatement = "PASS >> App Store for Android Platform is displayed successfully when clicked on App Store";
		String FailStatement = "Fail >> App Store for Android Platform is not displayed when clicked on App Store";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=iOSAppStore.isDisplayed();
		PassStatement = "PASS >> App Store for iOS Platform is displayed successfully when clicked on App Store";
		FailStatement = "Fail >> App Store for iOS Platform is not displayed when clicked on App Store";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnAddNewAppAndroid() throws InterruptedException
	{sleep(10);
		try {
		CloseWebAppPopup.click();
	}catch(Exception e) {
		Reporter.log("Upload POP Up is closed already", true);
	}
		AddApp.click();
		waitForidPresent("UploadApkMam");
		Reporter.log("Clicked on Add New App Button",true);
		sleep(4);
	}

	public void AppiumwaitForPresenceOfElement(AndroidDriver<WebElement> Driver,int time,String Xpath){
		WebDriverWait wait = new WebDriverWait(Driver,time);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xpath)));
	}
	public void verifynoApplicationAvailable() throws InterruptedException
	{
		AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[@text='No Applications available!']");
		if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='No Applications available!']").isDisplayed()) {
			Reporter.log("No Application Available is Displayed",true);
		}else {
			ALib.AssertFailMethod("No Application Available is not Displayed");
		}
	}

	public void VerifyOfAddNewAppPopUpAndroid() throws InterruptedException
	{
		String ActualValue = AddNewPopupAndroidHeader.getText();
		System.out.println(ActualValue);
		String ExpectedValue = "Select Options";
		String PassStatement = "PASS >> " + ActualValue+ " for Android Platform is displayed successfully when clicked on Add New App";
		String FailStatement = "Fail >> " + ActualValue+ " for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=UploadAPKAndroid.isDisplayed();
		PassStatement = "PASS >> Upload Apk for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Upload Apk for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=APKLinkAndroid.isDisplayed();
		PassStatement = "PASS >> APK Link for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> APK Link for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=WebAppAndroid.isDisplayed();
		PassStatement = "PASS >> Web App for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Web App for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnCloseAddNewAppAndroid() throws InterruptedException
	{
		CloseAddNewAppPopupAndroid.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close of Add New App Button",true);
		sleep(3);
	}

	public void ClickOnUploadAPKAndroid() throws InterruptedException
	{
		UploadAPKAndroid.click();
		waitForidPresent("dropZone");
		sleep(5);
		Reporter.log("Clicked On Upload APk,Navigated Succesfully to Upload Apk Popup", true);
	}

	public void ClickOnBrowseFileAndroid(String ApkPath) throws Throwable
	{
		UploadAPKAndroidBrowseFile.click();
		sleep(7);
		Runtime.getRuntime().exec(ApkPath);
		sleep(6);
		try {
			while (UploadAPKAndroidUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKAndroidExtracting.isDisplayed()) {}
		} catch (Exception e) {}
		sleep(15);
	}

	public void VerifyOfUploadApkPopupAndroid() throws InterruptedException
	{
		String ActualValue=AddNewPopupAndroidHeader.getText();
		String ExpectedValue="Upload APK";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Upload APK";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed even when clicked on Upload APK";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		ActualValue=UploadAPKAndroidSection.getText();
		ExpectedValue="Drop your APK file here, or select a file";
		PassStatement = "PASS >> "+ActualValue+" caption for Android Platform is displayed successfully when clicked on Upload APK";
		FailStatement = "Fail >> "+ActualValue+" for Android Platform is not displayed when clicked on Upload APK";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=UploadAPKAndroidBrowseFile.isDisplayed();
		PassStatement = "PASS >> Browse File is displayed successfully when clicked on Upload APK";
		FailStatement = "Fail >> Browse File is not displayed when clicked on Upload APK";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnCloseUploadAPkAndroid() throws InterruptedException
	{
		CloseUploadAPKPopupAndroid.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close of Upload APk Popup, Navigates successfully to App Store page",true);
		sleep(3);
	}

	public void ClickOnBrowseFileAndroid(int a) throws Throwable{
		UploadAPKAndroidBrowseFile.click();
		sleep(7);
		
		if(a==5)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\FBliteNewVersion.exe");
			sleep(6);
		}
		
		if(a==6)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\FacebookLowerUpdatedVersion.exe");
			sleep(6);
		}
		if(a==7)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\J Touch.exe");
			sleep(6);
		}
		if(a==8)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\Facebook283.exe");
			sleep(6);
		}
		try {
			while (UploadAPKAndroidUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKAndroidExtracting.isDisplayed()) {}
		} catch (Exception e) {}
		sleep(15);
	}

	public void VerifyOfUploadApkAndroid() throws InterruptedException
	{
		String ActualValue=UploadAPKHeader.getText();
		String ExpectedValue="Upload APK";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Apk is uploaded";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Apk is uploaded";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleAndroid,AppPackageAndroid,AppVersionAndroid,AppVersionCodeAndroid};
		String[] Text={"App Title","Package","Version","Version Code"};
		for(int i=0;i<wb.length;i++)
		{

			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when Apk is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when Apk is uploaded as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: AppTitle=ActualValue; break;
			case 1: ApkPackage=ActualValue; break;
			case 2: ApkVersion=ActualValue; break;
			case 3: ApkVersionCode=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryAndroid,AppDescriptionAndroid};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text1[i]+" of the application uploaded is not visibe when Apk is uploaded";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is visibe successfully when Apk is uploaded as "+ActualValue;
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void EnterDescriptionAndroid(String Description) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppDescriptionAndroid.sendKeys(Description);
	}

	public void EnterCategoryAndroid(String Category) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppCategoryAndroid.sendKeys(Category);
	}

	@FindBy(xpath="//p[contains(text(),'"+Config.AppStoreEditOldVersionToLatest+"')]")
	private WebElement Fbliteold;

	@FindBy(xpath="//div[p[text()='"+Config.AppStoreEditOldVersionToLatest+"']]/following-sibling::div/div/span")
	private WebElement FbliteOLDNEWMoreBtn;
	public void ClickingOnEditBtnOfLiteApkOLD() throws InterruptedException {

		if(Fbliteold.isDisplayed())
		{
			FbliteOLDNEWMoreBtn.click();
			sleep(2);
			Reporter.log("Clicked on LiteMoreBtn", true);

		}
		else {
			ALib.AssertFailMethod("Didnt click on Fbliteold Apk More Btn ");
		}
		sleep(2);
	}

	@FindBy(xpath="//*[@id='uploadApkPopupAdd']")
	private WebElement uploadApkPopupAdd;

	public void uploadApkPopupAdd_Btn() throws InterruptedException {
		uploadApkPopupAdd.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='uploadAnotherApk']")
	private WebElement uploadAnotherApk;

	public void UploadNewVersionApk(int a) throws InterruptedException, IOException {

		uploadAnotherApk.click();
		sleep(7);

		if(a==1)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\Facebook284.exe");
			sleep(6);
		}
		if(a==2)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\Facebook283.exe");
			sleep(6);
		}


		try {
			while (UploadAPKAndroidUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKAndroidExtracting.isDisplayed()) {}
		} catch (Exception e) {}
		sleep(15);
	}

	@FindBy(xpath="//p[contains(text(),'"+Config.AppStoreEdit+"')]/../../div[2]/div/ul/li/a[contains(text(),'Edit')]")
	private WebElement EditBtnLiteForUpdatingFB;

	public void UpdateFBliteClickOnEdit() throws InterruptedException {
		EditBtnLiteForUpdatingFB.click();
		waitForXpathPresent("//*[@id='editAppPopup']/div/div/div[1]/h4[text()='Edit App Details']");
		sleep(2);
	}

	@FindBy(xpath="//p[contains(text(),'"+Config.AppStoreEditOldVersionToLatest+"')]/../../div[2]/div/ul/li/a[contains(text(),'Edit')]")
	private WebElement EditBtnLiteForUpdatingFBOldToNewAgain;

	public void UpdateFBliteClickOnEditOldToNew() throws InterruptedException {

		EditBtnLiteForUpdatingFBOldToNewAgain.click();
		waitForXpathPresent("//*[@id='editAppPopup']/div/div/div[1]/h4[text()='Edit App Details']");
		sleep(2);

	}

	@FindBy(xpath="//span[text()='Application has been already added.']")
	private WebElement ErrorMessageAppStore;

	public void VerifyErrorMessage() throws InterruptedException {

		String ActualValue = ErrorMessageAppStore.getText();
		System.out.println(ActualValue);
		String ExpectedValue = "Application has been already added.";
		String PassStatement = "PASS >> " + ActualValue+ " is displayed";
		String FailStatement = "Fail >> " + ActualValue+ " is NOT displayed";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		sleep(2);


	}

	@FindBy(xpath="(.//*[@id='apkLinkMam'])[2]")
	private WebElement Enterapklink;
	public void EnterAppLink(String AppLink ) {
		waitForVisibilityOf("(.//*[@id='apkLinkMam'])[2]");
		Enterapklink.sendKeys(AppLink);
	}

	@FindBy(xpath="//*[@id='appTitleMam']")
	private WebElement EditAppTitleAndroidApk;
	public void EnterEditAppTitleAndroid(String AppTitle) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		waitForXpathPresent("//*[@id='appTitleMam']");
		EditAppTitleAndroidApk.clear();
		EditAppTitleAndroidApk.sendKeys(AppTitle);
	}

	@FindBy(xpath="(//*[@id='apkDescriptionMam'])[1]")
	private WebElement ApkLinkDescription;
	public void EnterEditAppLinkDescriptionAndroid(String Description) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		ApkLinkDescription.sendKeys(Description);
	}

	public void ErrorMessageToFillDetails() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Please fill the required fields.' is displayed successfully when Category ,Description is empty";
		String FailStatement = "Fail >> Alert Message : 'Please fill the required fields.' is not displayed when Category ,Description is empty";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageToFillDetails, true, PassStatement,FailStatement, 5);
	}

	@FindBy(xpath="(//input[@id='apkCategoryMam'])[1]")
	private WebElement ApkLinkCategory;
	public void EnterEditAppLinkCategoryAndroid(String Categoary) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		ApkLinkCategory.sendKeys(Categoary);
	}

	public void ClickOnUploadApkPopupAddButton_Error()
	{
		UploadApkPopupAddButton.click();
	}

	public void ClickOnUploadApkPopupAddButton() throws InterruptedException
	{sleep(20);
		UploadApkPopupAddButton.click();
		Reporter.log("Clicked on Add Button Of Upload APK popup, Navigates successfully to App Store Page",true);
		sleep(4);
	}

	public void ClickOnUploadApkPopupAddButton1() throws InterruptedException
	{
		UploadApkPopupAddButton.click();
		sleep(4);
	}

	public void EnterCategoryAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		ApkCategory=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterDescriptionAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		ApkDescription=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void ClearCategoryAndroid()
	{
		AppCategoryAndroid.clear();
	}

	public void ClearDescriptionAndroid() 
	{
		AppDescriptionAndroid.clear();
	}

	public void ClickOnUploadApkPopupBackButton() throws InterruptedException
	{
		UploadApkPopupBackButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Back Button Of Upload APK popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public boolean CheckforApplicationAndroid() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + AppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void VerifyOfApplicationAndroid_False() throws InterruptedException{
		boolean value=CheckforApplicationAndroid();
		String PassStatement = "PASS >> "+AppTitle+" is not added when Clicked on Back Button of Upload Apk";
		String FailStatement = "Fail >> "+AppTitle+" is added even when Clicked on Back Button of Upload Apk";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfApplicationAndroid_True() throws InterruptedException{
		boolean value=CheckforApplicationAndroid();
		String PassStatement = "PASS >> "+AppTitle+" is added when Clicked on Add Button of Upload Apk";
		String FailStatement = "Fail >> "+AppTitle+" is not added even when Clicked on Add Button of Upload Apk";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void ClickOnAppDetails() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[div[div[p[text()='"+AppTitle+"']]]]/preceding-sibling::div/img")).click();
		waitForidPresent("editBtn");
		sleep(5);
		Reporter.log("Clicked on Image Of Application, Navigates successfully to App Details Page",true);
	}

	public void ClickOnEditInAppDetails() throws InterruptedException{
		AppDetailsEditButton.click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit Button, Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyOfAppDetailsHeader() throws InterruptedException{
		String ActualValue=AppDetailsHeader.getText();
		String ExpectedValue="App Details";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Clicked on Image of Application";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed even when Clicked on Image of Application";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);

	}
	
	public void ClickOnUploadIPAcloseButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//*[@id='iOSAddNewAppPopupClose']")).click();
		sleep(3);
	}
	
	public void CloseAppDetailsiOSWindow() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//*[@id='appDet_modal']/div[1]/button")).click();
		sleep(3);
	}
	
	public void SearchAppIniOSAppStore() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//*[@id='iOSsearchAppMam']")).sendKeys("SureFox");
		sleep(5);
	}

	public void VerifyOfAppInAppDetails() throws InterruptedException{
		WebElement[] wb={AppDetailsAppTitleAndroid,AppDetailsVersionAndroid,AppDetailsCategoryAndroid,AppDetailsDescriptionAndroid};
		String[] Text={AppTitle,ApkVersion,ApkCategory,ApkDescription};
		String[] AppText={"App Title","Version","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getText();
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Apk is added in App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Apk is added in App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnCloseOfAppDetails() throws InterruptedException
	{
		AppDetailsClose.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnMoreSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+AppTitle+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+AppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnEditSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+AppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyEditAppDetailsHeader()
	{
		String ActualValue=EditAppDetailsHeader.getText();
		String ExpectedValue="Edit App Details";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Clicked on Edit Button of Application";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed even when Clicked on Edit Button of Application";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void VerifyOfAppInEditAppDetails() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppPackageAndroid,EditAppVersionAndroid,EditAppVersionCodeAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={AppTitle,ApkPackage,ApkVersion,ApkVersionCode,ApkCategory,ApkDescription};
		String[] AppText={"App Title","Package","Version","Version Code","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through Upload APK";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through Upload APK";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnCloseOfEditAppDetails() throws InterruptedException
	{
		CloseEditAppDetails.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void EnterEditAppCategoryAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditApkCategory=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppDescriptionAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditApkDescription=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppTitleAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppTitleAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditAppTitle=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void ClearEditAppTitleAndroid()
	{
		EditAppTitleAndroid.clear();
	}

	public void ClearEditAppCategoryAndroid()
	{
		EditAppCategoryAndroid.clear();
	}

	public void ClearEditAppDescriptionAndroid() 
	{
		EditAppDescriptionAndroid.clear();
	}

	public boolean CheckforEditedApplicationAndroid() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + EditAppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;

	}


	public void VerifyNotificationMessageAppStore() throws InterruptedException{
		boolean value =true;

		try{
			NotificationOnProfileCreated.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}
		Assert.assertTrue(value,"FAIL >> Profile Created Notification not displayed ");
		Reporter.log("PASS >> Notification 'Profile created successfully.'is displayed",true );	
		waitForidPresent("addPolicyBtn");
		sleep(4);

	}

	public void ClickOnCancelButtonOfEditAppDetails() throws InterruptedException
	{
		EditAppDetailsCancelButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Cancel Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfEditAppDetails() throws InterruptedException
	{
		EditAppDetailsSaveButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Save Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void VerifyOfEditApplicationAndroid_False() throws InterruptedException{
		boolean value=CheckforEditedApplicationAndroid();
		String PassStatement = "PASS >> App Title is not edited when Clicked on Cancel Button of Edit App Details Page";
		String FailStatement = "Fail >> App Title is edited even when Clicked on Cancel Button of Edit App Details Page";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfEditApplicationAndroid_True() throws InterruptedException{
		boolean value=CheckforEditedApplicationAndroid();
		String PassStatement = "PASS >> "+AppTitle+" is edited successfully as "+EditAppTitle+" when Clicked on Save Button of Edit App Details Page";
		String FailStatement = "Fail >> "+AppTitle+" is not edited as "+EditAppTitle+" even when Clicked on Save Button of Edit App Details Page";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfEditedAppInEditAppDetails_Cancel() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={AppTitle,ApkCategory,ApkDescription};
		String[] AppText={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is not edited successfully when Clicked on Cancel Button of Edit App Details Page";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is edited even when Clicked on Cancel Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnEditedAppMoreSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditAppTitle+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+EditAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnEditedAppEditSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyOfEditedAppInEditAppDetails_Save() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={EditAppTitle,EditApkCategory,EditApkDescription};
		String[] AppText1={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> "+AppText1[i]+" of the application is edited successfully as "+Text[i]+" when Clicked on Save Button of Edit App Details Page";
			String FailStatement = "Fail >> "+AppText1[i]+" of the application is not edited as "+Text[i]+" even when Clicked on Save Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnEditUploadAPkAndroid_DifferentPackage(int row,int col) throws Throwable
	{
		EditUploadAPk.click();
		sleep(3);
		Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet19",row ,col));

	}

	public void ErrorMessageUploadDifferentPackage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Cannot upload apk with different package name' is displayed successfully when Different package is uploaded in Edit App Details";
		String FailStatement = "Fail >> Alert Message : 'Cannot upload apk with different package name' is not displayed even when Different package is uploaded in Edit App Details";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageUploadDifferentPackage, true, PassStatement,FailStatement, 5);
	}

	public void ClickOnEditUploadAPkAndroid_SamePackage(int row,int col) throws Throwable
	{
		EditUploadAPk.click();
		sleep(3);
		Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet19",row ,col));
		sleep(5);
		try {
			while (UploadAPKAndroidUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKAndroidExtracting.isDisplayed()) {}
		} catch (Exception e) {}
		sleep(15);
	}

	public void VerifyOfEditUploadApkAndroid() throws InterruptedException
	{
		String ActualValue=UploadAPKHeader.getText();
		String ExpectedValue="Upload APK";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Apk is uploaded";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Apk is uploaded";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppVersionAndroid,AppVersionCodeAndroid};
		String[] Text={"Version","Version Code"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application updated succesffully when Same Package Apk is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application is not updated even when Same Package Apk is uploaded as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ApkVersion=ActualValue; break;
			case 1: ApkVersionCode=ActualValue; break;
			}
		}
		WebElement[] wb1={AppTitleAndroid,AppPackageAndroid,AppCategoryAndroid,AppDescriptionAndroid};
		String[] Text1={"App Title","Package","Category","Description"};

		String[] ExpectedText={AppTitle,ApkPackage,EditApkCategory,EditApkDescription};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			ExpectedValue=ExpectedText[i];
			PassStatement = "PASS >> App "+Text1[i]+" of the application updated succesffully when Same Package Apk is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text1[i]+" of the application is not updated even when Same Package Apk is uploaded as "+ActualValue;
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		}
	}

	public void ErrorMessageUploadSameAPK() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'APK has been already added' is displayed successfully when Same APK is uploaded again";
		String FailStatement = "Fail >> Alert Message : 'APK has been already added' is not displayed even when Same APK is uploaded again";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageUploadSameAPK, true, PassStatement,FailStatement, 5);
	}

	public void VerifyOfAppInEditAppDetails_SamePackage() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppPackageAndroid,EditAppVersionAndroid,EditAppVersionCodeAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={AppTitle,ApkPackage,ApkVersion,ApkVersionCode,EditApkCategory,EditApkDescription};
		String[] AppText={"App Title","Package","Version","Version Code","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Apk is added in Edit App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Apk is added in Edit App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	//APK Link
	public void ClickOnAPKLinkAndroid() throws InterruptedException
	{
		APKLinkAndroid.click();
		waitForidPresent("appApkLinkBackBtn");
		sleep(2);
		Reporter.log("Clicked On APk Link,Navigated Succesfully to Upload Apk Popup", true);
	}

	public void VerifyOfApkLinkPopupAndroid() throws InterruptedException
	{
		String ActualValue=APkLinkHeader.getText();
		String ExpectedValue="APK Link";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on APK Link";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed when clicked on APK Link";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void EnterAPKLink(int row,int col) throws Throwable{
		APKLinkTextBox.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		ApkLink=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void ClickOnCloseOfAPKLink() throws InterruptedException
	{
		APKLinkCloseButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of APK Link popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnBackButtonOfAPKLink() throws InterruptedException
	{
		APKLinkBackButton.click();
		waitForidPresent("addNewAppBtn");
		boolean value=true;
		try{
			AddNewPopupAndroidHeader.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		String PassStatement = "PASS >> Extracting App Details Popup is not displayed when Clicked on Back button of Apk Link Popup";
		String FailStatement = "Fail >> Extracting App Details Popup is displayed even when Clicked on Back button of Apk Link Popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("Clicked on Back Button Of APK Link popup, Navigates successfully to Select Options Popup",true);
		sleep(5);
	}

	public void ClickOnApkLinkPopupAddButton_Error() throws InterruptedException
	{
		APKLinkAddButton.click();
		sleep(2);
	}

	public void ClickOnApkLinkPopupAddButton() throws InterruptedException
	{
		APKLinkAddButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Add Button Of Upload APK popup, Navigates successfully to App Store Page",true);
		sleep(5);
	}

	public void APKLinkErrorMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Enter valid APK link.' is displayed successfully when invalid apk is added";
		String FailStatement = "Fail >> Alert Message : 'Enter valid APK link.' is not displayed even when invalid apk is added";
		Initialization.commonmethdpage.ConfirmationMessageVerify(APKLinkErrorMessage, true, PassStatement,FailStatement, 5);
	}

	public void ApkLinkExtracting() throws Throwable
	{
		String ActualValue = AddNewPopupAndroidHeader.getText();
		String ExpectedValue = "Extracting App Details";
		String PassStatement = "PASS >> " + ActualValue
				+ " Header is displayed successfully when clicked on Add Button of APK Link";
		String FailStatement = "Fail >> " + ActualValue + " Header is not displayed even when clicked on Add Button of APK Link";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		sleep(3);
		try {
			while (UploadAPKAndroidExtracting.isDisplayed()) {
			}
		} catch (Exception e) {
		}
		sleep(15);
	}

	public void VerifyOfAPkLinkAndroid() throws InterruptedException
	{
		String ActualValue=UploadAPKHeader.getText();
		String ExpectedValue="APK Link";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Apk Link is added";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Apk Link is added";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleAndroid,AppPackageAndroid,AppVersionAndroid,AppVersionCodeAndroid};
		String[] Text={"App Title","Package","Version","Version Code"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when Apk Link is added";
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when Apk Link is added";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: AppTitle=ActualValue; break;
			case 1: ApkPackage=ActualValue; break;
			case 2: ApkVersion=ActualValue; break;
			case 3: ApkVersionCode=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryAndroid,AppDescriptionAndroid};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text1[i]+" of the application uploaded is not visibe when Apk Link is added";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is visibe successfully when Apk Link is added";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyEditAppLinkDetailsHeader()
	{
		String ActualValue=EditAppLinkDetailsHeader.getText();
		String ExpectedValue="Edit App Details";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Clicked on Edit Button of Application";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed even when Clicked on Edit Button of Application";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void VerifyOfAppInEditAppLinkDetails() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkTextBox,EditAppLinkVersionAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={AppTitle,ApkLink,ApkVersion,ApkCategory,ApkDescription};
		String[] AppText={"App Title","APK Link","Version","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through APK Link";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through APK Link";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}


	public void ClickOnCloseOfEditAppLinkDetails() throws InterruptedException
	{
		CloseEditAppLinkDetails.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void EnterEditAppLinkCategoryAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditApkCategory=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppLinkDescriptionAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditApkDescription=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppLinkTitleAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkTitleAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditAppTitle = ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void ClearEditAppLinkTitleAndroid()
	{
		EditAppLinkTitleAndroid.clear();
	}

	public void ClearEditAppLinkCategoryAndroid()
	{
		EditAppLinkCategoryAndroid.clear();
	}

	public void ClearEditAppLinkDescriptionAndroid() 
	{
		EditAppLinkDescriptionAndroid.clear();
	}

	public void EnterEditAppLinkAndroid(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkTextBox.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		ApkLink=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void ClearEditAppLinkAndroid()
	{
		EditAppLinkTextBox.clear();

	}

	public void ClickOnCancelButtonOfEditAppLinkDetails() throws InterruptedException
	{
		EditAppLinkDetailsCancelButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Cancel Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfEditAppLinkDetails() throws InterruptedException
	{
		EditAppLinkDetailsSaveButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Save Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void VerifyOfEditedAppInEditAppLinkDetails_Cancel() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={AppTitle,ApkCategory,ApkDescription};
		String[] AppText={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is not edited successfully when Clicked on Cancel Button of Edit App Details Page";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is edited even when Clicked on Cancel Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void VerifyOfEditedAppInEditAppLinkDetails_Save() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={EditAppTitle,EditApkCategory,EditApkDescription};
		String[] AppText1={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> "+AppText1[i]+" of the application is edited successfully as "+Text[i]+" when Clicked on Save Button of Edit App Details Page";
			String FailStatement = "Fail >> "+AppText1[i]+" of the application is not edited as "+Text[i]+" even when Clicked on Save Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ErrorMessageAPKLinkDifferentPackage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Invalid APK link.' is displayed successfully when Different package APK Link is saved in Edit App Details";
		String FailStatement = "Fail >> Alert Message : 'Invalid APK link.' is not displayed even when Different package APK Link is saved in Edit App Details";
		Initialization.commonmethdpage.ConfirmationMessageVerify(EditAPKLinkErrorMessage, true, PassStatement,FailStatement, 5);
	}



	public void VerifyOfAppInEditAppLinkDetails_SamePackage() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkTextBox,EditAppLinkVersionAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={AppTitle,ApkLink,ApkVersion,EditApkCategory,EditApkDescription};
		String[] AppText={"App Title","Package","Version","Version Code","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Apk is added in Edit App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Apk is added in Edit App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void VerifyOfAPkLinkAndroid_Package() throws InterruptedException
	{
		String ActualValue=UploadAPKHeader.getText();
		String ExpectedValue="APK Link";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Apk Link is added";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Apk Link is added";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleAndroid,AppPackageAndroid,AppVersionAndroid,AppVersionCodeAndroid};
		String[] Text={"App Title","Package","Version","Version Code"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when Apk Link is added";
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when Apk Link is added";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: AppTitle=ActualValue; break;
			case 1: ApkPackage=ActualValue; break;
			case 2: ApkVersion=ActualValue; break;
			case 3: ApkVersionCode=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryAndroid,AppDescriptionAndroid};
		String[] AppText={EditApkCategory,EditApkDescription};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = ActualValue.equals(AppText[i]);
			PassStatement = "PASS >> App "+Text1[i]+" of the application is saved sucessfully when Apk Link is edited";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is not saved when Apk Link is edited";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	// Web App 

	public void ClickOnWebAppAndroid() throws InterruptedException
	{
		WebAppAndroid.click();
		waitForidPresent("appWebAppTitle");
		sleep(2);
		Reporter.log("Clicked On Web App,Navigated Succesfully to Web App Popup", true);
	}

	public void VerifyOfWebAppPopupAndroid() throws InterruptedException
	{
		String ActualValue=WebAppAndroidHeader.getText();
		String ExpectedValue="Upload Web App";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Web App";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed when clicked on Web App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void EnterWebAppTitleAndroid(int row,int col) throws Throwable{
		WebAppTitleAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppTitle=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppLinkAndroid(int row,int col) throws Throwable{
		WebAppLinkAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppLink=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppCategoryAndroid(int row,int col) throws Throwable{
		WebAppCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppCategory=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppDescriptionAndroid(int row,int col) throws Throwable{
		WebAppDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppDescription=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void ClearWebAppTitleAndroid() throws Throwable{
		WebAppTitleAndroid.clear();
	}

	public void ClearWebAppLinkAndroid() throws Throwable{
		WebAppLinkAndroid.clear();
	}

	public void ClearWebAppCategoryAndroid() throws Throwable{
		WebAppCategoryAndroid.clear();
	}

	public void ClearWebAppDescriptionAndroid() throws Throwable{
		WebAppDescriptionAndroid.clear();
	}

	public void ClickOnCloseButtonOfWebApp() throws InterruptedException
	{
		WebAppCloseButtonAndroid.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of Web App popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfWebApp_Error() throws InterruptedException
	{
		WebAppSaveButtonAndroid.click();

	}

	public void ClickOnSaveButtonOfWebApp() throws InterruptedException
	{
		WebAppSaveButtonAndroid.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Save Button Of Web App popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnBackButtonOfWebApp() throws InterruptedException
	{
		WebAppBackButtonAndroid.click();
		waitForidPresent("UploadWebAppMam");
		boolean value=true;
		try{
			AddNewPopupAndroidHeader.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		String PassStatement = "PASS >> Web App Details is not saved when clicked on Back Button of Web App popup";
		String FailStatement = "Fail >> Web App Details is saved even when clicked on Back Button of Web App popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("Clicked on Back Button Of Web App popup, Navigates successfully to Select Options Popup",true);
		sleep(3);
	}

	public boolean CheckforWebApplicationAndroid() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + WebAppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void VerifyOfWebApplicationAndroid_False() throws InterruptedException{
		boolean value=CheckforWebApplicationAndroid();
		String PassStatement = "PASS >> "+WebAppTitle+" is not added when Clicked on Back Button of Web App";
		String FailStatement = "Fail >> "+WebAppTitle+" is added even when Clicked on Back Button of Web Appk";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfWebApplicationAndroid_True() throws InterruptedException{
		boolean value=CheckforWebApplicationAndroid();
		String PassStatement = "PASS >> "+WebAppTitle+" is added when Clicked on Add Button of Web App";
		String FailStatement = "Fail >> "+WebAppTitle+" is not added even when Clicked on Add Button of Web App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void ConfirmationMessageWebAppAdding() throws InterruptedException
	{
		String PassStatement = "PASS >> Confirmation Message : 'Web App Added Successfully.' is displayed successfully when Web app is added";
		String FailStatement = "Fail >> Confirmation Message : 'Web App Added Successfully.' is not displayed even when Web app is added";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessageWebApp, true, PassStatement,FailStatement, 2);
	}

	public void ClickOnImageFieldWebAppAndroid(int b) throws Throwable
	{
		WebAppImageUploadAndroid.click();
		sleep(3);
		if(b==1)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\flipIMG.exe");
		}
		if(b==2)
		{
			Runtime.getRuntime().exec("D:\\UplaodFiles\\App Store-Android\\amazonIMG.exe");
		}
		sleep(1);
	}

	public void AlertMessageWebAppImageUploading() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Image width should be greater than 128px and less than 1024px.' is displayed successfully when Image greater than allowed pixel is uploaded";
		String FailStatement = "Fail >> Alert Message : 'Image width should be greater than 128px and less than 1024px.' is not displayed even when Image greater than allowed pixel is uploaded";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageWebAppImage, true, PassStatement,FailStatement, 5);
	}

	public void ImageVerification() throws InterruptedException
	{
		sleep(25);
		String ActualValue = Initialization.driver
				.findElement(By.xpath("//li[div[div[div[p[text()='" + WebAppTitle + "']]]]]/div/img")).getAttribute("src");
		String ExpectedValue = "no-image-for-app.png";
		boolean value = ActualValue.contains(ExpectedValue);
		String PassStatement = "PASS >> Image is saved successfully when Image is uploaded from Web app";
		String FailStatement = "Fail >> Image is not saved even when Image is uploaded from Web app";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnWebAppMoreSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+WebAppTitle+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+WebAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnWebAppEditSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+WebAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void ClickOnWebAppDetails() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[div[div[p[text()='"+WebAppTitle+"']]]]/preceding-sibling::div/img")).click();
		waitForidPresent("editBtn");
		sleep(5);
		Reporter.log("Clicked on Image Of Application, Navigates successfully to App Details Page",true);
	}

	public void VerifyOfWebAppInAppDetails() throws InterruptedException{
		WebElement[] wb={AppDetailsAppTitleAndroid,AppDetailsPackageAndroid,AppDetailsCategoryAndroid,AppDetailsDescriptionAndroid};
		String[] Text={WebAppTitle,WebAppLink,WebAppCategory,WebAppDescription};
		String[] AppText={"App Title","App Link","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getText();
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Web App is added in App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Web App is added in App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void VerifyEditWebAppDetailsHeader()
	{
		String ActualValue=EditWebAppDetailsHeader.getText();
		String ExpectedValue="Edit Web App Details";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Clicked on Edit Button of Application";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed even when Clicked on Edit Button of Application";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void VerifyOfWebAppInEditAppDetails() throws InterruptedException{
		WebElement[] wb={EditWebAppTitleAndroid,EditWebAppLinkAndroid,EditWebAppCategoryAndroid,EditWebAppDescriptionAndroid};
		String[] Text={WebAppTitle,WebAppLink,WebAppCategory,WebAppDescription};
		String[] AppText={"App Title","App Link","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through Web App";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through Web App";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnCloseOfEditWebAppDetails() throws InterruptedException
	{
		CloseEditWebAppDetails.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Close Of Edit Web App Details popup, Navigates successfully to App Store Page",true);
		sleep(4);
	}

	public void EnterEditWebAppTitleAndroid(int row,int col) throws Throwable{
		EditWebAppTitleAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		EditWebAppTitle=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterEditWebAppLinkAndroid(int row,int col) throws Throwable{
		EditWebAppLinkAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		EditWebAppLink=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterEditWebAppCategoryAndroid(int row,int col) throws Throwable{
		EditWebAppCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		EditWebAppCategory=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterEditWebAppDescriptionAndroid(int row,int col) throws Throwable{
		EditWebAppDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		EditWebAppDescription=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void ClearEditWebAppTitleAndroid() throws Throwable{
		EditWebAppTitleAndroid.clear();
	}

	public void ClearEditWebAppLinkAndroid() throws Throwable{
		EditWebAppLinkAndroid.clear();
	}

	public void ClearEditWebAppCategoryAndroid() throws Throwable{
		EditWebAppCategoryAndroid.clear();
	}

	public void ClearEditWebAppDescriptionAndroid() throws Throwable{
		EditWebAppDescriptionAndroid.clear();
	}

	public void ClickOnCancelButtonOfEditWebAppDetails() throws InterruptedException
	{
		EditWebAppDetailsCancelButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Cancel Button Of Edit Web App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfEditWebAppDetails() throws InterruptedException
	{
		EditWebAppDetailsSaveButton.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicked on Save Button Of Edit Web App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public boolean CheckforEditedWebApplicationAndroid() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + EditWebAppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;


	}


	public void VerifyOfEditWebApplicationAndroid_False() throws InterruptedException{
		boolean value=CheckforEditedWebApplicationAndroid();
		String PassStatement = "PASS >> App Title is not edited when Clicked on Cancel Button of Edit Web App Details Page";
		String FailStatement = "Fail >> App Title is edited even when Clicked on Cancel Button of Edit Web App Details Page";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}



	public void VerifyOfEditWebApplicationAndroid_True() throws InterruptedException{
		boolean value=CheckforEditedWebApplicationAndroid();
		String PassStatement = "PASS >> "+WebAppTitle+" is edited successfully as "+EditWebAppTitle+" when Clicked on Save Button of Edit Web App Details Page";
		String FailStatement = "Fail >> "+WebAppTitle+" is not edited as "+EditWebAppTitle+" even when Clicked on Save Button of Edit Web App Details Page";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(6);
	}

	public void VerifyOfEditedWebAppInEditAppDetails_Cancel() throws InterruptedException{
		WebElement[] wb={EditWebAppTitleAndroid,EditWebAppLinkAndroid,EditWebAppCategoryAndroid,EditWebAppDescriptionAndroid};
		String[] Text={WebAppTitle,WebAppLink,WebAppCategory,WebAppDescription};
		String[] AppText={"App Title","App Link","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is not edited successfully when Clicked on Cancel Button of Edit Web App Details Page";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is edited even when Clicked on Cancel Button of Edit Web App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnEditedWebAppMoreSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditWebAppTitle+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+EditWebAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnEditedWebAppEditSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditWebAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyOfEditedWebAppInEditWebAppDetails_Save() throws InterruptedException{
		WebElement[] wb={EditWebAppTitleAndroid,EditWebAppLinkAndroid,EditWebAppCategoryAndroid,EditWebAppDescriptionAndroid};
		String[] Text={EditWebAppTitle,EditWebAppLink,EditWebAppCategory,EditWebAppDescription};
		String[] AppText={"App Title","App Link","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> "+AppText[i]+" of the application is edited successfully as "+Text[i]+" when Clicked on Save Button of Edit Web App Details Page";
			String FailStatement = "Fail >> "+AppText[i]+" of the application is not edited as "+Text[i]+" even when Clicked on Save Button of Edit Web App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ErrorMessageWebApp() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'App has been already added!' is displayed successfully when Same web app is added again";
		String FailStatement = "Fail >> Alert Message : 'App has been already added!' is not displayed even when Same web app is added again";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageWebApp, true, PassStatement,FailStatement, 5);
	}

	public void ClickOnWebAppRemoveSymbol() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+WebAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Remove']")).click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/p");
		sleep(5);
		Reporter.log("Clicked on Remove Button , Navigates successfully to Remove popup",true);
	}

	public void ClickOnWebAppRemoveSymbol(String AppName) throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+AppName+"']]/following-sibling::div/div/ul/li/a[text()='Remove']")).click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		sleep(5);
		Initialization.driver.findElement(By.xpath("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")).click();
		Reporter.log("Clicked on Remove Button , Navigates successfully to Remove popup",true);
	}
	
	public void VerifyOfRemoveSummary() throws InterruptedException
	{
		String ActualValue = RemoveSummary.getText();
		String ExpectedValue = "Selected application will be removed. Do you wish to continue?";
		String PassStatement = "PASS >> " + ActualValue + " is displayed successfully when Clicked on Remove buton of the application";
		String FailStatement = "Fail >> " + ActualValue + " is not displayed even when Clicked on Remove buton of the application";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void ClickOnRemoveNoButtonAndroid() throws InterruptedException
	{
		RemoveNoButton.click();
		waitForidPresent("addNewAppBtn");
		sleep(3);
	}

	public void ClickOnRemoveYesButtonAndroid() throws InterruptedException
	{
		RemoveYesButton.click();
		waitForidPresent("addNewAppBtn");
		sleep(3);
	}

	public void VerifyOfRemoveWebApplicationAndroid_False() throws InterruptedException{
		boolean value=CheckforWebApplicationAndroid();
		String PassStatement = "PASS >> "+WebAppTitle+" is removed when Clicked on Yes Button of Remove Popup";
		String FailStatement = "Fail >> "+WebAppTitle+" is not removed even when Clicked on Yes Button of Remove Popup";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfRemoveWebApplicationAndroid_True() throws InterruptedException{
		boolean value=CheckforWebApplicationAndroid();
		String PassStatement = "PASS >> "+WebAppTitle+" is not removed when Clicked on No Button of Remove Popup";
		String FailStatement = "Fail >> "+WebAppTitle+" is removed when Clicked on No Button of Remove Popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void SearchErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'No matching result found.' is displayed successfully when Searched with invalid Device Name in Select Group popup";
		String FailStatement="Fail >> Error Message : 'No matching result found.' is not displayed when Searched with invalid Device Name in Select Group popup";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SearchErrorMeesage, true, PassStatement, FailStatement,2);
	}

	public void EnterSearchAndroid(int row,int col) throws Throwable
	{
		SearchAndroid.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
	}

	public void ClearSearchAndroid() throws Throwable
	{
		SearchAndroid.clear();
	}

	public void SearchAppNameAndroid_Partial(int row, int col) throws Throwable {
		int NoofApp = SearchAppAndroid.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppAndroid.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue = SearchAppNameAndroid.get(i).getText();
				String ExpectedValue = ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value = ActualValue.contains(ExpectedValue);
				String PassStatement = "PASS >> Partial text of App Name of the Application is Searched sucessfully in App Store";
				String FailStatement = "Fail >> Partial text of App Name of the Application is not Searched in App Store";
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	public void SearchAppNameAndroid_Full(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppAndroid.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppAndroid.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue=SearchAppNameAndroid.get(i).getText();
				String ExpectedValue=ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value=ExpectedValue.equals(ActualValue);
				String PassStatement="PASS >> App Name of the Application is Searched sucessfully in App Store";
				String FailStatement="Fail >> App Name of the Application is not Searched in App Store";
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	public void SearchCategoryAndroid_Partial(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppAndroid.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppAndroid.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue=SearchCategoryAndroid.get(i).getText();
				String ExpectedValue=ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value=ActualValue.contains(ExpectedValue);
				String PassStatement="PASS >> Partial text of Category of the Application is Searched sucessfully in App Store";
				String FailStatement="Fail >> Partial text of Category of the Application is not Searched in App Store";
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}

		}

	}

	public void SearchCategoryAndroid_Full(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppAndroid.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppAndroid.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue=SearchCategoryAndroid.get(i).getText();
				String ExpectedValue=ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value=ExpectedValue.equals(ActualValue);
				String PassStatement="PASS >> Category of the Application is Searched sucessfully in App Store";
				String FailStatement="Fail >> Category of the Application is not Searched in App Store";
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}


	//*******************//******************//*****************//*******************//**********************//********************//
	//*******************//******************//*****************//*******************//**********************//********************//
	//iOS Platform APp Store WebElements
	public static String ipaAppTitle=null;
	public static String ipaBundleId=null;
	public static String ipaVersion=null;
	public static String ipaCategory=null;
	public static String ipaDescription=null;

	public static String EditipaAppTitle=null;
	public static String EditipaBundleId=null;
	public static String EditipaVersion=null;
	public static String EditipaCategory=null;
	public static String EditipaDescription=null;

	public static String ManifestLink=null;

	@FindBy(id="iOSEAM")
	private WebElement iOSAppStore;

	@FindBy(id="iOSaddNewAppBtn")
	private WebElement AddAppiOS;

	@FindBy(xpath="//div[@id='iOSAddNewAppPopup']/div/div/div/h4")
	private WebElement AddNewPopupiOSHeader;

	@FindBy(id="iOSUploadApkMam")
	private WebElement UploadIPAiOS;

	@FindBy(id="ios_manifest_url_app")
	private WebElement ManifestLinkiOS;

	@FindBy(id="iOSSearchPlayStoreMam")
	private WebElement SearchAppStoreiOS;

	@FindBy(id="iOSUploadWebAppMam")
	private WebElement WebAppiOS;

	@FindBy(xpath="//div[@id='iOSAddNewAppPopup']/div/div/div/button[@class='close']")
	private WebElement CloseAddNewAppPopupiOS;

	@FindBy(xpath="//div[@id='iOSdropZone']/p")
	private WebElement UploadipaiOSSection;

	@FindBy(xpath="//div[@id='iOSdropZone']/button")
	private WebElement UploadipaiOSBrowseFile;

	@FindBy(xpath="//input[@id='iOSuploadAPKinputMam']/following-sibling::div/div/div/div/div[text()='Uploading']")
	private WebElement UploadAPKiOSUploading;

	@FindBy(xpath="//input[@id='iOSuploadAPKinputMam']/following-sibling::div/div/div/div/span/span")
	private WebElement UploadAPKiOSPercentage;

	@FindBy(xpath="//input[@id='iOSuploadAPKinputMam']/following-sibling::div/div/div/div/div[text()='Extracting App Details']")
	private WebElement UploadAPKiOSExtracting;

	@FindBy(xpath="//div[@id='iOSUploadAppPopup']/div/div/div/h4")
	private WebElement UploadiPAHeader;

	@FindBy(id="iOSuploadAppPopupBack")
	private WebElement UploadiPAPopupBackButton;

	@FindBy(id="iOSuploadAppPopupAdd")
	private WebElement UploadiPAPopupAddButton;

	@FindBy(id="iOSappTitleMam")
	private WebElement AppTitleiOS;

	@FindBy(id="iOSappPackageIdMam")
	private WebElement AppBundleIDiOS;

	@FindBy(id="iOSappVersionMam")
	private WebElement AppVersioniOS;

	@FindBy(id="iOSappCategoryMam")
	private WebElement AppCategoryiOS;

	@FindBy(id="iOSappDescriptionMam")
	private WebElement AppDescriptioniOS;

	@FindBy(xpath="//div[@id='iOSUploadAppPopup']/div/div/div/button[@class='close']")
	private WebElement CloseUploadipaPopupiOS;

	@FindBy(xpath="//span[contains(text(),'Error extracting ipa. Error: Corrupted zip : ')]")
	private WebElement ErrorMessageUploadDifferentPackageiOS;

	@FindBy(xpath="//span[text()='ipa has been already added.']")
	private WebElement ErrorMessageUploadSameipa;

	@FindBy(xpath="//div[@id='iOSAddNewAppPopup']/div/div/div/h4")
	private WebElement ManifestLinkHeader;

	@FindBy(xpath="//h4[text()='Manifest Link']/preceding-sibling::button")
	private WebElement ManifestLinkCloseButton;

	@FindBy(id="ios_eam_search_btn")
	private WebElement ManifestLinkSearchButton;

	@FindBy(xpath="//span[text()='Please provide a valid URL.']")
	private WebElement ManifestLinkErrorMessage_Empty;

	@FindBy(xpath="//span[text()='Invalid Manifest URL.']")
	private WebElement ManifestLinkErrorMessage_Inavlid;

	@FindBy(id="ios_eam_manifest_url_txtbox")
	private WebElement ManifestLinkTextBox;

	//Web app
	@FindBy(xpath="//div[@id='iOSAddNewAppPopup']/div/div/div/h4")
	private WebElement WebAppiOSHeader;

	@FindBy(id="iOS_webAppTitle")
	private WebElement WebAppTitleiOS;

	@FindBy(id="iOS_webAppLink")
	private WebElement WebAppLinkiOS;

	@FindBy(id="iOS_webAppCategory")
	private WebElement WebAppCategoryiOS;

	@FindBy(id="iOS_webAppDescription")
	private WebElement WebAppDescriptioniOS;

	@FindBy(xpath="//img[@id='iOS_webapp_img_preview']/following-sibling::div/input[@title='Upload Icon']")
	private WebElement WebAppImageUploadiOS;

	@FindBy(xpath="//div[@id='iOSAddNewAppPopup']/div/div/div/button")
	private WebElement WebAppCloseButtoniOS;

	@FindBy(id="ios_UploadWebAppAdd")
	private WebElement WebAppSaveButtoniOS;

	@FindBy(id="ios_UploadWebAppBack")
	private WebElement WebAppBackButtoniOS;
	//ffff
	@FindBy(id="ios_appstore_add_btn1")
	private WebElement SearchAppStoreAddButtoniOS;

	@FindBy(id="ios_eam_appstore_search_txtbox")
	private WebElement SearchAppStoreAppNameTextBox;

	@FindBy(id="ios_appstore_search_btn")
	private WebElement SearchAppStoreSearchButton;

	@FindBy(id="ios_app_type_dropdown")
	private WebElement SearchAppStoreAppTypeDropDown;

	@FindBy(id="ios_appstore_select_country_btn")
	private WebElement SearchAppStoreSelectCountryDropDown;

	@FindBy(xpath="//span[text()='Type keywords to search.']")
	private WebElement ErrorMessageSearchAppStore_Keywords;

	@FindBy(xpath="//span[text()='Please select an application.']")
	private WebElement ErrorMessageToFillDetails_SelectApplication;

	@FindBy(id="iOSsearchAppMam")
	private WebElement SearchiOS;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li")
	private List<WebElement> SearchAppiOS;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li/div/div/div/p[contains(@class,'apptitleMAM ')]")
	private List<WebElement> SearchAppNameiOS;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li/div/div/div/p[contains(@class,'categoryAppMAM')]")
	private List<WebElement> SearchCategoryiOS;



	public void ClickOniOSAppStore() throws InterruptedException
	{
		iOSAppStore.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicking On iOS App Store, Navigated to iOS App Store Page",true);
		sleep(5);
	}

	public void ClickOnAddNewAppiOS() throws InterruptedException
	{
		AddAppiOS.click();
		waitForidPresent("iOSUploadApkMam");
		Reporter.log("Clicked on Add New App Button of iOS",true);
		sleep(3);
	}

	public void VerifyOfAddNewAppPopupiOS() throws InterruptedException
	{
		String ActualValue=AddNewPopupiOSHeader.getText();
		String ExpectedValue="Select Options";
		String PassStatement = "PASS >> "+ActualValue+" for iOS Platform is displayed successfully when clicked on Add New App";
		String FailStatement = "Fail >> "+ActualValue+" for iOS Platform is not displayed when clicked on Add New App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=UploadIPAiOS.isDisplayed();
		PassStatement = "PASS >> Upload ipa for iOS Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Upload ipa for iOS Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=ManifestLinkiOS.isDisplayed();
		PassStatement = "PASS >> Manifest Link for iOS Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Manifest Link for iOS Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=SearchAppStoreiOS.isDisplayed();
		PassStatement = "PASS >> Search from App Store for iOS Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Search from App Store for iOS Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=WebAppiOS.isDisplayed();
		PassStatement = "PASS >> Web App for iOS Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Web App for iOS Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnCloseAddNewAppiOS() throws InterruptedException
	{
		CloseAddNewAppPopupiOS.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close of Add New App Button of iOS",true);
		sleep(3);
	}


	public void ClickOnRemoveNoButtoniOS() throws InterruptedException
	{
		RemoveNoButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		sleep(3);
	}

	public void VerifyOfAddNewAppPopUpiOs() throws InterruptedException
	{
		String ActualValue=AddNewPopupAndroidHeader.getText();
		String ExpectedValue="Select Options";
		String PassStatement = "PASS >> "+ActualValue+" for Android Platform is displayed successfully when clicked on Add New App";
		String FailStatement = "Fail >> "+ActualValue+" for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		ActualValue=AddNewPopupAndroidSection.getText();
		ExpectedValue="Upload APK / Enter APK Link / Web App";
		PassStatement = "PASS >> "+ActualValue+" caption for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> "+ActualValue+" for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=UploadAPKAndroid.isDisplayed();
		PassStatement = "PASS >> Upload Apk for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Upload Apk for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=UploadAPKAndroid.isDisplayed();
		PassStatement = "PASS >> APK Link for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> APK Link for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=UploadAPKAndroid.isDisplayed();
		PassStatement = "PASS >> Web App for Android Platform is displayed successfully when clicked on Add New App";
		FailStatement = "Fail >> Web App for Android Platform is not displayed when clicked on Add New App";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnUploadIPAiOS() throws InterruptedException
	{
		UploadIPAiOS.click();
		waitForidPresent("iOSdropZone");
		sleep(5);
		Reporter.log("Clicked On Upload ipa ,Navigated Succesfully to Upload ipa Popup", true);
	}
	
	public void EnterCategoryiOS(String value) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppCategoryiOS.sendKeys(value);
		ipaCategory=value;
	}
public void EnterDescriptioniOS(String value) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppDescriptioniOS.sendKeys(value);
		ipaDescription=value;
	}

	
	public void ClickOnBrowseFileiOS(String path) throws Throwable
	{
		UploadipaiOSBrowseFile.click();
		sleep(3);
		Runtime.getRuntime().exec(path);	
		sleep(5);
		try {
			while (UploadAPKiOSUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKiOSExtracting.isDisplayed()) {}
		} catch (Exception e) {}

		waitForXpathPresent("//div[@id='iOSUploadAppPopup']/div/div/div/h4");
		sleep(15);
	}



	public void VerifyOfUploadIpaPopupiOS() throws InterruptedException
	{
		String ActualValue=AddNewPopupiOSHeader.getText();
		String ExpectedValue="Upload ipa";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Upload iPA";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed even when clicked on Upload iPA";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		ActualValue=UploadipaiOSSection.getText();
		ExpectedValue="Drop files here, or select a file";
		PassStatement = "PASS >> "+ActualValue+" caption for iOS Platform is displayed successfully when clicked on Upload iPA";
		FailStatement = "Fail >> "+ActualValue+" for iOS Platform is not displayed when clicked on Upload iPA";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=UploadipaiOSBrowseFile.isDisplayed();
		PassStatement = "PASS >> Browse File is displayed successfully when clicked on Upload iPA";
		FailStatement = "Fail >> Browse File is not displayed when clicked on Upload iPA";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnBrowseFileiOS() throws Throwable
	{
		UploadipaiOSBrowseFile.click();
		sleep(3);
		Runtime.getRuntime().exec("./Uploads/UplaodFiles/AppStoe-iOS/\\File1.exe");	
		sleep(5);
		try {
			while (UploadAPKiOSUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKiOSExtracting.isDisplayed()) {}
		} catch (Exception e) {}

		waitForXpathPresent("//div[@id='iOSUploadAppPopup']/div/div/div/h4");
		sleep(15);
	}

	public void VerifyOfUploadiPAiOS() throws InterruptedException
	{
		String ActualValue=UploadiPAHeader.getText();
		String ExpectedValue="Add App";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when iPA is uploaded";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when iPA is uploaded";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleiOS,AppBundleIDiOS,AppVersioniOS};
		String[] Text={"App Title","Bundle Id","Version"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when iPA is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when iPA is uploaded as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ipaAppTitle=ActualValue; break;
			case 1: ipaBundleId=ActualValue; break;
			case 2: ipaVersion=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryiOS,AppDescriptioniOS};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text1[i]+" of the application uploaded is not visibe when iPA is uploaded";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is visibe successfully when iPA is uploaded as "+ActualValue;
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}
	public void VerifyOfUploadiPAiOS_Same() throws InterruptedException
	{
		String ActualValue=UploadiPAHeader.getText();
		String ExpectedValue="Add App";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when iPA is uploaded";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when iPA is uploaded";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleiOS,AppBundleIDiOS,AppVersioniOS};
		String[] Text={"App Title","Bundle Id","Version"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when iPA is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when iPA is uploaded as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ipaAppTitle=ActualValue; break;
			case 1: ipaBundleId=ActualValue; break;
			case 2: ipaVersion=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryiOS,AppDescriptioniOS};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text1[i]+" of the application uploaded is not visibe when iPA is uploaded";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is visibe successfully when iPA is uploaded as "+ActualValue;
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnUploadipaPopupAddButton_Error()
	{
		UploadiPAPopupAddButton.click();
	}

	public void ClickOnUploadipaPopupAddButton() throws InterruptedException
	{
		UploadiPAPopupAddButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Add Button Of Upload iPA popup, Navigates successfully to App Store Page",true);
		sleep(5);
	}

	public void ClickOnCloseUploadiPAiOS() throws InterruptedException
	{
		CloseUploadipaPopupiOS.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close of Upload IPA Popup, Navigates successfully to App Store page",true);
		sleep(3);
	}

	public void EnterCategoryiOS() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppCategoryiOS.sendKeys("SureFox");
		ipaCategory="SureFox";
	}

	public void EnterDescriptioniOS() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		AppDescriptioniOS.sendKeys("SureFox Lockdown");
		ipaDescription="SureFox Lockdown";
	}

	public void ClearCategoryiOS()
	{
		AppCategoryiOS.clear();
	}

	public void ClearDescriptioniOS() 
	{
		AppDescriptioniOS.clear();
	}

	public boolean CheckforApplicationiOS() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + ipaAppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void VerifyOfApplicationiOS_False() throws InterruptedException{
		boolean value=CheckforApplicationiOS();
		String PassStatement = "PASS >> "+ipaAppTitle+" is not added when Clicked on Back Button of Upload iPA";
		String FailStatement = "Fail >> "+ipaAppTitle+" is added even when Clicked on Back Button of Upload iPA";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfApplicationiOS_True() throws InterruptedException{
		boolean value=CheckforApplicationiOS();
		String PassStatement = "PASS >> "+ipaAppTitle+" is added when Clicked on Add Button of Upload iPA";
		String FailStatement = "Fail >> "+ipaAppTitle+" is not added even when Clicked on Add Button of Upload iPA";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void ClickOnUploadipaPopupBackButton() throws InterruptedException
	{
		UploadiPAPopupBackButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Back Button Of Upload ipa popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnAppDetailsiOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("(//div[div[div[p[text()='SureFox']]]]/preceding-sibling::div/img)[last()]")).click();
		waitForidPresent("editBtn");
		sleep(5);
		Reporter.log("Clicked on Image Of Application, Navigates successfully to App Details Page",true);
	}

	public void ClickOnEditInAppDetailsiOS() throws InterruptedException{
		AppDetailsEditButton.click();
		waitForXpathPresent("//div[@id='editAppPopup']/div/div/div/div/div/input[@id='appTitleMam']");
		sleep(5);
		Reporter.log("Clicked on Edit Button, Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyOfAppInAppDetailsiOS() throws InterruptedException{
		WebElement[] wb={AppDetailsAppTitleAndroid,AppDetailsVersionAndroid,AppDetailsCategoryAndroid,AppDetailsDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaVersion,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Version","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getText();
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when ipa is added in App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when ipa is added in App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnCloseOfAppDetailsiOS() throws InterruptedException
	{
		AppDetailsClose.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void VerifyOfAppInEditAppDetailsiOS() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppPackageAndroid,EditAppVersionAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaBundleId,ipaVersion,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Package Id","Version","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through Upload ipa";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through Upload ipa";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnMoreSymboliOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("(//div[p[text()='"+ipaAppTitle+"']]/following-sibling::div/div/span)[last()]")).click();
		waitForXpathPresent("(//div[p[text()='"+ipaAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit'])[last()]");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnEditSymboliOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("(//div[p[text()='"+ipaAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit'])[last()]")).click();
		waitForXpathPresent("//div[@id='editAppPopup']/div/div/div/div/div/input[@id='appTitleMam']");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void ClickOnCloseOfEditAppDetailsiOS() throws InterruptedException
	{
		CloseEditAppDetails.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void EnterEditAppCategoryiOS() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppCategoryAndroid.sendKeys("SureFox kiosk LockDown");
		EditipaCategory="SureFox kiosk LockDown";
	}

	public void EnterEditAppDescriptioniOS() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppDescriptionAndroid.sendKeys("Be alone to be Happy ..Be Happy to be alone");
		EditipaDescription="Be alone to be Happy ..Be Happy to be alone";
	}

	public void EnterEditAppTitleiOS() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppTitleAndroid.sendKeys("SureFox iOS");
		EditipaAppTitle="SureFox iOS";
	}


	public void ClearEditAppTitleiOS()
	{
		EditAppTitleAndroid.clear();
	}

	public void ClearEditAppCategoryiOS()
	{
		EditAppCategoryAndroid.clear();
	}

	public void ClearEditAppDescriptioniOS() 
	{
		EditAppDescriptionAndroid.clear();
	}

	public void ClickOnCancelButtonOfEditAppDetailsiOS() throws InterruptedException
	{
		EditAppDetailsCancelButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Cancel Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfEditAppDetailsiOS() throws InterruptedException
	{
		EditAppDetailsSaveButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Save Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public boolean CheckforEditedApplicationiOS() throws InterruptedException
	{
		boolean value=true;
		try {
			Initialization.driver.findElement(By.xpath("//p[text()='" + EditipaAppTitle + "']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;

	}

	public void VerifyOfEditApplicationiOS_False() throws InterruptedException{
		System.out.println(EditipaAppTitle);
		boolean value=CheckforEditedApplicationiOS();
		String PassStatement = "PASS >> App Title is not edited when Clicked on Cancel Button of Edit App Details Page";
		String FailStatement = "Fail >> App Title is edited even when Clicked on Cancel Button of Edit App Details Page";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfEditApplicationiOS_True() throws InterruptedException{
		boolean value=CheckforEditedApplicationiOS();
		String PassStatement = "PASS >> "+ipaAppTitle+" is edited successfully as "+EditipaAppTitle+" when Clicked on Save Button of Edit App Details Page";
		String FailStatement = "Fail >> "+ipaAppTitle+" is not edited as "+EditipaAppTitle+" even when Clicked on Save Button of Edit App Details Page";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfEditedAppInEditAppDetailsiOS_Cancel() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is not edited successfully when Clicked on Cancel Button of Edit App Details Page";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is edited even when Clicked on Cancel Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnEditedAppMoreSymboliOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditipaAppTitle+"']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='"+EditipaAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	public void ClickOnEditedAppEditSymboliOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+EditipaAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}

	public void VerifyOfEditedAppInEditAppDetailsiOS_Save() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={EditipaAppTitle,EditipaCategory,EditipaDescription};
		String[] AppText1={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> "+AppText1[i]+" of the application is edited successfully as "+Text[i]+" when Clicked on Save Button of Edit App Details Page";
			String FailStatement = "Fail >> "+AppText1[i]+" of the application is not edited as "+Text[i]+" even when Clicked on Save Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ErrorMessageUploadDifferentPackageiOS() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Error extracting ipa. Error: Corrupted zip : can't find end of central directory' is displayed successfully when Different package is uploaded in Edit App Details";
		String FailStatement = "Fail >> Alert Message : 'Error extracting ipa. Error: Corrupted zip : can't find end of central directory' is not displayed even when Different package is uploaded in Edit App Details";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageUploadDifferentPackageiOS, true, PassStatement,FailStatement, 5);
	}

	public void ClickOnEditUploadAPkiOS_SamePackage(int row,int col) throws Throwable
	{
		EditUploadAPk.click();
		sleep(3);
		Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet19",row ,col));
		sleep(5);
		try {
			while (UploadAPKiOSUploading.isDisplayed()) {}
		} catch (Exception e) {}
		try {
			while (UploadAPKiOSExtracting.isDisplayed()) {}
		} catch (Exception e) {}

		waitForXpathPresent("//div[@id='UploadAPKPopup']/div/div/div/h4");
		waitForXpathPresent("//div[@id='UploadAPKPopup']/div/div/div/h4");
		sleep(15);
	}

	public void VerifyOfEditUploadipaiOS() throws InterruptedException
	{
		String ActualValue=UploadAPKHeader.getText();
		String ExpectedValue="Upload APK";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Apk is uploaded";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Apk is uploaded";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppVersionAndroid,AppVersionCodeAndroid};
		String[] Text={"Version","Version Code"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application updated succesffully when Same Package Apk is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application is not updated even when Same Package Apk is uploaded as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ipaVersion=ActualValue; break;
			}
		}
		WebElement[] wb1={AppTitleAndroid,AppPackageAndroid,AppCategoryAndroid,AppDescriptionAndroid};
		String[] Text1={"App Title","Package","Category","Description"};

		String[] ExpectedText={ipaAppTitle,ipaBundleId,EditipaCategory,EditipaDescription};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			ExpectedValue=ExpectedText[i];
			PassStatement = "PASS >> App "+Text1[i]+" of the application updated succesffully when Same Package Apk is uploaded as "+ActualValue;
			FailStatement = "Fail >> App "+Text1[i]+" of the application is not updated even when Same Package Apk is uploaded as "+ActualValue;
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		}
	}

	public void VerifyOfAppInEditAppDetailsiOS_SamePackage() throws InterruptedException{
		WebElement[] wb={EditAppTitleAndroid,EditAppPackageAndroid,EditAppVersionAndroid,EditAppVersionCodeAndroid,EditAppCategoryAndroid,EditAppDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaBundleId,ipaVersion,ipaVersion,EditipaCategory,EditipaDescription};
		String[] AppText={"App Title","Package","Version","Version Code","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Apk is added in Edit App Details Popup";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Apk is added in Edit App Details Popup";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ErrorMessageUploadSameipa() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'ipa has been already added' is displayed successfully when Same APK is uploaded again";
		String FailStatement = "Fail >> Alert Message : 'ipa has been already added' is not displayed even when Same APK is uploaded again";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageUploadSameipa, true, PassStatement,FailStatement, 5);
	}


	public void ClickOnUploadapkPopupAddButton() throws InterruptedException
	{
		UploadApkPopupAddButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Add Button Of Upload iPA popup, Navigates successfully to App Store Page",true);
		sleep(30);
	}

	public void ClickOnUploadapkPopupAdd() throws InterruptedException
	{    sleep(20);
		UploadApkPopupAddButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Add Button Of Upload iPA popup, Navigates successfully to App Store Page",true);
	}
	//Manifest Link

	public void ClickOnManifestLinkiOS() throws InterruptedException
	{
		ManifestLinkiOS.click();
		waitForidPresent("ios_eam_manifest_url_txtbox");
		sleep(2);
		Reporter.log("Clicked On Manifest Link ,Navigated Succesfully to Upload Apk Popup", true);
	}

	public void VerifyOfManifestLinkPopupiOS() throws InterruptedException
	{
		String ActualValue=ManifestLinkHeader.getText();
		String ExpectedValue="Manifest Link";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Manifest Link";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed when clicked on Manifest Link";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void ClickOnCloseOfManifestLink() throws InterruptedException
	{
		ManifestLinkCloseButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of Manifest Link popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnManifestLinkPopupSearchButton_Error()
	{
		ManifestLinkSearchButton.click();
	}

	public void ClickOnManifestLinkPopupSearchButton() throws Throwable
	{
		ManifestLinkSearchButton.click();
		waitForXpathPresent("//div[@id='iOSUploadAppPopup']/div/div/div/h4");
		Reporter.log("Clicked on Search button Of Manifest Link popup, Navigates successfully to Add App Page",true);
		sleep(6);
	}

	public void ManifestLinkErrorMessage_Empty() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Please provide a valid URL.' is displayed successfully when Manifest Link is not mentioned";
		String FailStatement = "Fail >> Alert Message : 'Please provide a valid URL.' is not displayed even when Manifest Link is not mentioned";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ManifestLinkErrorMessage_Empty, true, PassStatement,FailStatement, 5);
	}

	public void ManifestLinkErrorMessage_Invalid() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Invalid Manifest URL.' is displayed successfully when Manifest Link is a invalid Link ";
		String FailStatement = "Fail >> Alert Message : 'Invalid Manifest URL.' is not displayed even when Manifest Link is a invalid Link";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ManifestLinkErrorMessage_Inavlid, true, PassStatement,FailStatement, 5);
	}

	public void EnterManifestLink_Invalid() throws Throwable{
		ManifestLinkTextBox.sendKeys("aaa");
	}

	public void EnterManifestLink(int row,int col) throws Throwable{
		ManifestLinkTextBox.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		ManifestLink=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void VerifyOfManifestLinkiOS() throws InterruptedException
	{
		String ActualValue=UploadiPAHeader.getText();
		String ExpectedValue="Add App";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Manifest Link is added";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Manifest Link is added";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleiOS,AppBundleIDiOS,AppVersioniOS};
		String[] Text={"App Title","Bundle Id","Version"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when Manifest Link is added";
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when Manifest Link is added";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ipaAppTitle=ActualValue; break;
			case 1: ipaBundleId=ActualValue; break;
			case 2: ipaVersion=ActualValue; break;
			}
		}
		WebElement[] wb1={AppCategoryiOS,AppDescriptioniOS};
		String[] Text1={"Category","Description"};
		for(int i=0;i<wb1.length;i++)
		{
			ActualValue = wb1[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text1[i]+" of the application uploaded is not visibe when Manifest Link is added";
			FailStatement = "Fail >> App "+Text1[i]+" of the application uploaded is visibe successfully when Manifest Link is added";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfAppInEditAppLinkDetailsiOS() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkBundleIdAndroid,EditAppLinkVersionAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaBundleId,ipaVersion,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Bundle Id","Version","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through Manifest Link";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through Manifest Link";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void EnterEditAppLinkCategoryiOS(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkCategoryAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditipaCategory=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppLinkDescriptioniOS(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkDescriptionAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditipaDescription=ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void EnterEditAppLinkTitleiOS(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EditAppLinkTitleAndroid.sendKeys(ELib.getDatafromExcel("Sheet19",row,col));
		EditipaAppTitle = ELib.getDatafromExcel("Sheet19",row,col);
	}

	public void ClearEditAppLinkTitleiOS()
	{
		EditAppLinkTitleAndroid.clear();
	}

	public void ClearEditAppLinkCategoryiOS()
	{
		EditAppLinkCategoryAndroid.clear();
	}

	public void ClearEditAppLinkDescriptioniOS() 
	{
		EditAppLinkDescriptionAndroid.clear();
	}

	public void VerifyOfEditedAppInEditAppLinkDetailsiOS_Cancel() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is not edited successfully when Clicked on Cancel Button of Edit App Details Page";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is edited even when Clicked on Cancel Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void VerifyOfEditedAppInEditAppLinkDetailsiOS_Save() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={EditipaAppTitle,EditipaCategory,EditipaDescription};
		String[] AppText1={"App Title","Category","Description"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> "+AppText1[i]+" of the application is edited successfully as "+Text[i]+" when Clicked on Save Button of Edit App Details Page";
			String FailStatement = "Fail >> "+AppText1[i]+" of the application is not edited as "+Text[i]+" even when Clicked on Save Button of Edit App Details Page";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void ClickOnSaveButtonOfEditAppLinkDetailsiOS() throws InterruptedException
	{
		EditAppLinkDetailsSaveButton.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Save Button Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnCloseOfEditAppLinkDetailsiOS() throws InterruptedException
	{
		CloseEditAppLinkDetails.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of Edit App Details popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	//Web App iOS

	public void ClickOnWebAppiOS() throws InterruptedException
	{
		WebAppiOS.click();
		waitForidPresent("iOS_webAppTitle");
		sleep(2);
		Reporter.log("Clicked On Web App,Navigated Succesfully to Web App Popup", true);
	}

	public void VerifyOfWebAppPopupiOS() throws InterruptedException
	{
		String ActualValue=WebAppiOSHeader.getText();
		String ExpectedValue="Upload Web App";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Web App";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed when clicked on Web App";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void EnterWebAppTitleiOS(int row,int col) throws Throwable{
		WebAppTitleiOS.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppTitle=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppLinkiOS(int row,int col) throws Throwable{
		WebAppLinkiOS.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppLink=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppCategoryiOS(int row,int col) throws Throwable{
		WebAppCategoryiOS.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppCategory=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void EnterWebAppDescriptioniOS(int row,int col) throws Throwable{
		WebAppDescriptioniOS.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
		WebAppDescription=ELib.getDatafromExcel("Sheet19", row, col);
	}

	public void ClearWebAppTitleiOS() throws Throwable{
		WebAppTitleiOS.clear();
	}

	public void ClearWebAppLinkiOS() throws Throwable{
		WebAppLinkiOS.clear();
	}

	public void ClearWebAppCategoryiOS() throws Throwable{
		WebAppCategoryiOS.clear();
	}

	public void ClearWebAppDescriptioniOS() throws Throwable{
		WebAppDescriptioniOS.clear();
	}

	public void ClickOnCloseButtonOfWebAppiOS() throws InterruptedException
	{
		WebAppCloseButtoniOS.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of Web App popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnSaveButtonOfWebAppiOS_Error() throws InterruptedException
	{
		WebAppSaveButtoniOS.click();

	}

	public void ClickOnSaveButtonOfWebAppiOS() throws InterruptedException
	{
		WebAppSaveButtoniOS.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Save Button Of Web App popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnBackButtonOfWebAppiOS() throws InterruptedException
	{
		WebAppBackButtoniOS.click();
		waitForidPresent("ios_manifest_url_app");
		boolean value=true;
		try{
			AddNewPopupiOSHeader.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		String PassStatement = "PASS >> Web App Details is not saved when clicked on Back Button of Web App popup";
		String FailStatement = "Fail >> Web App Details is saved even when clicked on Back Button of Web App popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("Clicked on Back Button Of Web App popup, Navigates successfully to Select Options Popup",true);
		sleep(3);
	}

	public void ClickOnImageFieldWebAppiOS(int row,int col) throws Throwable{
		WebAppImageUploadiOS.click();
		sleep(3);
		Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet19", row, col));
		sleep(1);
	}

	//Search from App Store
	//SearchAppStore
	public void ClickOnSearchFromAppStore() throws Throwable
	{
		SearchAppStoreiOS.click();
		waitForidPresent("ios_eam_appstore_search_txtbox");
		sleep(2);
		Reporter.log("Clicked On Search from App Store,Navigated Succesfully to Search from App Store Popup", true);
	}

	public void VerifyOfSearchAppStorePopupiOS() throws InterruptedException
	{
		String ActualValue=WebAppiOSHeader.getText();
		String ExpectedValue="Search Apps";
		String PassStatement = "PASS >> "+ActualValue+" Header is displayed successfully when clicked on Search from App Store";
		String FailStatement = "Fail >> "+ActualValue+" Header is not displayed when clicked on Search from App Store";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}

	public void ClickOnCloseButtonOfSearchAppStoreiOS() throws InterruptedException
	{
		WebAppCloseButtoniOS.click();
		waitForidPresent("iOSaddNewAppBtn");
		Reporter.log("Clicked on Close Of Search from App Store popup, Navigates successfully to App Store Page",true);
		sleep(3);
	}

	public void ClickOnAddButtonOfSearchAppStoreiOS_Error() throws InterruptedException
	{
		SearchAppStoreAddButtoniOS.click();

	}

	public void ClickOnAddButtonOfSearchAppStoreiOS() throws InterruptedException
	{
		SearchAppStoreAddButtoniOS.click();
		waitForidPresent("iOSappTitleMam");
		Reporter.log("Clicked on Add Button Of Search from App Store popup, Navigates successfully to Add App Page",true);
		sleep(3);
	}

	public void ErrorMessageSearchAppStore_Keywords() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Type keywords to search.' is displayed successfully when App Name is not entered and searched";
		String FailStatement = "Fail >> Alert Message : 'Type keywords to search.' is not displayed when App Name is not entered and searched";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageSearchAppStore_Keywords, true, PassStatement,FailStatement, 5);
	}

	public void ErrorMessageToFillDetails_SelectApplication() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Please select an application.' is displayed successfully when application is not selected";
		String FailStatement = "Fail >> Alert Message : 'Please select an application.' is not displayed even when application is not selected";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageToFillDetails_SelectApplication, true, PassStatement,FailStatement, 5);
	}

	public void ClickOnSearchButtonOfSearchAppStore() throws Throwable
	{
		SearchAppStoreSearchButton.click();
		sleep(3);
	}

	public void EnterAppNameSearchAppStore(int row,int col) throws  Throwable
	{
		SearchAppStoreAppNameTextBox.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
	}

	public void VerifyOfApplicationinSearchAppStore(int row,int col) throws  Throwable{

		String[] AppText={"Price","Version","Created By","Rating"};
		List<WebElement> Element=Initialization.driver.findElements(By.xpath("//table[@id='ios_eam_appstore_search_result']/tbody/tr/td[text()='"+ELib.getDatafromExcel("Sheet19", row-1, col)+"']/following-sibling::td"));
		for(int i=0;i<Element.size();i++)
		{
			String ActualValue = Element.get(i).getText();
			String ExpectedValue=ELib.getDatafromExcel("Sheet19", row+i, col);
			String PassStatement = "PASS >> "+AppText[i]+" of the application is displayed successfully under App Store Application as "+ActualValue;
			String FailStatement = "Fail >> "+AppText[i]+" of the application is not displayed under App Store Application as "+ActualValue;
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
		sleep(4);
		Initialization.driver.findElement(By.xpath("//table[@id='ios_eam_appstore_search_result']/tbody/tr/td[text()='"+ELib.getDatafromExcel("Sheet19", row-1, col)+"']")).click();
		Reporter.log("Clicked on Application under App Store Application",true);
	}

	public void VerifyOfAddAppfromSearchAppStore_Initial() throws InterruptedException
	{
		String ActualValue=UploadiPAHeader.getText();
		String ExpectedValue="Add App";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Application is added from Search From App Store ";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Application is added from Search From App Store";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleiOS,AppCategoryiOS,AppBundleIDiOS,AppVersioniOS,AppDescriptioniOS};
		String[] Text={"App Title","Category","Bundle Id","Version","Description"};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = (ActualValue != null && !ActualValue.isEmpty());
			PassStatement = "PASS >> App "+Text[i]+" of the application uploaded is visibe successfully when Application is added from Search From App Store as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application uploaded is not visibe when Application is added from Search From App Store as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			switch(i)
			{
			case 0: ipaAppTitle=ActualValue; break;
			case 1: ipaCategory=ActualValue; break;
			case 2: ipaBundleId=ActualValue; break;
			case 3: ipaVersion=ActualValue; break;
			case 4: ipaDescription=ActualValue; break;
			}
		}
	}

	public void VerifyOfAddAppfromSearchAppStore() throws InterruptedException
	{
		String ActualValue=UploadiPAHeader.getText();
		String ExpectedValue="Add App";
		String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Application is added from Search From App Store ";
		String FailStatement = "Fail >> "+ActualValue+" is not displayed when Application is added from Search From App Store";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		WebElement[] wb={AppTitleiOS,AppCategoryiOS,AppBundleIDiOS,AppVersioniOS,AppDescriptioniOS};
		String[] Text={"App Title","Category","Bundle Id","Version","Description"};
		String[] ExpectedValues={ipaAppTitle,ipaCategory,ipaBundleId,ipaVersion,ipaDescription};
		for(int i=0;i<wb.length;i++)
		{
			ActualValue = wb[i].getAttribute("value");
			boolean value = ActualValue.equals(ExpectedValues[i]);
			PassStatement = "PASS >> App "+Text[i]+" of the application added is visibe successfully when Application is added from Search From App Store as "+ActualValue;
			FailStatement = "Fail >> App "+Text[i]+" of the application added is not visibe when Application is added from Search From App Store as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfAppInAppDetailsiOS_SearchAppStore() throws InterruptedException{
		WebElement[] wb={AppDetailsAppTitleAndroid,AppDetailsVersionAndroid,AppDetailsCategoryAndroid};
		String[] Text={ipaAppTitle,ipaVersion,ipaCategory};
		String[] AppText={"App Title","Version","Category"};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getText();
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully when Application is added from Search From App Store";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved when Application is added from Search From App Store";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void VerifyOfAppInEditAppLinkDetailsiOS_SearchAppStore() throws InterruptedException{
		WebElement[] wb={EditAppLinkTitleAndroid,EditAppLinkBundleIdAndroid,EditAppLinkVersionAndroid,EditAppLinkCategoryAndroid,EditAppLinkDescriptionAndroid};
		String[] Text={ipaAppTitle,ipaBundleId,ipaVersion,ipaCategory,ipaDescription};
		String[] AppText={"App Title","Bundle Id","Version","Category","Description",};
		for(int i=0;i<wb.length;i++)
		{
			String ActualValue = wb[i].getAttribute("value");
			String ExpectedValue=Text[i];
			String PassStatement = "PASS >> App "+AppText[i]+" of the application is saved successfully in Edit App Details Popup when appliaction is added through Search From App Store";
			String FailStatement = "Fail >> App "+AppText[i]+" of the application is not saved in Edit App Details Popup when appliaction is added through Search From App Store";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
		}
	}

	public void SearchAppStoreAppTypeDropDown()
	{
		Select sel=new Select(SearchAppStoreAppTypeDropDown);
		sel.selectByVisibleText("iPad Only");
	}

	public void SearchAppStoreSelectCountryDropDown()
	{
		Select sel=new Select(SearchAppStoreSelectCountryDropDown);
		sel.selectByValue("in");
	}

	public void ClickOnAppRemoveSymbol_iOS() throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+ipaAppTitle+"']]/following-sibling::div/div/ul/li/a[text()='Remove']")).click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/p");
		sleep(5);
		Reporter.log("Clicked on Remove Button , Navigates successfully to Remove popup",true);
	}

	public void VerifyOfRemoveApplicationiOS_False() throws InterruptedException{
		boolean value=CheckforApplicationiOS();
		String PassStatement = "PASS >> "+ipaAppTitle+" is removed when Clicked on Yes Button of Remove Popup";
		String FailStatement = "Fail >> "+ipaAppTitle+" is not removed even when Clicked on Yes Button of Remove Popup";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void VerifyOfRemoveApplicationiOS_True() throws InterruptedException{
		boolean value=CheckforApplicationiOS();
		String PassStatement = "PASS >> "+ipaAppTitle+" is not removed when Clicked on No Button of Remove Popup";
		String FailStatement = "Fail >> "+ipaAppTitle+" is removed when Clicked on No Button of Remove Popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	public void EnterSearchiOS(int row,int col) throws Throwable
	{
		SearchiOS.sendKeys(ELib.getDatafromExcel("Sheet19", row, col));
	}

	public void ClearSearchiOS() throws Throwable
	{
		SearchiOS.clear();
	}

	public void SearchAppNameiOS_Partial(int row, int col) throws Throwable {
		int NoofApp = SearchAppiOS.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppiOS.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue = SearchAppNameiOS.get(i).getText();
				String ExpectedValue = ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value = ActualValue.contains(ExpectedValue);
				String PassStatement = "PASS >> Partial text of App Name of the Application is Searched sucessfully in App Store "+ActualValue;
				String FailStatement = "Fail >> Partial text of App Name of the Application is not Searched in App Store "+ActualValue;
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	public void SearchAppNameiOS_Full(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppiOS.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppiOS.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue = SearchAppNameiOS.get(i).getText();
				String ExpectedValue = ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value = ExpectedValue.equals(ActualValue);
				String PassStatement = "PASS >> App Name of the Application is Searched sucessfully in App Store "+ActualValue;
				String FailStatement = "Fail >> App Name of the Application is not Searched in App Store "+ActualValue;
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	public void SearchCategoryiOS_Partial(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppiOS.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppiOS.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue = SearchCategoryiOS.get(i).getText();
				String ExpectedValue = ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value = ActualValue.contains(ExpectedValue);
				String PassStatement = "PASS >> Partial text of Category of the Application is Searched sucessfully in App Store "+ActualValue;
				String FailStatement = "Fail >> Partial text of Category of the Application is not Searched in App Store "+ActualValue;
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	public void SearchCategoryiOS_Full(int row,int col) throws Throwable
	{
		int NoofApp = SearchAppiOS.size();
		for (int i = 0; i < NoofApp; i++) {
			String Attribute = SearchAppiOS.get(i).getAttribute("style");
			if (Attribute.contains("list")) {
				String ActualValue = SearchCategoryiOS.get(i).getText();
				String ExpectedValue = ELib.getDatafromExcel("Sheet19", row, col);
				boolean Value = ExpectedValue.equals(ActualValue);
				String PassStatement = "PASS >> Category of the Application is Searched sucessfully in App Store "+ActualValue;
				String FailStatement = "Fail >> Category of the Application is not Searched in App Store "+ActualValue;
				ALib.AssertTrueMethod(Value, PassStatement, FailStatement);
			}
		}

	}

	//User Management related
	@FindBy(xpath="//ul[@class='applistMAM']/li[1]/div/div/div/div/span/i")
	private WebElement FirstAppAndroidMoreSymbol;

	@FindBy(xpath="//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']")
	private WebElement FirstAppAndroidEditButtonOfMore;

	@FindBy(xpath="//ul[@class='applistMAM']/li[1]/div/img")
	private WebElement FirstAppAndroidImage;

	@FindBy(xpath="//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Remove']")
	private WebElement FirstAppAndroidRemoveButtonOfMore;

	//ios
	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li[1]/div/div/div/div/span/i")
	private WebElement FirstAppiOSMoreSymbol;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']")
	private WebElement FirstAppiOSEditButtonOfMore;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li[1]/div/img")
	private WebElement FirstAppiOSImage;

	@FindBy(xpath="//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Remove']")
	private WebElement FirstAppiOSRemoveButtonOfMore;


	//Android
	public boolean CheckAppStoreBtn()
	{
		boolean value = true;
		try{
			AppStore.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void ClickOnAddNewAppAndroid_UM()
	{
		AddApp.click();
	}

	public void ClickOnImageAndroid() throws InterruptedException
	{
		FirstAppAndroidImage.click();
		waitForidPresent("editBtn");
		sleep(3);
		Expected = AppVersion.getAttribute("style");
		AppDetailsEditButton.click();
		if (Expected.contains("none")) {
			waitForidPresent("appEditWebAppTitle");
			sleep(3);
			CloseEditWebAppDetails.click();

		} else {
			waitForXpathPresent("//div[@id='editAppPopup']/div/div/div/h4");
			sleep(3);
			CloseEditAppDetails.click();

		}
		waitForidPresent("addNewAppBtn");
		sleep(3);
	}

	public void ClickOnEditMoreAndroid_UM() throws InterruptedException
	{
		FirstAppAndroidMoreSymbol.click();
		waitForXpathPresent("//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppAndroidEditButtonOfMore.click();
	}

	public void ClickOnImageAndroid_UM() throws InterruptedException
	{
		FirstAppAndroidImage.click();
		waitForidPresent("editBtn");
		sleep(3);
		AppDetailsEditButton.click();
	}



	public void ClickOnEditMoreAndroid() throws InterruptedException
	{
		FirstAppAndroidMoreSymbol.click();
		waitForXpathPresent("//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppAndroidEditButtonOfMore.click();
		if (Expected.contains("none")) {
			waitForidPresent("appEditWebAppTitle");
			sleep(3);
			CloseEditWebAppDetails.click();

		} else {
			waitForXpathPresent("//div[@id='editAppPopup']/div/div/div/h4");
			sleep(3);
			CloseEditAppDetails.click();

		}
		waitForidPresent("addNewAppBtn");
		sleep(3);
	}

	public void ClickOnRemoveMoreAndroid_UM() throws InterruptedException
	{
		FirstAppAndroidMoreSymbol.click();
		waitForXpathPresent("//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppAndroidRemoveButtonOfMore.click();
	}

	public void ClickOnRemoveMoreAndroid() throws InterruptedException
	{
		FirstAppAndroidMoreSymbol.click();
		waitForXpathPresent("//ul[@class='applistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppAndroidRemoveButtonOfMore.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='No']");
		sleep(2);
	}

	//ios
	public void ClickOnAddNewAppiOS_UM()
	{
		AddAppiOS.click();
	}

	public void ClickOnImageiOS_UM() throws InterruptedException
	{
		FirstAppiOSImage.click();
		waitForidPresent("editBtn");
		sleep(3);
		AppDetailsEditButton.click();

	}

	public void ClickOnImageiOS() throws InterruptedException
	{
		FirstAppiOSImage.click();
		waitForidPresent("editBtn");
		sleep(3);
		Expected = AppVersion.getAttribute("style");
		AppDetailsEditButton.click();
		if (Expected.contains("none")) {
			waitForidPresent("appEditWebAppTitle");
			sleep(3);
			CloseEditWebAppDetails.click();

		} else {
			waitForXpathPresent("//div[@id='editAppPopup']/div/div/div/h4");
			sleep(3);
			CloseEditAppLinkDetails.click();

		}
		waitForidPresent("iOSaddNewAppBtn");
		sleep(3);
	}

	public void ClickOnEditMoreiOS_UM() throws InterruptedException
	{
		FirstAppiOSMoreSymbol.click();
		waitForXpathPresent("//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppiOSEditButtonOfMore.click();
	}

	public void ClickOnEditMoreiOS() throws InterruptedException
	{
		FirstAppiOSMoreSymbol.click();
		waitForXpathPresent("//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppiOSEditButtonOfMore.click();
		if (Expected.contains("none")) {
			waitForidPresent("appEditWebAppTitle");
			sleep(3);
			CloseEditWebAppDetails.click();

		} else {
			waitForXpathPresent("//div[@id='editAppLinkPopup']/div/div/div/h4");
			sleep(3);
			CloseEditAppLinkDetails.click();

		}
		waitForidPresent("iOSaddNewAppBtn");
		sleep(3);
	}

	public void ClickOnRemoveMoreiOS_UM() throws InterruptedException
	{
		FirstAppiOSMoreSymbol.click();
		waitForXpathPresent("//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppiOSRemoveButtonOfMore.click();
	}

	public void ClickOnRemoveMoreiOS() throws InterruptedException
	{
		FirstAppiOSMoreSymbol.click();
		waitForXpathPresent("//ul[@class='iOSapplistMAM']/li[1]/div[2]/div/div/div/ul/li/a[text()='Edit']");
		sleep(3);
		FirstAppiOSRemoveButtonOfMore.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='No']");
		sleep(2);
	}

	public void ClickOnMoreSymbolLite() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[p[text()='Lite']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='Lite']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	@FindBy(xpath="//div[p[text()='Lite']]/following-sibling::div/div/ul/li/a[text()='Remove']")
	private WebElement RemoveOptionlite;

	public void ClickOnRemoveOption() throws InterruptedException {
		RemoveOptionlite.click();
		sleep(2);
	}

	public void ClickOnMoreSymbolSIM() throws InterruptedException{

		Initialization.driver.findElement(By.xpath("//div[p[text()='simnumberchanger']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='simnumberchanger']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(3);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove",true);
	}

	@FindBy(xpath="//div[p[text()='simnumberchanger']]/following-sibling::div/div/ul/li/a[text()='Remove']")
	private WebElement RemoveOption;

	public void ClickOnRemove() throws InterruptedException {
		RemoveOption.click();
		sleep(3);
	}

	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement DialogueYES;

	public void ClickOnYESdialoguepopUp() throws InterruptedException {
		DialogueYES.click();
		sleep(9);
	}

	@FindBy(xpath="//span[text()='App Added Successfully.']")
	private WebElement ConfirmationMessageOnAddingAppAPK;


	public void VerifyConformationMessageOnAddingApp() throws InterruptedException{
		boolean value =true;
		try{
			ConfirmationMessageOnAddingAppAPK.isDisplayed();

		}catch(Exception e)
		{
			value=false;

		}
		Assert.assertTrue(value,"FAIL >> No confirmation message displayed");
		Reporter.log("PASS >> Confirmation message  'App Added Successfully.'  is displayed",true );	
	sleep(2);
	}










	/************************APPIUM PART************************/

	public void LaunchAppStore() throws InterruptedException, IOException    
	{            
		System.out.println("\nTest Case 0 : Launching App Store");                 
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");// to launch FileStore on Device                       
		sleep(5);                  
	}   

	public void LaunchNix() throws InterruptedException, IOException    
	{                          
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm");                       
		sleep(5);                  
	}     

	public void isWebAppPresent() throws InterruptedException {
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ESFileExplorer']").isDisplayed();
		String pass="PASS>> 'WebApp' is present"+apk+"";
		String fail="FAIL>> 'WebApp' is present"+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
		Initialization.driverAppium.findElementById("com.nix:id/linear_layout").click();
		sleep(10);
	}

	public void IS_MSGdisplayed() {
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='No Applications available!' and @index='1']").isDisplayed();
		String pass="PASS>> 'No Apps Available' message displayed"+apk+"";
		String fail="FAIL>> 'No Apps Available' message displayed"+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
	}

	public void IsHomeTabDisplayed() 
	{
		boolean apk= Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Home' and @index='0']").isDisplayed();
		String pass="PASS>> 'Home Tab'is present "+apk+"";
		String fail="FAIL>> 'Home Tab'is present "+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
	}


	public void IsStoreTabDisplayed() {	
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Store' and @index='0']").isDisplayed();
		String pass="PASS>> 'Store Tab'is present "+apk+"";
		String fail="FAIL>> 'Store Tab'is present "+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
	}

	public void  ClickOnStoreTab() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Store' and @index='0']").click();
		sleep(6);
	}

	public void  IsUpdateButtttonPresent() throws InterruptedException {

		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Update' and @index='0']").isDisplayed();
		String pass="PASS>> 'UpdateButttton'is present "+apk+"";
		String fail="FAIL>> 'UpdateButttton'is present "+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);  
	}

	public void IsInstallButtonPresent() {
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Install' and @index='0']").isDisplayed();
		String pass="PASS>> 'InstallButton'is present "+apk+"";
		String fail="FAIL>> 'InstallButton'is present "+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
	}

	public void ClickOnInstall() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Install' and @index='0']").click();

	}

	public void ClickOnNextButtonDeviceSide() {

		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,1800);

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Next' and @index='1']").click();  

	}

	public void ClickOnInstallButton() {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Install' and @index='1']").click();
	}

	@FindBy(xpath="//*[@id='profileList_tableCont']/div/div[2]/div[1]/div[2]/div[1]/div/input")
	private WebElement profileListSearchTextField;
	public void SearchProfile(String DefaultProfile) throws InterruptedException {
		profileListSearchTextField.clear();
		sleep(2);
		profileListSearchTextField.sendKeys(DefaultProfile);
		sleep(2);
	}

	public void VerifyAppDispalyedInHomeTabDevice() throws InterruptedException {
		boolean AppDisplayed= Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Facebook' and @index='1']").isDisplayed();
		String pass="PASS>> App is Installed";
		String fail="FAIL>> App not installed";
		ALib.AssertTrueMethod(AppDisplayed, pass, fail);
		sleep(2);
	}

	public void InstalltionOfAppOndeviceSide() throws InterruptedException {

		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,1800);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/done_button")));
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/done_button").click();

		sleep(2);
	}

	public void InstalltionOfAppOndevice() throws InterruptedException {

		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,1800);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/ok_button").click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/launch_button")));
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/launch_button").click();
		sleep(2);
	}

	public void InstallationLiteApp() throws InterruptedException {
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,1800);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.packageinstaller:id/ok_button")));
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/ok_button").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/ok_button").click();
		sleep(2);
		Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/done_button")).click();
	}

	public void VerifyAppUnistallation() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Uninstall']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();
		sleep(3);
		Clickon_HOMEtab();
		boolean bln=false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='AstroMail']").isDisplayed(); 
		}catch (Exception e) {
			bln=true;
		}
		ALib.AssertTrueMethod(bln, "PASS>>App Unistalled", "FAIL>>App Not unistalled");
	}

	@FindBy(xpath="//*[@id='policyTable']/tbody/tr/td[contains(text(),'CreationApplicationPolicy')]")
	private WebElement SelectProfileForDefault;

	@FindBy(xpath="//*[@id='setDefaultPolicy']")
	private WebElement setDefaultPolicy;
	public void VerifyInstalledAppsPresent() throws InterruptedException {
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='AstroMail']").isDisplayed();
		String pass="PASS>> 'Application 'is installed already "+apk+"";
		String fail="FAIL>> 'Application 'is Installed already"+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
		sleep(1);
		boolean apk1= Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Lite']").isDisplayed();
		String pass1="PASS>> 'Application 'is installed already "+apk+"";
		String fail1="FAIL>> 'Application 'is Installed already"+apk+"";
		ALib.AssertTrueMethod(apk1, pass1, fail1);

	}

	public void MakeProfileDefault() throws InterruptedException {

		SelectProfileForDefault.click();
		sleep(2);
		setDefaultPolicy.click();
		waitForXpathPresent("//span[contains(text(),'Changed default profile.')]");
		sleep(20);
	}

	public void Clickon_HOMEtab() throws InterruptedException 
	{
		Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Home']")).click();
		sleep(3);
	}

	public void VerifyWebAppsPresent() {
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Flipkart']").isDisplayed();
		String pass="PASS>> 'Flipkart Web App'is present "+apk+"";
		String fail="FAIL>> 'Flipkart Web App'is present "+apk+"";
		ALib.AssertTrueMethod(apk, pass, fail);
		boolean apk1 = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Amazon']").isDisplayed();
		String pass1="PASS>> 'Amazon Web App'is present "+apk+"";
		String fail1="FAIL>> 'Amazon Web App'is present "+apk+"";
		ALib.AssertTrueMethod(apk1, pass1, fail1);
	}

	public void VerifyTheAllTyepsOfApp() {
		WebElement apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Facebook']");
		if(apk.isDisplayed()) {
			Reporter.log("PASS>>Web app Present");
		}else {
			Reporter.log("Fail>>web App not present");

		}
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Store']").click();

		WebElement apk1=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='GangstarGames']");
		WebElement apk2=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureFox']");
		if(apk1.isDisplayed() && apk2.isDisplayed()) {
			Reporter.log("PASS>> Both APK are present");
		}else {
			Reporter.log("FAil>> APK's are NOT present");
		}
	}

	public void isprofile2webappreplaced() {
		if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Facebook']").isDisplayed()) {
			Reporter.log("PASS>>profile to replaced");
		}
		else 
		{
			Reporter.log("FAIL>>Prfolie NOT Replaced");
		}
	}
	
	public void clickOnDeleteBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='deletePolicyBtn']")).click();
		sleep(3);
	}
public void clickOnDeleteYESBtn() throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath(
						"//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']"))
				.click();
		sleep(3);
	}

	@FindBy(xpath="//*[@id='profileList_tableCont']/div/div[2]/div[1]/div[2]/div[1]/div/input")
	private WebElement SearchFiledProfile;

	public void SelectProfile(String profile) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='" + profile + "']"))
				.click();
		sleep(2);
	}
	public void DeleteCreatedProfile(String profile) throws InterruptedException {
		   SearchFiledProfile.sendKeys(profile);
			int profiles = Initialization.driver.findElements(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='" + profile + "']")).size();
			sleep(2);
			for(int i= 0;i<profiles;i++ ){
			SelectProfile(profile);
			sleep(2);
			clickOnDeleteBtn();
			sleep(2);
			clickOnDeleteYESBtn();
			sleep(2);}
			SearchFiledProfile.clear();
	}

}
