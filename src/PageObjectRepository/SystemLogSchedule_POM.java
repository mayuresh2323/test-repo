package PageObjectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class SystemLogSchedule_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="ShowAllLogs")
	private WebElement ShowAllLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowAllLogs']]/following-sibling::span")
	private WebElement ShowAllLogs;
	
	@FindBy(id="ShowOnlineOfflineLogs")
	private WebElement ShowOnlineOfflineLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowOnlineOfflineLogs']]/following-sibling::span")
	private WebElement ShowOnlineOfflineLogs;
	
	@FindBy(id="ShowAppInstallLogs")
	private WebElement ShowAppInstallLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowAppInstallLogs']]/following-sibling::span")
	private WebElement ShowAppInstallLogs;
	
	@FindBy(id="ShowJobLogs")
	private WebElement ShowJobLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowJobLogs']]/following-sibling::span")
	private WebElement ShowJobLogs;
	
	@FindBy(id="ShowRemoteSupportLogs")
	private WebElement ShowRemoteSupportLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowRemoteSupportLogs']]/following-sibling::span")
	private WebElement ShowRemoteSupportLogs;
	
	@FindBy(id="ShowDeviceInfoLogs")
	private WebElement ShowDeviceInfoLogsCheckbox;
	
	@FindBy(xpath="//span[input[@id='ShowDeviceInfoLogs']]/following-sibling::span")
	private WebElement ShowDeviceInfoLogs;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement ReportCycleColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement MailToColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement GroupColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement AllLogsColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement JobLogsColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement OnlineOfflineColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement RemoteSupportColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement AppInstallUnistallColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedule_systemlog']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement DeviceInfoColumnName;
}
