package Library;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common.Initialization;


public class WebDriverCommonLib {

	
	public void pagemaximize(){
		Driver.driver.manage().window().maximize();
		
	}
	
	public void refesh() throws InterruptedException{  
		Driver.driver.navigate().refresh();
		sleep(2);
	}
	
	public String getTittle(){
		String pageTittle = Driver.driver.getTitle();
	return pageTittle;
	}
	
	public void waitForPageToLoad(){
		Driver.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public void waitForXpathPresent(String wbXpath){
		WebDriverWait wait = new WebDriverWait(Driver.driver,150);
		wait.until(ExpectedConditions.
			 presenceOfAllElementsLocatedBy(By.xpath(wbXpath)));		
	}
	
	public void waitForNamePresent(String wbName){
		WebDriverWait wait = new WebDriverWait(Driver.driver,180);
		wait.until(ExpectedConditions.
				presenceOfAllElementsLocatedBy(By.name(wbName)));
			
	}
	
	public void waitForidPresent(String wbid){
		WebDriverWait wait = new WebDriverWait(Driver.driver,180);
		wait.until(ExpectedConditions.
				presenceOfAllElementsLocatedBy(By.id(wbid)));		
	}
	
	public void waitForVisibilityOf(String wbxpath){
		WebDriverWait wait = new WebDriverWait(Driver.driver,180);
		wait.until(ExpectedConditions.
				presenceOfAllElementsLocatedBy(By.xpath(wbxpath)));	
	}
	
	public void waitForidClickable(String wbid){
		WebDriverWait wait = new WebDriverWait(Driver.driver,180);
		wait.until(ExpectedConditions.elementToBeClickable
				(By.id(wbid)));		
	}
	public void sleep(int time) throws InterruptedException
	{
		Thread.sleep(time *1000);
	}
	
	 public void waitForElementPresent(String xpath) throws InterruptedException
     {
   	  WebDriverWait wi=new WebDriverWait(Initialization.driver,180);
   	  wi.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
   	  
   	  sleep(4);
   	  
     }
	 
	 public void waitForLogXpathPresent(String Log) throws InterruptedException
	 {
		 WebDriverWait wi=new WebDriverWait(Initialization.driver,300);
	   	  wi.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Log)));
	   	  sleep(4);
	 }
	 
	
	
	
}
