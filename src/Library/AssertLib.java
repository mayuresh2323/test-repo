package Library;

import org.testng.Assert;
import org.testng.Reporter;

public class AssertLib {

	public void AssertEqualsMethod(String ExpectedResult,String actual,String PassStatement,String FailStatement)
	{
		Assert.assertEquals(actual,ExpectedResult,FailStatement);
		 Reporter.log(PassStatement,true);
	}
	
	public void AssertEqualsMethodBoolean(boolean ExpectedResult,String ActualResult,String PassStatement,String FailStatement)
	{
		Assert.assertEquals(ActualResult,ExpectedResult,FailStatement);
		 Reporter.log(PassStatement,true);
	}
	

	public void AssertTrueMethod(boolean value,String PassStatement,String FailStatement)
	{
		Assert.assertTrue(value,FailStatement);
		 Reporter.log(PassStatement,true);
	}
	
	public void AssertFalseMethod(boolean value,String PassStatement,String FailStatement)
	{
		Assert.assertFalse(value,FailStatement);
		 Reporter.log(PassStatement,true);
	}
	
	public void AssertEqualsMethod1(String ExpectedResult,String ActualResult,String PassStatement,String FailStatement)
	{
		Assert.assertEquals(ActualResult,ExpectedResult,FailStatement);
		 Reporter.log(PassStatement,true);
	}
	
	public void AssertEqualsMethodInt(int ExpectedResult,int ActualResult,String PassStatement,String FailStatement)
	{
		Assert.assertEquals(ActualResult,ExpectedResult,FailStatement);
		 Reporter.log(PassStatement,true);
	}

	public void AssertFailMethod(String FailMessage)
	{
		Assert.fail(FailMessage);
		
		 Reporter.log(FailMessage,true);
	}
}

